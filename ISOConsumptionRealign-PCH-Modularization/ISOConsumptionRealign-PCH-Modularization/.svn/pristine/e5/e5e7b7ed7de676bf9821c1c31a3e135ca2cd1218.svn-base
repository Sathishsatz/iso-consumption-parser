grammar AlgorithmLoader;

options {
  language = Java;
  rewrite=true;
}

tokens {
  EQUALS;
  NOTEQUALS;
  DIV;
  NEGATION;
  SUBTRACT;
}

@header {
package com.mmpnc.rating.iso.excel.algorithm.loader;

import com.mmpnc.rating.icm.algorithm.vo.Ratebook;
import com.mmpnc.rating.icm.algorithm.vo.RatingEntity;
import com.mmpnc.rating.icm.algorithm.vo.Reference;
import com.mmpnc.rating.icm.algorithm.vo.Process;
import com.mmpnc.rating.icm.algorithm.vo.Program;
import com.mmpnc.rating.icm.algorithm.vo.Step;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
}

@lexer::header {
package com.mmpnc.rating.iso.excel.algorithm.loader;
  
import com.mmpnc.rating.icm.algorithm.vo.Ratebook;
import com.mmpnc.rating.icm.algorithm.vo.RatingEntity;
import com.mmpnc.rating.icm.algorithm.vo.Reference;
import com.mmpnc.rating.icm.algorithm.vo.Process;
import com.mmpnc.rating.icm.algorithm.vo.Program;
import com.mmpnc.rating.icm.algorithm.vo.Step;
  
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
}

@members {
    private Process process;
    private Context context;
    private int order;
    
    //update common object list
    private <T> T updateList(List<T> list, T object) {
        for (T t : list) {
            if (t.equals(object)) {
                return t;
            }
        }
        list.add(object);
        return object;
    }
    
    private <T> boolean isPresent(List<T> list, T object) {
        for (T t : list) {
            if (t.equals(object)) {
                return true;
            }
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    private <T> T updateContext(Context context, String objectName, T obj) {
        if (objectName.equals("Reference")) {
            obj = updateList((List<T>) context.getValue("ReferenceList"), obj);
        } else if (objectName.equals("Entity")) {
            obj = updateList((List<T>) context.getValue("EntityList"), obj);
        } else if (objectName.equals("Program")) {
            obj = updateList((List<T>) context.getValue("ProgramList"), obj);
        } else if (objectName.equals("Process")) {
            obj = updateList((List<T>) context.getValue("ProcessList"), obj);
        } else if (objectName.equals("Step")) {
            obj = updateList((List<T>) context.getValue("StepList"), obj);
        }

        return obj;
    }
}

algorithm [Ratebook ratebook, Context paramContext] returns [Context retContext] :
	{
	    if(paramContext == null){
		    this.context = new ContextBase();
		    this.context.putValue("EntityList", new ArrayList<RatingEntity>());
	        this.context.putValue("ReferenceList", new ArrayList<Reference>());
	        this.context.putValue("ProcessList", new ArrayList<Process>());
	        this.context.putValue("StepList", new ArrayList<Step>());
	        this.context.putValue("ProgramList", new ArrayList<Program>());
        }else{
            this.context = paramContext; 
        }
 	}
    (sheet[ratebook])+ EOF 
        { 
            retContext = this.context;
        };

sheet [Ratebook ratebook]  : (rule[ratebook])? (processblock[ratebook]);

rule [Ratebook ratebook]   : 'RUL' r=IDENTS 'TAB' e=IDENTS
        {
            RatingEntity entity = new RatingEntity();
            entity.setName($e.text);
            entity = updateContext(context,"Entity",entity);
            entity.setRatebook(ratebook);
            
            updateList(ratebook.getRatingEntity(), entity);
            
            Reference reference = new Reference();
            reference.setName($r.text);
            reference.setEntity(entity);
            reference = updateContext(context,"Reference",reference);
            reference.setRatebook(ratebook);
            ratebook.getReference().add(reference);
            
            entity.getReferences().add(reference);            
        }
        ;

processblock [Ratebook ratebook]
	    @init{
	        process = new Process();
	        order = 1;
	        StringBuffer buffer = new StringBuffer();
	    }
		:
		('BBB'|'CCH') (p=STRING)? n=IDENTS 
		{
		    RatingEntity entity = new RatingEntity();
            entity.setName($n.text);
            entity = updateContext(context,"Entity",entity);
            entity.setRatebook(ratebook);
            
            updateList(ratebook.getRatingEntity(), entity);
            
            process = new Process();
            process.setName($p.text.replaceAll("^\"|\"$",""));
            process.setEntity(entity);

            process = updateContext(context,"Process",process);
            
            entity.getProcesses().add(process);
		}
		(
		  {
		    buffer.delete(0, buffer.length());
		  }
		statement[buffer]
			{
	            if(buffer != null && buffer.length() > 0 ){
	                  Step step = new Step();
                    step.setBlock(buffer.toString());
                    step.setExecutionorder(order);
                    step.setProcess(process);
                    
                    step = updateContext(context,"Step",step);
                    
                    process.getSteps().add(step);
                    order++;
	            }    
	        }		
		)* 
        (pch[process])*;

pch [ Process process] : ('PCH' | 'SCH') (i=IDENTS|s=STRING) values=statements 
        {
//            StringBuffer prgBlock = new StringBuffer();
            int counter = 1;
            
            Program program = new Program();
            if($i != null){
                program.setName($i.text);
            }else{
                program.setName($s.text.replaceAll("^\"|\"$",""));
            }
            program.setProcess(process);
            program.setExecutionorder(order);
            
            for(String str : values)
            {
                Step step = new Step();
                step.setBlock(str);
                step.setExecutionorder(counter);
                step.setProgram(program);
                
                program.getStep().add(step);
//              prgBlock.append(str).append("\n");
                counter++;
            }
                       
//            program.setBlock(prgBlock.toString());
            
            program = updateContext(context,"Program",program);
           
            if(! isPresent(process.getPrograms(),program))
            {
	            process.getPrograms().add(program);
	            
	            //add this program as step in process e.g Call Program
//	             Step step = new Step();
//	             step.setBlock("Call " + $i.text);
//	             step.setExecutionorder(order);
//	             step.setProcess(process);
//	             
//	             step = updateContext(context,"Step",step);
//	             
//	             process.getSteps().add(step);
	             order++;
	          } 
            
        }
        ;

statements returns [List<String> values] 
        @init{
          StringBuffer buffer = new StringBuffer();
        }
        :
        {
            values = new ArrayList<String>();
            
        }
        statement[buffer] {$values.add(buffer.toString());} ({buffer.delete(0, buffer.length());} statement[buffer]{$values.add(buffer.toString());})*
        ;

statement [StringBuffer buffer]
        : assignment[$buffer] | ifStatement[$buffer] | loopStatement[$buffer] | function[$buffer] | callProgram[$buffer]
        ;

function [StringBuffer buffer]: 
        IDENTS '(' {$buffer.append($IDENTS.text).append(" ( ");} ('Using'?) (arithmeticExp[$buffer] (',' {$buffer.append(" , ");} arithmeticExp[$buffer])*)? ')' {$buffer.append(" )");};

domainTable [StringBuffer buffer]: 
        IDENTS '{' {$buffer.append($IDENTS.text).append(" {");} (atom[$buffer] (',' {$buffer.append(" , ");} atom[$buffer])*)? '}' {$buffer.append(" }");};

callProgram [StringBuffer buffer]:
        'Call' IDENTS {$buffer.append("Call ").append($IDENTS.text);};

specialFunction [StringBuffer buffer]
            @init{
              StringBuffer intermediateBuffer = new StringBuffer();
            }
            :'(' arithmeticExp[intermediateBuffer] ')' SPECIALFUNCTION    
            {
                $buffer.append($SPECIALFUNCTION.text).append("( ").append(intermediateBuffer).append(" )");
            }
            ;

rateFunction [StringBuffer buffer]: 
            'RateTable' ':' {$buffer.append("RateTable:");} function[$buffer] ;
        
assignment [StringBuffer buffer]:
        (IDENTS {$buffer.append($IDENTS.text);} | varType[$buffer] | xpath[$buffer] ) '=' {$buffer.append(" = ");}  arithmeticExp[$buffer];
        
ifStatement [StringBuffer buffer]: 
        'IF' {$buffer.append(" IF ");} ('NOT' {$buffer.append(" NOT ");})? '(' {$buffer.append(" ( ");} booleanExp[$buffer] ')' {$buffer.append(" ) ");} st1=statements { for(String str : $st1.values) { $buffer.append(str).append(" ");} }
        ('ELSE' {$buffer.append(" ELSE ");} st2=statements { for(String str : $st2.values) { $buffer.append(str).append(" ");} })?    
        'END IF' {$buffer.append(" END IF ");}
        ;

loopStatement [StringBuffer buffer]:
        'LOOP THROUGH' s1=STRING {$buffer.append(" LOOP THROUGH ").append($s1.text);} 
        st1=statements { for(String str : $st1.values) { $buffer.append(str).append(" ");} }
        'END LOOP' s2=STRING {$buffer.append(" END LOOP ").append($s2.text);}   
        ;
            
atom [StringBuffer buffer]
  : ( IDENTS {$buffer.append($IDENTS.text);} | function[$buffer] | expression[$buffer] | INTEGER {$buffer.append($INTEGER.text);} | STRING {$buffer.append($STRING.text);} | xpath[$buffer] | rateFunction[$buffer] | varType[$buffer] | domainTable[$buffer] | CHAR {$buffer.append($CHAR.text);} );

negation [StringBuffer buffer]:
        ('not' {$buffer.append(" not ");})? atom[$buffer]
        ;

unary [StringBuffer buffer]:
    (('+' {$buffer.append(" + ");} | negative {$buffer.append($negative.text);}))* negation[$buffer]
    ;
    
multi [StringBuffer buffer] :
    unary[$buffer] (( '*' {$buffer.append(" * ");} | '/' {$buffer.append(" / ");} | 'mod' {$buffer.append(" mod ");} ) unary[$buffer])*
    ;
  
arithmeticExp [StringBuffer buffer]:
    multi[$buffer] (( '+' {$buffer.append(" + ");} | subtraction {$buffer.append($subtraction.text);} ) multi[$buffer] )*
    ;

relation [StringBuffer buffer]:
    arithmeticExp[$buffer] (( equals {$buffer.append($equals.text);} | notequals {$buffer.append($notequals.text);} | '<' {$buffer.append(" < ");} | '<=' {$buffer.append(" <= ");} | '>' {$buffer.append(" > ");} | '>=' {$buffer.append(" >= ");} )  arithmeticExp[$buffer] )*
    ;
    
equals:
    'Is Equal To'
    ;

notequals:
    'Not Equal To'
    ;

negative :
    '-'|'Negative'
    ;
    
subtraction :
    '-'|'Subtract'
    ;
    
booleanExp [StringBuffer buffer] : relation[$buffer] (( 'And' {$buffer.append(" And ");} | 'Or' {$buffer.append(" Or ");} ) relation[$buffer] )*;

expression [StringBuffer buffer]
    : ('(' arithmeticExp[new StringBuffer()] ')' SPECIALFUNCTION ) => specialFunction[$buffer]
    | '(' { $buffer.append("( "); } booleanExp[$buffer] ')' { $buffer.append(" )"); };

xpath [StringBuffer buffer] : XPATHCONSTANT ':' STRING 
        {
            $buffer.append($XPATHCONSTANT.text).append(":").append($STRING.text);
        }
        ;

varType [StringBuffer buffer] : VARCONSTANT ':' (t=IDENTS | s=STRING) 
        { 
            if($t!=null)
            {
                $buffer.append($VARCONSTANT.text).append(":").append($t.text);
            }else{
                $buffer.append($VARCONSTANT.text).append(":").append($s.text);
            }   
        };

XPATHCONSTANT : 'XPATH' | 'XPATH_STRING' | 'XPATH_NUMERIC';
VARCONSTANT : 'COLUMN_NUMERIC' | 'COLUMN_STRING' | 'UI' | 'LV_NUMERIC' | 'LV_STRING' | 'CONSTANT';
SPECIALFUNCTION : 'RoundUpDollar' | 'RoundUpHundredth' | 'RoundUpHundredThousandth' | 'RoundUpThousandth' | 'RoundDollar' | 'RoundHundredth' | 'RoundHundredThousandth' | 'RoundThousandth' | 'Ceiling' | 'RoundTenThousandth';
IDENTS : ('a'..'z' | 'A'..'Z' | '_' | '/' | '.')('a'..'z' | 'A'..'Z' | '_' | '0'..'9' | '/' | '.' | '*' )*;
INTEGER : ('0'..'9')+('.' ('0'..'9')*)?;
STRING : '"' .* '"';
CHAR    : '\'' ('a'..'z' | 'A'..'Z' |'0'..'9')? '\'';
WS : (' '| '\n'| '\r'| '\t')* {$channel = HIDDEN;};
COMMENTS : '//' .* ('\n'|'\r') {$channel = HIDDEN;};
MULTI_COMMENT : '/*' .* '*/' {$channel = HIDDEN;};
