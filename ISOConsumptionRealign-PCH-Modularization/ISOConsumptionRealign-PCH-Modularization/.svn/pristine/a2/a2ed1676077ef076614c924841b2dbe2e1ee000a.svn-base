package com.mmpnc.rating.iso.algorithm.util;

import java.util.Map;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.AlgorithmProcessor;
import com.mmpnc.rating.iso.algorithm.ArgumentVariable;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.Evaluator.Type;
import com.mmpnc.rating.iso.algorithm.IgnorePCHHolder;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.exception.UnInitializeContextException;
import com.mmpnc.rating.iso.algorithm.sort.AlgorithmSorter;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.xml.XMLNodeReader;
import com.mmpnc.xml.xpath.parse.XPathActivity;
import com.mmpnc.xml.xpath.parse.XPathParserImpl;

public class AlgoUtil {

	// private static final Logger logger =
	// LoggerFactory.getLogger(AlgoUtil.class);

	public static void processStatementBlock(Object block, Context context)
			throws Exception {
		if (block instanceof PCH) {

			PCH pch = (PCH) block;

			// commented below block to check that same local variable will get
			// called by various programs.
			// if(pch.getExecuted()!= null && pch.getExecuted().equals("true")){
			// ISOConsumptionLogger.info("Already executed :)");
			// return;
			// }

			// ISOConsumptionLogger.info("Marked Processed [ " +
			// pch.getName().trim() + " ]");

			pch.setExecuted("true");

			/*
			 * AlgorithmConfigurator config = (AlgorithmConfigurator)
			 * context.getValue(ContextParam.CONFIGURTOR);
			 * 
			 * if(config.getIgnorePCH().isAvailable(pch.getName().trim())) {
			 * //logger.info("PCH ignored - " + pch.getName() +
			 * " as this is marked in ignored PCH list "); return; }
			 */

			if (IgnorePCHHolder.isAvailable(pch.getName().trim())) {
				ISOConsumptionLogger.info("PCH ignored - " + pch.getName());
				return;
			}
			@SuppressWarnings("unchecked")
			Map<String, String> processedBlocks = (Map<String, String>) context
					.getValue(ContextParam.PROCESSEDBLOCK);
			processedBlocks.put(pch.getName().trim(), pch.getName().trim());

			context.putValue(ContextParam.CURRENTPROCESS, pch.getName().trim());

			// ISOConsumptionLogger.info("pch - " + pch.getName());
			// ISOConsumptionLogger.info("pch - " + pch.getExcelRow() + " " +
			// pch.getExcelSheet());
			// algo.append("****** PCH Start - "
			// ).append(pch.getName()).append("\n");

			/*
			 * if(logger.isInfoEnabled()){ logger.info("Started Processing ####"
			 * + pch.getName()); }
			 */

			for (Object statement : pch.getContent()) {
				try {

					/*
					 * if(logger.isInfoEnabled()){
					 * logger.info("We get the statement : "+
					 * statement.toString()); }
					 */

					/*StringBuffer sb = (StringBuffer) context
							.getValue(ContextParam.ALGO);*/
					AlgoUtil.executeStatement(statement, context);
					// context.putValue(ContextParam., value)

					// if(logger.isInfoEnabled()){
					// logger.info("Algo step in context " +
					// context.getValue(ContextParam.ALGO));
					// }

				} catch (Exception _ex) {
					// logger.error("unable to process " + pch.getName().trim()
					// + " in " + context.getValue(ContextParam.SCOPE) , _ex);
					throw new RuntimeException(_ex);
				}
			}

			/*
			 * if(logger.isInfoEnabled()){ logger.info("End of Processing #### "
			 * + pch.getName()); }
			 */

			// algo.append("****** PCH End - "
			// ).append(pch.getName()).append("\n");
			// algo.append("****** PCH processed - ").append(pch.getExecuted()).append("\n");

		} else if (block instanceof String) {
			;
		} else {
			executeStatement(block, context);
		}
	}

	public static void executeStatement(Object block, Context context)
			throws Exception {

		/**
		 * process the statement to check if it contains reference to other pch
		 * meaning whether we need to include the other pch block before this
		 * statement
		 */

		StringBuffer content = new StringBuffer();
		XMLNodeReader xmlNodeReader = new XMLNodeReader(context);
		xmlNodeReader.readContent(block, content);

		if (content.length() != 0) {
			// ISOConsumptionLogger.info("Process Statement -> [ " + content +
			// " ]");
			context.putValue(ContextParam.PCHALGO, new StringBuffer());
			// context.putValue(ContextParam.SPACECOUNT, new Integer(0));

			AlgorithmProcessor processor = new AlgorithmSorter(context, content);
			processor.execute();
			// ISOConsumptionLogger.info("processed Statemen -> ["+content+"]");

		}

		// commented below to test if we can rearrange the execution steps.

		// StringBuffer algo = (StringBuffer)
		// context.getValue(ContextParam.ALGO);
		// algo.append("\n").append(content);

		// update block executed flag
		/*
		 * if(block instanceof If){ StringBuffer ifAlgo = (StringBuffer)
		 * context.getValue(ContextParam.ALGO); StringBuffer pchAlgo =
		 * (StringBuffer) context.getValue(ContextParam.PCHALGO);
		 * pchAlgo.append(ifAlgo);
		 * 
		 * context.putValue(ContextParam.ALGO, pchAlgo); ((If)
		 * block).setExecuted("true");
		 * 
		 * }else if(block instanceof Assign){ StringBuffer assignAlgo =
		 * (StringBuffer) context.getValue(ContextParam.ALGO); StringBuffer
		 * pchAlgo = (StringBuffer) context.getValue(ContextParam.PCHALGO);
		 * pchAlgo.append(assignAlgo);
		 * 
		 * context.putValue(ContextParam.ALGO, pchAlgo); ((Assign)
		 * block).setExecuted("true");
		 * 
		 * }else if(block instanceof Loop){
		 * 
		 * ((Loop) block).setExecuted("true"); }
		 */
	}

	public static boolean searchAndProcess(String variableType,
			String processName, Context context) throws ParserException {

		JXPathContext currentjxpContext = (JXPathContext) context
				.getValue(ContextParam.JXPATHCONTEXT);
		try {
			StringBuffer xpath = new StringBuffer();

			// below statement changed to include all the local variable PCH
			// blocks in all the main PCH blocks
			// xpath.append(currentjxpContext.getContextPointer().asPath()).append("/following-sibling::*[@recordType=\"PCH\" and @name=\""
			// + processName + "\" and not( @executed ) ][1]");

			String jxpathPointer = currentjxpContext.getContextPointer()
					.asPath();
			xpath.append(jxpathPointer).append(
					"/following-sibling::*[@recordType=\"PCH\" and @name=\""
							+ processName + "\" ][1]");

			Pointer refPointer = currentjxpContext.getPointer(xpath.toString());
			// THIS CODE IS ADDED TO CHECK IF THE VARIABLE PCH BLOCK IS
			// PRECEDING THE CURRENT REFERENCE POINT AS WE FOUND IT IN WC FILES
			if (refPointer != null && refPointer.getNode() == null) {
				xpath = new StringBuffer();
				xpath.append(jxpathPointer).append(
						"/preceding-sibling::*[@recordType=\"PCH\" and @name=\""
								+ processName + "\" ][1]");
				refPointer = currentjxpContext.getPointer(xpath.toString());
			}
			if (refPointer != null && refPointer.getNode() != null) {
				JXPathContext relativeContext = currentjxpContext
						.getRelativeContext(refPointer);
				@SuppressWarnings("unused")
				PCH pch = (PCH) currentjxpContext.getValue(xpath.toString());


				context.putValue(ContextParam.JXPATHCONTEXT, relativeContext);

				return true;
			}
			else {
				if (variableType != null) {
					@SuppressWarnings("unchecked")
					Map<String, String> processedBlock = (Map<String, String>) context
							.getValue(ContextParam.PROCESSEDBLOCK);

					if (!processedBlock.containsKey(processName)) {
						if (!variableType.contains("XPATH")) {
							// logger.error("*********** Variable does not point to a process "
							// + processName + " in " +
							// context.getValue(ContextParam.CURRENTPROCESS) +
							// " in " + context.getValue(ContextParam.SCOPE));
						}
						// else{
						// if(!ArgumentVariable.isVariablePresent((Database)context.getValue(ContextParam.OBJECTDATABASE),
						// (String)context.getValue(ContextParam.REFERENCE),
						// (String)context.getValue(ContextParam.SCOPE),
						// processName)){
						// System.err.println("########## XPath/Variable " +
						// processName + " is incorrect in scope " +
						// (String)context.getValue(ContextParam.SCOPE));
						// }
						// }
						if ((currentjxpContext.getContextBean() instanceof PCH)
								&& (processName.equals(((PCH) currentjxpContext
										.getContextBean()).getName()))) {
							return true;
						}
						processedBlock.put(processName, processName);
					} 
				}
				return false;
			}
		} catch (Exception _ex) {
			// logger.error("error occred to search " + processName, _ex);
			throw new ParserException("error occred to search " + processName);
		}

	}

	public static String updateVariableType(Context context, String str)
			throws UnInitializeContextException {
		if (context == null) {
			ISOConsumptionLogger.info("Please set the Context");
			throw new UnInitializeContextException("UnInitializeContextError");
		}

		if (str.contains("Total Premium")) {
			return "Premium";
		} else {
			StringBuffer xpathBuffer = new StringBuffer();
			StringBuffer value = new StringBuffer();

			context.putValue(ContextParam.XPATHSTRING, xpathBuffer);
			context.putValue(ContextParam.ISPREDICATE, false);
			context.putValue(ContextParam.ISFUNCTION, false);
			context.putValue(ContextParam.FUNCTIONNAME, "");
			/*
			 * if(str != null && str.contains("["))
			 * context.putValue(ContextParam.PREDICATEPARSER, true);
			 */

			XPathActivity parse = new XPathParserImpl(context,
					new StringBuffer(str.trim()));
			Evaluator.Type type;
			try {
				type = (Type) parse.execute();
				if (type.equals(Type.PATH)) {
					value.append("XPATH:\"")
							.append(context.getValue(ContextParam.XPATHSTRING))
							.append("\"");
				} else if (type.equals(Type.ATTRIBUTE)) {
					value.append(ArgumentVariable.assignType((Database) context
							.getValue(ContextParam.OBJECTDATABASE),
							(String) context.getValue(ContextParam.REFERENCE),
							(String) context.getValue(ContextParam.SCOPE),
							context.getValue(ContextParam.XPATHSTRING)
									.toString()));
				} else {
					value.append(context.getValue(ContextParam.XPATHSTRING));
				}
			} catch (Exception e) {
				StringBuffer error = new StringBuffer();
				error.append("Error : ").append(e)
						.append(" During Processing ").append(str.trim())
						.append(" in ");
				error.append("Reference - [")
						.append(context.getValue(ContextParam.REFERENCE))
						.append("] ");
				error.append("Scope - [")
						.append(context.getValue(ContextParam.SCOPE))
						.append("]");
				// ISOConsumptionLogger.info(error);
				value.append("XPATH:\"").append(str.trim()).append("\"");
			}

			return value.toString();
		}
	}

}