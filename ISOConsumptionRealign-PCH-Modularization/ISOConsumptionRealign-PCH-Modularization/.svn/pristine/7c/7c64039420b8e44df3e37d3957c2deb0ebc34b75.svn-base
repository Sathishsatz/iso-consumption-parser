package com.mmpnc.rating.iso.algorithm.correction;

import java.util.ArrayList;
import java.util.List;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.Activity;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.rating.iso.helper.Constants;

public class QualifiedAlgorithObject implements Activity {

	private Connection connection;
	private Query query;

	public QualifiedAlgorithObject(Database db) {
		this.connection = new XMLConnection(db);
		this.query = new XmlQuery(this.connection);
	}

	public <T> T process(T lob) throws Exception {
		updateLob((LOB)lob);
		return lob;
	}

	private void updateLob(LOB lob) throws Exception {
		List<Reference> refList = new ArrayList<Reference>();

		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				updateReference((Reference) ref, lob, refList);
			}
		}
	}

	private void updateReference(Reference ref, LOB lob, List<Reference> refList) throws Exception {
		List<Object> scopeList = ref.getContent();

		if (ref.getType().equals("Premium Calculation")
				|| ref.getType().equals("Common Rating")) {
			for (Object scope : scopeList) {
				if (scope instanceof Scope) {
					updateScope((Scope) scope, ref, lob, refList);
				}
			}
		}
	}

	private void updateScope(Scope scope, Reference ref, LOB lob,
			List<Reference> refList) throws Exception {

		scope.setFullyQualifiedPath(getParentXpath(((Scope) scope)
				.getDbTables()));

		List<Object> pchList = scope.getContent();
		for (Object obj : pchList) {
			if (obj instanceof PCH) {
				updatePCH((PCH) obj, scope, ref, lob, refList);
			} else {
				updateContent(obj, scope.getFullyQualifiedPath());
			}
		}
	}

	private void updatePCH(PCH obj, Scope scope, Reference ref, LOB lob,
			List<Reference> refList) throws Exception {
		List<Object> statementList = obj.getContent();
		for (Object statement : statementList) {
			updateContent(statement, scope.getFullyQualifiedPath());
		}
	}

	private void updateContent(Object obj, String scope) throws Exception {
		if (obj instanceof If) {
			updateIf((If) obj, scope);
		} else if (obj instanceof Assign) {
			updateAssign((Assign) obj, scope);
		} else if (obj instanceof Loop) {
			updateLoop((Loop) obj, scope);
		}
	}

	private void updateLoop(Loop loop, String scope) throws Exception {
		loop.setFullyQualifiedPath(getFullyQualifiedXpath(scope,
				loop.getThrough()));
		for (Object obj : loop.getContent()) {
			updateContent(obj, scope);
		}
	}

	private void updateAssign(Assign obj, String scope) throws Exception {
		obj.setFullyQualifiedPath(validQualPath(scope, obj.getLValue()));
		for (Object content : obj.getContent()) {
			if (content instanceof Expression) {
				updateExpression((Expression) content, scope);
			}
		}

	}

	private void updateExpression(Expression expression, String scope) throws Exception {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression exp = (Expression) obj;
				updateExpression(exp, scope);
			} else if (obj instanceof Function) {
				updateFunction((Function) obj, scope);
			} else if (obj instanceof Ratetable) {
				updateRatetable((Ratetable) obj, scope);
			} else if (obj instanceof String) {
				if (expression.getVariableType() != null
						&& !expression.getVariableType().equals("CONSTANT")) {
					if (obj != null && !obj.equals("")) {

						expression.setFullyQualifiedPath(validQualPath(scope,
								(String) expression.getContent().get(0)));
					}
				}else if(expression.getRuleReference()!=null){
					expression.setFullyQualifiedPath(validQualPath(scope,
							(String) expression.getContent().get(0)));
				}
			}
		}
	}

	private String validQualPath(String scope, String node) throws Exception {

		if (node.contains("domainTableLookup")) {
			return "";
		} else if (node.contains("{")) {
			return "";
		} else if (node.contains("Total Premium")) {
			return getFullyQualifiedXpath(scope, "Premium");
		} else {
			return getFullyQualifiedXpath(scope, node);
		}
	}

	private void updateRatetable(Ratetable ratetable, String scopeReplacement) {
		ISOConsumptionLogger.info("Rate Table " + ratetable.getCode());
		for(Object obj : ratetable.getContent()){
			if(obj instanceof Arg){
				Arg arg = (Arg) obj;
				ISOConsumptionLogger.info("Arg " + arg.getNumber() + " = " + arg.getContent() + " | ");
			}
		}
		ISOConsumptionLogger.info("");	
	}

	private void updateFunction(Function obj, String scopeReplacement) {

	}

	private void updateIf(If _if, String scope) throws Exception {
		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				updateCondition((Condition) obj, scope);
			} else if (obj instanceof Then) {
				updateThen((Then) obj, scope);
			} else if (obj instanceof Else) {
				updateElse((Else) obj, scope);
			}
		}
	}

	private void updateElse(Else _else, String scope) throws Exception {
		for (Object obj : _else.getContent()) {
			updateContent(obj, scope);
		}
	}

	private void updateThen(Then _then, String scope) throws Exception {
		for (Object obj : _then.getContent()) {
			updateContent(obj, scope);
		}
	}

	private void updateCondition(Condition condition, String scope) throws Exception {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				updateExpression((Expression) obj, scope);
			}
		}
	}

	private String getParentXpath(String ParentNode) throws Exception {
		String xpath = "";
		StringBuffer queryString = new StringBuffer();
		queryString.append("//").append(ParentNode).append("/@xpath/data()");

		ISOConsumptionLogger.info("pQuery -> " + queryString);

		query.createQuery(queryString.toString());
		Object obj = query.execute();

		if (obj == null) {
			ISOConsumptionLogger.info("No Data Found");
		} else {
			xpath = (String) obj;
		}

		ISOConsumptionLogger.info("pXpath -> " + xpath);

		return xpath;
	}

	private String getFullyQualifiedXpath(String ParentPath, String node) throws Exception {
		String xpath = "";
		StringBuffer queryString = new StringBuffer();
		queryString.append(ParentPath.replaceAll("(\\[).[^name][^\\]]*(\\])", "")).append("/").append(node)
				.append("/@xpath/data()");

		ISOConsumptionLogger.info("Query -> " + queryString);

		query.createQuery(queryString.toString());
		Object obj = query.execute();

		if (obj == null) {
			ISOConsumptionLogger.info("No Data Found");
		} else {
			if(((String)obj).equals(""))
			{
				xpath = "LOCAL";
			}else{
				xpath = (String) obj;
			}
		}

		if (!xpath.contains(ParentPath)) {
			xpath = xpath.replace(Constants.CURRENT_CONTEXT_OBJECT_NAME, Constants.REQUEST_OBJECT_NAME);
		}

		ISOConsumptionLogger.info("Xpath -> " + xpath);
		
		return xpath;
	}

}
