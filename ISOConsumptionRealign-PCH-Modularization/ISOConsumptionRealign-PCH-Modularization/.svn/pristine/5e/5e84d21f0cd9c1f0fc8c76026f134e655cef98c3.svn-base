package com.mmpnc.rating.iso.algorithm.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.rating.iso.algorithm.IgnorePCHHolder;
import com.mmpnc.rating.iso.algorithm.correction.AlgorithmObjectCorrector;
import com.mmpnc.rating.iso.algorithm.correction.CorrectionForPCHMoularizationImpl;
import com.mmpnc.rating.iso.algorithm.correction.ICorrectionForPCHModularization;
import com.mmpnc.rating.iso.algorithm.correction.PrepareActualVsProcessedPchReport;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.AlgoUtil;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.consumption.returnmodels.CreateICMRequiredData;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.rating.iso.helper.LookupNameDecider;
import com.mmpnc.rating.iso.wrapper.ICMRequiredData;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;
import com.mmpnc.util.IsoConsumptionUtil;

/**
 * @author Nilkanth9581
 * 
 */
public abstract class AlgorithmXMLProcessor extends AlgorithmObjectCorrector {

	// private StringBuffer algorithmLevels = new StringBuffer();

	private LOB lob;
	private Database db;
	// private AlgorithmConfigurator configurator;
	// private boolean rootConent;
	private Database dbForLoop;
	protected List<IcmRatingNodeContent> icmNodeContentList;
	protected Map<String, List<IcmRatingNodeContent>> ratingNodeMap = new HashMap<String, List<IcmRatingNodeContent>>();
	protected ICMRequiredData icmRequiredData;
	private HashMap<String, HashMap<String, String>> commonRatingDataMap;
	private HashMap<String, HashMap<String, String>> premiumCalculationDataMap;

	public AlgorithmXMLProcessor(Reader algoReader) {
		super(algoReader);
		// this.configurator = AlgorithmConfigurator.getInstance(algoReader);;
		// this.rootConent = rootConent;
	}

	private void createBasicStructure(ICMRequiredData icmRequiredData) {
		LookupModels globalLookup = new LookupModels();
		icmRequiredData.setIcmRatingContent(ratingNodeMap);
		icmRequiredData.setLookupModels(globalLookup);
		
		//Getting properties from iso consumption properties file and 
		//setting it into icmrequired data
		
		IsoConsumptionUtil isoConsumptionUtil = IsoConsumptionUtil.getInstance();
		icmRequiredData.setMulitState(new Boolean(isoConsumptionUtil.getProperty(Constants.MULTISTATE_KEY)).booleanValue());
		icmRequiredData.setProductIDLevel(new Integer(isoConsumptionUtil.getProperty(Constants.PRODUCTID_FILTER_KEY)).intValue());
	}

	@Override
	public void output(LOB lob, Database db, Database dbForLoop) {
		this.lob = lob;
		this.db = db;
		this.dbForLoop = dbForLoop;
	}

	@SuppressWarnings("unchecked")
	protected void buildAlgorithm() throws Exception {
		// Correcting algorithm object if it has some pch with / in it it will
		// create a new algorithm for such pch
		process();

		Context context = new ContextBase();
		context.putValue(ContextParam.OBJECTDATABASE, this.db);
		context.putValue(ContextParam.OBJECTBASEFORLOOP, this.dbForLoop);
		// Creating algorithm configurator for newly added sorting process
		try {
			// context.putValue(ContextParam.CONFIGURTOR, this.configurator);
			PrepareActualVsProcessedPchReport prepareReport = PrepareActualVsProcessedPchReport.getInstance();
			prepareReport.process(context, lob);
			commonRatingDataMap = (HashMap<String, HashMap<String, String>>) context.getValue(ContextParam.COMMONRRATING_DATA);
			premiumCalculationDataMap = (HashMap<String, HashMap<String, String>>) context.getValue(ContextParam.PREMIUMCALCULATION_DATA);
			
			icmRequiredData = ICMRequiredData.getInstance();
			// creating and adding empty global lookup list and a rating node
			// map
			createBasicStructure(icmRequiredData);

			StringBuffer pchAlgoText = null;

			// THIS RATEBOOK ID AND VERSION USED TO CREATE THE UNIQUE CRITERIA
			// NAME
			// AS
			// WE MAY HAVE SAME LOOKUPS IN DIFFERENT CIRCULARS OF DIFFERENT
			// STATES
			LookupNameDecider lookupNameDecider = LookupNameDecider
					.getInstance();
			lookupNameDecider.setRateBookId(lob.getRatebookId());
			lookupNameDecider.setVersion(lob.getVersion());
			
			for (Object ref : lob.getContent()) {
				
				String referenceType = null;
				if (ref instanceof Reference) {

					if ((((Reference) ref).getType().equals("Common Rating") || ((Reference) ref).getType().equals("Premium Calculation"))) {
						ISOConsumptionLogger.info("*************STARTING ALGORITHM "+((Reference) ref).getDbTables()+ " *****************");
						System.out.println("*************STARTING ALGORITHM "+ ((Reference) ref).getDbTables()+  " OF TYPE "+((Reference)ref).getType()+" *****************");
						// initializing the icmNodeContent List as we need to
						// create node in icm for every algorithm
						icmNodeContentList = new ArrayList<IcmRatingNodeContent>();
						referenceType = ((Reference) ref).getType();
						List<String> pchNameList = null;
						for (Object scope : ((Reference) ref).getContent()) {
						//	List<AlgoStep> algoSteps = null;
							// every scope is an algorithm in rating engine
							if (scope instanceof Scope) {
								
								StringBuffer completeAlgorithmText = new StringBuffer();
								// creating a list to hold pch names executed in
								// this scope
								pchNameList = new ArrayList<String>();
								IcmRatingNodeContent icmRatingNodeContent = new IcmRatingNodeContent();
								
								
								
								setICMRatingNodeContentData(icmRatingNodeContent, (Scope)scope, (Reference)ref);
								// CREATING JXPATH CONTEXT FOR THE CURRENT SCOPE
								JXPathContext jxpContext = JXPathContext.newContext(scope);
								boolean isStateWideAlgorithm = BasicConfiguration.getInstance().isStateWideAlgorithms();
								int counter = 1;
								//THIS HANDLING IS ADDED FOR THE SCOPES WITH NONE OVERRIDDEN PCH 
								//boolean isScopeWithNoneOverriddenPCH = true;
								boolean didAlgoHasAtLeastOneOverriddenPCH = false;
								for (Object block : ((Scope) scope).getContent()) {
									
									
									if (block instanceof PCH) {
										
										
										PCH pch = ((PCH) block);
										//IF A STATE WIDE ALORITHM THEN CONSUME ONLY ONVERRIDDEN PCH AND IF COUNRYWIDE THEN CONSUEM ALL THE PCH
										if(isStateWideAlgorithm){
											if(!pch.getOverridden()){
												prepareReport.addToOverridenPchMap(((Scope)scope).getDbTables(), pch.getName(),pch.getOverridden());
												continue;
											}
										}
										didAlgoHasAtLeastOneOverriddenPCH = true;
										//isScopeWithNoneOverriddenPCH = false;
										prepareReport.addToOverridenPchMap(((Scope)scope).getDbTables(), pch.getName(),pch.getOverridden());
										
										String pchName = pch.getName().trim();
										pchNameList.add(pchName);
										// Checking if the PCH is in ignore pch
										// list
										if (IgnorePCHHolder	.isAvailable(pchName))
											continue;
										// initializing algorithm steps list for
										// this scope
										List<AlgoStep> algoSteps  = new ArrayList<AlgoStep>();
										// creating rating node content pojo

										// CREATING MAP FOR THE PCH NAME AND
										// ALGO STEPS
										if (icmRatingNodeContent.getPchNameAndAlgoSteps().containsKey(pchName)) {
											ISOConsumptionLogger.info("PCH  already present --"	+ pchName);
										}
										
										icmRatingNodeContent.getPchNameAndAlgoSteps().put(pchName,algoSteps);
										
										pchAlgoText = new StringBuffer();
										Map<String, String> processedBlocks = new HashMap<String, String>();

										// PUTTING REQUIRED VALUES IN THE
										// CONTEXT
										context.putValue(ContextParam.ALGO,	pchAlgoText);
										context.putValue(ContextParam.REFERENCE,((Reference) ref).getDbTables());
										context.putValue(ContextParam.REFERENCETYPE,((Reference) ref).getType());
										context.putValue(ContextParam.SCOPE,((Scope) scope).getDbTables());
										context.putValue(ContextParam.PROCESSEDBLOCK,processedBlocks);

										/*
										 * if( !rootConent && (pch.getState()!=
										 * null &&
										 * (pch.getState().contains("CW") &&
										 * pch.getOverridden
										 * ().toLowerCase().equals("false")))){
										 * // ISOConsumptionLogger.info(
										 * "we will not process algorithm state "
										 * + pch.getState() + " for " +
										 * pch.getName());
										 * pch.setExecuted("true"); continue; }
										 */

										// if
										// (!processedBlocks.containsKey(((PCH)
										// block).getName())) {
										if (!(pch.getExecuted() != null && pch.getExecuted().equals("true"))) {

											String xPathReference = "content["+ counter + "]";
											Pointer refPointer = jxpContext.getPointer(xPathReference);
											JXPathContext relativeContext = jxpContext.getRelativeContext(refPointer);
											context.putValue(ContextParam.JXPATHCONTEXT,relativeContext);
											context.putValue(ContextParam.PARENTPROCESS,pch);
											context.putValue(ContextParam.SPACECOUNT,new Integer(0));
											context.putValue(ContextParam.LOOPSTATEMENTEXECUTION,false);
											// algoText =
											// (StringBuffer)context.getValue(ContextParam.ALGO);
											AlgoUtil.processStatementBlock(	block, context);
											// algoText.append((StringBuffer)context.getValue(ContextParam.ALGO));

											pchAlgoText = (StringBuffer) context.getValue(ContextParam.ALGO);
											// Creating node reference object
											// for the current
											// algorithm
											ISOConsumptionLogger.info("Executing pch:["+ pchName+"] Satements:-");
											System.out.println("Executing pch:["+ pchName+ "] Satements:-");
											ISOConsumptionLogger.info(pchAlgoText.toString());
											NodeReference nodeReference = createNodeReferenceForAlgorithm(ref, scope,icmRatingNodeContent,
																				algoSteps, pchAlgoText,completeAlgorithmText);
											// calling life_cyle for this icm
											// rating node
											processAlgorithmTextFile(pch.getName(),icmRatingNodeContent,pchAlgoText, db,nodeReference, dbForLoop);
										}
										
										if("Premium Calculation".equals(referenceType)){
											HashMap<String, String>pchMap = premiumCalculationDataMap.get(((Scope)scope).getDbTables());
											if(pchMap!=null){
												pchMap.put(pchName, "true");
											}
										} else if("Common Rating".equals(referenceType)){
											HashMap<String, String>pchMap = commonRatingDataMap.get(((Scope)scope).getDbTables());
											if(pchMap!=null){
												pchMap.put(pchName, "true");
											}
										}

									} else if (block instanceof String) {
										;
									} else {

										String xPathReference = "content["+ counter + "]";
										Pointer refPointer = jxpContext.getPointer(xPathReference);
										JXPathContext relativeContext = jxpContext.getRelativeContext(refPointer);
										context.putValue(ContextParam.JXPATHCONTEXT,relativeContext);
										context.putValue(ContextParam.SPACECOUNT,new Integer(0));
										AlgoUtil.executeStatement(block,context);

									}
								}
								
								if(didAlgoHasAtLeastOneOverriddenPCH){
									// Adding the rating node content object in the
									// list
									icmRatingNodeContent.setAlgorithmText(completeAlgorithmText);
									
									icmNodeContentList.add(icmRatingNodeContent);
									
									// THIS WILL CREATE EXECUTE MODULE STEPS AND ADD
									// IT TO THE ICMRATINGNODECONTENT
									ICorrectionForPCHModularization pchModularization = new CorrectionForPCHMoularizationImpl();
									pchModularization.createExecuteModelStepsAndAddToIcmNodeContent(referenceType,pchNameList,icmRatingNodeContent,
													(Scope) scope);
									// ISOConsumptionLogger.info("What the....");
									
									//System.out.println("SOMETHING");
									prepareReport.printOverridenPCHMapReports(((Scope) scope).getDbTables());
								}
								
							}
							
						}
						if (referenceType != null)
							CreateICMRequiredData.getInstance().createIcmRequiredDataModel(referenceType,ratingNodeMap, icmNodeContentList);
						
						//System.out.println("ICMNOde conent list size AFTER="+icmNodeContentList.size());
					}
					/*
					 * for(String key :
					 * icmNodeContentList.get(0).getPchNameAndAlgoSteps
					 * ().keySet()){ ISOConsumptionLogger.info(key); }
					 */
					prepareReport.prepareReport();
					
				}
			}
		} catch (Exception _ex) {
			StringBuilder sb = new StringBuilder();
			PCH pch = (PCH) context.getValue(ContextParam.PARENTPROCESS);
			if(pch!=null){
				sb.append("\nWhile processing PCH: [").append(pch.getName())
				.append("]");
				sb.append("\nScope: [")
				.append(context.getValue(ContextParam.SCOPE)).append("]");
				sb.append("\nExchel Sheet: [").append(pch.getExcelSheet())
				.append("] @Excel Row: ").append(pch.getExcelRow());
				sb.append("\n").append(_ex.getMessage());
			}
			throw new RuntimeException(sb.toString(), _ex);
		} finally {
			
			// CLOSING BASEX DATABASES
			this.db.closeDatabase();
			this.dbForLoop.closeDatabase();
			// writeLanguageTextFile(algoText);
		}
		
		//publishing all the step at once
		for(IcmRatingNodeContent node:icmNodeContentList){
			publishAlgorithm(node);
		}
	}

	
	
/*	private final boolean checkIfTheCircualrIsStateWideAndPCHIsOverridden(boolean isStateDideAlgorithm,PCH currentPCH){
		if(isStateDideAlgorithm){
			if(currentPCH.getOverridden())
				return true;
		}
		return false;
	}*/
	
	protected abstract void publishAlgorithm(IcmRatingNodeContent icmRatingNodeContent)throws ParserException;
	/**
	 * @param ref
	 * @param scope
	 * @param icmRatingNodeContent
	 * @param algoSteps
	 * @param algoText
	 * @return creating a node reference model for every scope in the reference
	 */
	//private StringBugger algoRithmText = new Stringb
	private NodeReference createNodeReferenceForAlgorithm(Object ref,
			Object scope, IcmRatingNodeContent icmRatingNodeContent,
			List<AlgoStep> algoSteps, StringBuffer pchAlgoText,StringBuffer completeAlgoText) {
		NodeReference nodeReference = new NodeReference();
		nodeReference.setReference(((Reference) ref).getDbTables());
		nodeReference.setReferenceType(((Reference) ref).getType());
		nodeReference.setScope(((Scope) scope).getDbTables());
		//icmRatingNodeContent.setAlgoStepList(algoSteps);
		completeAlgoText.append(pchAlgoText);
		
		return nodeReference;
	}
	
	private void setICMRatingNodeContentData(IcmRatingNodeContent icmRatingNodeContent,Scope scope,Reference ref){
		String scopeQualPath = XpathUtil.getScopePath(db,(scope.getDbTables()));
		//setting qualified path to scope object s well
		scope.setFullyQualifiedPath(scopeQualPath);
		icmRatingNodeContent.setDbTables(scope.getDbTables());
		icmRatingNodeContent.setAlgorithmLevel(scopeQualPath);
		icmRatingNodeContent.setPass( scope.getPass());
		icmRatingNodeContent.setRefType(ref.getType());
		icmRatingNodeContent.setState(ref.getState());
		int sequenceNo = ( scope.getSequenceNo());
		icmRatingNodeContent.setSequenceNo(sequenceNo);
		Map<String, List<AlgoStep>> pchNameAndAlgoSteps = new HashMap<String, List<AlgoStep>>();
		icmRatingNodeContent.setPchNameAndAlgoSteps(pchNameAndAlgoSteps);
		icmRatingNodeContent.setDbTables(scope.getDbTables());
	}

	protected abstract void processAlgorithmTextFile(
			String pchName,IcmRatingNodeContent icmRatingNodeContent, StringBuffer algo,
			Database dsDatabase, NodeReference nodeReference, Database dbForLoop)
			throws Exception;

	protected abstract void process(String pchName,IcmRatingNodeContent icmRatingNodeContent,
			StringBuffer algo, Database dsDatabase, String scopePath,
			Database dsForLoop) throws Exception;

	// method to write language text file to cross verify the algorithm
	protected void writeLanguageTextFile(StringBuffer buffer) throws Exception {
		BasicConfiguration basicConfiguration = Configurer.getInstance()
				.getBaseConfig();
		if (basicConfiguration.isCreateLangeuageTextFile()) {
			File f = new File(basicConfiguration.getLanguageTextFileDirectory()
					+ "//" + basicConfiguration.getLanguageTextFileName());
			FileOutputStream fileOutputStream = new FileOutputStream(f);
			fileOutputStream.write(buffer.toString().getBytes());
		}
	}

}
