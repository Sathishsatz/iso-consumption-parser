// $ANTLR 3.4 com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g 2013-08-01 12:29:58

  package com.mmpnc.rating.iso.algorithm.parse;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class AlgorithmGrammarLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int COMMENTS=4;
    public static final int DIV=5;
    public static final int EQUALS=6;
    public static final int IDENTS=7;
    public static final int INTEGER=8;
    public static final int MULTI_COMMENT=9;
    public static final int NEGATION=10;
    public static final int NOT=11;
    public static final int NOTEQUALS=12;
    public static final int SPECIALFUNCTION=13;
    public static final int STRING=14;
    public static final int SUBTRACT=15;
    public static final int VARCONSTANT=16;
    public static final int WS=17;
    public static final int XPATHCONSTANT=18;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public AlgorithmGrammarLexer() {} 
    public AlgorithmGrammarLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public AlgorithmGrammarLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g"; }

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:11:7: ( '!=' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:11:9: '!='
            {
            match("!="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:12:7: ( '&&' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:12:9: '&&'
            {
            match("&&"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:13:7: ( '(' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:13:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:14:7: ( ')' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:14:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:15:7: ( '*' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:15:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:16:7: ( '+' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:16:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:17:7: ( ',' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:17:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:18:7: ( '-' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:18:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:19:7: ( '/' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:19:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:20:7: ( ':' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:20:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:21:7: ( '<' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:21:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:22:7: ( '<=' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:22:9: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:23:7: ( '=' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:23:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:24:7: ( '==' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:24:9: '=='
            {
            match("=="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:25:7: ( '>' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:25:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:26:7: ( '>=' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:26:9: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:27:7: ( 'Call' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:27:9: 'Call'
            {
            match("Call"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:28:7: ( 'ELSE' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:28:9: 'ELSE'
            {
            match("ELSE"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:29:7: ( 'END IF' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:29:9: 'END IF'
            {
            match("END IF"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:30:7: ( 'END LOOP' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:30:9: 'END LOOP'
            {
            match("END LOOP"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:31:7: ( 'IF' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:31:9: 'IF'
            {
            match("IF"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:32:7: ( 'Is Equal To' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:32:9: 'Is Equal To'
            {
            match("Is Equal To"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:7: ( 'LOOP THROUGH' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:9: 'LOOP THROUGH'
            {
            match("LOOP THROUGH"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:7: ( 'NOT' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:9: 'NOT'
            {
            match("NOT"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:35:7: ( 'Negative' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:35:9: 'Negative'
            {
            match("Negative"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:36:7: ( 'Not Equal To' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:36:9: 'Not Equal To'
            {
            match("Not Equal To"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:7: ( 'RateTable' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:9: 'RateTable'
            {
            match("RateTable"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:7: ( 'Subtract' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:9: 'Subtract'
            {
            match("Subtract"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:39:7: ( 'THEN' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:39:9: 'THEN'
            {
            match("THEN"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:40:7: ( 'Using' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:40:9: 'Using'
            {
            match("Using"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:7: ( 'mod' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:9: 'mod'
            {
            match("mod"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:7: ( '{' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:43:7: ( '||' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:43:9: '||'
            {
            match("||"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:44:7: ( '}' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:44:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "XPATHCONSTANT"
    public final void mXPATHCONSTANT() throws RecognitionException {
        try {
            int _type = XPATHCONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:120:15: ( 'XPATH' | 'XPATH_STRING' | 'XPATH_NUMERIC' )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 'X':
                {
                switch ( input.LA(2) ) {
                case 'P':
                    {
                    switch ( input.LA(3) ) {
                    case 'A':
                        {
                        switch ( input.LA(4) ) {
                        case 'T':
                            {
                            switch ( input.LA(5) ) {
                            case 'H':
                                {
                                switch ( input.LA(6) ) {
                                case '_':
                                    {
                                    switch ( input.LA(7) ) {
                                    case 'S':
                                        {
                                        alt1=2;
                                        }
                                        break;
                                    case 'N':
                                        {
                                        alt1=3;
                                        }
                                        break;
                                    default:
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 1, 6, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                default:
                                    alt1=1;
                                }

                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 1, 4, input);

                                throw nvae;

                            }

                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 1, 3, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 2, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;

                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:120:17: 'XPATH'
                    {
                    match("XPATH"); 



                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:120:27: 'XPATH_STRING'
                    {
                    match("XPATH_STRING"); 



                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:120:44: 'XPATH_NUMERIC'
                    {
                    match("XPATH_NUMERIC"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "XPATHCONSTANT"

    // $ANTLR start "VARCONSTANT"
    public final void mVARCONSTANT() throws RecognitionException {
        try {
            int _type = VARCONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:13: ( 'COLUMN_NUMERIC' | 'COLUMN_STRING' | 'UI' | 'CONSTANT' | 'LOCAL' | 'LV_NUMERIC' | 'LV_STRING' | 'LV_DOUBLE' | 'LV_INTEGER' | 'LV_BOOLEAN' | 'LV_TIMESPAN' | 'LV_DATE' | 'LV_VALUE' )
            int alt2=13;
            switch ( input.LA(1) ) {
            case 'C':
                {
                switch ( input.LA(2) ) {
                case 'O':
                    {
                    switch ( input.LA(3) ) {
                    case 'L':
                        {
                        switch ( input.LA(4) ) {
                        case 'U':
                            {
                            switch ( input.LA(5) ) {
                            case 'M':
                                {
                                switch ( input.LA(6) ) {
                                case 'N':
                                    {
                                    switch ( input.LA(7) ) {
                                    case '_':
                                        {
                                        switch ( input.LA(8) ) {
                                        case 'N':
                                            {
                                            alt2=1;
                                            }
                                            break;
                                        case 'S':
                                            {
                                            alt2=2;
                                            }
                                            break;
                                        default:
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 2, 22, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    default:
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 2, 21, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                default:
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 2, 18, input);

                                    throw nvae;

                                }

                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 2, 10, input);

                                throw nvae;

                            }

                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 2, 7, input);

                            throw nvae;

                        }

                        }
                        break;
                    case 'N':
                        {
                        alt2=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 4, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;

                }

                }
                break;
            case 'U':
                {
                alt2=3;
                }
                break;
            case 'L':
                {
                switch ( input.LA(2) ) {
                case 'O':
                    {
                    alt2=5;
                    }
                    break;
                case 'V':
                    {
                    switch ( input.LA(3) ) {
                    case '_':
                        {
                        switch ( input.LA(4) ) {
                        case 'N':
                            {
                            alt2=6;
                            }
                            break;
                        case 'S':
                            {
                            alt2=7;
                            }
                            break;
                        case 'D':
                            {
                            switch ( input.LA(5) ) {
                            case 'O':
                                {
                                alt2=8;
                                }
                                break;
                            case 'A':
                                {
                                alt2=12;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 2, 13, input);

                                throw nvae;

                            }

                            }
                            break;
                        case 'I':
                            {
                            alt2=9;
                            }
                            break;
                        case 'B':
                            {
                            alt2=10;
                            }
                            break;
                        case 'T':
                            {
                            alt2=11;
                            }
                            break;
                        case 'V':
                            {
                            alt2=13;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 2, 9, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 6, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 3, input);

                    throw nvae;

                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:15: 'COLUMN_NUMERIC'
                    {
                    match("COLUMN_NUMERIC"); 



                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:34: 'COLUMN_STRING'
                    {
                    match("COLUMN_STRING"); 



                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:52: 'UI'
                    {
                    match("UI"); 



                    }
                    break;
                case 4 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:59: 'CONSTANT'
                    {
                    match("CONSTANT"); 



                    }
                    break;
                case 5 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:72: 'LOCAL'
                    {
                    match("LOCAL"); 



                    }
                    break;
                case 6 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:82: 'LV_NUMERIC'
                    {
                    match("LV_NUMERIC"); 



                    }
                    break;
                case 7 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:97: 'LV_STRING'
                    {
                    match("LV_STRING"); 



                    }
                    break;
                case 8 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:111: 'LV_DOUBLE'
                    {
                    match("LV_DOUBLE"); 



                    }
                    break;
                case 9 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:125: 'LV_INTEGER'
                    {
                    match("LV_INTEGER"); 



                    }
                    break;
                case 10 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:140: 'LV_BOOLEAN'
                    {
                    match("LV_BOOLEAN"); 



                    }
                    break;
                case 11 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:155: 'LV_TIMESPAN'
                    {
                    match("LV_TIMESPAN"); 



                    }
                    break;
                case 12 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:171: 'LV_DATE'
                    {
                    match("LV_DATE"); 



                    }
                    break;
                case 13 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:183: 'LV_VALUE'
                    {
                    match("LV_VALUE"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "VARCONSTANT"

    // $ANTLR start "SPECIALFUNCTION"
    public final void mSPECIALFUNCTION() throws RecognitionException {
        try {
            int _type = SPECIALFUNCTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:17: ( 'RoundUpDollar' | 'RoundUpHundredth' | 'RoundUpHundredThousandth' | 'RoundUpThousandth' | 'RoundDollar' | 'RoundHundredth' | 'RoundHundredThousandth' | 'RoundThousandth' | 'Ceiling' | 'RoundTenThousandth' )
            int alt3=10;
            switch ( input.LA(1) ) {
            case 'R':
                {
                switch ( input.LA(2) ) {
                case 'o':
                    {
                    switch ( input.LA(3) ) {
                    case 'u':
                        {
                        switch ( input.LA(4) ) {
                        case 'n':
                            {
                            switch ( input.LA(5) ) {
                            case 'd':
                                {
                                switch ( input.LA(6) ) {
                                case 'U':
                                    {
                                    switch ( input.LA(7) ) {
                                    case 'p':
                                        {
                                        switch ( input.LA(8) ) {
                                        case 'D':
                                            {
                                            alt3=1;
                                            }
                                            break;
                                        case 'H':
                                            {
                                            switch ( input.LA(9) ) {
                                            case 'u':
                                                {
                                                switch ( input.LA(10) ) {
                                                case 'n':
                                                    {
                                                    switch ( input.LA(11) ) {
                                                    case 'd':
                                                        {
                                                        switch ( input.LA(12) ) {
                                                        case 'r':
                                                            {
                                                            switch ( input.LA(13) ) {
                                                            case 'e':
                                                                {
                                                                switch ( input.LA(14) ) {
                                                                case 'd':
                                                                    {
                                                                    switch ( input.LA(15) ) {
                                                                    case 't':
                                                                        {
                                                                        alt3=2;
                                                                        }
                                                                        break;
                                                                    case 'T':
                                                                        {
                                                                        alt3=3;
                                                                        }
                                                                        break;
                                                                    default:
                                                                        NoViableAltException nvae =
                                                                            new NoViableAltException("", 3, 30, input);

                                                                        throw nvae;

                                                                    }

                                                                    }
                                                                    break;
                                                                default:
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 3, 27, input);

                                                                    throw nvae;

                                                                }

                                                                }
                                                                break;
                                                            default:
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 3, 25, input);

                                                                throw nvae;

                                                            }

                                                            }
                                                            break;
                                                        default:
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 3, 23, input);

                                                            throw nvae;

                                                        }

                                                        }
                                                        break;
                                                    default:
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 3, 21, input);

                                                        throw nvae;

                                                    }

                                                    }
                                                    break;
                                                default:
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 3, 19, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            default:
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 3, 16, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        case 'T':
                                            {
                                            alt3=4;
                                            }
                                            break;
                                        default:
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 3, 11, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    default:
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 3, 7, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 'D':
                                    {
                                    alt3=5;
                                    }
                                    break;
                                case 'H':
                                    {
                                    switch ( input.LA(7) ) {
                                    case 'u':
                                        {
                                        switch ( input.LA(8) ) {
                                        case 'n':
                                            {
                                            switch ( input.LA(9) ) {
                                            case 'd':
                                                {
                                                switch ( input.LA(10) ) {
                                                case 'r':
                                                    {
                                                    switch ( input.LA(11) ) {
                                                    case 'e':
                                                        {
                                                        switch ( input.LA(12) ) {
                                                        case 'd':
                                                            {
                                                            switch ( input.LA(13) ) {
                                                            case 't':
                                                                {
                                                                alt3=6;
                                                                }
                                                                break;
                                                            case 'T':
                                                                {
                                                                alt3=7;
                                                                }
                                                                break;
                                                            default:
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 3, 26, input);

                                                                throw nvae;

                                                            }

                                                            }
                                                            break;
                                                        default:
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 3, 24, input);

                                                            throw nvae;

                                                        }

                                                        }
                                                        break;
                                                    default:
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 3, 22, input);

                                                        throw nvae;

                                                    }

                                                    }
                                                    break;
                                                default:
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 3, 20, input);

                                                    throw nvae;

                                                }

                                                }
                                                break;
                                            default:
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 3, 18, input);

                                                throw nvae;

                                            }

                                            }
                                            break;
                                        default:
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 3, 12, input);

                                            throw nvae;

                                        }

                                        }
                                        break;
                                    default:
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 3, 9, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                case 'T':
                                    {
                                    switch ( input.LA(7) ) {
                                    case 'h':
                                        {
                                        alt3=8;
                                        }
                                        break;
                                    case 'e':
                                        {
                                        alt3=10;
                                        }
                                        break;
                                    default:
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 3, 10, input);

                                        throw nvae;

                                    }

                                    }
                                    break;
                                default:
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 3, 6, input);

                                    throw nvae;

                                }

                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 3, 5, input);

                                throw nvae;

                            }

                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 3, 4, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 3, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;

                }

                }
                break;
            case 'C':
                {
                alt3=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }

            switch (alt3) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:19: 'RoundUpDollar'
                    {
                    match("RoundUpDollar"); 



                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:37: 'RoundUpHundredth'
                    {
                    match("RoundUpHundredth"); 



                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:58: 'RoundUpHundredThousandth'
                    {
                    match("RoundUpHundredThousandth"); 



                    }
                    break;
                case 4 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:87: 'RoundUpThousandth'
                    {
                    match("RoundUpThousandth"); 



                    }
                    break;
                case 5 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:109: 'RoundDollar'
                    {
                    match("RoundDollar"); 



                    }
                    break;
                case 6 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:125: 'RoundHundredth'
                    {
                    match("RoundHundredth"); 



                    }
                    break;
                case 7 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:144: 'RoundHundredThousandth'
                    {
                    match("RoundHundredThousandth"); 



                    }
                    break;
                case 8 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:171: 'RoundThousandth'
                    {
                    match("RoundThousandth"); 



                    }
                    break;
                case 9 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:191: 'Ceiling'
                    {
                    match("Ceiling"); 



                    }
                    break;
                case 10 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:203: 'RoundTenThousandth'
                    {
                    match("RoundTenThousandth"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SPECIALFUNCTION"

    // $ANTLR start "IDENTS"
    public final void mIDENTS() throws RecognitionException {
        try {
            int _type = IDENTS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:123:8: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '/' | '.' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '/' | '.' | '*' )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:123:10: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '/' | '.' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '/' | '.' | '*' )*
            {
            if ( (input.LA(1) >= '.' && input.LA(1) <= '/')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:123:49: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '/' | '.' | '*' )*
            loop4:
            do {
                int alt4=2;
                switch ( input.LA(1) ) {
                case '*':
                case '.':
                case '/':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '_':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt4=1;
                    }
                    break;

                }

                switch (alt4) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:
            	    {
            	    if ( input.LA(1)=='*'||(input.LA(1) >= '.' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENTS"

    // $ANTLR start "INTEGER"
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:124:9: ( ( '0' .. '9' )+ ( '.' ( '0' .. '9' )* )? )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:124:11: ( '0' .. '9' )+ ( '.' ( '0' .. '9' )* )?
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:124:11: ( '0' .. '9' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                switch ( input.LA(1) ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt5=1;
                    }
                    break;

                }

                switch (alt5) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:124:22: ( '.' ( '0' .. '9' )* )?
            int alt7=2;
            switch ( input.LA(1) ) {
                case '.':
                    {
                    alt7=1;
                    }
                    break;
            }

            switch (alt7) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:124:23: '.' ( '0' .. '9' )*
                    {
                    match('.'); 

                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:124:27: ( '0' .. '9' )*
                    loop6:
                    do {
                        int alt6=2;
                        switch ( input.LA(1) ) {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            {
                            alt6=1;
                            }
                            break;

                        }

                        switch (alt6) {
                    	case 1 :
                    	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGER"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:8: ( '\"' ( . )* '\"' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:10: '\"' ( . )* '\"'
            {
            match('\"'); 

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:14: ( . )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='\"') ) {
                    alt8=2;
                }
                else if ( ((LA8_0 >= '\u0000' && LA8_0 <= '!')||(LA8_0 >= '#' && LA8_0 <= '\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:14: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:126:4: ( ( ' ' | '\\n' | '\\r' | '\\t' )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:126:6: ( ' ' | '\\n' | '\\r' | '\\t' )*
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:126:6: ( ' ' | '\\n' | '\\r' | '\\t' )*
            loop9:
            do {
                int alt9=2;
                switch ( input.LA(1) ) {
                case '\t':
                case '\n':
                case '\r':
                case ' ':
                    {
                    alt9=1;
                    }
                    break;

                }

                switch (alt9) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:
            	    {
            	    if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "COMMENTS"
    public final void mCOMMENTS() throws RecognitionException {
        try {
            int _type = COMMENTS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:127:10: ( '//' ( . )* ( '\\n' | '\\r' ) )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:127:12: '//' ( . )* ( '\\n' | '\\r' )
            {
            match("//"); 



            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:127:17: ( . )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0=='\n'||LA10_0=='\r') ) {
                    alt10=2;
                }
                else if ( ((LA10_0 >= '\u0000' && LA10_0 <= '\t')||(LA10_0 >= '\u000B' && LA10_0 <= '\f')||(LA10_0 >= '\u000E' && LA10_0 <= '\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:127:17: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMENTS"

    // $ANTLR start "MULTI_COMMENT"
    public final void mMULTI_COMMENT() throws RecognitionException {
        try {
            int _type = MULTI_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:15: ( '/*' ( . )* '*/' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:17: '/*' ( . )* '*/'
            {
            match("/*"); 



            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:22: ( . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1 >= '\u0000' && LA11_1 <= '.')||(LA11_1 >= '0' && LA11_1 <= '\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0 >= '\u0000' && LA11_0 <= ')')||(LA11_0 >= '+' && LA11_0 <= '\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:22: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            match("*/"); 



            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MULTI_COMMENT"

    public void mTokens() throws RecognitionException {
        // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:8: ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | XPATHCONSTANT | VARCONSTANT | SPECIALFUNCTION | IDENTS | INTEGER | STRING | WS | COMMENTS | MULTI_COMMENT )
        int alt12=43;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:10: T__19
                {
                mT__19(); 


                }
                break;
            case 2 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:16: T__20
                {
                mT__20(); 


                }
                break;
            case 3 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:22: T__21
                {
                mT__21(); 


                }
                break;
            case 4 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:28: T__22
                {
                mT__22(); 


                }
                break;
            case 5 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:34: T__23
                {
                mT__23(); 


                }
                break;
            case 6 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:40: T__24
                {
                mT__24(); 


                }
                break;
            case 7 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:46: T__25
                {
                mT__25(); 


                }
                break;
            case 8 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:52: T__26
                {
                mT__26(); 


                }
                break;
            case 9 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:58: T__27
                {
                mT__27(); 


                }
                break;
            case 10 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:64: T__28
                {
                mT__28(); 


                }
                break;
            case 11 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:70: T__29
                {
                mT__29(); 


                }
                break;
            case 12 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:76: T__30
                {
                mT__30(); 


                }
                break;
            case 13 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:82: T__31
                {
                mT__31(); 


                }
                break;
            case 14 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:88: T__32
                {
                mT__32(); 


                }
                break;
            case 15 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:94: T__33
                {
                mT__33(); 


                }
                break;
            case 16 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:100: T__34
                {
                mT__34(); 


                }
                break;
            case 17 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:106: T__35
                {
                mT__35(); 


                }
                break;
            case 18 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:112: T__36
                {
                mT__36(); 


                }
                break;
            case 19 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:118: T__37
                {
                mT__37(); 


                }
                break;
            case 20 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:124: T__38
                {
                mT__38(); 


                }
                break;
            case 21 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:130: T__39
                {
                mT__39(); 


                }
                break;
            case 22 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:136: T__40
                {
                mT__40(); 


                }
                break;
            case 23 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:142: T__41
                {
                mT__41(); 


                }
                break;
            case 24 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:148: T__42
                {
                mT__42(); 


                }
                break;
            case 25 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:154: T__43
                {
                mT__43(); 


                }
                break;
            case 26 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:160: T__44
                {
                mT__44(); 


                }
                break;
            case 27 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:166: T__45
                {
                mT__45(); 


                }
                break;
            case 28 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:172: T__46
                {
                mT__46(); 


                }
                break;
            case 29 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:178: T__47
                {
                mT__47(); 


                }
                break;
            case 30 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:184: T__48
                {
                mT__48(); 


                }
                break;
            case 31 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:190: T__49
                {
                mT__49(); 


                }
                break;
            case 32 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:196: T__50
                {
                mT__50(); 


                }
                break;
            case 33 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:202: T__51
                {
                mT__51(); 


                }
                break;
            case 34 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:208: T__52
                {
                mT__52(); 


                }
                break;
            case 35 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:214: XPATHCONSTANT
                {
                mXPATHCONSTANT(); 


                }
                break;
            case 36 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:228: VARCONSTANT
                {
                mVARCONSTANT(); 


                }
                break;
            case 37 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:240: SPECIALFUNCTION
                {
                mSPECIALFUNCTION(); 


                }
                break;
            case 38 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:256: IDENTS
                {
                mIDENTS(); 


                }
                break;
            case 39 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:263: INTEGER
                {
                mINTEGER(); 


                }
                break;
            case 40 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:271: STRING
                {
                mSTRING(); 


                }
                break;
            case 41 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:278: WS
                {
                mWS(); 


                }
                break;
            case 42 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:281: COMMENTS
                {
                mCOMMENTS(); 


                }
                break;
            case 43 :
                // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:1:290: MULTI_COMMENT
                {
                mMULTI_COMMENT(); 


                }
                break;

        }

    }


    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA12_eotS =
        "\1\37\10\uffff\1\42\1\uffff\1\44\1\46\1\50\12\34\3\uffff\1\34\4"+
        "\uffff\2\34\7\uffff\5\34\1\110\13\34\1\125\2\34\1\uffff\3\34\1\uffff"+
        "\6\34\2\uffff\3\34\1\150\7\34\1\uffff\1\160\2\34\1\162\3\34\1\166"+
        "\1\uffff\11\34\1\uffff\1\34\1\uffff\3\34\1\u0087\1\34\1\uffff\1"+
        "\34\1\uffff\3\34\4\uffff\1\125\14\34\1\uffff\1\u009c\1\u009e\22"+
        "\34\1\uffff\1\34\1\uffff\2\34\1\u00b7\3\34\1\125\20\34\1\125\1\uffff"+
        "\6\34\1\125\1\u00d3\10\34\1\u00dc\5\34\2\125\3\34\1\uffff\1\u00e5"+
        "\7\34\1\uffff\4\34\3\125\1\34\1\uffff\13\34\1\125\3\34\1\u00b7\15"+
        "\34\1\u009e\2\34\1\125\1\u00b7\6\34\1\u009e\1\125\2\34\1\u00b7\7"+
        "\34\1\u00b7\1\34\1\u00b7\5\34\1\u00b7\4\34\1\u00b7\7\34\1\u00b7"+
        "\1\34\1\u00b7";
    static final String DFA12_eofS =
        "\u0132\uffff";
    static final String DFA12_minS =
        "\1\41\10\uffff\1\52\1\uffff\3\75\1\117\1\114\1\106\2\117\1\141\1"+
        "\165\1\110\1\111\1\157\3\uffff\1\120\4\uffff\2\0\7\uffff\1\154\1"+
        "\114\1\151\1\123\1\104\1\52\1\40\1\103\1\137\1\124\1\147\2\164\1"+
        "\165\1\142\1\105\1\151\1\52\1\144\1\101\1\uffff\3\0\1\uffff\1\154"+
        "\1\125\1\123\1\154\1\105\1\40\2\uffff\1\120\1\101\1\102\1\52\1\141"+
        "\1\40\1\145\1\156\1\164\1\116\1\156\1\uffff\1\52\1\124\1\0\1\52"+
        "\1\115\1\124\1\151\1\52\1\111\1\40\1\114\1\125\1\124\1\101\1\116"+
        "\1\117\1\111\1\101\1\uffff\1\164\1\uffff\1\124\1\144\1\162\1\52"+
        "\1\147\1\uffff\1\110\1\uffff\1\116\1\101\1\156\4\uffff\1\52\1\115"+
        "\1\122\1\125\2\124\1\117\1\115\1\114\1\151\1\141\1\104\1\141\1\uffff"+
        "\2\52\1\137\1\116\1\147\1\105\1\111\1\102\2\105\1\114\1\105\1\125"+
        "\1\166\1\142\1\160\1\157\1\165\1\145\1\143\1\uffff\1\116\1\uffff"+
        "\1\116\1\124\1\52\1\122\1\116\1\114\1\52\1\107\1\105\1\123\1\105"+
        "\1\145\1\154\1\104\1\154\1\156\1\157\1\156\1\164\1\124\2\125\1\124"+
        "\1\52\1\uffff\1\111\1\107\2\105\1\101\1\120\2\52\1\145\1\157\1\165"+
        "\1\150\1\154\1\144\1\165\1\124\1\52\1\122\2\115\1\122\1\103\2\52"+
        "\1\122\1\116\1\101\1\uffff\1\52\1\154\1\156\1\157\1\141\1\162\1"+
        "\163\1\150\1\uffff\1\111\2\105\1\111\3\52\1\116\1\uffff\1\154\1"+
        "\144\1\165\1\162\1\145\1\141\1\157\1\116\2\122\1\116\1\52\1\141"+
        "\1\162\1\163\1\52\1\144\1\156\1\165\1\107\2\111\1\107\1\162\1\145"+
        "\1\141\1\124\1\144\1\163\1\52\2\103\2\52\1\144\1\156\2\150\1\164"+
        "\1\141\2\52\1\124\1\144\1\52\1\157\1\150\1\156\2\150\1\164\1\165"+
        "\1\52\1\144\1\52\1\157\1\150\1\163\1\164\1\165\1\52\1\141\1\150"+
        "\1\163\1\156\1\52\1\141\1\144\1\156\1\164\1\144\1\150\1\164\1\52"+
        "\1\150\1\52";
    static final String DFA12_maxS =
        "\1\175\10\uffff\1\172\1\uffff\3\75\1\145\1\116\1\163\1\126\2\157"+
        "\1\165\1\110\1\163\1\157\3\uffff\1\120\4\uffff\2\uffff\7\uffff\1"+
        "\154\1\116\1\151\1\123\1\104\1\172\1\40\1\117\1\137\1\124\1\147"+
        "\2\164\1\165\1\142\1\105\1\151\1\172\1\144\1\101\1\uffff\3\uffff"+
        "\1\uffff\1\154\1\125\1\123\1\154\1\105\1\40\2\uffff\1\120\1\101"+
        "\1\126\1\172\1\141\1\40\1\145\1\156\1\164\1\116\1\156\1\uffff\1"+
        "\172\1\124\1\uffff\1\172\1\115\1\124\1\151\1\172\1\114\1\40\1\114"+
        "\1\125\1\124\1\117\1\116\1\117\1\111\1\101\1\uffff\1\164\1\uffff"+
        "\1\124\1\144\1\162\1\172\1\147\1\uffff\1\110\1\uffff\1\116\1\101"+
        "\1\156\4\uffff\1\172\1\115\1\122\1\125\2\124\1\117\1\115\1\114\1"+
        "\151\1\141\1\125\1\141\1\uffff\2\172\1\137\1\116\1\147\1\105\1\111"+
        "\1\102\2\105\1\114\1\105\1\125\1\166\1\142\1\160\1\157\1\165\1\150"+
        "\1\143\1\uffff\1\123\1\uffff\1\123\1\124\1\172\1\122\1\116\1\114"+
        "\1\172\1\107\1\105\1\123\1\105\1\145\1\154\1\124\1\154\1\156\1\157"+
        "\1\156\1\164\1\124\2\125\1\124\1\172\1\uffff\1\111\1\107\2\105\1"+
        "\101\1\120\2\172\1\145\1\157\1\165\1\150\1\154\1\144\1\165\1\124"+
        "\1\172\1\122\2\115\1\122\1\103\2\172\1\122\1\116\1\101\1\uffff\1"+
        "\172\1\154\1\156\1\157\1\141\1\162\1\163\1\150\1\uffff\1\111\2\105"+
        "\1\111\3\172\1\116\1\uffff\1\154\1\144\1\165\1\162\1\145\1\141\1"+
        "\157\1\116\2\122\1\116\1\172\1\141\1\162\1\163\1\172\1\144\1\156"+
        "\1\165\1\107\2\111\1\107\1\162\1\145\1\141\1\164\1\144\1\163\1\172"+
        "\2\103\2\172\1\144\1\156\2\150\1\164\1\141\2\172\1\164\1\144\1\172"+
        "\1\157\1\150\1\156\2\150\1\164\1\165\1\172\1\144\1\172\1\157\1\150"+
        "\1\163\1\164\1\165\1\172\1\141\1\150\1\163\1\156\1\172\1\141\1\144"+
        "\1\156\1\164\1\144\1\150\1\164\1\172\1\150\1\172";
    static final String DFA12_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\uffff\1\12\15\uffff"+
        "\1\40\1\41\1\42\1\uffff\1\46\1\47\1\50\1\51\2\uffff\1\11\1\14\1"+
        "\13\1\16\1\15\1\20\1\17\24\uffff\1\52\3\uffff\1\53\6\uffff\1\25"+
        "\1\26\13\uffff\1\44\22\uffff\1\30\1\uffff\1\32\5\uffff\1\37\1\uffff"+
        "\1\21\3\uffff\1\22\1\23\1\24\1\27\15\uffff\1\35\24\uffff\1\36\1"+
        "\uffff\1\43\30\uffff\1\45\33\uffff\1\31\10\uffff\1\34\10\uffff\1"+
        "\33\114\uffff";
    static final String DFA12_specialS =
        "\40\uffff\1\3\1\5\34\uffff\1\1\1\2\1\0\27\uffff\1\4\u00d9\uffff}>";
    static final String[] DFA12_transitionS = {
            "\1\1\1\36\3\uffff\1\2\1\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\34"+
            "\1\11\12\35\1\12\1\uffff\1\13\1\14\1\15\2\uffff\2\34\1\16\1"+
            "\34\1\17\3\34\1\20\2\34\1\21\1\34\1\22\3\34\1\23\1\24\1\25\1"+
            "\26\2\34\1\33\2\34\4\uffff\1\34\1\uffff\14\34\1\27\15\34\1\30"+
            "\1\31\1\32",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\41\3\uffff\1\34\1\40\12\34\7\uffff\32\34\4\uffff\1\34\1"+
            "\uffff\32\34",
            "",
            "\1\43",
            "\1\45",
            "\1\47",
            "\1\52\21\uffff\1\51\3\uffff\1\53",
            "\1\54\1\uffff\1\55",
            "\1\56\54\uffff\1\57",
            "\1\60\6\uffff\1\61",
            "\1\62\25\uffff\1\63\11\uffff\1\64",
            "\1\65\15\uffff\1\66",
            "\1\67",
            "\1\70",
            "\1\72\51\uffff\1\71",
            "\1\73",
            "",
            "",
            "",
            "\1\74",
            "",
            "",
            "",
            "",
            "\52\75\1\76\3\75\14\76\7\75\32\76\4\75\1\76\1\75\32\76\uff85"+
            "\75",
            "\52\101\1\77\3\101\14\100\7\101\32\100\4\101\1\100\1\101\32"+
            "\100\uff85\101",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\102",
            "\1\103\1\uffff\1\104",
            "\1\105",
            "\1\106",
            "\1\107",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\111",
            "\1\113\13\uffff\1\112",
            "\1\114",
            "\1\115",
            "\1\116",
            "\1\117",
            "\1\120",
            "\1\121",
            "\1\122",
            "\1\123",
            "\1\124",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\126",
            "\1\127",
            "",
            "\52\75\1\76\3\75\14\76\7\75\32\76\4\75\1\76\1\75\32\76\uff85"+
            "\75",
            "\52\101\1\77\3\101\1\100\1\130\12\100\7\101\32\100\4\101\1"+
            "\100\1\101\32\100\uff85\101",
            "\52\101\1\77\3\101\14\100\7\101\32\100\4\101\1\100\1\101\32"+
            "\100\uff85\101",
            "",
            "\1\131",
            "\1\132",
            "\1\133",
            "\1\134",
            "\1\135",
            "\1\136",
            "",
            "",
            "\1\137",
            "\1\140",
            "\1\145\1\uffff\1\143\4\uffff\1\144\4\uffff\1\141\4\uffff\1"+
            "\142\1\146\1\uffff\1\147",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\151",
            "\1\152",
            "\1\153",
            "\1\154",
            "\1\155",
            "\1\156",
            "\1\157",
            "",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\161",
            "\52\101\1\77\3\101\14\100\7\101\32\100\4\101\1\100\1\101\32"+
            "\100\uff85\101",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\167\2\uffff\1\170",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174",
            "\1\176\15\uffff\1\175",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "",
            "\1\u0083",
            "",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0088",
            "",
            "\1\u0089",
            "",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "",
            "",
            "",
            "",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u008d",
            "\1\u008e",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096",
            "\1\u0098\3\uffff\1\u0099\13\uffff\1\u009a\1\u0097",
            "\1\u009b",
            "",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\u009d\1\uffff\32"+
            "\34",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\u00b0\2\uffff\1\u00af",
            "\1\u00b1",
            "",
            "\1\u00b3\4\uffff\1\u00b2",
            "",
            "\1\u00b4\4\uffff\1\u00b5",
            "\1\u00b6",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1\3\uffff\1\u00c2\13\uffff\1\u00c3",
            "\1\u00c4",
            "\1\u00c5",
            "\1\u00c6",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\1\u00cb",
            "\1\u00cc",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "",
            "\1\u00cd",
            "\1\u00ce",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00d4",
            "\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\1\u00d8",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00dd",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0",
            "\1\u00e1",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00e2",
            "\1\u00e3",
            "\1\u00e4",
            "",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00e6",
            "\1\u00e7",
            "\1\u00e8",
            "\1\u00e9",
            "\1\u00ea",
            "\1\u00eb",
            "\1\u00ec",
            "",
            "\1\u00ed",
            "\1\u00ee",
            "\1\u00ef",
            "\1\u00f0",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00f1",
            "",
            "\1\u00f2",
            "\1\u00f3",
            "\1\u00f4",
            "\1\u00f5",
            "\1\u00f6",
            "\1\u00f7",
            "\1\u00f8",
            "\1\u00f9",
            "\1\u00fa",
            "\1\u00fb",
            "\1\u00fc",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u00fd",
            "\1\u00fe",
            "\1\u00ff",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0100",
            "\1\u0101",
            "\1\u0102",
            "\1\u0103",
            "\1\u0104",
            "\1\u0105",
            "\1\u0106",
            "\1\u0107",
            "\1\u0108",
            "\1\u0109",
            "\1\u010b\37\uffff\1\u010a",
            "\1\u010c",
            "\1\u010d",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u010e",
            "\1\u010f",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0110",
            "\1\u0111",
            "\1\u0112",
            "\1\u0113",
            "\1\u0114",
            "\1\u0115",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0117\37\uffff\1\u0116",
            "\1\u0118",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0119",
            "\1\u011a",
            "\1\u011b",
            "\1\u011c",
            "\1\u011d",
            "\1\u011e",
            "\1\u011f",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0120",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0121",
            "\1\u0122",
            "\1\u0123",
            "\1\u0124",
            "\1\u0125",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0126",
            "\1\u0127",
            "\1\u0128",
            "\1\u0129",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u012a",
            "\1\u012b",
            "\1\u012c",
            "\1\u012d",
            "\1\u012e",
            "\1\u012f",
            "\1\u0130",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\u0131",
            "\1\34\3\uffff\14\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34"
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | XPATHCONSTANT | VARCONSTANT | SPECIALFUNCTION | IDENTS | INTEGER | STRING | WS | COMMENTS | MULTI_COMMENT );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA12_64 = input.LA(1);

                        s = -1;
                        if ( (LA12_64=='*') ) {s = 63;}

                        else if ( ((LA12_64 >= '.' && LA12_64 <= '9')||(LA12_64 >= 'A' && LA12_64 <= 'Z')||LA12_64=='_'||(LA12_64 >= 'a' && LA12_64 <= 'z')) ) {s = 64;}

                        else if ( ((LA12_64 >= '\u0000' && LA12_64 <= ')')||(LA12_64 >= '+' && LA12_64 <= '-')||(LA12_64 >= ':' && LA12_64 <= '@')||(LA12_64 >= '[' && LA12_64 <= '^')||LA12_64=='`'||(LA12_64 >= '{' && LA12_64 <= '\uFFFF')) ) {s = 65;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA12_62 = input.LA(1);

                        s = -1;
                        if ( ((LA12_62 >= '\u0000' && LA12_62 <= ')')||(LA12_62 >= '+' && LA12_62 <= '-')||(LA12_62 >= ':' && LA12_62 <= '@')||(LA12_62 >= '[' && LA12_62 <= '^')||LA12_62=='`'||(LA12_62 >= '{' && LA12_62 <= '\uFFFF')) ) {s = 61;}

                        else if ( (LA12_62=='*'||(LA12_62 >= '.' && LA12_62 <= '9')||(LA12_62 >= 'A' && LA12_62 <= 'Z')||LA12_62=='_'||(LA12_62 >= 'a' && LA12_62 <= 'z')) ) {s = 62;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA12_63 = input.LA(1);

                        s = -1;
                        if ( (LA12_63=='/') ) {s = 88;}

                        else if ( (LA12_63=='*') ) {s = 63;}

                        else if ( (LA12_63=='.'||(LA12_63 >= '0' && LA12_63 <= '9')||(LA12_63 >= 'A' && LA12_63 <= 'Z')||LA12_63=='_'||(LA12_63 >= 'a' && LA12_63 <= 'z')) ) {s = 64;}

                        else if ( ((LA12_63 >= '\u0000' && LA12_63 <= ')')||(LA12_63 >= '+' && LA12_63 <= '-')||(LA12_63 >= ':' && LA12_63 <= '@')||(LA12_63 >= '[' && LA12_63 <= '^')||LA12_63=='`'||(LA12_63 >= '{' && LA12_63 <= '\uFFFF')) ) {s = 65;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA12_32 = input.LA(1);

                        s = -1;
                        if ( ((LA12_32 >= '\u0000' && LA12_32 <= ')')||(LA12_32 >= '+' && LA12_32 <= '-')||(LA12_32 >= ':' && LA12_32 <= '@')||(LA12_32 >= '[' && LA12_32 <= '^')||LA12_32=='`'||(LA12_32 >= '{' && LA12_32 <= '\uFFFF')) ) {s = 61;}

                        else if ( (LA12_32=='*'||(LA12_32 >= '.' && LA12_32 <= '9')||(LA12_32 >= 'A' && LA12_32 <= 'Z')||LA12_32=='_'||(LA12_32 >= 'a' && LA12_32 <= 'z')) ) {s = 62;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA12_88 = input.LA(1);

                        s = -1;
                        if ( (LA12_88=='*') ) {s = 63;}

                        else if ( ((LA12_88 >= '.' && LA12_88 <= '9')||(LA12_88 >= 'A' && LA12_88 <= 'Z')||LA12_88=='_'||(LA12_88 >= 'a' && LA12_88 <= 'z')) ) {s = 64;}

                        else if ( ((LA12_88 >= '\u0000' && LA12_88 <= ')')||(LA12_88 >= '+' && LA12_88 <= '-')||(LA12_88 >= ':' && LA12_88 <= '@')||(LA12_88 >= '[' && LA12_88 <= '^')||LA12_88=='`'||(LA12_88 >= '{' && LA12_88 <= '\uFFFF')) ) {s = 65;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA12_33 = input.LA(1);

                        s = -1;
                        if ( (LA12_33=='*') ) {s = 63;}

                        else if ( ((LA12_33 >= '.' && LA12_33 <= '9')||(LA12_33 >= 'A' && LA12_33 <= 'Z')||LA12_33=='_'||(LA12_33 >= 'a' && LA12_33 <= 'z')) ) {s = 64;}

                        else if ( ((LA12_33 >= '\u0000' && LA12_33 <= ')')||(LA12_33 >= '+' && LA12_33 <= '-')||(LA12_33 >= ':' && LA12_33 <= '@')||(LA12_33 >= '[' && LA12_33 <= '^')||LA12_33=='`'||(LA12_33 >= '{' && LA12_33 <= '\uFFFF')) ) {s = 65;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 12, _s, input);
            error(nvae);
            throw nvae;
        }

    }
 

}