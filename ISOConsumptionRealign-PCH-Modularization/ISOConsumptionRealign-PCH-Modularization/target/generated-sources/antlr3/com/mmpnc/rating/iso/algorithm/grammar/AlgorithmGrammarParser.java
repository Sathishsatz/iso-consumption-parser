// $ANTLR 3.4 com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g 2013-08-01 12:29:58

  package com.mmpnc.rating.iso.algorithm.parse;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class AlgorithmGrammarParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENTS", "DIV", "EQUALS", "IDENTS", "INTEGER", "MULTI_COMMENT", "NEGATION", "NOT", "NOTEQUALS", "SPECIALFUNCTION", "STRING", "SUBTRACT", "VARCONSTANT", "WS", "XPATHCONSTANT", "'!='", "'&&'", "'('", "')'", "'*'", "'+'", "','", "'-'", "'/'", "':'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'Call'", "'ELSE'", "'END IF'", "'END LOOP'", "'IF'", "'Is Equal To'", "'LOOP THROUGH'", "'NOT'", "'Negative'", "'Not Equal To'", "'RateTable'", "'Subtract'", "'THEN'", "'Using'", "'mod'", "'{'", "'||'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int COMMENTS=4;
    public static final int DIV=5;
    public static final int EQUALS=6;
    public static final int IDENTS=7;
    public static final int INTEGER=8;
    public static final int MULTI_COMMENT=9;
    public static final int NEGATION=10;
    public static final int NOT=11;
    public static final int NOTEQUALS=12;
    public static final int SPECIALFUNCTION=13;
    public static final int STRING=14;
    public static final int SUBTRACT=15;
    public static final int VARCONSTANT=16;
    public static final int WS=17;
    public static final int XPATHCONSTANT=18;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public AlgorithmGrammarParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public AlgorithmGrammarParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return AlgorithmGrammarParser.tokenNames; }
    public String getGrammarFileName() { return "com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g"; }


    public static class algorithm_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "algorithm"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:26:1: algorithm : statements EOF !;
    public final AlgorithmGrammarParser.algorithm_return algorithm() throws RecognitionException {
        AlgorithmGrammarParser.algorithm_return retval = new AlgorithmGrammarParser.algorithm_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        AlgorithmGrammarParser.statements_return statements1 =null;


        CommonTree EOF2_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:26:11: ( statements EOF !)
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:27:5: statements EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_statements_in_algorithm95);
            statements1=statements();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statements1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_algorithm97); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            /*reportError(re);
            recover(input,re);
    		retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);*/
        	throw re;

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "algorithm"


    public static class statements_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statements"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:29:1: statements : statement ( statement )* ;
    public final AlgorithmGrammarParser.statements_return statements() throws RecognitionException {
        AlgorithmGrammarParser.statements_return retval = new AlgorithmGrammarParser.statements_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.statement_return statement3 =null;

        AlgorithmGrammarParser.statement_return statement4 =null;



        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:29:12: ( statement ( statement )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:30:9: statement ( statement )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_statement_in_statements114);
            statement3=statement();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statement3.getTree());

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:30:19: ( statement )*
            loop1:
            do {
                int alt1=2;
                switch ( input.LA(1) ) {
                case IDENTS:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 35:
                case 39:
                case 41:
                    {
                    alt1=1;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:30:19: statement
            	    {
            	    pushFollow(FOLLOW_statement_in_statements116);
            	    statement4=statement();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement4.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statements"


    public static class statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statement"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:1: statement : ( assignment | ifStatement | loopStatement | function | callProgram );
    public final AlgorithmGrammarParser.statement_return statement() throws RecognitionException {
        AlgorithmGrammarParser.statement_return retval = new AlgorithmGrammarParser.statement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.assignment_return assignment5 =null;

        AlgorithmGrammarParser.ifStatement_return ifStatement6 =null;

        AlgorithmGrammarParser.loopStatement_return loopStatement7 =null;

        AlgorithmGrammarParser.function_return function8 =null;

        AlgorithmGrammarParser.callProgram_return callProgram9 =null;



        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:11: ( assignment | ifStatement | loopStatement | function | callProgram )
            int alt2=5;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                switch ( input.LA(2) ) {
                case 21:
                    {
                    alt2=4;
                    }
                    break;
                case 31:
                    {
                    alt2=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;

                }

                }
                break;
            case VARCONSTANT:
            case XPATHCONSTANT:
                {
                alt2=1;
                }
                break;
            case 39:
                {
                alt2=2;
                }
                break;
            case 41:
                {
                alt2=3;
                }
                break;
            case 35:
                {
                alt2=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:9: assignment
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_assignment_in_statement143);
                    assignment5=assignment();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment5.getTree());

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:22: ifStatement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_ifStatement_in_statement147);
                    ifStatement6=ifStatement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, ifStatement6.getTree());

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:36: loopStatement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_loopStatement_in_statement151);
                    loopStatement7=loopStatement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, loopStatement7.getTree());

                    }
                    break;
                case 4 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:52: function
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_function_in_statement155);
                    function8=function();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, function8.getTree());

                    }
                    break;
                case 5 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:34:63: callProgram
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_callProgram_in_statement159);
                    callProgram9=callProgram();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, callProgram9.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statement"


    public static class function_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "function"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:1: function : IDENTS ^ '(' ! ( ( 'Using' !)? ) ( arithmeticExp ( ',' arithmeticExp )* )? ')' !;
    public final AlgorithmGrammarParser.function_return function() throws RecognitionException {
        AlgorithmGrammarParser.function_return retval = new AlgorithmGrammarParser.function_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS10=null;
        Token char_literal11=null;
        Token string_literal12=null;
        Token char_literal14=null;
        Token char_literal16=null;
        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp13 =null;

        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp15 =null;


        CommonTree IDENTS10_tree=null;
        CommonTree char_literal11_tree=null;
        CommonTree string_literal12_tree=null;
        CommonTree char_literal14_tree=null;
        CommonTree char_literal16_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:10: ( IDENTS ^ '(' ! ( ( 'Using' !)? ) ( arithmeticExp ( ',' arithmeticExp )* )? ')' !)
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:9: IDENTS ^ '(' ! ( ( 'Using' !)? ) ( arithmeticExp ( ',' arithmeticExp )* )? ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENTS10=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_function185); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTS10_tree = 
            (CommonTree)adaptor.create(IDENTS10)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(IDENTS10_tree, root_0);
            }

            char_literal11=(Token)match(input,21,FOLLOW_21_in_function188); if (state.failed) return retval;

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:22: ( ( 'Using' !)? )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:23: ( 'Using' !)?
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:30: ( 'Using' !)?
            int alt3=2;
            switch ( input.LA(1) ) {
                case 48:
                    {
                    alt3=1;
                    }
                    break;
            }

            switch (alt3) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:30: 'Using' !
                    {
                    string_literal12=(Token)match(input,48,FOLLOW_48_in_function192); if (state.failed) return retval;

                    }
                    break;

            }


            }


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:34: ( arithmeticExp ( ',' arithmeticExp )* )?
            int alt5=2;
            switch ( input.LA(1) ) {
                case IDENTS:
                case INTEGER:
                case SPECIALFUNCTION:
                case STRING:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 21:
                case 24:
                case 26:
                case 42:
                case 43:
                case 45:
                    {
                    alt5=1;
                    }
                    break;
            }

            switch (alt5) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:35: arithmeticExp ( ',' arithmeticExp )*
                    {
                    pushFollow(FOLLOW_arithmeticExp_in_function198);
                    arithmeticExp13=arithmeticExp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp13.getTree());

                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:49: ( ',' arithmeticExp )*
                    loop4:
                    do {
                        int alt4=2;
                        switch ( input.LA(1) ) {
                        case 25:
                            {
                            alt4=1;
                            }
                            break;

                        }

                        switch (alt4) {
                    	case 1 :
                    	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:38:50: ',' arithmeticExp
                    	    {
                    	    char_literal14=(Token)match(input,25,FOLLOW_25_in_function201); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    char_literal14_tree = 
                    	    (CommonTree)adaptor.create(char_literal14)
                    	    ;
                    	    adaptor.addChild(root_0, char_literal14_tree);
                    	    }

                    	    pushFollow(FOLLOW_arithmeticExp_in_function203);
                    	    arithmeticExp15=arithmeticExp();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp15.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal16=(Token)match(input,22,FOLLOW_22_in_function209); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "function"


    public static class domainTable_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "domainTable"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:1: domainTable : IDENTS ^ '{' ! ( atom ( ',' atom )* )? '}' !;
    public final AlgorithmGrammarParser.domainTable_return domainTable() throws RecognitionException {
        AlgorithmGrammarParser.domainTable_return retval = new AlgorithmGrammarParser.domainTable_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS17=null;
        Token char_literal18=null;
        Token char_literal20=null;
        Token char_literal22=null;
        AlgorithmGrammarParser.atom_return atom19 =null;

        AlgorithmGrammarParser.atom_return atom21 =null;


        CommonTree IDENTS17_tree=null;
        CommonTree char_literal18_tree=null;
        CommonTree char_literal20_tree=null;
        CommonTree char_literal22_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:13: ( IDENTS ^ '{' ! ( atom ( ',' atom )* )? '}' !)
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:9: IDENTS ^ '{' ! ( atom ( ',' atom )* )? '}' !
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENTS17=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_domainTable228); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTS17_tree = 
            (CommonTree)adaptor.create(IDENTS17)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(IDENTS17_tree, root_0);
            }

            char_literal18=(Token)match(input,50,FOLLOW_50_in_domainTable231); if (state.failed) return retval;

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:22: ( atom ( ',' atom )* )?
            int alt7=2;
            switch ( input.LA(1) ) {
                case IDENTS:
                case INTEGER:
                case SPECIALFUNCTION:
                case STRING:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 21:
                case 45:
                    {
                    alt7=1;
                    }
                    break;
            }

            switch (alt7) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:23: atom ( ',' atom )*
                    {
                    pushFollow(FOLLOW_atom_in_domainTable235);
                    atom19=atom();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, atom19.getTree());

                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:28: ( ',' atom )*
                    loop6:
                    do {
                        int alt6=2;
                        switch ( input.LA(1) ) {
                        case 25:
                            {
                            alt6=1;
                            }
                            break;

                        }

                        switch (alt6) {
                    	case 1 :
                    	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:42:29: ',' atom
                    	    {
                    	    char_literal20=(Token)match(input,25,FOLLOW_25_in_domainTable238); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    char_literal20_tree = 
                    	    (CommonTree)adaptor.create(char_literal20)
                    	    ;
                    	    adaptor.addChild(root_0, char_literal20_tree);
                    	    }

                    	    pushFollow(FOLLOW_atom_in_domainTable240);
                    	    atom21=atom();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, atom21.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal22=(Token)match(input,52,FOLLOW_52_in_domainTable246); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "domainTable"


    public static class callProgram_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "callProgram"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:44:1: callProgram : 'Call' ^ IDENTS ;
    public final AlgorithmGrammarParser.callProgram_return callProgram() throws RecognitionException {
        AlgorithmGrammarParser.callProgram_return retval = new AlgorithmGrammarParser.callProgram_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal23=null;
        Token IDENTS24=null;

        CommonTree string_literal23_tree=null;
        CommonTree IDENTS24_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:44:13: ( 'Call' ^ IDENTS )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:45:9: 'Call' ^ IDENTS
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal23=(Token)match(input,35,FOLLOW_35_in_callProgram263); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal23_tree = 
            (CommonTree)adaptor.create(string_literal23)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal23_tree, root_0);
            }

            IDENTS24=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_callProgram266); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTS24_tree = 
            (CommonTree)adaptor.create(IDENTS24)
            ;
            adaptor.addChild(root_0, IDENTS24_tree);
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "callProgram"


    public static class specialFunction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "specialFunction"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:47:1: specialFunction : SPECIALFUNCTION ^ '(' ! arithmeticExp ')' !;
    public final AlgorithmGrammarParser.specialFunction_return specialFunction() throws RecognitionException {
        AlgorithmGrammarParser.specialFunction_return retval = new AlgorithmGrammarParser.specialFunction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SPECIALFUNCTION25=null;
        Token char_literal26=null;
        Token char_literal28=null;
        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp27 =null;


        CommonTree SPECIALFUNCTION25_tree=null;
        CommonTree char_literal26_tree=null;
        CommonTree char_literal28_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:47:17: ( SPECIALFUNCTION ^ '(' ! arithmeticExp ')' !)
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:48:13: SPECIALFUNCTION ^ '(' ! arithmeticExp ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            SPECIALFUNCTION25=(Token)match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_specialFunction287); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            SPECIALFUNCTION25_tree = 
            (CommonTree)adaptor.create(SPECIALFUNCTION25)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(SPECIALFUNCTION25_tree, root_0);
            }

            char_literal26=(Token)match(input,21,FOLLOW_21_in_specialFunction290); if (state.failed) return retval;

            pushFollow(FOLLOW_arithmeticExp_in_specialFunction293);
            arithmeticExp27=arithmeticExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp27.getTree());

            char_literal28=(Token)match(input,22,FOLLOW_22_in_specialFunction295); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "specialFunction"


    public static class rateFunction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "rateFunction"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:50:1: rateFunction : 'RateTable' ^ ':' ! function ;
    public final AlgorithmGrammarParser.rateFunction_return rateFunction() throws RecognitionException {
        AlgorithmGrammarParser.rateFunction_return retval = new AlgorithmGrammarParser.rateFunction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal29=null;
        Token char_literal30=null;
        AlgorithmGrammarParser.function_return function31 =null;


        CommonTree string_literal29_tree=null;
        CommonTree char_literal30_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:50:14: ( 'RateTable' ^ ':' ! function )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:51:13: 'RateTable' ^ ':' ! function
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal29=(Token)match(input,45,FOLLOW_45_in_rateFunction318); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal29_tree = 
            (CommonTree)adaptor.create(string_literal29)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal29_tree, root_0);
            }

            char_literal30=(Token)match(input,28,FOLLOW_28_in_rateFunction321); if (state.failed) return retval;

            pushFollow(FOLLOW_function_in_rateFunction324);
            function31=function();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, function31.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "rateFunction"


    public static class assignment_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "assignment"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:53:1: assignment : ( IDENTS | varType | xpath ) '=' ^ arithmeticExp ;
    public final AlgorithmGrammarParser.assignment_return assignment() throws RecognitionException {
        AlgorithmGrammarParser.assignment_return retval = new AlgorithmGrammarParser.assignment_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS32=null;
        Token char_literal35=null;
        AlgorithmGrammarParser.varType_return varType33 =null;

        AlgorithmGrammarParser.xpath_return xpath34 =null;

        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp36 =null;


        CommonTree IDENTS32_tree=null;
        CommonTree char_literal35_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:53:11: ( ( IDENTS | varType | xpath ) '=' ^ arithmeticExp )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:54:9: ( IDENTS | varType | xpath ) '=' ^ arithmeticExp
            {
            root_0 = (CommonTree)adaptor.nil();


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:54:9: ( IDENTS | varType | xpath )
            int alt8=3;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                alt8=1;
                }
                break;
            case VARCONSTANT:
                {
                alt8=2;
                }
                break;
            case XPATHCONSTANT:
                {
                alt8=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }

            switch (alt8) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:54:10: IDENTS
                    {
                    IDENTS32=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_assignment348); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTS32_tree = 
                    (CommonTree)adaptor.create(IDENTS32)
                    ;
                    adaptor.addChild(root_0, IDENTS32_tree);
                    }

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:54:19: varType
                    {
                    pushFollow(FOLLOW_varType_in_assignment352);
                    varType33=varType();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, varType33.getTree());

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:54:29: xpath
                    {
                    pushFollow(FOLLOW_xpath_in_assignment356);
                    xpath34=xpath();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, xpath34.getTree());

                    }
                    break;

            }


            char_literal35=(Token)match(input,31,FOLLOW_31_in_assignment359); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal35_tree = 
            (CommonTree)adaptor.create(char_literal35)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(char_literal35_tree, root_0);
            }

            pushFollow(FOLLOW_arithmeticExp_in_assignment362);
            arithmeticExp36=arithmeticExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp36.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "assignment"


    public static class ifStatement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ifStatement"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:56:1: ifStatement : 'IF' ^ ( not )? '(' ! booleanExp ')' ! 'THEN' ! statements ( 'ELSE' statements )? 'END IF' !;
    public final AlgorithmGrammarParser.ifStatement_return ifStatement() throws RecognitionException {
        AlgorithmGrammarParser.ifStatement_return retval = new AlgorithmGrammarParser.ifStatement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal37=null;
        Token char_literal39=null;
        Token char_literal41=null;
        Token string_literal42=null;
        Token string_literal44=null;
        Token string_literal46=null;
        AlgorithmGrammarParser.not_return not38 =null;

        AlgorithmGrammarParser.booleanExp_return booleanExp40 =null;

        AlgorithmGrammarParser.statements_return statements43 =null;

        AlgorithmGrammarParser.statements_return statements45 =null;


        CommonTree string_literal37_tree=null;
        CommonTree char_literal39_tree=null;
        CommonTree char_literal41_tree=null;
        CommonTree string_literal42_tree=null;
        CommonTree string_literal44_tree=null;
        CommonTree string_literal46_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:56:12: ( 'IF' ^ ( not )? '(' ! booleanExp ')' ! 'THEN' ! statements ( 'ELSE' statements )? 'END IF' !)
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:9: 'IF' ^ ( not )? '(' ! booleanExp ')' ! 'THEN' ! statements ( 'ELSE' statements )? 'END IF' !
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal37=(Token)match(input,39,FOLLOW_39_in_ifStatement387); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal37_tree = 
            (CommonTree)adaptor.create(string_literal37)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal37_tree, root_0);
            }

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:15: ( not )?
            int alt9=2;
            switch ( input.LA(1) ) {
                case 42:
                    {
                    alt9=1;
                    }
                    break;
            }

            switch (alt9) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:16: not
                    {
                    pushFollow(FOLLOW_not_in_ifStatement391);
                    not38=not();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, not38.getTree());

                    }
                    break;

            }


            char_literal39=(Token)match(input,21,FOLLOW_21_in_ifStatement395); if (state.failed) return retval;

            pushFollow(FOLLOW_booleanExp_in_ifStatement398);
            booleanExp40=booleanExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, booleanExp40.getTree());

            char_literal41=(Token)match(input,22,FOLLOW_22_in_ifStatement400); if (state.failed) return retval;

            string_literal42=(Token)match(input,47,FOLLOW_47_in_ifStatement403); if (state.failed) return retval;

            pushFollow(FOLLOW_statements_in_ifStatement406);
            statements43=statements();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statements43.getTree());

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:58:9: ( 'ELSE' statements )?
            int alt10=2;
            switch ( input.LA(1) ) {
                case 36:
                    {
                    alt10=1;
                    }
                    break;
            }

            switch (alt10) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:58:10: 'ELSE' statements
                    {
                    string_literal44=(Token)match(input,36,FOLLOW_36_in_ifStatement418); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal44_tree = 
                    (CommonTree)adaptor.create(string_literal44)
                    ;
                    adaptor.addChild(root_0, string_literal44_tree);
                    }

                    pushFollow(FOLLOW_statements_in_ifStatement420);
                    statements45=statements();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statements45.getTree());

                    }
                    break;

            }


            string_literal46=(Token)match(input,37,FOLLOW_37_in_ifStatement436); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ifStatement"


    public static class loopStatement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loopStatement"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:62:1: loopStatement : 'LOOP THROUGH' ^ STRING statements 'END LOOP' !;
    public final AlgorithmGrammarParser.loopStatement_return loopStatement() throws RecognitionException {
        AlgorithmGrammarParser.loopStatement_return retval = new AlgorithmGrammarParser.loopStatement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal47=null;
        Token STRING48=null;
        Token string_literal50=null;
        AlgorithmGrammarParser.statements_return statements49 =null;


        CommonTree string_literal47_tree=null;
        CommonTree STRING48_tree=null;
        CommonTree string_literal50_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:62:15: ( 'LOOP THROUGH' ^ STRING statements 'END LOOP' !)
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:63:9: 'LOOP THROUGH' ^ STRING statements 'END LOOP' !
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal47=(Token)match(input,41,FOLLOW_41_in_loopStatement462); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal47_tree = 
            (CommonTree)adaptor.create(string_literal47)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal47_tree, root_0);
            }

            STRING48=(Token)match(input,STRING,FOLLOW_STRING_in_loopStatement465); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            STRING48_tree = 
            (CommonTree)adaptor.create(STRING48)
            ;
            adaptor.addChild(root_0, STRING48_tree);
            }

            pushFollow(FOLLOW_statements_in_loopStatement475);
            statements49=statements();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statements49.getTree());

            string_literal50=(Token)match(input,38,FOLLOW_38_in_loopStatement485); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loopStatement"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:1: atom : ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable ) ;
    public final AlgorithmGrammarParser.atom_return atom() throws RecognitionException {
        AlgorithmGrammarParser.atom_return retval = new AlgorithmGrammarParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS51=null;
        Token INTEGER54=null;
        Token STRING55=null;
        AlgorithmGrammarParser.function_return function52 =null;

        AlgorithmGrammarParser.expression_return expression53 =null;

        AlgorithmGrammarParser.xpath_return xpath56 =null;

        AlgorithmGrammarParser.rateFunction_return rateFunction57 =null;

        AlgorithmGrammarParser.varType_return varType58 =null;

        AlgorithmGrammarParser.domainTable_return domainTable59 =null;


        CommonTree IDENTS51_tree=null;
        CommonTree INTEGER54_tree=null;
        CommonTree STRING55_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:6: ( ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable ) )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:8: ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable )
            {
            root_0 = (CommonTree)adaptor.nil();


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:8: ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable )
            int alt11=9;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                switch ( input.LA(2) ) {
                case 21:
                    {
                    alt11=2;
                    }
                    break;
                case 50:
                    {
                    alt11=9;
                    }
                    break;
                case EOF:
                case IDENTS:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 19:
                case 20:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 29:
                case 30:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 44:
                case 46:
                case 49:
                case 51:
                case 52:
                    {
                    alt11=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae;

                }

                }
                break;
            case SPECIALFUNCTION:
            case 21:
                {
                alt11=3;
                }
                break;
            case INTEGER:
                {
                alt11=4;
                }
                break;
            case STRING:
                {
                alt11=5;
                }
                break;
            case XPATHCONSTANT:
                {
                alt11=6;
                }
                break;
            case 45:
                {
                alt11=7;
                }
                break;
            case VARCONSTANT:
                {
                alt11=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }

            switch (alt11) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:10: IDENTS
                    {
                    IDENTS51=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_atom520); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTS51_tree = 
                    (CommonTree)adaptor.create(IDENTS51)
                    ;
                    adaptor.addChild(root_0, IDENTS51_tree);
                    }

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:19: function
                    {
                    pushFollow(FOLLOW_function_in_atom524);
                    function52=function();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, function52.getTree());

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:30: expression
                    {
                    pushFollow(FOLLOW_expression_in_atom528);
                    expression53=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression53.getTree());

                    }
                    break;
                case 4 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:43: INTEGER
                    {
                    INTEGER54=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_atom532); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    INTEGER54_tree = 
                    (CommonTree)adaptor.create(INTEGER54)
                    ;
                    adaptor.addChild(root_0, INTEGER54_tree);
                    }

                    }
                    break;
                case 5 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:53: STRING
                    {
                    STRING55=(Token)match(input,STRING,FOLLOW_STRING_in_atom536); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    STRING55_tree = 
                    (CommonTree)adaptor.create(STRING55)
                    ;
                    adaptor.addChild(root_0, STRING55_tree);
                    }

                    }
                    break;
                case 6 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:62: xpath
                    {
                    pushFollow(FOLLOW_xpath_in_atom540);
                    xpath56=xpath();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, xpath56.getTree());

                    }
                    break;
                case 7 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:70: rateFunction
                    {
                    pushFollow(FOLLOW_rateFunction_in_atom544);
                    rateFunction57=rateFunction();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, rateFunction57.getTree());

                    }
                    break;
                case 8 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:85: varType
                    {
                    pushFollow(FOLLOW_varType_in_atom548);
                    varType58=varType();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, varType58.getTree());

                    }
                    break;
                case 9 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:68:95: domainTable
                    {
                    pushFollow(FOLLOW_domainTable_in_atom552);
                    domainTable59=domainTable();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, domainTable59.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"


    public static class negation_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "negation"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:70:1: negation : ( not ^)? atom ;
    public final AlgorithmGrammarParser.negation_return negation() throws RecognitionException {
        AlgorithmGrammarParser.negation_return retval = new AlgorithmGrammarParser.negation_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.not_return not60 =null;

        AlgorithmGrammarParser.atom_return atom61 =null;



        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:70:10: ( ( not ^)? atom )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:9: ( not ^)? atom
            {
            root_0 = (CommonTree)adaptor.nil();


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:9: ( not ^)?
            int alt12=2;
            switch ( input.LA(1) ) {
                case 42:
                    {
                    alt12=1;
                    }
                    break;
            }

            switch (alt12) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:10: not ^
                    {
                    pushFollow(FOLLOW_not_in_negation570);
                    not60=not();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(not60.getTree(), root_0);

                    }
                    break;

            }


            pushFollow(FOLLOW_atom_in_negation575);
            atom61=atom();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, atom61.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "negation"


    public static class not_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "not"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:74:1: not : ( 'NOT' ) -> NOT ;
    public final AlgorithmGrammarParser.not_return not() throws RecognitionException {
        AlgorithmGrammarParser.not_return retval = new AlgorithmGrammarParser.not_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal62=null;

        CommonTree string_literal62_tree=null;
        RewriteRuleTokenStream stream_42=new RewriteRuleTokenStream(adaptor,"token 42");

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:74:5: ( ( 'NOT' ) -> NOT )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:75:3: ( 'NOT' )
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:75:3: ( 'NOT' )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:75:4: 'NOT'
            {
            string_literal62=(Token)match(input,42,FOLLOW_42_in_not595); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_42.add(string_literal62);


            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 75:11: -> NOT
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NOT, "NOT")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "not"


    public static class unary_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unary"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:78:1: unary : ( ( '+' !| negative ^) )* negation ;
    public final AlgorithmGrammarParser.unary_return unary() throws RecognitionException {
        AlgorithmGrammarParser.unary_return retval = new AlgorithmGrammarParser.unary_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal63=null;
        AlgorithmGrammarParser.negative_return negative64 =null;

        AlgorithmGrammarParser.negation_return negation65 =null;


        CommonTree char_literal63_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:78:7: ( ( ( '+' !| negative ^) )* negation )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:79:5: ( ( '+' !| negative ^) )* negation
            {
            root_0 = (CommonTree)adaptor.nil();


            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:79:5: ( ( '+' !| negative ^) )*
            loop14:
            do {
                int alt14=2;
                switch ( input.LA(1) ) {
                case 24:
                case 26:
                case 43:
                    {
                    alt14=1;
                    }
                    break;

                }

                switch (alt14) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:79:6: ( '+' !| negative ^)
            	    {
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:79:6: ( '+' !| negative ^)
            	    int alt13=2;
            	    switch ( input.LA(1) ) {
            	    case 24:
            	        {
            	        alt13=1;
            	        }
            	        break;
            	    case 26:
            	    case 43:
            	        {
            	        alt13=2;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 13, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt13) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:79:7: '+' !
            	            {
            	            char_literal63=(Token)match(input,24,FOLLOW_24_in_unary617); if (state.failed) return retval;

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:79:14: negative ^
            	            {
            	            pushFollow(FOLLOW_negative_in_unary622);
            	            negative64=negative();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(negative64.getTree(), root_0);

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            pushFollow(FOLLOW_negation_in_unary628);
            negation65=negation();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, negation65.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unary"


    public static class multi_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multi"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:1: multi : unary ( ( '*' ^| '/' ^| 'mod' ^) unary )* ;
    public final AlgorithmGrammarParser.multi_return multi() throws RecognitionException {
        AlgorithmGrammarParser.multi_return retval = new AlgorithmGrammarParser.multi_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal67=null;
        Token char_literal68=null;
        Token string_literal69=null;
        AlgorithmGrammarParser.unary_return unary66 =null;

        AlgorithmGrammarParser.unary_return unary70 =null;


        CommonTree char_literal67_tree=null;
        CommonTree char_literal68_tree=null;
        CommonTree string_literal69_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:7: ( unary ( ( '*' ^| '/' ^| 'mod' ^) unary )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:5: unary ( ( '*' ^| '/' ^| 'mod' ^) unary )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_unary_in_multi649);
            unary66=unary();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, unary66.getTree());

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:11: ( ( '*' ^| '/' ^| 'mod' ^) unary )*
            loop16:
            do {
                int alt16=2;
                switch ( input.LA(1) ) {
                case 23:
                case 27:
                case 49:
                    {
                    alt16=1;
                    }
                    break;

                }

                switch (alt16) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:12: ( '*' ^| '/' ^| 'mod' ^) unary
            	    {
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:12: ( '*' ^| '/' ^| 'mod' ^)
            	    int alt15=3;
            	    switch ( input.LA(1) ) {
            	    case 23:
            	        {
            	        alt15=1;
            	        }
            	        break;
            	    case 27:
            	        {
            	        alt15=2;
            	        }
            	        break;
            	    case 49:
            	        {
            	        alt15=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt15) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:14: '*' ^
            	            {
            	            char_literal67=(Token)match(input,23,FOLLOW_23_in_multi654); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal67_tree = 
            	            (CommonTree)adaptor.create(char_literal67)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal67_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:21: '/' ^
            	            {
            	            char_literal68=(Token)match(input,27,FOLLOW_27_in_multi659); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal68_tree = 
            	            (CommonTree)adaptor.create(char_literal68)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal68_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 3 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:83:28: 'mod' ^
            	            {
            	            string_literal69=(Token)match(input,49,FOLLOW_49_in_multi664); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal69_tree = 
            	            (CommonTree)adaptor.create(string_literal69)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal69_tree, root_0);
            	            }

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_unary_in_multi669);
            	    unary70=unary();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary70.getTree());

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multi"


    public static class arithmeticExp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmeticExp"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:1: arithmeticExp : multi ( ( '+' ^| subtraction ^) multi )* ;
    public final AlgorithmGrammarParser.arithmeticExp_return arithmeticExp() throws RecognitionException {
        AlgorithmGrammarParser.arithmeticExp_return retval = new AlgorithmGrammarParser.arithmeticExp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal72=null;
        AlgorithmGrammarParser.multi_return multi71 =null;

        AlgorithmGrammarParser.subtraction_return subtraction73 =null;

        AlgorithmGrammarParser.multi_return multi74 =null;


        CommonTree char_literal72_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:15: ( multi ( ( '+' ^| subtraction ^) multi )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:87:5: multi ( ( '+' ^| subtraction ^) multi )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multi_in_arithmeticExp690);
            multi71=multi();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, multi71.getTree());

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:87:11: ( ( '+' ^| subtraction ^) multi )*
            loop18:
            do {
                int alt18=2;
                switch ( input.LA(1) ) {
                case 24:
                case 26:
                case 46:
                    {
                    alt18=1;
                    }
                    break;

                }

                switch (alt18) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:87:12: ( '+' ^| subtraction ^) multi
            	    {
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:87:12: ( '+' ^| subtraction ^)
            	    int alt17=2;
            	    switch ( input.LA(1) ) {
            	    case 24:
            	        {
            	        alt17=1;
            	        }
            	        break;
            	    case 26:
            	    case 46:
            	        {
            	        alt17=2;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 17, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt17) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:87:14: '+' ^
            	            {
            	            char_literal72=(Token)match(input,24,FOLLOW_24_in_arithmeticExp695); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal72_tree = 
            	            (CommonTree)adaptor.create(char_literal72)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal72_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:87:21: subtraction ^
            	            {
            	            pushFollow(FOLLOW_subtraction_in_arithmeticExp700);
            	            subtraction73=subtraction();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(subtraction73.getTree(), root_0);

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_multi_in_arithmeticExp705);
            	    multi74=multi();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, multi74.getTree());

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmeticExp"


    public static class relation_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relation"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:1: relation : arithmeticExp ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )* ;
    public final AlgorithmGrammarParser.relation_return relation() throws RecognitionException {
        AlgorithmGrammarParser.relation_return retval = new AlgorithmGrammarParser.relation_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal78=null;
        Token string_literal79=null;
        Token char_literal80=null;
        Token string_literal81=null;
        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp75 =null;

        AlgorithmGrammarParser.equals_return equals76 =null;

        AlgorithmGrammarParser.notequals_return notequals77 =null;

        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp82 =null;


        CommonTree char_literal78_tree=null;
        CommonTree string_literal79_tree=null;
        CommonTree char_literal80_tree=null;
        CommonTree string_literal81_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:10: ( arithmeticExp ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:5: arithmeticExp ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_arithmeticExp_in_relation725);
            arithmeticExp75=arithmeticExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp75.getTree());

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:19: ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )*
            loop20:
            do {
                int alt20=2;
                switch ( input.LA(1) ) {
                case 19:
                case 29:
                case 30:
                case 32:
                case 33:
                case 34:
                case 40:
                case 44:
                    {
                    alt20=1;
                    }
                    break;

                }

                switch (alt20) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:20: ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp
            	    {
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:20: ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^)
            	    int alt19=6;
            	    switch ( input.LA(1) ) {
            	    case 32:
            	    case 40:
            	        {
            	        alt19=1;
            	        }
            	        break;
            	    case 19:
            	    case 44:
            	        {
            	        alt19=2;
            	        }
            	        break;
            	    case 29:
            	        {
            	        alt19=3;
            	        }
            	        break;
            	    case 30:
            	        {
            	        alt19=4;
            	        }
            	        break;
            	    case 33:
            	        {
            	        alt19=5;
            	        }
            	        break;
            	    case 34:
            	        {
            	        alt19=6;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 19, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt19) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:22: equals ^
            	            {
            	            pushFollow(FOLLOW_equals_in_relation730);
            	            equals76=equals();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(equals76.getTree(), root_0);

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:32: notequals ^
            	            {
            	            pushFollow(FOLLOW_notequals_in_relation735);
            	            notequals77=notequals();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(notequals77.getTree(), root_0);

            	            }
            	            break;
            	        case 3 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:45: '<' ^
            	            {
            	            char_literal78=(Token)match(input,29,FOLLOW_29_in_relation740); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal78_tree = 
            	            (CommonTree)adaptor.create(char_literal78)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal78_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 4 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:52: '<=' ^
            	            {
            	            string_literal79=(Token)match(input,30,FOLLOW_30_in_relation745); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal79_tree = 
            	            (CommonTree)adaptor.create(string_literal79)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal79_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 5 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:60: '>' ^
            	            {
            	            char_literal80=(Token)match(input,33,FOLLOW_33_in_relation750); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal80_tree = 
            	            (CommonTree)adaptor.create(char_literal80)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal80_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 6 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:91:67: '>=' ^
            	            {
            	            string_literal81=(Token)match(input,34,FOLLOW_34_in_relation755); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal81_tree = 
            	            (CommonTree)adaptor.create(string_literal81)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal81_tree, root_0);
            	            }

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_arithmeticExp_in_relation761);
            	    arithmeticExp82=arithmeticExp();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp82.getTree());

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relation"


    public static class equals_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equals"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:1: equals : ( 'Is Equal To' | '==' ) -> EQUALS ;
    public final AlgorithmGrammarParser.equals_return equals() throws RecognitionException {
        AlgorithmGrammarParser.equals_return retval = new AlgorithmGrammarParser.equals_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal83=null;
        Token string_literal84=null;

        CommonTree string_literal83_tree=null;
        CommonTree string_literal84_tree=null;
        RewriteRuleTokenStream stream_32=new RewriteRuleTokenStream(adaptor,"token 32");
        RewriteRuleTokenStream stream_40=new RewriteRuleTokenStream(adaptor,"token 40");

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:7: ( ( 'Is Equal To' | '==' ) -> EQUALS )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:95:5: ( 'Is Equal To' | '==' )
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:95:5: ( 'Is Equal To' | '==' )
            int alt21=2;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt21=1;
                }
                break;
            case 32:
                {
                alt21=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;

            }

            switch (alt21) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:95:6: 'Is Equal To'
                    {
                    string_literal83=(Token)match(input,40,FOLLOW_40_in_equals785); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_40.add(string_literal83);


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:95:22: '=='
                    {
                    string_literal84=(Token)match(input,32,FOLLOW_32_in_equals789); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_32.add(string_literal84);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 95:28: -> EQUALS
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(EQUALS, "EQUALS")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equals"


    public static class notequals_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "notequals"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:98:1: notequals : ( 'Not Equal To' | '!=' ) -> NOTEQUALS ;
    public final AlgorithmGrammarParser.notequals_return notequals() throws RecognitionException {
        AlgorithmGrammarParser.notequals_return retval = new AlgorithmGrammarParser.notequals_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal85=null;
        Token string_literal86=null;

        CommonTree string_literal85_tree=null;
        CommonTree string_literal86_tree=null;
        RewriteRuleTokenStream stream_44=new RewriteRuleTokenStream(adaptor,"token 44");
        RewriteRuleTokenStream stream_19=new RewriteRuleTokenStream(adaptor,"token 19");

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:98:10: ( ( 'Not Equal To' | '!=' ) -> NOTEQUALS )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:99:5: ( 'Not Equal To' | '!=' )
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:99:5: ( 'Not Equal To' | '!=' )
            int alt22=2;
            switch ( input.LA(1) ) {
            case 44:
                {
                alt22=1;
                }
                break;
            case 19:
                {
                alt22=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;

            }

            switch (alt22) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:99:6: 'Not Equal To'
                    {
                    string_literal85=(Token)match(input,44,FOLLOW_44_in_notequals811); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_44.add(string_literal85);


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:99:22: '!='
                    {
                    string_literal86=(Token)match(input,19,FOLLOW_19_in_notequals814); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_19.add(string_literal86);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 99:28: -> NOTEQUALS
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NOTEQUALS, "NOTEQUALS")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "notequals"


    public static class negative_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "negative"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:102:1: negative : ( '-' | 'Negative' ) -> NEGATION ;
    public final AlgorithmGrammarParser.negative_return negative() throws RecognitionException {
        AlgorithmGrammarParser.negative_return retval = new AlgorithmGrammarParser.negative_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal87=null;
        Token string_literal88=null;

        CommonTree char_literal87_tree=null;
        CommonTree string_literal88_tree=null;
        RewriteRuleTokenStream stream_43=new RewriteRuleTokenStream(adaptor,"token 43");
        RewriteRuleTokenStream stream_26=new RewriteRuleTokenStream(adaptor,"token 26");

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:102:10: ( ( '-' | 'Negative' ) -> NEGATION )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:103:5: ( '-' | 'Negative' )
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:103:5: ( '-' | 'Negative' )
            int alt23=2;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt23=1;
                }
                break;
            case 43:
                {
                alt23=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;

            }

            switch (alt23) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:103:6: '-'
                    {
                    char_literal87=(Token)match(input,26,FOLLOW_26_in_negative837); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_26.add(char_literal87);


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:103:10: 'Negative'
                    {
                    string_literal88=(Token)match(input,43,FOLLOW_43_in_negative839); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_43.add(string_literal88);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 103:22: -> NEGATION
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NEGATION, "NEGATION")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "negative"


    public static class subtraction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "subtraction"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:106:1: subtraction : ( '-' | 'Subtract' ) -> SUBTRACT ;
    public final AlgorithmGrammarParser.subtraction_return subtraction() throws RecognitionException {
        AlgorithmGrammarParser.subtraction_return retval = new AlgorithmGrammarParser.subtraction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal89=null;
        Token string_literal90=null;

        CommonTree char_literal89_tree=null;
        CommonTree string_literal90_tree=null;
        RewriteRuleTokenStream stream_46=new RewriteRuleTokenStream(adaptor,"token 46");
        RewriteRuleTokenStream stream_26=new RewriteRuleTokenStream(adaptor,"token 26");

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:106:13: ( ( '-' | 'Subtract' ) -> SUBTRACT )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:107:5: ( '-' | 'Subtract' )
            {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:107:5: ( '-' | 'Subtract' )
            int alt24=2;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt24=1;
                }
                break;
            case 46:
                {
                alt24=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }

            switch (alt24) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:107:6: '-'
                    {
                    char_literal89=(Token)match(input,26,FOLLOW_26_in_subtraction866); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_26.add(char_literal89);


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:107:10: 'Subtract'
                    {
                    string_literal90=(Token)match(input,46,FOLLOW_46_in_subtraction868); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_46.add(string_literal90);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 107:22: -> SUBTRACT
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(SUBTRACT, "SUBTRACT")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "subtraction"


    public static class booleanExp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "booleanExp"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:1: booleanExp : relation ( ( '&&' ^| '||' ^) relation )* ;
    public final AlgorithmGrammarParser.booleanExp_return booleanExp() throws RecognitionException {
        AlgorithmGrammarParser.booleanExp_return retval = new AlgorithmGrammarParser.booleanExp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal92=null;
        Token string_literal93=null;
        AlgorithmGrammarParser.relation_return relation91 =null;

        AlgorithmGrammarParser.relation_return relation94 =null;


        CommonTree string_literal92_tree=null;
        CommonTree string_literal93_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:12: ( relation ( ( '&&' ^| '||' ^) relation )* )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:14: relation ( ( '&&' ^| '||' ^) relation )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_relation_in_booleanExp890);
            relation91=relation();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, relation91.getTree());

            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:23: ( ( '&&' ^| '||' ^) relation )*
            loop26:
            do {
                int alt26=2;
                switch ( input.LA(1) ) {
                case 20:
                case 51:
                    {
                    alt26=1;
                    }
                    break;

                }

                switch (alt26) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:24: ( '&&' ^| '||' ^) relation
            	    {
            	    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:24: ( '&&' ^| '||' ^)
            	    int alt25=2;
            	    switch ( input.LA(1) ) {
            	    case 20:
            	        {
            	        alt25=1;
            	        }
            	        break;
            	    case 51:
            	        {
            	        alt25=2;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 25, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt25) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:26: '&&' ^
            	            {
            	            string_literal92=(Token)match(input,20,FOLLOW_20_in_booleanExp895); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal92_tree = 
            	            (CommonTree)adaptor.create(string_literal92)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal92_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:34: '||' ^
            	            {
            	            string_literal93=(Token)match(input,51,FOLLOW_51_in_booleanExp900); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal93_tree = 
            	            (CommonTree)adaptor.create(string_literal93)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal93_tree, root_0);
            	            }

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_relation_in_booleanExp905);
            	    relation94=relation();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, relation94.getTree());

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "booleanExp"


    public static class expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:112:1: expression : ( ( SPECIALFUNCTION '(' arithmeticExp ')' )=> specialFunction | '(' ! booleanExp ')' !);
    public final AlgorithmGrammarParser.expression_return expression() throws RecognitionException {
        AlgorithmGrammarParser.expression_return retval = new AlgorithmGrammarParser.expression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal96=null;
        Token char_literal98=null;
        AlgorithmGrammarParser.specialFunction_return specialFunction95 =null;

        AlgorithmGrammarParser.booleanExp_return booleanExp97 =null;


        CommonTree char_literal96_tree=null;
        CommonTree char_literal98_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:113:5: ( ( SPECIALFUNCTION '(' arithmeticExp ')' )=> specialFunction | '(' ! booleanExp ')' !)
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==SPECIALFUNCTION) && (synpred1_AlgorithmGrammar())) {
                alt27=1;
            }
            else if ( (LA27_0==21) ) {
                alt27=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;

            }
            switch (alt27) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:113:7: ( SPECIALFUNCTION '(' arithmeticExp ')' )=> specialFunction
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_specialFunction_in_expression932);
                    specialFunction95=specialFunction();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, specialFunction95.getTree());

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:114:7: '(' ! booleanExp ')' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal96=(Token)match(input,21,FOLLOW_21_in_expression940); if (state.failed) return retval;

                    pushFollow(FOLLOW_booleanExp_in_expression943);
                    booleanExp97=booleanExp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, booleanExp97.getTree());

                    char_literal98=(Token)match(input,22,FOLLOW_22_in_expression945); if (state.failed) return retval;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class xpath_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "xpath"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:116:1: xpath : XPATHCONSTANT ':' STRING ;
    public final AlgorithmGrammarParser.xpath_return xpath() throws RecognitionException {
        AlgorithmGrammarParser.xpath_return retval = new AlgorithmGrammarParser.xpath_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token XPATHCONSTANT99=null;
        Token char_literal100=null;
        Token STRING101=null;

        CommonTree XPATHCONSTANT99_tree=null;
        CommonTree char_literal100_tree=null;
        CommonTree STRING101_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:116:7: ( XPATHCONSTANT ':' STRING )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:116:9: XPATHCONSTANT ':' STRING
            {
            root_0 = (CommonTree)adaptor.nil();


            XPATHCONSTANT99=(Token)match(input,XPATHCONSTANT,FOLLOW_XPATHCONSTANT_in_xpath954); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            XPATHCONSTANT99_tree = 
            (CommonTree)adaptor.create(XPATHCONSTANT99)
            ;
            adaptor.addChild(root_0, XPATHCONSTANT99_tree);
            }

            char_literal100=(Token)match(input,28,FOLLOW_28_in_xpath956); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal100_tree = 
            (CommonTree)adaptor.create(char_literal100)
            ;
            adaptor.addChild(root_0, char_literal100_tree);
            }

            STRING101=(Token)match(input,STRING,FOLLOW_STRING_in_xpath958); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            STRING101_tree = 
            (CommonTree)adaptor.create(STRING101)
            ;
            adaptor.addChild(root_0, STRING101_tree);
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "xpath"


    public static class varType_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "varType"
    // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:1: varType : VARCONSTANT ':' ( IDENTS | STRING ) ;
    public final AlgorithmGrammarParser.varType_return varType() throws RecognitionException {
        AlgorithmGrammarParser.varType_return retval = new AlgorithmGrammarParser.varType_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token VARCONSTANT102=null;
        Token char_literal103=null;
        Token set104=null;

        CommonTree VARCONSTANT102_tree=null;
        CommonTree char_literal103_tree=null;
        CommonTree set104_tree=null;

        try {
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:9: ( VARCONSTANT ':' ( IDENTS | STRING ) )
            // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:11: VARCONSTANT ':' ( IDENTS | STRING )
            {
            root_0 = (CommonTree)adaptor.nil();


            VARCONSTANT102=(Token)match(input,VARCONSTANT,FOLLOW_VARCONSTANT_in_varType967); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            VARCONSTANT102_tree = 
            (CommonTree)adaptor.create(VARCONSTANT102)
            ;
            adaptor.addChild(root_0, VARCONSTANT102_tree);
            }

            char_literal103=(Token)match(input,28,FOLLOW_28_in_varType969); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal103_tree = 
            (CommonTree)adaptor.create(char_literal103)
            ;
            adaptor.addChild(root_0, char_literal103_tree);
            }

            set104=(Token)input.LT(1);

            if ( input.LA(1)==IDENTS||input.LA(1)==STRING ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set104)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "varType"

    // $ANTLR start synpred1_AlgorithmGrammar
    public final void synpred1_AlgorithmGrammar_fragment() throws RecognitionException {
        // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:113:7: ( SPECIALFUNCTION '(' arithmeticExp ')' )
        // com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:113:8: SPECIALFUNCTION '(' arithmeticExp ')'
        {
        match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_synpred1_AlgorithmGrammar922); if (state.failed) return ;

        match(input,21,FOLLOW_21_in_synpred1_AlgorithmGrammar924); if (state.failed) return ;

        pushFollow(FOLLOW_arithmeticExp_in_synpred1_AlgorithmGrammar926);
        arithmeticExp();

        state._fsp--;
        if (state.failed) return ;

        match(input,22,FOLLOW_22_in_synpred1_AlgorithmGrammar928); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred1_AlgorithmGrammar

    // Delegated rules

    public final boolean synpred1_AlgorithmGrammar() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_AlgorithmGrammar_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_statements_in_algorithm95 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_algorithm97 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_statements114 = new BitSet(new long[]{0x0000028800050082L});
    public static final BitSet FOLLOW_statement_in_statements116 = new BitSet(new long[]{0x0000028800050082L});
    public static final BitSet FOLLOW_assignment_in_statement143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loopStatement_in_statement151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_statement155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callProgram_in_statement159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_function185 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_function188 = new BitSet(new long[]{0x00012C0005656180L});
    public static final BitSet FOLLOW_48_in_function192 = new BitSet(new long[]{0x00002C0005656180L});
    public static final BitSet FOLLOW_arithmeticExp_in_function198 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_25_in_function201 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_arithmeticExp_in_function203 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_22_in_function209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_domainTable228 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_50_in_domainTable231 = new BitSet(new long[]{0x0010200000256180L});
    public static final BitSet FOLLOW_atom_in_domainTable235 = new BitSet(new long[]{0x0010000002000000L});
    public static final BitSet FOLLOW_25_in_domainTable238 = new BitSet(new long[]{0x0000200000256180L});
    public static final BitSet FOLLOW_atom_in_domainTable240 = new BitSet(new long[]{0x0010000002000000L});
    public static final BitSet FOLLOW_52_in_domainTable246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_callProgram263 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_IDENTS_in_callProgram266 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_specialFunction287 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_specialFunction290 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_arithmeticExp_in_specialFunction293 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_specialFunction295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rateFunction318 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_rateFunction321 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_function_in_rateFunction324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_assignment348 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_varType_in_assignment352 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_xpath_in_assignment356 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_assignment359 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_arithmeticExp_in_assignment362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ifStatement387 = new BitSet(new long[]{0x0000040000200000L});
    public static final BitSet FOLLOW_not_in_ifStatement391 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ifStatement395 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_booleanExp_in_ifStatement398 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ifStatement400 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ifStatement403 = new BitSet(new long[]{0x0000028800050080L});
    public static final BitSet FOLLOW_statements_in_ifStatement406 = new BitSet(new long[]{0x0000003000000000L});
    public static final BitSet FOLLOW_36_in_ifStatement418 = new BitSet(new long[]{0x0000028800050080L});
    public static final BitSet FOLLOW_statements_in_ifStatement420 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ifStatement436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_loopStatement462 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_STRING_in_loopStatement465 = new BitSet(new long[]{0x0000028800050080L});
    public static final BitSet FOLLOW_statements_in_loopStatement475 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_loopStatement485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_atom520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_atom524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_atom528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_atom532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_atom536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_xpath_in_atom540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rateFunction_in_atom544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varType_in_atom548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_domainTable_in_atom552 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_not_in_negation570 = new BitSet(new long[]{0x0000200000256180L});
    public static final BitSet FOLLOW_atom_in_negation575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_not595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_unary617 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_negative_in_unary622 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_negation_in_unary628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_in_multi649 = new BitSet(new long[]{0x0002000008800002L});
    public static final BitSet FOLLOW_23_in_multi654 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_27_in_multi659 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_49_in_multi664 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_unary_in_multi669 = new BitSet(new long[]{0x0002000008800002L});
    public static final BitSet FOLLOW_multi_in_arithmeticExp690 = new BitSet(new long[]{0x0000400005000002L});
    public static final BitSet FOLLOW_24_in_arithmeticExp695 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_subtraction_in_arithmeticExp700 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_multi_in_arithmeticExp705 = new BitSet(new long[]{0x0000400005000002L});
    public static final BitSet FOLLOW_arithmeticExp_in_relation725 = new BitSet(new long[]{0x0000110760080002L});
    public static final BitSet FOLLOW_equals_in_relation730 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_notequals_in_relation735 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_29_in_relation740 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_30_in_relation745 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_33_in_relation750 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_34_in_relation755 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_arithmeticExp_in_relation761 = new BitSet(new long[]{0x0000110760080002L});
    public static final BitSet FOLLOW_40_in_equals785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_equals789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_notequals811 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_notequals814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_negative837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_negative839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_subtraction866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_subtraction868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relation_in_booleanExp890 = new BitSet(new long[]{0x0008000000100002L});
    public static final BitSet FOLLOW_20_in_booleanExp895 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_51_in_booleanExp900 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_relation_in_booleanExp905 = new BitSet(new long[]{0x0008000000100002L});
    public static final BitSet FOLLOW_specialFunction_in_expression932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_expression940 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_booleanExp_in_expression943 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_expression945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_XPATHCONSTANT_in_xpath954 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_xpath956 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_STRING_in_xpath958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VARCONSTANT_in_varType967 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_varType969 = new BitSet(new long[]{0x0000000000004080L});
    public static final BitSet FOLLOW_set_in_varType971 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_synpred1_AlgorithmGrammar922 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_synpred1_AlgorithmGrammar924 = new BitSet(new long[]{0x00002C0005256180L});
    public static final BitSet FOLLOW_arithmeticExp_in_synpred1_AlgorithmGrammar926 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_synpred1_AlgorithmGrammar928 = new BitSet(new long[]{0x0000000000000002L});

}