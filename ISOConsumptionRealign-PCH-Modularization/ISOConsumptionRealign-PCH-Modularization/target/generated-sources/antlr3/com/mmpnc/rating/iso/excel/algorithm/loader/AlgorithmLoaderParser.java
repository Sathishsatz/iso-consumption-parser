// $ANTLR 3.4 com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g 2013-08-01 12:29:59

package com.mmpnc.rating.iso.excel.algorithm.loader;

import com.mmpnc.rating.icm.algorithm.vo.Ratebook;
import com.mmpnc.rating.icm.algorithm.vo.RatingEntity;
import com.mmpnc.rating.icm.algorithm.vo.Reference;
import com.mmpnc.rating.icm.algorithm.vo.Process;
import com.mmpnc.rating.icm.algorithm.vo.Program;
import com.mmpnc.rating.icm.algorithm.vo.Step;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class AlgorithmLoaderParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "CHAR", "COMMENTS", "DIV", "EQUALS", "IDENTS", "INTEGER", "MULTI_COMMENT", "NEGATION", "NOTEQUALS", "SPECIALFUNCTION", "STRING", "SUBTRACT", "VARCONSTANT", "WS", "XPATHCONSTANT", "'('", "')'", "'*'", "'+'", "','", "'-'", "'/'", "':'", "'<'", "'<='", "'='", "'>'", "'>='", "'And'", "'BBB'", "'CCH'", "'Call'", "'ELSE'", "'END IF'", "'END LOOP'", "'IF'", "'Is Equal To'", "'LOOP THROUGH'", "'NOT'", "'Negative'", "'Not Equal To'", "'Or'", "'PCH'", "'RUL'", "'RateTable'", "'SCH'", "'Subtract'", "'TAB'", "'Using'", "'mod'", "'not'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int CHAR=4;
    public static final int COMMENTS=5;
    public static final int DIV=6;
    public static final int EQUALS=7;
    public static final int IDENTS=8;
    public static final int INTEGER=9;
    public static final int MULTI_COMMENT=10;
    public static final int NEGATION=11;
    public static final int NOTEQUALS=12;
    public static final int SPECIALFUNCTION=13;
    public static final int STRING=14;
    public static final int SUBTRACT=15;
    public static final int VARCONSTANT=16;
    public static final int WS=17;
    public static final int XPATHCONSTANT=18;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public AlgorithmLoaderParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public AlgorithmLoaderParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return AlgorithmLoaderParser.tokenNames; }
    public String getGrammarFileName() { return "com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g"; }


        private Process process;
        private Context context;
        private int order;
        
        //update common object list
        private <T> T updateList(List<T> list, T object) {
            for (T t : list) {
                if (t.equals(object)) {
                    return t;
                }
            }
            list.add(object);
            return object;
        }
        
        private <T> boolean isPresent(List<T> list, T object) {
            for (T t : list) {
                if (t.equals(object)) {
                    return true;
                }
            }
            return false;
        }
        
        @SuppressWarnings("unchecked")
        private <T> T updateContext(Context context, String objectName, T obj) {
            if (objectName.equals("Reference")) {
                obj = updateList((List<T>) context.getValue("ReferenceList"), obj);
            } else if (objectName.equals("Entity")) {
                obj = updateList((List<T>) context.getValue("EntityList"), obj);
            } else if (objectName.equals("Program")) {
                obj = updateList((List<T>) context.getValue("ProgramList"), obj);
            } else if (objectName.equals("Process")) {
                obj = updateList((List<T>) context.getValue("ProcessList"), obj);
            } else if (objectName.equals("Step")) {
                obj = updateList((List<T>) context.getValue("StepList"), obj);
            }

            return obj;
        }



    // $ANTLR start "algorithm"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:89:1: algorithm[Ratebook ratebook, Context paramContext] returns [Context retContext] : ( sheet[ratebook] )+ EOF ;
    public final Context algorithm(Ratebook ratebook, Context paramContext) throws RecognitionException {
        Context retContext = null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:89:82: ( ( sheet[ratebook] )+ EOF )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:90:2: ( sheet[ratebook] )+ EOF
            {
            if ( state.backtracking==0 ) {
            	    if(paramContext == null){
            		    this.context = new ContextBase();
            		    this.context.putValue("EntityList", new ArrayList<RatingEntity>());
            	        this.context.putValue("ReferenceList", new ArrayList<Reference>());
            	        this.context.putValue("ProcessList", new ArrayList<Process>());
            	        this.context.putValue("StepList", new ArrayList<Step>());
            	        this.context.putValue("ProgramList", new ArrayList<Program>());
                    }else{
                        this.context = paramContext; 
                    }
             	}

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:102:5: ( sheet[ratebook] )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                switch ( input.LA(1) ) {
                case 33:
                case 34:
                case 47:
                    {
                    alt1=1;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:102:6: sheet[ratebook]
            	    {
            	    pushFollow(FOLLOW_sheet_in_algorithm96);
            	    sheet(ratebook);

            	    state._fsp--;
            	    if (state.failed) return retContext;

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
            	    if (state.backtracking>0) {state.failed=true; return retContext;}
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            match(input,EOF,FOLLOW_EOF_in_algorithm101); if (state.failed) return retContext;

            if ( state.backtracking==0 ) { 
                        retContext = this.context;
                    }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retContext;
    }
    // $ANTLR end "algorithm"



    // $ANTLR start "sheet"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:1: sheet[Ratebook ratebook] : ( rule[ratebook] )? ( processblock[ratebook] ) ;
    public final void sheet(Ratebook ratebook) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:28: ( ( rule[ratebook] )? ( processblock[ratebook] ) )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:30: ( rule[ratebook] )? ( processblock[ratebook] )
            {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:30: ( rule[ratebook] )?
            int alt2=2;
            switch ( input.LA(1) ) {
                case 47:
                    {
                    alt2=1;
                    }
                    break;
            }

            switch (alt2) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:31: rule[ratebook]
                    {
                    pushFollow(FOLLOW_rule_in_sheet124);
                    rule(ratebook);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:48: ( processblock[ratebook] )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:107:49: processblock[ratebook]
            {
            pushFollow(FOLLOW_processblock_in_sheet130);
            processblock(ratebook);

            state._fsp--;
            if (state.failed) return ;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "sheet"



    // $ANTLR start "rule"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:109:1: rule[Ratebook ratebook] : 'RUL' r= IDENTS 'TAB' e= IDENTS ;
    public final void rule(Ratebook ratebook) throws RecognitionException {
        Token r=null;
        Token e=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:109:28: ( 'RUL' r= IDENTS 'TAB' e= IDENTS )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:109:30: 'RUL' r= IDENTS 'TAB' e= IDENTS
            {
            match(input,47,FOLLOW_47_in_rule144); if (state.failed) return ;

            r=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_rule148); if (state.failed) return ;

            match(input,51,FOLLOW_51_in_rule150); if (state.failed) return ;

            e=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_rule154); if (state.failed) return ;

            if ( state.backtracking==0 ) {
                        RatingEntity entity = new RatingEntity();
                        entity.setName((e!=null?e.getText():null));
                        entity = updateContext(context,"Entity",entity);
                        entity.setRatebook(ratebook);
                        
                        updateList(ratebook.getRatingEntity(), entity);
                        
                        Reference reference = new Reference();
                        reference.setName((r!=null?r.getText():null));
                        reference.setEntity(entity);
                        reference = updateContext(context,"Reference",reference);
                        reference.setRatebook(ratebook);
                        ratebook.getReference().add(reference);
                        
                        entity.getReferences().add(reference);            
                    }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "rule"



    // $ANTLR start "processblock"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:129:1: processblock[Ratebook ratebook] : ( 'BBB' | 'CCH' ) (p= STRING )? n= IDENTS ( statement[buffer] )* ( pch[process] )* ;
    public final void processblock(Ratebook ratebook) throws RecognitionException {
        Token p=null;
        Token n=null;


        	        process = new Process();
        	        order = 1;
        	        StringBuffer buffer = new StringBuffer();
        	    
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:135:3: ( ( 'BBB' | 'CCH' ) (p= STRING )? n= IDENTS ( statement[buffer] )* ( pch[process] )* )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:136:3: ( 'BBB' | 'CCH' ) (p= STRING )? n= IDENTS ( statement[buffer] )* ( pch[process] )*
            {
            if ( (input.LA(1) >= 33 && input.LA(1) <= 34) ) {
                input.consume();
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:136:17: (p= STRING )?
            int alt3=2;
            switch ( input.LA(1) ) {
                case STRING:
                    {
                    alt3=1;
                    }
                    break;
            }

            switch (alt3) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:136:18: p= STRING
                    {
                    p=(Token)match(input,STRING,FOLLOW_STRING_in_processblock205); if (state.failed) return ;

                    }
                    break;

            }


            n=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_processblock211); if (state.failed) return ;

            if ( state.backtracking==0 ) {
            		    RatingEntity entity = new RatingEntity();
                        entity.setName((n!=null?n.getText():null));
                        entity = updateContext(context,"Entity",entity);
                        entity.setRatebook(ratebook);
                        
                        updateList(ratebook.getRatingEntity(), entity);
                        
                        process = new Process();
                        process.setName((p!=null?p.getText():null).replaceAll("^\"|\"$",""));
                        process.setEntity(entity);

                        process = updateContext(context,"Process",process);
                        
                        entity.getProcesses().add(process);
            		}

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:153:3: ( statement[buffer] )*
            loop4:
            do {
                int alt4=2;
                switch ( input.LA(1) ) {
                case IDENTS:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 35:
                case 39:
                case 41:
                    {
                    alt4=1;
                    }
                    break;

                }

                switch (alt4) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:154:5: statement[buffer]
            	    {
            	    if ( state.backtracking==0 ) {
            	    		    buffer.delete(0, buffer.length());
            	    		  }

            	    pushFollow(FOLLOW_statement_in_processblock230);
            	    statement(buffer);

            	    state._fsp--;
            	    if (state.failed) return ;

            	    if ( state.backtracking==0 ) {
            	    	            if(buffer != null && buffer.length() > 0 ){
            	    	                  Step step = new Step();
            	                        step.setBlock(buffer.toString());
            	                        step.setExecutionorder(order);
            	                        step.setProcess(process);
            	                        
            	                        step = updateContext(context,"Step",step);
            	                        
            	                        process.getSteps().add(step);
            	                        order++;
            	    	            }    
            	    	        }

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:172:9: ( pch[process] )*
            loop5:
            do {
                int alt5=2;
                switch ( input.LA(1) ) {
                case 46:
                case 49:
                    {
                    alt5=1;
                    }
                    break;

                }

                switch (alt5) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:172:10: pch[process]
            	    {
            	    pushFollow(FOLLOW_pch_in_processblock255);
            	    pch(process);

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "processblock"



    // $ANTLR start "pch"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:174:1: pch[ Process process] : ( 'PCH' | 'SCH' ) (i= IDENTS |s= STRING ) values= statements ;
    public final void pch(Process process) throws RecognitionException {
        Token i=null;
        Token s=null;
        List<String> values =null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:174:24: ( ( 'PCH' | 'SCH' ) (i= IDENTS |s= STRING ) values= statements )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:174:26: ( 'PCH' | 'SCH' ) (i= IDENTS |s= STRING ) values= statements
            {
            if ( input.LA(1)==46||input.LA(1)==49 ) {
                input.consume();
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:174:42: (i= IDENTS |s= STRING )
            int alt6=2;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                alt6=1;
                }
                break;
            case STRING:
                {
                alt6=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:174:43: i= IDENTS
                    {
                    i=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_pch279); if (state.failed) return ;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:174:52: s= STRING
                    {
                    s=(Token)match(input,STRING,FOLLOW_STRING_in_pch283); if (state.failed) return ;

                    }
                    break;

            }


            pushFollow(FOLLOW_statements_in_pch288);
            values=statements();

            state._fsp--;
            if (state.failed) return ;

            if ( state.backtracking==0 ) {
            //            StringBuffer prgBlock = new StringBuffer();
                        int counter = 1;
                        
                        Program program = new Program();
                        if(i != null){
                            program.setName((i!=null?i.getText():null));
                        }else{
                            program.setName((s!=null?s.getText():null).replaceAll("^\"|\"$",""));
                        }
                        program.setProcess(process);
                        program.setExecutionorder(order);
                        
                        for(String str : values)
                        {
                            Step step = new Step();
                            step.setBlock(str);
                            step.setExecutionorder(counter);
                            step.setProgram(program);
                            
                            program.getStep().add(step);
            //              prgBlock.append(str).append("\n");
                            counter++;
                        }
                                   
            //            program.setBlock(prgBlock.toString());
                        
                        program = updateContext(context,"Program",program);
                       
                        if(! isPresent(process.getPrograms(),program))
                        {
            	            process.getPrograms().add(program);
            	            
            	            //add this program as step in process e.g Call Program
            //	             Step step = new Step();
            //	             step.setBlock("Call " + (i!=null?i.getText():null));
            //	             step.setExecutionorder(order);
            //	             step.setProcess(process);
            //	             
            //	             step = updateContext(context,"Step",step);
            //	             
            //	             process.getSteps().add(step);
            	             order++;
            	          } 
                        
                    }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "pch"



    // $ANTLR start "statements"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:223:1: statements returns [List<String> values] : statement[buffer] ( statement[buffer] )* ;
    public final List<String> statements() throws RecognitionException {
        List<String> values = null;



                  StringBuffer buffer = new StringBuffer();
                
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:227:9: ( statement[buffer] ( statement[buffer] )* )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:228:9: statement[buffer] ( statement[buffer] )*
            {
            if ( state.backtracking==0 ) {
                        values = new ArrayList<String>();
                        
                    }

            pushFollow(FOLLOW_statement_in_statements359);
            statement(buffer);

            state._fsp--;
            if (state.failed) return values;

            if ( state.backtracking==0 ) {values.add(buffer.toString());}

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:232:61: ( statement[buffer] )*
            loop7:
            do {
                int alt7=2;
                switch ( input.LA(1) ) {
                case IDENTS:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 35:
                case 39:
                case 41:
                    {
                    alt7=1;
                    }
                    break;

                }

                switch (alt7) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:232:62: statement[buffer]
            	    {
            	    if ( state.backtracking==0 ) {buffer.delete(0, buffer.length());}

            	    pushFollow(FOLLOW_statement_in_statements367);
            	    statement(buffer);

            	    state._fsp--;
            	    if (state.failed) return values;

            	    if ( state.backtracking==0 ) {values.add(buffer.toString());}

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return values;
    }
    // $ANTLR end "statements"



    // $ANTLR start "statement"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:235:1: statement[StringBuffer buffer] : ( assignment[$buffer] | ifStatement[$buffer] | loopStatement[$buffer] | function[$buffer] | callProgram[$buffer] );
    public final void statement(StringBuffer buffer) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:236:9: ( assignment[$buffer] | ifStatement[$buffer] | loopStatement[$buffer] | function[$buffer] | callProgram[$buffer] )
            int alt8=5;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                switch ( input.LA(2) ) {
                case 19:
                    {
                    alt8=4;
                    }
                    break;
                case 29:
                    {
                    alt8=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;

                }

                }
                break;
            case VARCONSTANT:
            case XPATHCONSTANT:
                {
                alt8=1;
                }
                break;
            case 39:
                {
                alt8=2;
                }
                break;
            case 41:
                {
                alt8=3;
                }
                break;
            case 35:
                {
                alt8=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }

            switch (alt8) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:236:11: assignment[$buffer]
                    {
                    pushFollow(FOLLOW_assignment_in_statement398);
                    assignment(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:236:33: ifStatement[$buffer]
                    {
                    pushFollow(FOLLOW_ifStatement_in_statement403);
                    ifStatement(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:236:56: loopStatement[$buffer]
                    {
                    pushFollow(FOLLOW_loopStatement_in_statement408);
                    loopStatement(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 4 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:236:81: function[$buffer]
                    {
                    pushFollow(FOLLOW_function_in_statement413);
                    function(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 5 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:236:101: callProgram[$buffer]
                    {
                    pushFollow(FOLLOW_callProgram_in_statement418);
                    callProgram(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "statement"



    // $ANTLR start "function"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:239:1: function[StringBuffer buffer] : IDENTS '(' ( ( 'Using' )? ) ( arithmeticExp[$buffer] ( ',' arithmeticExp[$buffer] )* )? ')' ;
    public final void function(StringBuffer buffer) throws RecognitionException {
        Token IDENTS1=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:239:31: ( IDENTS '(' ( ( 'Using' )? ) ( arithmeticExp[$buffer] ( ',' arithmeticExp[$buffer] )* )? ')' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:9: IDENTS '(' ( ( 'Using' )? ) ( arithmeticExp[$buffer] ( ',' arithmeticExp[$buffer] )* )? ')'
            {
            IDENTS1=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_function446); if (state.failed) return ;

            match(input,19,FOLLOW_19_in_function448); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append((IDENTS1!=null?IDENTS1.getText():null)).append(" ( ");}

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:66: ( ( 'Using' )? )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:67: ( 'Using' )?
            {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:67: ( 'Using' )?
            int alt9=2;
            switch ( input.LA(1) ) {
                case 52:
                    {
                    alt9=1;
                    }
                    break;
            }

            switch (alt9) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:67: 'Using'
                    {
                    match(input,52,FOLLOW_52_in_function453); if (state.failed) return ;

                    }
                    break;

            }


            }


            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:77: ( arithmeticExp[$buffer] ( ',' arithmeticExp[$buffer] )* )?
            int alt11=2;
            switch ( input.LA(1) ) {
                case CHAR:
                case IDENTS:
                case INTEGER:
                case STRING:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 19:
                case 22:
                case 24:
                case 43:
                case 48:
                case 54:
                    {
                    alt11=1;
                    }
                    break;
            }

            switch (alt11) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:78: arithmeticExp[$buffer] ( ',' arithmeticExp[$buffer] )*
                    {
                    pushFollow(FOLLOW_arithmeticExp_in_function458);
                    arithmeticExp(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:101: ( ',' arithmeticExp[$buffer] )*
                    loop10:
                    do {
                        int alt10=2;
                        switch ( input.LA(1) ) {
                        case 23:
                            {
                            alt10=1;
                            }
                            break;

                        }

                        switch (alt10) {
                    	case 1 :
                    	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:240:102: ',' arithmeticExp[$buffer]
                    	    {
                    	    match(input,23,FOLLOW_23_in_function462); if (state.failed) return ;

                    	    if ( state.backtracking==0 ) {buffer.append(" , ");}

                    	    pushFollow(FOLLOW_arithmeticExp_in_function466);
                    	    arithmeticExp(buffer);

                    	    state._fsp--;
                    	    if (state.failed) return ;

                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    }
                    break;

            }


            match(input,20,FOLLOW_20_in_function473); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" )");}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "function"



    // $ANTLR start "domainTable"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:242:1: domainTable[StringBuffer buffer] : IDENTS '{' ( atom[$buffer] ( ',' atom[$buffer] )* )? '}' ;
    public final void domainTable(StringBuffer buffer) throws RecognitionException {
        Token IDENTS2=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:242:34: ( IDENTS '{' ( atom[$buffer] ( ',' atom[$buffer] )* )? '}' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:243:9: IDENTS '{' ( atom[$buffer] ( ',' atom[$buffer] )* )? '}'
            {
            IDENTS2=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_domainTable493); if (state.failed) return ;

            match(input,55,FOLLOW_55_in_domainTable495); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append((IDENTS2!=null?IDENTS2.getText():null)).append(" {");}

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:243:65: ( atom[$buffer] ( ',' atom[$buffer] )* )?
            int alt13=2;
            switch ( input.LA(1) ) {
                case CHAR:
                case IDENTS:
                case INTEGER:
                case STRING:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 19:
                case 48:
                    {
                    alt13=1;
                    }
                    break;
            }

            switch (alt13) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:243:66: atom[$buffer] ( ',' atom[$buffer] )*
                    {
                    pushFollow(FOLLOW_atom_in_domainTable500);
                    atom(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:243:80: ( ',' atom[$buffer] )*
                    loop12:
                    do {
                        int alt12=2;
                        switch ( input.LA(1) ) {
                        case 23:
                            {
                            alt12=1;
                            }
                            break;

                        }

                        switch (alt12) {
                    	case 1 :
                    	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:243:81: ',' atom[$buffer]
                    	    {
                    	    match(input,23,FOLLOW_23_in_domainTable504); if (state.failed) return ;

                    	    if ( state.backtracking==0 ) {buffer.append(" , ");}

                    	    pushFollow(FOLLOW_atom_in_domainTable508);
                    	    atom(buffer);

                    	    state._fsp--;
                    	    if (state.failed) return ;

                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);


                    }
                    break;

            }


            match(input,56,FOLLOW_56_in_domainTable515); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" }");}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "domainTable"



    // $ANTLR start "callProgram"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:245:1: callProgram[StringBuffer buffer] : 'Call' IDENTS ;
    public final void callProgram(StringBuffer buffer) throws RecognitionException {
        Token IDENTS3=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:245:34: ( 'Call' IDENTS )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:246:9: 'Call' IDENTS
            {
            match(input,35,FOLLOW_35_in_callProgram534); if (state.failed) return ;

            IDENTS3=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_callProgram536); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append("Call ").append((IDENTS3!=null?IDENTS3.getText():null));}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "callProgram"



    // $ANTLR start "specialFunction"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:248:1: specialFunction[StringBuffer buffer] : '(' arithmeticExp[intermediateBuffer] ')' SPECIALFUNCTION ;
    public final void specialFunction(StringBuffer buffer) throws RecognitionException {
        Token SPECIALFUNCTION4=null;


                      StringBuffer intermediateBuffer = new StringBuffer();
                    
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:252:13: ( '(' arithmeticExp[intermediateBuffer] ')' SPECIALFUNCTION )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:252:14: '(' arithmeticExp[intermediateBuffer] ')' SPECIALFUNCTION
            {
            match(input,19,FOLLOW_19_in_specialFunction575); if (state.failed) return ;

            pushFollow(FOLLOW_arithmeticExp_in_specialFunction577);
            arithmeticExp(intermediateBuffer);

            state._fsp--;
            if (state.failed) return ;

            match(input,20,FOLLOW_20_in_specialFunction580); if (state.failed) return ;

            SPECIALFUNCTION4=(Token)match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_specialFunction582); if (state.failed) return ;

            if ( state.backtracking==0 ) {
                            buffer.append((SPECIALFUNCTION4!=null?SPECIALFUNCTION4.getText():null)).append("( ").append(intermediateBuffer).append(" )");
                        }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "specialFunction"



    // $ANTLR start "rateFunction"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:258:1: rateFunction[StringBuffer buffer] : 'RateTable' ':' function[$buffer] ;
    public final void rateFunction(StringBuffer buffer) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:258:35: ( 'RateTable' ':' function[$buffer] )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:259:13: 'RateTable' ':' function[$buffer]
            {
            match(input,48,FOLLOW_48_in_rateFunction635); if (state.failed) return ;

            match(input,26,FOLLOW_26_in_rateFunction637); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append("RateTable:");}

            pushFollow(FOLLOW_function_in_rateFunction641);
            function(buffer);

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "rateFunction"



    // $ANTLR start "assignment"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:261:1: assignment[StringBuffer buffer] : ( IDENTS | varType[$buffer] | xpath[$buffer] ) '=' arithmeticExp[$buffer] ;
    public final void assignment(StringBuffer buffer) throws RecognitionException {
        Token IDENTS5=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:261:33: ( ( IDENTS | varType[$buffer] | xpath[$buffer] ) '=' arithmeticExp[$buffer] )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:262:9: ( IDENTS | varType[$buffer] | xpath[$buffer] ) '=' arithmeticExp[$buffer]
            {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:262:9: ( IDENTS | varType[$buffer] | xpath[$buffer] )
            int alt14=3;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                alt14=1;
                }
                break;
            case VARCONSTANT:
                {
                alt14=2;
                }
                break;
            case XPATHCONSTANT:
                {
                alt14=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }

            switch (alt14) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:262:10: IDENTS
                    {
                    IDENTS5=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_assignment669); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append((IDENTS5!=null?IDENTS5.getText():null));}

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:262:51: varType[$buffer]
                    {
                    pushFollow(FOLLOW_varType_in_assignment675);
                    varType(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:262:70: xpath[$buffer]
                    {
                    pushFollow(FOLLOW_xpath_in_assignment680);
                    xpath(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            match(input,29,FOLLOW_29_in_assignment685); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" = ");}

            pushFollow(FOLLOW_arithmeticExp_in_assignment690);
            arithmeticExp(buffer);

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "assignment"



    // $ANTLR start "ifStatement"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:264:1: ifStatement[StringBuffer buffer] : 'IF' ( 'NOT' )? '(' booleanExp[$buffer] ')' st1= statements ( 'ELSE' st2= statements )? 'END IF' ;
    public final void ifStatement(StringBuffer buffer) throws RecognitionException {
        List<String> st1 =null;

        List<String> st2 =null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:264:34: ( 'IF' ( 'NOT' )? '(' booleanExp[$buffer] ')' st1= statements ( 'ELSE' st2= statements )? 'END IF' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:265:9: 'IF' ( 'NOT' )? '(' booleanExp[$buffer] ')' st1= statements ( 'ELSE' st2= statements )? 'END IF'
            {
            match(input,39,FOLLOW_39_in_ifStatement717); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" IF ");}

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:265:40: ( 'NOT' )?
            int alt15=2;
            switch ( input.LA(1) ) {
                case 42:
                    {
                    alt15=1;
                    }
                    break;
            }

            switch (alt15) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:265:41: 'NOT'
                    {
                    match(input,42,FOLLOW_42_in_ifStatement722); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append(" NOT ");}

                    }
                    break;

            }


            match(input,19,FOLLOW_19_in_ifStatement728); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" ( ");}

            pushFollow(FOLLOW_booleanExp_in_ifStatement732);
            booleanExp(buffer);

            state._fsp--;
            if (state.failed) return ;

            match(input,20,FOLLOW_20_in_ifStatement735); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" ) ");}

            pushFollow(FOLLOW_statements_in_ifStatement741);
            st1=statements();

            state._fsp--;
            if (state.failed) return ;

            if ( state.backtracking==0 ) { for(String str : st1) { buffer.append(str).append(" ");} }

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:266:9: ( 'ELSE' st2= statements )?
            int alt16=2;
            switch ( input.LA(1) ) {
                case 36:
                    {
                    alt16=1;
                    }
                    break;
            }

            switch (alt16) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:266:10: 'ELSE' st2= statements
                    {
                    match(input,36,FOLLOW_36_in_ifStatement754); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append(" ELSE ");}

                    pushFollow(FOLLOW_statements_in_ifStatement760);
                    st2=statements();

                    state._fsp--;
                    if (state.failed) return ;

                    if ( state.backtracking==0 ) { for(String str : st2) { buffer.append(str).append(" ");} }

                    }
                    break;

            }


            match(input,37,FOLLOW_37_in_ifStatement778); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" END IF ");}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "ifStatement"



    // $ANTLR start "loopStatement"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:270:1: loopStatement[StringBuffer buffer] : 'LOOP THROUGH' s1= STRING st1= statements 'END LOOP' s2= STRING ;
    public final void loopStatement(StringBuffer buffer) throws RecognitionException {
        Token s1=null;
        Token s2=null;
        List<String> st1 =null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:270:36: ( 'LOOP THROUGH' s1= STRING st1= statements 'END LOOP' s2= STRING )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:271:9: 'LOOP THROUGH' s1= STRING st1= statements 'END LOOP' s2= STRING
            {
            match(input,41,FOLLOW_41_in_loopStatement806); if (state.failed) return ;

            s1=(Token)match(input,STRING,FOLLOW_STRING_in_loopStatement810); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" LOOP THROUGH ").append((s1!=null?s1.getText():null));}

            pushFollow(FOLLOW_statements_in_loopStatement825);
            st1=statements();

            state._fsp--;
            if (state.failed) return ;

            if ( state.backtracking==0 ) { for(String str : st1) { buffer.append(str).append(" ");} }

            match(input,38,FOLLOW_38_in_loopStatement837); if (state.failed) return ;

            s2=(Token)match(input,STRING,FOLLOW_STRING_in_loopStatement841); if (state.failed) return ;

            if ( state.backtracking==0 ) {buffer.append(" END LOOP ").append((s2!=null?s2.getText():null));}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "loopStatement"



    // $ANTLR start "atom"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:276:1: atom[StringBuffer buffer] : ( IDENTS | function[$buffer] | expression[$buffer] | INTEGER | STRING | xpath[$buffer] | rateFunction[$buffer] | varType[$buffer] | domainTable[$buffer] | CHAR ) ;
    public final void atom(StringBuffer buffer) throws RecognitionException {
        Token IDENTS6=null;
        Token INTEGER7=null;
        Token STRING8=null;
        Token CHAR9=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:3: ( ( IDENTS | function[$buffer] | expression[$buffer] | INTEGER | STRING | xpath[$buffer] | rateFunction[$buffer] | varType[$buffer] | domainTable[$buffer] | CHAR ) )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:5: ( IDENTS | function[$buffer] | expression[$buffer] | INTEGER | STRING | xpath[$buffer] | rateFunction[$buffer] | varType[$buffer] | domainTable[$buffer] | CHAR )
            {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:5: ( IDENTS | function[$buffer] | expression[$buffer] | INTEGER | STRING | xpath[$buffer] | rateFunction[$buffer] | varType[$buffer] | domainTable[$buffer] | CHAR )
            int alt17=10;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                switch ( input.LA(2) ) {
                case 19:
                    {
                    alt17=2;
                    }
                    break;
                case 55:
                    {
                    alt17=9;
                    }
                    break;
                case EOF:
                case IDENTS:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 27:
                case 28:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 44:
                case 45:
                case 46:
                case 47:
                case 49:
                case 50:
                case 53:
                case 56:
                    {
                    alt17=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 17, 1, input);

                    throw nvae;

                }

                }
                break;
            case 19:
                {
                alt17=3;
                }
                break;
            case INTEGER:
                {
                alt17=4;
                }
                break;
            case STRING:
                {
                alt17=5;
                }
                break;
            case XPATHCONSTANT:
                {
                alt17=6;
                }
                break;
            case 48:
                {
                alt17=7;
                }
                break;
            case VARCONSTANT:
                {
                alt17=8;
                }
                break;
            case CHAR:
                {
                alt17=10;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;

            }

            switch (alt17) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:7: IDENTS
                    {
                    IDENTS6=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_atom881); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append((IDENTS6!=null?IDENTS6.getText():null));}

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:48: function[$buffer]
                    {
                    pushFollow(FOLLOW_function_in_atom887);
                    function(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:68: expression[$buffer]
                    {
                    pushFollow(FOLLOW_expression_in_atom892);
                    expression(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 4 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:90: INTEGER
                    {
                    INTEGER7=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_atom897); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append((INTEGER7!=null?INTEGER7.getText():null));}

                    }
                    break;
                case 5 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:133: STRING
                    {
                    STRING8=(Token)match(input,STRING,FOLLOW_STRING_in_atom903); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append((STRING8!=null?STRING8.getText():null));}

                    }
                    break;
                case 6 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:174: xpath[$buffer]
                    {
                    pushFollow(FOLLOW_xpath_in_atom909);
                    xpath(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 7 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:191: rateFunction[$buffer]
                    {
                    pushFollow(FOLLOW_rateFunction_in_atom914);
                    rateFunction(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 8 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:215: varType[$buffer]
                    {
                    pushFollow(FOLLOW_varType_in_atom919);
                    varType(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 9 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:234: domainTable[$buffer]
                    {
                    pushFollow(FOLLOW_domainTable_in_atom924);
                    domainTable(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 10 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:277:257: CHAR
                    {
                    CHAR9=(Token)match(input,CHAR,FOLLOW_CHAR_in_atom929); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append((CHAR9!=null?CHAR9.getText():null));}

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "atom"



    // $ANTLR start "negation"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:279:1: negation[StringBuffer buffer] : ( 'not' )? atom[$buffer] ;
    public final void negation(StringBuffer buffer) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:279:31: ( ( 'not' )? atom[$buffer] )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:280:9: ( 'not' )? atom[$buffer]
            {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:280:9: ( 'not' )?
            int alt18=2;
            switch ( input.LA(1) ) {
                case 54:
                    {
                    alt18=1;
                    }
                    break;
            }

            switch (alt18) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:280:10: 'not'
                    {
                    match(input,54,FOLLOW_54_in_negation951); if (state.failed) return ;

                    if ( state.backtracking==0 ) {buffer.append(" not ");}

                    }
                    break;

            }


            pushFollow(FOLLOW_atom_in_negation957);
            atom(buffer);

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "negation"



    // $ANTLR start "unary"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:283:1: unary[StringBuffer buffer] : ( ( '+' | negative ) )* negation[$buffer] ;
    public final void unary(StringBuffer buffer) throws RecognitionException {
        AlgorithmLoaderParser.negative_return negative10 =null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:283:28: ( ( ( '+' | negative ) )* negation[$buffer] )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:284:5: ( ( '+' | negative ) )* negation[$buffer]
            {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:284:5: ( ( '+' | negative ) )*
            loop20:
            do {
                int alt20=2;
                switch ( input.LA(1) ) {
                case 22:
                case 24:
                case 43:
                    {
                    alt20=1;
                    }
                    break;

                }

                switch (alt20) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:284:6: ( '+' | negative )
            	    {
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:284:6: ( '+' | negative )
            	    int alt19=2;
            	    switch ( input.LA(1) ) {
            	    case 22:
            	        {
            	        alt19=1;
            	        }
            	        break;
            	    case 24:
            	    case 43:
            	        {
            	        alt19=2;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return ;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 19, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt19) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:284:7: '+'
            	            {
            	            match(input,22,FOLLOW_22_in_unary982); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" + ");}

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:284:38: negative
            	            {
            	            pushFollow(FOLLOW_negative_in_unary988);
            	            negative10=negative();

            	            state._fsp--;
            	            if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append((negative10!=null?input.toString(negative10.start,negative10.stop):null));}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            pushFollow(FOLLOW_negation_in_unary995);
            negation(buffer);

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "unary"



    // $ANTLR start "multi"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:287:1: multi[StringBuffer buffer] : unary[$buffer] ( ( '*' | '/' | 'mod' ) unary[$buffer] )* ;
    public final void multi(StringBuffer buffer) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:287:29: ( unary[$buffer] ( ( '*' | '/' | 'mod' ) unary[$buffer] )* )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:5: unary[$buffer] ( ( '*' | '/' | 'mod' ) unary[$buffer] )*
            {
            pushFollow(FOLLOW_unary_in_multi1019);
            unary(buffer);

            state._fsp--;
            if (state.failed) return ;

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:20: ( ( '*' | '/' | 'mod' ) unary[$buffer] )*
            loop22:
            do {
                int alt22=2;
                switch ( input.LA(1) ) {
                case 21:
                case 25:
                case 53:
                    {
                    alt22=1;
                    }
                    break;

                }

                switch (alt22) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:21: ( '*' | '/' | 'mod' ) unary[$buffer]
            	    {
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:21: ( '*' | '/' | 'mod' )
            	    int alt21=3;
            	    switch ( input.LA(1) ) {
            	    case 21:
            	        {
            	        alt21=1;
            	        }
            	        break;
            	    case 25:
            	        {
            	        alt21=2;
            	        }
            	        break;
            	    case 53:
            	        {
            	        alt21=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return ;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 21, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt21) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:23: '*'
            	            {
            	            match(input,21,FOLLOW_21_in_multi1025); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" * ");}

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:54: '/'
            	            {
            	            match(input,25,FOLLOW_25_in_multi1031); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" / ");}

            	            }
            	            break;
            	        case 3 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:288:85: 'mod'
            	            {
            	            match(input,53,FOLLOW_53_in_multi1037); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" mod ");}

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_unary_in_multi1043);
            	    unary(buffer);

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "multi"



    // $ANTLR start "arithmeticExp"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:291:1: arithmeticExp[StringBuffer buffer] : multi[$buffer] ( ( '+' | subtraction ) multi[$buffer] )* ;
    public final void arithmeticExp(StringBuffer buffer) throws RecognitionException {
        AlgorithmLoaderParser.subtraction_return subtraction11 =null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:291:36: ( multi[$buffer] ( ( '+' | subtraction ) multi[$buffer] )* )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:292:5: multi[$buffer] ( ( '+' | subtraction ) multi[$buffer] )*
            {
            pushFollow(FOLLOW_multi_in_arithmeticExp1066);
            multi(buffer);

            state._fsp--;
            if (state.failed) return ;

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:292:20: ( ( '+' | subtraction ) multi[$buffer] )*
            loop24:
            do {
                int alt24=2;
                switch ( input.LA(1) ) {
                case 22:
                case 24:
                case 50:
                    {
                    alt24=1;
                    }
                    break;

                }

                switch (alt24) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:292:21: ( '+' | subtraction ) multi[$buffer]
            	    {
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:292:21: ( '+' | subtraction )
            	    int alt23=2;
            	    switch ( input.LA(1) ) {
            	    case 22:
            	        {
            	        alt23=1;
            	        }
            	        break;
            	    case 24:
            	    case 50:
            	        {
            	        alt23=2;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return ;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 23, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt23) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:292:23: '+'
            	            {
            	            match(input,22,FOLLOW_22_in_arithmeticExp1072); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" + ");}

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:292:54: subtraction
            	            {
            	            pushFollow(FOLLOW_subtraction_in_arithmeticExp1078);
            	            subtraction11=subtraction();

            	            state._fsp--;
            	            if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append((subtraction11!=null?input.toString(subtraction11.start,subtraction11.stop):null));}

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_multi_in_arithmeticExp1084);
            	    multi(buffer);

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "arithmeticExp"



    // $ANTLR start "relation"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:295:1: relation[StringBuffer buffer] : arithmeticExp[$buffer] ( ( equals | notequals | '<' | '<=' | '>' | '>=' ) arithmeticExp[$buffer] )* ;
    public final void relation(StringBuffer buffer) throws RecognitionException {
        AlgorithmLoaderParser.equals_return equals12 =null;

        AlgorithmLoaderParser.notequals_return notequals13 =null;


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:295:31: ( arithmeticExp[$buffer] ( ( equals | notequals | '<' | '<=' | '>' | '>=' ) arithmeticExp[$buffer] )* )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:5: arithmeticExp[$buffer] ( ( equals | notequals | '<' | '<=' | '>' | '>=' ) arithmeticExp[$buffer] )*
            {
            pushFollow(FOLLOW_arithmeticExp_in_relation1106);
            arithmeticExp(buffer);

            state._fsp--;
            if (state.failed) return ;

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:28: ( ( equals | notequals | '<' | '<=' | '>' | '>=' ) arithmeticExp[$buffer] )*
            loop26:
            do {
                int alt26=2;
                switch ( input.LA(1) ) {
                case 27:
                case 28:
                case 30:
                case 31:
                case 40:
                case 44:
                    {
                    alt26=1;
                    }
                    break;

                }

                switch (alt26) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:29: ( equals | notequals | '<' | '<=' | '>' | '>=' ) arithmeticExp[$buffer]
            	    {
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:29: ( equals | notequals | '<' | '<=' | '>' | '>=' )
            	    int alt25=6;
            	    switch ( input.LA(1) ) {
            	    case 40:
            	        {
            	        alt25=1;
            	        }
            	        break;
            	    case 44:
            	        {
            	        alt25=2;
            	        }
            	        break;
            	    case 27:
            	        {
            	        alt25=3;
            	        }
            	        break;
            	    case 28:
            	        {
            	        alt25=4;
            	        }
            	        break;
            	    case 30:
            	        {
            	        alt25=5;
            	        }
            	        break;
            	    case 31:
            	        {
            	        alt25=6;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return ;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 25, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt25) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:31: equals
            	            {
            	            pushFollow(FOLLOW_equals_in_relation1112);
            	            equals12=equals();

            	            state._fsp--;
            	            if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append((equals12!=null?input.toString(equals12.start,equals12.stop):null));}

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:72: notequals
            	            {
            	            pushFollow(FOLLOW_notequals_in_relation1118);
            	            notequals13=notequals();

            	            state._fsp--;
            	            if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append((notequals13!=null?input.toString(notequals13.start,notequals13.stop):null));}

            	            }
            	            break;
            	        case 3 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:119: '<'
            	            {
            	            match(input,27,FOLLOW_27_in_relation1124); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" < ");}

            	            }
            	            break;
            	        case 4 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:150: '<='
            	            {
            	            match(input,28,FOLLOW_28_in_relation1130); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" <= ");}

            	            }
            	            break;
            	        case 5 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:183: '>'
            	            {
            	            match(input,30,FOLLOW_30_in_relation1136); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" > ");}

            	            }
            	            break;
            	        case 6 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:296:214: '>='
            	            {
            	            match(input,31,FOLLOW_31_in_relation1142); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" >= ");}

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_arithmeticExp_in_relation1149);
            	    arithmeticExp(buffer);

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "relation"


    public static class equals_return extends ParserRuleReturnScope {
    };


    // $ANTLR start "equals"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:299:1: equals : 'Is Equal To' ;
    public final AlgorithmLoaderParser.equals_return equals() throws RecognitionException {
        AlgorithmLoaderParser.equals_return retval = new AlgorithmLoaderParser.equals_return();
        retval.start = input.LT(1);


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:299:7: ( 'Is Equal To' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:300:5: 'Is Equal To'
            {
            match(input,40,FOLLOW_40_in_equals1173); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equals"


    public static class notequals_return extends ParserRuleReturnScope {
    };


    // $ANTLR start "notequals"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:303:1: notequals : 'Not Equal To' ;
    public final AlgorithmLoaderParser.notequals_return notequals() throws RecognitionException {
        AlgorithmLoaderParser.notequals_return retval = new AlgorithmLoaderParser.notequals_return();
        retval.start = input.LT(1);


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:303:10: ( 'Not Equal To' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:304:5: 'Not Equal To'
            {
            match(input,44,FOLLOW_44_in_notequals1189); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "notequals"


    public static class negative_return extends ParserRuleReturnScope {
    };


    // $ANTLR start "negative"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:307:1: negative : ( '-' | 'Negative' );
    public final AlgorithmLoaderParser.negative_return negative() throws RecognitionException {
        AlgorithmLoaderParser.negative_return retval = new AlgorithmLoaderParser.negative_return();
        retval.start = input.LT(1);


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:307:10: ( '-' | 'Negative' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
            {
            if ( input.LA(1)==24||input.LA(1)==43 ) {
                input.consume();
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "negative"


    public static class subtraction_return extends ParserRuleReturnScope {
    };


    // $ANTLR start "subtraction"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:311:1: subtraction : ( '-' | 'Subtract' );
    public final AlgorithmLoaderParser.subtraction_return subtraction() throws RecognitionException {
        AlgorithmLoaderParser.subtraction_return retval = new AlgorithmLoaderParser.subtraction_return();
        retval.start = input.LT(1);


        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:311:13: ( '-' | 'Subtract' )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
            {
            if ( input.LA(1)==24||input.LA(1)==50 ) {
                input.consume();
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "subtraction"



    // $ANTLR start "booleanExp"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:1: booleanExp[StringBuffer buffer] : relation[$buffer] ( ( 'And' | 'Or' ) relation[$buffer] )* ;
    public final void booleanExp(StringBuffer buffer) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:34: ( relation[$buffer] ( ( 'And' | 'Or' ) relation[$buffer] )* )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:36: relation[$buffer] ( ( 'And' | 'Or' ) relation[$buffer] )*
            {
            pushFollow(FOLLOW_relation_in_booleanExp1250);
            relation(buffer);

            state._fsp--;
            if (state.failed) return ;

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:54: ( ( 'And' | 'Or' ) relation[$buffer] )*
            loop28:
            do {
                int alt28=2;
                switch ( input.LA(1) ) {
                case 32:
                case 45:
                    {
                    alt28=1;
                    }
                    break;

                }

                switch (alt28) {
            	case 1 :
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:55: ( 'And' | 'Or' ) relation[$buffer]
            	    {
            	    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:55: ( 'And' | 'Or' )
            	    int alt27=2;
            	    switch ( input.LA(1) ) {
            	    case 32:
            	        {
            	        alt27=1;
            	        }
            	        break;
            	    case 45:
            	        {
            	        alt27=2;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return ;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 27, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt27) {
            	        case 1 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:57: 'And'
            	            {
            	            match(input,32,FOLLOW_32_in_booleanExp1256); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" And ");}

            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:315:92: 'Or'
            	            {
            	            match(input,45,FOLLOW_45_in_booleanExp1262); if (state.failed) return ;

            	            if ( state.backtracking==0 ) {buffer.append(" Or ");}

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_relation_in_booleanExp1268);
            	    relation(buffer);

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "booleanExp"



    // $ANTLR start "expression"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:317:1: expression[StringBuffer buffer] : ( ( '(' arithmeticExp[new StringBuffer()] ')' SPECIALFUNCTION )=> specialFunction[$buffer] | '(' booleanExp[$buffer] ')' );
    public final void expression(StringBuffer buffer) throws RecognitionException {
        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:318:5: ( ( '(' arithmeticExp[new StringBuffer()] ')' SPECIALFUNCTION )=> specialFunction[$buffer] | '(' booleanExp[$buffer] ')' )
            int alt29=2;
            switch ( input.LA(1) ) {
            case 19:
                {
                int LA29_1 = input.LA(2);

                if ( (synpred1_AlgorithmLoader()) ) {
                    alt29=1;
                }
                else if ( (true) ) {
                    alt29=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 29, 1, input);

                    throw nvae;

                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;

            }

            switch (alt29) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:318:7: ( '(' arithmeticExp[new StringBuffer()] ')' SPECIALFUNCTION )=> specialFunction[$buffer]
                    {
                    pushFollow(FOLLOW_specialFunction_in_expression1300);
                    specialFunction(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:319:7: '(' booleanExp[$buffer] ')'
                    {
                    match(input,19,FOLLOW_19_in_expression1309); if (state.failed) return ;

                    if ( state.backtracking==0 ) { buffer.append("( "); }

                    pushFollow(FOLLOW_booleanExp_in_expression1313);
                    booleanExp(buffer);

                    state._fsp--;
                    if (state.failed) return ;

                    match(input,20,FOLLOW_20_in_expression1316); if (state.failed) return ;

                    if ( state.backtracking==0 ) { buffer.append(" )"); }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "expression"



    // $ANTLR start "xpath"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:321:1: xpath[StringBuffer buffer] : XPATHCONSTANT ':' STRING ;
    public final void xpath(StringBuffer buffer) throws RecognitionException {
        Token XPATHCONSTANT14=null;
        Token STRING15=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:321:29: ( XPATHCONSTANT ':' STRING )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:321:31: XPATHCONSTANT ':' STRING
            {
            XPATHCONSTANT14=(Token)match(input,XPATHCONSTANT,FOLLOW_XPATHCONSTANT_in_xpath1328); if (state.failed) return ;

            match(input,26,FOLLOW_26_in_xpath1330); if (state.failed) return ;

            STRING15=(Token)match(input,STRING,FOLLOW_STRING_in_xpath1332); if (state.failed) return ;

            if ( state.backtracking==0 ) {
                        buffer.append((XPATHCONSTANT14!=null?XPATHCONSTANT14.getText():null)).append(":").append((STRING15!=null?STRING15.getText():null));
                    }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "xpath"



    // $ANTLR start "varType"
    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:327:1: varType[StringBuffer buffer] : VARCONSTANT ':' (t= IDENTS |s= STRING ) ;
    public final void varType(StringBuffer buffer) throws RecognitionException {
        Token t=null;
        Token s=null;
        Token VARCONSTANT16=null;

        try {
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:327:31: ( VARCONSTANT ':' (t= IDENTS |s= STRING ) )
            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:327:33: VARCONSTANT ':' (t= IDENTS |s= STRING )
            {
            VARCONSTANT16=(Token)match(input,VARCONSTANT,FOLLOW_VARCONSTANT_in_varType1362); if (state.failed) return ;

            match(input,26,FOLLOW_26_in_varType1364); if (state.failed) return ;

            // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:327:49: (t= IDENTS |s= STRING )
            int alt30=2;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                alt30=1;
                }
                break;
            case STRING:
                {
                alt30=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;

            }

            switch (alt30) {
                case 1 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:327:50: t= IDENTS
                    {
                    t=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_varType1369); if (state.failed) return ;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:327:61: s= STRING
                    {
                    s=(Token)match(input,STRING,FOLLOW_STRING_in_varType1375); if (state.failed) return ;

                    }
                    break;

            }


            if ( state.backtracking==0 ) { 
                        if(t!=null)
                        {
                            buffer.append((VARCONSTANT16!=null?VARCONSTANT16.getText():null)).append(":").append((t!=null?t.getText():null));
                        }else{
                            buffer.append((VARCONSTANT16!=null?VARCONSTANT16.getText():null)).append(":").append((s!=null?s.getText():null));
                        }   
                    }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "varType"

    // $ANTLR start synpred1_AlgorithmLoader
    public final void synpred1_AlgorithmLoader_fragment() throws RecognitionException {
        // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:318:7: ( '(' arithmeticExp[new StringBuffer()] ')' SPECIALFUNCTION )
        // com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:318:8: '(' arithmeticExp[new StringBuffer()] ')' SPECIALFUNCTION
        {
        match(input,19,FOLLOW_19_in_synpred1_AlgorithmLoader1287); if (state.failed) return ;

        pushFollow(FOLLOW_arithmeticExp_in_synpred1_AlgorithmLoader1289);
        arithmeticExp(new StringBuffer());

        state._fsp--;
        if (state.failed) return ;

        match(input,20,FOLLOW_20_in_synpred1_AlgorithmLoader1292); if (state.failed) return ;

        match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_synpred1_AlgorithmLoader1294); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred1_AlgorithmLoader

    // Delegated rules

    public final boolean synpred1_AlgorithmLoader() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_AlgorithmLoader_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_sheet_in_algorithm96 = new BitSet(new long[]{0x0000800600000000L});
    public static final BitSet FOLLOW_EOF_in_algorithm101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule_in_sheet124 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_processblock_in_sheet130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_rule144 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTS_in_rule148 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_rule150 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTS_in_rule154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_processblock196 = new BitSet(new long[]{0x0000000000004100L});
    public static final BitSet FOLLOW_STRING_in_processblock205 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTS_in_processblock211 = new BitSet(new long[]{0x0002428800050102L});
    public static final BitSet FOLLOW_statement_in_processblock230 = new BitSet(new long[]{0x0002428800050102L});
    public static final BitSet FOLLOW_pch_in_processblock255 = new BitSet(new long[]{0x0002400000000002L});
    public static final BitSet FOLLOW_set_in_pch268 = new BitSet(new long[]{0x0000000000004100L});
    public static final BitSet FOLLOW_IDENTS_in_pch279 = new BitSet(new long[]{0x0000028800050100L});
    public static final BitSet FOLLOW_STRING_in_pch283 = new BitSet(new long[]{0x0000028800050100L});
    public static final BitSet FOLLOW_statements_in_pch288 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_statements359 = new BitSet(new long[]{0x0000028800050102L});
    public static final BitSet FOLLOW_statement_in_statements367 = new BitSet(new long[]{0x0000028800050102L});
    public static final BitSet FOLLOW_assignment_in_statement398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loopStatement_in_statement408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_statement413 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callProgram_in_statement418 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_function446 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_function448 = new BitSet(new long[]{0x00510800015D4310L});
    public static final BitSet FOLLOW_52_in_function453 = new BitSet(new long[]{0x00410800015D4310L});
    public static final BitSet FOLLOW_arithmeticExp_in_function458 = new BitSet(new long[]{0x0000000000900000L});
    public static final BitSet FOLLOW_23_in_function462 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_arithmeticExp_in_function466 = new BitSet(new long[]{0x0000000000900000L});
    public static final BitSet FOLLOW_20_in_function473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_domainTable493 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_55_in_domainTable495 = new BitSet(new long[]{0x01010000000D4310L});
    public static final BitSet FOLLOW_atom_in_domainTable500 = new BitSet(new long[]{0x0100000000800000L});
    public static final BitSet FOLLOW_23_in_domainTable504 = new BitSet(new long[]{0x00010000000D4310L});
    public static final BitSet FOLLOW_atom_in_domainTable508 = new BitSet(new long[]{0x0100000000800000L});
    public static final BitSet FOLLOW_56_in_domainTable515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_callProgram534 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTS_in_callProgram536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_specialFunction575 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_arithmeticExp_in_specialFunction577 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_specialFunction580 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_specialFunction582 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rateFunction635 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_rateFunction637 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_function_in_rateFunction641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_assignment669 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_varType_in_assignment675 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_xpath_in_assignment680 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_assignment685 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_arithmeticExp_in_assignment690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ifStatement717 = new BitSet(new long[]{0x0000040000080000L});
    public static final BitSet FOLLOW_42_in_ifStatement722 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ifStatement728 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_booleanExp_in_ifStatement732 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ifStatement735 = new BitSet(new long[]{0x0000028800050100L});
    public static final BitSet FOLLOW_statements_in_ifStatement741 = new BitSet(new long[]{0x0000003000000000L});
    public static final BitSet FOLLOW_36_in_ifStatement754 = new BitSet(new long[]{0x0000028800050100L});
    public static final BitSet FOLLOW_statements_in_ifStatement760 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ifStatement778 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_loopStatement806 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_STRING_in_loopStatement810 = new BitSet(new long[]{0x0000028800050100L});
    public static final BitSet FOLLOW_statements_in_loopStatement825 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_loopStatement837 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_STRING_in_loopStatement841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_atom881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_atom887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_atom892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_atom897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_atom903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_xpath_in_atom909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rateFunction_in_atom914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varType_in_atom919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_domainTable_in_atom924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CHAR_in_atom929 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_negation951 = new BitSet(new long[]{0x00010000000D4310L});
    public static final BitSet FOLLOW_atom_in_negation957 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_unary982 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_negative_in_unary988 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_negation_in_unary995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_in_multi1019 = new BitSet(new long[]{0x0020000002200002L});
    public static final BitSet FOLLOW_21_in_multi1025 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_25_in_multi1031 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_53_in_multi1037 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_unary_in_multi1043 = new BitSet(new long[]{0x0020000002200002L});
    public static final BitSet FOLLOW_multi_in_arithmeticExp1066 = new BitSet(new long[]{0x0004000001400002L});
    public static final BitSet FOLLOW_22_in_arithmeticExp1072 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_subtraction_in_arithmeticExp1078 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_multi_in_arithmeticExp1084 = new BitSet(new long[]{0x0004000001400002L});
    public static final BitSet FOLLOW_arithmeticExp_in_relation1106 = new BitSet(new long[]{0x00001100D8000002L});
    public static final BitSet FOLLOW_equals_in_relation1112 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_notequals_in_relation1118 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_27_in_relation1124 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_28_in_relation1130 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_30_in_relation1136 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_31_in_relation1142 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_arithmeticExp_in_relation1149 = new BitSet(new long[]{0x00001100D8000002L});
    public static final BitSet FOLLOW_40_in_equals1173 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_notequals1189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relation_in_booleanExp1250 = new BitSet(new long[]{0x0000200100000002L});
    public static final BitSet FOLLOW_32_in_booleanExp1256 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_45_in_booleanExp1262 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_relation_in_booleanExp1268 = new BitSet(new long[]{0x0000200100000002L});
    public static final BitSet FOLLOW_specialFunction_in_expression1300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_expression1309 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_booleanExp_in_expression1313 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_expression1316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_XPATHCONSTANT_in_xpath1328 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_xpath1330 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_STRING_in_xpath1332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VARCONSTANT_in_varType1362 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_varType1364 = new BitSet(new long[]{0x0000000000004100L});
    public static final BitSet FOLLOW_IDENTS_in_varType1369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_varType1375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_synpred1_AlgorithmLoader1287 = new BitSet(new long[]{0x00410800014D4310L});
    public static final BitSet FOLLOW_arithmeticExp_in_synpred1_AlgorithmLoader1289 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_synpred1_AlgorithmLoader1292 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_synpred1_AlgorithmLoader1294 = new BitSet(new long[]{0x0000000000000002L});

}