// $ANTLR 3.4 com\\mmpnc\\xml\\xpath\\grammar\\XPath.g 2013-08-01 12:29:59

  package com.mmpnc.xml.xpath.parse;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class XPathParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABRPATH", "APOS", "AT", "AxisName", "CC", "COLON", "COMMA", "CURRENT", "CURRENTNODE", "DOT", "DOTDOT", "Digits", "FUNCTION", "GE", "LBRAC", "LE", "LESS", "LPAR", "Literal", "MINUS", "MORE", "MUL", "NAMETEST", "NCName", "NCNameChar", "NCNameStartChar", "NEGETION", "NODETEST", "NodeType", "Number", "PATH", "PATHSEP", "PIPE", "PLUS", "PRIORNODE", "QUOT", "RBRAC", "RELATIVEFROMROOT", "RELATIVEPATH", "RPAR", "Whitespace", "'!='", "'$'", "'='", "'and'", "'div'", "'mod'", "'or'", "'processing-instruction'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int ABRPATH=4;
    public static final int APOS=5;
    public static final int AT=6;
    public static final int AxisName=7;
    public static final int CC=8;
    public static final int COLON=9;
    public static final int COMMA=10;
    public static final int CURRENT=11;
    public static final int CURRENTNODE=12;
    public static final int DOT=13;
    public static final int DOTDOT=14;
    public static final int Digits=15;
    public static final int FUNCTION=16;
    public static final int GE=17;
    public static final int LBRAC=18;
    public static final int LE=19;
    public static final int LESS=20;
    public static final int LPAR=21;
    public static final int Literal=22;
    public static final int MINUS=23;
    public static final int MORE=24;
    public static final int MUL=25;
    public static final int NAMETEST=26;
    public static final int NCName=27;
    public static final int NCNameChar=28;
    public static final int NCNameStartChar=29;
    public static final int NEGETION=30;
    public static final int NODETEST=31;
    public static final int NodeType=32;
    public static final int Number=33;
    public static final int PATH=34;
    public static final int PATHSEP=35;
    public static final int PIPE=36;
    public static final int PLUS=37;
    public static final int PRIORNODE=38;
    public static final int QUOT=39;
    public static final int RBRAC=40;
    public static final int RELATIVEFROMROOT=41;
    public static final int RELATIVEPATH=42;
    public static final int RPAR=43;
    public static final int Whitespace=44;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public XPathParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public XPathParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return XPathParser.tokenNames; }
    public String getGrammarFileName() { return "com\\mmpnc\\xml\\xpath\\grammar\\XPath.g"; }


    public static class xpath_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "xpath"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:68:1: xpath : expr EOF !;
    public final XPathParser.xpath_return xpath() throws RecognitionException {
        XPathParser.xpath_return retval = new XPathParser.xpath_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        XPathParser.expr_return expr1 =null;


        CommonTree EOF2_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:68:8: ( expr EOF !)
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:68:11: expr EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_expr_in_xpath360);
            expr1=expr();

            state._fsp--;

            adaptor.addChild(root_0, expr1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_xpath362); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "xpath"


    public static class locationPath_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "locationPath"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:70:1: locationPath : ( relativeLocationPath | absoluteLocationPathNoroot );
    public final XPathParser.locationPath_return locationPath() throws RecognitionException {
        XPathParser.locationPath_return retval = new XPathParser.locationPath_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        XPathParser.relativeLocationPath_return relativeLocationPath3 =null;

        XPathParser.absoluteLocationPathNoroot_return absoluteLocationPathNoroot4 =null;



        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:71:3: ( relativeLocationPath | absoluteLocationPathNoroot )
            int alt1=2;
            switch ( input.LA(1) ) {
            case AT:
            case AxisName:
            case DOT:
            case DOTDOT:
            case MUL:
            case NCName:
            case NodeType:
            case 52:
                {
                alt1=1;
                }
                break;
            case ABRPATH:
            case PATHSEP:
                {
                alt1=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:71:6: relativeLocationPath
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_relativeLocationPath_in_locationPath375);
                    relativeLocationPath3=relativeLocationPath();

                    state._fsp--;

                    adaptor.addChild(root_0, relativeLocationPath3.getTree());

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:72:6: absoluteLocationPathNoroot
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_absoluteLocationPathNoroot_in_locationPath382);
                    absoluteLocationPathNoroot4=absoluteLocationPathNoroot();

                    state._fsp--;

                    adaptor.addChild(root_0, absoluteLocationPathNoroot4.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "locationPath"


    public static class absoluteLocationPathNoroot_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "absoluteLocationPathNoroot"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:75:1: absoluteLocationPathNoroot : ( '/' relativeLocationPath -> ^( CURRENT relativeLocationPath ) | '//' relativeLocationPath -> ^( RELATIVEFROMROOT relativeLocationPath ) );
    public final XPathParser.absoluteLocationPathNoroot_return absoluteLocationPathNoroot() throws RecognitionException {
        XPathParser.absoluteLocationPathNoroot_return retval = new XPathParser.absoluteLocationPathNoroot_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal5=null;
        Token string_literal7=null;
        XPathParser.relativeLocationPath_return relativeLocationPath6 =null;

        XPathParser.relativeLocationPath_return relativeLocationPath8 =null;


        CommonTree char_literal5_tree=null;
        CommonTree string_literal7_tree=null;
        RewriteRuleTokenStream stream_ABRPATH=new RewriteRuleTokenStream(adaptor,"token ABRPATH");
        RewriteRuleTokenStream stream_PATHSEP=new RewriteRuleTokenStream(adaptor,"token PATHSEP");
        RewriteRuleSubtreeStream stream_relativeLocationPath=new RewriteRuleSubtreeStream(adaptor,"rule relativeLocationPath");
        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:76:3: ( '/' relativeLocationPath -> ^( CURRENT relativeLocationPath ) | '//' relativeLocationPath -> ^( RELATIVEFROMROOT relativeLocationPath ) )
            int alt2=2;
            switch ( input.LA(1) ) {
            case PATHSEP:
                {
                alt2=1;
                }
                break;
            case ABRPATH:
                {
                alt2=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:76:6: '/' relativeLocationPath
                    {
                    char_literal5=(Token)match(input,PATHSEP,FOLLOW_PATHSEP_in_absoluteLocationPathNoroot396);  
                    stream_PATHSEP.add(char_literal5);


                    pushFollow(FOLLOW_relativeLocationPath_in_absoluteLocationPathNoroot398);
                    relativeLocationPath6=relativeLocationPath();

                    state._fsp--;

                    stream_relativeLocationPath.add(relativeLocationPath6.getTree());

                    // AST REWRITE
                    // elements: relativeLocationPath
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 76:31: -> ^( CURRENT relativeLocationPath )
                    {
                        // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:76:34: ^( CURRENT relativeLocationPath )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(CURRENT, "CURRENT")
                        , root_1);

                        adaptor.addChild(root_1, stream_relativeLocationPath.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:77:6: '//' relativeLocationPath
                    {
                    string_literal7=(Token)match(input,ABRPATH,FOLLOW_ABRPATH_in_absoluteLocationPathNoroot413);  
                    stream_ABRPATH.add(string_literal7);


                    pushFollow(FOLLOW_relativeLocationPath_in_absoluteLocationPathNoroot415);
                    relativeLocationPath8=relativeLocationPath();

                    state._fsp--;

                    stream_relativeLocationPath.add(relativeLocationPath8.getTree());

                    // AST REWRITE
                    // elements: relativeLocationPath
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 77:32: -> ^( RELATIVEFROMROOT relativeLocationPath )
                    {
                        // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:77:35: ^( RELATIVEFROMROOT relativeLocationPath )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(RELATIVEFROMROOT, "RELATIVEFROMROOT")
                        , root_1);

                        adaptor.addChild(root_1, stream_relativeLocationPath.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "absoluteLocationPathNoroot"


    public static class relativeLocationPath_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relativeLocationPath"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:80:1: relativeLocationPath : step ( relativepath ^ step )* ;
    public final XPathParser.relativeLocationPath_return relativeLocationPath() throws RecognitionException {
        XPathParser.relativeLocationPath_return retval = new XPathParser.relativeLocationPath_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        XPathParser.step_return step9 =null;

        XPathParser.relativepath_return relativepath10 =null;

        XPathParser.step_return step11 =null;



        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:81:3: ( step ( relativepath ^ step )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:81:6: step ( relativepath ^ step )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_step_in_relativeLocationPath437);
            step9=step();

            state._fsp--;

            adaptor.addChild(root_0, step9.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:81:11: ( relativepath ^ step )*
            loop3:
            do {
                int alt3=2;
                switch ( input.LA(1) ) {
                case ABRPATH:
                case PATHSEP:
                    {
                    alt3=1;
                    }
                    break;

                }

                switch (alt3) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:81:12: relativepath ^ step
            	    {
            	    pushFollow(FOLLOW_relativepath_in_relativeLocationPath440);
            	    relativepath10=relativepath();

            	    state._fsp--;

            	    root_0 = (CommonTree)adaptor.becomeRoot(relativepath10.getTree(), root_0);

            	    pushFollow(FOLLOW_step_in_relativeLocationPath443);
            	    step11=step();

            	    state._fsp--;

            	    adaptor.addChild(root_0, step11.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relativeLocationPath"


    public static class relativepath_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relativepath"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:83:1: relativepath : ( '/' -> PATH | '//' -> RELATIVEPATH ) ;
    public final XPathParser.relativepath_return relativepath() throws RecognitionException {
        XPathParser.relativepath_return retval = new XPathParser.relativepath_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal12=null;
        Token string_literal13=null;

        CommonTree char_literal12_tree=null;
        CommonTree string_literal13_tree=null;
        RewriteRuleTokenStream stream_ABRPATH=new RewriteRuleTokenStream(adaptor,"token ABRPATH");
        RewriteRuleTokenStream stream_PATHSEP=new RewriteRuleTokenStream(adaptor,"token PATHSEP");

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:83:13: ( ( '/' -> PATH | '//' -> RELATIVEPATH ) )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:83:15: ( '/' -> PATH | '//' -> RELATIVEPATH )
            {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:83:15: ( '/' -> PATH | '//' -> RELATIVEPATH )
            int alt4=2;
            switch ( input.LA(1) ) {
            case PATHSEP:
                {
                alt4=1;
                }
                break;
            case ABRPATH:
                {
                alt4=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }

            switch (alt4) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:83:16: '/'
                    {
                    char_literal12=(Token)match(input,PATHSEP,FOLLOW_PATHSEP_in_relativepath454);  
                    stream_PATHSEP.add(char_literal12);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 83:20: -> PATH
                    {
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(PATH, "PATH")
                        );

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:83:29: '//'
                    {
                    string_literal13=(Token)match(input,ABRPATH,FOLLOW_ABRPATH_in_relativepath461);  
                    stream_ABRPATH.add(string_literal13);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 83:34: -> RELATIVEPATH
                    {
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(RELATIVEPATH, "RELATIVEPATH")
                        );

                    }


                    retval.tree = root_0;

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relativepath"


    public static class step_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "step"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:85:1: step : ( axisSpecifier nodeTest ( predicate )* | abbreviatedStep );
    public final XPathParser.step_return step() throws RecognitionException {
        XPathParser.step_return retval = new XPathParser.step_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        XPathParser.axisSpecifier_return axisSpecifier14 =null;

        XPathParser.nodeTest_return nodeTest15 =null;

        XPathParser.predicate_return predicate16 =null;

        XPathParser.abbreviatedStep_return abbreviatedStep17 =null;



        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:85:7: ( axisSpecifier nodeTest ( predicate )* | abbreviatedStep )
            int alt6=2;
            switch ( input.LA(1) ) {
            case AT:
            case AxisName:
            case MUL:
            case NCName:
            case NodeType:
            case 52:
                {
                alt6=1;
                }
                break;
            case DOT:
            case DOTDOT:
                {
                alt6=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:85:10: axisSpecifier nodeTest ( predicate )*
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_axisSpecifier_in_step478);
                    axisSpecifier14=axisSpecifier();

                    state._fsp--;

                    adaptor.addChild(root_0, axisSpecifier14.getTree());

                    pushFollow(FOLLOW_nodeTest_in_step480);
                    nodeTest15=nodeTest();

                    state._fsp--;

                    adaptor.addChild(root_0, nodeTest15.getTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:85:33: ( predicate )*
                    loop5:
                    do {
                        int alt5=2;
                        switch ( input.LA(1) ) {
                        case LBRAC:
                            {
                            alt5=1;
                            }
                            break;

                        }

                        switch (alt5) {
                    	case 1 :
                    	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:85:33: predicate
                    	    {
                    	    pushFollow(FOLLOW_predicate_in_step482);
                    	    predicate16=predicate();

                    	    state._fsp--;

                    	    adaptor.addChild(root_0, predicate16.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:86:6: abbreviatedStep
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_abbreviatedStep_in_step490);
                    abbreviatedStep17=abbreviatedStep();

                    state._fsp--;

                    adaptor.addChild(root_0, abbreviatedStep17.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "step"


    public static class axisSpecifier_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "axisSpecifier"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:89:1: axisSpecifier : ( AxisName '::' !| ( '@' -> AT )? );
    public final XPathParser.axisSpecifier_return axisSpecifier() throws RecognitionException {
        XPathParser.axisSpecifier_return retval = new XPathParser.axisSpecifier_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token AxisName18=null;
        Token string_literal19=null;
        Token char_literal20=null;

        CommonTree AxisName18_tree=null;
        CommonTree string_literal19_tree=null;
        CommonTree char_literal20_tree=null;
        RewriteRuleTokenStream stream_AT=new RewriteRuleTokenStream(adaptor,"token AT");

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:90:3: ( AxisName '::' !| ( '@' -> AT )? )
            int alt8=2;
            switch ( input.LA(1) ) {
            case AxisName:
                {
                switch ( input.LA(2) ) {
                case CC:
                    {
                    alt8=1;
                    }
                    break;
                case EOF:
                case ABRPATH:
                case COLON:
                case COMMA:
                case GE:
                case LBRAC:
                case LE:
                case LESS:
                case MINUS:
                case MORE:
                case MUL:
                case PATHSEP:
                case PIPE:
                case PLUS:
                case RBRAC:
                case RPAR:
                case 45:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 54:
                    {
                    alt8=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;

                }

                }
                break;
            case AT:
            case MUL:
            case NCName:
            case NodeType:
            case 52:
                {
                alt8=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }

            switch (alt8) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:90:6: AxisName '::' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    AxisName18=(Token)match(input,AxisName,FOLLOW_AxisName_in_axisSpecifier504); 
                    AxisName18_tree = 
                    (CommonTree)adaptor.create(AxisName18)
                    ;
                    adaptor.addChild(root_0, AxisName18_tree);


                    string_literal19=(Token)match(input,CC,FOLLOW_CC_in_axisSpecifier506); 

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:91:6: ( '@' -> AT )?
                    {
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:91:6: ( '@' -> AT )?
                    int alt7=2;
                    switch ( input.LA(1) ) {
                        case AT:
                            {
                            alt7=1;
                            }
                            break;
                    }

                    switch (alt7) {
                        case 1 :
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:91:7: '@'
                            {
                            char_literal20=(Token)match(input,AT,FOLLOW_AT_in_axisSpecifier515);  
                            stream_AT.add(char_literal20);


                            // AST REWRITE
                            // elements: 
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                            root_0 = (CommonTree)adaptor.nil();
                            // 91:11: -> AT
                            {
                                adaptor.addChild(root_0, 
                                (CommonTree)adaptor.create(AT, "AT")
                                );

                            }


                            retval.tree = root_0;

                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "axisSpecifier"


    public static class nodeTest_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "nodeTest"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:94:1: nodeTest : ( nameTest -> ^( NAMETEST nameTest ) | NodeType '(' ')' -> ^( NODETEST NodeType ) | 'processing-instruction' '(' Literal ')' -> ^( NODETEST NodeType Literal ) );
    public final XPathParser.nodeTest_return nodeTest() throws RecognitionException {
        XPathParser.nodeTest_return retval = new XPathParser.nodeTest_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NodeType22=null;
        Token char_literal23=null;
        Token char_literal24=null;
        Token string_literal25=null;
        Token char_literal26=null;
        Token Literal27=null;
        Token char_literal28=null;
        XPathParser.nameTest_return nameTest21 =null;


        CommonTree NodeType22_tree=null;
        CommonTree char_literal23_tree=null;
        CommonTree char_literal24_tree=null;
        CommonTree string_literal25_tree=null;
        CommonTree char_literal26_tree=null;
        CommonTree Literal27_tree=null;
        CommonTree char_literal28_tree=null;
        RewriteRuleTokenStream stream_RPAR=new RewriteRuleTokenStream(adaptor,"token RPAR");
        RewriteRuleTokenStream stream_Literal=new RewriteRuleTokenStream(adaptor,"token Literal");
        RewriteRuleTokenStream stream_LPAR=new RewriteRuleTokenStream(adaptor,"token LPAR");
        RewriteRuleTokenStream stream_NodeType=new RewriteRuleTokenStream(adaptor,"token NodeType");
        RewriteRuleTokenStream stream_52=new RewriteRuleTokenStream(adaptor,"token 52");
        RewriteRuleSubtreeStream stream_nameTest=new RewriteRuleSubtreeStream(adaptor,"rule nameTest");
        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:94:9: ( nameTest -> ^( NAMETEST nameTest ) | NodeType '(' ')' -> ^( NODETEST NodeType ) | 'processing-instruction' '(' Literal ')' -> ^( NODETEST NodeType Literal ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case AxisName:
            case MUL:
            case NCName:
                {
                alt9=1;
                }
                break;
            case NodeType:
                {
                alt9=2;
                }
                break;
            case 52:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }

            switch (alt9) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:94:12: nameTest
                    {
                    pushFollow(FOLLOW_nameTest_in_nodeTest532);
                    nameTest21=nameTest();

                    state._fsp--;

                    stream_nameTest.add(nameTest21.getTree());

                    // AST REWRITE
                    // elements: nameTest
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 94:21: -> ^( NAMETEST nameTest )
                    {
                        // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:94:24: ^( NAMETEST nameTest )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(NAMETEST, "NAMETEST")
                        , root_1);

                        adaptor.addChild(root_1, stream_nameTest.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:95:6: NodeType '(' ')'
                    {
                    NodeType22=(Token)match(input,NodeType,FOLLOW_NodeType_in_nodeTest548);  
                    stream_NodeType.add(NodeType22);


                    char_literal23=(Token)match(input,LPAR,FOLLOW_LPAR_in_nodeTest550);  
                    stream_LPAR.add(char_literal23);


                    char_literal24=(Token)match(input,RPAR,FOLLOW_RPAR_in_nodeTest552);  
                    stream_RPAR.add(char_literal24);


                    // AST REWRITE
                    // elements: NodeType
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 95:23: -> ^( NODETEST NodeType )
                    {
                        // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:95:26: ^( NODETEST NodeType )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(NODETEST, "NODETEST")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_NodeType.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:96:6: 'processing-instruction' '(' Literal ')'
                    {
                    string_literal25=(Token)match(input,52,FOLLOW_52_in_nodeTest568);  
                    stream_52.add(string_literal25);


                    char_literal26=(Token)match(input,LPAR,FOLLOW_LPAR_in_nodeTest570);  
                    stream_LPAR.add(char_literal26);


                    Literal27=(Token)match(input,Literal,FOLLOW_Literal_in_nodeTest572);  
                    stream_Literal.add(Literal27);


                    char_literal28=(Token)match(input,RPAR,FOLLOW_RPAR_in_nodeTest574);  
                    stream_RPAR.add(char_literal28);


                    // AST REWRITE
                    // elements: Literal
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 96:47: -> ^( NODETEST NodeType Literal )
                    {
                        // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:96:50: ^( NODETEST NodeType Literal )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(NODETEST, "NODETEST")
                        , root_1);

                        adaptor.addChild(root_1, 
                        (CommonTree)adaptor.create(NodeType, "NodeType")
                        );

                        adaptor.addChild(root_1, 
                        stream_Literal.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "nodeTest"


    public static class predicate_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "predicate"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:99:1: predicate : '[' expr ']' ;
    public final XPathParser.predicate_return predicate() throws RecognitionException {
        XPathParser.predicate_return retval = new XPathParser.predicate_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal29=null;
        Token char_literal31=null;
        XPathParser.expr_return expr30 =null;


        CommonTree char_literal29_tree=null;
        CommonTree char_literal31_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:100:3: ( '[' expr ']' )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:100:6: '[' expr ']'
            {
            root_0 = (CommonTree)adaptor.nil();


            char_literal29=(Token)match(input,LBRAC,FOLLOW_LBRAC_in_predicate599); 
            char_literal29_tree = 
            (CommonTree)adaptor.create(char_literal29)
            ;
            adaptor.addChild(root_0, char_literal29_tree);


            pushFollow(FOLLOW_expr_in_predicate601);
            expr30=expr();

            state._fsp--;

            adaptor.addChild(root_0, expr30.getTree());

            char_literal31=(Token)match(input,RBRAC,FOLLOW_RBRAC_in_predicate603); 
            char_literal31_tree = 
            (CommonTree)adaptor.create(char_literal31)
            ;
            adaptor.addChild(root_0, char_literal31_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "predicate"


    public static class abbreviatedStep_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "abbreviatedStep"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:103:1: abbreviatedStep : ( '.' -> CURRENTNODE | '..' -> PRIORNODE );
    public final XPathParser.abbreviatedStep_return abbreviatedStep() throws RecognitionException {
        XPathParser.abbreviatedStep_return retval = new XPathParser.abbreviatedStep_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal32=null;
        Token string_literal33=null;

        CommonTree char_literal32_tree=null;
        CommonTree string_literal33_tree=null;
        RewriteRuleTokenStream stream_DOTDOT=new RewriteRuleTokenStream(adaptor,"token DOTDOT");
        RewriteRuleTokenStream stream_DOT=new RewriteRuleTokenStream(adaptor,"token DOT");

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:104:3: ( '.' -> CURRENTNODE | '..' -> PRIORNODE )
            int alt10=2;
            switch ( input.LA(1) ) {
            case DOT:
                {
                alt10=1;
                }
                break;
            case DOTDOT:
                {
                alt10=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }

            switch (alt10) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:104:6: '.'
                    {
                    char_literal32=(Token)match(input,DOT,FOLLOW_DOT_in_abbreviatedStep618);  
                    stream_DOT.add(char_literal32);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 104:10: -> CURRENTNODE
                    {
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(CURRENTNODE, "CURRENTNODE")
                        );

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:105:6: '..'
                    {
                    string_literal33=(Token)match(input,DOTDOT,FOLLOW_DOTDOT_in_abbreviatedStep629);  
                    stream_DOTDOT.add(string_literal33);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 105:11: -> PRIORNODE
                    {
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(PRIORNODE, "PRIORNODE")
                        );

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "abbreviatedStep"


    public static class expr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:108:1: expr : orExpr ;
    public final XPathParser.expr_return expr() throws RecognitionException {
        XPathParser.expr_return retval = new XPathParser.expr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        XPathParser.orExpr_return orExpr34 =null;



        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:108:7: ( orExpr )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:108:10: orExpr
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_orExpr_in_expr646);
            orExpr34=orExpr();

            state._fsp--;

            adaptor.addChild(root_0, orExpr34.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"


    public static class primaryExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "primaryExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:111:1: primaryExpr : ( variableReference | '(' expr ')' | Literal | Number | functionCall | spclFunctionCall );
    public final XPathParser.primaryExpr_return primaryExpr() throws RecognitionException {
        XPathParser.primaryExpr_return retval = new XPathParser.primaryExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal36=null;
        Token char_literal38=null;
        Token Literal39=null;
        Token Number40=null;
        XPathParser.variableReference_return variableReference35 =null;

        XPathParser.expr_return expr37 =null;

        XPathParser.functionCall_return functionCall41 =null;

        XPathParser.spclFunctionCall_return spclFunctionCall42 =null;


        CommonTree char_literal36_tree=null;
        CommonTree char_literal38_tree=null;
        CommonTree Literal39_tree=null;
        CommonTree Number40_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:112:3: ( variableReference | '(' expr ')' | Literal | Number | functionCall | spclFunctionCall )
            int alt11=6;
            switch ( input.LA(1) ) {
            case 46:
                {
                alt11=1;
                }
                break;
            case LPAR:
                {
                alt11=2;
                }
                break;
            case Literal:
                {
                alt11=3;
                }
                break;
            case Number:
                {
                alt11=4;
                }
                break;
            case AxisName:
            case NCName:
                {
                switch ( input.LA(2) ) {
                case COLON:
                    {
                    switch ( input.LA(3) ) {
                    case AxisName:
                    case NCName:
                        {
                        switch ( input.LA(4) ) {
                        case LPAR:
                            {
                            alt11=5;
                            }
                            break;
                        case 53:
                            {
                            alt11=6;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 11, 9, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 11, 6, input);

                        throw nvae;

                    }

                    }
                    break;
                case LPAR:
                    {
                    alt11=5;
                    }
                    break;
                case 53:
                    {
                    alt11=6;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 5, input);

                    throw nvae;

                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }

            switch (alt11) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:112:6: variableReference
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_variableReference_in_primaryExpr660);
                    variableReference35=variableReference();

                    state._fsp--;

                    adaptor.addChild(root_0, variableReference35.getTree());

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:113:6: '(' expr ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal36=(Token)match(input,LPAR,FOLLOW_LPAR_in_primaryExpr667); 
                    char_literal36_tree = 
                    (CommonTree)adaptor.create(char_literal36)
                    ;
                    adaptor.addChild(root_0, char_literal36_tree);


                    pushFollow(FOLLOW_expr_in_primaryExpr669);
                    expr37=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr37.getTree());

                    char_literal38=(Token)match(input,RPAR,FOLLOW_RPAR_in_primaryExpr671); 
                    char_literal38_tree = 
                    (CommonTree)adaptor.create(char_literal38)
                    ;
                    adaptor.addChild(root_0, char_literal38_tree);


                    }
                    break;
                case 3 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:114:6: Literal
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    Literal39=(Token)match(input,Literal,FOLLOW_Literal_in_primaryExpr678); 
                    Literal39_tree = 
                    (CommonTree)adaptor.create(Literal39)
                    ;
                    adaptor.addChild(root_0, Literal39_tree);


                    }
                    break;
                case 4 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:115:6: Number
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    Number40=(Token)match(input,Number,FOLLOW_Number_in_primaryExpr685); 
                    Number40_tree = 
                    (CommonTree)adaptor.create(Number40)
                    ;
                    adaptor.addChild(root_0, Number40_tree);


                    }
                    break;
                case 5 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:116:6: functionCall
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_functionCall_in_primaryExpr694);
                    functionCall41=functionCall();

                    state._fsp--;

                    adaptor.addChild(root_0, functionCall41.getTree());

                    }
                    break;
                case 6 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:117:6: spclFunctionCall
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_spclFunctionCall_in_primaryExpr701);
                    spclFunctionCall42=spclFunctionCall();

                    state._fsp--;

                    adaptor.addChild(root_0, spclFunctionCall42.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "primaryExpr"


    public static class functionCall_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "functionCall"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:120:1: functionCall : functionName '(' ( expr ( ',' expr )* )? ')' -> ^( FUNCTION functionName ( expr ( ',' expr )* )? ) ;
    public final XPathParser.functionCall_return functionCall() throws RecognitionException {
        XPathParser.functionCall_return retval = new XPathParser.functionCall_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal44=null;
        Token char_literal46=null;
        Token char_literal48=null;
        XPathParser.functionName_return functionName43 =null;

        XPathParser.expr_return expr45 =null;

        XPathParser.expr_return expr47 =null;


        CommonTree char_literal44_tree=null;
        CommonTree char_literal46_tree=null;
        CommonTree char_literal48_tree=null;
        RewriteRuleTokenStream stream_RPAR=new RewriteRuleTokenStream(adaptor,"token RPAR");
        RewriteRuleTokenStream stream_LPAR=new RewriteRuleTokenStream(adaptor,"token LPAR");
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        RewriteRuleSubtreeStream stream_functionName=new RewriteRuleSubtreeStream(adaptor,"rule functionName");
        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:3: ( functionName '(' ( expr ( ',' expr )* )? ')' -> ^( FUNCTION functionName ( expr ( ',' expr )* )? ) )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:6: functionName '(' ( expr ( ',' expr )* )? ')'
            {
            pushFollow(FOLLOW_functionName_in_functionCall715);
            functionName43=functionName();

            state._fsp--;

            stream_functionName.add(functionName43.getTree());

            char_literal44=(Token)match(input,LPAR,FOLLOW_LPAR_in_functionCall717);  
            stream_LPAR.add(char_literal44);


            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:23: ( expr ( ',' expr )* )?
            int alt13=2;
            switch ( input.LA(1) ) {
                case ABRPATH:
                case AT:
                case AxisName:
                case DOT:
                case DOTDOT:
                case LPAR:
                case Literal:
                case MINUS:
                case MUL:
                case NCName:
                case NodeType:
                case Number:
                case PATHSEP:
                case PIPE:
                case PLUS:
                case 46:
                case 52:
                    {
                    alt13=1;
                    }
                    break;
            }

            switch (alt13) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:25: expr ( ',' expr )*
                    {
                    pushFollow(FOLLOW_expr_in_functionCall721);
                    expr45=expr();

                    state._fsp--;

                    stream_expr.add(expr45.getTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:30: ( ',' expr )*
                    loop12:
                    do {
                        int alt12=2;
                        switch ( input.LA(1) ) {
                        case COMMA:
                            {
                            alt12=1;
                            }
                            break;

                        }

                        switch (alt12) {
                    	case 1 :
                    	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:32: ',' expr
                    	    {
                    	    char_literal46=(Token)match(input,COMMA,FOLLOW_COMMA_in_functionCall725);  
                    	    stream_COMMA.add(char_literal46);


                    	    pushFollow(FOLLOW_expr_in_functionCall727);
                    	    expr47=expr();

                    	    state._fsp--;

                    	    stream_expr.add(expr47.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal48=(Token)match(input,RPAR,FOLLOW_RPAR_in_functionCall735);  
            stream_RPAR.add(char_literal48);


            // AST REWRITE
            // elements: expr, functionName, COMMA, expr
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 121:51: -> ^( FUNCTION functionName ( expr ( ',' expr )* )? )
            {
                // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:54: ^( FUNCTION functionName ( expr ( ',' expr )* )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(FUNCTION, "FUNCTION")
                , root_1);

                adaptor.addChild(root_1, stream_functionName.nextTree());

                // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:78: ( expr ( ',' expr )* )?
                if ( stream_expr.hasNext()||stream_COMMA.hasNext()||stream_expr.hasNext() ) {
                    adaptor.addChild(root_1, stream_expr.nextTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:121:85: ( ',' expr )*
                    while ( stream_COMMA.hasNext()||stream_expr.hasNext() ) {
                        adaptor.addChild(root_1, 
                        stream_COMMA.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                    }
                    stream_COMMA.reset();
                    stream_expr.reset();

                }
                stream_expr.reset();
                stream_COMMA.reset();
                stream_expr.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "functionCall"


    public static class spclFunctionCall_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "spclFunctionCall"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:124:1: spclFunctionCall : functionName '{' ( expr ( ',' expr )* )? '}' -> ^( FUNCTION functionName ( expr ( ',' expr )* )? ) ;
    public final XPathParser.spclFunctionCall_return spclFunctionCall() throws RecognitionException {
        XPathParser.spclFunctionCall_return retval = new XPathParser.spclFunctionCall_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal50=null;
        Token char_literal52=null;
        Token char_literal54=null;
        XPathParser.functionName_return functionName49 =null;

        XPathParser.expr_return expr51 =null;

        XPathParser.expr_return expr53 =null;


        CommonTree char_literal50_tree=null;
        CommonTree char_literal52_tree=null;
        CommonTree char_literal54_tree=null;
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_53=new RewriteRuleTokenStream(adaptor,"token 53");
        RewriteRuleTokenStream stream_54=new RewriteRuleTokenStream(adaptor,"token 54");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        RewriteRuleSubtreeStream stream_functionName=new RewriteRuleSubtreeStream(adaptor,"rule functionName");
        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:3: ( functionName '{' ( expr ( ',' expr )* )? '}' -> ^( FUNCTION functionName ( expr ( ',' expr )* )? ) )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:6: functionName '{' ( expr ( ',' expr )* )? '}'
            {
            pushFollow(FOLLOW_functionName_in_spclFunctionCall774);
            functionName49=functionName();

            state._fsp--;

            stream_functionName.add(functionName49.getTree());

            char_literal50=(Token)match(input,53,FOLLOW_53_in_spclFunctionCall776);  
            stream_53.add(char_literal50);


            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:23: ( expr ( ',' expr )* )?
            int alt15=2;
            switch ( input.LA(1) ) {
                case ABRPATH:
                case AT:
                case AxisName:
                case DOT:
                case DOTDOT:
                case LPAR:
                case Literal:
                case MINUS:
                case MUL:
                case NCName:
                case NodeType:
                case Number:
                case PATHSEP:
                case PIPE:
                case PLUS:
                case 46:
                case 52:
                    {
                    alt15=1;
                    }
                    break;
            }

            switch (alt15) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:25: expr ( ',' expr )*
                    {
                    pushFollow(FOLLOW_expr_in_spclFunctionCall780);
                    expr51=expr();

                    state._fsp--;

                    stream_expr.add(expr51.getTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:30: ( ',' expr )*
                    loop14:
                    do {
                        int alt14=2;
                        switch ( input.LA(1) ) {
                        case COMMA:
                            {
                            alt14=1;
                            }
                            break;

                        }

                        switch (alt14) {
                    	case 1 :
                    	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:32: ',' expr
                    	    {
                    	    char_literal52=(Token)match(input,COMMA,FOLLOW_COMMA_in_spclFunctionCall784);  
                    	    stream_COMMA.add(char_literal52);


                    	    pushFollow(FOLLOW_expr_in_spclFunctionCall786);
                    	    expr53=expr();

                    	    state._fsp--;

                    	    stream_expr.add(expr53.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal54=(Token)match(input,54,FOLLOW_54_in_spclFunctionCall794);  
            stream_54.add(char_literal54);


            // AST REWRITE
            // elements: expr, COMMA, expr, functionName
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 125:51: -> ^( FUNCTION functionName ( expr ( ',' expr )* )? )
            {
                // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:54: ^( FUNCTION functionName ( expr ( ',' expr )* )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(FUNCTION, "FUNCTION")
                , root_1);

                adaptor.addChild(root_1, stream_functionName.nextTree());

                // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:78: ( expr ( ',' expr )* )?
                if ( stream_expr.hasNext()||stream_COMMA.hasNext()||stream_expr.hasNext() ) {
                    adaptor.addChild(root_1, stream_expr.nextTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:125:85: ( ',' expr )*
                    while ( stream_expr.hasNext()||stream_COMMA.hasNext() ) {
                        adaptor.addChild(root_1, 
                        stream_COMMA.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                    }
                    stream_expr.reset();
                    stream_COMMA.reset();

                }
                stream_expr.reset();
                stream_COMMA.reset();
                stream_expr.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "spclFunctionCall"


    public static class unionExprNoRoot_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unionExprNoRoot"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:128:1: unionExprNoRoot : ( pathExprNoRoot ( '|' ^ unionExprNoRoot )? | '|' ^ unionExprNoRoot );
    public final XPathParser.unionExprNoRoot_return unionExprNoRoot() throws RecognitionException {
        XPathParser.unionExprNoRoot_return retval = new XPathParser.unionExprNoRoot_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal56=null;
        Token char_literal58=null;
        XPathParser.pathExprNoRoot_return pathExprNoRoot55 =null;

        XPathParser.unionExprNoRoot_return unionExprNoRoot57 =null;

        XPathParser.unionExprNoRoot_return unionExprNoRoot59 =null;


        CommonTree char_literal56_tree=null;
        CommonTree char_literal58_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:129:3: ( pathExprNoRoot ( '|' ^ unionExprNoRoot )? | '|' ^ unionExprNoRoot )
            int alt17=2;
            switch ( input.LA(1) ) {
            case ABRPATH:
            case AT:
            case AxisName:
            case DOT:
            case DOTDOT:
            case LPAR:
            case Literal:
            case MUL:
            case NCName:
            case NodeType:
            case Number:
            case PATHSEP:
            case 46:
            case 52:
                {
                alt17=1;
                }
                break;
            case PIPE:
                {
                alt17=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;

            }

            switch (alt17) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:129:6: pathExprNoRoot ( '|' ^ unionExprNoRoot )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_pathExprNoRoot_in_unionExprNoRoot834);
                    pathExprNoRoot55=pathExprNoRoot();

                    state._fsp--;

                    adaptor.addChild(root_0, pathExprNoRoot55.getTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:129:21: ( '|' ^ unionExprNoRoot )?
                    int alt16=2;
                    switch ( input.LA(1) ) {
                        case PIPE:
                            {
                            alt16=1;
                            }
                            break;
                    }

                    switch (alt16) {
                        case 1 :
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:129:22: '|' ^ unionExprNoRoot
                            {
                            char_literal56=(Token)match(input,PIPE,FOLLOW_PIPE_in_unionExprNoRoot837); 
                            char_literal56_tree = 
                            (CommonTree)adaptor.create(char_literal56)
                            ;
                            root_0 = (CommonTree)adaptor.becomeRoot(char_literal56_tree, root_0);


                            pushFollow(FOLLOW_unionExprNoRoot_in_unionExprNoRoot840);
                            unionExprNoRoot57=unionExprNoRoot();

                            state._fsp--;

                            adaptor.addChild(root_0, unionExprNoRoot57.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:131:6: '|' ^ unionExprNoRoot
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal58=(Token)match(input,PIPE,FOLLOW_PIPE_in_unionExprNoRoot850); 
                    char_literal58_tree = 
                    (CommonTree)adaptor.create(char_literal58)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(char_literal58_tree, root_0);


                    pushFollow(FOLLOW_unionExprNoRoot_in_unionExprNoRoot853);
                    unionExprNoRoot59=unionExprNoRoot();

                    state._fsp--;

                    adaptor.addChild(root_0, unionExprNoRoot59.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unionExprNoRoot"


    public static class pathExprNoRoot_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "pathExprNoRoot"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:134:1: pathExprNoRoot : ( locationPath | filterExpr ( ( '/' ^| '//' ^) relativeLocationPath )? );
    public final XPathParser.pathExprNoRoot_return pathExprNoRoot() throws RecognitionException {
        XPathParser.pathExprNoRoot_return retval = new XPathParser.pathExprNoRoot_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal62=null;
        Token string_literal63=null;
        XPathParser.locationPath_return locationPath60 =null;

        XPathParser.filterExpr_return filterExpr61 =null;

        XPathParser.relativeLocationPath_return relativeLocationPath64 =null;


        CommonTree char_literal62_tree=null;
        CommonTree string_literal63_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:135:3: ( locationPath | filterExpr ( ( '/' ^| '//' ^) relativeLocationPath )? )
            int alt20=2;
            switch ( input.LA(1) ) {
            case AxisName:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case ABRPATH:
                case CC:
                case COMMA:
                case GE:
                case LBRAC:
                case LE:
                case LESS:
                case MINUS:
                case MORE:
                case MUL:
                case PATHSEP:
                case PIPE:
                case PLUS:
                case RBRAC:
                case RPAR:
                case 45:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 54:
                    {
                    alt20=1;
                    }
                    break;
                case COLON:
                    {
                    switch ( input.LA(3) ) {
                    case MUL:
                        {
                        alt20=1;
                        }
                        break;
                    case AxisName:
                    case NCName:
                        {
                        switch ( input.LA(4) ) {
                        case EOF:
                        case ABRPATH:
                        case COMMA:
                        case GE:
                        case LBRAC:
                        case LE:
                        case LESS:
                        case MINUS:
                        case MORE:
                        case MUL:
                        case PATHSEP:
                        case PIPE:
                        case PLUS:
                        case RBRAC:
                        case RPAR:
                        case 45:
                        case 47:
                        case 48:
                        case 49:
                        case 50:
                        case 51:
                        case 54:
                            {
                            alt20=1;
                            }
                            break;
                        case LPAR:
                        case 53:
                            {
                            alt20=2;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 20, 6, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 5, input);

                        throw nvae;

                    }

                    }
                    break;
                case LPAR:
                case 53:
                    {
                    alt20=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 1, input);

                    throw nvae;

                }

                }
                break;
            case ABRPATH:
            case AT:
            case DOT:
            case DOTDOT:
            case MUL:
            case NodeType:
            case PATHSEP:
            case 52:
                {
                alt20=1;
                }
                break;
            case NCName:
                {
                switch ( input.LA(2) ) {
                case COLON:
                    {
                    switch ( input.LA(3) ) {
                    case MUL:
                        {
                        alt20=1;
                        }
                        break;
                    case AxisName:
                    case NCName:
                        {
                        switch ( input.LA(4) ) {
                        case EOF:
                        case ABRPATH:
                        case COMMA:
                        case GE:
                        case LBRAC:
                        case LE:
                        case LESS:
                        case MINUS:
                        case MORE:
                        case MUL:
                        case PATHSEP:
                        case PIPE:
                        case PLUS:
                        case RBRAC:
                        case RPAR:
                        case 45:
                        case 47:
                        case 48:
                        case 49:
                        case 50:
                        case 51:
                        case 54:
                            {
                            alt20=1;
                            }
                            break;
                        case LPAR:
                        case 53:
                            {
                            alt20=2;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 20, 6, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 5, input);

                        throw nvae;

                    }

                    }
                    break;
                case EOF:
                case ABRPATH:
                case COMMA:
                case GE:
                case LBRAC:
                case LE:
                case LESS:
                case MINUS:
                case MORE:
                case MUL:
                case PATHSEP:
                case PIPE:
                case PLUS:
                case RBRAC:
                case RPAR:
                case 45:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 54:
                    {
                    alt20=1;
                    }
                    break;
                case LPAR:
                case 53:
                    {
                    alt20=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 3, input);

                    throw nvae;

                }

                }
                break;
            case LPAR:
            case Literal:
            case Number:
            case 46:
                {
                alt20=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;

            }

            switch (alt20) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:135:6: locationPath
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_locationPath_in_pathExprNoRoot867);
                    locationPath60=locationPath();

                    state._fsp--;

                    adaptor.addChild(root_0, locationPath60.getTree());

                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:136:6: filterExpr ( ( '/' ^| '//' ^) relativeLocationPath )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_filterExpr_in_pathExprNoRoot874);
                    filterExpr61=filterExpr();

                    state._fsp--;

                    adaptor.addChild(root_0, filterExpr61.getTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:136:17: ( ( '/' ^| '//' ^) relativeLocationPath )?
                    int alt19=2;
                    switch ( input.LA(1) ) {
                        case ABRPATH:
                        case PATHSEP:
                            {
                            alt19=1;
                            }
                            break;
                    }

                    switch (alt19) {
                        case 1 :
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:136:18: ( '/' ^| '//' ^) relativeLocationPath
                            {
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:136:18: ( '/' ^| '//' ^)
                            int alt18=2;
                            switch ( input.LA(1) ) {
                            case PATHSEP:
                                {
                                alt18=1;
                                }
                                break;
                            case ABRPATH:
                                {
                                alt18=2;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 18, 0, input);

                                throw nvae;

                            }

                            switch (alt18) {
                                case 1 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:136:19: '/' ^
                                    {
                                    char_literal62=(Token)match(input,PATHSEP,FOLLOW_PATHSEP_in_pathExprNoRoot878); 
                                    char_literal62_tree = 
                                    (CommonTree)adaptor.create(char_literal62)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(char_literal62_tree, root_0);


                                    }
                                    break;
                                case 2 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:136:24: '//' ^
                                    {
                                    string_literal63=(Token)match(input,ABRPATH,FOLLOW_ABRPATH_in_pathExprNoRoot881); 
                                    string_literal63_tree = 
                                    (CommonTree)adaptor.create(string_literal63)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(string_literal63_tree, root_0);


                                    }
                                    break;

                            }


                            pushFollow(FOLLOW_relativeLocationPath_in_pathExprNoRoot885);
                            relativeLocationPath64=relativeLocationPath();

                            state._fsp--;

                            adaptor.addChild(root_0, relativeLocationPath64.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "pathExprNoRoot"


    public static class filterExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "filterExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:139:1: filterExpr : primaryExpr ( predicate )* ;
    public final XPathParser.filterExpr_return filterExpr() throws RecognitionException {
        XPathParser.filterExpr_return retval = new XPathParser.filterExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        XPathParser.primaryExpr_return primaryExpr65 =null;

        XPathParser.predicate_return predicate66 =null;



        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:140:3: ( primaryExpr ( predicate )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:140:6: primaryExpr ( predicate )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_primaryExpr_in_filterExpr901);
            primaryExpr65=primaryExpr();

            state._fsp--;

            adaptor.addChild(root_0, primaryExpr65.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:140:18: ( predicate )*
            loop21:
            do {
                int alt21=2;
                switch ( input.LA(1) ) {
                case LBRAC:
                    {
                    alt21=1;
                    }
                    break;

                }

                switch (alt21) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:140:18: predicate
            	    {
            	    pushFollow(FOLLOW_predicate_in_filterExpr903);
            	    predicate66=predicate();

            	    state._fsp--;

            	    adaptor.addChild(root_0, predicate66.getTree());

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "filterExpr"


    public static class orExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:143:1: orExpr : andExpr ( 'or' ^ andExpr )* ;
    public final XPathParser.orExpr_return orExpr() throws RecognitionException {
        XPathParser.orExpr_return retval = new XPathParser.orExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal68=null;
        XPathParser.andExpr_return andExpr67 =null;

        XPathParser.andExpr_return andExpr69 =null;


        CommonTree string_literal68_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:143:9: ( andExpr ( 'or' ^ andExpr )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:143:12: andExpr ( 'or' ^ andExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_andExpr_in_orExpr917);
            andExpr67=andExpr();

            state._fsp--;

            adaptor.addChild(root_0, andExpr67.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:143:20: ( 'or' ^ andExpr )*
            loop22:
            do {
                int alt22=2;
                switch ( input.LA(1) ) {
                case 51:
                    {
                    alt22=1;
                    }
                    break;

                }

                switch (alt22) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:143:21: 'or' ^ andExpr
            	    {
            	    string_literal68=(Token)match(input,51,FOLLOW_51_in_orExpr920); 
            	    string_literal68_tree = 
            	    (CommonTree)adaptor.create(string_literal68)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(string_literal68_tree, root_0);


            	    pushFollow(FOLLOW_andExpr_in_orExpr923);
            	    andExpr69=andExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, andExpr69.getTree());

            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orExpr"


    public static class andExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "andExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:146:1: andExpr : equalityExpr ( 'and' ^ equalityExpr )* ;
    public final XPathParser.andExpr_return andExpr() throws RecognitionException {
        XPathParser.andExpr_return retval = new XPathParser.andExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal71=null;
        XPathParser.equalityExpr_return equalityExpr70 =null;

        XPathParser.equalityExpr_return equalityExpr72 =null;


        CommonTree string_literal71_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:146:10: ( equalityExpr ( 'and' ^ equalityExpr )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:146:13: equalityExpr ( 'and' ^ equalityExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_equalityExpr_in_andExpr938);
            equalityExpr70=equalityExpr();

            state._fsp--;

            adaptor.addChild(root_0, equalityExpr70.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:146:26: ( 'and' ^ equalityExpr )*
            loop23:
            do {
                int alt23=2;
                switch ( input.LA(1) ) {
                case 48:
                    {
                    alt23=1;
                    }
                    break;

                }

                switch (alt23) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:146:27: 'and' ^ equalityExpr
            	    {
            	    string_literal71=(Token)match(input,48,FOLLOW_48_in_andExpr941); 
            	    string_literal71_tree = 
            	    (CommonTree)adaptor.create(string_literal71)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(string_literal71_tree, root_0);


            	    pushFollow(FOLLOW_equalityExpr_in_andExpr944);
            	    equalityExpr72=equalityExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, equalityExpr72.getTree());

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "andExpr"


    public static class equalityExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equalityExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:149:1: equalityExpr : relationalExpr ( ( '=' ^| '!=' ^) relationalExpr )* ;
    public final XPathParser.equalityExpr_return equalityExpr() throws RecognitionException {
        XPathParser.equalityExpr_return retval = new XPathParser.equalityExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal74=null;
        Token string_literal75=null;
        XPathParser.relationalExpr_return relationalExpr73 =null;

        XPathParser.relationalExpr_return relationalExpr76 =null;


        CommonTree char_literal74_tree=null;
        CommonTree string_literal75_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:3: ( relationalExpr ( ( '=' ^| '!=' ^) relationalExpr )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:6: relationalExpr ( ( '=' ^| '!=' ^) relationalExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_relationalExpr_in_equalityExpr960);
            relationalExpr73=relationalExpr();

            state._fsp--;

            adaptor.addChild(root_0, relationalExpr73.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:21: ( ( '=' ^| '!=' ^) relationalExpr )*
            loop25:
            do {
                int alt25=2;
                switch ( input.LA(1) ) {
                case 45:
                case 47:
                    {
                    alt25=1;
                    }
                    break;

                }

                switch (alt25) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:22: ( '=' ^| '!=' ^) relationalExpr
            	    {
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:22: ( '=' ^| '!=' ^)
            	    int alt24=2;
            	    switch ( input.LA(1) ) {
            	    case 47:
            	        {
            	        alt24=1;
            	        }
            	        break;
            	    case 45:
            	        {
            	        alt24=2;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 24, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt24) {
            	        case 1 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:24: '=' ^
            	            {
            	            char_literal74=(Token)match(input,47,FOLLOW_47_in_equalityExpr965); 
            	            char_literal74_tree = 
            	            (CommonTree)adaptor.create(char_literal74)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal74_tree, root_0);


            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:150:31: '!=' ^
            	            {
            	            string_literal75=(Token)match(input,45,FOLLOW_45_in_equalityExpr970); 
            	            string_literal75_tree = 
            	            (CommonTree)adaptor.create(string_literal75)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal75_tree, root_0);


            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_relationalExpr_in_equalityExpr975);
            	    relationalExpr76=relationalExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, relationalExpr76.getTree());

            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equalityExpr"


    public static class relationalExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relationalExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:153:1: relationalExpr : additiveExpr ( ( '<' ^| '>' ^| '<=' ^| '>=' ^) additiveExpr )* ;
    public final XPathParser.relationalExpr_return relationalExpr() throws RecognitionException {
        XPathParser.relationalExpr_return retval = new XPathParser.relationalExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal78=null;
        Token char_literal79=null;
        Token string_literal80=null;
        Token string_literal81=null;
        XPathParser.additiveExpr_return additiveExpr77 =null;

        XPathParser.additiveExpr_return additiveExpr82 =null;


        CommonTree char_literal78_tree=null;
        CommonTree char_literal79_tree=null;
        CommonTree string_literal80_tree=null;
        CommonTree string_literal81_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:3: ( additiveExpr ( ( '<' ^| '>' ^| '<=' ^| '>=' ^) additiveExpr )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:6: additiveExpr ( ( '<' ^| '>' ^| '<=' ^| '>=' ^) additiveExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_additiveExpr_in_relationalExpr991);
            additiveExpr77=additiveExpr();

            state._fsp--;

            adaptor.addChild(root_0, additiveExpr77.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:19: ( ( '<' ^| '>' ^| '<=' ^| '>=' ^) additiveExpr )*
            loop27:
            do {
                int alt27=2;
                switch ( input.LA(1) ) {
                case GE:
                case LE:
                case LESS:
                case MORE:
                    {
                    alt27=1;
                    }
                    break;

                }

                switch (alt27) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:20: ( '<' ^| '>' ^| '<=' ^| '>=' ^) additiveExpr
            	    {
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:20: ( '<' ^| '>' ^| '<=' ^| '>=' ^)
            	    int alt26=4;
            	    switch ( input.LA(1) ) {
            	    case LESS:
            	        {
            	        alt26=1;
            	        }
            	        break;
            	    case MORE:
            	        {
            	        alt26=2;
            	        }
            	        break;
            	    case LE:
            	        {
            	        alt26=3;
            	        }
            	        break;
            	    case GE:
            	        {
            	        alt26=4;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 26, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt26) {
            	        case 1 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:22: '<' ^
            	            {
            	            char_literal78=(Token)match(input,LESS,FOLLOW_LESS_in_relationalExpr996); 
            	            char_literal78_tree = 
            	            (CommonTree)adaptor.create(char_literal78)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal78_tree, root_0);


            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:29: '>' ^
            	            {
            	            char_literal79=(Token)match(input,MORE,FOLLOW_MORE_in_relationalExpr1001); 
            	            char_literal79_tree = 
            	            (CommonTree)adaptor.create(char_literal79)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal79_tree, root_0);


            	            }
            	            break;
            	        case 3 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:36: '<=' ^
            	            {
            	            string_literal80=(Token)match(input,LE,FOLLOW_LE_in_relationalExpr1006); 
            	            string_literal80_tree = 
            	            (CommonTree)adaptor.create(string_literal80)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal80_tree, root_0);


            	            }
            	            break;
            	        case 4 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:154:44: '>=' ^
            	            {
            	            string_literal81=(Token)match(input,GE,FOLLOW_GE_in_relationalExpr1011); 
            	            string_literal81_tree = 
            	            (CommonTree)adaptor.create(string_literal81)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal81_tree, root_0);


            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_additiveExpr_in_relationalExpr1016);
            	    additiveExpr82=additiveExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, additiveExpr82.getTree());

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relationalExpr"


    public static class additiveExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "additiveExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:157:1: additiveExpr : multiplicativeExpr ( ( '+' ^| '-' ^) multiplicativeExpr )* ;
    public final XPathParser.additiveExpr_return additiveExpr() throws RecognitionException {
        XPathParser.additiveExpr_return retval = new XPathParser.additiveExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal84=null;
        Token char_literal85=null;
        XPathParser.multiplicativeExpr_return multiplicativeExpr83 =null;

        XPathParser.multiplicativeExpr_return multiplicativeExpr86 =null;


        CommonTree char_literal84_tree=null;
        CommonTree char_literal85_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:3: ( multiplicativeExpr ( ( '+' ^| '-' ^) multiplicativeExpr )* )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:6: multiplicativeExpr ( ( '+' ^| '-' ^) multiplicativeExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multiplicativeExpr_in_additiveExpr1032);
            multiplicativeExpr83=multiplicativeExpr();

            state._fsp--;

            adaptor.addChild(root_0, multiplicativeExpr83.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:25: ( ( '+' ^| '-' ^) multiplicativeExpr )*
            loop29:
            do {
                int alt29=2;
                switch ( input.LA(1) ) {
                case MINUS:
                case PLUS:
                    {
                    alt29=1;
                    }
                    break;

                }

                switch (alt29) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:26: ( '+' ^| '-' ^) multiplicativeExpr
            	    {
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:26: ( '+' ^| '-' ^)
            	    int alt28=2;
            	    switch ( input.LA(1) ) {
            	    case PLUS:
            	        {
            	        alt28=1;
            	        }
            	        break;
            	    case MINUS:
            	        {
            	        alt28=2;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 28, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt28) {
            	        case 1 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:28: '+' ^
            	            {
            	            char_literal84=(Token)match(input,PLUS,FOLLOW_PLUS_in_additiveExpr1037); 
            	            char_literal84_tree = 
            	            (CommonTree)adaptor.create(char_literal84)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal84_tree, root_0);


            	            }
            	            break;
            	        case 2 :
            	            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:158:35: '-' ^
            	            {
            	            char_literal85=(Token)match(input,MINUS,FOLLOW_MINUS_in_additiveExpr1042); 
            	            char_literal85_tree = 
            	            (CommonTree)adaptor.create(char_literal85)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal85_tree, root_0);


            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_multiplicativeExpr_in_additiveExpr1047);
            	    multiplicativeExpr86=multiplicativeExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multiplicativeExpr86.getTree());

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "additiveExpr"


    public static class multiplicativeExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multiplicativeExpr"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:161:1: multiplicativeExpr : ( unaryExprNoRoot ( ( '*' ^| 'div' ^| 'mod' ^) multiplicativeExpr )? | '/' ( ( 'div' ^| 'mod' ^) multiplicativeExpr )? );
    public final XPathParser.multiplicativeExpr_return multiplicativeExpr() throws RecognitionException {
        XPathParser.multiplicativeExpr_return retval = new XPathParser.multiplicativeExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal88=null;
        Token string_literal89=null;
        Token string_literal90=null;
        Token char_literal92=null;
        Token string_literal93=null;
        Token string_literal94=null;
        XPathParser.unaryExprNoRoot_return unaryExprNoRoot87 =null;

        XPathParser.multiplicativeExpr_return multiplicativeExpr91 =null;

        XPathParser.multiplicativeExpr_return multiplicativeExpr95 =null;


        CommonTree char_literal88_tree=null;
        CommonTree string_literal89_tree=null;
        CommonTree string_literal90_tree=null;
        CommonTree char_literal92_tree=null;
        CommonTree string_literal93_tree=null;
        CommonTree string_literal94_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:3: ( unaryExprNoRoot ( ( '*' ^| 'div' ^| 'mod' ^) multiplicativeExpr )? | '/' ( ( 'div' ^| 'mod' ^) multiplicativeExpr )? )
            int alt34=2;
            switch ( input.LA(1) ) {
            case ABRPATH:
            case AT:
            case AxisName:
            case DOT:
            case DOTDOT:
            case LPAR:
            case Literal:
            case MINUS:
            case MUL:
            case NCName:
            case NodeType:
            case Number:
            case PIPE:
            case PLUS:
            case 46:
            case 52:
                {
                alt34=1;
                }
                break;
            case PATHSEP:
                {
                switch ( input.LA(2) ) {
                case AT:
                case AxisName:
                case DOT:
                case DOTDOT:
                case MUL:
                case NCName:
                case NodeType:
                case 52:
                    {
                    alt34=1;
                    }
                    break;
                case EOF:
                case COMMA:
                case GE:
                case LE:
                case LESS:
                case MINUS:
                case MORE:
                case PLUS:
                case RBRAC:
                case RPAR:
                case 45:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 54:
                    {
                    alt34=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 34, 2, input);

                    throw nvae;

                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;

            }

            switch (alt34) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:6: unaryExprNoRoot ( ( '*' ^| 'div' ^| 'mod' ^) multiplicativeExpr )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_unaryExprNoRoot_in_multiplicativeExpr1063);
                    unaryExprNoRoot87=unaryExprNoRoot();

                    state._fsp--;

                    adaptor.addChild(root_0, unaryExprNoRoot87.getTree());

                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:22: ( ( '*' ^| 'div' ^| 'mod' ^) multiplicativeExpr )?
                    int alt31=2;
                    switch ( input.LA(1) ) {
                        case MUL:
                        case 49:
                        case 50:
                            {
                            alt31=1;
                            }
                            break;
                    }

                    switch (alt31) {
                        case 1 :
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:23: ( '*' ^| 'div' ^| 'mod' ^) multiplicativeExpr
                            {
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:23: ( '*' ^| 'div' ^| 'mod' ^)
                            int alt30=3;
                            switch ( input.LA(1) ) {
                            case MUL:
                                {
                                alt30=1;
                                }
                                break;
                            case 49:
                                {
                                alt30=2;
                                }
                                break;
                            case 50:
                                {
                                alt30=3;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 30, 0, input);

                                throw nvae;

                            }

                            switch (alt30) {
                                case 1 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:25: '*' ^
                                    {
                                    char_literal88=(Token)match(input,MUL,FOLLOW_MUL_in_multiplicativeExpr1068); 
                                    char_literal88_tree = 
                                    (CommonTree)adaptor.create(char_literal88)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(char_literal88_tree, root_0);


                                    }
                                    break;
                                case 2 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:32: 'div' ^
                                    {
                                    string_literal89=(Token)match(input,49,FOLLOW_49_in_multiplicativeExpr1073); 
                                    string_literal89_tree = 
                                    (CommonTree)adaptor.create(string_literal89)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(string_literal89_tree, root_0);


                                    }
                                    break;
                                case 3 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:162:41: 'mod' ^
                                    {
                                    string_literal90=(Token)match(input,50,FOLLOW_50_in_multiplicativeExpr1078); 
                                    string_literal90_tree = 
                                    (CommonTree)adaptor.create(string_literal90)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(string_literal90_tree, root_0);


                                    }
                                    break;

                            }


                            pushFollow(FOLLOW_multiplicativeExpr_in_multiplicativeExpr1083);
                            multiplicativeExpr91=multiplicativeExpr();

                            state._fsp--;

                            adaptor.addChild(root_0, multiplicativeExpr91.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:163:6: '/' ( ( 'div' ^| 'mod' ^) multiplicativeExpr )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal92=(Token)match(input,PATHSEP,FOLLOW_PATHSEP_in_multiplicativeExpr1092); 
                    char_literal92_tree = 
                    (CommonTree)adaptor.create(char_literal92)
                    ;
                    adaptor.addChild(root_0, char_literal92_tree);


                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:163:10: ( ( 'div' ^| 'mod' ^) multiplicativeExpr )?
                    int alt33=2;
                    switch ( input.LA(1) ) {
                        case 49:
                        case 50:
                            {
                            alt33=1;
                            }
                            break;
                    }

                    switch (alt33) {
                        case 1 :
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:163:11: ( 'div' ^| 'mod' ^) multiplicativeExpr
                            {
                            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:163:11: ( 'div' ^| 'mod' ^)
                            int alt32=2;
                            switch ( input.LA(1) ) {
                            case 49:
                                {
                                alt32=1;
                                }
                                break;
                            case 50:
                                {
                                alt32=2;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 32, 0, input);

                                throw nvae;

                            }

                            switch (alt32) {
                                case 1 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:163:13: 'div' ^
                                    {
                                    string_literal93=(Token)match(input,49,FOLLOW_49_in_multiplicativeExpr1097); 
                                    string_literal93_tree = 
                                    (CommonTree)adaptor.create(string_literal93)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(string_literal93_tree, root_0);


                                    }
                                    break;
                                case 2 :
                                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:163:22: 'mod' ^
                                    {
                                    string_literal94=(Token)match(input,50,FOLLOW_50_in_multiplicativeExpr1102); 
                                    string_literal94_tree = 
                                    (CommonTree)adaptor.create(string_literal94)
                                    ;
                                    root_0 = (CommonTree)adaptor.becomeRoot(string_literal94_tree, root_0);


                                    }
                                    break;

                            }


                            pushFollow(FOLLOW_multiplicativeExpr_in_multiplicativeExpr1107);
                            multiplicativeExpr95=multiplicativeExpr();

                            state._fsp--;

                            adaptor.addChild(root_0, multiplicativeExpr95.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multiplicativeExpr"


    public static class unaryExprNoRoot_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unaryExprNoRoot"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:166:1: unaryExprNoRoot : ( '+' !| negation ^)* unionExprNoRoot ;
    public final XPathParser.unaryExprNoRoot_return unaryExprNoRoot() throws RecognitionException {
        XPathParser.unaryExprNoRoot_return retval = new XPathParser.unaryExprNoRoot_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal96=null;
        XPathParser.negation_return negation97 =null;

        XPathParser.unionExprNoRoot_return unionExprNoRoot98 =null;


        CommonTree char_literal96_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:167:3: ( ( '+' !| negation ^)* unionExprNoRoot )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:167:6: ( '+' !| negation ^)* unionExprNoRoot
            {
            root_0 = (CommonTree)adaptor.nil();


            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:167:6: ( '+' !| negation ^)*
            loop35:
            do {
                int alt35=3;
                switch ( input.LA(1) ) {
                case PLUS:
                    {
                    alt35=1;
                    }
                    break;
                case MINUS:
                    {
                    alt35=2;
                    }
                    break;

                }

                switch (alt35) {
            	case 1 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:167:7: '+' !
            	    {
            	    char_literal96=(Token)match(input,PLUS,FOLLOW_PLUS_in_unaryExprNoRoot1124); 

            	    }
            	    break;
            	case 2 :
            	    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:167:14: negation ^
            	    {
            	    pushFollow(FOLLOW_negation_in_unaryExprNoRoot1129);
            	    negation97=negation();

            	    state._fsp--;

            	    root_0 = (CommonTree)adaptor.becomeRoot(negation97.getTree(), root_0);

            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            pushFollow(FOLLOW_unionExprNoRoot_in_unaryExprNoRoot1134);
            unionExprNoRoot98=unionExprNoRoot();

            state._fsp--;

            adaptor.addChild(root_0, unionExprNoRoot98.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unaryExprNoRoot"


    public static class negation_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "negation"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:170:1: negation : '-' -> NEGETION ;
    public final XPathParser.negation_return negation() throws RecognitionException {
        XPathParser.negation_return retval = new XPathParser.negation_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal99=null;

        CommonTree char_literal99_tree=null;
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:170:10: ( '-' -> NEGETION )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:171:3: '-'
            {
            char_literal99=(Token)match(input,MINUS,FOLLOW_MINUS_in_negation1147);  
            stream_MINUS.add(char_literal99);


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 171:7: -> NEGETION
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NEGETION, "NEGETION")
                );

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "negation"


    public static class qName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "qName"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:174:1: qName : nCName ( ':' nCName )? ;
    public final XPathParser.qName_return qName() throws RecognitionException {
        XPathParser.qName_return retval = new XPathParser.qName_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal101=null;
        XPathParser.nCName_return nCName100 =null;

        XPathParser.nCName_return nCName102 =null;


        CommonTree char_literal101_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:174:8: ( nCName ( ':' nCName )? )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:174:11: nCName ( ':' nCName )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_nCName_in_qName1164);
            nCName100=nCName();

            state._fsp--;

            adaptor.addChild(root_0, nCName100.getTree());

            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:174:18: ( ':' nCName )?
            int alt36=2;
            switch ( input.LA(1) ) {
                case COLON:
                    {
                    alt36=1;
                    }
                    break;
            }

            switch (alt36) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:174:19: ':' nCName
                    {
                    char_literal101=(Token)match(input,COLON,FOLLOW_COLON_in_qName1167); 
                    char_literal101_tree = 
                    (CommonTree)adaptor.create(char_literal101)
                    ;
                    adaptor.addChild(root_0, char_literal101_tree);


                    pushFollow(FOLLOW_nCName_in_qName1169);
                    nCName102=nCName();

                    state._fsp--;

                    adaptor.addChild(root_0, nCName102.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "qName"


    public static class functionName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "functionName"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:177:1: functionName : qName ;
    public final XPathParser.functionName_return functionName() throws RecognitionException {
        XPathParser.functionName_return retval = new XPathParser.functionName_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        XPathParser.qName_return qName103 =null;



        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:178:3: ( qName )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:178:6: qName
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_qName_in_functionName1185);
            qName103=qName();

            state._fsp--;

            adaptor.addChild(root_0, qName103.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "functionName"


    public static class variableReference_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "variableReference"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:181:1: variableReference : '$' qName ;
    public final XPathParser.variableReference_return variableReference() throws RecognitionException {
        XPathParser.variableReference_return retval = new XPathParser.variableReference_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal104=null;
        XPathParser.qName_return qName105 =null;


        CommonTree char_literal104_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:182:3: ( '$' qName )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:182:6: '$' qName
            {
            root_0 = (CommonTree)adaptor.nil();


            char_literal104=(Token)match(input,46,FOLLOW_46_in_variableReference1201); 
            char_literal104_tree = 
            (CommonTree)adaptor.create(char_literal104)
            ;
            adaptor.addChild(root_0, char_literal104_tree);


            pushFollow(FOLLOW_qName_in_variableReference1203);
            qName105=qName();

            state._fsp--;

            adaptor.addChild(root_0, qName105.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "variableReference"


    public static class nameTest_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "nameTest"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:185:1: nameTest : ( '*' | nCName ':' '*' | qName );
    public final XPathParser.nameTest_return nameTest() throws RecognitionException {
        XPathParser.nameTest_return retval = new XPathParser.nameTest_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal106=null;
        Token char_literal108=null;
        Token char_literal109=null;
        XPathParser.nCName_return nCName107 =null;

        XPathParser.qName_return qName110 =null;


        CommonTree char_literal106_tree=null;
        CommonTree char_literal108_tree=null;
        CommonTree char_literal109_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:185:9: ( '*' | nCName ':' '*' | qName )
            int alt37=3;
            switch ( input.LA(1) ) {
            case MUL:
                {
                alt37=1;
                }
                break;
            case AxisName:
            case NCName:
                {
                switch ( input.LA(2) ) {
                case COLON:
                    {
                    switch ( input.LA(3) ) {
                    case MUL:
                        {
                        alt37=2;
                        }
                        break;
                    case AxisName:
                    case NCName:
                        {
                        alt37=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 37, 3, input);

                        throw nvae;

                    }

                    }
                    break;
                case EOF:
                case ABRPATH:
                case COMMA:
                case GE:
                case LBRAC:
                case LE:
                case LESS:
                case MINUS:
                case MORE:
                case MUL:
                case PATHSEP:
                case PIPE:
                case PLUS:
                case RBRAC:
                case RPAR:
                case 45:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 54:
                    {
                    alt37=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 37, 2, input);

                    throw nvae;

                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;

            }

            switch (alt37) {
                case 1 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:185:12: '*'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal106=(Token)match(input,MUL,FOLLOW_MUL_in_nameTest1214); 
                    char_literal106_tree = 
                    (CommonTree)adaptor.create(char_literal106)
                    ;
                    adaptor.addChild(root_0, char_literal106_tree);


                    }
                    break;
                case 2 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:186:6: nCName ':' '*'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_nCName_in_nameTest1221);
                    nCName107=nCName();

                    state._fsp--;

                    adaptor.addChild(root_0, nCName107.getTree());

                    char_literal108=(Token)match(input,COLON,FOLLOW_COLON_in_nameTest1223); 
                    char_literal108_tree = 
                    (CommonTree)adaptor.create(char_literal108)
                    ;
                    adaptor.addChild(root_0, char_literal108_tree);


                    char_literal109=(Token)match(input,MUL,FOLLOW_MUL_in_nameTest1225); 
                    char_literal109_tree = 
                    (CommonTree)adaptor.create(char_literal109)
                    ;
                    adaptor.addChild(root_0, char_literal109_tree);


                    }
                    break;
                case 3 :
                    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:187:6: qName
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_qName_in_nameTest1232);
                    qName110=qName();

                    state._fsp--;

                    adaptor.addChild(root_0, qName110.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "nameTest"


    public static class nCName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "nCName"
    // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:190:1: nCName : ( NCName | AxisName );
    public final XPathParser.nCName_return nCName() throws RecognitionException {
        XPathParser.nCName_return retval = new XPathParser.nCName_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set111=null;

        CommonTree set111_tree=null;

        try {
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:190:9: ( NCName | AxisName )
            // com\\mmpnc\\xml\\xpath\\grammar\\XPath.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set111=(Token)input.LT(1);

            if ( input.LA(1)==AxisName||input.LA(1)==NCName ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set111)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "nCName"

    // Delegated rules


 

    public static final BitSet FOLLOW_expr_in_xpath360 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_xpath362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relativeLocationPath_in_locationPath375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_absoluteLocationPathNoroot_in_locationPath382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PATHSEP_in_absoluteLocationPathNoroot396 = new BitSet(new long[]{0x001000010A0060C0L});
    public static final BitSet FOLLOW_relativeLocationPath_in_absoluteLocationPathNoroot398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ABRPATH_in_absoluteLocationPathNoroot413 = new BitSet(new long[]{0x001000010A0060C0L});
    public static final BitSet FOLLOW_relativeLocationPath_in_absoluteLocationPathNoroot415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_step_in_relativeLocationPath437 = new BitSet(new long[]{0x0000000800000012L});
    public static final BitSet FOLLOW_relativepath_in_relativeLocationPath440 = new BitSet(new long[]{0x001000010A0060C0L});
    public static final BitSet FOLLOW_step_in_relativeLocationPath443 = new BitSet(new long[]{0x0000000800000012L});
    public static final BitSet FOLLOW_PATHSEP_in_relativepath454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ABRPATH_in_relativepath461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_axisSpecifier_in_step478 = new BitSet(new long[]{0x001000010A000080L});
    public static final BitSet FOLLOW_nodeTest_in_step480 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_predicate_in_step482 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_abbreviatedStep_in_step490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AxisName_in_axisSpecifier504 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_CC_in_axisSpecifier506 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AT_in_axisSpecifier515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nameTest_in_nodeTest532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NodeType_in_nodeTest548 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_LPAR_in_nodeTest550 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAR_in_nodeTest552 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_nodeTest568 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_LPAR_in_nodeTest570 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_Literal_in_nodeTest572 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAR_in_nodeTest574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LBRAC_in_predicate599 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_expr_in_predicate601 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_RBRAC_in_predicate603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_abbreviatedStep618 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOTDOT_in_abbreviatedStep629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_orExpr_in_expr646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_variableReference_in_primaryExpr660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAR_in_primaryExpr667 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_expr_in_primaryExpr669 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAR_in_primaryExpr671 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Literal_in_primaryExpr678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Number_in_primaryExpr685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_primaryExpr694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_spclFunctionCall_in_primaryExpr701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionName_in_functionCall715 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_LPAR_in_functionCall717 = new BitSet(new long[]{0x0010483B0AE060D0L});
    public static final BitSet FOLLOW_expr_in_functionCall721 = new BitSet(new long[]{0x0000080000000400L});
    public static final BitSet FOLLOW_COMMA_in_functionCall725 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_expr_in_functionCall727 = new BitSet(new long[]{0x0000080000000400L});
    public static final BitSet FOLLOW_RPAR_in_functionCall735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionName_in_spclFunctionCall774 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_53_in_spclFunctionCall776 = new BitSet(new long[]{0x0050403B0AE060D0L});
    public static final BitSet FOLLOW_expr_in_spclFunctionCall780 = new BitSet(new long[]{0x0040000000000400L});
    public static final BitSet FOLLOW_COMMA_in_spclFunctionCall784 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_expr_in_spclFunctionCall786 = new BitSet(new long[]{0x0040000000000400L});
    public static final BitSet FOLLOW_54_in_spclFunctionCall794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pathExprNoRoot_in_unionExprNoRoot834 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_PIPE_in_unionExprNoRoot837 = new BitSet(new long[]{0x0010401B0A6060D0L});
    public static final BitSet FOLLOW_unionExprNoRoot_in_unionExprNoRoot840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PIPE_in_unionExprNoRoot850 = new BitSet(new long[]{0x0010401B0A6060D0L});
    public static final BitSet FOLLOW_unionExprNoRoot_in_unionExprNoRoot853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_locationPath_in_pathExprNoRoot867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_filterExpr_in_pathExprNoRoot874 = new BitSet(new long[]{0x0000000800000012L});
    public static final BitSet FOLLOW_PATHSEP_in_pathExprNoRoot878 = new BitSet(new long[]{0x001000010A0060C0L});
    public static final BitSet FOLLOW_ABRPATH_in_pathExprNoRoot881 = new BitSet(new long[]{0x001000010A0060C0L});
    public static final BitSet FOLLOW_relativeLocationPath_in_pathExprNoRoot885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primaryExpr_in_filterExpr901 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_predicate_in_filterExpr903 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_andExpr_in_orExpr917 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_51_in_orExpr920 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_andExpr_in_orExpr923 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_equalityExpr_in_andExpr938 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_48_in_andExpr941 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_equalityExpr_in_andExpr944 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_relationalExpr_in_equalityExpr960 = new BitSet(new long[]{0x0000A00000000002L});
    public static final BitSet FOLLOW_47_in_equalityExpr965 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_45_in_equalityExpr970 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_relationalExpr_in_equalityExpr975 = new BitSet(new long[]{0x0000A00000000002L});
    public static final BitSet FOLLOW_additiveExpr_in_relationalExpr991 = new BitSet(new long[]{0x00000000011A0002L});
    public static final BitSet FOLLOW_LESS_in_relationalExpr996 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_MORE_in_relationalExpr1001 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_LE_in_relationalExpr1006 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_GE_in_relationalExpr1011 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_additiveExpr_in_relationalExpr1016 = new BitSet(new long[]{0x00000000011A0002L});
    public static final BitSet FOLLOW_multiplicativeExpr_in_additiveExpr1032 = new BitSet(new long[]{0x0000002000800002L});
    public static final BitSet FOLLOW_PLUS_in_additiveExpr1037 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_MINUS_in_additiveExpr1042 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_multiplicativeExpr_in_additiveExpr1047 = new BitSet(new long[]{0x0000002000800002L});
    public static final BitSet FOLLOW_unaryExprNoRoot_in_multiplicativeExpr1063 = new BitSet(new long[]{0x0006000002000002L});
    public static final BitSet FOLLOW_MUL_in_multiplicativeExpr1068 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_49_in_multiplicativeExpr1073 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_50_in_multiplicativeExpr1078 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_multiplicativeExpr_in_multiplicativeExpr1083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PATHSEP_in_multiplicativeExpr1092 = new BitSet(new long[]{0x0006000000000002L});
    public static final BitSet FOLLOW_49_in_multiplicativeExpr1097 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_50_in_multiplicativeExpr1102 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_multiplicativeExpr_in_multiplicativeExpr1107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLUS_in_unaryExprNoRoot1124 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_negation_in_unaryExprNoRoot1129 = new BitSet(new long[]{0x0010403B0AE060D0L});
    public static final BitSet FOLLOW_unionExprNoRoot_in_unaryExprNoRoot1134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_negation1147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nCName_in_qName1164 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_COLON_in_qName1167 = new BitSet(new long[]{0x0000000008000080L});
    public static final BitSet FOLLOW_nCName_in_qName1169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_qName_in_functionName1185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_variableReference1201 = new BitSet(new long[]{0x0000000008000080L});
    public static final BitSet FOLLOW_qName_in_variableReference1203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MUL_in_nameTest1214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nCName_in_nameTest1221 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_COLON_in_nameTest1223 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_MUL_in_nameTest1225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_qName_in_nameTest1232 = new BitSet(new long[]{0x0000000000000002L});

}