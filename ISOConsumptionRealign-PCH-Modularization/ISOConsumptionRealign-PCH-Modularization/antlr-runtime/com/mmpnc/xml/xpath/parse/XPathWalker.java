// $ANTLR 3.4 D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g 2015-05-18 10:09:57

package com.mmpnc.xml.xpath.parse;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator.*;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class XPathWalker extends TreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABRPATH", "APOS", "AT", "AxisName", "CC", "COLON", "COMMA", "CURRENT", "CURRENTNODE", "DOT", "DOTDOT", "Digits", "FUNCTION", "GE", "LBRAC", "LE", "LESS", "LPAR", "Literal", "MINUS", "MORE", "MUL", "NAMETEST", "NCName", "NCNameChar", "NCNameStartChar", "NEGETION", "NODETEST", "NodeType", "Number", "PATH", "PATHSEP", "PIPE", "PLUS", "PRIORNODE", "QUOT", "RBRAC", "RELATIVEFROMROOT", "RELATIVEPATH", "RPAR", "Whitespace", "'!='", "'$'", "'='", "'and'", "'div'", "'mod'", "'or'", "'processing-instruction'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int ABRPATH=4;
    public static final int APOS=5;
    public static final int AT=6;
    public static final int AxisName=7;
    public static final int CC=8;
    public static final int COLON=9;
    public static final int COMMA=10;
    public static final int CURRENT=11;
    public static final int CURRENTNODE=12;
    public static final int DOT=13;
    public static final int DOTDOT=14;
    public static final int Digits=15;
    public static final int FUNCTION=16;
    public static final int GE=17;
    public static final int LBRAC=18;
    public static final int LE=19;
    public static final int LESS=20;
    public static final int LPAR=21;
    public static final int Literal=22;
    public static final int MINUS=23;
    public static final int MORE=24;
    public static final int MUL=25;
    public static final int NAMETEST=26;
    public static final int NCName=27;
    public static final int NCNameChar=28;
    public static final int NCNameStartChar=29;
    public static final int NEGETION=30;
    public static final int NODETEST=31;
    public static final int NodeType=32;
    public static final int Number=33;
    public static final int PATH=34;
    public static final int PATHSEP=35;
    public static final int PIPE=36;
    public static final int PLUS=37;
    public static final int PRIORNODE=38;
    public static final int QUOT=39;
    public static final int RBRAC=40;
    public static final int RELATIVEFROMROOT=41;
    public static final int RELATIVEPATH=42;
    public static final int RPAR=43;
    public static final int Whitespace=44;

    // delegates
    public TreeParser[] getDelegates() {
        return new TreeParser[] {};
    }

    // delegators


    public XPathWalker(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public XPathWalker(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return XPathWalker.tokenNames; }
    public String getGrammarFileName() { return "D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g"; }


    private Context context = null;



    // $ANTLR start "xpath"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:21:1: xpath[Context context] returns [ Evaluator.Type type] :e1= expr ;
    public final Evaluator.Type xpath(Context context) throws RecognitionException {
        Evaluator.Type type = null;


        Evaluator e1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:22:3: (e1= expr )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:22:5: e1= expr
            {
             this.context = context;

            pushFollow(FOLLOW_expr_in_xpath69);
            e1=expr();

            state._fsp--;


             
                    //System.out.println("xpath called");
                    Evaluator eval = e1;
                    eval.setContext(context);
                    type = (Evaluator.Type)eval.evaluate();
                  

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return type;
    }
    // $ANTLR end "xpath"



    // $ANTLR start "expr"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:32:1: expr returns [Evaluator eval] : ( ^( '+' e1= expr e2= expr ) | ^( '-' e1= expr e2= expr ) | ^( '*' e1= expr e2= expr ) | ^( 'div' e1= expr e2= expr ) | ^( 'mod' e1= expr e2= expr ) | ^( 'and' e1= expr e2= expr ) | ^( 'or' e1= expr e2= expr ) | ^( '<' e1= expr e2= expr ) | ^( '>' e1= expr e2= expr ) | ^( '<=' e1= expr e2= expr ) | ^( '>=' e1= expr e2= expr ) | ^( '=' e1= expr e2= expr ) | ^( '!=' e1= expr e2= expr ) | ^( '|' e1= expr e2= expr ) | ^( NEGETION e1= expr ) |l1= locationPath | function | Literal | Number |s1= step );
    public final Evaluator expr() throws RecognitionException {
        Evaluator eval = null;


        CommonTree Literal2=null;
        CommonTree Number3=null;
        Evaluator e1 =null;

        Evaluator e2 =null;

        Evaluator l1 =null;

        Evaluator s1 =null;

        Evaluator function1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:32:31: ( ^( '+' e1= expr e2= expr ) | ^( '-' e1= expr e2= expr ) | ^( '*' e1= expr e2= expr ) | ^( 'div' e1= expr e2= expr ) | ^( 'mod' e1= expr e2= expr ) | ^( 'and' e1= expr e2= expr ) | ^( 'or' e1= expr e2= expr ) | ^( '<' e1= expr e2= expr ) | ^( '>' e1= expr e2= expr ) | ^( '<=' e1= expr e2= expr ) | ^( '>=' e1= expr e2= expr ) | ^( '=' e1= expr e2= expr ) | ^( '!=' e1= expr e2= expr ) | ^( '|' e1= expr e2= expr ) | ^( NEGETION e1= expr ) |l1= locationPath | function | Literal | Number |s1= step )
            int alt1=20;
            switch ( input.LA(1) ) {
            case PLUS:
                {
                alt1=1;
                }
                break;
            case MINUS:
                {
                alt1=2;
                }
                break;
            case MUL:
                {
                alt1=3;
                }
                break;
            case 49:
                {
                alt1=4;
                }
                break;
            case 50:
                {
                alt1=5;
                }
                break;
            case 48:
                {
                alt1=6;
                }
                break;
            case 51:
                {
                alt1=7;
                }
                break;
            case LESS:
                {
                alt1=8;
                }
                break;
            case MORE:
                {
                alt1=9;
                }
                break;
            case LE:
                {
                alt1=10;
                }
                break;
            case GE:
                {
                alt1=11;
                }
                break;
            case 47:
                {
                alt1=12;
                }
                break;
            case 45:
                {
                alt1=13;
                }
                break;
            case PIPE:
                {
                alt1=14;
                }
                break;
            case NEGETION:
                {
                alt1=15;
                }
                break;
            case CURRENT:
            case PATH:
            case RELATIVEFROMROOT:
            case RELATIVEPATH:
                {
                alt1=16;
                }
                break;
            case FUNCTION:
                {
                alt1=17;
                }
                break;
            case Literal:
                {
                alt1=18;
                }
                break;
            case Number:
                {
                alt1=19;
                }
                break;
            case AT:
            case AxisName:
            case CURRENTNODE:
            case NAMETEST:
            case NODETEST:
            case PRIORNODE:
                {
                alt1=20;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:33:7: ^( '+' e1= expr e2= expr )
                    {
                    match(input,PLUS,FOLLOW_PLUS_in_expr101); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr105);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr109);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new PlusEvaluator(e1 , e2);

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:34:7: ^( '-' e1= expr e2= expr )
                    {
                    match(input,MINUS,FOLLOW_MINUS_in_expr122); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr126);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr130);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new MinusEvaluator(e1 , e2);

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:35:7: ^( '*' e1= expr e2= expr )
                    {
                    match(input,MUL,FOLLOW_MUL_in_expr143); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr147);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr151);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new MultiplyEvaluator(e1 , e2);

                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:36:7: ^( 'div' e1= expr e2= expr )
                    {
                    match(input,49,FOLLOW_49_in_expr164); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr168);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr172);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new DivideEvaluator(e1 , e2);

                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:37:7: ^( 'mod' e1= expr e2= expr )
                    {
                    match(input,50,FOLLOW_50_in_expr183); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr187);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr191);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new ModEvaluator(e1 , e2);

                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:38:7: ^( 'and' e1= expr e2= expr )
                    {
                    match(input,48,FOLLOW_48_in_expr202); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr206);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr210);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new AndEvaluator(e1 , e2);

                    }
                    break;
                case 7 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:39:7: ^( 'or' e1= expr e2= expr )
                    {
                    match(input,51,FOLLOW_51_in_expr221); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr225);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr229);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new OrEvaluator(e1 , e2);

                    }
                    break;
                case 8 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:40:7: ^( '<' e1= expr e2= expr )
                    {
                    match(input,LESS,FOLLOW_LESS_in_expr241); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr245);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr249);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new LessThanEvaluator(e1 , e2);

                    }
                    break;
                case 9 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:41:7: ^( '>' e1= expr e2= expr )
                    {
                    match(input,MORE,FOLLOW_MORE_in_expr262); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr266);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr270);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new GreaterThanEvaluator(e1 , e2);

                    }
                    break;
                case 10 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:42:7: ^( '<=' e1= expr e2= expr )
                    {
                    match(input,LE,FOLLOW_LE_in_expr283); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr287);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr291);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new LessThanEqualsEvaluator(e1 , e2);

                    }
                    break;
                case 11 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:43:7: ^( '>=' e1= expr e2= expr )
                    {
                    match(input,GE,FOLLOW_GE_in_expr303); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr307);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr311);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new GreaterThanEqualsEvaluator(e1 , e2);

                    }
                    break;
                case 12 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:44:7: ^( '=' e1= expr e2= expr )
                    {
                    match(input,47,FOLLOW_47_in_expr323); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr327);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr331);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new EqualsEvaluator(e1 , e2);

                    }
                    break;
                case 13 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:45:7: ^( '!=' e1= expr e2= expr )
                    {
                    match(input,45,FOLLOW_45_in_expr344); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr348);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr352);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new NotEqualsEvaluator(e1 , e2);

                    }
                    break;
                case 14 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:46:7: ^( '|' e1= expr e2= expr )
                    {
                    match(input,PIPE,FOLLOW_PIPE_in_expr364); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr368);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr372);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new UnionEvaluator(e1, e2);

                    }
                    break;
                case 15 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:47:7: ^( NEGETION e1= expr )
                    {
                    match(input,NEGETION,FOLLOW_NEGETION_in_expr385); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr389);
                    e1=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = new NegationEvaluator(e1);

                    }
                    break;
                case 16 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:48:7: l1= locationPath
                    {
                    pushFollow(FOLLOW_locationPath_in_expr406);
                    l1=locationPath();

                    state._fsp--;


                     eval = l1; 

                    }
                    break;
                case 17 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:49:7: function
                    {
                    pushFollow(FOLLOW_function_in_expr424);
                    function1=function();

                    state._fsp--;


                     eval = function1; 

                    }
                    break;
                case 18 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:50:7: Literal
                    {
                    Literal2=(CommonTree)match(input,Literal,FOLLOW_Literal_in_expr434); 

                    eval = new LiteralEvaluator((Literal2!=null?Literal2.getText():null));

                    }
                    break;
                case 19 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:51:7: Number
                    {
                    Number3=(CommonTree)match(input,Number,FOLLOW_Number_in_expr444); 

                    eval = new NumberEvaluator((Number3!=null?Number3.getText():null));

                    }
                    break;
                case 20 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:52:7: s1= step
                    {
                    pushFollow(FOLLOW_step_in_expr456);
                    s1=step();

                    state._fsp--;


                     eval = s1; 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "expr"



    // $ANTLR start "locationPath"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:55:1: locationPath returns [ Evaluator eval ] : (r1= relativeLocationPath |a1= absoluteLocationPathNoroot );
    public final Evaluator locationPath() throws RecognitionException {
        Evaluator eval = null;


        Evaluator r1 =null;

        Evaluator a1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:56:3: (r1= relativeLocationPath |a1= absoluteLocationPathNoroot )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==PATH||LA2_0==RELATIVEPATH) ) {
                alt2=1;
            }
            else if ( (LA2_0==CURRENT||LA2_0==RELATIVEFROMROOT) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:56:7: r1= relativeLocationPath
                    {
                    pushFollow(FOLLOW_relativeLocationPath_in_locationPath482);
                    r1=relativeLocationPath();

                    state._fsp--;


                     eval = r1; 

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:57:7: a1= absoluteLocationPathNoroot
                    {
                    pushFollow(FOLLOW_absoluteLocationPathNoroot_in_locationPath494);
                    a1=absoluteLocationPathNoroot();

                    state._fsp--;


                     eval = a1; 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "locationPath"



    // $ANTLR start "absoluteLocationPathNoroot"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:60:1: absoluteLocationPathNoroot returns [Evaluator eval ] : ( ^( CURRENT e1= expr ) | ^( RELATIVEFROMROOT e1= expr ) );
    public final Evaluator absoluteLocationPathNoroot() throws RecognitionException {
        Evaluator eval = null;


        Evaluator e1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:61:3: ( ^( CURRENT e1= expr ) | ^( RELATIVEFROMROOT e1= expr ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==CURRENT) ) {
                alt3=1;
            }
            else if ( (LA3_0==RELATIVEFROMROOT) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:61:5: ^( CURRENT e1= expr )
                    {
                    match(input,CURRENT,FOLLOW_CURRENT_in_absoluteLocationPathNoroot514); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_absoluteLocationPathNoroot518);
                    e1=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                     eval = new CurrentPathEvaluator(e1); 

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:62:5: ^( RELATIVEFROMROOT e1= expr )
                    {
                    match(input,RELATIVEFROMROOT,FOLLOW_RELATIVEFROMROOT_in_absoluteLocationPathNoroot528); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_absoluteLocationPathNoroot532);
                    e1=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                     eval = new RelativePathEvaluator(e1); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "absoluteLocationPathNoroot"



    // $ANTLR start "function"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:65:1: function returns [ Evaluator eval] : ^( FUNCTION fname= functionName (e1= expr ( ',' e2= expr )* )? ) ;
    public final Evaluator function() throws RecognitionException {
        Evaluator eval = null;


        Evaluator fname =null;

        Evaluator e1 =null;

        Evaluator e2 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:65:35: ( ^( FUNCTION fname= functionName (e1= expr ( ',' e2= expr )* )? ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:66:3: ^( FUNCTION fname= functionName (e1= expr ( ',' e2= expr )* )? )
            {

                  List<Evaluator> evallist = new ArrayList<Evaluator>();
              

            match(input,FUNCTION,FOLLOW_FUNCTION_in_function555); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_functionName_in_function559);
            fname=functionName();

            state._fsp--;


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:69:33: (e1= expr ( ',' e2= expr )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0 >= AT && LA5_0 <= AxisName)||(LA5_0 >= CURRENT && LA5_0 <= CURRENTNODE)||(LA5_0 >= FUNCTION && LA5_0 <= GE)||(LA5_0 >= LE && LA5_0 <= LESS)||(LA5_0 >= Literal && LA5_0 <= NAMETEST)||(LA5_0 >= NEGETION && LA5_0 <= NODETEST)||(LA5_0 >= Number && LA5_0 <= PATH)||(LA5_0 >= PIPE && LA5_0 <= PRIORNODE)||(LA5_0 >= RELATIVEFROMROOT && LA5_0 <= RELATIVEPATH)||LA5_0==45||(LA5_0 >= 47 && LA5_0 <= 51)) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:69:35: e1= expr ( ',' e2= expr )*
                    {
                    pushFollow(FOLLOW_expr_in_function565);
                    e1=expr();

                    state._fsp--;


                     evallist.add(e1); 

                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:69:71: ( ',' e2= expr )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==COMMA) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:69:73: ',' e2= expr
                    	    {
                    	    match(input,COMMA,FOLLOW_COMMA_in_function571); 

                    	    pushFollow(FOLLOW_expr_in_function575);
                    	    e2=expr();

                    	    state._fsp--;


                    	     evallist.add(e2); 

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }


            match(input, Token.UP, null); 


             
                  eval = new FunctionEvaluator(fname, evallist);
                

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "function"



    // $ANTLR start "functionName"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:75:1: functionName returns [ Evaluator eval] : e1= qName ;
    public final Evaluator functionName() throws RecognitionException {
        Evaluator eval = null;


        Evaluator e1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:76:3: (e1= qName )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:76:6: e1= qName
            {
            pushFollow(FOLLOW_qName_in_functionName610);
            e1=qName();

            state._fsp--;


             eval = e1; 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "functionName"



    // $ANTLR start "relativeLocationPath"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:79:1: relativeLocationPath returns [ Evaluator eval] : ^( (t= PATH |t= RELATIVEPATH ) e1= expr (e2= expr )? ) ;
    public final Evaluator relativeLocationPath() throws RecognitionException {
        Evaluator eval = null;


        CommonTree t=null;
        Evaluator e1 =null;

        Evaluator e2 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:3: ( ^( (t= PATH |t= RELATIVEPATH ) e1= expr (e2= expr )? ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:5: ^( (t= PATH |t= RELATIVEPATH ) e1= expr (e2= expr )? )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:7: (t= PATH |t= RELATIVEPATH )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==PATH) ) {
                alt6=1;
            }
            else if ( (LA6_0==RELATIVEPATH) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }
            switch (alt6) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:8: t= PATH
                    {
                    t=(CommonTree)match(input,PATH,FOLLOW_PATH_in_relativeLocationPath633); 

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:15: t= RELATIVEPATH
                    {
                    t=(CommonTree)match(input,RELATIVEPATH,FOLLOW_RELATIVEPATH_in_relativeLocationPath637); 

                    }
                    break;

            }


            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_expr_in_relativeLocationPath642);
            e1=expr();

            state._fsp--;


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:41: (e2= expr )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0 >= AT && LA7_0 <= AxisName)||(LA7_0 >= CURRENT && LA7_0 <= CURRENTNODE)||(LA7_0 >= FUNCTION && LA7_0 <= GE)||(LA7_0 >= LE && LA7_0 <= LESS)||(LA7_0 >= Literal && LA7_0 <= NAMETEST)||(LA7_0 >= NEGETION && LA7_0 <= NODETEST)||(LA7_0 >= Number && LA7_0 <= PATH)||(LA7_0 >= PIPE && LA7_0 <= PRIORNODE)||(LA7_0 >= RELATIVEFROMROOT && LA7_0 <= RELATIVEPATH)||LA7_0==45||(LA7_0 >= 47 && LA7_0 <= 51)) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:80:41: e2= expr
                    {
                    pushFollow(FOLLOW_expr_in_relativeLocationPath646);
                    e2=expr();

                    state._fsp--;


                    }
                    break;

            }


            match(input, Token.UP, null); 


            eval = new PathEvaluator((t!=null?t.getText():null), e1, e2);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "relativeLocationPath"



    // $ANTLR start "step"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:83:1: step returns [Evaluator eval] : ( (a1= AxisName |a2= AT )? n1= nodeTest (p1= predicate )* | CURRENTNODE | PRIORNODE );
    public final Evaluator step() throws RecognitionException {
        Evaluator eval = null;


        CommonTree a1=null;
        CommonTree a2=null;
        Evaluator n1 =null;

        Evaluator p1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:83:30: ( (a1= AxisName |a2= AT )? n1= nodeTest (p1= predicate )* | CURRENTNODE | PRIORNODE )
            int alt10=3;
            switch ( input.LA(1) ) {
            case AT:
            case AxisName:
            case NAMETEST:
            case NODETEST:
                {
                alt10=1;
                }
                break;
            case CURRENTNODE:
                {
                alt10=2;
                }
                break;
            case PRIORNODE:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }

            switch (alt10) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:84:3: (a1= AxisName |a2= AT )? n1= nodeTest (p1= predicate )*
                    {

                         List<Evaluator> predicateEval = new ArrayList<Evaluator>();
                      

                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:87:3: (a1= AxisName |a2= AT )?
                    int alt8=3;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==AxisName) ) {
                        alt8=1;
                    }
                    else if ( (LA8_0==AT) ) {
                        alt8=2;
                    }
                    switch (alt8) {
                        case 1 :
                            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:87:4: a1= AxisName
                            {
                            a1=(CommonTree)match(input,AxisName,FOLLOW_AxisName_in_step674); 

                            }
                            break;
                        case 2 :
                            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:87:18: a2= AT
                            {
                            a2=(CommonTree)match(input,AT,FOLLOW_AT_in_step680); 

                            }
                            break;

                    }


                    pushFollow(FOLLOW_nodeTest_in_step686);
                    n1=nodeTest();

                    state._fsp--;


                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:87:38: (p1= predicate )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==LBRAC) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:87:39: p1= predicate
                    	    {
                    	    pushFollow(FOLLOW_predicate_in_step691);
                    	    p1=predicate();

                    	    state._fsp--;


                    	     predicateEval.add(p1);

                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                     
                        if (a1 != null) { 
                           eval = new StepEvaluator((a1!=null?a1.getText():null),n1, predicateEval);
                        } else if (a2 != null){ 
                           eval = new StepEvaluator((a2!=null?a2.getText():null),n1, predicateEval);
                        } else {    
                           eval = new StepEvaluator(null,n1, predicateEval);
                        }
                      

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:97:5: CURRENTNODE
                    {
                    match(input,CURRENTNODE,FOLLOW_CURRENTNODE_in_step705); 

                    eval = new AttributeEvaluator(".");

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:98:5: PRIORNODE
                    {
                    match(input,PRIORNODE,FOLLOW_PRIORNODE_in_step713); 

                    eval = new AttributeEvaluator("..");

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "step"



    // $ANTLR start "predicate"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:101:1: predicate returns [ Evaluator eval ] : '[' expr ']' ;
    public final Evaluator predicate() throws RecognitionException {
        Evaluator eval = null;


        Evaluator expr4 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:102:3: ( '[' expr ']' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:102:6: '[' expr ']'
            {
            match(input,LBRAC,FOLLOW_LBRAC_in_predicate733); 

            pushFollow(FOLLOW_expr_in_predicate735);
            expr4=expr();

            state._fsp--;


            match(input,RBRAC,FOLLOW_RBRAC_in_predicate737); 

             eval = new PredicateEvaluator(expr4);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "predicate"



    // $ANTLR start "nodeTest"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:105:1: nodeTest returns [Evaluator eval] : ( ^( NAMETEST nameTest ) | ^( NODETEST NodeType ) | ^( NODETEST NodeType Literal ) );
    public final Evaluator nodeTest() throws RecognitionException {
        Evaluator eval = null;


        CommonTree NodeType6=null;
        CommonTree NodeType7=null;
        CommonTree Literal8=null;
        Evaluator nameTest5 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:105:34: ( ^( NAMETEST nameTest ) | ^( NODETEST NodeType ) | ^( NODETEST NodeType Literal ) )
            int alt11=3;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==NAMETEST) ) {
                alt11=1;
            }
            else if ( (LA11_0==NODETEST) ) {
                int LA11_2 = input.LA(2);

                if ( (LA11_2==DOWN) ) {
                    int LA11_3 = input.LA(3);

                    if ( (LA11_3==NodeType) ) {
                        int LA11_4 = input.LA(4);

                        if ( (LA11_4==UP) ) {
                            alt11=2;
                        }
                        else if ( (LA11_4==Literal) ) {
                            alt11=3;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 11, 4, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 11, 3, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 2, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:105:37: ^( NAMETEST nameTest )
                    {
                    match(input,NAMETEST,FOLLOW_NAMETEST_in_nodeTest755); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_nameTest_in_nodeTest758);
                    nameTest5=nameTest();

                    state._fsp--;


                    match(input, Token.UP, null); 


                     eval = nameTest5; 

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:106:7: ^( NODETEST NodeType )
                    {
                    match(input,NODETEST,FOLLOW_NODETEST_in_nodeTest771); 

                    match(input, Token.DOWN, null); 
                    NodeType6=(CommonTree)match(input,NodeType,FOLLOW_NodeType_in_nodeTest773); 

                    match(input, Token.UP, null); 


                     eval = new NodeEvaluator((NodeType6!=null?NodeType6.getText():null), null);

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:107:7: ^( NODETEST NodeType Literal )
                    {
                    match(input,NODETEST,FOLLOW_NODETEST_in_nodeTest786); 

                    match(input, Token.DOWN, null); 
                    NodeType7=(CommonTree)match(input,NodeType,FOLLOW_NodeType_in_nodeTest788); 

                    Literal8=(CommonTree)match(input,Literal,FOLLOW_Literal_in_nodeTest790); 

                    match(input, Token.UP, null); 


                     eval = new NodeEvaluator((NodeType7!=null?NodeType7.getText():null), (Literal8!=null?Literal8.getText():null));

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "nodeTest"



    // $ANTLR start "qName"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:110:1: qName returns [Evaluator eval] : e1= nCName ( ':' e2= nCName )? ;
    public final Evaluator qName() throws RecognitionException {
        Evaluator eval = null;


        Evaluator e1 =null;

        Evaluator e2 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:110:32: (e1= nCName ( ':' e2= nCName )? )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:110:35: e1= nCName ( ':' e2= nCName )?
            {
            pushFollow(FOLLOW_nCName_in_qName813);
            e1=nCName();

            state._fsp--;


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:110:45: ( ':' e2= nCName )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==COLON) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:110:46: ':' e2= nCName
                    {
                    match(input,COLON,FOLLOW_COLON_in_qName816); 

                    pushFollow(FOLLOW_nCName_in_qName820);
                    e2=nCName();

                    state._fsp--;


                    }
                    break;

            }


             
                  if(e2 != null){
                    System.out.println("We will need to handle this case");
                    eval = e1;
                  }
                  else{  
                    eval = e1;
                  }      
                

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "qName"



    // $ANTLR start "nameTest"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:122:1: nameTest returns [Evaluator eval] : ( '*' | nCName ':' '*' | qName );
    public final Evaluator nameTest() throws RecognitionException {
        Evaluator eval = null;


        Evaluator nCName9 =null;

        Evaluator qName10 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:122:35: ( '*' | nCName ':' '*' | qName )
            int alt13=3;
            switch ( input.LA(1) ) {
            case MUL:
                {
                alt13=1;
                }
                break;
            case NCName:
                {
                int LA13_2 = input.LA(2);

                if ( (LA13_2==COLON) ) {
                    int LA13_4 = input.LA(3);

                    if ( (LA13_4==MUL) ) {
                        alt13=2;
                    }
                    else if ( (LA13_4==AxisName||LA13_4==NCName) ) {
                        alt13=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 4, input);

                        throw nvae;

                    }
                }
                else if ( (LA13_2==UP) ) {
                    alt13=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 2, input);

                    throw nvae;

                }
                }
                break;
            case AxisName:
                {
                int LA13_3 = input.LA(2);

                if ( (LA13_3==COLON) ) {
                    int LA13_4 = input.LA(3);

                    if ( (LA13_4==MUL) ) {
                        alt13=2;
                    }
                    else if ( (LA13_4==AxisName||LA13_4==NCName) ) {
                        alt13=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 4, input);

                        throw nvae;

                    }
                }
                else if ( (LA13_3==UP) ) {
                    alt13=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 3, input);

                    throw nvae;

                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }

            switch (alt13) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:122:38: '*'
                    {
                    match(input,MUL,FOLLOW_MUL_in_nameTest845); 

                     eval = new AttributeEvaluator("*");

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:123:6: nCName ':' '*'
                    {
                    pushFollow(FOLLOW_nCName_in_nameTest854);
                    nCName9=nCName();

                    state._fsp--;


                    match(input,COLON,FOLLOW_COLON_in_nameTest856); 

                    match(input,MUL,FOLLOW_MUL_in_nameTest858); 

                     eval = nCName9 ; System.out.println("Need to Handle * in nameTest");

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:124:6: qName
                    {
                    pushFollow(FOLLOW_qName_in_nameTest867);
                    qName10=qName();

                    state._fsp--;


                     eval = qName10; 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "nameTest"



    // $ANTLR start "nCName"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:127:1: nCName returns [Evaluator eval] : ( NCName | AxisName );
    public final Evaluator nCName() throws RecognitionException {
        Evaluator eval = null;


        CommonTree NCName11=null;
        CommonTree AxisName12=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:127:34: ( NCName | AxisName )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==NCName) ) {
                alt14=1;
            }
            else if ( (LA14_0==AxisName) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:127:37: NCName
                    {
                    NCName11=(CommonTree)match(input,NCName,FOLLOW_NCName_in_nCName886); 

                     eval = new AttributeEvaluator((NCName11!=null?NCName11.getText():null)); 

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\xml\\xpath\\grammar\\XPathWalker.g:128:6: AxisName
                    {
                    AxisName12=(CommonTree)match(input,AxisName,FOLLOW_AxisName_in_nCName895); 

                    eval = new AxisNameEvaluator((AxisName12!=null?AxisName12.getText():null)); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "nCName"

    // Delegated rules


 

    public static final BitSet FOLLOW_expr_in_xpath69 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLUS_in_expr101 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr105 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr109 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_in_expr122 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr126 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr130 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MUL_in_expr143 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr147 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr151 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_49_in_expr164 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr168 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr172 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_50_in_expr183 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr187 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr191 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_48_in_expr202 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr206 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr210 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_51_in_expr221 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr225 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr229 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LESS_in_expr241 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr245 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr249 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MORE_in_expr262 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr266 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr270 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LE_in_expr283 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr287 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr291 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_GE_in_expr303 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr307 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr311 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_47_in_expr323 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr327 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr331 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_45_in_expr344 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr348 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr352 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PIPE_in_expr364 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr368 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_expr372 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NEGETION_in_expr385 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr389 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_locationPath_in_expr406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_expr424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Literal_in_expr434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Number_in_expr444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_step_in_expr456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relativeLocationPath_in_locationPath482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_absoluteLocationPathNoroot_in_locationPath494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CURRENT_in_absoluteLocationPathNoroot514 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_absoluteLocationPathNoroot518 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RELATIVEFROMROOT_in_absoluteLocationPathNoroot528 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_absoluteLocationPathNoroot532 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FUNCTION_in_function555 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_functionName_in_function559 = new BitSet(new long[]{0x000FA676C7DB18C8L});
    public static final BitSet FOLLOW_expr_in_function565 = new BitSet(new long[]{0x0000000000000408L});
    public static final BitSet FOLLOW_COMMA_in_function571 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_function575 = new BitSet(new long[]{0x0000000000000408L});
    public static final BitSet FOLLOW_qName_in_functionName610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PATH_in_relativeLocationPath633 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_RELATIVEPATH_in_relativeLocationPath637 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_relativeLocationPath642 = new BitSet(new long[]{0x000FA676C7DB18C8L});
    public static final BitSet FOLLOW_expr_in_relativeLocationPath646 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_AxisName_in_step674 = new BitSet(new long[]{0x0000000084000000L});
    public static final BitSet FOLLOW_AT_in_step680 = new BitSet(new long[]{0x0000000084000000L});
    public static final BitSet FOLLOW_nodeTest_in_step686 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_predicate_in_step691 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_CURRENTNODE_in_step705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PRIORNODE_in_step713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LBRAC_in_predicate733 = new BitSet(new long[]{0x000FA676C7DB18C0L});
    public static final BitSet FOLLOW_expr_in_predicate735 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_RBRAC_in_predicate737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAMETEST_in_nodeTest755 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_nameTest_in_nodeTest758 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NODETEST_in_nodeTest771 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NodeType_in_nodeTest773 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NODETEST_in_nodeTest786 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NodeType_in_nodeTest788 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_Literal_in_nodeTest790 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_nCName_in_qName813 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_COLON_in_qName816 = new BitSet(new long[]{0x0000000008000080L});
    public static final BitSet FOLLOW_nCName_in_qName820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MUL_in_nameTest845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nCName_in_nameTest854 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_COLON_in_nameTest856 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_MUL_in_nameTest858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_qName_in_nameTest867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NCName_in_nCName886 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AxisName_in_nCName895 = new BitSet(new long[]{0x0000000000000002L});

}