// $ANTLR 3.4 D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g 2015-05-18 10:09:59

package com.mmpnc.rating.iso.excel.algorithm.loader;
  
import com.mmpnc.rating.icm.algorithm.vo.Ratebook;
import com.mmpnc.rating.icm.algorithm.vo.RatingEntity;
import com.mmpnc.rating.icm.algorithm.vo.Reference;
import com.mmpnc.rating.icm.algorithm.vo.Process;
import com.mmpnc.rating.icm.algorithm.vo.Program;
import com.mmpnc.rating.icm.algorithm.vo.Step;
  
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class AlgorithmLoaderLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int CHAR=4;
    public static final int COMMENTS=5;
    public static final int DIV=6;
    public static final int EQUALS=7;
    public static final int IDENTS=8;
    public static final int INTEGER=9;
    public static final int MULTI_COMMENT=10;
    public static final int NEGATION=11;
    public static final int NOTEQUALS=12;
    public static final int SPECIALFUNCTION=13;
    public static final int STRING=14;
    public static final int SUBTRACT=15;
    public static final int VARCONSTANT=16;
    public static final int WS=17;
    public static final int XPATHCONSTANT=18;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public AlgorithmLoaderLexer() {} 
    public AlgorithmLoaderLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public AlgorithmLoaderLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g"; }

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:22:7: ( '(' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:22:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:23:7: ( ')' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:23:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:24:7: ( '*' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:24:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:25:7: ( '+' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:25:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:26:7: ( ',' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:26:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:27:7: ( '-' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:27:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:28:7: ( '/' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:28:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:29:7: ( ':' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:29:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:30:7: ( '<' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:30:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:31:7: ( '<=' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:31:9: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:32:7: ( '=' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:32:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:33:7: ( '>' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:33:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:34:7: ( '>=' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:34:9: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:35:7: ( 'And' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:35:9: 'And'
            {
            match("And"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:36:7: ( 'BBB' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:36:9: 'BBB'
            {
            match("BBB"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:37:7: ( 'CCH' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:37:9: 'CCH'
            {
            match("CCH"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:38:7: ( 'Call' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:38:9: 'Call'
            {
            match("Call"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:39:7: ( 'ELSE' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:39:9: 'ELSE'
            {
            match("ELSE"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:40:7: ( 'END IF' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:40:9: 'END IF'
            {
            match("END IF"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:41:7: ( 'END LOOP' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:41:9: 'END LOOP'
            {
            match("END LOOP"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:42:7: ( 'IF' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:42:9: 'IF'
            {
            match("IF"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:43:7: ( 'Is Equal To' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:43:9: 'Is Equal To'
            {
            match("Is Equal To"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:44:7: ( 'LOOP THROUGH' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:44:9: 'LOOP THROUGH'
            {
            match("LOOP THROUGH"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:45:7: ( 'NOT' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:45:9: 'NOT'
            {
            match("NOT"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:46:7: ( 'Negative' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:46:9: 'Negative'
            {
            match("Negative"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:47:7: ( 'Not Equal To' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:47:9: 'Not Equal To'
            {
            match("Not Equal To"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:48:7: ( 'Or' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:48:9: 'Or'
            {
            match("Or"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:49:7: ( 'PCH' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:49:9: 'PCH'
            {
            match("PCH"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:50:7: ( 'RUL' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:50:9: 'RUL'
            {
            match("RUL"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:51:7: ( 'RateTable' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:51:9: 'RateTable'
            {
            match("RateTable"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:52:7: ( 'SCH' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:52:9: 'SCH'
            {
            match("SCH"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:53:7: ( 'Subtract' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:53:9: 'Subtract'
            {
            match("Subtract"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:54:7: ( 'TAB' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:54:9: 'TAB'
            {
            match("TAB"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:55:7: ( 'Using' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:55:9: 'Using'
            {
            match("Using"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:56:7: ( 'mod' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:56:9: 'mod'
            {
            match("mod"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:57:7: ( 'not' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:57:9: 'not'
            {
            match("not"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:58:7: ( '{' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:58:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:59:7: ( '}' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:59:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "XPATHCONSTANT"
    public final void mXPATHCONSTANT() throws RecognitionException {
        try {
            int _type = XPATHCONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:337:15: ( 'XPATH' | 'XPATH_STRING' | 'XPATH_NUMERIC' )
            int alt1=3;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='X') ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1=='P') ) {
                    int LA1_2 = input.LA(3);

                    if ( (LA1_2=='A') ) {
                        int LA1_3 = input.LA(4);

                        if ( (LA1_3=='T') ) {
                            int LA1_4 = input.LA(5);

                            if ( (LA1_4=='H') ) {
                                int LA1_5 = input.LA(6);

                                if ( (LA1_5=='_') ) {
                                    int LA1_6 = input.LA(7);

                                    if ( (LA1_6=='S') ) {
                                        alt1=2;
                                    }
                                    else if ( (LA1_6=='N') ) {
                                        alt1=3;
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 1, 6, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    alt1=1;
                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 1, 4, input);

                                throw nvae;

                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 1, 3, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 2, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }
            switch (alt1) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:337:17: 'XPATH'
                    {
                    match("XPATH"); 



                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:337:27: 'XPATH_STRING'
                    {
                    match("XPATH_STRING"); 



                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:337:44: 'XPATH_NUMERIC'
                    {
                    match("XPATH_NUMERIC"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "XPATHCONSTANT"

    // $ANTLR start "VARCONSTANT"
    public final void mVARCONSTANT() throws RecognitionException {
        try {
            int _type = VARCONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:13: ( 'COLUMN_NUMERIC' | 'COLUMN_STRING' | 'UI' | 'LV_NUMERIC' | 'LV_STRING' | 'CONSTANT' )
            int alt2=6;
            switch ( input.LA(1) ) {
            case 'C':
                {
                int LA2_1 = input.LA(2);

                if ( (LA2_1=='O') ) {
                    int LA2_4 = input.LA(3);

                    if ( (LA2_4=='L') ) {
                        int LA2_6 = input.LA(4);

                        if ( (LA2_6=='U') ) {
                            int LA2_9 = input.LA(5);

                            if ( (LA2_9=='M') ) {
                                int LA2_12 = input.LA(6);

                                if ( (LA2_12=='N') ) {
                                    int LA2_13 = input.LA(7);

                                    if ( (LA2_13=='_') ) {
                                        int LA2_14 = input.LA(8);

                                        if ( (LA2_14=='N') ) {
                                            alt2=1;
                                        }
                                        else if ( (LA2_14=='S') ) {
                                            alt2=2;
                                        }
                                        else {
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 2, 14, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 2, 13, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 2, 12, input);

                                    throw nvae;

                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 2, 9, input);

                                throw nvae;

                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 2, 6, input);

                            throw nvae;

                        }
                    }
                    else if ( (LA2_4=='N') ) {
                        alt2=6;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 4, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;

                }
                }
                break;
            case 'U':
                {
                alt2=3;
                }
                break;
            case 'L':
                {
                int LA2_3 = input.LA(2);

                if ( (LA2_3=='V') ) {
                    int LA2_5 = input.LA(3);

                    if ( (LA2_5=='_') ) {
                        int LA2_8 = input.LA(4);

                        if ( (LA2_8=='N') ) {
                            alt2=4;
                        }
                        else if ( (LA2_8=='S') ) {
                            alt2=5;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 2, 8, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 5, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 3, input);

                    throw nvae;

                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:15: 'COLUMN_NUMERIC'
                    {
                    match("COLUMN_NUMERIC"); 



                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:34: 'COLUMN_STRING'
                    {
                    match("COLUMN_STRING"); 



                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:52: 'UI'
                    {
                    match("UI"); 



                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:59: 'LV_NUMERIC'
                    {
                    match("LV_NUMERIC"); 



                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:74: 'LV_STRING'
                    {
                    match("LV_STRING"); 



                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:338:88: 'CONSTANT'
                    {
                    match("CONSTANT"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "VARCONSTANT"

    // $ANTLR start "SPECIALFUNCTION"
    public final void mSPECIALFUNCTION() throws RecognitionException {
        try {
            int _type = SPECIALFUNCTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:17: ( 'RoundUpDollar' | 'RoundUpHundredth' | 'RoundUpHundredThousandth' | 'RoundUpThousandth' | 'RoundDollar' | 'RoundHundredth' | 'RoundHundredThousandth' | 'RoundThousandth' | 'Ceiling' | 'RoundTenThousandth' )
            int alt3=10;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='R') ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1=='o') ) {
                    int LA3_3 = input.LA(3);

                    if ( (LA3_3=='u') ) {
                        int LA3_4 = input.LA(4);

                        if ( (LA3_4=='n') ) {
                            int LA3_5 = input.LA(5);

                            if ( (LA3_5=='d') ) {
                                switch ( input.LA(6) ) {
                                case 'U':
                                    {
                                    int LA3_7 = input.LA(7);

                                    if ( (LA3_7=='p') ) {
                                        switch ( input.LA(8) ) {
                                        case 'D':
                                            {
                                            alt3=1;
                                            }
                                            break;
                                        case 'H':
                                            {
                                            int LA3_16 = input.LA(9);

                                            if ( (LA3_16=='u') ) {
                                                int LA3_19 = input.LA(10);

                                                if ( (LA3_19=='n') ) {
                                                    int LA3_21 = input.LA(11);

                                                    if ( (LA3_21=='d') ) {
                                                        int LA3_23 = input.LA(12);

                                                        if ( (LA3_23=='r') ) {
                                                            int LA3_25 = input.LA(13);

                                                            if ( (LA3_25=='e') ) {
                                                                int LA3_27 = input.LA(14);

                                                                if ( (LA3_27=='d') ) {
                                                                    int LA3_30 = input.LA(15);

                                                                    if ( (LA3_30=='t') ) {
                                                                        alt3=2;
                                                                    }
                                                                    else if ( (LA3_30=='T') ) {
                                                                        alt3=3;
                                                                    }
                                                                    else {
                                                                        NoViableAltException nvae =
                                                                            new NoViableAltException("", 3, 30, input);

                                                                        throw nvae;

                                                                    }
                                                                }
                                                                else {
                                                                    NoViableAltException nvae =
                                                                        new NoViableAltException("", 3, 27, input);

                                                                    throw nvae;

                                                                }
                                                            }
                                                            else {
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 3, 25, input);

                                                                throw nvae;

                                                            }
                                                        }
                                                        else {
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 3, 23, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 3, 21, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 3, 19, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 3, 16, input);

                                                throw nvae;

                                            }
                                            }
                                            break;
                                        case 'T':
                                            {
                                            alt3=4;
                                            }
                                            break;
                                        default:
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 3, 11, input);

                                            throw nvae;

                                        }

                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 3, 7, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 'D':
                                    {
                                    alt3=5;
                                    }
                                    break;
                                case 'H':
                                    {
                                    int LA3_9 = input.LA(7);

                                    if ( (LA3_9=='u') ) {
                                        int LA3_12 = input.LA(8);

                                        if ( (LA3_12=='n') ) {
                                            int LA3_18 = input.LA(9);

                                            if ( (LA3_18=='d') ) {
                                                int LA3_20 = input.LA(10);

                                                if ( (LA3_20=='r') ) {
                                                    int LA3_22 = input.LA(11);

                                                    if ( (LA3_22=='e') ) {
                                                        int LA3_24 = input.LA(12);

                                                        if ( (LA3_24=='d') ) {
                                                            int LA3_26 = input.LA(13);

                                                            if ( (LA3_26=='t') ) {
                                                                alt3=6;
                                                            }
                                                            else if ( (LA3_26=='T') ) {
                                                                alt3=7;
                                                            }
                                                            else {
                                                                NoViableAltException nvae =
                                                                    new NoViableAltException("", 3, 26, input);

                                                                throw nvae;

                                                            }
                                                        }
                                                        else {
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 3, 24, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 3, 22, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 3, 20, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 3, 18, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 3, 12, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 3, 9, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                case 'T':
                                    {
                                    int LA3_10 = input.LA(7);

                                    if ( (LA3_10=='h') ) {
                                        alt3=8;
                                    }
                                    else if ( (LA3_10=='e') ) {
                                        alt3=10;
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 3, 10, input);

                                        throw nvae;

                                    }
                                    }
                                    break;
                                default:
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 3, 6, input);

                                    throw nvae;

                                }

                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 3, 5, input);

                                throw nvae;

                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 3, 4, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 3, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA3_0=='C') ) {
                alt3=9;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:19: 'RoundUpDollar'
                    {
                    match("RoundUpDollar"); 



                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:37: 'RoundUpHundredth'
                    {
                    match("RoundUpHundredth"); 



                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:58: 'RoundUpHundredThousandth'
                    {
                    match("RoundUpHundredThousandth"); 



                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:87: 'RoundUpThousandth'
                    {
                    match("RoundUpThousandth"); 



                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:109: 'RoundDollar'
                    {
                    match("RoundDollar"); 



                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:125: 'RoundHundredth'
                    {
                    match("RoundHundredth"); 



                    }
                    break;
                case 7 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:144: 'RoundHundredThousandth'
                    {
                    match("RoundHundredThousandth"); 



                    }
                    break;
                case 8 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:171: 'RoundThousandth'
                    {
                    match("RoundThousandth"); 



                    }
                    break;
                case 9 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:191: 'Ceiling'
                    {
                    match("Ceiling"); 



                    }
                    break;
                case 10 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:339:203: 'RoundTenThousandth'
                    {
                    match("RoundTenThousandth"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SPECIALFUNCTION"

    // $ANTLR start "IDENTS"
    public final void mIDENTS() throws RecognitionException {
        try {
            int _type = IDENTS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:340:8: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '/' | '.' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '/' | '.' | '*' )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:340:10: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '/' | '.' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '/' | '.' | '*' )*
            {
            if ( (input.LA(1) >= '.' && input.LA(1) <= '/')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:340:49: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '/' | '.' | '*' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='*'||(LA4_0 >= '.' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'Z')||LA4_0=='_'||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
            	    {
            	    if ( input.LA(1)=='*'||(input.LA(1) >= '.' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENTS"

    // $ANTLR start "INTEGER"
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:341:9: ( ( '0' .. '9' )+ ( '.' ( '0' .. '9' )* )? )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:341:11: ( '0' .. '9' )+ ( '.' ( '0' .. '9' )* )?
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:341:11: ( '0' .. '9' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:341:22: ( '.' ( '0' .. '9' )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='.') ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:341:23: '.' ( '0' .. '9' )*
                    {
                    match('.'); 

                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:341:27: ( '0' .. '9' )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGER"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:342:8: ( '\"' ( . )* '\"' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:342:10: '\"' ( . )* '\"'
            {
            match('\"'); 

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:342:14: ( . )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='\"') ) {
                    alt8=2;
                }
                else if ( ((LA8_0 >= '\u0000' && LA8_0 <= '!')||(LA8_0 >= '#' && LA8_0 <= '\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:342:14: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "CHAR"
    public final void mCHAR() throws RecognitionException {
        try {
            int _type = CHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:343:9: ( '\\'' ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )? '\\'' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:343:11: '\\'' ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )? '\\''
            {
            match('\''); 

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:343:16: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0 >= '0' && LA9_0 <= '9')||(LA9_0 >= 'A' && LA9_0 <= 'Z')||(LA9_0 >= 'a' && LA9_0 <= 'z')) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
                    {
                    if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CHAR"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:344:4: ( ( ' ' | '\\n' | '\\r' | '\\t' )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:344:6: ( ' ' | '\\n' | '\\r' | '\\t' )*
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:344:6: ( ' ' | '\\n' | '\\r' | '\\t' )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0 >= '\t' && LA10_0 <= '\n')||LA10_0=='\r'||LA10_0==' ') ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:
            	    {
            	    if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "COMMENTS"
    public final void mCOMMENTS() throws RecognitionException {
        try {
            int _type = COMMENTS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:345:10: ( '//' ( . )* ( '\\n' | '\\r' ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:345:12: '//' ( . )* ( '\\n' | '\\r' )
            {
            match("//"); 



            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:345:17: ( . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='\n'||LA11_0=='\r') ) {
                    alt11=2;
                }
                else if ( ((LA11_0 >= '\u0000' && LA11_0 <= '\t')||(LA11_0 >= '\u000B' && LA11_0 <= '\f')||(LA11_0 >= '\u000E' && LA11_0 <= '\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:345:17: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMENTS"

    // $ANTLR start "MULTI_COMMENT"
    public final void mMULTI_COMMENT() throws RecognitionException {
        try {
            int _type = MULTI_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:346:15: ( '/*' ( . )* '*/' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:346:17: '/*' ( . )* '*/'
            {
            match("/*"); 



            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:346:22: ( . )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0=='*') ) {
                    int LA12_1 = input.LA(2);

                    if ( (LA12_1=='/') ) {
                        alt12=2;
                    }
                    else if ( ((LA12_1 >= '\u0000' && LA12_1 <= '.')||(LA12_1 >= '0' && LA12_1 <= '\uFFFF')) ) {
                        alt12=1;
                    }


                }
                else if ( ((LA12_0 >= '\u0000' && LA12_0 <= ')')||(LA12_0 >= '+' && LA12_0 <= '\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:346:22: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            match("*/"); 



            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MULTI_COMMENT"

    public void mTokens() throws RecognitionException {
        // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:8: ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | XPATHCONSTANT | VARCONSTANT | SPECIALFUNCTION | IDENTS | INTEGER | STRING | CHAR | WS | COMMENTS | MULTI_COMMENT )
        int alt13=48;
        alt13 = dfa13.predict(input);
        switch (alt13) {
            case 1 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:10: T__19
                {
                mT__19(); 


                }
                break;
            case 2 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:16: T__20
                {
                mT__20(); 


                }
                break;
            case 3 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:22: T__21
                {
                mT__21(); 


                }
                break;
            case 4 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:28: T__22
                {
                mT__22(); 


                }
                break;
            case 5 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:34: T__23
                {
                mT__23(); 


                }
                break;
            case 6 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:40: T__24
                {
                mT__24(); 


                }
                break;
            case 7 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:46: T__25
                {
                mT__25(); 


                }
                break;
            case 8 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:52: T__26
                {
                mT__26(); 


                }
                break;
            case 9 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:58: T__27
                {
                mT__27(); 


                }
                break;
            case 10 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:64: T__28
                {
                mT__28(); 


                }
                break;
            case 11 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:70: T__29
                {
                mT__29(); 


                }
                break;
            case 12 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:76: T__30
                {
                mT__30(); 


                }
                break;
            case 13 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:82: T__31
                {
                mT__31(); 


                }
                break;
            case 14 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:88: T__32
                {
                mT__32(); 


                }
                break;
            case 15 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:94: T__33
                {
                mT__33(); 


                }
                break;
            case 16 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:100: T__34
                {
                mT__34(); 


                }
                break;
            case 17 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:106: T__35
                {
                mT__35(); 


                }
                break;
            case 18 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:112: T__36
                {
                mT__36(); 


                }
                break;
            case 19 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:118: T__37
                {
                mT__37(); 


                }
                break;
            case 20 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:124: T__38
                {
                mT__38(); 


                }
                break;
            case 21 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:130: T__39
                {
                mT__39(); 


                }
                break;
            case 22 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:136: T__40
                {
                mT__40(); 


                }
                break;
            case 23 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:142: T__41
                {
                mT__41(); 


                }
                break;
            case 24 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:148: T__42
                {
                mT__42(); 


                }
                break;
            case 25 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:154: T__43
                {
                mT__43(); 


                }
                break;
            case 26 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:160: T__44
                {
                mT__44(); 


                }
                break;
            case 27 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:166: T__45
                {
                mT__45(); 


                }
                break;
            case 28 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:172: T__46
                {
                mT__46(); 


                }
                break;
            case 29 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:178: T__47
                {
                mT__47(); 


                }
                break;
            case 30 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:184: T__48
                {
                mT__48(); 


                }
                break;
            case 31 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:190: T__49
                {
                mT__49(); 


                }
                break;
            case 32 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:196: T__50
                {
                mT__50(); 


                }
                break;
            case 33 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:202: T__51
                {
                mT__51(); 


                }
                break;
            case 34 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:208: T__52
                {
                mT__52(); 


                }
                break;
            case 35 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:214: T__53
                {
                mT__53(); 


                }
                break;
            case 36 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:220: T__54
                {
                mT__54(); 


                }
                break;
            case 37 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:226: T__55
                {
                mT__55(); 


                }
                break;
            case 38 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:232: T__56
                {
                mT__56(); 


                }
                break;
            case 39 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:238: XPATHCONSTANT
                {
                mXPATHCONSTANT(); 


                }
                break;
            case 40 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:252: VARCONSTANT
                {
                mVARCONSTANT(); 


                }
                break;
            case 41 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:264: SPECIALFUNCTION
                {
                mSPECIALFUNCTION(); 


                }
                break;
            case 42 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:280: IDENTS
                {
                mIDENTS(); 


                }
                break;
            case 43 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:287: INTEGER
                {
                mINTEGER(); 


                }
                break;
            case 44 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:295: STRING
                {
                mSTRING(); 


                }
                break;
            case 45 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:302: CHAR
                {
                mCHAR(); 


                }
                break;
            case 46 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:307: WS
                {
                mWS(); 


                }
                break;
            case 47 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:310: COMMENTS
                {
                mCOMMENTS(); 


                }
                break;
            case 48 :
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\excel\\algorithm\\loader\\AlgorithmLoader.g:1:319: MULTI_COMMENT
                {
                mMULTI_COMMENT(); 


                }
                break;

        }

    }


    protected DFA13 dfa13 = new DFA13(this);
    static final String DFA13_eotS =
        "\1\42\6\uffff\1\45\1\uffff\1\47\1\uffff\1\51\17\36\2\uffff\1\36"+
        "\5\uffff\2\36\5\uffff\10\36\1\124\6\36\1\133\10\36\1\144\3\36\1"+
        "\uffff\3\36\1\uffff\1\151\1\152\1\153\6\36\2\uffff\2\36\1\165\2"+
        "\36\1\uffff\1\170\1\171\2\36\1\174\1\36\1\176\1\36\1\uffff\1\u0080"+
        "\1\u0081\2\36\3\uffff\1\u0083\3\36\1\u0087\1\uffff\3\36\1\uffff"+
        "\1\36\3\uffff\2\36\1\uffff\1\36\1\uffff\1\36\2\uffff\1\36\1\uffff"+
        "\3\36\4\uffff\6\36\1\u009f\1\u00a1\14\36\1\uffff\1\36\1\uffff\2"+
        "\36\1\u00b4\16\36\1\144\1\uffff\2\36\1\u00c7\10\36\1\u00d0\5\36"+
        "\1\144\1\uffff\1\u00d6\7\36\1\uffff\4\36\1\144\1\uffff\16\36\1\u00b4"+
        "\15\36\1\u00a1\2\36\1\144\1\u00b4\6\36\1\u00a1\1\144\2\36\1\u00b4"+
        "\7\36\1\u00b4\1\36\1\u00b4\5\36\1\u00b4\4\36\1\u00b4\7\36\1\u00b4"+
        "\1\36\1\u00b4";
    static final String DFA13_eofS =
        "\u0122\uffff";
    static final String DFA13_minS =
        "\1\42\6\uffff\1\52\1\uffff\1\75\1\uffff\1\75\1\156\1\102\1\103\1"+
        "\114\1\106\2\117\1\162\1\103\1\125\1\103\1\101\1\111\2\157\2\uffff"+
        "\1\120\5\uffff\2\0\5\uffff\1\144\1\102\1\110\1\154\1\114\1\151\1"+
        "\123\1\104\1\52\1\40\1\117\1\137\1\124\1\147\1\164\1\52\1\110\1"+
        "\114\1\164\1\165\1\110\1\142\1\102\1\151\1\52\1\144\1\164\1\101"+
        "\1\uffff\3\0\1\uffff\3\52\1\154\1\125\1\123\1\154\1\105\1\40\2\uffff"+
        "\1\120\1\116\1\52\1\141\1\40\1\uffff\2\52\1\145\1\156\1\52\1\164"+
        "\1\52\1\156\1\uffff\2\52\1\124\1\0\3\uffff\1\52\1\115\1\124\1\151"+
        "\1\52\1\111\1\40\1\125\1\124\1\uffff\1\164\3\uffff\1\124\1\144\1"+
        "\uffff\1\162\1\uffff\1\147\2\uffff\1\110\1\uffff\1\116\1\101\1\156"+
        "\4\uffff\1\115\1\122\1\151\1\141\1\104\1\141\2\52\1\137\1\116\1"+
        "\147\1\105\1\111\1\166\1\142\1\160\1\157\1\165\1\145\1\143\1\uffff"+
        "\1\116\1\uffff\1\116\1\124\1\52\1\122\1\116\1\145\1\154\1\104\1"+
        "\154\1\156\1\157\1\156\1\164\1\124\2\125\1\124\1\52\1\uffff\1\111"+
        "\1\107\1\52\1\145\1\157\1\165\1\150\1\154\1\144\1\165\1\124\1\52"+
        "\1\122\2\115\1\122\1\103\1\52\1\uffff\1\52\1\154\1\156\1\157\1\141"+
        "\1\162\1\163\1\150\1\uffff\1\111\2\105\1\111\1\52\1\uffff\1\154"+
        "\1\144\1\165\1\162\1\145\1\141\1\157\1\116\2\122\1\116\1\141\1\162"+
        "\1\163\1\52\1\144\1\156\1\165\1\107\2\111\1\107\1\162\1\145\1\141"+
        "\1\124\1\144\1\163\1\52\2\103\2\52\1\144\1\156\2\150\1\164\1\141"+
        "\2\52\1\124\1\144\1\52\1\157\1\150\1\156\2\150\1\164\1\165\1\52"+
        "\1\144\1\52\1\157\1\150\1\163\1\164\1\165\1\52\1\141\1\150\1\163"+
        "\1\156\1\52\1\141\1\144\1\156\1\164\1\144\1\150\1\164\1\52\1\150"+
        "\1\52";
    static final String DFA13_maxS =
        "\1\175\6\uffff\1\172\1\uffff\1\75\1\uffff\1\75\1\156\1\102\1\145"+
        "\1\116\1\163\1\126\1\157\1\162\1\103\1\157\1\165\1\101\1\163\2\157"+
        "\2\uffff\1\120\5\uffff\2\uffff\5\uffff\1\144\1\102\1\110\1\154\1"+
        "\116\1\151\1\123\1\104\1\172\1\40\1\117\1\137\1\124\1\147\1\164"+
        "\1\172\1\110\1\114\1\164\1\165\1\110\1\142\1\102\1\151\1\172\1\144"+
        "\1\164\1\101\1\uffff\3\uffff\1\uffff\3\172\1\154\1\125\1\123\1\154"+
        "\1\105\1\40\2\uffff\1\120\1\123\1\172\1\141\1\40\1\uffff\2\172\1"+
        "\145\1\156\1\172\1\164\1\172\1\156\1\uffff\2\172\1\124\1\uffff\3"+
        "\uffff\1\172\1\115\1\124\1\151\1\172\1\114\1\40\1\125\1\124\1\uffff"+
        "\1\164\3\uffff\1\124\1\144\1\uffff\1\162\1\uffff\1\147\2\uffff\1"+
        "\110\1\uffff\1\116\1\101\1\156\4\uffff\1\115\1\122\1\151\1\141\1"+
        "\125\1\141\2\172\1\137\1\116\1\147\1\105\1\111\1\166\1\142\1\160"+
        "\1\157\1\165\1\150\1\143\1\uffff\1\123\1\uffff\1\123\1\124\1\172"+
        "\1\122\1\116\1\145\1\154\1\124\1\154\1\156\1\157\1\156\1\164\1\124"+
        "\2\125\1\124\1\172\1\uffff\1\111\1\107\1\172\1\145\1\157\1\165\1"+
        "\150\1\154\1\144\1\165\1\124\1\172\1\122\2\115\1\122\1\103\1\172"+
        "\1\uffff\1\172\1\154\1\156\1\157\1\141\1\162\1\163\1\150\1\uffff"+
        "\1\111\2\105\1\111\1\172\1\uffff\1\154\1\144\1\165\1\162\1\145\1"+
        "\141\1\157\1\116\2\122\1\116\1\141\1\162\1\163\1\172\1\144\1\156"+
        "\1\165\1\107\2\111\1\107\1\162\1\145\1\141\1\164\1\144\1\163\1\172"+
        "\2\103\2\172\1\144\1\156\2\150\1\164\1\141\2\172\1\164\1\144\1\172"+
        "\1\157\1\150\1\156\2\150\1\164\1\165\1\172\1\144\1\172\1\157\1\150"+
        "\1\163\1\164\1\165\1\172\1\141\1\150\1\163\1\156\1\172\1\141\1\144"+
        "\1\156\1\164\1\144\1\150\1\164\1\172\1\150\1\172";
    static final String DFA13_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\uffff\1\10\1\uffff\1\13\20\uffff"+
        "\1\45\1\46\1\uffff\1\52\1\53\1\54\1\55\1\56\2\uffff\1\7\1\12\1\11"+
        "\1\15\1\14\34\uffff\1\57\3\uffff\1\60\11\uffff\1\25\1\26\5\uffff"+
        "\1\33\10\uffff\1\50\4\uffff\1\16\1\17\1\20\11\uffff\1\30\1\uffff"+
        "\1\32\1\34\1\35\2\uffff\1\37\1\uffff\1\41\1\uffff\1\43\1\44\1\uffff"+
        "\1\21\3\uffff\1\22\1\23\1\24\1\27\24\uffff\1\42\1\uffff\1\47\22"+
        "\uffff\1\51\22\uffff\1\31\10\uffff\1\40\5\uffff\1\36\113\uffff";
    static final String DFA13_specialS =
        "\43\uffff\1\5\1\0\42\uffff\1\4\1\1\1\3\36\uffff\1\2\u00b9\uffff}>";
    static final String[] DFA13_transitionS = {
            "\1\40\4\uffff\1\41\1\1\1\2\1\3\1\4\1\5\1\6\1\36\1\7\12\37\1"+
            "\10\1\uffff\1\11\1\12\1\13\2\uffff\1\14\1\15\1\16\1\36\1\17"+
            "\3\36\1\20\2\36\1\21\1\36\1\22\1\23\1\24\1\36\1\25\1\26\1\27"+
            "\1\30\2\36\1\35\2\36\4\uffff\1\36\1\uffff\14\36\1\31\1\32\14"+
            "\36\1\33\1\uffff\1\34",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\44\3\uffff\1\36\1\43\12\36\7\uffff\32\36\4\uffff\1\36\1"+
            "\uffff\32\36",
            "",
            "\1\46",
            "",
            "\1\50",
            "\1\52",
            "\1\53",
            "\1\54\13\uffff\1\56\21\uffff\1\55\3\uffff\1\57",
            "\1\60\1\uffff\1\61",
            "\1\62\54\uffff\1\63",
            "\1\64\6\uffff\1\65",
            "\1\66\25\uffff\1\67\11\uffff\1\70",
            "\1\71",
            "\1\72",
            "\1\73\13\uffff\1\74\15\uffff\1\75",
            "\1\76\61\uffff\1\77",
            "\1\100",
            "\1\102\51\uffff\1\101",
            "\1\103",
            "\1\104",
            "",
            "",
            "\1\105",
            "",
            "",
            "",
            "",
            "",
            "\52\106\1\107\3\106\14\107\7\106\32\107\4\106\1\107\1\106\32"+
            "\107\uff85\106",
            "\52\112\1\110\3\112\14\111\7\112\32\111\4\112\1\111\1\112\32"+
            "\111\uff85\112",
            "",
            "",
            "",
            "",
            "",
            "\1\113",
            "\1\114",
            "\1\115",
            "\1\116",
            "\1\117\1\uffff\1\120",
            "\1\121",
            "\1\122",
            "\1\123",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\125",
            "\1\126",
            "\1\127",
            "\1\130",
            "\1\131",
            "\1\132",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\134",
            "\1\135",
            "\1\136",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142",
            "\1\143",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\145",
            "\1\146",
            "\1\147",
            "",
            "\52\106\1\107\3\106\14\107\7\106\32\107\4\106\1\107\1\106\32"+
            "\107\uff85\106",
            "\52\112\1\110\3\112\1\111\1\150\12\111\7\112\32\111\4\112\1"+
            "\111\1\112\32\111\uff85\112",
            "\52\112\1\110\3\112\14\111\7\112\32\111\4\112\1\111\1\112\32"+
            "\111\uff85\112",
            "",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\154",
            "\1\155",
            "\1\156",
            "\1\157",
            "\1\160",
            "\1\161",
            "",
            "",
            "\1\162",
            "\1\163\4\uffff\1\164",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\166",
            "\1\167",
            "",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\172",
            "\1\173",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\175",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\177",
            "",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0082",
            "\52\112\1\110\3\112\14\111\7\112\32\111\4\112\1\111\1\112\32"+
            "\111\uff85\112",
            "",
            "",
            "",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0088\2\uffff\1\u0089",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "",
            "\1\u008d",
            "",
            "",
            "",
            "\1\u008e",
            "\1\u008f",
            "",
            "\1\u0090",
            "",
            "\1\u0091",
            "",
            "",
            "\1\u0092",
            "",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "",
            "",
            "",
            "",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\1\u0099",
            "\1\u009b\3\uffff\1\u009c\13\uffff\1\u009d\1\u009a",
            "\1\u009e",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\u00a0\1\uffff\32"+
            "\36",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\1\u00ab",
            "\1\u00ad\2\uffff\1\u00ac",
            "\1\u00ae",
            "",
            "\1\u00b0\4\uffff\1\u00af",
            "",
            "\1\u00b1\4\uffff\1\u00b2",
            "\1\u00b3",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u00b5",
            "\1\u00b6",
            "\1\u00b7",
            "\1\u00b8",
            "\1\u00b9\3\uffff\1\u00ba\13\uffff\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\u00c4",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\u00c5",
            "\1\u00c6",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\1\u00cb",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "\1\u00cf",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u00d1",
            "\1\u00d2",
            "\1\u00d3",
            "\1\u00d4",
            "\1\u00d5",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u00d7",
            "\1\u00d8",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\1\u00dc",
            "\1\u00dd",
            "",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0",
            "\1\u00e1",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\u00e2",
            "\1\u00e3",
            "\1\u00e4",
            "\1\u00e5",
            "\1\u00e6",
            "\1\u00e7",
            "\1\u00e8",
            "\1\u00e9",
            "\1\u00ea",
            "\1\u00eb",
            "\1\u00ec",
            "\1\u00ed",
            "\1\u00ee",
            "\1\u00ef",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u00f0",
            "\1\u00f1",
            "\1\u00f2",
            "\1\u00f3",
            "\1\u00f4",
            "\1\u00f5",
            "\1\u00f6",
            "\1\u00f7",
            "\1\u00f8",
            "\1\u00f9",
            "\1\u00fb\37\uffff\1\u00fa",
            "\1\u00fc",
            "\1\u00fd",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u00fe",
            "\1\u00ff",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0100",
            "\1\u0101",
            "\1\u0102",
            "\1\u0103",
            "\1\u0104",
            "\1\u0105",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0107\37\uffff\1\u0106",
            "\1\u0108",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0109",
            "\1\u010a",
            "\1\u010b",
            "\1\u010c",
            "\1\u010d",
            "\1\u010e",
            "\1\u010f",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0110",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0111",
            "\1\u0112",
            "\1\u0113",
            "\1\u0114",
            "\1\u0115",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0116",
            "\1\u0117",
            "\1\u0118",
            "\1\u0119",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u011a",
            "\1\u011b",
            "\1\u011c",
            "\1\u011d",
            "\1\u011e",
            "\1\u011f",
            "\1\u0120",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0121",
            "\1\36\3\uffff\14\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36"
    };

    static final short[] DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
    static final short[] DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
    static final char[] DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
    static final char[] DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
    static final short[] DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
    static final short[] DFA13_special = DFA.unpackEncodedString(DFA13_specialS);
    static final short[][] DFA13_transition;

    static {
        int numStates = DFA13_transitionS.length;
        DFA13_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
        }
    }

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = DFA13_eot;
            this.eof = DFA13_eof;
            this.min = DFA13_min;
            this.max = DFA13_max;
            this.accept = DFA13_accept;
            this.special = DFA13_special;
            this.transition = DFA13_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | XPATHCONSTANT | VARCONSTANT | SPECIALFUNCTION | IDENTS | INTEGER | STRING | CHAR | WS | COMMENTS | MULTI_COMMENT );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA13_36 = input.LA(1);

                        s = -1;
                        if ( (LA13_36=='*') ) {s = 72;}

                        else if ( ((LA13_36 >= '.' && LA13_36 <= '9')||(LA13_36 >= 'A' && LA13_36 <= 'Z')||LA13_36=='_'||(LA13_36 >= 'a' && LA13_36 <= 'z')) ) {s = 73;}

                        else if ( ((LA13_36 >= '\u0000' && LA13_36 <= ')')||(LA13_36 >= '+' && LA13_36 <= '-')||(LA13_36 >= ':' && LA13_36 <= '@')||(LA13_36 >= '[' && LA13_36 <= '^')||LA13_36=='`'||(LA13_36 >= '{' && LA13_36 <= '\uFFFF')) ) {s = 74;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;

                    case 1 : 
                        int LA13_72 = input.LA(1);

                        s = -1;
                        if ( (LA13_72=='/') ) {s = 104;}

                        else if ( (LA13_72=='*') ) {s = 72;}

                        else if ( (LA13_72=='.'||(LA13_72 >= '0' && LA13_72 <= '9')||(LA13_72 >= 'A' && LA13_72 <= 'Z')||LA13_72=='_'||(LA13_72 >= 'a' && LA13_72 <= 'z')) ) {s = 73;}

                        else if ( ((LA13_72 >= '\u0000' && LA13_72 <= ')')||(LA13_72 >= '+' && LA13_72 <= '-')||(LA13_72 >= ':' && LA13_72 <= '@')||(LA13_72 >= '[' && LA13_72 <= '^')||LA13_72=='`'||(LA13_72 >= '{' && LA13_72 <= '\uFFFF')) ) {s = 74;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;

                    case 2 : 
                        int LA13_104 = input.LA(1);

                        s = -1;
                        if ( (LA13_104=='*') ) {s = 72;}

                        else if ( ((LA13_104 >= '.' && LA13_104 <= '9')||(LA13_104 >= 'A' && LA13_104 <= 'Z')||LA13_104=='_'||(LA13_104 >= 'a' && LA13_104 <= 'z')) ) {s = 73;}

                        else if ( ((LA13_104 >= '\u0000' && LA13_104 <= ')')||(LA13_104 >= '+' && LA13_104 <= '-')||(LA13_104 >= ':' && LA13_104 <= '@')||(LA13_104 >= '[' && LA13_104 <= '^')||LA13_104=='`'||(LA13_104 >= '{' && LA13_104 <= '\uFFFF')) ) {s = 74;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;

                    case 3 : 
                        int LA13_73 = input.LA(1);

                        s = -1;
                        if ( (LA13_73=='*') ) {s = 72;}

                        else if ( ((LA13_73 >= '.' && LA13_73 <= '9')||(LA13_73 >= 'A' && LA13_73 <= 'Z')||LA13_73=='_'||(LA13_73 >= 'a' && LA13_73 <= 'z')) ) {s = 73;}

                        else if ( ((LA13_73 >= '\u0000' && LA13_73 <= ')')||(LA13_73 >= '+' && LA13_73 <= '-')||(LA13_73 >= ':' && LA13_73 <= '@')||(LA13_73 >= '[' && LA13_73 <= '^')||LA13_73=='`'||(LA13_73 >= '{' && LA13_73 <= '\uFFFF')) ) {s = 74;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;

                    case 4 : 
                        int LA13_71 = input.LA(1);

                        s = -1;
                        if ( ((LA13_71 >= '\u0000' && LA13_71 <= ')')||(LA13_71 >= '+' && LA13_71 <= '-')||(LA13_71 >= ':' && LA13_71 <= '@')||(LA13_71 >= '[' && LA13_71 <= '^')||LA13_71=='`'||(LA13_71 >= '{' && LA13_71 <= '\uFFFF')) ) {s = 70;}

                        else if ( (LA13_71=='*'||(LA13_71 >= '.' && LA13_71 <= '9')||(LA13_71 >= 'A' && LA13_71 <= 'Z')||LA13_71=='_'||(LA13_71 >= 'a' && LA13_71 <= 'z')) ) {s = 71;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;

                    case 5 : 
                        int LA13_35 = input.LA(1);

                        s = -1;
                        if ( ((LA13_35 >= '\u0000' && LA13_35 <= ')')||(LA13_35 >= '+' && LA13_35 <= '-')||(LA13_35 >= ':' && LA13_35 <= '@')||(LA13_35 >= '[' && LA13_35 <= '^')||LA13_35=='`'||(LA13_35 >= '{' && LA13_35 <= '\uFFFF')) ) {s = 70;}

                        else if ( (LA13_35=='*'||(LA13_35 >= '.' && LA13_35 <= '9')||(LA13_35 >= 'A' && LA13_35 <= 'Z')||LA13_35=='_'||(LA13_35 >= 'a' && LA13_35 <= 'z')) ) {s = 71;}

                        else s = 30;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 13, _s, input);
            error(nvae);
            throw nvae;
        }

    }
 

}