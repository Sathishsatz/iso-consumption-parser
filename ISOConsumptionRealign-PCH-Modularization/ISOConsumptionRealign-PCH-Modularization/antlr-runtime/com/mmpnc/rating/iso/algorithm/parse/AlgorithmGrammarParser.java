// $ANTLR 3.4 D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g 2015-05-18 10:10:02

  package com.mmpnc.rating.iso.algorithm.parse;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class AlgorithmGrammarParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "COMMENTS", "DIV", "EQUALS", "EXPONENT", "IDENTS", "INTEGER", "MULTI_COMMENT", "NEGATION", "NOT", "NOTEQUALS", "OR", "SPECIALFUNCTION", "STARTS", "STRING", "SUBTRACT", "VARCONSTANT", "WS", "XPATHCONSTANT", "'!='", "'&&'", "'('", "')'", "'*'", "'+'", "','", "'-'", "'/'", "':'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'And'", "'Call'", "'ELSE'", "'END IF'", "'END LOOP'", "'IF'", "'Is Equal To'", "'LOOP THROUGH'", "'NOT'", "'Negative'", "'Not Equal To'", "'Or'", "'RateTable'", "'StartsWith'", "'Subtract'", "'THEN'", "'Using'", "'mod'", "'{'", "'||'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int AND=4;
    public static final int COMMENTS=5;
    public static final int DIV=6;
    public static final int EQUALS=7;
    public static final int EXPONENT=8;
    public static final int IDENTS=9;
    public static final int INTEGER=10;
    public static final int MULTI_COMMENT=11;
    public static final int NEGATION=12;
    public static final int NOT=13;
    public static final int NOTEQUALS=14;
    public static final int OR=15;
    public static final int SPECIALFUNCTION=16;
    public static final int STARTS=17;
    public static final int STRING=18;
    public static final int SUBTRACT=19;
    public static final int VARCONSTANT=20;
    public static final int WS=21;
    public static final int XPATHCONSTANT=22;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public AlgorithmGrammarParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public AlgorithmGrammarParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return AlgorithmGrammarParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g"; }


    public static class algorithm_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "algorithm"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:29:1: algorithm : statements EOF !;
    public final AlgorithmGrammarParser.algorithm_return algorithm() throws RecognitionException {
        AlgorithmGrammarParser.algorithm_return retval = new AlgorithmGrammarParser.algorithm_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        AlgorithmGrammarParser.statements_return statements1 =null;


        CommonTree EOF2_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:29:11: ( statements EOF !)
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:30:5: statements EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_statements_in_algorithm110);
            statements1=statements();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statements1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_algorithm112); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            /*reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);*/
        	throw re;
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "algorithm"


    public static class statements_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statements"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:32:1: statements : statement ( statement )* ;
    public final AlgorithmGrammarParser.statements_return statements() throws RecognitionException {
        AlgorithmGrammarParser.statements_return retval = new AlgorithmGrammarParser.statements_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.statement_return statement3 =null;

        AlgorithmGrammarParser.statement_return statement4 =null;



        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:32:12: ( statement ( statement )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:9: statement ( statement )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_statement_in_statements129);
            statement3=statement();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statement3.getTree());

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:19: ( statement )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==IDENTS||LA1_0==VARCONSTANT||LA1_0==XPATHCONSTANT||LA1_0==40||LA1_0==44||LA1_0==46) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:33:19: statement
            	    {
            	    pushFollow(FOLLOW_statement_in_statements131);
            	    statement4=statement();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement4.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statements"


    public static class statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statement"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:36:1: statement : ( assignment | ifStatement | loopStatement | function | callProgram );
    public final AlgorithmGrammarParser.statement_return statement() throws RecognitionException {
        AlgorithmGrammarParser.statement_return retval = new AlgorithmGrammarParser.statement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.assignment_return assignment5 =null;

        AlgorithmGrammarParser.ifStatement_return ifStatement6 =null;

        AlgorithmGrammarParser.loopStatement_return loopStatement7 =null;

        AlgorithmGrammarParser.function_return function8 =null;

        AlgorithmGrammarParser.callProgram_return callProgram9 =null;



        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:36:11: ( assignment | ifStatement | loopStatement | function | callProgram )
            int alt2=5;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==25) ) {
                    alt2=4;
                }
                else if ( (LA2_1==35) ) {
                    alt2=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;

                }
                }
                break;
            case VARCONSTANT:
            case XPATHCONSTANT:
                {
                alt2=1;
                }
                break;
            case 44:
                {
                alt2=2;
                }
                break;
            case 46:
                {
                alt2=3;
                }
                break;
            case 40:
                {
                alt2=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:9: assignment
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_assignment_in_statement159);
                    assignment5=assignment();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment5.getTree());

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:22: ifStatement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_ifStatement_in_statement163);
                    ifStatement6=ifStatement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, ifStatement6.getTree());

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:36: loopStatement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_loopStatement_in_statement167);
                    loopStatement7=loopStatement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, loopStatement7.getTree());

                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:52: function
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_function_in_statement171);
                    function8=function();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, function8.getTree());

                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:37:63: callProgram
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_callProgram_in_statement175);
                    callProgram9=callProgram();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, callProgram9.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statement"


    public static class function_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "function"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:40:1: function : IDENTS ^ '(' ! ( ( 'Using' !)? ) ( arithmeticExp ( ',' arithmeticExp )* )? ')' !;
    public final AlgorithmGrammarParser.function_return function() throws RecognitionException {
        AlgorithmGrammarParser.function_return retval = new AlgorithmGrammarParser.function_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS10=null;
        Token char_literal11=null;
        Token string_literal12=null;
        Token char_literal14=null;
        Token char_literal16=null;
        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp13 =null;

        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp15 =null;


        CommonTree IDENTS10_tree=null;
        CommonTree char_literal11_tree=null;
        CommonTree string_literal12_tree=null;
        CommonTree char_literal14_tree=null;
        CommonTree char_literal16_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:40:10: ( IDENTS ^ '(' ! ( ( 'Using' !)? ) ( arithmeticExp ( ',' arithmeticExp )* )? ')' !)
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:9: IDENTS ^ '(' ! ( ( 'Using' !)? ) ( arithmeticExp ( ',' arithmeticExp )* )? ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENTS10=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_function201); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTS10_tree = 
            (CommonTree)adaptor.create(IDENTS10)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(IDENTS10_tree, root_0);
            }

            char_literal11=(Token)match(input,25,FOLLOW_25_in_function204); if (state.failed) return retval;

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:22: ( ( 'Using' !)? )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:23: ( 'Using' !)?
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:30: ( 'Using' !)?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==55) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:30: 'Using' !
                    {
                    string_literal12=(Token)match(input,55,FOLLOW_55_in_function208); if (state.failed) return retval;

                    }
                    break;

            }


            }


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:34: ( arithmeticExp ( ',' arithmeticExp )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0 >= IDENTS && LA5_0 <= INTEGER)||LA5_0==SPECIALFUNCTION||LA5_0==STRING||LA5_0==VARCONSTANT||LA5_0==XPATHCONSTANT||LA5_0==25||LA5_0==28||LA5_0==30||(LA5_0 >= 47 && LA5_0 <= 48)||LA5_0==51) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:35: arithmeticExp ( ',' arithmeticExp )*
                    {
                    pushFollow(FOLLOW_arithmeticExp_in_function214);
                    arithmeticExp13=arithmeticExp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp13.getTree());

                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:49: ( ',' arithmeticExp )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==29) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:41:50: ',' arithmeticExp
                    	    {
                    	    char_literal14=(Token)match(input,29,FOLLOW_29_in_function217); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    char_literal14_tree = 
                    	    (CommonTree)adaptor.create(char_literal14)
                    	    ;
                    	    adaptor.addChild(root_0, char_literal14_tree);
                    	    }

                    	    pushFollow(FOLLOW_arithmeticExp_in_function219);
                    	    arithmeticExp15=arithmeticExp();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp15.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal16=(Token)match(input,26,FOLLOW_26_in_function225); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "function"


    public static class domainTable_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "domainTable"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:44:1: domainTable : IDENTS ^ '{' ! ( atom ( ',' atom )* )? '}' !;
    public final AlgorithmGrammarParser.domainTable_return domainTable() throws RecognitionException {
        AlgorithmGrammarParser.domainTable_return retval = new AlgorithmGrammarParser.domainTable_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS17=null;
        Token char_literal18=null;
        Token char_literal20=null;
        Token char_literal22=null;
        AlgorithmGrammarParser.atom_return atom19 =null;

        AlgorithmGrammarParser.atom_return atom21 =null;


        CommonTree IDENTS17_tree=null;
        CommonTree char_literal18_tree=null;
        CommonTree char_literal20_tree=null;
        CommonTree char_literal22_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:44:13: ( IDENTS ^ '{' ! ( atom ( ',' atom )* )? '}' !)
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:45:9: IDENTS ^ '{' ! ( atom ( ',' atom )* )? '}' !
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENTS17=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_domainTable244); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTS17_tree = 
            (CommonTree)adaptor.create(IDENTS17)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(IDENTS17_tree, root_0);
            }

            char_literal18=(Token)match(input,57,FOLLOW_57_in_domainTable247); if (state.failed) return retval;

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:45:22: ( atom ( ',' atom )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0 >= IDENTS && LA7_0 <= INTEGER)||LA7_0==SPECIALFUNCTION||LA7_0==STRING||LA7_0==VARCONSTANT||LA7_0==XPATHCONSTANT||LA7_0==25||LA7_0==51) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:45:23: atom ( ',' atom )*
                    {
                    pushFollow(FOLLOW_atom_in_domainTable251);
                    atom19=atom();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, atom19.getTree());

                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:45:28: ( ',' atom )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==29) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:45:29: ',' atom
                    	    {
                    	    char_literal20=(Token)match(input,29,FOLLOW_29_in_domainTable254); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    char_literal20_tree = 
                    	    (CommonTree)adaptor.create(char_literal20)
                    	    ;
                    	    adaptor.addChild(root_0, char_literal20_tree);
                    	    }

                    	    pushFollow(FOLLOW_atom_in_domainTable256);
                    	    atom21=atom();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, atom21.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal22=(Token)match(input,59,FOLLOW_59_in_domainTable262); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "domainTable"


    public static class callProgram_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "callProgram"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:47:1: callProgram : 'Call' ^ IDENTS ;
    public final AlgorithmGrammarParser.callProgram_return callProgram() throws RecognitionException {
        AlgorithmGrammarParser.callProgram_return retval = new AlgorithmGrammarParser.callProgram_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal23=null;
        Token IDENTS24=null;

        CommonTree string_literal23_tree=null;
        CommonTree IDENTS24_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:47:13: ( 'Call' ^ IDENTS )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:48:9: 'Call' ^ IDENTS
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal23=(Token)match(input,40,FOLLOW_40_in_callProgram279); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal23_tree = 
            (CommonTree)adaptor.create(string_literal23)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal23_tree, root_0);
            }

            IDENTS24=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_callProgram282); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTS24_tree = 
            (CommonTree)adaptor.create(IDENTS24)
            ;
            adaptor.addChild(root_0, IDENTS24_tree);
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "callProgram"


    public static class specialFunction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "specialFunction"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:50:1: specialFunction : SPECIALFUNCTION ^ '(' ! arithmeticExp ')' !;
    public final AlgorithmGrammarParser.specialFunction_return specialFunction() throws RecognitionException {
        AlgorithmGrammarParser.specialFunction_return retval = new AlgorithmGrammarParser.specialFunction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SPECIALFUNCTION25=null;
        Token char_literal26=null;
        Token char_literal28=null;
        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp27 =null;


        CommonTree SPECIALFUNCTION25_tree=null;
        CommonTree char_literal26_tree=null;
        CommonTree char_literal28_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:50:17: ( SPECIALFUNCTION ^ '(' ! arithmeticExp ')' !)
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:51:13: SPECIALFUNCTION ^ '(' ! arithmeticExp ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            SPECIALFUNCTION25=(Token)match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_specialFunction303); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            SPECIALFUNCTION25_tree = 
            (CommonTree)adaptor.create(SPECIALFUNCTION25)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(SPECIALFUNCTION25_tree, root_0);
            }

            char_literal26=(Token)match(input,25,FOLLOW_25_in_specialFunction306); if (state.failed) return retval;

            pushFollow(FOLLOW_arithmeticExp_in_specialFunction309);
            arithmeticExp27=arithmeticExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp27.getTree());

            char_literal28=(Token)match(input,26,FOLLOW_26_in_specialFunction311); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "specialFunction"


    public static class rateFunction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "rateFunction"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:53:1: rateFunction : 'RateTable' ^ ':' ! function ;
    public final AlgorithmGrammarParser.rateFunction_return rateFunction() throws RecognitionException {
        AlgorithmGrammarParser.rateFunction_return retval = new AlgorithmGrammarParser.rateFunction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal29=null;
        Token char_literal30=null;
        AlgorithmGrammarParser.function_return function31 =null;


        CommonTree string_literal29_tree=null;
        CommonTree char_literal30_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:53:14: ( 'RateTable' ^ ':' ! function )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:54:13: 'RateTable' ^ ':' ! function
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal29=(Token)match(input,51,FOLLOW_51_in_rateFunction334); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal29_tree = 
            (CommonTree)adaptor.create(string_literal29)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal29_tree, root_0);
            }

            char_literal30=(Token)match(input,32,FOLLOW_32_in_rateFunction337); if (state.failed) return retval;

            pushFollow(FOLLOW_function_in_rateFunction340);
            function31=function();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, function31.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "rateFunction"


    public static class assignment_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "assignment"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:56:1: assignment : ( IDENTS | varType | xpath ) '=' ^ arithmeticExp ;
    public final AlgorithmGrammarParser.assignment_return assignment() throws RecognitionException {
        AlgorithmGrammarParser.assignment_return retval = new AlgorithmGrammarParser.assignment_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS32=null;
        Token char_literal35=null;
        AlgorithmGrammarParser.varType_return varType33 =null;

        AlgorithmGrammarParser.xpath_return xpath34 =null;

        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp36 =null;


        CommonTree IDENTS32_tree=null;
        CommonTree char_literal35_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:56:11: ( ( IDENTS | varType | xpath ) '=' ^ arithmeticExp )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:9: ( IDENTS | varType | xpath ) '=' ^ arithmeticExp
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:9: ( IDENTS | varType | xpath )
            int alt8=3;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                alt8=1;
                }
                break;
            case VARCONSTANT:
                {
                alt8=2;
                }
                break;
            case XPATHCONSTANT:
                {
                alt8=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }

            switch (alt8) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:10: IDENTS
                    {
                    IDENTS32=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_assignment364); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTS32_tree = 
                    (CommonTree)adaptor.create(IDENTS32)
                    ;
                    adaptor.addChild(root_0, IDENTS32_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:19: varType
                    {
                    pushFollow(FOLLOW_varType_in_assignment368);
                    varType33=varType();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, varType33.getTree());

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:57:29: xpath
                    {
                    pushFollow(FOLLOW_xpath_in_assignment372);
                    xpath34=xpath();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, xpath34.getTree());

                    }
                    break;

            }


            char_literal35=(Token)match(input,35,FOLLOW_35_in_assignment375); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal35_tree = 
            (CommonTree)adaptor.create(char_literal35)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(char_literal35_tree, root_0);
            }

            pushFollow(FOLLOW_arithmeticExp_in_assignment378);
            arithmeticExp36=arithmeticExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp36.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "assignment"


    public static class ifStatement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ifStatement"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:59:1: ifStatement : 'IF' ^ ( not )? '(' ! booleanExp ')' ! ( 'THEN' !)? statements ( 'ELSE' statements )? 'END IF' !;
    public final AlgorithmGrammarParser.ifStatement_return ifStatement() throws RecognitionException {
        AlgorithmGrammarParser.ifStatement_return retval = new AlgorithmGrammarParser.ifStatement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal37=null;
        Token char_literal39=null;
        Token char_literal41=null;
        Token string_literal42=null;
        Token string_literal44=null;
        Token string_literal46=null;
        AlgorithmGrammarParser.not_return not38 =null;

        AlgorithmGrammarParser.booleanExp_return booleanExp40 =null;

        AlgorithmGrammarParser.statements_return statements43 =null;

        AlgorithmGrammarParser.statements_return statements45 =null;


        CommonTree string_literal37_tree=null;
        CommonTree char_literal39_tree=null;
        CommonTree char_literal41_tree=null;
        CommonTree string_literal42_tree=null;
        CommonTree string_literal44_tree=null;
        CommonTree string_literal46_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:59:12: ( 'IF' ^ ( not )? '(' ! booleanExp ')' ! ( 'THEN' !)? statements ( 'ELSE' statements )? 'END IF' !)
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:60:9: 'IF' ^ ( not )? '(' ! booleanExp ')' ! ( 'THEN' !)? statements ( 'ELSE' statements )? 'END IF' !
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal37=(Token)match(input,44,FOLLOW_44_in_ifStatement403); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal37_tree = 
            (CommonTree)adaptor.create(string_literal37)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal37_tree, root_0);
            }

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:60:15: ( not )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==47) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:60:16: not
                    {
                    pushFollow(FOLLOW_not_in_ifStatement407);
                    not38=not();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, not38.getTree());

                    }
                    break;

            }


            char_literal39=(Token)match(input,25,FOLLOW_25_in_ifStatement411); if (state.failed) return retval;

            pushFollow(FOLLOW_booleanExp_in_ifStatement414);
            booleanExp40=booleanExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, booleanExp40.getTree());

            char_literal41=(Token)match(input,26,FOLLOW_26_in_ifStatement416); if (state.failed) return retval;

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:60:43: ( 'THEN' !)?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==54) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:60:44: 'THEN' !
                    {
                    string_literal42=(Token)match(input,54,FOLLOW_54_in_ifStatement420); if (state.failed) return retval;

                    }
                    break;

            }


            pushFollow(FOLLOW_statements_in_ifStatement425);
            statements43=statements();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statements43.getTree());

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:61:9: ( 'ELSE' statements )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==41) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:61:10: 'ELSE' statements
                    {
                    string_literal44=(Token)match(input,41,FOLLOW_41_in_ifStatement437); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal44_tree = 
                    (CommonTree)adaptor.create(string_literal44)
                    ;
                    adaptor.addChild(root_0, string_literal44_tree);
                    }

                    pushFollow(FOLLOW_statements_in_ifStatement439);
                    statements45=statements();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statements45.getTree());

                    }
                    break;

            }


            string_literal46=(Token)match(input,42,FOLLOW_42_in_ifStatement455); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ifStatement"


    public static class loopStatement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loopStatement"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:65:1: loopStatement : 'LOOP THROUGH' ^ STRING statements 'END LOOP' !;
    public final AlgorithmGrammarParser.loopStatement_return loopStatement() throws RecognitionException {
        AlgorithmGrammarParser.loopStatement_return retval = new AlgorithmGrammarParser.loopStatement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal47=null;
        Token STRING48=null;
        Token string_literal50=null;
        AlgorithmGrammarParser.statements_return statements49 =null;


        CommonTree string_literal47_tree=null;
        CommonTree STRING48_tree=null;
        CommonTree string_literal50_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:65:15: ( 'LOOP THROUGH' ^ STRING statements 'END LOOP' !)
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:66:9: 'LOOP THROUGH' ^ STRING statements 'END LOOP' !
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal47=(Token)match(input,46,FOLLOW_46_in_loopStatement481); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal47_tree = 
            (CommonTree)adaptor.create(string_literal47)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(string_literal47_tree, root_0);
            }

            STRING48=(Token)match(input,STRING,FOLLOW_STRING_in_loopStatement484); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            STRING48_tree = 
            (CommonTree)adaptor.create(STRING48)
            ;
            adaptor.addChild(root_0, STRING48_tree);
            }

            pushFollow(FOLLOW_statements_in_loopStatement494);
            statements49=statements();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, statements49.getTree());

            string_literal50=(Token)match(input,43,FOLLOW_43_in_loopStatement504); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loopStatement"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:1: atom : ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable ) ;
    public final AlgorithmGrammarParser.atom_return atom() throws RecognitionException {
        AlgorithmGrammarParser.atom_return retval = new AlgorithmGrammarParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENTS51=null;
        Token INTEGER54=null;
        Token STRING55=null;
        AlgorithmGrammarParser.function_return function52 =null;

        AlgorithmGrammarParser.expression_return expression53 =null;

        AlgorithmGrammarParser.xpath_return xpath56 =null;

        AlgorithmGrammarParser.rateFunction_return rateFunction57 =null;

        AlgorithmGrammarParser.varType_return varType58 =null;

        AlgorithmGrammarParser.domainTable_return domainTable59 =null;


        CommonTree IDENTS51_tree=null;
        CommonTree INTEGER54_tree=null;
        CommonTree STRING55_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:6: ( ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:8: ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable )
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:8: ( IDENTS | function | expression | INTEGER | STRING | xpath | rateFunction | varType | domainTable )
            int alt12=9;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                switch ( input.LA(2) ) {
                case 25:
                    {
                    alt12=2;
                    }
                    break;
                case 57:
                    {
                    alt12=9;
                    }
                    break;
                case EOF:
                case IDENTS:
                case VARCONSTANT:
                case XPATHCONSTANT:
                case 23:
                case 24:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 33:
                case 34:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 49:
                case 50:
                case 52:
                case 53:
                case 56:
                case 58:
                case 59:
                    {
                    alt12=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 1, input);

                    throw nvae;

                }

                }
                break;
            case SPECIALFUNCTION:
            case 25:
                {
                alt12=3;
                }
                break;
            case INTEGER:
                {
                alt12=4;
                }
                break;
            case STRING:
                {
                alt12=5;
                }
                break;
            case XPATHCONSTANT:
                {
                alt12=6;
                }
                break;
            case 51:
                {
                alt12=7;
                }
                break;
            case VARCONSTANT:
                {
                alt12=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }

            switch (alt12) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:10: IDENTS
                    {
                    IDENTS51=(Token)match(input,IDENTS,FOLLOW_IDENTS_in_atom539); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTS51_tree = 
                    (CommonTree)adaptor.create(IDENTS51)
                    ;
                    adaptor.addChild(root_0, IDENTS51_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:19: function
                    {
                    pushFollow(FOLLOW_function_in_atom543);
                    function52=function();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, function52.getTree());

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:30: expression
                    {
                    pushFollow(FOLLOW_expression_in_atom547);
                    expression53=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression53.getTree());

                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:43: INTEGER
                    {
                    INTEGER54=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_atom551); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    INTEGER54_tree = 
                    (CommonTree)adaptor.create(INTEGER54)
                    ;
                    adaptor.addChild(root_0, INTEGER54_tree);
                    }

                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:53: STRING
                    {
                    STRING55=(Token)match(input,STRING,FOLLOW_STRING_in_atom555); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    STRING55_tree = 
                    (CommonTree)adaptor.create(STRING55)
                    ;
                    adaptor.addChild(root_0, STRING55_tree);
                    }

                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:62: xpath
                    {
                    pushFollow(FOLLOW_xpath_in_atom559);
                    xpath56=xpath();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, xpath56.getTree());

                    }
                    break;
                case 7 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:70: rateFunction
                    {
                    pushFollow(FOLLOW_rateFunction_in_atom563);
                    rateFunction57=rateFunction();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, rateFunction57.getTree());

                    }
                    break;
                case 8 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:85: varType
                    {
                    pushFollow(FOLLOW_varType_in_atom567);
                    varType58=varType();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, varType58.getTree());

                    }
                    break;
                case 9 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:71:95: domainTable
                    {
                    pushFollow(FOLLOW_domainTable_in_atom571);
                    domainTable59=domainTable();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, domainTable59.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"


    public static class negation_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "negation"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:73:1: negation : ( not ^)? atom ;
    public final AlgorithmGrammarParser.negation_return negation() throws RecognitionException {
        AlgorithmGrammarParser.negation_return retval = new AlgorithmGrammarParser.negation_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.not_return not60 =null;

        AlgorithmGrammarParser.atom_return atom61 =null;



        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:73:10: ( ( not ^)? atom )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:74:9: ( not ^)? atom
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:74:9: ( not ^)?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==47) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:74:10: not ^
                    {
                    pushFollow(FOLLOW_not_in_negation589);
                    not60=not();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(not60.getTree(), root_0);

                    }
                    break;

            }


            pushFollow(FOLLOW_atom_in_negation594);
            atom61=atom();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, atom61.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "negation"


    public static class not_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "not"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:77:1: not : ( 'NOT' ) -> NOT ;
    public final AlgorithmGrammarParser.not_return not() throws RecognitionException {
        AlgorithmGrammarParser.not_return retval = new AlgorithmGrammarParser.not_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal62=null;

        CommonTree string_literal62_tree=null;
        RewriteRuleTokenStream stream_47=new RewriteRuleTokenStream(adaptor,"token 47");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:77:5: ( ( 'NOT' ) -> NOT )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:78:3: ( 'NOT' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:78:3: ( 'NOT' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:78:4: 'NOT'
            {
            string_literal62=(Token)match(input,47,FOLLOW_47_in_not614); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_47.add(string_literal62);


            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 78:11: -> NOT
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NOT, "NOT")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "not"


    public static class unary_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unary"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:81:1: unary : ( ( '+' !| negative ^) )* negation ;
    public final AlgorithmGrammarParser.unary_return unary() throws RecognitionException {
        AlgorithmGrammarParser.unary_return retval = new AlgorithmGrammarParser.unary_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal63=null;
        AlgorithmGrammarParser.negative_return negative64 =null;

        AlgorithmGrammarParser.negation_return negation65 =null;


        CommonTree char_literal63_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:81:7: ( ( ( '+' !| negative ^) )* negation )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:5: ( ( '+' !| negative ^) )* negation
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:5: ( ( '+' !| negative ^) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==28||LA15_0==30||LA15_0==48) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:6: ( '+' !| negative ^)
            	    {
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:6: ( '+' !| negative ^)
            	    int alt14=2;
            	    int LA14_0 = input.LA(1);

            	    if ( (LA14_0==28) ) {
            	        alt14=1;
            	    }
            	    else if ( (LA14_0==30||LA14_0==48) ) {
            	        alt14=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 14, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt14) {
            	        case 1 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:7: '+' !
            	            {
            	            char_literal63=(Token)match(input,28,FOLLOW_28_in_unary636); if (state.failed) return retval;

            	            }
            	            break;
            	        case 2 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:82:14: negative ^
            	            {
            	            pushFollow(FOLLOW_negative_in_unary641);
            	            negative64=negative();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(negative64.getTree(), root_0);

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            pushFollow(FOLLOW_negation_in_unary647);
            negation65=negation();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, negation65.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unary"


    public static class multi_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multi"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:85:1: multi : unary ( ( '*' ^| '/' ^| 'mod' ^) unary )* ;
    public final AlgorithmGrammarParser.multi_return multi() throws RecognitionException {
        AlgorithmGrammarParser.multi_return retval = new AlgorithmGrammarParser.multi_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal67=null;
        Token char_literal68=null;
        Token string_literal69=null;
        AlgorithmGrammarParser.unary_return unary66 =null;

        AlgorithmGrammarParser.unary_return unary70 =null;


        CommonTree char_literal67_tree=null;
        CommonTree char_literal68_tree=null;
        CommonTree string_literal69_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:85:7: ( unary ( ( '*' ^| '/' ^| 'mod' ^) unary )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:5: unary ( ( '*' ^| '/' ^| 'mod' ^) unary )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_unary_in_multi668);
            unary66=unary();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, unary66.getTree());

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:11: ( ( '*' ^| '/' ^| 'mod' ^) unary )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==27||LA17_0==31||LA17_0==56) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:12: ( '*' ^| '/' ^| 'mod' ^) unary
            	    {
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:12: ( '*' ^| '/' ^| 'mod' ^)
            	    int alt16=3;
            	    switch ( input.LA(1) ) {
            	    case 27:
            	        {
            	        alt16=1;
            	        }
            	        break;
            	    case 31:
            	        {
            	        alt16=2;
            	        }
            	        break;
            	    case 56:
            	        {
            	        alt16=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 16, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt16) {
            	        case 1 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:14: '*' ^
            	            {
            	            char_literal67=(Token)match(input,27,FOLLOW_27_in_multi673); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal67_tree = 
            	            (CommonTree)adaptor.create(char_literal67)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal67_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:21: '/' ^
            	            {
            	            char_literal68=(Token)match(input,31,FOLLOW_31_in_multi678); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal68_tree = 
            	            (CommonTree)adaptor.create(char_literal68)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal68_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 3 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:86:28: 'mod' ^
            	            {
            	            string_literal69=(Token)match(input,56,FOLLOW_56_in_multi683); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal69_tree = 
            	            (CommonTree)adaptor.create(string_literal69)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal69_tree, root_0);
            	            }

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_unary_in_multi688);
            	    unary70=unary();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary70.getTree());

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multi"


    public static class arithmeticExp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arithmeticExp"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:89:1: arithmeticExp : multi ( ( '+' ^| subtraction ^) multi )* ;
    public final AlgorithmGrammarParser.arithmeticExp_return arithmeticExp() throws RecognitionException {
        AlgorithmGrammarParser.arithmeticExp_return retval = new AlgorithmGrammarParser.arithmeticExp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal72=null;
        AlgorithmGrammarParser.multi_return multi71 =null;

        AlgorithmGrammarParser.subtraction_return subtraction73 =null;

        AlgorithmGrammarParser.multi_return multi74 =null;


        CommonTree char_literal72_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:89:15: ( multi ( ( '+' ^| subtraction ^) multi )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:5: multi ( ( '+' ^| subtraction ^) multi )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multi_in_arithmeticExp709);
            multi71=multi();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, multi71.getTree());

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:11: ( ( '+' ^| subtraction ^) multi )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==28||LA19_0==30||LA19_0==53) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:12: ( '+' ^| subtraction ^) multi
            	    {
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:12: ( '+' ^| subtraction ^)
            	    int alt18=2;
            	    int LA18_0 = input.LA(1);

            	    if ( (LA18_0==28) ) {
            	        alt18=1;
            	    }
            	    else if ( (LA18_0==30||LA18_0==53) ) {
            	        alt18=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 18, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt18) {
            	        case 1 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:14: '+' ^
            	            {
            	            char_literal72=(Token)match(input,28,FOLLOW_28_in_arithmeticExp714); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal72_tree = 
            	            (CommonTree)adaptor.create(char_literal72)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal72_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:90:21: subtraction ^
            	            {
            	            pushFollow(FOLLOW_subtraction_in_arithmeticExp719);
            	            subtraction73=subtraction();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(subtraction73.getTree(), root_0);

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_multi_in_arithmeticExp724);
            	    multi74=multi();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, multi74.getTree());

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arithmeticExp"


    public static class relation_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relation"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:93:1: relation : arithmeticExp ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )* ;
    public final AlgorithmGrammarParser.relation_return relation() throws RecognitionException {
        AlgorithmGrammarParser.relation_return retval = new AlgorithmGrammarParser.relation_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal78=null;
        Token string_literal79=null;
        Token char_literal80=null;
        Token string_literal81=null;
        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp75 =null;

        AlgorithmGrammarParser.equals_return equals76 =null;

        AlgorithmGrammarParser.notequals_return notequals77 =null;

        AlgorithmGrammarParser.arithmeticExp_return arithmeticExp82 =null;


        CommonTree char_literal78_tree=null;
        CommonTree string_literal79_tree=null;
        CommonTree char_literal80_tree=null;
        CommonTree string_literal81_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:93:10: ( arithmeticExp ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:5: arithmeticExp ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_arithmeticExp_in_relation744);
            arithmeticExp75=arithmeticExp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp75.getTree());

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:19: ( ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==23||(LA21_0 >= 33 && LA21_0 <= 34)||(LA21_0 >= 36 && LA21_0 <= 38)||LA21_0==45||LA21_0==49) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:20: ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^) arithmeticExp
            	    {
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:20: ( equals ^| notequals ^| '<' ^| '<=' ^| '>' ^| '>=' ^)
            	    int alt20=6;
            	    switch ( input.LA(1) ) {
            	    case 36:
            	    case 45:
            	        {
            	        alt20=1;
            	        }
            	        break;
            	    case 23:
            	    case 49:
            	        {
            	        alt20=2;
            	        }
            	        break;
            	    case 33:
            	        {
            	        alt20=3;
            	        }
            	        break;
            	    case 34:
            	        {
            	        alt20=4;
            	        }
            	        break;
            	    case 37:
            	        {
            	        alt20=5;
            	        }
            	        break;
            	    case 38:
            	        {
            	        alt20=6;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 20, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt20) {
            	        case 1 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:22: equals ^
            	            {
            	            pushFollow(FOLLOW_equals_in_relation749);
            	            equals76=equals();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(equals76.getTree(), root_0);

            	            }
            	            break;
            	        case 2 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:32: notequals ^
            	            {
            	            pushFollow(FOLLOW_notequals_in_relation754);
            	            notequals77=notequals();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(notequals77.getTree(), root_0);

            	            }
            	            break;
            	        case 3 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:45: '<' ^
            	            {
            	            char_literal78=(Token)match(input,33,FOLLOW_33_in_relation759); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal78_tree = 
            	            (CommonTree)adaptor.create(char_literal78)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal78_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 4 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:52: '<=' ^
            	            {
            	            string_literal79=(Token)match(input,34,FOLLOW_34_in_relation764); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal79_tree = 
            	            (CommonTree)adaptor.create(string_literal79)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal79_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 5 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:60: '>' ^
            	            {
            	            char_literal80=(Token)match(input,37,FOLLOW_37_in_relation769); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal80_tree = 
            	            (CommonTree)adaptor.create(char_literal80)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(char_literal80_tree, root_0);
            	            }

            	            }
            	            break;
            	        case 6 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:94:67: '>=' ^
            	            {
            	            string_literal81=(Token)match(input,38,FOLLOW_38_in_relation774); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            string_literal81_tree = 
            	            (CommonTree)adaptor.create(string_literal81)
            	            ;
            	            root_0 = (CommonTree)adaptor.becomeRoot(string_literal81_tree, root_0);
            	            }

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_arithmeticExp_in_relation780);
            	    arithmeticExp82=arithmeticExp();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, arithmeticExp82.getTree());

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relation"


    public static class equals_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equals"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:97:1: equals : ( 'Is Equal To' | '==' ) -> EQUALS ;
    public final AlgorithmGrammarParser.equals_return equals() throws RecognitionException {
        AlgorithmGrammarParser.equals_return retval = new AlgorithmGrammarParser.equals_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal83=null;
        Token string_literal84=null;

        CommonTree string_literal83_tree=null;
        CommonTree string_literal84_tree=null;
        RewriteRuleTokenStream stream_45=new RewriteRuleTokenStream(adaptor,"token 45");
        RewriteRuleTokenStream stream_36=new RewriteRuleTokenStream(adaptor,"token 36");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:97:7: ( ( 'Is Equal To' | '==' ) -> EQUALS )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:98:5: ( 'Is Equal To' | '==' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:98:5: ( 'Is Equal To' | '==' )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==45) ) {
                alt22=1;
            }
            else if ( (LA22_0==36) ) {
                alt22=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;

            }
            switch (alt22) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:98:6: 'Is Equal To'
                    {
                    string_literal83=(Token)match(input,45,FOLLOW_45_in_equals804); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_45.add(string_literal83);


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:98:22: '=='
                    {
                    string_literal84=(Token)match(input,36,FOLLOW_36_in_equals808); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_36.add(string_literal84);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 98:28: -> EQUALS
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(EQUALS, "EQUALS")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equals"


    public static class notequals_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "notequals"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:101:1: notequals : ( 'Not Equal To' | '!=' ) -> NOTEQUALS ;
    public final AlgorithmGrammarParser.notequals_return notequals() throws RecognitionException {
        AlgorithmGrammarParser.notequals_return retval = new AlgorithmGrammarParser.notequals_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal85=null;
        Token string_literal86=null;

        CommonTree string_literal85_tree=null;
        CommonTree string_literal86_tree=null;
        RewriteRuleTokenStream stream_49=new RewriteRuleTokenStream(adaptor,"token 49");
        RewriteRuleTokenStream stream_23=new RewriteRuleTokenStream(adaptor,"token 23");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:101:10: ( ( 'Not Equal To' | '!=' ) -> NOTEQUALS )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:102:5: ( 'Not Equal To' | '!=' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:102:5: ( 'Not Equal To' | '!=' )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==49) ) {
                alt23=1;
            }
            else if ( (LA23_0==23) ) {
                alt23=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;

            }
            switch (alt23) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:102:6: 'Not Equal To'
                    {
                    string_literal85=(Token)match(input,49,FOLLOW_49_in_notequals830); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_49.add(string_literal85);


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:102:22: '!='
                    {
                    string_literal86=(Token)match(input,23,FOLLOW_23_in_notequals833); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_23.add(string_literal86);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 102:28: -> NOTEQUALS
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NOTEQUALS, "NOTEQUALS")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "notequals"


    public static class negative_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "negative"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:105:1: negative : ( '-' | 'Negative' ) -> NEGATION ;
    public final AlgorithmGrammarParser.negative_return negative() throws RecognitionException {
        AlgorithmGrammarParser.negative_return retval = new AlgorithmGrammarParser.negative_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal87=null;
        Token string_literal88=null;

        CommonTree char_literal87_tree=null;
        CommonTree string_literal88_tree=null;
        RewriteRuleTokenStream stream_48=new RewriteRuleTokenStream(adaptor,"token 48");
        RewriteRuleTokenStream stream_30=new RewriteRuleTokenStream(adaptor,"token 30");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:105:10: ( ( '-' | 'Negative' ) -> NEGATION )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:106:5: ( '-' | 'Negative' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:106:5: ( '-' | 'Negative' )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==30) ) {
                alt24=1;
            }
            else if ( (LA24_0==48) ) {
                alt24=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }
            switch (alt24) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:106:6: '-'
                    {
                    char_literal87=(Token)match(input,30,FOLLOW_30_in_negative856); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_30.add(char_literal87);


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:106:10: 'Negative'
                    {
                    string_literal88=(Token)match(input,48,FOLLOW_48_in_negative858); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_48.add(string_literal88);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 106:22: -> NEGATION
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(NEGATION, "NEGATION")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "negative"


    public static class subtraction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "subtraction"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:109:1: subtraction : ( '-' | 'Subtract' ) -> SUBTRACT ;
    public final AlgorithmGrammarParser.subtraction_return subtraction() throws RecognitionException {
        AlgorithmGrammarParser.subtraction_return retval = new AlgorithmGrammarParser.subtraction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal89=null;
        Token string_literal90=null;

        CommonTree char_literal89_tree=null;
        CommonTree string_literal90_tree=null;
        RewriteRuleTokenStream stream_30=new RewriteRuleTokenStream(adaptor,"token 30");
        RewriteRuleTokenStream stream_53=new RewriteRuleTokenStream(adaptor,"token 53");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:109:13: ( ( '-' | 'Subtract' ) -> SUBTRACT )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:5: ( '-' | 'Subtract' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:5: ( '-' | 'Subtract' )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==30) ) {
                alt25=1;
            }
            else if ( (LA25_0==53) ) {
                alt25=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;

            }
            switch (alt25) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:6: '-'
                    {
                    char_literal89=(Token)match(input,30,FOLLOW_30_in_subtraction885); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_30.add(char_literal89);


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:110:10: 'Subtract'
                    {
                    string_literal90=(Token)match(input,53,FOLLOW_53_in_subtraction887); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_53.add(string_literal90);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 110:22: -> SUBTRACT
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(SUBTRACT, "SUBTRACT")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "subtraction"


    public static class andOperator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "andOperator"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:113:1: andOperator : ( 'And' | '&&' ) -> AND ;
    public final AlgorithmGrammarParser.andOperator_return andOperator() throws RecognitionException {
        AlgorithmGrammarParser.andOperator_return retval = new AlgorithmGrammarParser.andOperator_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal91=null;
        Token string_literal92=null;

        CommonTree string_literal91_tree=null;
        CommonTree string_literal92_tree=null;
        RewriteRuleTokenStream stream_24=new RewriteRuleTokenStream(adaptor,"token 24");
        RewriteRuleTokenStream stream_39=new RewriteRuleTokenStream(adaptor,"token 39");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:113:13: ( ( 'And' | '&&' ) -> AND )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:114:5: ( 'And' | '&&' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:114:5: ( 'And' | '&&' )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==39) ) {
                alt26=1;
            }
            else if ( (LA26_0==24) ) {
                alt26=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;

            }
            switch (alt26) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:114:6: 'And'
                    {
                    string_literal91=(Token)match(input,39,FOLLOW_39_in_andOperator910); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_39.add(string_literal91);


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:114:14: '&&'
                    {
                    string_literal92=(Token)match(input,24,FOLLOW_24_in_andOperator914); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_24.add(string_literal92);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 114:20: -> AND
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(AND, "AND")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "andOperator"


    public static class orOperator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orOperator"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:117:1: orOperator : ( 'Or' | '||' ) -> OR ;
    public final AlgorithmGrammarParser.orOperator_return orOperator() throws RecognitionException {
        AlgorithmGrammarParser.orOperator_return retval = new AlgorithmGrammarParser.orOperator_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal93=null;
        Token string_literal94=null;

        CommonTree string_literal93_tree=null;
        CommonTree string_literal94_tree=null;
        RewriteRuleTokenStream stream_58=new RewriteRuleTokenStream(adaptor,"token 58");
        RewriteRuleTokenStream stream_50=new RewriteRuleTokenStream(adaptor,"token 50");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:117:12: ( ( 'Or' | '||' ) -> OR )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:5: ( 'Or' | '||' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:5: ( 'Or' | '||' )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==50) ) {
                alt27=1;
            }
            else if ( (LA27_0==58) ) {
                alt27=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;

            }
            switch (alt27) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:6: 'Or'
                    {
                    string_literal93=(Token)match(input,50,FOLLOW_50_in_orOperator941); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_50.add(string_literal93);


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:118:13: '||'
                    {
                    string_literal94=(Token)match(input,58,FOLLOW_58_in_orOperator945); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_58.add(string_literal94);


                    }
                    break;

            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 118:19: -> OR
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(OR, "OR")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orOperator"


    public static class startsOperator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "startsOperator"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:1: startsOperator : ( 'StartsWith' ) -> STARTS ;
    public final AlgorithmGrammarParser.startsOperator_return startsOperator() throws RecognitionException {
        AlgorithmGrammarParser.startsOperator_return retval = new AlgorithmGrammarParser.startsOperator_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal95=null;

        CommonTree string_literal95_tree=null;
        RewriteRuleTokenStream stream_52=new RewriteRuleTokenStream(adaptor,"token 52");

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:121:16: ( ( 'StartsWith' ) -> STARTS )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:5: ( 'StartsWith' )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:5: ( 'StartsWith' )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:122:6: 'StartsWith'
            {
            string_literal95=(Token)match(input,52,FOLLOW_52_in_startsOperator970); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_52.add(string_literal95);


            }


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {

            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 122:20: -> STARTS
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(STARTS, "STARTS")
                );

            }


            retval.tree = root_0;
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "startsOperator"


    public static class booleanExp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "booleanExp"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:1: booleanExp : relation ( ( andOperator ^| orOperator ^| startsOperator ^) relation )* ;
    public final AlgorithmGrammarParser.booleanExp_return booleanExp() throws RecognitionException {
        AlgorithmGrammarParser.booleanExp_return retval = new AlgorithmGrammarParser.booleanExp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        AlgorithmGrammarParser.relation_return relation96 =null;

        AlgorithmGrammarParser.andOperator_return andOperator97 =null;

        AlgorithmGrammarParser.orOperator_return orOperator98 =null;

        AlgorithmGrammarParser.startsOperator_return startsOperator99 =null;

        AlgorithmGrammarParser.relation_return relation100 =null;



        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:12: ( relation ( ( andOperator ^| orOperator ^| startsOperator ^) relation )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:14: relation ( ( andOperator ^| orOperator ^| startsOperator ^) relation )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_relation_in_booleanExp990);
            relation96=relation();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, relation96.getTree());

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:23: ( ( andOperator ^| orOperator ^| startsOperator ^) relation )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==24||LA29_0==39||LA29_0==50||LA29_0==52||LA29_0==58) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:24: ( andOperator ^| orOperator ^| startsOperator ^) relation
            	    {
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:24: ( andOperator ^| orOperator ^| startsOperator ^)
            	    int alt28=3;
            	    switch ( input.LA(1) ) {
            	    case 24:
            	    case 39:
            	        {
            	        alt28=1;
            	        }
            	        break;
            	    case 50:
            	    case 58:
            	        {
            	        alt28=2;
            	        }
            	        break;
            	    case 52:
            	        {
            	        alt28=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 28, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt28) {
            	        case 1 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:26: andOperator ^
            	            {
            	            pushFollow(FOLLOW_andOperator_in_booleanExp995);
            	            andOperator97=andOperator();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(andOperator97.getTree(), root_0);

            	            }
            	            break;
            	        case 2 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:41: orOperator ^
            	            {
            	            pushFollow(FOLLOW_orOperator_in_booleanExp1000);
            	            orOperator98=orOperator();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(orOperator98.getTree(), root_0);

            	            }
            	            break;
            	        case 3 :
            	            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:125:55: startsOperator ^
            	            {
            	            pushFollow(FOLLOW_startsOperator_in_booleanExp1005);
            	            startsOperator99=startsOperator();

            	            state._fsp--;
            	            if (state.failed) return retval;
            	            if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(startsOperator99.getTree(), root_0);

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_relation_in_booleanExp1009);
            	    relation100=relation();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, relation100.getTree());

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "booleanExp"


    public static class expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:127:1: expression : ( ( SPECIALFUNCTION '(' arithmeticExp ')' )=> specialFunction | '(' ! booleanExp ')' !);
    public final AlgorithmGrammarParser.expression_return expression() throws RecognitionException {
        AlgorithmGrammarParser.expression_return retval = new AlgorithmGrammarParser.expression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal102=null;
        Token char_literal104=null;
        AlgorithmGrammarParser.specialFunction_return specialFunction101 =null;

        AlgorithmGrammarParser.booleanExp_return booleanExp103 =null;


        CommonTree char_literal102_tree=null;
        CommonTree char_literal104_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:5: ( ( SPECIALFUNCTION '(' arithmeticExp ')' )=> specialFunction | '(' ! booleanExp ')' !)
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==SPECIALFUNCTION) && (synpred1_AlgorithmGrammar())) {
                alt30=1;
            }
            else if ( (LA30_0==25) ) {
                alt30=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;

            }
            switch (alt30) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:7: ( SPECIALFUNCTION '(' arithmeticExp ')' )=> specialFunction
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_specialFunction_in_expression1036);
                    specialFunction101=specialFunction();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, specialFunction101.getTree());

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:129:7: '(' ! booleanExp ')' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal102=(Token)match(input,25,FOLLOW_25_in_expression1044); if (state.failed) return retval;

                    pushFollow(FOLLOW_booleanExp_in_expression1047);
                    booleanExp103=booleanExp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, booleanExp103.getTree());

                    char_literal104=(Token)match(input,26,FOLLOW_26_in_expression1049); if (state.failed) return retval;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class xpath_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "xpath"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:131:1: xpath : XPATHCONSTANT ':' STRING ;
    public final AlgorithmGrammarParser.xpath_return xpath() throws RecognitionException {
        AlgorithmGrammarParser.xpath_return retval = new AlgorithmGrammarParser.xpath_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token XPATHCONSTANT105=null;
        Token char_literal106=null;
        Token STRING107=null;

        CommonTree XPATHCONSTANT105_tree=null;
        CommonTree char_literal106_tree=null;
        CommonTree STRING107_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:131:7: ( XPATHCONSTANT ':' STRING )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:131:9: XPATHCONSTANT ':' STRING
            {
            root_0 = (CommonTree)adaptor.nil();


            XPATHCONSTANT105=(Token)match(input,XPATHCONSTANT,FOLLOW_XPATHCONSTANT_in_xpath1058); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            XPATHCONSTANT105_tree = 
            (CommonTree)adaptor.create(XPATHCONSTANT105)
            ;
            adaptor.addChild(root_0, XPATHCONSTANT105_tree);
            }

            char_literal106=(Token)match(input,32,FOLLOW_32_in_xpath1060); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal106_tree = 
            (CommonTree)adaptor.create(char_literal106)
            ;
            adaptor.addChild(root_0, char_literal106_tree);
            }

            STRING107=(Token)match(input,STRING,FOLLOW_STRING_in_xpath1062); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            STRING107_tree = 
            (CommonTree)adaptor.create(STRING107)
            ;
            adaptor.addChild(root_0, STRING107_tree);
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "xpath"


    public static class varType_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "varType"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:133:1: varType : VARCONSTANT ':' ( IDENTS | STRING ) ;
    public final AlgorithmGrammarParser.varType_return varType() throws RecognitionException {
        AlgorithmGrammarParser.varType_return retval = new AlgorithmGrammarParser.varType_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token VARCONSTANT108=null;
        Token char_literal109=null;
        Token set110=null;

        CommonTree VARCONSTANT108_tree=null;
        CommonTree char_literal109_tree=null;
        CommonTree set110_tree=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:133:9: ( VARCONSTANT ':' ( IDENTS | STRING ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:133:11: VARCONSTANT ':' ( IDENTS | STRING )
            {
            root_0 = (CommonTree)adaptor.nil();


            VARCONSTANT108=(Token)match(input,VARCONSTANT,FOLLOW_VARCONSTANT_in_varType1071); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            VARCONSTANT108_tree = 
            (CommonTree)adaptor.create(VARCONSTANT108)
            ;
            adaptor.addChild(root_0, VARCONSTANT108_tree);
            }

            char_literal109=(Token)match(input,32,FOLLOW_32_in_varType1073); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal109_tree = 
            (CommonTree)adaptor.create(char_literal109)
            ;
            adaptor.addChild(root_0, char_literal109_tree);
            }

            set110=(Token)input.LT(1);

            if ( input.LA(1)==IDENTS||input.LA(1)==STRING ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set110)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "varType"

    // $ANTLR start synpred1_AlgorithmGrammar
    public final void synpred1_AlgorithmGrammar_fragment() throws RecognitionException {
        // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:7: ( SPECIALFUNCTION '(' arithmeticExp ')' )
        // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmGrammar.g:128:8: SPECIALFUNCTION '(' arithmeticExp ')'
        {
        match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_synpred1_AlgorithmGrammar1026); if (state.failed) return ;

        match(input,25,FOLLOW_25_in_synpred1_AlgorithmGrammar1028); if (state.failed) return ;

        pushFollow(FOLLOW_arithmeticExp_in_synpred1_AlgorithmGrammar1030);
        arithmeticExp();

        state._fsp--;
        if (state.failed) return ;

        match(input,26,FOLLOW_26_in_synpred1_AlgorithmGrammar1032); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred1_AlgorithmGrammar

    // Delegated rules

    public final boolean synpred1_AlgorithmGrammar() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_AlgorithmGrammar_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_statements_in_algorithm110 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_algorithm112 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_statements129 = new BitSet(new long[]{0x0000510000500202L});
    public static final BitSet FOLLOW_statement_in_statements131 = new BitSet(new long[]{0x0000510000500202L});
    public static final BitSet FOLLOW_assignment_in_statement159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement163 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loopStatement_in_statement167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_statement171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callProgram_in_statement175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_function201 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_function204 = new BitSet(new long[]{0x0089800056550600L});
    public static final BitSet FOLLOW_55_in_function208 = new BitSet(new long[]{0x0009800056550600L});
    public static final BitSet FOLLOW_arithmeticExp_in_function214 = new BitSet(new long[]{0x0000000024000000L});
    public static final BitSet FOLLOW_29_in_function217 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_arithmeticExp_in_function219 = new BitSet(new long[]{0x0000000024000000L});
    public static final BitSet FOLLOW_26_in_function225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_domainTable244 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_57_in_domainTable247 = new BitSet(new long[]{0x0808000002550600L});
    public static final BitSet FOLLOW_atom_in_domainTable251 = new BitSet(new long[]{0x0800000020000000L});
    public static final BitSet FOLLOW_29_in_domainTable254 = new BitSet(new long[]{0x0008000002550600L});
    public static final BitSet FOLLOW_atom_in_domainTable256 = new BitSet(new long[]{0x0800000020000000L});
    public static final BitSet FOLLOW_59_in_domainTable262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_callProgram279 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENTS_in_callProgram282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_specialFunction303 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_specialFunction306 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_arithmeticExp_in_specialFunction309 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_specialFunction311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_rateFunction334 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_rateFunction337 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_function_in_rateFunction340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_assignment364 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_varType_in_assignment368 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_xpath_in_assignment372 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_assignment375 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_arithmeticExp_in_assignment378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ifStatement403 = new BitSet(new long[]{0x0000800002000000L});
    public static final BitSet FOLLOW_not_in_ifStatement407 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ifStatement411 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_booleanExp_in_ifStatement414 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ifStatement416 = new BitSet(new long[]{0x0040510000500200L});
    public static final BitSet FOLLOW_54_in_ifStatement420 = new BitSet(new long[]{0x0000510000500200L});
    public static final BitSet FOLLOW_statements_in_ifStatement425 = new BitSet(new long[]{0x0000060000000000L});
    public static final BitSet FOLLOW_41_in_ifStatement437 = new BitSet(new long[]{0x0000510000500200L});
    public static final BitSet FOLLOW_statements_in_ifStatement439 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_ifStatement455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_loopStatement481 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_STRING_in_loopStatement484 = new BitSet(new long[]{0x0000510000500200L});
    public static final BitSet FOLLOW_statements_in_loopStatement494 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_loopStatement504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_atom539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_atom543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_atom547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_atom551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_atom555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_xpath_in_atom559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rateFunction_in_atom563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varType_in_atom567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_domainTable_in_atom571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_not_in_negation589 = new BitSet(new long[]{0x0008000002550600L});
    public static final BitSet FOLLOW_atom_in_negation594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_not614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_unary636 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_negative_in_unary641 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_negation_in_unary647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_in_multi668 = new BitSet(new long[]{0x0100000088000002L});
    public static final BitSet FOLLOW_27_in_multi673 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_31_in_multi678 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_56_in_multi683 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_unary_in_multi688 = new BitSet(new long[]{0x0100000088000002L});
    public static final BitSet FOLLOW_multi_in_arithmeticExp709 = new BitSet(new long[]{0x0020000050000002L});
    public static final BitSet FOLLOW_28_in_arithmeticExp714 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_subtraction_in_arithmeticExp719 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_multi_in_arithmeticExp724 = new BitSet(new long[]{0x0020000050000002L});
    public static final BitSet FOLLOW_arithmeticExp_in_relation744 = new BitSet(new long[]{0x0002207600800002L});
    public static final BitSet FOLLOW_equals_in_relation749 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_notequals_in_relation754 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_33_in_relation759 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_34_in_relation764 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_37_in_relation769 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_38_in_relation774 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_arithmeticExp_in_relation780 = new BitSet(new long[]{0x0002207600800002L});
    public static final BitSet FOLLOW_45_in_equals804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_equals808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_notequals830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_notequals833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_negative856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_negative858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_subtraction885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_subtraction887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_andOperator910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_andOperator914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_orOperator941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_58_in_orOperator945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_startsOperator970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relation_in_booleanExp990 = new BitSet(new long[]{0x0414008001000002L});
    public static final BitSet FOLLOW_andOperator_in_booleanExp995 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_orOperator_in_booleanExp1000 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_startsOperator_in_booleanExp1005 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_relation_in_booleanExp1009 = new BitSet(new long[]{0x0414008001000002L});
    public static final BitSet FOLLOW_specialFunction_in_expression1036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_expression1044 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_booleanExp_in_expression1047 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_expression1049 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_XPATHCONSTANT_in_xpath1058 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_xpath1060 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_STRING_in_xpath1062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VARCONSTANT_in_varType1071 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_varType1073 = new BitSet(new long[]{0x0000000000040200L});
    public static final BitSet FOLLOW_set_in_varType1075 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_synpred1_AlgorithmGrammar1026 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_synpred1_AlgorithmGrammar1028 = new BitSet(new long[]{0x0009800052550600L});
    public static final BitSet FOLLOW_arithmeticExp_in_synpred1_AlgorithmGrammar1030 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_synpred1_AlgorithmGrammar1032 = new BitSet(new long[]{0x0000000000000002L});

}