// $ANTLR 3.4 D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g 2015-05-18 10:10:02

package com.mmpnc.rating.iso.algorithm.parse;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.EvaluatorFactory;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class AlgorithmWalker extends TreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "COMMENTS", "DIV", "EQUALS", "EXPONENT", "IDENTS", "INTEGER", "MULTI_COMMENT", "NEGATION", "NOT", "NOTEQUALS", "OR", "SPECIALFUNCTION", "STARTS", "STRING", "SUBTRACT", "VARCONSTANT", "WS", "XPATHCONSTANT", "'!='", "'&&'", "'('", "')'", "'*'", "'+'", "','", "'-'", "'/'", "':'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'And'", "'Call'", "'ELSE'", "'END IF'", "'END LOOP'", "'IF'", "'Is Equal To'", "'LOOP THROUGH'", "'NOT'", "'Negative'", "'Not Equal To'", "'Or'", "'RateTable'", "'StartsWith'", "'Subtract'", "'THEN'", "'Using'", "'mod'", "'{'", "'||'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int AND=4;
    public static final int COMMENTS=5;
    public static final int DIV=6;
    public static final int EQUALS=7;
    public static final int EXPONENT=8;
    public static final int IDENTS=9;
    public static final int INTEGER=10;
    public static final int MULTI_COMMENT=11;
    public static final int NEGATION=12;
    public static final int NOT=13;
    public static final int NOTEQUALS=14;
    public static final int OR=15;
    public static final int SPECIALFUNCTION=16;
    public static final int STARTS=17;
    public static final int STRING=18;
    public static final int SUBTRACT=19;
    public static final int VARCONSTANT=20;
    public static final int WS=21;
    public static final int XPATHCONSTANT=22;

    // delegates
    public TreeParser[] getDelegates() {
        return new TreeParser[] {};
    }

    // delegators


    public AlgorithmWalker(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public AlgorithmWalker(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return AlgorithmWalker.tokenNames; }
    public String getGrammarFileName() { return "D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g"; }


    private Context context = null;
    private EvaluatorFactory evaluatorFactory = null;
     
    public void setEvaluatorFactory(EvaluatorFactory evaluatorFactory){
      this.evaluatorFactory = evaluatorFactory;
    }



    // $ANTLR start "algorithm"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:26:1: algorithm[Context context] :e1= statements ;
    public final void algorithm(Context context) throws RecognitionException {
        List<Evaluator> e1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:26:28: (e1= statements )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:27:13: e1= statements
            {

                        this.context = context;
                        

            pushFollow(FOLLOW_statements_in_algorithm82);
            e1=statements();

            state._fsp--;



                            for(Evaluator eval : e1)
                            {
                                eval.setContext(context);
                                eval.evaluate();
                            }
                        

            }

        }
        catch (RecognitionException re) {
            //reportError(re);
            //recover(input,re);
        	throw re;
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "algorithm"



    // $ANTLR start "statements"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:40:1: statements returns [List<Evaluator> eval] :e1= statement (e2= statement )* ;
    public final List<Evaluator> statements() throws RecognitionException {
        List<Evaluator> eval = null;


        Evaluator e1 =null;

        Evaluator e2 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:41:9: (e1= statement (e2= statement )* )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:42:9: e1= statement (e2= statement )*
            {

                        eval = new ArrayList<Evaluator>();
                    

            pushFollow(FOLLOW_statement_in_statements147);
            e1=statement();

            state._fsp--;


            eval.add(e1);

            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:45:45: (e2= statement )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==35||LA1_0==40||LA1_0==44||LA1_0==46) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:45:46: e2= statement
            	    {
            	    pushFollow(FOLLOW_statement_in_statements154);
            	    e2=statement();

            	    state._fsp--;


            	    eval.add(e2);

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "statements"



    // $ANTLR start "statement"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:47:1: statement returns [Evaluator eval] : ( assignment | ifStatement | loopStatement | callProgram ) ;
    public final Evaluator statement() throws RecognitionException {
        Evaluator eval = null;


        Evaluator assignment1 =null;

        Evaluator ifStatement2 =null;

        Evaluator loopStatement3 =null;

        Evaluator callProgram4 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:47:35: ( ( assignment | ifStatement | loopStatement | callProgram ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:48:9: ( assignment | ifStatement | loopStatement | callProgram )
            {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:48:9: ( assignment | ifStatement | loopStatement | callProgram )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt2=1;
                }
                break;
            case 44:
                {
                alt2=2;
                }
                break;
            case 46:
                {
                alt2=3;
                }
                break;
            case 40:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:48:10: assignment
                    {
                    pushFollow(FOLLOW_assignment_in_statement178);
                    assignment1=assignment();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:48:23: ifStatement
                    {
                    pushFollow(FOLLOW_ifStatement_in_statement182);
                    ifStatement2=ifStatement();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:48:37: loopStatement
                    {
                    pushFollow(FOLLOW_loopStatement_in_statement186);
                    loopStatement3=loopStatement();

                    state._fsp--;


                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:48:53: callProgram
                    {
                    pushFollow(FOLLOW_callProgram_in_statement190);
                    callProgram4=callProgram();

                    state._fsp--;


                    }
                    break;

            }



                            if(assignment1 != null)
                            {
                                eval = assignment1;
                            }
                            else if(ifStatement2 != null)
                            {
                                eval = ifStatement2;
                            }else if(loopStatement3 != null)
                            {
                                eval = loopStatement3;
                            }else if(callProgram4 != null)
                            {
                                eval = callProgram4;
                            }
                        

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "statement"



    // $ANTLR start "callProgram"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:67:1: callProgram returns [Evaluator eval] : ^( 'Call' v= IDENTS ) ;
    public final Evaluator callProgram() throws RecognitionException {
        Evaluator eval = null;


        CommonTree v=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:67:37: ( ^( 'Call' v= IDENTS ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:68:5: ^( 'Call' v= IDENTS )
            {
            match(input,40,FOLLOW_40_in_callProgram232); 

            match(input, Token.DOWN, null); 
            v=(CommonTree)match(input,IDENTS,FOLLOW_IDENTS_in_callProgram236); 

            match(input, Token.UP, null); 



                        eval = this.evaluatorFactory.getProgramEvaluator(context,(v!=null?v.getText():null));
                    

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "callProgram"



    // $ANTLR start "assignment"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:74:1: assignment returns [Evaluator eval] : ^( '=' (i= IDENTS |v= varType |x= xpath ) e1= arithmeticExp ) ;
    public final Evaluator assignment() throws RecognitionException {
        Evaluator eval = null;


        CommonTree i=null;
        Evaluator v =null;

        Evaluator x =null;

        Evaluator e1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:74:37: ( ^( '=' (i= IDENTS |v= varType |x= xpath ) e1= arithmeticExp ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:75:5: ^( '=' (i= IDENTS |v= varType |x= xpath ) e1= arithmeticExp )
            {
            match(input,35,FOLLOW_35_in_assignment269); 

            match(input, Token.DOWN, null); 
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:75:11: (i= IDENTS |v= varType |x= xpath )
            int alt3=3;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                alt3=1;
                }
                break;
            case VARCONSTANT:
                {
                alt3=2;
                }
                break;
            case XPATHCONSTANT:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }

            switch (alt3) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:75:12: i= IDENTS
                    {
                    i=(CommonTree)match(input,IDENTS,FOLLOW_IDENTS_in_assignment274); 

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:75:23: v= varType
                    {
                    pushFollow(FOLLOW_varType_in_assignment280);
                    v=varType();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:75:35: x= xpath
                    {
                    pushFollow(FOLLOW_xpath_in_assignment286);
                    x=xpath();

                    state._fsp--;


                    }
                    break;

            }


            pushFollow(FOLLOW_arithmeticExp_in_assignment291);
            e1=arithmeticExp();

            state._fsp--;


            match(input, Token.UP, null); 



                        if(i != null)
                        {
                            eval = this.evaluatorFactory.getAssignmentEvaluator(context, this.evaluatorFactory.getVarEvaluator(null, (i!=null?i.getText():null)), e1);
                        }
                        else {
                            if(v != null)
                            {
                                eval = this.evaluatorFactory.getAssignmentLocalEvaluator(context, v, e1);
                            } else {
                                eval = this.evaluatorFactory.getAssignmentXpathEvaluator(context, x, e1);
                            }
                        }
                    

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "assignment"



    // $ANTLR start "function"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:92:1: function[String constant] returns [Evaluator eval] : ^(e1= IDENTS (e2= arithmeticExp ( ',' e3= arithmeticExp )* )? ) ;
    public final Evaluator function(String constant) throws RecognitionException {
        Evaluator eval = null;


        CommonTree e1=null;
        Evaluator e2 =null;

        Evaluator e3 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:92:52: ( ^(e1= IDENTS (e2= arithmeticExp ( ',' e3= arithmeticExp )* )? ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:93:7: ^(e1= IDENTS (e2= arithmeticExp ( ',' e3= arithmeticExp )* )? )
            {

                      List<Evaluator> evallist = new ArrayList<Evaluator>();
                  

            e1=(CommonTree)match(input,IDENTS,FOLLOW_IDENTS_in_function336); 

            if ( input.LA(1)==Token.DOWN ) {
                match(input, Token.DOWN, null); 
                // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:96:17: (e2= arithmeticExp ( ',' e3= arithmeticExp )* )?
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0 >= IDENTS && LA5_0 <= INTEGER)||LA5_0==NEGATION||LA5_0==SPECIALFUNCTION||(LA5_0 >= STRING && LA5_0 <= VARCONSTANT)||LA5_0==XPATHCONSTANT||(LA5_0 >= 27 && LA5_0 <= 28)||LA5_0==31||LA5_0==35||LA5_0==51||LA5_0==56) ) {
                    alt5=1;
                }
                switch (alt5) {
                    case 1 :
                        // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:96:18: e2= arithmeticExp ( ',' e3= arithmeticExp )*
                        {
                        pushFollow(FOLLOW_arithmeticExp_in_function341);
                        e2=arithmeticExp();

                        state._fsp--;


                        evallist.add(e2);

                        // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:96:61: ( ',' e3= arithmeticExp )*
                        loop4:
                        do {
                            int alt4=2;
                            int LA4_0 = input.LA(1);

                            if ( (LA4_0==29) ) {
                                alt4=1;
                            }


                            switch (alt4) {
                        	case 1 :
                        	    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:96:62: ',' e3= arithmeticExp
                        	    {
                        	    match(input,29,FOLLOW_29_in_function346); 

                        	    pushFollow(FOLLOW_arithmeticExp_in_function350);
                        	    e3=arithmeticExp();

                        	    state._fsp--;


                        	    evallist.add(e3);

                        	    }
                        	    break;

                        	default :
                        	    break loop4;
                            }
                        } while (true);


                        }
                        break;

                }


                match(input, Token.UP, null); 
            }



                      eval = this.evaluatorFactory.getFunctionEvaluator(constant,(e1!=null?e1.getText():null), evallist);
                  

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "function"



    // $ANTLR start "specialFunction"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:102:1: specialFunction returns [Evaluator eval] : ^(t= SPECIALFUNCTION e1= arithmeticExp ) ;
    public final Evaluator specialFunction() throws RecognitionException {
        Evaluator eval = null;


        CommonTree t=null;
        Evaluator e1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:102:41: ( ^(t= SPECIALFUNCTION e1= arithmeticExp ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:103:5: ^(t= SPECIALFUNCTION e1= arithmeticExp )
            {
            t=(CommonTree)match(input,SPECIALFUNCTION,FOLLOW_SPECIALFUNCTION_in_specialFunction389); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_arithmeticExp_in_specialFunction393);
            e1=arithmeticExp();

            state._fsp--;


            match(input, Token.UP, null); 



                      eval = this.evaluatorFactory.getSpecialFunctionEvaluator(context,(t!=null?t.getText():null), e1);
                  

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "specialFunction"



    // $ANTLR start "rateFunction"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:109:1: rateFunction returns [Evaluator eval] : ^(e1= 'RateTable' e2= function[$e1.text] ) ;
    public final Evaluator rateFunction() throws RecognitionException {
        Evaluator eval = null;


        CommonTree e1=null;
        Evaluator e2 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:109:38: ( ^(e1= 'RateTable' e2= function[$e1.text] ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:110:5: ^(e1= 'RateTable' e2= function[$e1.text] )
            {
            e1=(CommonTree)match(input,51,FOLLOW_51_in_rateFunction429); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_function_in_rateFunction433);
            e2=function((e1!=null?e1.getText():null));

            state._fsp--;


            match(input, Token.UP, null); 



                     eval = e2;
            //         eval = this.evaluatorFactory.getRateTableEvaluator(context,(e1!=null?e1.getText():null));
                  

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "rateFunction"



    // $ANTLR start "ifStatement"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:117:1: ifStatement returns [Evaluator eval] : ^( 'IF' (n= NOT )? bool= booleanExp s1= statements ( 'ELSE' s2= statements )? ) ;
    public final Evaluator ifStatement() throws RecognitionException {
        Evaluator eval = null;


        CommonTree n=null;
        Evaluator bool =null;

        List<Evaluator> s1 =null;

        List<Evaluator> s2 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:117:37: ( ^( 'IF' (n= NOT )? bool= booleanExp s1= statements ( 'ELSE' s2= statements )? ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:118:5: ^( 'IF' (n= NOT )? bool= booleanExp s1= statements ( 'ELSE' s2= statements )? )
            {
            match(input,44,FOLLOW_44_in_ifStatement473); 

            match(input, Token.DOWN, null); 
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:118:12: (n= NOT )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==NOT) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==AND||LA6_1==EQUALS||(LA6_1 >= IDENTS && LA6_1 <= INTEGER)||(LA6_1 >= NEGATION && LA6_1 <= VARCONSTANT)||LA6_1==XPATHCONSTANT||(LA6_1 >= 27 && LA6_1 <= 28)||LA6_1==31||(LA6_1 >= 33 && LA6_1 <= 35)||(LA6_1 >= 37 && LA6_1 <= 38)||LA6_1==51||LA6_1==56) ) {
                    alt6=1;
                }
            }
            switch (alt6) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:118:13: n= NOT
                    {
                    n=(CommonTree)match(input,NOT,FOLLOW_NOT_in_ifStatement478); 

                    }
                    break;

            }


            pushFollow(FOLLOW_booleanExp_in_ifStatement484);
            bool=booleanExp();

            state._fsp--;


            pushFollow(FOLLOW_statements_in_ifStatement488);
            s1=statements();

            state._fsp--;


            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:118:51: ( 'ELSE' s2= statements )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==41) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:118:52: 'ELSE' s2= statements
                    {
                    match(input,41,FOLLOW_41_in_ifStatement491); 

                    pushFollow(FOLLOW_statements_in_ifStatement495);
                    s2=statements();

                    state._fsp--;


                    }
                    break;

            }


            match(input, Token.UP, null); 



                      if(n != null)
                      {
                        eval = this.evaluatorFactory.getIfEvaluator(context, this.evaluatorFactory.getNotBooleanEvaluator(bool), s1, s2);
                      } else {
                        eval = this.evaluatorFactory.getIfEvaluator(context, this.evaluatorFactory.getBooleanEvaluator(bool), s1, s2);
                      }
                    

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "ifStatement"



    // $ANTLR start "loopStatement"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:129:1: loopStatement returns [Evaluator eval] : ^( 'LOOP THROUGH' v1= STRING s1= statements ) ;
    public final Evaluator loopStatement() throws RecognitionException {
        Evaluator eval = null;


        CommonTree v1=null;
        List<Evaluator> s1 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:129:39: ( ^( 'LOOP THROUGH' v1= STRING s1= statements ) )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:130:5: ^( 'LOOP THROUGH' v1= STRING s1= statements )
            {
            match(input,46,FOLLOW_46_in_loopStatement533); 

            match(input, Token.DOWN, null); 
            v1=(CommonTree)match(input,STRING,FOLLOW_STRING_in_loopStatement537); 

            pushFollow(FOLLOW_statements_in_loopStatement541);
            s1=statements();

            state._fsp--;


            match(input, Token.UP, null); 



                        eval = this.evaluatorFactory.getLoopEvaluator(context, (v1!=null?v1.getText():null), s1);
                    

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "loopStatement"



    // $ANTLR start "xpath"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:136:1: xpath returns [Evaluator eval] : XPATHCONSTANT ':' t= STRING ;
    public final Evaluator xpath() throws RecognitionException {
        Evaluator eval = null;


        CommonTree t=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:137:5: ( XPATHCONSTANT ':' t= STRING )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:137:7: XPATHCONSTANT ':' t= STRING
            {
            match(input,XPATHCONSTANT,FOLLOW_XPATHCONSTANT_in_xpath573); 

            match(input,32,FOLLOW_32_in_xpath575); 

            t=(CommonTree)match(input,STRING,FOLLOW_STRING_in_xpath579); 


                        eval = this.evaluatorFactory.getXpathEvaluator ((t!=null?t.getText():null));
                    

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "xpath"



    // $ANTLR start "varType"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:143:1: varType returns [Evaluator eval] : v= VARCONSTANT ':' t= IDENTS ;
    public final Evaluator varType() throws RecognitionException {
        Evaluator eval = null;


        CommonTree v=null;
        CommonTree t=null;

        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:144:5: (v= VARCONSTANT ':' t= IDENTS )
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:144:7: v= VARCONSTANT ':' t= IDENTS
            {
            v=(CommonTree)match(input,VARCONSTANT,FOLLOW_VARCONSTANT_in_varType613); 

            match(input,32,FOLLOW_32_in_varType615); 

            t=(CommonTree)match(input,IDENTS,FOLLOW_IDENTS_in_varType619); 


                        eval = this.evaluatorFactory.getVarEvaluator ((v!=null?v.getText():null), (t!=null?t.getText():null));
                    

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "varType"



    // $ANTLR start "atom"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:150:1: atom returns [Evaluator eval] : ( (i= IDENTS |ti= INTEGER |ts= STRING ) |e= function[null] |e1= rateFunction |e2= xpath |e3= varType |e4= specialFunction );
    public final Evaluator atom() throws RecognitionException {
        Evaluator eval = null;


        CommonTree i=null;
        CommonTree ti=null;
        CommonTree ts=null;
        Evaluator e =null;

        Evaluator e1 =null;

        Evaluator e2 =null;

        Evaluator e3 =null;

        Evaluator e4 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:151:5: ( (i= IDENTS |ti= INTEGER |ts= STRING ) |e= function[null] |e1= rateFunction |e2= xpath |e3= varType |e4= specialFunction )
            int alt9=6;
            switch ( input.LA(1) ) {
            case IDENTS:
                {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==DOWN) ) {
                    alt9=2;
                }
                else if ( ((LA9_1 >= UP && LA9_1 <= AND)||LA9_1==EQUALS||(LA9_1 >= IDENTS && LA9_1 <= INTEGER)||(LA9_1 >= NEGATION && LA9_1 <= VARCONSTANT)||LA9_1==XPATHCONSTANT||(LA9_1 >= 27 && LA9_1 <= 29)||LA9_1==31||(LA9_1 >= 33 && LA9_1 <= 35)||(LA9_1 >= 37 && LA9_1 <= 38)||LA9_1==40||LA9_1==44||LA9_1==46||LA9_1==51||LA9_1==56) ) {
                    alt9=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;

                }
                }
                break;
            case INTEGER:
            case STRING:
                {
                alt9=1;
                }
                break;
            case 51:
                {
                alt9=3;
                }
                break;
            case XPATHCONSTANT:
                {
                alt9=4;
                }
                break;
            case VARCONSTANT:
                {
                alt9=5;
                }
                break;
            case SPECIALFUNCTION:
                {
                alt9=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }

            switch (alt9) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:151:8: (i= IDENTS |ti= INTEGER |ts= STRING )
                    {
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:151:8: (i= IDENTS |ti= INTEGER |ts= STRING )
                    int alt8=3;
                    switch ( input.LA(1) ) {
                    case IDENTS:
                        {
                        alt8=1;
                        }
                        break;
                    case INTEGER:
                        {
                        alt8=2;
                        }
                        break;
                    case STRING:
                        {
                        alt8=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 0, input);

                        throw nvae;

                    }

                    switch (alt8) {
                        case 1 :
                            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:151:10: i= IDENTS
                            {
                            i=(CommonTree)match(input,IDENTS,FOLLOW_IDENTS_in_atom660); 

                            }
                            break;
                        case 2 :
                            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:151:21: ti= INTEGER
                            {
                            ti=(CommonTree)match(input,INTEGER,FOLLOW_INTEGER_in_atom666); 

                            }
                            break;
                        case 3 :
                            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:151:34: ts= STRING
                            {
                            ts=(CommonTree)match(input,STRING,FOLLOW_STRING_in_atom672); 

                            }
                            break;

                    }



                                    if(i != null)
                                    {
                                        eval = this.evaluatorFactory.getVarEvaluator(null, (i!=null?i.getText():null));
                                    }
                                    else
                                    {
                                        if(ti == null){
                                            eval = this.evaluatorFactory.getStringEvaluator ((ts!=null?ts.getText():null));
                                        }else{
                                            eval = this.evaluatorFactory.getIntegerEvaluator ((ti!=null?ti.getText():null));
                                        }
                                    }
                                

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:166:10: e= function[null]
                    {
                    pushFollow(FOLLOW_function_in_atom701);
                    e=function(null);

                    state._fsp--;



                                    eval = e;
                                

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:170:10: e1= rateFunction
                    {
                    pushFollow(FOLLOW_rateFunction_in_atom729);
                    e1=rateFunction();

                    state._fsp--;



                                    eval = e1;
                                

                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:174:10: e2= xpath
                    {
                    pushFollow(FOLLOW_xpath_in_atom756);
                    e2=xpath();

                    state._fsp--;



                                  eval = e2;  
                                

                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:178:10: e3= varType
                    {
                    pushFollow(FOLLOW_varType_in_atom787);
                    e3=varType();

                    state._fsp--;



                                  eval = e3;
                                

                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:182:10: e4= specialFunction
                    {
                    pushFollow(FOLLOW_specialFunction_in_atom814);
                    e4=specialFunction();

                    state._fsp--;



                                  eval = e4;
                                

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "atom"



    // $ANTLR start "arithmeticExp"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:188:1: arithmeticExp returns [Evaluator eval] : ( ^( '+' e1= arithmeticExp e2= arithmeticExp ) | ^( SUBTRACT e1= arithmeticExp e2= arithmeticExp ) | ^( '*' e1= arithmeticExp e2= arithmeticExp ) | ^( '/' e1= arithmeticExp e2= arithmeticExp ) | ^( 'mod' e1= arithmeticExp e2= arithmeticExp ) | ^( NEGATION e1= atom ) |e= atom |e= assignment );
    public final Evaluator arithmeticExp() throws RecognitionException {
        Evaluator eval = null;


        Evaluator e1 =null;

        Evaluator e2 =null;

        Evaluator e =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:189:5: ( ^( '+' e1= arithmeticExp e2= arithmeticExp ) | ^( SUBTRACT e1= arithmeticExp e2= arithmeticExp ) | ^( '*' e1= arithmeticExp e2= arithmeticExp ) | ^( '/' e1= arithmeticExp e2= arithmeticExp ) | ^( 'mod' e1= arithmeticExp e2= arithmeticExp ) | ^( NEGATION e1= atom ) |e= atom |e= assignment )
            int alt10=8;
            switch ( input.LA(1) ) {
            case 28:
                {
                alt10=1;
                }
                break;
            case SUBTRACT:
                {
                alt10=2;
                }
                break;
            case 27:
                {
                alt10=3;
                }
                break;
            case 31:
                {
                alt10=4;
                }
                break;
            case 56:
                {
                alt10=5;
                }
                break;
            case NEGATION:
                {
                alt10=6;
                }
                break;
            case IDENTS:
            case INTEGER:
            case SPECIALFUNCTION:
            case STRING:
            case VARCONSTANT:
            case XPATHCONSTANT:
            case 51:
                {
                alt10=7;
                }
                break;
            case 35:
                {
                alt10=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }

            switch (alt10) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:189:7: ^( '+' e1= arithmeticExp e2= arithmeticExp )
                    {
                    match(input,28,FOLLOW_28_in_arithmeticExp850); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp854);
                    e1=arithmeticExp();

                    state._fsp--;


                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp858);
                    e2=arithmeticExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getPlusEvaluator (e1, e2);

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:190:7: ^( SUBTRACT e1= arithmeticExp e2= arithmeticExp )
                    {
                    match(input,SUBTRACT,FOLLOW_SUBTRACT_in_arithmeticExp869); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp873);
                    e1=arithmeticExp();

                    state._fsp--;


                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp877);
                    e2=arithmeticExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getMinusEvaluator (e1, e2);

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:191:7: ^( '*' e1= arithmeticExp e2= arithmeticExp )
                    {
                    match(input,27,FOLLOW_27_in_arithmeticExp888); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp892);
                    e1=arithmeticExp();

                    state._fsp--;


                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp896);
                    e2=arithmeticExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getMultiplyEvaluator (e1, e2);

                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:192:7: ^( '/' e1= arithmeticExp e2= arithmeticExp )
                    {
                    match(input,31,FOLLOW_31_in_arithmeticExp907); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp911);
                    e1=arithmeticExp();

                    state._fsp--;


                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp915);
                    e2=arithmeticExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getDivideEvaluator (e1, e2);

                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:193:7: ^( 'mod' e1= arithmeticExp e2= arithmeticExp )
                    {
                    match(input,56,FOLLOW_56_in_arithmeticExp926); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp930);
                    e1=arithmeticExp();

                    state._fsp--;


                    pushFollow(FOLLOW_arithmeticExp_in_arithmeticExp934);
                    e2=arithmeticExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getModEvaluator (e1, e2);

                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:194:7: ^( NEGATION e1= atom )
                    {
                    match(input,NEGATION,FOLLOW_NEGATION_in_arithmeticExp945); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_atom_in_arithmeticExp949);
                    e1=atom();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getUnaryNegationEvaluator(e1);

                    }
                    break;
                case 7 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:195:7: e= atom
                    {
                    pushFollow(FOLLOW_atom_in_arithmeticExp961);
                    e=atom();

                    state._fsp--;


                     eval = e;

                    }
                    break;
                case 8 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:196:7: e= assignment
                    {
                    pushFollow(FOLLOW_assignment_in_arithmeticExp980);
                    e=assignment();

                    state._fsp--;


                     eval = e;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "arithmeticExp"



    // $ANTLR start "booleanExp"
    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:199:1: booleanExp returns [Evaluator eval] : ( ^( STARTS e1= booleanExp e2= booleanExp ) | ^( AND e1= booleanExp e2= booleanExp ) | ^( OR e1= booleanExp e2= booleanExp ) | ^( EQUALS e1= booleanExp e2= booleanExp ) | ^( NOTEQUALS e1= booleanExp e2= booleanExp ) | ^( '<' e1= booleanExp e2= booleanExp ) | ^( '<=' e1= booleanExp e2= booleanExp ) | ^( '>' e1= booleanExp e2= booleanExp ) | ^( '>=' e1= booleanExp e2= booleanExp ) | ^( NOT e1= booleanExp ) | arithmeticExp );
    public final Evaluator booleanExp() throws RecognitionException {
        Evaluator eval = null;


        Evaluator e1 =null;

        Evaluator e2 =null;

        Evaluator arithmeticExp5 =null;


        try {
            // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:200:5: ( ^( STARTS e1= booleanExp e2= booleanExp ) | ^( AND e1= booleanExp e2= booleanExp ) | ^( OR e1= booleanExp e2= booleanExp ) | ^( EQUALS e1= booleanExp e2= booleanExp ) | ^( NOTEQUALS e1= booleanExp e2= booleanExp ) | ^( '<' e1= booleanExp e2= booleanExp ) | ^( '<=' e1= booleanExp e2= booleanExp ) | ^( '>' e1= booleanExp e2= booleanExp ) | ^( '>=' e1= booleanExp e2= booleanExp ) | ^( NOT e1= booleanExp ) | arithmeticExp )
            int alt11=11;
            switch ( input.LA(1) ) {
            case STARTS:
                {
                alt11=1;
                }
                break;
            case AND:
                {
                alt11=2;
                }
                break;
            case OR:
                {
                alt11=3;
                }
                break;
            case EQUALS:
                {
                alt11=4;
                }
                break;
            case NOTEQUALS:
                {
                alt11=5;
                }
                break;
            case 33:
                {
                alt11=6;
                }
                break;
            case 34:
                {
                alt11=7;
                }
                break;
            case 37:
                {
                alt11=8;
                }
                break;
            case 38:
                {
                alt11=9;
                }
                break;
            case NOT:
                {
                alt11=10;
                }
                break;
            case IDENTS:
            case INTEGER:
            case NEGATION:
            case SPECIALFUNCTION:
            case STRING:
            case SUBTRACT:
            case VARCONSTANT:
            case XPATHCONSTANT:
            case 27:
            case 28:
            case 31:
            case 35:
            case 51:
            case 56:
                {
                alt11=11;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }

            switch (alt11) {
                case 1 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:200:7: ^( STARTS e1= booleanExp e2= booleanExp )
                    {
                    match(input,STARTS,FOLLOW_STARTS_in_booleanExp1004); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1008);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1012);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getCustomOperatorEvaluator (e1, e2);

                    }
                    break;
                case 2 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:201:7: ^( AND e1= booleanExp e2= booleanExp )
                    {
                    match(input,AND,FOLLOW_AND_in_booleanExp1024); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1028);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1032);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getAndEvaluator (e1, e2);

                    }
                    break;
                case 3 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:202:7: ^( OR e1= booleanExp e2= booleanExp )
                    {
                    match(input,OR,FOLLOW_OR_in_booleanExp1044); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1048);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1052);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getOrEvaluator (e1, e2);

                    }
                    break;
                case 4 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:203:7: ^( EQUALS e1= booleanExp e2= booleanExp )
                    {
                    match(input,EQUALS,FOLLOW_EQUALS_in_booleanExp1063); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1067);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1071);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getEqualsEvaluator (e1, e2);

                    }
                    break;
                case 5 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:204:7: ^( NOTEQUALS e1= booleanExp e2= booleanExp )
                    {
                    match(input,NOTEQUALS,FOLLOW_NOTEQUALS_in_booleanExp1082); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1086);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1090);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getNotEqualsEvaluator (e1, e2);

                    }
                    break;
                case 6 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:205:7: ^( '<' e1= booleanExp e2= booleanExp )
                    {
                    match(input,33,FOLLOW_33_in_booleanExp1101); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1105);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1109);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getLessThanEvaluator (e1, e2);

                    }
                    break;
                case 7 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:206:7: ^( '<=' e1= booleanExp e2= booleanExp )
                    {
                    match(input,34,FOLLOW_34_in_booleanExp1120); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1124);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1128);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getLessThanEqualsEvaluator (e1, e2);

                    }
                    break;
                case 8 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:207:7: ^( '>' e1= booleanExp e2= booleanExp )
                    {
                    match(input,37,FOLLOW_37_in_booleanExp1139); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1143);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1147);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getGreaterThanEvaluator (e1, e2);

                    }
                    break;
                case 9 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:208:7: ^( '>=' e1= booleanExp e2= booleanExp )
                    {
                    match(input,38,FOLLOW_38_in_booleanExp1158); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1162);
                    e1=booleanExp();

                    state._fsp--;


                    pushFollow(FOLLOW_booleanExp_in_booleanExp1166);
                    e2=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getGreaterThanEqualsEvaluator (e1, e2);

                    }
                    break;
                case 10 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:209:7: ^( NOT e1= booleanExp )
                    {
                    match(input,NOT,FOLLOW_NOT_in_booleanExp1177); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_booleanExp_in_booleanExp1181);
                    e1=booleanExp();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    eval = this.evaluatorFactory.getNotBooleanEvaluator (e1);

                    }
                    break;
                case 11 :
                    // D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOConsumptionRealign-PCH-Modularization\\src\\main\\resources\\com\\mmpnc\\rating\\iso\\algorithm\\grammar\\AlgorithmWalker.g:210:7: arithmeticExp
                    {
                    pushFollow(FOLLOW_arithmeticExp_in_booleanExp1191);
                    arithmeticExp5=arithmeticExp();

                    state._fsp--;


                    eval = arithmeticExp5;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return eval;
    }
    // $ANTLR end "booleanExp"

    // Delegated rules


 

    public static final BitSet FOLLOW_statements_in_algorithm82 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_statements147 = new BitSet(new long[]{0x0000510800000002L});
    public static final BitSet FOLLOW_statement_in_statements154 = new BitSet(new long[]{0x0000510800000002L});
    public static final BitSet FOLLOW_assignment_in_statement178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loopStatement_in_statement186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callProgram_in_statement190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_callProgram232 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENTS_in_callProgram236 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_35_in_assignment269 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENTS_in_assignment274 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_varType_in_assignment280 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_xpath_in_assignment286 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_assignment291 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_IDENTS_in_function336 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_function341 = new BitSet(new long[]{0x0000000020000008L});
    public static final BitSet FOLLOW_29_in_function346 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_function350 = new BitSet(new long[]{0x0000000020000008L});
    public static final BitSet FOLLOW_SPECIALFUNCTION_in_specialFunction389 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_specialFunction393 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_51_in_rateFunction429 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_function_in_rateFunction433 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_44_in_ifStatement473 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_NOT_in_ifStatement478 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_ifStatement484 = new BitSet(new long[]{0x0000510800000000L});
    public static final BitSet FOLLOW_statements_in_ifStatement488 = new BitSet(new long[]{0x0000020000000008L});
    public static final BitSet FOLLOW_41_in_ifStatement491 = new BitSet(new long[]{0x0000510800000000L});
    public static final BitSet FOLLOW_statements_in_ifStatement495 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_46_in_loopStatement533 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_STRING_in_loopStatement537 = new BitSet(new long[]{0x0000510800000000L});
    public static final BitSet FOLLOW_statements_in_loopStatement541 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_XPATHCONSTANT_in_xpath573 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_xpath575 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_STRING_in_xpath579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VARCONSTANT_in_varType613 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_varType615 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENTS_in_varType619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTS_in_atom660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_atom666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_atom672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_function_in_atom701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rateFunction_in_atom729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_xpath_in_atom756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varType_in_atom787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_specialFunction_in_atom814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_arithmeticExp850 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp854 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp858 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SUBTRACT_in_arithmeticExp869 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp873 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp877 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_27_in_arithmeticExp888 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp892 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp896 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_31_in_arithmeticExp907 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp911 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp915 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_56_in_arithmeticExp926 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp930 = new BitSet(new long[]{0x01080008985D1600L});
    public static final BitSet FOLLOW_arithmeticExp_in_arithmeticExp934 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NEGATION_in_arithmeticExp945 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_atom_in_arithmeticExp949 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_atom_in_arithmeticExp961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_in_arithmeticExp980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STARTS_in_booleanExp1004 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1008 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1012 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_AND_in_booleanExp1024 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1028 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1032 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_OR_in_booleanExp1044 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1048 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1052 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EQUALS_in_booleanExp1063 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1067 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1071 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NOTEQUALS_in_booleanExp1082 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1086 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1090 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_33_in_booleanExp1101 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1105 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1109 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_34_in_booleanExp1120 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1124 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1128 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_37_in_booleanExp1139 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1143 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1147 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_38_in_booleanExp1158 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1162 = new BitSet(new long[]{0x0108006E985FF690L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1166 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NOT_in_booleanExp1177 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_booleanExp_in_booleanExp1181 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_arithmeticExp_in_booleanExp1191 = new BitSet(new long[]{0x0000000000000002L});

}