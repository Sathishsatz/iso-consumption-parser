package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 * THIS TEST CASE WILL CHECK THE PCH NAMES IN THE ALGORITHM FILE FOR FOLLOWING THINGS
 * PCH NAMES STATRING WITH ../
 * EMPTY PCH
 * PCH NAME HAVING ANCESTOR/DESCENDANT 
 * 
 */
public class TestPCHNamesBeforeALGFileProcessing {
	
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
		
	
	public void checkForPCHNames(FileReader fileReader){
		
		
		StringBuffer emptyPCHSocpes = new StringBuffer();
		StringBuffer pchNamesWithdotInit = new StringBuffer();
		StringBuffer pchNamesWithAncestor = new StringBuffer();
		StringBuffer pchNamesWithFunction = new StringBuffer();
		LOB lob = getLOBObject(fileReader);
		
		List<Object>contents = lob.getContent();
		
		for(Object referece: contents){
			if(referece instanceof Reference ){
				Reference referenceOb = (Reference)referece;
				//String refName = referenceOb.getName();
				
				
				if(referenceOb.getType().equals("Common Rating") || referenceOb.getType().equals("Premium Calculation")){
					List<Object> referenceContent = ((Reference) referece).getContent();
					for(Object scope : referenceContent){
						if(scope instanceof Scope){
							
							Scope scop = (Scope)scope;
							List <Object> scopeContent = scop.getContent();
							
							for(Object pchs: scopeContent){
								if(pchs instanceof PCH){
									PCH pch = (PCH)pchs ;
									String pchName = pch.getName();
									
									if(pchName == null){
										emptyPCHSocpes.append("EMPTY PCH IN SCOPE ["+scop.getDbTables()+"]");
										emptyPCHSocpes.append("\n");
										continue;
									}
									
									if(pchName.trim().startsWith("../")){
										//if( referenceOb.getType().equals("Premium Calculation")){
											pchNamesWithdotInit.append("SCOPE NAME:["+scop.getDbTables()+"] PCH NAME:=["+pchName+"]");
											pchNamesWithdotInit.append("\n");
										//}
									}
									
									
									if(pchName.contains("ancestor::*") ||pchName.contains("descendant::")){
										pchNamesWithAncestor.append("SOCPE NAME:["+scop.getDbTables()+"] HAS A PCH WITH ANCESOTR IN NAME ["+pchName+"]");
										pchNamesWithAncestor.append("\n");
									}
									
									if(pchName.contains("{")){
										pchNamesWithFunction.append("SCOPE NAME:["+scop.getDbTables()+"] HAS A PCH WITH FUNCTIONN NAME["+pchName+"]");
										pchNamesWithFunction.append("\n");
									}
									
								}
							}
							
						}
					}
				}
				
			}
			
		}
		System.out.println("----------EMTPY PCH SCOPES---------------");
		System.out.println(emptyPCHSocpes);
		System.out.println("-----------------------------------------");
		System.out.println("---------PCH NAMES WITH ../ IN IT--------");
		System.out.println(pchNamesWithdotInit);
		System.out.println("-----------------------------------------");
		System.out.println("---------PCH NAMES WITH ANCESTOR IN IT---");
		System.out.println(pchNamesWithAncestor);
		System.out.println("-----------------------------------------");
		System.out.println("--------- PCH NAMES WITH CUSTOM FUNCTIONS -----");
		System.out.println(pchNamesWithFunction);
	}
	
	//private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-PA-07012013-V02\\ALG-BP-PA-07012013-V02_MR_REARRANGED.xml";
	
	//MD ALGORITHM FILE
	//private static final String fileName = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-MD-09012012-V01/ALG-BP-MD-09012012-V01_MR.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-CO-11012013-V01\\ALG-BP-CO-11012013-REARRANGED_V01_MR.xml";
	//String fileName ="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-CO-11012013-V01\\test-co-algorithm.xml";
	//String fileName  = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";
	String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\GL files from parser 3.2.2\\ALG-GL-CW-05012014-V01_MR_MODIFIED.xml";
	@Test
	public void testAlgoFileForPCh(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			checkForPCHNames(fileReader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
}
