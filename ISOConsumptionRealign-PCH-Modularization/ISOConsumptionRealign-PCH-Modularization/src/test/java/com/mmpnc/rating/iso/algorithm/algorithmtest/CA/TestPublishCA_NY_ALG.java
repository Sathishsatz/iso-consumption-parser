package com.mmpnc.rating.iso.algorithm.algorithmtest.CA;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author Nilkanth9581
 *
 */
public class TestPublishCA_NY_ALG {
	private static final String caContentFile1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA\\RC-CA-CW-10012014-V02\\RC-CA-CW-10012014-V02\\RC-CA-CW1-10012014-V02.xml";
	private static final String caContentFile2 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA\\RC-CA-CW-10012014-V02\\RC-CA-CW-10012014-V02\\RC-CA-CW2-10012014-V02.xml";
	private static final String caContentFile3 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA\\RC-CA-NY-07012015-V02\\RC-CA-NY-07012015-V02\\RC-CA-NY-07012015-V02.xml";
	//private static final String caAlgoFile = "D:\\files-by-rajesh\\ALG-CA-NY-10012012-V01_MR_P3-2.xml";
	private static final String caCWAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA-NY\\ALG-CA-CW-10012014-V02_MR-MODIFIED.xml";
	private static final String caNYAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA\\ALG-CA-CW-10012014-V02_MR-MODIFIED-NEW\\ALG-CA-CW-10012014-V02_MR-MODIFIED-NEW\\ALG-CA-NY-07012015-V02_MR_MODIFIED.xml";
	private static final String testCAFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA\\RC-CA-NY-07012015-V02\\test-ca-ny.xml";
	private static final String testCACWFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\CA\\RC-CA-CW-10012014-V02\\test-ca-cw.xml";
	private static final String minitest = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\minitest.xml";
	private static final String PUBLISH_GLOBAL_LOOKUPS = "N";
	private static final String PUBLISH_ALGORITHM_STEPS = "N";
	String str ="Created expression";
	private static final String LINE_OF_BUSINESS = "CA";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = true;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	String s = "StartsWith ";
	
	//TARGET DATABASE CONFIGURATION [ICM_DEV]
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.244.144:1521:DEVBID";
	private static final String DB_USER_ICM_DEV = "stg_core";
	private static final String DB_PASSWD_ICM_DEV = "stg_core";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	private static final String LANGUAGE_TEXT_FILE_DIRECTORY = "D://DEMO_ISO_ERC_29th_JAN//intermediary-language-text-file-created";
	private static final String LANGUAGE_TEXT_FILE_NAME = "language-text-file";
	
	/*public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE");
		dbConfiguration.setDatabasePassword("ISORATING_BAS123E");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}*/
	
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.16555:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	public DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		basicConfiguration.setStateWideAlgorithms(false);
		return basicConfiguration;
	}
	
	@Test
	public void testISoConForCA() throws Exception{
		
		Reader algoReader = new FileReader(caCWAlgoFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForTest(), null);
		
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		Reader contentFileReader1 = new FileReader(new File(caContentFile1));
		Reader contentFileReader2 = new FileReader(new File(caContentFile2));
		Reader contentFileReader3 = new FileReader(new File(caContentFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
	
	
	public static void method(String name){
		name = name.toUpperCase();
	}
	
	public static void main(String[] args) {
		String s = "Premium HiredAutoFactor AccidentPreventionDiscountFactor FleetFactor PremiumILFDeductible auxillaryRunningLampsDiscountFactor BasePremium liabilityCombinedRatingFactor amusementDeviceFactor PremiumPriorToMinimum showroomLiabilityMinimumPremium bobtailLiabilityFactor TypeFactor PIPFactor SchoolBusProrateFactor taxiLimousineTypeFactor liabilityPrimaryRatingFactor showroomLiabilityFactor ClassFactor totalNumberVehicleDaysLeasedPerYear PremiumAfterMinimum LocationPremium locationPremium nonOwnedTotalFactor lcmLiability territoryFactor PreliminaryBasePremium IncreaseLiabilityFactor DeductibleFactor territory TerritoryFactor LookupZoneRatedLiabilityBasePremium LookupPublicTransportationLiabilityBasePremium days CostOfHirePerHundred PreliminaryBasicPremium LookupSpecialTypesAmbulanceFactor LookupSpecialTypesDriverTrainingFactor LookupSpecialTypesFireDepartmentFactor LookupSpecialTypesFuneralDirectorFactor LookupSpecialTypesLawEnforcementFactor LookupSpecialTypesGolfCartsAndLowSpeedVehiclesFactor LookupSpecialTypesLeasingConcernFactor LookupSpecialTypesLeasingConcernMobileHomeFactor LookupSpecialTypesLeasingConcernMiscellaneousFactor LookupSpecialTypesMobileHomeFactor LookupSpecialTypesMotorcycleLiabilityFactor LookupSpecialTypesSpecialEquipmentFactor LookupSpecialTypesSpecialEquipmentFactor1 ratingPrimaryClassCode LookupTruckLiabilityBasePremium IncreaseLiabilityClass garagingZone farthestTerminalZone MinimumPremium broadFormProductsFactor liabilityUnlimitedFactor ratingUnitsTotal liabilityTrailerFactor hundredDollarDeductibleFactor AggregateLimitFactor PreliminaryMinimumPremium MinimumPremiumFactor LookupGarageDealersLiabilityBasePremium Territory publicTransportationGroup InsuredPrimaryFactor supplementaryType LookupTruckIncreaseLimitFactorClass basePremium sizeClass PremiumIndicator BasicLimitsPremium EarnedInFullIndicator GarageLiabSurchargeToReachMinPremium ProRateIndicator seasonalProduceMonths seasonalProduceFactor trailerHowUsed classType garagingLocation type fleet ZoneRated ../LiabilityTrailerFactor ../LiabilityUnlimitedFactor ../EmployeeLiabilityFactor ../EmployeesFactor ../EmployeesAsInsuredsFactor ../NonOwnedPartnershipFactor ../NonOwnedPartnershipRate ../NonOwnedTotalFactor HighestLiabilityBasePremium HighestLiabilityBaseTerritory ../AuxillaryRunningLampsDiscountFactor ../TaxiLimousineTypeFactor ../ShowroomLiabilityMinimumPremium ancestor::*[name(.)='CommercialAuto' and not(@offset='Deleted')]/CommercialAutoGarage/CommercialAutoVehicleLiabilityCoverage/Premium ancestor::*[name(.)='CommercialAuto' and not(@offset='Deleted')]/CommercialAutoGarage/CommercialAutoVehicleLiabilityCoverage/GarageLiabSurchargeToReachMinPremium HighestLiabilityBasicLimitsPremium HighestLiabilityPremiumForExperience HighestLiabilityPremiumClassCodeForExperience HighestPrivatePassengerLiabilityPremium employeeLiabilityFactor numberOfEmployees nonOwnedPartnershipFactor numberOfPartners employeesFactor employeesAsInsuredsFactor nonOwnedPartnershipRate garagePremiumToReachMininum unknownClassCode ratingBasisNumeric";
		String []arr = s.split(" ");
		
		for(String st:arr){
			System.out.println(st);
		}
		String name = new String("nilkanth");
		String []nameArr = new String[1];
		nameArr[0]= name;
		new TestPublishCA_NY_ALG().method(nameArr);
		
		//TestPublishCA_NY_ALG.method(name);
		//System.out.println(name);
		
	}
	
	public void method(String arr[]){
		String name = null;
		try{
			name = arr[3];
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("exception throws");
			name = arr[0];
		}
		
		System.out.println("name is ="+name);
	}
	
	
	
	
	
}
