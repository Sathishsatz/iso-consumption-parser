package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author Nilkanth9581
 *	THIS TEST CASE HAS BEEN WRITTEN TO LIST THE NAME OF THE ALGORITHMS
 *  WHICH HAVE STATEMENTS OUT OF PCH BLOCK
 */
public class TestAlgorithmForOutOfPCHstatements {
	
	String caAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-FL-12012012-V01\\ALG-CA-FL-12012012-V01_MR.xml";
	String caAlgoTestFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-FL-12012012-V01\\test-ca.xml";
	private static final String txAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-TX-05012013-V01\\ALG-BP-TX-05012013-V01_MR_REARRANGED.xml";
	private static final String testTxFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-TX-05012013-V01\\TestTXAlgoFile.xml";
	private static final String txAlgoFile_WC = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";
	/**
	 * 
	 */
	@Test
	public void testAlgorithmForStaements(){
		readLOB(getLOBObject());
	}
	
	
	
	public LOB getLOBObject(){
		LOB lob = null;
		try{
			Reader algoReader = new FileReader(new File(txAlgoFile));
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			lob  = (LOB) unmarshal.unmarshal(algoReader);	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Marshalling excpetion:");
		}
		return lob;
	}
	
	
	public void readLOB(LOB lob) {
		List<String> scopeNamesforLoopStmt = new ArrayList<String>();
		List<String> scopeNamesforIfStmt = new ArrayList<String>();
		List<String> scopeNamesforAssignStmt = new ArrayList<String>();	
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref,scopeNamesforLoopStmt,scopeNamesforIfStmt,scopeNamesforAssignStmt);
				}
				
			}
		}
		
		System.out.println(" ------------------ Scope With For Statement outside the PCH -------------------");
		for(String scopeName:scopeNamesforLoopStmt){
			System.out.println(scopeName);
		}
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("--------------------Scopes with if Statement outside the PCH -------------------");
		for(String scopeName:scopeNamesforIfStmt){
			System.out.println(scopeName);
		}
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("---------------------Scope with assign Statement outside the PCH-----------------");
		for(String scopeName:scopeNamesforAssignStmt){
			System.out.println(scopeName);
		}
		System.out.println("--------------------------------------------------------------------------------");
	}

	private void readReference(Reference reference,List<String> scopeNamesforLoopStmt,List<String> scopeNamesforIfStmt,List<String> scopeNamesforAssignStmt) {
		
		
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				readScope((Scope) scope,scopeNamesforIfStmt,scopeNamesforAssignStmt,scopeNamesforLoopStmt,reference.getType());
			}
		}
		
		
	}
	
	private void readScope(Scope scope,List<String>scopeNamesWithIf,List<String>scopeNameWithAssign,List<String>scopeNameWithLoop,String refType) {
		
					//originanNumberOfPCH = pchList.size();
		for (Object obj : scope.getContent()) {
			
			if (obj instanceof If) {
				scopeNamesWithIf.add("scope Name:["+scope.getDbTables()+"] Ref Type:["+refType+"]");
				break;
			} 
			else if (obj instanceof Assign) {
				scopeNameWithAssign.add("scope Name:["+scope.getDbTables()+"] Ref Type:["+refType+"]");
				break;
			} 
			else if (obj instanceof Loop) {
				scopeNameWithLoop.add("scope Name:["+scope.getDbTables()+"] Ref Type:["+refType+"]");
				break;
			} 
		
		}
	}
}
