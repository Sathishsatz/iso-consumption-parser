package com.mmpnc.rating.iso.algorithm.algorithmtest.GL;

import java.io.Reader;
import java.util.List;
import java.util.Map;

import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ExecuteModuleStep;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.wrapper.ICMRequiredData;
import com.mmpnc.rating.iso.wrapper.ISOConsuptionWrapper;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public class TestHelper extends ISOConsuptionWrapper {
	
	public TestHelper(Reader algoReader) {
		super(algoReader);
		// TODO Auto-generated constructor stub
	}
	
	public void testISOConsuptionWrapperWithReaders()throws Exception{
		
	}
	
	public void texasAlgoTesting(List<Reader> contentFileReaderList,String ratingCategory ,String lineOfBusiness)throws Exception{
		readAlgoFileAndCreateICMInputXml(contentFileReaderList,ratingCategory, lineOfBusiness,BasicConfiguration.getInstance().isStateWideAlgorithms());
	}
	
	
	StringBuffer algoName = new StringBuffer();
		
	public  void printAlgoidAndThereNames(){
		/*File f = new File("D:\\DEMO_ISO_ERC_29th_JAN\\Algo_ID_Algo_Names.xml");
		
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(f);
			fileOutputStream.write(algoName.toString().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	/* 
	 * Client Program for this consumption process should pverride this metho
	 */
	public void cosumptionPostProcess(ICMRequiredData icmRequiredData) {
		
		System.out.println("Say Hi");	
		System.out.println("");
		//LookupNameDecider.getInstance().getLookupNamesWithoutKeys();
		//LookupNameDecider.getInstance().printLookupNameMap("D:\\DEMO_ISO_ERC_29th_JAN\\icm-input-xml-created\\lookup_names.xml");
		//System.out.println(LookupNameDecider.getInstance().getLookupNamesWithoutKeys().size());
		
		List<IcmRatingNodeContent> nodeContent = icmRequiredData.getIcmRatingContent().get("Premium Calcualtion");
		
		for(IcmRatingNodeContent ratingNode: nodeContent){
			System.out.println("Algorithm Level:"+ratingNode.getAlgorithmLevel());
			System.out.println("Execute Module Steps:");
			
			for(AlgoStep algoStep:ratingNode.getAlgoStepList()){
				if(algoStep instanceof ExecuteModuleStep){
					ExecuteModuleStep executeModuleStep = (ExecuteModuleStep)algoStep;
					System.out.println(executeModuleStep.getStepType()+":"+executeModuleStep.getModuleName());
				}
			}
			
			Map<String, List<AlgoStep>> pchAndStepsMap = ratingNode.getPchNameAndAlgoSteps();
			
			for(String pchName:pchAndStepsMap.keySet()){
				System.out.println("PCH Name:"+pchName);
				List<AlgoStep> pchSteps = pchAndStepsMap.get(pchName);
				for(AlgoStep step: pchSteps){
					if(step instanceof ExecuteModuleStep){
						ExecuteModuleStep execMod = (ExecuteModuleStep) step;
						System.out.println("Execute Module step in pch:"+execMod);
					}
				}
			}
			
		}
		
		
		
	}

}
