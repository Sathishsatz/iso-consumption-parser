package com.mmpnc.rating.iso.algorithm.algorithmtest.BOP;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;
import com.mmpnc.rating.iso.icmxml.creater.AlgorithmIdCreater;

/**
 * @author nilkanth9581
 *
 */
public class TestPublishMDAlgorithms {
	
	// ALGO FILE AND CONTNET FILE LOCATIONS ON LOCAL MACHINE
	private static final String ALGORITHM_FILE_LOCATION = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-MD-09012012-V01\\ALG-BP-MD-09012012-V01_MR_REARRANGED.xml";
	private static final String CONTENT_FILE_1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-MD-09012012-V01\\RC-BP-MD-09012012-V01\\RC-BP-CW-09012012-V01.xml";
	private static final String CONTENT_FILE_2 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-MD-09012012-V01\\RC-BP-MD-09012012-V01\\RC-BP-MD-09012012-V01.xml";
	private static final String mdTestFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-MD-09012012-V01\\test-md-ref.xml";
	
	
	//REQUIRED PROPERTIES FOR THE ALGORITHM ID GENERATION
	private static final String PRODUCT_NAME = "SILVER";
	private static final String PROGRAM = "BOP";
	private static final String STATE = "MD";
	private static final String EFFECTIVE_DATE = "11-May-2013";
	private static final String EXPIRATION_DATE = "15-May-2013";
	
	
	//TARGET DATABASE CONFIGURATION  [ISO_BASE]
	private static final String DB_URL = "jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01";
	private static final String DB_USER = "ISORATING_BASE";
	private static final String DB_PASSWD = "ISORATING_BASE";
	private static final String DB_DRIVER_NAME = "oracle.jdbc.driver.OracleDriver";
	
	
	//TARGET DATABASE CONFIGURATION [ICM_DEV]
	private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";
	
	//PROPERTIES REQUIRED FOR PUBLISHING GLOBAL LOOKUPS / ALGORITHM STEPS 
	
	private static final String PUBLISH_GLOBAL_LOOKUPS = "Y";
	private static final String PUBLISH_ALGORITHM_STEPS = "Y";
	
	private static final String LINE_OF_BUSINESS = "BOP";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = false;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	//private static final String LANGUAGE_TEXT_FILE_DIRECTORY = "D://DEMO_ISO_ERC_29th_JAN//intermediary-language-text-file-created";
	//private static final String LANGUAGE_TEXT_FILE_NAME = "language-text-file";
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	public DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		//basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		//basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		return basicConfiguration;
	}
	/*//DATABSE CONFIGURATION FOR ISO_BASE DB
	public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl(DB_URL);
		dbConfiguration.setDatabaseUser(DB_USER);
		dbConfiguration.setDatabasePassword(DB_PASSWD);
		dbConfiguration.setDatabaseDriverName(DB_DRIVER_NAME);
		return dbConfiguration;
	}*/
	//DATABASE CONFIGURATION FOR ICM_DEV
	
	/*public DBConfiguration createDbConfigForICMDev(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl(DB_URL_ICM_DEV);
		dbConfiguration.setDatabaseUser(DB_USER_ICM_DEV);
		dbConfiguration.setDatabasePassword(DB_PASSWD_ICM_DEV);
		dbConfiguration.setDatabaseDriverName(DB_DRIVER_NAME_ICM_DEV);
		return dbConfiguration;
	}*/
	
	
	//SETTING REQUIRED DATA FOR ALGORITHMID  GENERATION USING FOLLOWING FILEDS
	//Product Name,Program,State,Effective Date,Expiration Date,Rating Node Name
	private void setRequiredDataForAlgorithmIdGen(){
		AlgorithmIdCreater algorithmIdCreater =  AlgorithmIdCreater.getInstance();
		algorithmIdCreater.setEffectiveDate(EFFECTIVE_DATE);
		algorithmIdCreater.setExpirationDate(EXPIRATION_DATE);
		algorithmIdCreater.setProgram(PROGRAM);
		algorithmIdCreater.setState(STATE);
		algorithmIdCreater.setProductName(PRODUCT_NAME);
	}
	
	@Test
	public void testISoConForMD() throws Exception{
		//SETTING PROPERTIES FOR ALGORITHM ID GENERATION
		setRequiredDataForAlgorithmIdGen();
		//CONFIGURING CONFIG
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForTest(), null);
		Reader algoReader = new FileReader(ALGORITHM_FILE_LOCATION);
		TestHelper testHelper = new TestHelper(algoReader);
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		Reader contentFileReader1 = new FileReader(new File(CONTENT_FILE_1));
		Reader contentFileReader2 = new FileReader(new File(CONTENT_FILE_2));
		//Reader contentFileReader3 = new FileReader(new File(contetFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		//conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
	
}
