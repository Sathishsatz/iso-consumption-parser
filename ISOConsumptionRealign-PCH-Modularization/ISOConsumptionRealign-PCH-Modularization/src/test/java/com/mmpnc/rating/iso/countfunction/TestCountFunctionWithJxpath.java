/*package com.mmpnc.rating.iso.countfunction;

import java.io.File;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.majescomastek.stgicd.rating.dom.BOPStructure;
*//**
 * @author nilkanth9581
 *
 *//*
public class TestCountFunctionWithJxpath  {
	
	
	
	*//**
	 * @param sreRequest
	 * @param xPath
	 * @return
	 *//*
	public double getCount(Object sreRequest , String xPath) {
		JXPathContext context = JXPathContext.newContext(sreRequest);
		if(xPath.contains("SRERequest") || xPath.contains("CCSRERequest"))
		{
			xPath = xPath.replace("CCSRERequest.","").replace("SRERequest.", "");
		}
		xPath = "count("+convertXpathToLowerCase(xPath)+")";
		return (Double)context.getValue(xPath);
	}
	
	
	public Object getAttributeValue(Object sreOrCCSREObj , String xPath)throws Exception{
		
		JXPathContext context = JXPathContext.newContext(sreOrCCSREObj);
		if(xPath.contains("SRERequest") || xPath.contains("CCSRERequest"))
		{
			xPath = xPath.replace("CCSRERequest.","").replace("SRERequest.", "");
		}
		String [] arr = xPath.replace(".", "~").split("~");
		int arrayLength = arr.length;
		StringBuffer xpathBuffer = new StringBuffer();
		xpathBuffer.append("/");
		for(int i=0;i<arrayLength;i++){
			String xpathCont = arr[i];
			if(Character.isUpperCase(xpathCont.charAt(0)) && !Character.isUpperCase(xpathCont.charAt(1)) ){
					String returnedXpathCont = Character.toLowerCase(xpathCont.charAt(0))+(xpathCont.length() > 1 ? xpathCont.substring(1) : "");
					xpathBuffer.append(returnedXpathCont);
					if(i != arrayLength-1)
						xpathBuffer.append("/");
				}
			else{
				xpathBuffer.append(xpathCont);
				if(i != arrayLength-1)
					xpathBuffer.append("/");
			}
			}
		
		Object attributeValue = null;
		//try{
		attributeValue  = context.getValue(xpathBuffer.toString());
		  if(attributeValue == null){
			attributeValue = getAttributeDefaultValue(xpathBuffer.toString());
		  }
		}catch(JXPathNotFoundException exp){
				attributeValue = getAttributeDefaultValue(xpathBuffer.toString());
		}
		return attributeValue;
	}
	
	public Object getAttributeDefaultValue(String xpath) throws ClassNotFoundException, SecurityException, NoSuchFieldException{
		
		String []arr = xpath.split("/");
		
		String object = arr[arr.length-2];
		String attribute = arr[arr.length-1];
		String packagename = "com.majescomastek.stgicd.rating.dom";
		if(object.contains("[")){
			
			object = object.replace("[1]", "");
			object = packagename+"."+object;
			Class<?> c = Class.forName(object);
		    if(attribute.contains("[")){
		    	attribute =attribute.replace("[1]", "");
		    }
			Field f = c.getDeclaredField(attribute);
		    if( f.getType() == String.class ){
		    	return new String("");
		    }
		    else if (f.getType() == BigDecimal.class){
		    	return new BigDecimal("0.0");
		    }
		}
		return null;
	}
	*//**
	 * @param xpath
	 * @return
	 *//*
	public String convertXpathToLowerCase(String xpath){
		
		String origiString = new String(xpath);
		xpath = xpath.replace("/", "~");
		String [] arr = xpath.replace(".", "~").split("~");
		for(String xpathCont : arr){
			if(xpathCont.contains("[") || xpathCont.contains(" and ") || xpathCont.contains(" or ")){
				List<String> contnetList = new ArrayList<String>();
				String predArr[] = xpathCont.split("\\[");
				for(String predCont : predArr){
					
					String [] array = null;
					if(predCont.contains(" and ")){
						array = predCont.split(" and ");
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains(" or ")){
						array = predCont.split(" or ");
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains("=")){
						array = predCont.split("=");
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains(">")){
						array = predCont.split(">");			
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains("<")){
						array = predCont.split("<");
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains("<=")){
						array = predCont.split("<=");
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains(">=")){
						array = predCont.split(">=");
						contnetList.addAll(Arrays.asList(array));
					}
					if(predCont.contains("!=")){
						array = predCont.split("!=");
						contnetList.addAll(Arrays.asList(array));
					}
					else{
						array = new String[]{predCont};
						contnetList.addAll(Arrays.asList(array));
					}
					for(String predString: contnetList){
						predString = predString.trim();
						if(!"".equals(predString) && Character.isUpperCase(predString.trim().charAt(0)) && !Character.isUpperCase(predString.trim().charAt(1)))
						{
							String returnedXpathCont = Character.toLowerCase(predString.charAt(0))+(predString.length() > 1 ? predString.substring(1) : "");
							origiString = origiString.replace(predString, returnedXpathCont);
						}
					}
				}
			}
			else{
				if(!"".equals(xpathCont) && Character.isUpperCase(xpathCont.trim().charAt(0)))
				{
					String []xpathArr = null;
					if(xpathCont.contains("=")){
						xpathArr = xpathCont.split("=");
						for(String predString: xpathArr){
							predString = predString.trim();
							if(!"".equals(predString) && Character.isUpperCase(predString.trim().charAt(0)) && !Character.isUpperCase(predString.trim().charAt(1)))
							{
								String returnedXpathCont = Character.toLowerCase(predString.charAt(0))+(predString.length() > 1 ?predString.substring(1) : "");
								origiString = origiString.replace(predString, returnedXpathCont);
							}
						}
					}
					
					if(!Character.isUpperCase((xpathCont.charAt(1)))){
						String returnedXpathCont = Character.toLowerCase(xpathCont.charAt(0))+(xpathCont.length() > 1 ? xpathCont.substring(1) : "");
						origiString = origiString.replace(xpathCont,returnedXpathCont);
					}
					
				}
			}
		}
		//CONVERTING . IN STRING BY /
		//NEED TO HANDLE THE PREDICATES WITH ../ OR .= THESE ARE SPECIAL CASES WHERE WE CAN NOT REPLACE . WITH /
		//ALSO IF THE XPATH CONTAINS .. IN IT WE CAN NOT CONVERT IT TO /
		StringBuffer buffer = new StringBuffer();
		buffer.append("/");
		for(String st : origiString.split("\\[")){
			if(!st.contains("]")){
				st = st.replace(".", "/");
				buffer.append(st);
			}
			else{
				buffer.append("[");
				String ar [] = st.split("]");
				buffer.append(ar[0]);
				buffer.append("]");
				if(ar.length>1){
					st = ar[1].replace(".", "/");
					buffer.append(st);
				}
			}
		}
		return buffer.toString();
	}
	
	@Test
	public void test() throws Exception{
		String xpathWithPredicate  = "CCSRERequest.GeneralLiability.GeneralLiabilityLocation.GeneralLiabilityClassification[ ../Territory = '001' and ../ProdsCompldOpsBIPDDeductible= 'No Deductible'  and ClassCode = '50019']";
		String xpathWithPredicate2 = "SRERequest.GeneralLiability.GeneralLiabilityLocation.GeneralLiabilityClassification.ClassCode[. = '50019']";
		String xpathWithPredicate3 = "GeneralLiability.GeneralLiabilityLocation.GeneralLiabilityClassification[Id='STG-GLCL-10005'].GeneralLiabilityClassificationPremOpsCoverage";
		String xpathWithPredicate4 = "GeneralLiability.GeneralLiabilityLocation.GeneralLiabilityClassification[ ../Territory = '001' and ../ProdsCompldOpsBIPDDeductible= 'No Deductible'  and ClassCode = '50019']";
		String xpathWithPredicate5 = "GeneralLiability.GeneralLiabilityLocation.GeneralLiabilityClassification";
		String bopPredicate = "BOP.BOPLocation[1].BOPStructure[1].BOPClassification.ClassDescription";
		String predicate = "/generalLiability/generalLiabilityLocation/generalLiabilityClassification[ ../territory= '001' and ../ProdsCompldOpsterritory = '999' and classCode = '50019']";
		System.out.println(predicate.replaceAll("ProdsCompldOpsterritory", "prodsCompldOpsterritory"));
		//new TestCountFunctionWithJxpath().handleCount(xpathWithPredicate);
		Object sreRequest = JAXBUtils.readFromFile(new File("D:\\files-by-shahsi\\Latest-project-with-predicates\\ISOAlgorithmRealign\\SREBOPReq.xml"), com.majescomastek.stgicd.rating.dom.SRERequest.class);
		System.out.println("getCountCount  1= "+new TestCountFunctionWithJxpath().getCount(sreRequest, xpathWithPredicate));
		System.out.println("getCountFunction predicate 2="+new TestCountFunctionWithJxpath().getCount(sreRequest, xpathWithPredicate2));
		System.out.println("getCount Prdicate 3 = "+new TestCountFunctionWithJxpath().getCount(sreRequest, xpathWithPredicate3));
		System.out.println("getCount predicate 4 = "+new TestCountFunctionWithJxpath().getCount(sreRequest, xpathWithPredicate4));
		System.out.println("getCount predicate 5 = "+new TestCountFunctionWithJxpath().getCount(sreRequest, xpathWithPredicate5));
		
		String bopPredicate1 = "BOP.BOPLocation.BOPStructure[../ComputerFraudNumEmployees='0']";
		String bopPredicate2 = "BOP.BOPLocation.BOPStructure[../BOPLocation/Id='Location1']";
		//System.out.println(new TestCountFunctionWithJxpath().getCount(sreRequest, bopPredicate1));
		System.out.println("attribute value ["+new TestCountFunctionWithJxpath().getAttributeValue(sreRequest, bopPredicate)+"]");
		
		//System.out.println(new TestCountFunctionWithJxpath().getCount(sreRequest,xpathWithPredicate));
		//new TestCountFunctionWithJxpath().handleCount(xpathWithPredicate);
	}
}
*/