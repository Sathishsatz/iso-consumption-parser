/*package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.junit.Test;
import com.majescomastek.stgicd.utilities.JAXBUtils;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

*//****
 * @author Nilkanth9581
 **//*
public class TestCaseForLoopThroughFiltersHandling {
	
	public void createDbForLoopThrough(Reader algoFileReader){
		
		LOB lob = null;
		Database db = null;
		try {
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			lob = (LOB) unmarshal.unmarshal(algoFileReader);
		} catch (JAXBException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(Object ref: lob.getContent()){
			
			if(ref instanceof Reference){
				
				if (((Reference) ref).getName().equals("Table Objects")) {
					
					Reference reference =  (Reference)ref;
						//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
					String refInXml = JAXBUtils.writeFromObject(reference);
						// TODO Auto-generated catch block
					
					
					try {
						Writer writer = new FileWriter(new File("D:\\ds\\ds-xml.xml"));
						writer.write(xmlDS.toString());
						writer.flush();
						writer.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("DSFile", refInXml);

					db = DBFactoryBuilder.getXMLDatabase("RuntimeDSDBFORLOOP", xmlMap);
					db.buildDatabase();
				}

			}
			
		}
		
		Connection connection = DBFactoryBuilder.getXmlConnection(db);
		Query query = new XmlQuery(connection);
		query = query.createQuery("/reference[@name='Table Objects']/TableName[@name='BOP']/TableName[@name='BOPLocation']");
		Object returnObejct  = query.execute();
		
		if("".equals(returnObejct)){
			System.out.println("Attribute");
		}
		else{
			System.out.println("Model");
		}
		System.out.println("Done Successfully");
		
	}
	
	private static final String txAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-TX-05012013-V01\\ALG-BP-TX-05012013-V01_MR_REARRANGED.xml";
	
	@Test
	public void testLoopThroughAttributeCondition()throws Exception{
		Reader algoReader = new FileReader(txAlgoFile);
		createDbForLoopThrough(algoReader);
	}
}
*/