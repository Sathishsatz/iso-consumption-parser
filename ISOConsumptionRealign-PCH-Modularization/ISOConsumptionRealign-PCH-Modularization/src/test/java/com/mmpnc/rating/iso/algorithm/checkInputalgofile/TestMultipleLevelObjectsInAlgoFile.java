package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.vo.ColumnNode;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.TableName;
import com.mmpnc.rating.iso.helper.Constants;

/**
 * @author nilkanth9581 THIS TEST CASE WILL CHECK MULTILEVEL ALGORITHM NAMES
 */

public class TestMultipleLevelObjectsInAlgoFile {

	private static Map<String, String> javaKeyWordsAndReplacements = null;
	// private static final String ALGORITHM_FILE_LOCATION =
	// "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";
	private static final String ALGORITHM_FILE_LOCATION = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\FileFromParser3.2.2\\ALG-CA-NY-07012014-V01_MR.xml";

	private static void intializeKeyWordsMap() {
		javaKeyWordsAndReplacements = new HashMap<String, String>();
		javaKeyWordsAndReplacements.put("Class", "Classs");
		javaKeyWordsAndReplacements.put("Default", "Defaultt");
	}

	/**
	 * @param column
	 * @return THIS METHOD IS ADDED TO HANNDLE JAVA KEYWORDS RELATED MODEL NAMES
	 * 
	 */
	private static String getColumnContent(ColumnNode column) {
		if (javaKeyWordsAndReplacements.containsKey(column.getContent().trim())) {
			return javaKeyWordsAndReplacements.get(column.getContent().trim());
		}
		return column.getContent();
	}

	/**
	 * @param ref
	 * @return
	 */
	private StringBuffer simplifyDataStrucure(Reference ref) {

		intializeKeyWordsMap();

		StringBuffer content = new StringBuffer();
		content.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
		content.append("<Policy xpath='Policy'>");
		content.append(createWrapperNodeContent());
		for (Object obj : ref.getContent()) {
			if (obj instanceof TableName) {
				readTable(content, (TableName) obj, new StringBuffer("Policy"),
						new StringBuffer("Policy"));
			}

		}
		content.append("</Policy>");

		return content;
	}

	/**
	 * @param content
	 * @param table
	 * @param priorXPath
	 * @param priorQualPath
	 */
	private static void readTable(StringBuffer content, TableName table,
			StringBuffer priorXPath, StringBuffer priorQualPath) {

		StringBuffer xpath = getPath(table.getName(), priorXPath, "/");
		// StringBuffer qualpath = getPath(table.getName(),priorQualPath,".");

		StringBuffer qualpath = new StringBuffer();

		content.append("<").append(table.getName());
		content.append(" ").append("xpath='").append(xpath).append("'");
		// content.append(" ").append("qualPath='").append(qualpath).append("'");
		content.append(" ").append("multiple='")
				.append(table.isMultipleAllowed()).append("'");
		content.append(">\n");

		for (Object obj : table.getContent()) {
			if (obj instanceof ColumnNode) {
				// read the columns here
				readColumn(content, (ColumnNode) obj, xpath, qualpath);
			} else if (obj instanceof TableName) {
				// read the other content here
				readTable(content, (TableName) obj, xpath, qualpath);
			}
		}

		content.append("</").append(table.getName()).append(">\n");
	}

	/**
	 * @param content
	 * @param column
	 * @param xpath
	 * @param qualpath
	 */
	private static void readColumn(StringBuffer content, ColumnNode column,
			StringBuffer xpath, StringBuffer qualpath) {
		content.append("<").append(column.getContent());
		content.append(" ").append("xpath='").append(xpath).append("/")
				.append(getColumnContent(column)).append("'");
		// content.append(" ").append("qualPath='").append(qualpath).append(".").append(column.getContent()).append("'");
		content.append(" ").append("multiple='false'");
		content.append("/>\n");
	}

	/**
	 * @param nodeName
	 * @param priorPath
	 * @param joiner
	 * @return
	 */
	private static StringBuffer getPath(String nodeName,
			StringBuffer priorPath, String joiner) {
		// System.out.println("joiner");
		StringBuffer buffer = new StringBuffer();
		if (priorPath == null) {
			buffer.append(nodeName);
		} else {
			buffer.append(priorPath).append(joiner).append(nodeName);
		}
		return buffer;
	}

	/**
	 * @return
	 */
	private static StringBuffer createWrapperNodeContent() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("	<EffDate xpath='Policy/EffDate'/>");
		buffer.append("	<ExpDate xpath='Policy/ExpDate'/>");
		buffer.append("	<Premium xpath='Policy/Premium'/>");
		buffer.append(" <StateId xpath='CCSRERequest/StateId'/>");
		buffer.append("	<State xpath='Policy/State'>");
		buffer.append("		<Code xpath='Policy/State/Code'/>");
		buffer.append("		<Id xpath='Policy/State/Id'/>");
		buffer.append("		<Name xpath='Policy/State/Name'/>");
		buffer.append("	</State>");
		// TRANSACTION MODEL IS ADDED AS IT IS USED INT CA FL ALGORITHM FILE
		buffer.append(" <Transaction xpath='Policy/Transaction'>");
		buffer.append("		<UnitNumber xpath='Policy/Transaction/UnitNumber'/>");
		buffer.append("		<Status xpath='Policy/Transaction/Status'/>");
		buffer.append("	</Transaction>");
		return buffer;
	}

	private Database createDatabaseForCompleteAlgorithmFile(LOB lob) {

		Map<String, String> xmlMap = new HashMap<String, String>();
		// xmlMap.put("DSFile", xmlDS.toString());
		// dsDB =
		// DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEDSDB,
		// xmlMap);

		return null;
	}

	/**
	 * @param lob
	 * @return
	 */
	private Database createDataBaseForTableObjects(LOB lob) {

		Database dsDB = null;
		for (Object lobContent : lob.getContent()) {

			if (lobContent instanceof Reference) {
				Reference ref = (Reference) lobContent;

				if (((Reference) ref).getName().equals("Table Objects")) {

					StringBuffer xmlDS = simplifyDataStrucure((Reference) ref);
					// writeDataStrucutreFile(xmlDS);
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("DSFile", xmlDS.toString());
					try {
						dsDB = DBFactoryBuilder.getXMLDatabase(
								BasexDatabaseNames.RUNTIMEDSDB, xmlMap);
						dsDB.buildDatabase();
					} catch (BasexException e) {
						System.out.println("BasexException" + e.getMessage());
					} finally {
						try {
							// dsDB.closeDatabase();
						} catch (BasexException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}

		}
		return dsDB;
	}

	class RefnameAndCount {

		String refName;
		Integer count;

		public String getRefName() {
			return refName;
		}

		public void setRefName(String refName) {
			this.refName = refName;
		}

		public Integer getCount() {
			return count;
		}

		public void setCount(Integer count) {
			this.count = count;
		}

	}

	/**
	 * @param dataStructureDB
	 * @param lob
	 */
	private void checkMultipleLevelAlgorithms(Database dataStructureDB, LOB lob) {
		List<Object> lobContent = lob.getContent();
		int numberOfAlgorithm = 0;
		StringBuffer multipleLevelAlgorithmsReprot = new StringBuffer();
		Map<String, RefnameAndCount> numberOfAlgosMap = new HashMap<String, RefnameAndCount>();

		for (Object cont : lobContent) {

			if (cont instanceof Reference) {

				Reference reference = (Reference) cont;
				String refType = reference.getType();
				String refName = reference.getName();

				if (reference.getType().equals("Common Rating")
						|| reference.getType().equals("Premium Calculation")) {
					List<Object> refContent = reference.getContent();

					for (Object scopeCont : refContent) {

						if (scopeCont instanceof Scope) {

							Scope scope = (Scope) scopeCont;
							String scopeName = scope.getDbTables();

							String numberOfAlgoMapKey = refType + "_"
									+ scopeName + "_" + scope.getPass();

							if (numberOfAlgosMap
									.containsKey(numberOfAlgoMapKey)) {
								RefnameAndCount reFnameAndCount = numberOfAlgosMap
										.get(numberOfAlgoMapKey);
								int counter = reFnameAndCount.getCount();
								reFnameAndCount.setCount(++counter);
								String newRefName = reFnameAndCount
										.getRefName() + "," + refName;
								reFnameAndCount.setRefName(newRefName);

							} else {
								RefnameAndCount refnameAndCount = new RefnameAndCount();
								refnameAndCount.setCount(1);
								refnameAndCount.setRefName(refName);
								numberOfAlgosMap.put(numberOfAlgoMapKey,
										refnameAndCount);
							}

							String fullyQualPath = getScopePath(
									dataStructureDB, scopeName);
							int arrLength = fullyQualPath.split(" ").length;
							if (arrLength > 1) {
								getAlgorithmAndItsLevle(
										fullyQualPath.split(" "), scopeName);
								numberOfAlgorithm++;
								multipleLevelAlgorithmsReprot
										.append("Algorithm Name:[" + scopeName
												+ "] Number Of Levels :["
												+ arrLength + "]");
								multipleLevelAlgorithmsReprot.append("\n");
							}

						}
					}

				}
			}

		}

		System.out.println("Number of total multilevel algorithms "
				+ numberOfAlgorithm);
		System.out.println("----------------COMPLETE REPORT--------------");
		System.out.println(multipleLevelAlgorithmsReprot.toString());
		System.out.println("----------------THE END----------------------");

		System.out.println("COMPLETE REPORT FOR THE LEVELS OF ALGORITHM");
		System.out.println(algorithmAndItsLevels.toString());
		System.out.println("HMM");

		System.out
				.println("--------Algorithms which appeard more than once in algorithm file-------");
		for (String mapKey : numberOfAlgosMap.keySet()) {
			RefnameAndCount refnameAndCount = numberOfAlgosMap.get(mapKey);

			if (refnameAndCount.getCount() > 1)
				System.out.println("Scope:[" + mapKey.substring(mapKey.indexOf("_")+1,mapKey.lastIndexOf("_"))
						+ "]Occurrences:["
						+ refnameAndCount.getCount() + "] RefName:["+refnameAndCount.getRefName()+"]");
		}

	}

	private void writeDataStrucutreFile(StringBuffer xmlDS) {

		// BasicConfiguration basicConfiguration =
		// BasicConfiguration.getInstance();
		// if("Y".equals(basicConfiguration.getPublishAlgorithmSteps())){
		try {
			Writer writer = new FileWriter(new File("D:\\ds\\ds-xml.xml"));
			writer.write(xmlDS.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// }
	}

	private StringBuffer algorithmAndItsLevels = new StringBuffer();

	/**
	 * @param arr
	 * @param scopePath
	 */
	private void getAlgorithmAndItsLevle(String arr[], String scopePath) {
		algorithmAndItsLevels.append("ALGORITHM:[" + scopePath + "] LEVELS:[");
		for (String algorithmLevle : arr) {
			algorithmLevle = algorithmLevle.replace("." + scopePath, "");
			algorithmLevle = algorithmLevle.substring(algorithmLevle
					.lastIndexOf(".") + 1);
			algorithmAndItsLevels.append(algorithmLevle);
			algorithmAndItsLevels.append(",");

		}
		algorithmAndItsLevels.append("]");
		algorithmAndItsLevels.append("\n");
	}

	/**
	 * @param db
	 * @param scopeDbTable
	 * @return
	 */
	public static String getScopePath(Database db, String scopeDbTable) {
		Connection conn = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(conn);

		StringBuffer qry = new StringBuffer();
		qry.append("//").append(scopeDbTable).append("/@xpath/data()");

		query.createQuery(qry.toString());
		String retString = (String) query.execute();

		if (retString != null) {
			if (retString.contains("Policy")) {
				retString = retString.replace("Policy",
						Constants.CURRENT_CONTEXT_OBJECT_NAME);
			}
			retString = retString.replaceAll("/", ".");
			return retString;
		} else {
			return "";
		}
	}

	private void startTest() {
		try {
			// UNMARSHALLING ALGORITHM FILE USING JAXB
			Reader algoReader = new FileReader(ALGORITHM_FILE_LOCATION);
			JAXBContext jbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshal.unmarshal(algoReader);
			// CREATING SIMPLIFIED DATASTRUCTURE DATABASE
			Database dataStrucutreDB = createDataBaseForTableObjects(lob);
			// CHECKING ALGORITHMS FOR ITS MULTIPLE LEVEL PRESENCE
			checkMultipleLevelAlgorithms(dataStrucutreDB, lob);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * THIS TEST CASE WILL FIND THE NUMBER OF LEVELS AN ALGORITHM IS PRESENT AND
	 */
	@Test
	public void testFindMultipleLevelAlgorithms() {
		startTest();
	}

}
