package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.connection.db.impl.OracleDbConnection;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

public class TestDeleteAllTheDataOFAnAlgorithm {
	
	
	private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";
	
	
	private static final String algorithmId = "1O7R5A8LQQFKBRC7DQVI68SLKQ";
	
	//private static String driverName
	/*static{
		dbUrl ="" ;
	}*/
	
	
	/*2J3B42IDC08JJESR4NURQCT492	09-JUN-13	p		
	4CKB6G6SLV9GRVMSCSF105VIG0	09-JUN-13	p		
	GDQBDS2O32OR23QCA0NQEFL9	09-JUN-13	p		
	4IDVRRDL0GGN63I6VTD3NQQPNK	09-JUN-13	p		
	702BCHSQGTOM5VVJ238MEN09C2	09-JUN-13	p		
	1O7R5A8LQQFKBRC7DQVI68SLKQ	09-JUN-13	p		
	6SENVPU1SRGI959VT1SK1S8253	09-JUN-13	p		
	3V13GFP48HJ8ATG0MTCG62SL2B	09-JUN-13	p		
	757PJ6BM6QAA37NICFRJO2Q8E2*/
	
	
	private static DBConfiguration createDBConfiguration(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl(DB_URL_ICM_DEV);
		dbConfiguration.setDatabaseUser(DB_USER_ICM_DEV);
		dbConfiguration.setDatabasePassword(DB_PASSWD_ICM_DEV);
		dbConfiguration.setDatabaseDriverName(DB_DRIVER_NAME_ICM_DEV);
		return dbConfiguration;
	}
	
	
	public void deleteData(Connection oracleDbConnection,String algorithmId) throws SQLException{
		
		String algoRetrunAtt = "delete from algo_dtl_lookup_crtr_ret_att where algorithm_step_id in(select algorithm_step_id from algorithm_detail where algorithm_id=?)";
		String lookupfltext = "delete from algo_dtl_lookup_crtr_flt_extn where algorithm_step_id in(select algorithm_step_id from algorithm_detail where algorithm_id=?)";
		String lookFltr = "delete from lookup_criteria_filter where criteria_id in (select criteria_id FROM algorithm_detail where algorithm_id=?)";
		String lookupCrtr = "delete from lookup_criteria where criteria_id in (select criteria_id FROM algorithm_detail where algorithm_id=?)";
		String lookcrtrretattr = "delete from lookup_crtr_return_attributes where criteria_id in (select criteria_id FROM algorithm_detail where algorithm_id=?)";
		String algoDetail = "delete from algorithm_detail  where algorithm_id=?";
		String algoMaster = "delete from algorithm_master where algorithm_id=?";
		
		List<String>queries = new ArrayList<String>();
		queries.add(algoRetrunAtt);
		queries.add(lookupfltext);
		queries.add(lookupCrtr);
		queries.add(lookFltr);
		queries.add(lookcrtrretattr);
		queries.add(algoDetail);
		queries.add(algoMaster);
		
		
		for(String query:queries){
			PreparedStatement prep1 = oracleDbConnection.prepareStatement(query);
			prep1.setString(1, algorithmId);
			try{
			prep1.execute();
			}
			catch(SQLException e){
				e.printStackTrace();
				throw e;
			}
			}
		
		oracleDbConnection.commit();
		oracleDbConnection.close();
		
	}
	
	
	public void deleteAlgorithmData(String algorithmId) throws SQLException{
		DBConfiguration dbConfiguration = createDBConfiguration();
		Configurer configurer = Configurer.getInstance();
		configurer.buildConfigurer(BasicConfiguration.getInstance(), dbConfiguration, null);
		OracleDbConnection oracleDbConnection = OracleDbConnection.getInstance();
		deleteData(oracleDbConnection.getConnection(), algorithmId);
		
	}
	
	
	
	
	@Test
	public void testDeleteData()throws Exception{
		if(algorithmId.contains(",")){
			String [] ids = algorithmId.split(",");
			for(String id : ids){
				deleteAlgorithmData(id);
			}
		}
		else{
			deleteAlgorithmData(algorithmId);
		}
		
	}
}
