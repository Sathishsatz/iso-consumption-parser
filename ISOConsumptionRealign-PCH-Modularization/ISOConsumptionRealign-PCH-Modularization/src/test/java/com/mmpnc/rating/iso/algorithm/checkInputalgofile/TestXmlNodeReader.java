package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.rating.iso.algorithm.exception.UnInitializeContextException;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.xml.XMLNodeReader;

/**
 * @author nilkanth9581
 *
 */
public class TestXmlNodeReader {
	
	private String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-DE-02012014-V01\\test.xml";
	
	public void testInputAlgorithmFile() throws JAXBException, FileNotFoundException{
		Reader fileReader = new FileReader(new File(fileName));
		JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
		Unmarshaller unmarshal = jbContext.createUnmarshaller();
		LOB lob = (LOB) unmarshal.unmarshal(fileReader);
		Context context = new ContextBase();
		XMLNodeReader xmlNodeReader = new XMLNodeReader(context);
		
		for(Object ref:lob.getContent()){
			
			if(ref instanceof Reference){
				
				List<Object> refContent = ((Reference) ref).getContent();
				
				
				if(((Reference) ref).getType().equals("Common Rating") || ((Reference) ref)
						.getType().equals("Premium Calculation")){
					for(Object refCont:refContent){
						if(refCont instanceof Scope){
							Scope scope = (Scope)refCont; 
							List<Object> scopeContList = scope.getContent();
							for(Object scopeCont :scopeContList){
								if(scopeCont instanceof PCH){
									StringBuffer buffer = new StringBuffer();
									try {
										xmlNodeReader.readPCH((PCH)scopeCont, buffer);
										} catch (UnInitializeContextException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									System.out.println(buffer);
								}
							}
							
						}
					}
				}
				
			}
		}
		
		
		
		
		
	}
	@Test
	public void testXmlNodeReader()throws Exception{
		testInputAlgorithmFile();
	}
	
}
