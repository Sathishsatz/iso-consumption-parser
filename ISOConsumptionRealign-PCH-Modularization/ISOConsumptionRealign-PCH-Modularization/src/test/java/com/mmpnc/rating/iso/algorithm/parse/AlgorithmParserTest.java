package com.mmpnc.rating.iso.algorithm.parse;

import org.junit.Test;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.rating.iso.algorithm.AlgorithmProcessor;
import com.mmpnc.rating.iso.algorithm.sort.AlgorithmSorter;

public class AlgorithmParserTest {

	@Test
	public void testParse(){
		Context context = new ContextBase();
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("ProdsCompldOpsPDDeductible = \"No Deductible\"");
		
		AlgorithmProcessor processor = new AlgorithmSorter(context, buffer );
		try {
			processor.execute();
		} catch (Exception e) {
			System.out.println("Error occured " + e.toString());
		}
		
		
	}
	
}
