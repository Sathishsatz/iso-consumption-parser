package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Premium;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

public class TestMergeFlowChartRatingAndPremiumNodes {

	public LOB getLOBObject(FileReader algoFileReader) {

		JAXBContext jaxbContext;
		try {

			jaxbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshaller.unmarshal(algoFileReader);
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void modifyFlowChart(LOB lob) {
		for (Object object : lob.getContent()) {
			if (object instanceof Reference) {
				Reference reference = (Reference) object;
				if (reference.getName().equals("FlowChart")) {
					List<Object> content = reference.getContent();
					for(Object classObj:content){
						if(classObj instanceof Class){
							readClass((Class)classObj,new StringBuffer(),new StringBuffer());
						}
					}
				}

			}
		}
		
		for(String key:map.keySet()){
			System.out.println("Scope:"+key);
			System.out.println("\t\t\t Premium Node:"+map.get(key).getPremium());
			System.out.println("\t\t\t Rating Node:"+map.get(key).getRating());
		}
	}

	class ClassInfo{
		String rating;
		String premium;
		public String getRating() {
			return rating;
		}
		public void setRating(String rating) {
			this.rating = rating;
		}
		public String getPremium() {
			return premium;
		}
		public void setPremium(String premium) {
			this.premium = premium;
		}
	   
	}
	
	Map<String, ClassInfo> map = new HashMap<String, ClassInfo>();
	
	private void readClass(Class classs,StringBuffer ratingVar,StringBuffer premiumVar) {
		List<Object> list = classs.getContent();
		
		for (Object object : list) {

			if (object instanceof Class) {
				Class classNew  = (Class)object;
				StringBuffer ratingVars = new StringBuffer();
				StringBuffer premiumVars = new StringBuffer();
				String classKey = classs.getName()+classs.getType()+classs.getPass();
				if(!map.containsKey(classKey)){
					map.put(classKey, new ClassInfo());
				}
				readClass(classNew,ratingVars,premiumVars);
			} 
			else if (object instanceof Premium) {
				
				Premium premium = (Premium) object;
				String classKey = classs.getName()+classs.getType()+classs.getPass();
				if(map.containsKey(classKey)){
					ClassInfo classInfo = map.get(classKey);
					if(classInfo.getRating() == null)
					classInfo.setPremium(premium.getVariables());
					else
					classInfo.setPremium(classInfo.getPremium().concat(",").concat(premium.getVariables()));
				}
				premiumVar.append(premium.getVariables());
			} 
			else if (object instanceof Rating) {
				Rating rating = (Rating) object;
				String classKey = classs.getName()+classs.getType()+classs.getPass();
				if(map.containsKey(classKey)){
					ClassInfo classInfo = map.get(classKey);
					if(classInfo.getRating() == null)
					classInfo.setRating(rating.getVariables());
					else
					classInfo.setRating(classInfo.getRating().concat(",").concat(rating.getVariables()));
				}
				ratingVar.append(rating.getVariables());
			}
		}
		
		//Now modifying flowchart 
		modifyClassInFlowChart(ratingVar, premiumVar, classs);
		
		
		
	}
	
	private void modifyClassInFlowChart(StringBuffer ratingVars,StringBuffer premiumVars,Class classs){
		
		//case1: when we have both rating and premium node then add premium node variables at 
		//the end of the rating node varaibles
		if(ratingVars.length() >1 && premiumVars.length()>1){
			//now adding functionality for modifying 
			//1- if a class has both rating and premium nodes
			if(ratingVars.length() >1 && premiumVars.length()>1){
				List<Object> content = classs.getContent();
				for(Object cont:content){
					if(cont instanceof Rating){
						Rating rating = (Rating)cont;
						rating.setVariables(rating.getVariables().concat(",").concat(premiumVars.toString()));
					}
				}
			}
		}
		//case 2: When there isn't any rating node but a premium node then we convert that premium node into
		//rating node
		if(ratingVars.length() == 0 && premiumVars.length()>1){
			List<Object> content = classs.getContent();
			Rating rating = null;
			for(Object cont:content){
				if(cont instanceof Premium){
					Premium premium = (Premium)cont;
					//creating a rating node and adding it to the class
					rating = new Rating();
					rating.setVariables(premium.getVariables());
					//content.add(rating);
				}
			}
			content.add(rating);
			
		}
		//case 3: if both nodes are not present then we can keep class as it is
		//case 4: if there is only a rating node then we will keep as it is so that our current functionality will work
		//without any changes
		
	}
	
	/**
	 * @param lob
	 */
	private void writeLOB(LOB lob) throws IOException,Exception{
			String fileNameLoc = fileName.substring(0,fileName.lastIndexOf("\\")+1);
			File f = new File(fileNameLoc+"\\"+fileNameNew);
			FileOutputStream fileOutputStream = new FileOutputStream(f);
			fileOutputStream.write(JAXBUtils.writeFromObject(lob).getBytes());	
	}
		String fileNameNew = "modified-flowchart.xml";
		String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\GL files from parser 3.2.2\\test-flowchart.xml";
	//private static final String txAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\CA_NY_TEST.xml";
	
		
	@Test
	public void testCheckFlowChart(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			modifyFlowChart(lob);
			writeLOB(lob);
			//System.out.println("TOTAL LOOP FOUND="+loopCount);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
