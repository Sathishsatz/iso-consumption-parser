package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author nilkanth9581
 *
 */
public class TestPublishWAAlgorithms {
	private static final String waContentFile1= "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-WA-08012013-V01\\RC-BP-WA-08012013-V01\\RC-BP-CW-07012013-V01.xml";
	private static final String waContentFile2= "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-WA-08012013-V01\\RC-BP-WA-08012013-V01\\RC-BP-WA-08012013-V01.xml";
	
	private static final String waAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-WA-08012013-V01\\ALG-BP-WA-08012013-V01_MR.xml";
	private static final String PUBLISH_GLOBAL_LOOKUPS = "Y";
	private static final String PUBLISH_ALGORITHM_STEPS = "Y";
	
	private static final String LINE_OF_BUSINESS = "BOP";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = false;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	
	
private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		//basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		//basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		return basicConfiguration;
	}

	public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE");
		dbConfiguration.setDatabasePassword("ISORATING_BASE");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}
	
	@Test
	public void testISoConForWA() throws Exception{
		Reader algoReader = new FileReader(waAlgoFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDbConfig(), null);
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		
		Reader contentFileReader1 = new FileReader(new File(waContentFile1));
		Reader contentFileReader2 = new FileReader(new File(waContentFile2));
		//Reader contentFileReader3 = new FileReader(new File(contetFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		//conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
}
