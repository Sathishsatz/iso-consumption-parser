package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.rating.iso.datastructure.AlgorithmDataStructure;

/**
 * @author nilkanth9581
 * THIS TEST CASE HAS BEEN WRITTEN TO CHECK THE LOOP THROUGH CONDITIONS
 * IN THE ALGORITHM FILE BEFORE WE START THE ALGORITHM CONSUMPTION
 * 
 */
public class TestLoopThroughConditionsInAlgoFile {
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private Database createDataBaseForTableObjects(LOB lob){
		
		Database dsDB = null;
		for(Object lobContent:lob.getContent())
		{
			
			if(lobContent instanceof Reference){
				Reference ref = (Reference)lobContent;
				
				
				if (((Reference) ref).getName().equals("Table Objects")) {
					
					AlgorithmDataStructure  ds = new AlgorithmDataStructure(
							(Reference) ref);
					StringBuffer xmlDS = ds.simplifyDataStrucure();
					/*try {
						Writer writer = new FileWriter(new File("D:\\ds\\ds-xml.xml"));
						writer.write(xmlDS.toString());
						writer.flush();
						writer.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("DSFile", xmlDS.toString());
					
					
					try{
						dsDB = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEDSDB, xmlMap);
						dsDB.buildDatabase();
						}catch(BasexException e){
							System.out.println("BasexException"+e.getMessage());
						}finally{
							try {
								dsDB.closeDatabase();
							} catch (BasexException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
				}
			}

		}
		return dsDB;
	}
	
	
	public void readLOB(LOB lob) {
		Database dsDB = createDataBaseForTableObjects(lob);
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref,dsDB);
				}
				
			}
		}
	}
	
	
	

	private void readReference(Reference reference, Database dsDB) {
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope)scope;
				readScope(curScope,dsDB);
				//currentScopeDB.closeDatabase();
			}
		}
	}
	
	
	private void readScope(Scope scope,Database dsDB) {
		//setting the current scope path 
		currentScopePath = scope.getDbTables();
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj,dsDB);
			} else if (obj instanceof If) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readIf((If) obj,dsDB);
			} else if (obj instanceof Assign) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readAssign((Assign) obj,dsDB);
			} else if (obj instanceof Loop) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readLoop((Loop) obj,dsDB);
			} else if (obj instanceof Class) {
			}
		}
	}
	
	private void readPCH(PCH pch,Database dsDB) {

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj,dsDB);
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj,dsDB);
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj,dsDB);
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	}
	
	
	private void readIf(If _if,Database dsDB ) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj,dsDB);
			} else if (obj instanceof Then) {
				readThen((Then) obj,dsDB);
			} else if (obj instanceof Else) {
				readElse((Else) obj,dsDB);
			}
		}
	}

	private void readCondition(Condition condition,Database dsDB) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,dsDB);
			}
		}
	}

	private void readThen(Then then,Database dsDB ) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,dsDB );
			} else if (obj instanceof If) {
				readIf((If) obj,dsDB );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,dsDB);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	
	//private int loopIndex = 0;
	private String currentScopePath = null;
	private static int loopCount =0;
	private void readLoop(Loop loop,Database dsDB ) {
		loopCount++;
		checkLoopThroughCondtion(loop.getThrough(), dsDB);
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,dsDB );
			} else if (obj instanceof If) {
				readIf((If) obj,dsDB);
			} else if (obj instanceof Loop) {
				String originalScopePath = new String(currentScopePath);
				currentScopePath = loop.getThrough();
				readLoop((Loop) obj,dsDB);
				currentScopePath = originalScopePath;
				
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void checkLoopThroughCondtion(String loopThroughCond ,Database dsDB){
		
		
		if(loopThroughCond.contains("/*"))
			System.out.println("scope ["+currentScopePath+"] contains loopthoruhg with * ["+loopThroughCond);
		/*StringBuffer queryString = new StringBuffer();
		queryString.append("//").append(currentScopePath).append("/").append(loopThroughCond).append("/@xpath/data()");
		Connection curScopeCon = new XMLConnection(dsDB);
		Query query = new XmlQuery(curScopeCon); 
		query.createQuery(queryString.toString());
		Object returnObject = query.execute();
		if(returnObject == null || "".equals(returnObject)){
			System.out.println("SCOPE NAME:["+currentScopePath+"] LOOP THROGUH CONDITION="+loopThroughCond);
		}*/
		
		if(loopThroughCond.contains("//"))
			System.out.println("Scope ["+currentScopePath+"] contains loop through with // in xpath");
		
	}
	
	private void readElse(Else _else,Database dsDB) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,dsDB );
			} else if (obj instanceof If) {
				readIf((If) obj,dsDB);
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,dsDB);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}


	private void readAssign(Assign assign,Database dsDB) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,dsDB);
			}
		}
	}

	private void readExpression(Expression expression,Database dsDB ) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
//			sb.append(" expression -> " + expression.getOp());
			readBothSides(expression,dsDB);
		} else {
			readContent(expression,dsDB );
		}
	}

	private void readBothSides(Expression expression,Database dsDB ) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex,dsDB);
			} else if (obj instanceof Function) {
				//readFunction((Function) obj,dsDB);
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj,dsDB);
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	private void readContent(Expression expression,Database dsDB) {
		for (Object obj : expression.getContent()) {
			
			if (obj instanceof String) {
				
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj,dsDB);
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj,dsDB);
			} else if (obj instanceof Function) {
				//((Function) obj,dsDB);
			}
		}
	}

				

	//private static final String fileName = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-MD-09012012-V01/ALG-BP-MD-09012012-V01_MR.xml";
	String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\ALG-CA-NY-10012012-V01_MR_REARRANGED.xml";
	//private static final String txAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\CA_NY_TEST.xml";
	@Test
	public void testAlgoFileForPCh(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			readLOB(lob);
			System.out.println("TOTAL LOOP FOUND="+loopCount);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

}	
	
