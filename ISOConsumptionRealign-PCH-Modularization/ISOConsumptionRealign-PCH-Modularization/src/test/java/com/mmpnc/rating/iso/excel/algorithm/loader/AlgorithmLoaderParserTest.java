package com.mmpnc.rating.iso.excel.algorithm.loader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenSource;
import org.antlr.runtime.TokenStream;
import org.junit.Test;

import com.mmpnc.context.Context;
import com.mmpnc.rating.icm.algorithm.vo.Ratebook;

public class AlgorithmLoaderParserTest {

	@Test
	public void TestAlgo() throws JAXBException{
		Ratebook rb = new Ratebook();
		Context context = null;
		StringBuffer buffer = new StringBuffer();
		buffer.append("IF ( Count ( CommercialAuto100DollarDeductibleForCompletedOperationsDoesNotApply ) > 0.0 )  THEN\r\n" + 
				"  HundredDollarDeductibleFactor = RateTable:GarageDealersDeductibleForCompletedOperationsDoesNotApplyFactor ( \"Y\" ) \r\n" + 
				"ELSE\r\n" + 
				"  HundredDollarDeductibleFactor = 1.0 \r\n" + 
				"END IF");
		
		CharStream stream = new ANTLRStringStream(buffer.toString());
		TokenSource tokenSource = new AlgorithmLoaderLexer(stream);
		TokenStream input = new CommonTokenStream(tokenSource);
		AlgorithmLoaderParser parser = new AlgorithmLoaderParser(input);
		try {
			parser.algorithm(rb, context);
		} catch (RecognitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JAXBContext jb = JAXBContext.newInstance("com.mmpnc.rating.icm.algorithm.vo");
		Marshaller mr = jb.createMarshaller();
		mr.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
		mr.marshal(rb, System.out);
	}
}
