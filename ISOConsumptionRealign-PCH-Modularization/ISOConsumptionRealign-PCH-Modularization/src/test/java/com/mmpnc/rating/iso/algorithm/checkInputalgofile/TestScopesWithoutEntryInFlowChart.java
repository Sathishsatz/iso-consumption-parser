package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.correction.RatingObjectConverter;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.RatingHolder;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.thoughtworks.xstream.XStream;

/**
 * @author nilkanth9581
 * THIS TEST CASE WILL CHECK THE SCOPES WHICH DO NOT HAVE ENTRY IN FLWO CHART
 * 
 */
public class TestScopesWithoutEntryInFlowChart {
	
	
	private Database createDataBaseForFlowChart(LOB lob){
		
		Database flowChartDb = null;
		for(Object lobContent:lob.getContent())
		{
			
			if(lobContent instanceof Reference){
				Reference ref = (Reference)lobContent;
				
				
				if (((Reference) ref).getName().equals("FlowChart")) {
					
					Reference reference =  (Reference)ref;
						//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
					String refInXml = null;
					try {
						refInXml = JAXBUtils.writeFromObject(reference);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("FLWOCHARTDATABSE", refInXml);

					
					try{
						flowChartDb = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEFLOWCHARTDB, xmlMap);
						flowChartDb.buildDatabase();
						}catch(BasexException e){
							System.out.println("BasexException"+e.getMessage());
						}finally{
							try {
								flowChartDb.closeDatabase();
							} catch (BasexException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					break;
				}
			}

		}
		return flowChartDb;
	}
	
	
	private void printReport(StringBuffer report){
		System.out.println("------- SCOPES THAT DO NOT HAVE ENTRY IN FLOWCHART --------");
		
		System.out.println(report.toString());
		System.out.println("------ SCOPE THAT DO NOT HAVE ENTRY IN FLOWCHART ENDS -----");
	}
	
	
	private StringBuffer scopesWihtoutFlowChartEntry = new StringBuffer();
	
	/**
	 * @param scope
	 * @param flowChartDB
	 * @param referenceType
	 * @return
	 */
	private void checkFlowChartEntryForScope(Scope scope,Database flowChartDB,String referenceType){
		
		StringBuffer queryString = new StringBuffer();
		//class[@name='BOP']/rating
		queryString.append("//class[@name='").append(scope.getDbTables()).append("']/rating[@type='"+referenceType+"']");
		
		
		Object returnObj = createQueryAndReturnResult(flowChartDB, queryString.toString());
		
		if(returnObj.equals("")){
			scopesWihtoutFlowChartEntry.append("SCOPE:["+scope.getDbTables()+"] REFERENCE-TYPE:["+referenceType+"]");
			scopesWihtoutFlowChartEntry.append("\n");
		}
		
	}
	

		/**
		 * @param database
		 * @param queryString
		 * @return
		 */
		private Object createQueryAndReturnResult(Database database,String queryString){
			Connection connection = new XMLConnection(database);
			Query query = new XmlQuery(connection); 
			query.createQuery(queryString);
			Object returnObj = query.execute();
			return returnObj;
		}
		
		
		/**
		 * @param lob
		 */
		public void readLOB(LOB lob) {
			
			Database flowChartDatabase = createDataBaseForFlowChart(lob);
			//Database flowChartDatabase = createDataBaseForTableObects(lob);
			for (Object ref : lob.getContent()) {
				if (ref instanceof Reference) {
					Reference reference = (Reference)ref;
					//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
					if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
						readReference((Reference) ref,flowChartDatabase);
					}
					
				}
			}
			
		}

		/**
		 * @param reference
		 * @param flowChartDB
		 */
		private void readReference(Reference reference, Database flowChartDB) {
			
			List<Object> scopeList = new ArrayList<Object>();
			scopeList.addAll(reference.getContent());
			for (Object scope : scopeList) {
				if (scope instanceof Scope) {
					Scope curScope = (Scope)scope;
					checkFlowChartEntryForScope(curScope, flowChartDB, reference.getType());
				}
			}
		}
		
		
		/**
		 * @param algoFileReader
		 * @return
		 */
		public LOB getLOBObject(FileReader algoFileReader){
			
			JAXBContext jaxbContext;
			try {
				
				jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
				return lob;
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		
		private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\ALG-CA-NY-10012012-V01_MR_REARRANGED.xml";
		
		@Test
		public void testScopesForFlowChartEntry(){
			try {
				FileReader fileReader = new FileReader(new File(fileName));
				readLOB(getLOBObject(fileReader));
				printReport(scopesWihtoutFlowChartEntry);
			} catch (FileNotFoundException e) {
				System.err.println("FILE NOT FOUND");
				e.printStackTrace();
			}
		}
		
}
