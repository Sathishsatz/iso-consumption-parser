package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public class TestAlgoFileBeforeProcessing {
	
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
		
	
	public void checkForPCHNames(FileReader fileReader){
		
		LOB lob = getLOBObject(fileReader);
		
		List<Object>contents = lob.getContent();
		
		for(Object referece: contents){
			if(referece instanceof Reference ){
				Reference referenceOb = (Reference)referece;
				String refName = referenceOb.getName();
				//if(refName.equals("Common Rating") || refName.equals("Premium Calculation")){
					List<Object> referenceContent = ((Reference) referece).getContent();
					for(Object scope : referenceContent){
						if(scope instanceof Scope){
							
							Scope scop = (Scope)scope;
							List <Object> scopeContent = scop.getContent();
							
							for(Object pchs: scopeContent){
								if(pchs instanceof PCH){
									PCH pch = (PCH)pchs ;
									String pchName = pch.getName();
									if(pchName.contains("../")){
										System.out.println("REFERENCE NAME:["+refName+"] PCH NAME:=["+pchName+"]");
									}
								}
							}
							
						}
					}
				//}
				
			}
			
		}
	}
	
	//private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-BP-PA-07012013-V02\\ALG-BP-PA-07012013-V02_MR_REARRANGED.xml";
	
	//MD ALGORITHM FILE
	private static final String fileName = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-MD-09012012-V01/ALG-BP-MD-09012012-V01_MR.xml";
	
	@Test
	public void testAlgoFileForPCh(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			checkForPCHNames(fileReader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
}
