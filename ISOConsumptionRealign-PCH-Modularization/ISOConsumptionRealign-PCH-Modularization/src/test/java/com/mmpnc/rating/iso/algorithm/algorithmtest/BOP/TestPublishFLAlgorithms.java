package com.mmpnc.rating.iso.algorithm.algorithmtest.BOP;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author nilkanth9581
 * 
 */
public class TestPublishFLAlgorithms {
	
	private static final String ALGORITHM_FILE_LOCATION="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-FL-07012014-V01\\ALG-BP-FL-07012014-V01_MR_AUTO_MODIFIED.xml";
	private static final String CONTENT_FILE_1="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-FL-07012014-V01\\RC-BP-FL-07012014-V01\\RC-BP-CW-07012014-V01.xml";
	private static final String CONTENT_FILE_2="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-FL-07012014-V01\\RC-BP-FL-07012014-V01\\RC-BP-FL-07012014-V01.xml";
	
	private static final String psTestFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-PA-07012013-V02\\test-problems-pa.xml";
	
	private static final String PUBLISH_GLOBAL_LOOKUPS = "N";
	private static final String PUBLISH_ALGORITHM_STEPS = "N";
	
	private static final String LINE_OF_BUSINESS = "BOP";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = false;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		//basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		basicConfiguration.setStateWideAlgorithms(true);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		//basicConfiguration.(true);
		return basicConfiguration;
	}
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	
	private DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	
	
	
	@Test
	public void testISoConForPS() throws Exception{
		Reader algoReader = new FileReader(ALGORITHM_FILE_LOCATION);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForTest(), null);
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		
		Reader contentFileReader1 = new FileReader(new File(CONTENT_FILE_1));
		Reader contentFileReader2 = new FileReader(new File(CONTENT_FILE_2));
		//Reader contentFileReader3 = new FileReader(new File(contetFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		//conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
}
