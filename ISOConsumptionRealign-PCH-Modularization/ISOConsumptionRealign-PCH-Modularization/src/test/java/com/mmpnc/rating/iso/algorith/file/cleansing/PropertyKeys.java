package com.mmpnc.rating.iso.algorith.file.cleansing;

/**
 * @author Nilkanth9581
 *
 */
public enum PropertyKeys {
	state_wide_file_location("state-wide-file-location"),
	country_wide_file_location( "country-wide-file-location"),
	file_to_cleanse("file-to-cleanse");
	
	
	String value;
	
	private PropertyKeys(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
