package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Ref;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Class;

public class TestCheckFlowChartForClassWithoutRatingNode {
	
	private final String txAlgoFile = "";
	
	public LOB getLOBObject(){
		LOB lob = null;
		try{
			Reader algoReader = new FileReader(new File(txAlgoFile));
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			lob  = (LOB) unmarshal.unmarshal(algoReader);	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Marshalling excpetion:");
		}
		return lob;
	}

	private void readFlowChart(LOB lob){
		List<Object> content = lob.getContent();
		
		for(Object object:content){
			if(object instanceof Reference){
				Reference ref = (Reference) object;
				if("FlowChart".equals(ref.getName())){
					readReference((Reference)object);
				}
				
			}
			
		}
	}
	
	private void readReference(Reference reference){
		List<Object> refContent = reference.getContent();
		if(refContent instanceof Class){
			
		}
	}


}
