package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.correction.RatingObjectConverter;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.RatingHolder;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.util.VariableType;
import com.thoughtworks.xstream.XStream;

/**
 * @author Nilkanth9581
 *	THIS CLASS IS WRITTEN TO TEST THE FULLY QUALIFIED PATH FOR THE 
 *  LOOP THROUGH CONDITIONS 
 *  
 */
public class TestLoopThroughConditionsXpath {
	private Map<String,List<String>> processedPCHMap ;
	private Database createDataBaseForFlowChart(LOB lob) throws BasexException{
	
		Database flowChartDb = null;
		for(Object lobContent:lob.getContent())
		{
			
			if(lobContent instanceof Reference){
				Reference ref = (Reference)lobContent;
				
				
				if (((Reference) ref).getName().equals("FlowChart")) {
					
					Reference reference =  (Reference)ref;
						//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
					String refInXml = null;
					try {
						refInXml = JAXBUtils.writeFromObject(reference);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("FLWOCHARTDATABSE", refInXml);

					flowChartDb = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEFLOWCHARTDB, xmlMap);
					flowChartDb.buildDatabase();
					
				}
			}

		}
		return flowChartDb;
	}
	
	/**
	 * @param database
	 * Method is for closing the basex database
	 * 
	 */
	private void closeDatabase(Database database,String dbName)throws BasexException{
		database.closeDatabase();
		//database.dropDatabase(dbName);
	}
	
	/**
	 * @param reference
	 * @return
	 * THIS METHOD WILL CREATE BASEX DATABASE FOR THE CURRENT REFERENCE
	 * 
	 */
	private Database createCurrentScopeDtabase(Scope scope)throws BasexException{
		//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
		Database currentReferenceDatabase = null;
		String refInXml = null;
		try {
			refInXml = JAXBUtils.writeFromObject(scope);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Map<String, String> xmlMap = new HashMap<String, String>();
		xmlMap.put("CURRENTSCOPE", refInXml);
	
		currentReferenceDatabase = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.CURRENTSCOPEDB, xmlMap);
		currentReferenceDatabase.buildDatabase();
		return currentReferenceDatabase;
	}
	
	
	/**
	 * @param lob
	 * @return
	 * THIS METHOD WILL REARRAGE THE ALGORITHM ACCORDING TO THE FLOW CHART VARIABLE NAMES
	 * 
	 */
	public LOB rearrageLOBUsingFlowChart(LOB lob)throws BasexException {
		StringBuffer nonProcessedScopes = new StringBuffer();
		System.out.println("************************* Starting Rearranging LOB using FlowChart ************");
		processedPCHMap = new HashMap<String, List<String>>();
		cosolidatedReportList = new ArrayList<String>(); 
		Database flowChartDatabase = createDataBaseForFlowChart(lob);
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref,flowChartDatabase,nonProcessedScopes);
				}
				
			}
		}
		flowChartDatabase.closeDatabase();
		//PRINTING THE LOG FOR OWN REFERENCE
		printReport(true,nonProcessedScopes);
		//WRTING COMPLETE REPORT
		return lob;
	}

	private void readReference(Reference reference, Database flowChartDB,StringBuffer nonProcessedScopes)throws BasexException {
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				
				Scope curScope = (Scope)scope;
				//ADDING CHECK FOR PCH NAMES WITH ANCESTOR XPATH IN IT
				handleAncestorPCHName(curScope);
				
				//int indexOfScope = scopeList.indexOf(curScope);
				//reference.getContent().remove(curScope);
				//CREATING BASEX DATABSE FOR THE CURRENT SCOPE
				Database currentScopeDB = createCurrentScopeDtabase(curScope);
				readScope(reference,(Scope) scope,flowChartDB,currentScopeDB,nonProcessedScopes);
				//CLOSING DATABASE AS THE CURRENT SCOPE ALGORITHM SPLIT IS COMPLETE
				closeDatabase(currentScopeDB,"CURRENTSCOPE");
				//currentScopeDB.closeDatabase();
			}
		}
	}
	
	
	
	private void printReport(boolean yesNo,StringBuffer nonProcessedScopes){
		Iterator<Map.Entry<String, List<String>>> map = processedPCHMap.entrySet().iterator();
		StringBuffer report = new StringBuffer();
		while(map.hasNext()){
			Map.Entry<String, List<String>> entry = map.next();
			Iterator<String> itr = entry.getValue().iterator();
			report.append("---------------[Algortihm Name:["+entry.getKey()+"]-----------------");
			report.append("\n");
			report.append(" TOTAL NUMBER OF PCH INCLUDED :["+entry.getValue().size()+"]");
			report.append("\n");
			report.append("PCH NAMES [");
			while(itr.hasNext()){
				report.append(itr.next());
				report.append(",");
			}
			report.append("]");
			report.append("\n");
			report.append("\n");
			report.append("---------------------------------------------------------------------");
			report.append("\n");
			report.append("\n");
		}
		System.out.println(report);
		System.out.println("----------------------- ALGORITHM NAMES WIHTOUT RATING IN FLOWCHART ---------------");
		System.out.println("\n");
		System.out.println(nonProcessedScopes);
		System.out.println("\n");
		System.out.println(" ----------------------------------------------------------------------------------");
		
		/*if(cosolidatedReportList != null){
			Iterator<String> consolidateRepItr = cosolidatedReportList.iterator();
			while (consolidateRepItr.hasNext()) {
				System.out.println(consolidateRepItr.next());
			}
		}*/
		System.out.println("************************* Ending Rearranging LOB using FlowChart ************");
	}

	List<String> cosolidatedReportList = null;
	
	
	/**
	 * @param ratingHolder
	 * @param currentScopeDB
	 * @param ratingHolderNew
	 * THIS METHOD WILL REMOVE THE RATING VARIABLE NAMES FROM THE RATINGHOLDER WHICH ARE NOT IN THIS SCOPE
	 * THIS IS ADDED BECAUSE IN FLOWCHART THERE ARE TWO CLASS NODES WITH SAME NAME AND DIFFERENT RATING VARIABLES
	 * 
	 */
	private void checkAndRemoveRatingNodeWithSameName(RatingHolder ratingHolder,Database currentScopeDB,RatingHolder ratingHolderNew ){
		
		if(ratingHolder.getRating()!= null && ratingHolder.getRating().size()>1){
			
			for(Rating ratingFromHolder: ratingHolder.getRating()){
					String PCHToCheck = ratingFromHolder.getVariables().split(",")[0];
					Connection mainScopeCon = new XMLConnection(currentScopeDB);
					Query queryPch = new XmlQuery(mainScopeCon); 
					StringBuffer queryString1 = new StringBuffer();
					//class[@name='BOP']/rating
					queryString1.append("//PCH[@name='").append(PCHToCheck).append("']");
					queryPch.createQuery(queryString1.toString());
					Object returnPCH  = "<scope>"+queryPch.execute()+"</scope>";
					try {
						Scope scoprForPchCheck = JAXBUtils.readFromSource(returnPCH.toString(), Scope.class);
						if(scoprForPchCheck.getContent() != null && scoprForPchCheck.getContent().size()>0){
							ratingHolderNew.getRating().add(ratingFromHolder);
						} else{
							;
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
	}
	
	
	/**
	 * @param database
	 * @param queryString
	 * @return
	 */
	private Object createQueryAndReturnResult(Database database,String queryString){
		Connection connection = new XMLConnection(database);
		Query query = new XmlQuery(connection); 
		query.createQuery(queryString);
		Object returnObj = query.execute();
		return returnObj;
	}
	
	
	/**
	 * @param sequenceNo
	 * @param currentScopeDB
	 * @param pchArryList
	 * THIS METHOD WILL GET ALL THE PCH NAMES HAVING / IN IT AS THESE VARIABLE NAMES WILL NOT BE IN 
	 * THE VARIABLE LIST
	 * 
	 */
	private void addPCHWithNameContainingSlash(int sequenceNo,Database currentScopeDB,List<String>pchArryList){
		if(sequenceNo == 1){
			StringBuffer queryString1 = new StringBuffer();
			//GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			queryString1.append("//PCH[@name[contains(.,'/')]]");
			String returnPCHList = createQueryAndReturnResult(currentScopeDB,queryString1.toString()).toString();
			if(returnPCHList != null && !"".equals(returnPCHList)){
				returnPCHList = "<scope>"+returnPCHList+"</scope>";
				try {
					Object object = JAXBUtils.readFromSource(returnPCHList, Scope.class);
					if(object instanceof Scope){
						Scope scopeNewObj = (Scope)object;
						for(Object pchobject:scopeNewObj.getContent()){
							if(pchobject instanceof PCH){
								//ADDING PCH NAMES TO PCH NAME ARRAY SO THIS WILL BE INCLUDED IN THE FIRST SCOPE WE SPLIT
								pchArryList.add(((PCH) pchobject).getName());
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * @param ratingHolderStr
	 * @param scope
	 * @param flowChartDB
	 * @return
	 * CREATE AND RETURN RATINGHOLDER OBJECT FOR THE RATING NODES IN FLOWCHART FOR THE CURRENT SCOPE DBTABLE 
	 */
	private RatingHolder createAndReturnRatingHolderObject(Scope scope,Database flowChartDB,String referenceType){
		StringBuffer queryString = new StringBuffer();
		//class[@name='BOP']/rating
		queryString.append("//class[@name='").append(scope.getDbTables()).append("']/rating[@type='"+referenceType+"']");
		Object returnObj = "<RatingHolder>"+createQueryAndReturnResult(flowChartDB, queryString.toString())+"</RatingHolder>";
		//CONVERTING STRING INTO OBJECT AGAIN USING XSTREAM FOR THIS
		XStream xstream = new XStream();
		xstream.registerConverter(new RatingObjectConverter());
		xstream.alias("RatingHolder", RatingHolder.class);
		xstream.alias("Rating", Rating.class);
		xstream.addImplicitCollection(RatingHolder.class, "rating",Rating.class);
		
		/*xstream.useAttributeFor("variable", String.class);
		xstream.useAttributeFor("rating", String.class);*/
		
		RatingHolder ratingHolder  = (RatingHolder)xstream.fromXML(returnObj.toString());
		return ratingHolder;
		
	}
	/**
	 * @param scope
	 * @param flowChartDB
	 * @param currentScopeDB
	 * @return
	 * THIS METHOD WILL SPLIT SCOPE INTO MULTIPLE SCOPES IF ALGROTIHM EXECUTES LIST OF PCH AT DIFFERENT LEVELS
	 */
	private List<Scope> splitScopeUsingFlowChart(Scope scope,Database flowChartDB,Database currentScopeDB,StringBuffer nonProcessedScopes,String referenceType){
		
		/*xstream.useAttributeFor("variable", String.class);
		xstream.useAttributeFor("rating", String.class);*/
		
		RatingHolder ratingHolder  = createAndReturnRatingHolderObject(scope, flowChartDB,referenceType);
		
		//CREATING NEW RATINGHOLDER AND LOOPING THROUGH IT TO AVOID CONCURRANCY ERROR
		RatingHolder ratingHolderNew = new RatingHolder();
		List<Rating> ratingList = new ArrayList <Rating>();
		ratingHolderNew.setRating(ratingList);
		
		//CHECKING IF THERE ARE TWO NODES WITH SAME NAME
		checkAndRemoveRatingNodeWithSameName(ratingHolder, currentScopeDB, ratingHolderNew);
		
		List<Scope> newScopeList = new ArrayList<Scope>();
		int sequenceNo = 0;
		/*RatingHolder ratingHolderNew1 = new RatingHolder();	
		ratingHolderNew1.setRating(ratingHolderNew.getRating());
		*/
		if(ratingHolderNew.getRating()!= null && ratingHolderNew.getRating().size() != 0){
			
			for(Rating ratingObj:ratingHolderNew.getRating()){
				if(ratingObj instanceof Rating){
					//CREATING SEPERATE SCOPE FOR EVERY RATING NODE UNDER THE CLASS 
					Scope newScope = new Scope();
					newScope.setDbTables(scope.getDbTables());
					newScope.setSequenceNo(++sequenceNo);
					newScope.setPass(scope.getPass());
					newScope.setRecordType(scope.getRecordType());
					
					Rating rating = (Rating)ratingObj;
					String variable = rating.getVariables();
					String pchArr [] = null;
					if(variable.contains(",")){
						pchArr = variable.split(",");
					}else{
						pchArr = new String[]{variable};
					}
					List<String> pchNameListOfRatingNodes = Arrays.asList(pchArr);
					List<String> pchArryList = new ArrayList<String>();
					pchArryList.addAll(pchNameListOfRatingNodes);
					//ADDING PCH TO ARRAY WHICH CONTAINS / IN IT WE NEED TO ADD THIS PCH ONLY IN ONE SCOPE
					//NO NEED TO ADD THIS IN MULTIPLE SCOPES SO CHECKING THE SQUENCE AND IF IT IS O THEN ADDING THESE PCHS
					addPCHWithNameContainingSlash(sequenceNo, currentScopeDB, pchArryList);
					//CREATING NEW PCHNAMELIST AND ADDING TO THE PROCESSED PCHMAP 
					//THIS HAS BEEN ADDED BSE IF SAME PCH REFERENCED BY MULTIPLE SPLITTED SCOPE
					List<String> pchNameListForScope = new ArrayList<String>();
					processedPCHMap.put(scope.getDbTables()+sequenceNo+scope.getPass(),pchNameListForScope);
					
					//GETTING ALL THE PCH DECLARED IN THE VARAIBLES AND ADDING THOSE IN NEWLY CREATE SCOPE OBJECT
					
					getPCHFromScopeAndAddTonewScope(scope, pchArryList, currentScopeDB, sequenceNo, newScope);
					
					/*try {
						String newScopeStr = JAXBUtils.writeFromObject(newScope);
						System.out.println(newScopeStr);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
					
					//ADDING NEWLY CREATED SCOPE TO THE SCOPE LIST
					newScopeList.add(newScope);
					
					
				}
			}
			System.out.println("SPLIT FOR ALGORITHM =["+scope.getDbTables()+"] NUMBER OF NEW ALGORITHMS CREATED =["+sequenceNo+"]");
		}
		else{
			//ENLISINTG SCOPE NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART
			addOriginalScopeToNewScopeList(nonProcessedScopes, scope, newScopeList, sequenceNo);
		}
		
		return newScopeList;
	}
	
	/**
	 * @param ScopeContentList
	 * THIS WILL SORT THE PCH LIST ACCORDING TO THE EXCEL ROW NUMBER INT THE PCH
	 */
	private void sortPCHListAccordingToExcelRow(Scope scope){
		
		Comparator<Object> comparator = new Comparator<Object>() {

			@Override
			public int compare(Object o1, Object o2) {
				if(o1 instanceof PCH && o2 instanceof PCH){
					PCH pch1 = (PCH) o1;
					PCH pch2 = (PCH) o2;
					if(pch1.getExcelRow() > pch2.getExcelRow())
						return 1;
				}
				return 0;
			}
		};
		
		Collections.sort(scope.getContent(), comparator);
	
	}
	
	/**
	 * @param scope
	 * @param pchArryList
	 * @param currentScopeDB
	 * @param sequenceNo
	 * @param newScope
	 * THIS MEHTO WILL LOOP THROUGH THE PCH NAMES DECLARED IN RATING NODE AND GET THOSE PCH FROM THE MAIN SCOPE
	 * AND ADD IT TO THE NEWLY CREATED SCOPE
	 */
	private void getPCHFromScopeAndAddTonewScope(Scope scope,List<String>pchArryList,Database currentScopeDB,int sequenceNo,Scope newScope){
		for(String pchName : pchArryList){
			StringBuffer queryString1 = new StringBuffer();
			//HANDLING ANCESTOR IN XPATH STRING
			//pchName = handleAncestorInXpath(pchName);
			///GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			queryString1.append("//(scope[@dbTables='").append(scope.getDbTables()).append("']/PCH[@name='"+pchName+"'])[1]");
			Object returnPCH = createQueryAndReturnResult(currentScopeDB, queryString1.toString());
			try {
				if(!"".equals(returnPCH) ){
					Object pchObj  = JAXBUtils.readFromSource(returnPCH.toString().trim(),PCH.class);
					PCH pch = (PCH) pchObj;
					//String pchNameWithoutAncestor = handleAncestorInXpath(pch.getName());
					processedPCHMap.get(scope.getDbTables()+sequenceNo+scope.getPass()).add(pch.getName().trim());
					//pch.setExecutedAtScopeSplitLevel(true);
					newScope.getContent().add(pch);
				}
			} catch (Exception e) {
				System.out.println(returnPCH.toString());
				e.printStackTrace();
			}
		
		}
	}
	
	/**
	 * 
	 */
	private void addOriginalScopeToNewScopeList(StringBuffer nonProcessedScopes,Scope scope,List<Scope> newScopeList,int sequenceNo){
		//ENLISINTG SCOPE NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART
		nonProcessedScopes.append("ALGORITHM NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART =[");
		nonProcessedScopes.append(scope.getDbTables());
		nonProcessedScopes.append("]");
		nonProcessedScopes.append("\n");
		//ADDIGN SEQUENCE NUMBER TO THE NON PROCESSED SCOPE
		scope.setSequenceNo(++sequenceNo);
		
		newScopeList.add(scope);
	}
	
	private void readScope(Reference reference ,Scope scope,Database flowChartDB,Database currentScopeDB,StringBuffer nonProcessedScopes) {
		
		
		//Scope originalScope = scope;
		
		List<Scope>newScopeList = splitScopeUsingFlowChart(scope,flowChartDB,currentScopeDB,nonProcessedScopes,reference.getType());
		
		//CHECKING WHETHER SCOPE IS SPLITT INTO MULTIPLE ALGORITHMS
		if(newScopeList.size()>1){
			//AS WE NEED TO SPLIT THE SCOPE REMOVING SCOPE FROM THE REFERENCE
			reference.getContent().remove(scope);
			//int originanNumberOfPCH = 0;
			//int numberOFPCHAfterProcess = 0;
				for(Scope newScope:newScopeList){
					List<Object> pchList = new ArrayList<Object>();
					pchList.addAll(newScope.getContent());
					
					
					//originanNumberOfPCH = pchList.size();
					for (Object obj : pchList) {
						if (obj instanceof PCH) {
							readPCH((PCH) obj,currentScopeDB,newScope);
						} else if (obj instanceof If) {
							System.out.println("NOT EXPECTED THIS SITUATION");
							readIf((If) obj,currentScopeDB,newScope);
						} else if (obj instanceof Assign) {
							System.out.println("NOT EXPECTED THIS SITUATION");
							readAssign((Assign) obj,currentScopeDB,newScope);
						} else if (obj instanceof Loop) {
							System.out.println("NOT EXPECTED THIS SITUATION");
							readLoop((Loop) obj,currentScopeDB,newScope);
						} else if (obj instanceof Class) {
						}
					}
				}
				
		    }else{
		    	//REMOVING SCOPE FROM THE REFERENCE AS WE ARE ADDING WHILE ITERATING THROUGH NEWSCOPELIST
		    	reference.getContent().remove(scope); 
		    }
		List<String> processdPchInTheScope = new ArrayList<String>();
		//ADDING NEWLY CREATED SCOPES IN CURRENT REFERENCE
		for(Scope newScope:newScopeList){
			//SORTING THE PCH LIST IN SCOPE ACCORDING TO THE EXCELL ROW AS THE SORTING FUNCTIONALITY DEPENDS ON THIS
			sortPCHListAccordingToExcelRow(newScope);
			reference.getContent().add(newScope);
			List<String> procssedPCHList = processedPCHMap.get(newScope.getDbTables()+newScope.getSequenceNo()+newScope.getPass()); 
			if(procssedPCHList != null)
			processdPchInTheScope.addAll(procssedPCHList);
		}
		
		//THIS METHOD IS ADDED TO CHECK IF THERE ARE ANY PCH WIHTOUT ANY REFERENCE OR ENTRY IN VARAIBLE TYPE
		checkForExcluedePCH(currentScopeDB, scope, processdPchInTheScope, newScopeList.get(0));
		//AFTER EXECUTING ALL THE SCOPES WE NEED TO CHECK FOR ANY REMAINING PCH WHOSE NAME IS NOT
		//INCLUDE IN THE VARIABLE TYPE AND IT IS NOT A INTERMEDIARY PCH USED
			
		
	}

	/**
	 * @param currentScopeDB
	 * @param mainScope
	 */
	private void checkForExcluedePCH(Database currentScopeDB,Scope mainScope,List<String> processedPchList,Scope newScope){
		if(processedPchList.size() == 0)
			return;
		Connection currentScopeCon = new XMLConnection(currentScopeDB);
		Query query = new XmlQuery(currentScopeCon); 
		StringBuffer queryString = new StringBuffer();
		//class[@name='BOP']/rating
		queryString.append("//PCH/@name");
		Set<String> pchSet = new HashSet<String>();
		query.createQuery(queryString.toString());
		Object returnObj = query.execute();
		if(returnObj != null){
			String pchName = returnObj.toString();
			String [] arr = pchName.split("name=");
			for(String pchNam : arr){
				if(!"".equals(pchNam) && pchNam.contains("\""))
				{
					pchNam = pchNam.replace("\"", "").trim();
					 pchSet.add(pchNam);
				}
				 
			}
		}
		for(String pchLeft : pchSet){
			if(!processedPchList.contains(pchLeft)){
				System.out.println("NO REFERENCE FOR PCH WITH NAME=["+pchLeft+"] IN ALGORITHM=[ "+newScope.getDbTables()+"]");
				queryString = new StringBuffer();
				queryString.append("//PCH[@name='").append(pchLeft).append("'][1]");
				query.createQuery(queryString.toString());
				Object returnObject = query.execute();
				if(returnObject != null && !"".equals(returnObject)){
					try {
						PCH pch =(PCH)JAXBUtils.readFromSource(returnObject.toString(), PCH.class);
						//pchLeft = handleAncestorInXpath(pchLeft);
						processedPCHMap.get(newScope.getDbTables()+newScope.getSequenceNo()+newScope.getPass()).add(pchLeft.trim());
						newScope.getContent().add(pch);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}
		//SORTING NEWLY CREATED SCOPE ACCORDING TO THE EXCEL ROW NUMBER
		sortPCHListAccordingToExcelRow(newScope);
	}
	
	private void readPCH(PCH pch,Database currentScopeDB,Scope currentScope) {

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope);
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope);
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	}

	/*private void readPCHContent(Object obj,Database currentScopeDB,Scope currentScope ) {
//		System.out.println(obj.getClass().getSimpleName());
		if (obj instanceof If) {
//			System.out.println("***** If");
			readIf((If) obj,currentScopeDB,currentScope );
		} else if (obj instanceof Assign) {
//			System.out.println("**** Assign");
			readAssign((Assign) obj,currentScopeDB,currentScope );
		} else if (obj instanceof Loop) {
//			System.out.println("**** Loop");
			readLoop((Loop) obj,currentScopeDB,currentScope );
		} else if (obj instanceof Class) {
//			System.out.println("**** Class");
		}else if(obj instanceof String){
			;
		}else{
			System.out.println("We will need to handle this case -> " + obj.getClass());
		}
	}*/

	private void readIf(If _if,Database currentScopeDB,Scope currentScope ) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Then) {
				readThen((Then) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Else) {
				readElse((Else) obj,currentScopeDB,currentScope );
			}
		}
	}

	private void readCondition(Condition condition,Database currentScopeDB,Scope currentScope ) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,currentScopeDB,currentScope );
			}
		}
	}

	private void readThen(Then then,Database currentScopeDB,Scope currentScope ) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope );
			} else if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	
	//private int loopIndex = 0;
	
	private void readLoop(Loop loop,Database currentScopeDB,Scope currentScope ) {
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope );
			} else if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void readElse(Else _else,Database currentScopeDB,Scope currentScope ) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope );
			} else if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}


	private void readAssign(Assign assign,Database currentScopeDB,Scope currentScope ) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,currentScopeDB,currentScope );
			}
		}
	}

	private void readExpression(Expression expression,Database currentScopeDB,Scope currentScope ) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
//			sb.append(" expression -> " + expression.getOp());
			readBothSides(expression,currentScopeDB,currentScope );
		} else {
			readContent(expression,currentScopeDB,currentScope );
		}
	}

	private void readBothSides(Expression expression,Database currentScopeDB,Scope currentScope ) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex,currentScopeDB,currentScope );
			} else if (obj instanceof Function) {
				readFunction((Function) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj,currentScopeDB,currentScope );
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	private void readContent(Expression expression,Database currentScopeDB,Scope currentScope ) {
		for (Object obj : expression.getContent()) {
			
			if (obj instanceof String) {
				checkVariableName(obj.toString(), currentScopeDB, expression.getVariableType(),currentScope,false );
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Function) {
				readFunction((Function) obj,currentScopeDB,currentScope );
			}
		}
	}

	/**
	 * @param variableName
	 * @param currentScopeDB
	 * @param variableType
	 * 
	 * THIS METHOD WILL CHECK THE VARAIBLE IS POINTING TO PCH IN SCOPE
	 *  
	 */
	private void checkVariableName(String variableName,Database currentScopeDB,String variableType,Scope currentScope,boolean isFromRatetable ){
		if(variableType != null)
		if ( variableType.equals(VariableType.LV_VALUE)|| variableType.equals(VariableType.LV_INTEGER) || variableType.equals(VariableType.LV_NUMERIC) || variableType.equals(VariableType.LV_DOUBLE) || variableType.equals(VariableType.LV_STRING) || variableType.equals(VariableType.LV_BOOLEAN) || variableType.equals(VariableType.LV_DATE) || variableType.equals(VariableType.LV_TIMESPAN) || isFromRatetable){
			//CHECK WHTHER THIS VARIABLE POINTS TO PCH IF SO ADD THIS PCH TO THE CURRENT NEW SCOPE
			Connection curScopeCon = new XMLConnection(currentScopeDB);
			Query query = new XmlQuery(curScopeCon); 
			StringBuffer queryString = new StringBuffer();
			//class[@name='BOP']/rating
			queryString.append("//(PCH[@name='").append(variableName.trim()).append("'])[1]");
			query.createQuery(queryString.toString());
			Object returnPCH = query.execute();
			if(returnPCH != null && !returnPCH.equals("")){
				try {
					Object pchObj  = JAXBUtils.readFromSource(returnPCH.toString(), PCH.class);
					PCH pch = (PCH)pchObj;
					String preceedPCHMapKey = currentScope.getDbTables()+currentScope.getSequenceNo()+currentScope.getPass(); 
					if(!processedPCHMap.get(preceedPCHMapKey).contains(pch.getName().trim())){
						currentScope.getContent().add(pch);
						String pchName = pch.getName().trim();
						//pchName = handleAncestorInXpath(pchName);
						processedPCHMap.get(preceedPCHMapKey).add(pchName);
						//READING NEWLY ADDED PCH
						readPCH(pch, currentScopeDB, currentScope);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		
		}
	}
	
	private void readFunction(Function function,Database currentScopeDB,Scope currentScope ) {
		// System.out.print(function.getType());
		// System.out.print("( ");
		// sb.append(":");
		for (Object obj : function.getContent()) {
			if(obj instanceof Arg){
				Arg arg = (Arg) obj;
				String variableType = arg.getVariableType();
				if(variableType == null)
					variableType = "dummy";
				checkVariableName(arg.getContent(), currentScopeDB,variableType,currentScope,true );
			}
		}
		
	}

	private void readRatetable(Ratetable ratetable,Database currentScopeDB,Scope currentScope) {

		for (Object obj : ratetable.getContent()) {

			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				String content = arg.getContent().trim();
				String variableType = arg.getVariableType();
				if(variableType == null)
					variableType = "dummy";
				checkVariableName(content, currentScopeDB, variableType, currentScope,true );
			}

		}
	}


	
	private void testMehtod(){
		String completeAlgoFile = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-TX-05012013-V01/ALG-BP-TX-05012013-V01_MR_REARRANGED.xml";
		String testXmlFile = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-TX-05012013-V01/TestTXAlgoFile.xml";
		try{
			Reader algoReader = new FileReader(new File(testXmlFile));
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshal.unmarshal(algoReader);	
			rearrageLOBUsingFlowChart(lob);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Excpetion in the test mehtod");
		}
	}
	
	/**
	 * @param xpathStringWithAncestor
	 * @return
	 * THIS METHOD IS TO HANDLE ANCESTOR IN THE XPATH STRING
	 * 
	 */
	private void handleAncestorPCHName(Scope scope){
		
		List<Object> contentList = scope.getContent();
		
		for(Object scopeContent : contentList){
			if(scopeContent instanceof PCH){
				PCH pch = (PCH) scopeContent;
				String pchName = pch.getName();
				//THIS METHOD WILL HANDLE AND CHANGE THE PCH NAME IF REQURIED
				pchName = removeAncestorFromXpath(pchName);
				pch.setName(pchName);
			}
		}
		
	}
	
	
	/**
	 * @param xPathStr
	 * @return
	 */
	private String removeAncestorFromXpath(String xPathStr){
		String fullXpath = xPathStr;
		if(xPathStr.contains("ancestor::*[name(.)")){
			String ancestorString = null;
			StringBuffer xpathWithoutAncestor = new StringBuffer();
			if(xPathStr.contains("/")){
				String arrs[] = xPathStr.split("/");
				for(String xpathStr: arrs){
					if(xpathStr.trim().startsWith("ancestor::*[name(.)")){
						ancestorString  = xpathStr;
						String arncestorName = xPathStr.split("'")[1];
						xpathWithoutAncestor.append(arncestorName);
						xpathWithoutAncestor.append("/");
					}
					else
					{
						xpathWithoutAncestor.append(xpathStr);
						xpathWithoutAncestor.append("/");
					}
				}
				fullXpath =  xpathWithoutAncestor.toString().substring(0,xpathWithoutAncestor.length()-1);
				
			}
			else{
				String arncestorName = xPathStr.split("'")[1];
				fullXpath =  xPathStr.replace(xPathStr, arncestorName);
			}
		}
		else if(xPathStr.contains("[not(@offset=")){
			fullXpath = xPathStr.replace("[not(@offset='Deleted')]", "");
		}
		return fullXpath;
	}
	
	
	/**
	 * 
	 */
	private void testCA(){
		
		/*String str = "Something/[not(offest='Deleted')]/LCMLiability[.='Contractor'] ";
		
		System.out.println(handleAncestorInXpath(str));
		String[] arr = str.split("Deleted");
			
				String ancestorName = arr[0];
				String remainingxpath = arr[1];
				
				String []rr =  ancestorName.split("'");
				String ancestorNameEx = rr[1];
				System.out.println(ancestorNameEx);
				System.out.println(ancestorName);
				System.out.println(remainingxpath);*/
		
		String caAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-FL-12012012-V01\\ALG-CA-FL-12012012-V01_MR.xml";
		String caAlgoTestFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-FL-12012012-V01\\test-ca.xml";
		try{
			Reader algoReader = new FileReader(new File(caAlgoFile));
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshal.unmarshal(algoReader);	
			rearrageLOBUsingFlowChart(lob);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Excpetion in the test mehtod");
		}
	}
	
	/*private void testCAAlgorithm(){
		new CorrectionAccordingToExecutionFlow().testCA();
		
	}
	
	public static void main(String []args) {
		
		String str = "../CommercialAutoVehicleUnderinsuredMotoristPolicyCoverage[not(@offset='Deleted')]/Limit";
		
		if(str.contains("[not(@offset=")){
			str = str.replace("[not(@offset='Deleted')]","");
		}
		System.out.println(str);
		new CorrectionAccordingToExecutionFlow().testMehtod();
	}*/
}
