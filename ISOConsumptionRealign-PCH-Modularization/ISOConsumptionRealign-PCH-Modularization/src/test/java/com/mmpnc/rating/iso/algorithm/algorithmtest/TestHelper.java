package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.io.Reader;
import java.util.List;
import java.util.Map;

import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ExecuteModuleStep;
import com.mmpnc.icm.common.ide.models.ForStep;
import com.mmpnc.icm.common.ide.models.IFStatementStep;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.icmxml.creater.AlgorithmStepLookupPublisher;
import com.mmpnc.rating.iso.wrapper.ICMRequiredData;
import com.mmpnc.rating.iso.wrapper.ISOConsuptionWrapper;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 * 
 */
public class TestHelper extends ISOConsuptionWrapper {

	public TestHelper(Reader algoReader) {
		super(algoReader);
		// TODO Auto-generated constructor stub
	}

	public void testISOConsuptionWrapperWithReaders() throws Exception {

	}

	public void texasAlgoTesting(List<Reader> contentFileReaderList,
			String ratingCategory, String lineOfBusiness) throws Exception {
		readAlgoFileAndCreateICMInputXml(contentFileReaderList, ratingCategory,
				lineOfBusiness, BasicConfiguration.getInstance()
						.isStateWideAlgorithms());
	}

	StringBuffer algoName = new StringBuffer();

	/*
	 * Client Program for this consumption process should override this method
	 */
	public void cosumptionPostProcess(ICMRequiredData icmRequiredData) {

		System.out.println("Say Hi");
		System.out.println("");
		// System.out.println(LookupNameDecider.getInstance().getLookupNamesWithoutKeys().size());
		List<IcmRatingNodeContent> nodeContent = icmRequiredData
				.getIcmRatingContent().get("Premium Calculation");

		StringBuffer buffer = new StringBuffer();
		printAlogLevelExecutSteps(nodeContent, buffer);
		System.out.println(buffer);

		nodeContent = icmRequiredData.getIcmRatingContent()
				.get("Common Rating");
		buffer = new StringBuffer();
		if(nodeContent != null)
		printAlogLevelExecutSteps(nodeContent, buffer);
		
		System.out.println(buffer);
		LookupModels lookupModels = icmRequiredData.getLookupModels();
		System.out.println("Lookup Names in the algorithm file...");
		for (LookupModel lookupModel : lookupModels.getLookupModels()) {
			System.out.println(lookupModel.getLookupName());
		}
		Map<String, String> map = AlgorithmStepLookupPublisher.getInstance()
				.getAlgorithmNameAndID();
		
		for (String key : map.keySet()) {
			algoName.append(map.get(key));
			algoName.append(":");
			algoName.append(key);
			algoName.append("\n");
		}

		System.out.println(algoName.toString());
		printAlogLevelExecutSteps(nodeContent,new StringBuffer());

	}

	private void printAlogLevelExecutSteps(List<IcmRatingNodeContent> nodeContent,StringBuffer buffer) {
		
		StringBuffer algoNameAndPCHAvailable = new StringBuffer();
		if(nodeContent != null)
		for (IcmRatingNodeContent ratingNode : nodeContent) {

			StringBuffer algo = new StringBuffer();

			StringBuffer idents = new StringBuffer("----");

			algo.append(idents + "Algorithm Level:" + ratingNode.getDbTables()
					+ "_" + ratingNode.getSequenceNo());
			algo.append("\n");
			algo.append(idents.append("--")
					+ "Algorithm Level Execute Module Steps:");
			algo.append("\n");

			int counter = 0;
			for (AlgoStep algoStep : ratingNode.getAlgoStepList()) {
				if (algoStep instanceof ExecuteModuleStep) {
					StringBuffer executeModuleSteps = new StringBuffer();
					ExecuteModuleStep executeModuleStep = (ExecuteModuleStep) algoStep;
					// System.out.println(executeModuleStep.getStepType()+":"+executeModuleStep.getModuleName());
					executeModuleSteps.append(idents + "-- ExecuteModule:["
							+ (++counter) + ":"
							+ executeModuleStep.getModuleName() + "]");
					algo.append(executeModuleSteps);
					algo.append("\n");
				}
			}
			algoNameAndPCHAvailable.append("\n");
			algoNameAndPCHAvailable.append("---- Scope Name --["+ratingNode.getDbTables()+"_"+ratingNode.getSequenceNo()+"] --------");
			
			for(String key:ratingNode.getPchNameAndAlgoSteps().keySet()){
				
				algoNameAndPCHAvailable.append("\t PCH NAME:[").append(key).append("]").append("\n");
			}
			
			/*
			 * algo.append("------ PCH Level Execute Module Steps:");
			 * algo.append("\n");
			 * 
			 * StringBuffer pchIdents = new StringBuffer("------"); Map<String,
			 * List<AlgoStep>> pchAndStepsMap =
			 * ratingNode.getPchNameAndAlgoSteps(); for(String
			 * pchName:pchAndStepsMap.keySet()){
			 * algo.append(pchIdents+"------ PCH:["+pchName+"]"); List<AlgoStep>
			 * pchSteps = pchAndStepsMap.get(pchName); for(AlgoStep step:
			 * pchSteps){ if(step instanceof ExecuteModuleStep){
			 * ExecuteModuleStep execMod = (ExecuteModuleStep) step;
			 * algo.append(
			 * "-------------- ExecuteModule:["+execMod.getModuleName()+"]");
			 * algo.append("\n"); }else if(step instanceof IFStatementStep){
			 * StringBuffer sb = new
			 * StringBuffer("--------------").append("\n");
			 * algo.append(sb+"IF:");
			 * addIfStatementExecuteModuleStep((IFStatementStep
			 * )step,sb,pchName); algo.append(sb); algo.append("\n"); }else
			 * if(step instanceof ForStep){
			 * addForStatementExecuteModuleStep((ForStep)step,algo,pchName); } }
			 * }
			 */
			buffer.append(algo);
		}
		
		System.out.println(algoNameAndPCHAvailable);
	}

	public void addIfStatementExecuteModuleStep(
			IFStatementStep ifStatementStep, StringBuffer pchExec,
			String pchName) {
		StringBuffer ifStringBuffer = new StringBuffer();
		for (AlgoStep algoStep : ifStatementStep.getAlgoStepList()) {
			if (algoStep instanceof ExecuteModuleStep) {
				ExecuteModuleStep executeModuleStep = (ExecuteModuleStep) algoStep;
				ifStringBuffer.append(executeModuleStep.getParentCondition())
						.append(":").append(executeModuleStep.getModuleName());
				ifStringBuffer.append("\n");
				pchExec.append(ifStringBuffer);
			} else if (algoStep instanceof IFStatementStep) {
				addIfStatementExecuteModuleStep((IFStatementStep) algoStep,
						pchExec, pchName);
			} else if (algoStep instanceof ForStep) {
				addForStatementExecuteModuleStep((ForStep) algoStep, pchExec,
						pchName);
			}

		}
	}

	public void addForStatementExecuteModuleStep(ForStep forStep,
			StringBuffer pchExec, String pchName) {
		StringBuffer forStepBuffer = new StringBuffer();
		for (AlgoStep algoStep : forStep.getAlgoStepList()) {
			if (algoStep instanceof ExecuteModuleStep) {
				ExecuteModuleStep executeModuleStep = (ExecuteModuleStep) algoStep;
				forStepBuffer.append(executeModuleStep.getParentCondition())
						.append(":").append(executeModuleStep.getModuleName())
						.append("\n");

				pchExec.append(forStepBuffer);
			} else if (algoStep instanceof IFStatementStep) {
				IFStatementStep ifStatementStep = (IFStatementStep) algoStep;
				addIfStatementExecuteModuleStep(ifStatementStep, pchExec,
						pchName);
			} else if (algoStep instanceof ForStep) {
				ForStep locForStep = (ForStep) algoStep;
				addForStatementExecuteModuleStep(locForStep, pchExec, pchName);
			}

		}
	}

}
