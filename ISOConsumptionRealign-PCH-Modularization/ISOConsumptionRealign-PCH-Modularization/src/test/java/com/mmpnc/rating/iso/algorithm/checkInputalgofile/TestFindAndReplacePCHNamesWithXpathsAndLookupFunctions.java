package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.rating.iso.datastructure.AlgorithmDataStructure;

/**
 * @author nilkanth9581
 *	THIS Test case is written to deal with Xpath used in WC algorithm files
 *	WC algorithm files uses xpaths like 
 *  /AscendantOne/EffDate
 * 
 */
public class TestFindAndReplacePCHNamesWithXpathsAndLookupFunctions {

	
	
	
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public void readLOB(LOB lob) {
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref );
				}
				
			}
		}
	}
	
	
	public void writeLOB(LOB lob) throws IOException,Exception{
		File f = new File(fileNameNEw);
		FileOutputStream fileOutputStream = new FileOutputStream(f);
		fileOutputStream.write(JAXBUtils.writeFromObject(lob).getBytes());	
	}
	
	

	private void readReference(Reference reference ) {
		
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope)scope;
				readScope(curScope );
				//currentScopeDB.closeDatabase();
			}
		}
	}
	
	
	
	
	private void readScope(Scope scope ) {
		//setting the current scope path 
		
		
		
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj );
			} else if (obj instanceof If) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readIf((If) obj );
			} else if (obj instanceof Assign) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readLoop((Loop) obj );
			} else if (obj instanceof Class) {
			}
		}
	}
	
	
	private String removeOpeningAndClosingBraces(String inputString){
		inputString = inputString.substring(0,inputString.indexOf("{")).trim();
		return inputString;
	}
	
	private String removeXpathFromPCHName(String pchName){
		return pchName.substring(pchName.lastIndexOf("/")+1);
	}
	
	private void readPCH(PCH pch ) {

		String name = pch.getName().trim();
		
		if(name.startsWith("../")){
			name = removeXpathFromPCHName(name);
			pch.setName(name);
		}
		else if(name.contains("ancestor")){
			name = removeXpathFromPCHName(name);
			pch.setName(name);
		}else if(name.contains("{") && name.startsWith("Lookup")){
			name = removeOpeningAndClosingBraces(name);
			pch.setName(name);
		}
		
		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj );
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	}
	
	
	private void readIf(If _if  ) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj );
			} else if (obj instanceof Then) {
				readThen((Then) obj );
			} else if (obj instanceof Else) {
				readElse((Else) obj );
			}
		}
	}

	private void readCondition(Condition condition ) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj );
			}
		}
	}

	private void readThen(Then then  ) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj  );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	
	//private int loopIndex = 0;
	
	
	private void readLoop(Loop loop  ) {
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void readElse(Else _else ) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}


	private void readAssign(Assign assign ) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());
		if(assign.getLValue().contains("{")){
			String lValue = removeOpeningAndClosingBraces(assign.getLValue());
			assign.setLValue(lValue);
		}
		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj );
			}
		}
	}

	private void readExpression(Expression expression  ) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
//			sb.append(" expression -> " + expression.getOp());
			readBothSides(expression );
		} else {
			readContent(expression);
		}
	}

	private void readBothSides(Expression expression  ) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex );
			} 
			else if (obj instanceof Bracket){
				Bracket bracket = (Bracket)obj;
				readBracket(bracket);
			}
			else if (obj instanceof Function) {
				//readFunction((Function) obj );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	
	
	private void readContent(Expression expression ) {
		
		Expression expressionLoc = new Expression();
		expressionLoc.getContent().addAll(expression.getContent());
		for (Object obj : expressionLoc.getContent()) {
			
			if (obj instanceof String ) {
				String expressionContent = (String) obj;
				if(expressionContent.trim().startsWith("Lookup")){
					//THIS CODE IS TO REPLACE THE ALGORITHM CONTENT
					String expressionCont = expressionContent.replaceAll("\\{.*?\\}","").trim();
					List<String> contentList = new ArrayList<String>();
					contentList.add(expressionCont);
					expression.getContent().clear();
					expression.getContent().addAll(contentList);
				}
				
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
			} else if (obj instanceof Function) {
				//((Function) obj );
			}
		}
	}

	
	private List<String> listOfIncorrectXpaths = new ArrayList<String>();
	
	private Database dataStructureDB;
	
	private void buildDatabase(LOB lob){
			for (Object ref : lob.getContent()) {
				if (ref instanceof Reference) {
					if (((Reference) ref).getName().equals("Table Objects")) {
						AlgorithmDataStructure ds = new AlgorithmDataStructure(
								(Reference) ref);
						StringBuffer xmlDS = ds.simplifyDataStrucure();
						
						Map<String, String> xmlMap = new HashMap<String, String>();
						xmlMap.put("DSFile", xmlDS.toString());
						
						
						
						try{
							this.dataStructureDB = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEDSDB, xmlMap);
							this.dataStructureDB.buildDatabase();
							}catch(BasexException e){
								System.out.println("BasexException"+e.getMessage());
							}finally{
								try {
									dataStructureDB.closeDatabase();
								} catch (BasexException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						
					}
					
				}
		}
		
	}
	
	
	
	private void readBracket(Bracket bracket){
		for(Object obj : bracket.getContent()){
			if(obj instanceof Expression){
				readExpression((Expression)obj);
			}
		}
	}
	
	public static void main(String args[]) throws FileNotFoundException{
		/*
		String str ="LookupNamedPerilsBusnPrsnlPropFactor {\"No\"}";
		str = str.substring(0,str.indexOf("{")).trim();
		System.out.println(str);*/
		
		String str1 = "../A/B";
		str1 = str1.substring(str1.lastIndexOf("/")+1);
		System.out.println(str1);
		
		
		
	}
	
	@Test
	public void testFindAnnDateAndReplaceItWithXpath(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			buildDatabase(lob);
			readLOB(lob);
			//writeLOB(lob);
		}catch(Exception e){
			System.out.println("Problem in reading algorithm file");
			e.printStackTrace();
		}
		
		}
	
//	private static String currentScopeQualPath = "Policy/WorkersComp/WorkersCompOccasionalPrivateResidenceExposureCoverage";
//	private static final String commonStringToReplace = "/AscendantOne/*/";
//	private static final String replacedBy = "AnnDate";
//	private static final String levelOfReplacedBy = "Policy";
//	private StringBuffer report = new StringBuffer();
	private String fileNameNEw =  "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\ALG-GL-NY-10012014-V01_MR_NEW.xml";
	private static final String fileName =  "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\CA_7thJAN\\ALG-CA-CW-02012014-V01_MR_MODIFIED_2.xml";
//	private String currentScopeDbTable = null;
//	private String currentReferenceType = null;
}
