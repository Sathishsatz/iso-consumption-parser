package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.util.AlgoUtil;
import com.mmpnc.rating.iso.algorithm.util.UnInitializedVariableListHolder;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 * THIS TEST CASE WILL CHECK THE VARIABLE NAMES WHICH ARE NOT INITIALIZED IN THE
 * ALGORITHM FILE
 */
public class TestUninitializedVariables {
	
	
	
	
	
	public void readLOB(LOB lob) {
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref);
				}
				
			}
		}
	}
	
	
private void readReference(Reference reference) {
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope)scope;
				//readScope(curScope);
				//currentScopeDB.closeDatabase();
			}
		}
	}


private void createCurrentContext(){
	
}
private void readScope(Scope scope,Database dsDB) {
	//setting the current scope path 
	String currentScopePath = scope.getDbTables();
	for (Object obj : scope.getContent()) {
		if (obj instanceof PCH) {
			//readPCH((PCH) obj);
		} 
	}
}
	/**
	 * @param variableType
	 * @param processName
	 * @param context
	 * 
	 * THIS METHOD WILL SEARCH AND PROCESS THE CURRENT VARIABLE NAME 
	 */
	public static void searchAndProcess(String variableType, String processName, Context context){
		
		JXPathContext currentjxpContext = (JXPathContext) context.getValue(ContextParam.JXPATHCONTEXT);
		
		try{
			StringBuffer xpath = new StringBuffer();
			xpath.append(currentjxpContext.getContextPointer().asPath()).append("/following-sibling::*[@recordType=\"PCH\" and @name=\"" + processName + "\" and not( @executed ) ][1]");
			Pointer refPointer = currentjxpContext.getPointer(xpath.toString());
			//THIS CODE IS ADDED TO HANDLE WHEN A INTERMEDIARY PCH IS NOT A FOLLOWING SIBLING BUT ITS A PRECEDING SIBLING
			Pointer refPointerPreced = null;
			StringBuffer precXpath = new StringBuffer();
			//IF THE PCH IS NOT A FOLLOWING SIBLING THEN WE ARE TRYING TO PRECEDING SIBLING OF THE PCH
			if(refPointer == null || refPointer.getNode() == null){
				precXpath.append(currentjxpContext.getContextPointer().asPath()).append("/preceding-sibling::*[@recordType=\"PCH\" and @name=\"" + processName + "\" and not( @executed ) ][1]");
				refPointerPreced = currentjxpContext.getPointer(precXpath.toString());
			}
			
			if(refPointer != null && refPointer.getNode() != null){
				JXPathContext relativeContext = currentjxpContext.getRelativeContext(refPointer);
				PCH pch = (PCH) currentjxpContext.getValue(xpath.toString());
				
//				System.out.println("PCH found " + pch.getName());
				
//				context.putValue(ContextParam.CURRENTPROCESS, pch.getName());
				try {
					
					context.putValue(ContextParam.JXPATHCONTEXT, relativeContext);					
					AlgoUtil.processStatementBlock(pch, context);
					context.putValue(ContextParam.JXPATHCONTEXT, currentjxpContext);
					
				} catch (Exception e) {
					System.out.println("Error Processing PCH statements " + e.toString());
				}
			}
			else if(refPointerPreced != null && refPointerPreced.getNode() != null){
				JXPathContext relativeContext = currentjxpContext.getRelativeContext(refPointerPreced);
				PCH pch = (PCH) currentjxpContext.getValue(precXpath.toString());
				
//				System.out.println("PCH found " + pch.getName());
				
//				context.putValue(ContextParam.CURRENTPROCESS, pch.getName());
				try {
					
					context.putValue(ContextParam.JXPATHCONTEXT, relativeContext);					
					AlgoUtil.processStatementBlock(pch, context);
					context.putValue(ContextParam.JXPATHCONTEXT, currentjxpContext);
					
				} catch (Exception e) {
					System.out.println("Error Processing PCH statements " + e.toString());
				}
			}
			else{
				if(variableType != null){
					@SuppressWarnings("unchecked")
					Map<String, String> processedBlock = (Map<String, String>) context.getValue(ContextParam.PROCESSEDBLOCK);
					
					if(! processedBlock.containsKey(processName)){
						if(!variableType.contains("XPATH")){
							/*StringBuffer algo = (StringBuffer) context.getValue(ContextParam.ALGO);
							algo.append("\n");*/
							UnInitializedVariableListHolder.getInstance().addVariableToUnIniList(context.getValue(ContextParam.SCOPE).toString(), processName);
							//System.out.println("*********** Variable does not point to a process " + processName + " in " + context.getValue(ContextParam.CURRENTPROCESS) + " in " + context.getValue(ContextParam.SCOPE));
						}else{
//							if(!ArgumentVariable.isVariablePresent((Database)context.getValue(ContextParam.OBJECTDATABASE), 
//																(String)context.getValue(ContextParam.REFERENCE), (String)context.getValue(ContextParam.SCOPE), processName)){
//								System.err.println("########## XPath/Variable " + processName + " is incorrect in scope " + (String)context.getValue(ContextParam.SCOPE));
//							}
						}
						processedBlock.put(processName, processName);
					} 
					
				}
			}
		}catch(Exception _ex){
			System.out.println("error occred to search " + processName + " : " + _ex.toString());
		}
		
		
	}
}
