package com.mmpnc.rating.iso.algorithm.algorithmtest.BOP;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author nilkanth9581
 *
 */
public class TestPublishPSAlgorithms {
//Configuration files required for the peneselvia algo consumption
	
	private static final String psAlgoFile="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-PA-07012013-V02\\ALG-BP-PA-07012013-V02_MR_REARRANGED.xml";
	private static final String psContentFile1="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-PA-07012013-V02\\RC-BP-PA-07012013-V02\\RC-BP-CW-07012013-V01.xml";
	private static final String psContentFile2="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-PA-07012013-V02\\RC-BP-PA-07012013-V02\\RC-BP-PA-07012013-V02.xml";
	
	private static final String psTestFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-PA-07012013-V02\\test-problems-co.xml";
	
	private static final String PUBLISH_GLOBAL_LOOKUPS = "Y";
	private static final String PUBLISH_ALGORITHM_STEPS = "Y";
	
	private static final String LINE_OF_BUSINESS = "BOP";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = false;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		//basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		//basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		return basicConfiguration;
	}
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	private static final String DB_URL_TEST_ICM ="jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_TEST_ICM = "dv_icm_04";
	private static final String DB_PASSWD_TEST_ICM = "dv_icm_04";
	private static final String DB_DRIVER_NAME_TEST_ICM = "oracle.jdbc.driver.OracleDriver";
	
	
	
/*	private DBConfiguration createDBForICMTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST_ICM);
		testDbConfig.setDatabaseUser(DB_USER_TEST_ICM);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST_ICM);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST_ICM);
		return testDbConfig;
	}*/
	
	private DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	
	/*public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE11");
		dbConfiguration.setDatabasePassword("ISORATING_BASE11");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}*/
	
	@Test
	public void testISoConForPS() throws Exception{
		Reader algoReader = new FileReader(psTestFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForTest(), null);
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		
		Reader contentFileReader1 = new FileReader(new File(psContentFile1));
		Reader contentFileReader2 = new FileReader(new File(psContentFile2));
		//Reader contentFileReader3 = new FileReader(new File(contetFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		//conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
}
