package com.mmpnc.rating.iso.algorithm.splitalgousingvariables;

import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.jxpath.JXPathContext;

import com.mmpnc.rating.iso.algorithm.vo.LOB;

public class TestSplitAlgoUsingRequiredVariable {
	
	
	public LOB createLob(FileReader algoFileReader)throws Exception{
		JAXBContext jbContext= JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
		Unmarshaller unmarshal = jbContext.createUnmarshaller();
		LOB lob = (LOB) unmarshal.unmarshal(algoFileReader);
		return lob;
	}
	
	
	
	public void handleMultipleRatingNodes(LOB lob){
		
		//GETTING THE FLOWCHART REFERENCE FROM THE LOB
		
		JXPathContext jxPathContext = JXPathContext.newContext(lob);
	
	}
}
