package com.mmpnc.rating.iso.algorithm.algorithmtest.WC;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author nilkanth9581
 *
 */
public class TestPublish_WC_PA_Algorithms {
	private static final String paContentFile1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-PA-01012014-V01/RC-WC-PA-01012014-V01.xml";
	private static final String testPaFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-PA-01012014-V01/PA-WC-TEST.xml";
	private static final String paAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-PA-01012014-V01/ALG-WC-PA-01012014-V01_MR.xml";
	private static final String PUBLISH_GLOBAL_LOOKUPS = "Y";
	private static final String PUBLISH_ALGORITHM_STEPS = "Y";
	private static final String LINE_OF_BUSINESS = "WC";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = true;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	
	private static final String DB_URL_BID_DEV = //"jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01"; //ISO_BASE
		"jdbc:oracle:thin:@172.16.244.144:1521:DEVBID"; //BIDMOCK
	private static final String DB_USER_BID_DEV = //"ISORATING_BASE";
		"stg_core";
	private static final String DB_PASSWD_BID_DEV = //"ISORATING_BASE";
		"stg_core";
	private static final String DB_DRIVER_NAME = "oracle.jdbc.driver.OracleDriver";
	
	//TARGET DATABASE CONFIGURATION [ICM_DEV]
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.244.144:1521:DEVBID";
	private static final String DB_USER_ICM_DEV = "stg_core";
	private static final String DB_PASSWD_ICM_DEV = "stg_core";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	private static final String LANGUAGE_TEXT_FILE_DIRECTORY = "D://DEMO_ISO_ERC_29th_JAN//intermediary-language-text-file-created";
	private static final String LANGUAGE_TEXT_FILE_NAME = "language-text-file";
	/*public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE");
		dbConfiguration.setDatabasePassword("ISORATING_BASE");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}*/
	
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	/*public DBConfiguration createDBForBIDDEV(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_BID_DEV);
		testDbConfig.setDatabaseUser(DB_USER_BID_DEV);
		testDbConfig.setDatabasePassword(DB_PASSWD_BID_DEV);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}*/
	
	private DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	
	//DATABASE CONFIGURATION FOR ICM_DEV
	/*public DBConfiguration createDbConfigForICMDev(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl(DB_URL_ICM_DEV);
		dbConfiguration.setDatabaseUser(DB_USER_ICM_DEV);
		dbConfiguration.setDatabasePassword(DB_PASSWD_ICM_DEV);
		dbConfiguration.setDatabaseDriverName(DB_DRIVER_NAME_ICM_DEV);
		return dbConfiguration;
	}*/
	
		
/**
 * @return
 */
private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		//String str = "Already executed";
		return basicConfiguration;
	}
	
	@Test
	public void testISoConForTexas() throws Exception{
		//String arr[] = "..".split("\\.");
		Reader algoReader = new FileReader(testPaFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData(),createDBForTest(), null);
		
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		Reader contentFileReader1 = new FileReader(new File(paContentFile1));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
}
