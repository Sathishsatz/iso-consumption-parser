package com.mmpnc.rating.iso.algorithm.algorithmtest.CA;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;
import com.mmpnc.rating.iso.icmxml.creater.AlgorithmIdCreater;

/**
 * @author Nilkanth9581
 *
 */
public class TestPublishCA_WA_ALG {
	private static final String caContentFile1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\CA_ALGO_FILES_FOR_TESTING_MOUDLARIZATION\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01\\RC-CA-CW1-02012014-V01.xml";
	private static final String caContentFile2 =  "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\CA_ALGO_FILES_FOR_TESTING_MOUDLARIZATION\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01\\RC-CA-CW2-02012014-V01.xml";
	private static final String caContentFile3 ="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\CA_ALGO_FILES_FOR_TESTING_MOUDLARIZATION\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01\\RC-CA-CA-03012014-V01.xml";
	//private static final String caAlgoFile = "D:\\files-by-rajesh\\ALG-CA-NY-10012012-V01_MR_P3-2.xml";
	private static final String caAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\CA_ALGO_FILES_FOR_TESTING_MOUDLARIZATION\\RC-CA-WA-10012014-V01\\ALG-CA-WA-10012014-V01_MR_MODIFIED.xml";
	private static final String testCAFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\CA_ALGO_FILES_FOR_TESTING_MOUDLARIZATION\\RC-CA-PA-02012014-V01\\TEST_CA_PA_02012014.xml";
	private static final String minitest = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\Latest-by-iso-parser-3.2.3\\RB-CA-MD-02012014-V01\\test-ca-md.xml";
	private static final String PUBLISH_GLOBAL_LOOKUPS = "N";
	private static final String PUBLISH_ALGORITHM_STEPS = "N";
	private static final boolean IS_STATEWIDE_ALGORITHM_FILE = true;
	String str ="Created expression";
	private static final String LINE_OF_BUSINESS = "CA";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = true;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	
	
	//TARGET DATABASE CONFIGURATION [ICM_DEV]
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.244.144:1521:DEVBID";
	private static final String DB_USER_ICM_DEV = "stg_core";
	private static final String DB_PASSWD_ICM_DEV = "stg_core";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	private static final String LANGUAGE_TEXT_FILE_DIRECTORY = "D://DEMO_ISO_ERC_29th_JAN//intermediary-language-text-file-created";
	private static final String LANGUAGE_TEXT_FILE_NAME = "language-text-file";
	
	/*public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE");
		dbConfiguration.setDatabasePassword("ISORATING_BAS123E");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}*/
	
	
	//REQUIRED PROPERTIES FOR THE ALGORITHM ID GENERATION
	private static final String PRODUCT_NAME = "SILVER";
	private static final String PROGRAM = "BOP";
	private static final String STATE = "MD";
	private static final String EFFECTIVE_DATE = "11-May-2013";
	private static final String EXPIRATION_DATE = "15-May-2013";
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	public DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		basicConfiguration.setStateWideAlgorithms(IS_STATEWIDE_ALGORITHM_FILE);
		return basicConfiguration;
	}
	
	private void setRequiredDataForAlgorithmIdGen(){
		AlgorithmIdCreater algorithmIdCreater =  AlgorithmIdCreater.getInstance();
		algorithmIdCreater.setEffectiveDate(EFFECTIVE_DATE);
		algorithmIdCreater.setExpirationDate(EXPIRATION_DATE);
		algorithmIdCreater.setProgram(PROGRAM);
		algorithmIdCreater.setState(STATE);
		algorithmIdCreater.setProductName(PRODUCT_NAME);
	}
	
	@Test
	public void testISoConForCA() throws Exception{
		
		setRequiredDataForAlgorithmIdGen();
		
		Reader algoReader = new FileReader(caAlgoFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForTest(), null);
		
		
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		Reader contentFileReader1 = new FileReader(new File(caContentFile1));
		Reader contentFileReader2 = new FileReader(new File(caContentFile2));
		Reader contentFileReader3 = new FileReader(new File(caContentFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
		
		//IssueListHolder.printIssueMapForAlgorithmFile();
	}
	
	
	public static void method(String name){
		name = name.toUpperCase();
	}
	
	public static void main(String[] args) {
		String name = new String("nilkanth");
		String []nameArr = new String[1];
		nameArr[0]= name;
		new TestPublishCA_WA_ALG().method(nameArr);
		
		//TestPublishCA_NY_ALG.method(name);
		//System.out.println(name);
		
	}
	
	public void method(String arr[]){
		String name = null;
		try{
			name = arr[3];
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("exception throws");
			name = arr[0];
		}
		
		System.out.println("name is ="+name);
	}
	
	
	
	
	
}
