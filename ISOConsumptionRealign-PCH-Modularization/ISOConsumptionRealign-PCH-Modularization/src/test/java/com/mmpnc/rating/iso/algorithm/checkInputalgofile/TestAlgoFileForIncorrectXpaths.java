package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlDatabase;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.rating.iso.datastructure.AlgorithmDataStructure;

/**
 * @author nilkanth9581
 * 
 */
public class TestAlgoFileForIncorrectXpaths {
	
	ConcurrentMap<String, String> basexDBNameHolder = new ConcurrentHashMap<String, String>();
	
	public LOB getLOBObject(FileReader algoFileReader) {

		JAXBContext jaxbContext;
		try {

			jaxbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshaller.unmarshal(algoFileReader);
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Database getXMLDatabase(String dbName , Map<String,String>xmlMap){
		XmlDatabase db = new XmlDatabase(dbName,xmlMap);
		return db.buildDatabase();
	}
	
	private Database createDataBaseForTableObjects(LOB lob) {

		Database dsDB = null;
		for (Object lobContent : lob.getContent()) {

			if (lobContent instanceof Reference) {
				Reference ref = (Reference) lobContent;

				if (((Reference) ref).getName().equals("Table Objects")) {

					//CREATING TABLE OBJECT DATABASE TO CHECK THE TABLE OBJECTS
					createTableObjectsDataBase(ref);
					
					AlgorithmDataStructure ds = new AlgorithmDataStructure(
							(Reference) ref);
					StringBuffer xmlDS = ds.simplifyDataStrucure();
					/*
					 * try { Writer writer = new FileWriter(new
					 * File("D:\\ds\\ds-xml.xml"));
					 * writer.write(xmlDS.toString()); writer.flush();
					 * writer.close(); } catch (IOException e) { // TODO
					 * Auto-generated catch block e.printStackTrace(); }
					 */
					Map<String, String> xmlMap = new HashMap<String, String>();
					String dbName = "DSFile"+new Random().nextInt();
					if(checkIfTheBasexDBPresent(dbName)){
						dbName = dbName+new Random().nextInt();
					}
					xmlMap.put(dbName, xmlDS.toString());
					dsDB = getXMLDatabase(dbName, xmlMap);
					dsDB.buildDatabase();
					printDataStructureFile(xmlDS);

				}
			}

		}
		
		return dsDB;
	}
	
	
	
	private boolean checkIfTheBasexDBPresent(String dbName){
		if(basexDBNameHolder.containsKey(dbName))
			return true;
		return false;
	}

	public void readLOB(LOB lob) {
		System.out.println(Thread.currentThread().getName() +"Enters in readLOB at "+System.currentTimeMillis());
		Database dsDB = createDataBaseForTableObjects(lob);
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference) ref;
				// CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if ("Common Rating".equals(reference.getType())
						|| "Premium Calculation".equals(reference.getType())) {
					readReference((Reference) ref, dsDB);
				}

			}
		}
		
	}

	private void readReference(Reference reference, Database dsDB) {

		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope) scope;
				readScope(curScope, dsDB);
				// currentScopeDB.closeDatabase();
			}
		}
	}

	private void readScope(Scope scope, Database dsDB) {
		// setting the current scope path
		currentScopePath = scope.getDbTables();
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj, dsDB);
			} else if (obj instanceof If) {
				// System.out.println("NOT EXPECTED THIS SITUATION");
				readIf((If) obj, dsDB);
			} else if (obj instanceof Assign) {
				// System.out.println("NOT EXPECTED THIS SITUATION");
				readAssign((Assign) obj, dsDB);
			} else if (obj instanceof Loop) {
				// System.out.println("NOT EXPECTED THIS SITUATION");
				readLoop((Loop) obj, dsDB);
			} else if (obj instanceof Class) {
			}
		}
	}

	private String currentPCHName = null;
	
	private void readPCH(PCH pch, Database dsDB) {
		
		boolean isPchWithSlashInIt = false;
		String origianlScopePath = null;
		//THIS CHANGE HAS BEEN ADDED TO WHEN THE PCH HAS / IN ITS NAME
		currentPCHName = pch.getName();
		if(pch.getName().contains("/") && !pch.getName().startsWith("ancestor") && !pch.getName().startsWith("../")){
			isPchWithSlashInIt = true;
			origianlScopePath = currentScopePath;
			currentScopePath = pch.getName().split("/")[0];
		}
		
		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj, dsDB);
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj, dsDB);
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj, dsDB);
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	if(isPchWithSlashInIt)
		currentScopePath = origianlScopePath;
		
	}

	private void readIf(If _if, Database dsDB) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj, dsDB);
			} else if (obj instanceof Then) {
				readThen((Then) obj, dsDB);
			} else if (obj instanceof Else) {
				readElse((Else) obj, dsDB);
			}
		}
	}

	private void readCondition(Condition condition, Database dsDB) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj, dsDB);
			}
		}
	}

	private void readThen(Then then, Database dsDB) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj, dsDB);
			} else if (obj instanceof If) {
				readIf((If) obj, dsDB);
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj, dsDB);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	// private int loopIndex = 0;
	private String currentScopePath = null;
	

	private void readLoop(Loop loop, Database dsDB) {
		// checkLoopThroughCondtion(loop.getThrough(), dsDB);
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj, dsDB);
			} else if (obj instanceof If) {
				readIf((If) obj, dsDB);
			} else if (obj instanceof Loop) {
				String originalScopePath = new String(currentScopePath);
				currentScopePath = loop.getThrough();
				readLoop((Loop) obj, dsDB);
				currentScopePath = originalScopePath;

			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void readBracket(Bracket bracket,Database dsDB){
		List<Object>content = bracket.getContent();
		for(Object obj:content){
			if(obj instanceof Expression){
				Expression expression = (Expression)obj;
				readExpression(expression, dsDB);
			}
		}
	}
	private void readRatetable(Ratetable ratetable, Database dsDB) {

		for (Object obj : ratetable.getContent()) {
			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				readArgument(arg, dsDB);
			}
		}
	}

	private void readArgument(Arg arg, Database dsDB) {
		readArgContent(arg, dsDB);
	}

	private void readArgContent(Arg arg, Database dsDB) {
			checkXpath(arg.getVariableType(),(String)arg.getContent(), dsDB);
	}

	
	
	
	private Map <String,List<String>> incorrectXpathMap = new HashMap<String, List<String>>();
	
	private Set<String> objectsNotinTableObjects = new HashSet<String>();
	private Database tableObejctsDB;
	
	
	private void checkWhetherTableObjectIsPresent(String incorrectXpath){
		if(!incorrectXpath.startsWith(".") && !incorrectXpath.startsWith("ancestor") && !incorrectXpath.startsWith("*") && incorrectXpath.contains("/")){
			String tableObjectName = incorrectXpath.replace("/", "~").split("~")[0];
			StringBuffer queryString = new StringBuffer();
			queryString.append("//TableName[@name="+tableObjectName+"]/@xpath/data()");
			Connection tableObjectsCon = new XMLConnection(tableObejctsDB);
			
			Query query = new XmlQuery(tableObjectsCon);
			query.createQuery(queryString.toString());
			Object returnObject = query.execute();
			
			if (returnObject == null || "".equals(returnObject)) {
				objectsNotinTableObjects.add(tableObjectName);
			}
		}
	}
	
	
	private void createTableObjectsDataBase(Reference tableObjects){
		
		String tableObjectStr = null;
		try {
			tableObjectStr = JAXBUtils.writeFromObject(tableObjects);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, String> xmlMap = new HashMap<String, String>();
		String dbName = "TableObjects"+new Random().nextInt();
		if(checkIfTheBasexDBPresent(dbName)){
			dbName = dbName+new Random().nextInt();
		}
		xmlMap.put(dbName, tableObjectStr);
		tableObejctsDB = getXMLDatabase(dbName, xmlMap);
		tableObejctsDB.buildDatabase();
		
	}
	
	private void checkXpath(String variableType,String xPath, Database dsDB) {

		if(!(variableType != null && ("XPATH".equals(variableType)|| "XPATH_STRING".equals(variableType) || "XPATH_NUMERIC".equals(variableType))))
			return;
		StringBuffer queryString = new StringBuffer();
		boolean ascendantOneFlag = false;
		xPath = checkAndRemovePredicatesInXpath(xPath);
		
		if(xPath.endsWith(".Date") || xPath.endsWith(".Year") || xPath.endsWith(".Month")){
			xPath  = xPath.replace(".Date", "").replace(".Year", "").replace(".Month", "");
		}
		
		if(xPath.startsWith("/AscendantOne")){
			ascendantOneFlag = true;
			xPath = xPath.replace("/AscendantOne/", "");
			queryString.append("//(").append(xPath).append(")/@xpath/data()");
		}
		else if(xPath.startsWith("/")){
			if(xPath.startsWith("//")){
				xPath = xPath.substring(2);
			}else{
				xPath = xPath.substring(1);
			}
			
			queryString.append("//(").append(xPath).append(")/@xpath/data()");
		}
		else{
			queryString.append("//").append(currentScopePath).append("/")
			.append(xPath).append("/@xpath/data()");
		}
		
		Connection curScopeCon = new XMLConnection(dsDB);
		
		
		Query query = new XmlQuery(curScopeCon);
		query.createQuery(queryString.toString());
		Object returnObject = query.execute();
		if (returnObject == null || "".equals(returnObject)) {
			
			//Checking table objects 
			checkWhetherTableObjectIsPresent(xPath);
			
			if(ascendantOneFlag)
				xPath = "/AscendantOne/" +xPath;
			
			xPath = xPath + "-------- PCH NAME:["+currentPCHName+"]";
			
			if(incorrectXpathMap.containsKey(currentScopePath)){
				List <String> incorrectXpathList = incorrectXpathMap.get(currentScopePath);
				if(!incorrectXpathList.contains(xPath))
					incorrectXpathList.add(xPath);
			}else{
				List<String> incorrectXpahList = new ArrayList<String>();
				incorrectXpahList.add(xPath);
				incorrectXpathMap.put(currentScopePath, incorrectXpahList);
			}
			
		}

	}
	
	private String checkAndRemovePredicatesInXpath(String xpathAttribute){
		if(xpathAttribute.contains("[")){
			
			//IF XPATH CONTINAS ANCESTOR OR NOT(@OFFSET) THEN WE DO NOT HAVE TO CONISDER IT AS PREDICATE
			if(xpathAttribute.contains("ancestor::*") || xpathAttribute.contains("not(@offset=") || xpathAttribute.contains("descendant::") || xpathAttribute.contains("not(@offset =")){
				
				/*if(countFunctionFlag != null){
					
					if((Boolean) countFunctionFlag){
						//predicateContext.putValue(ContextParam.PREDICATEWITHCOUNTFUNCTION,booleanFlag);
						return handleCountFunctionPredicate();
					}
					
				}*/
				
				xpathAttribute = xpathAttribute.replaceAll("^\"|\"$", "");
				//StringBuffer xpathBuffer = new StringBuffer(xpathStr.trim());
				//Context predicateContext = createContextForPredicate(xpathBuffer, xpathStr ,ifConditionXpath,isAssignRhsXpath);
				//EXECUTING PREDICATE EVALUATOR TO GET THE PREDICATES XPATH
				//executePredicateEval(predicateContext, xpathBuffer);
				
				/*if(!checkXpathForIfConditionOrCountFunction(countFunctionFlag, predicateContext, ifConditionXpath)){
					//AS FOR COUNT FUNCTION XPATH PREDICATE AND IF CONDITION PREDICATE WE DO NOT NEED A MEMORY LOOKUP
					return createMemoryLookupStep(xpathStr,predicateContext);
				}*/
					
				xpathAttribute = xpathAttribute.replaceAll("\\[.*?\\]", "");
			}else{
				//FOR NOW WE ARE NOT CHECKING CORRECTNESS OF PRDICATES
				xpathAttribute = xpathAttribute.replaceAll("^\"|\"$", "");
				xpathAttribute = xpathAttribute.replaceAll("\\[.*?\\]", "");
			}
			
		}
		return xpathAttribute;
	}
	
	private void printDataStructureFile(StringBuffer xmlDS){
		try{
			Writer writer = new FileWriter(new File("D:\\ds\\ds-xml.xml"));
			writer.write(xmlDS.toString());
			writer.flush();
			writer.close();
		}
		catch(Exception e){
			System.err.println("Exception while pringin data strucutre file");
		}
	}
	
	public void printIncoreectXpathList(String fileName){
		
	/*	for(Map.Entry<String, String> entry : incorrectXpathMap.entrySet()){
			System.out.println(entry.getKey());
			
			Map<String,List<String>> scopeAndCustomFunction = new HashMap<String, List<String>>();*/
			//Map<String,List<String>> scopeAndIncorrectXpath = new HashMap<String, List<String>>();
			System.out.println("-----------------Incorrect Xpath found in "+fileName+" ----------------");
			/*for(Map.Entry<String, String> entry : incorrectXpathMap.entrySet()){
				String scopeName = entry.getValue().trim();
				String incorrectXpath = entry.getKey().trim();
				
				if(scopeAndIncorrectXpath.containsKey(scopeName)){
					List<String>functionList = scopeAndIncorrectXpath.get(scopeName);
					functionList.add(incorrectXpath);
				}else{
					List<String>functionList = new ArrayList<String>();
					functionList.add(incorrectXpath);
					scopeAndIncorrectXpath.put(scopeName, functionList);
				}
			}*/
			
			for(Map.Entry<String, List<String>> entry : incorrectXpathMap.entrySet()){
				System.out.println("~~~~SCOPE NAME : ["+entry.getKey()+"]");
				System.out.println("----Incorrect Xpath :");
				for(String incorrectXpath:entry.getValue()){
					System.out.println("\t\t\t"+incorrectXpath);
				}
			}
		//}
	}
	
	
	public  void printXpathDoNotHaveObjectInTableObjects(){
		
		for(String tableObjectNames:objectsNotinTableObjects){
			System.out.println(tableObjectNames);
		}
	}
	private void readElse(Else _else, Database dsDB) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj, dsDB);
			} else if (obj instanceof If) {
				readIf((If) obj, dsDB);
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj, dsDB);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}

	private void readAssign(Assign assign, Database dsDB) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj, dsDB);
			}
		}
	}

	private void readExpression(Expression expression, Database dsDB) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
			// sb.append(" expression -> " + expression.getOp());
			readBothSides(expression, dsDB);
		} else {
			readContent(expression, dsDB);
		}
	}

	private void readBothSides(Expression expression, Database dsDB) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex, dsDB);
			} else if (obj instanceof Function) {
				// readFunction((Function) obj,dsDB);
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj, dsDB);
				// rateTableList.add((Ratetable)obj);
			}else if (obj instanceof Bracket){
				readBracket((Bracket)obj,dsDB);
			}
		}
	}

	private void readContent(Expression expression, Database dsDB) {
		for (Object obj : expression.getContent()) {

			if (obj instanceof String) {
				checkXpath(expression.getVariableType(),(String)obj, dsDB);
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj, dsDB);
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj,dsDB);
			} else if (obj instanceof Function) {
				// ((Function) obj,dsDB);
			}
		}
	}

	// private static final String fileName =
	// "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-MD-09012012-V01/ALG-BP-MD-09012012-V01_MR.xml";
	//static String fileName1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-TX-09072013-V01\\ALG-WC-TX-09072013-V01_MR.xml";
	/*String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-CO-11012013-V01\\ALG-BP-CO-11012013-REARRANGED_V01_MR.xml";*/
	/*String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-MD-02012014-V01\\ALG-CA-MD-02012014-V01_MR.xml";*/
	//String fileName3 ="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\ALG-CA-NY-10012012-V01_MR.xml";
	//String fileName4 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\HO-ALGORITHM-FILES\\RC-HO-AZ-09012012-V01\\ALG-HO-AZ-09012012-V01_MR.xml";
	/*String fileName =  "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\HO-ALGORITHM-FILES\\ALG-HO-IL-06012012-V01_MR\\ALG-HO-IL-06012012-V01_MR.xml";*/
	//String fileName2 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-NJ-01012013-V01\\ALG-WC-NJ-01012013-V01_MR.xml";
	//String fileName1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-PA-01012014-V01/ALG-WC-PA-01012014-V01_MR.xml";
	/*String fileName1 =  "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RC-WC-CO-01012014-V02\\ALG-WC-CO-01012014-V02_MR.xml";*/
	String fileName1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\FileFromParser3.2.2\\ALG-CA-NY-07012014-V01_MR.xml";
	
	// private static final String txAlgoFile =
	// "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";
	// String fileName =
	// "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\CA_NY_TEST.xml";
	@Test
	public void testAlgoFileForPCh() throws Exception {

		
		List<String> fileName = new ArrayList<String>();
		
		fileName.add(fileName1);
		/*fileName.add(fileName);
		fileName.add(fileName1);
		fileName.add(fileName4);*/
		
		checkListOfAlgorithmFiles(fileName);
		
		/*try {
			System.out.println("Checking Xpath please wait ...");
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			readLOB(lob);
			printIncoreectXpathList();
			System.out.println(".....Process Complete....");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

	}
	
	
	private void checkSingleAlgorithmFile(String fileName){
		try {
			System.out.println("Checking Xpath of file "+fileName+" please wait ...");
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			readLOB(lob);
			printIncoreectXpathList(fileName);
			System.out.println(".....Process Complete....");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void checkListOfAlgorithmFiles(List<String> fileNames) throws ExecutionException,InterruptedException{
			
			
			
		//System.out.println("starting time"+System.currentTimeMillis());	
		ExecutorService service = Executors.newFixedThreadPool(10);
			
			for(String fileName : fileNames){
				
				/*Thread thread = new Thread(new ExecuteRequestInParellelThreads(fileName));
				//synchronized (thread) {
					thread.setName(fileName);
					thread.start();*/
				//	thread.wait();
				//}
				//service.
				Future future = service.submit(new ExecuteRequestInParellelThreads(fileName));
				/*if(future.get() == null){
					System.out.println("task executes successfully");
				}*/
				/*thread.setName(fileName);
				thread.start()*/
				//Thread.currentThread().join();
				
			}
			service.shutdown();
			
			service.awaitTermination(1, TimeUnit.DAYS);
			
			//}
			
			
			
		}
	}

class ThreadPerTaskExecutor implements Executor{

	/*public ThreadPerTaskExecutor(String fileName){
		
	}*/
	@Override
	public void execute(Runnable command) {
		new Thread(command).start();
		
	}
	
}

class ExecuteRequestInParellelThreads implements Runnable{

	private String fileName;
	
	public ExecuteRequestInParellelThreads(String fileName){
		this.fileName = fileName;
	}
	
	@Override
	public void run() {
		try{
			long startTime = System.currentTimeMillis();
			
			/*System.out.println("Checking Xpath of file "+fileName+" please wait ...");*/
			FileReader fileReader = new FileReader(new File(fileName));
			TestAlgoFileForIncorrectXpaths test = new TestAlgoFileForIncorrectXpaths();
			LOB lob = test.getLOBObject(fileReader);
			test.readLOB(lob);
			
			test.printIncoreectXpathList(fileName);
			//synchronized (this) {
			//	notify();
			//}
			System.out.println("Start time for ="+Thread.currentThread().getName()+"= "+startTime);
			long endTime = System.currentTimeMillis();
			System.out.println("EndTime for ="+Thread.currentThread().getName()+"="+endTime);
			/*System.out.println(".....Process Complete....");*/
			System.out.println("Printing xpaths whose table objects are not present in ALG FILE");
			test.printXpathDoNotHaveObjectInTableObjects();
		}catch(FileNotFoundException e){
			System.out.println("Please check the file name");
		}
		
		
	}
	
	
	
	
}
