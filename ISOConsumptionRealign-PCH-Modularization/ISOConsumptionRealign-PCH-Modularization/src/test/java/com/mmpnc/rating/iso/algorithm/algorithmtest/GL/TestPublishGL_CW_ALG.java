package com.mmpnc.rating.iso.algorithm.algorithmtest.GL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author Nilkanth9581
 *
 */
public class TestPublishGL_CW_ALG {
	private static final String glContentFile1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\GL files from parser 3.2.2\\satish-issue\\ALG-GL-NY-10012014-V01_MR_MODIFIED\\RC-GL-CW1-05012014-V01.xml";
	private static final String glContentFile2 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\GL files from parser 3.2.2\\satish-issue\\ALG-GL-NY-10012014-V01_MR_MODIFIED\\RC-GL-CW2-05012014-V01.xml";
	private static final String glContentFile3 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\GL files from parser 3.2.2\\satish-issue\\ALG-GL-NY-10012014-V01_MR_MODIFIED\\RC-GL-NY-10012014-V01.xml";
	private static final String GLAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\GL files from parser 3.2.2\\satish-issue\\ALG-GL-NY-10012014-V01_MR_MODIFIED\\ALG-GL-NY-10012014-V01_MR_MODIFIED.xml";
	private static final String testGLAZFile ="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\rakesh-GL-issue\\TX\\test-countrywide.xml";
	private static final String PUBLISH_GLOBAL_LOOKUPS = "N";
	private static final String PUBLISH_ALGORITHM_STEPS = "N";
	String str ="Created expression";
	private static final String LINE_OF_BUSINESS = "GL";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = true;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	
	//TARGET DATABASE CONFIGURATION [ICM_DEV]
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.244.144:1521:DEVBID";
	private static final String DB_USER_ICM_DEV = "stg_core";
	private static final String DB_PASSWD_ICM_DEV = "stg_core";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	private static final String LANGUAGE_TEXT_FILE_DIRECTORY = "D://DEMO_ISO_ERC_29th_JAN//intermediary-language-text-file-created";
	private static final String LANGUAGE_TEXT_FILE_NAME = "language-text-file";
	
	/*public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE");
		dbConfiguration.setDatabasePassword("ISORATING_BAS123E");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}*/
	
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SREasdfa";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	private static final String DB_URL_MOD_TEST = "jdbc:oracle:thin:@172.16.244.185:1521:rating";
	private static final String DB_USER_MOD_TEST = "WASICM2asdf";
	private static final String DB_PASSWD_MOD_TEST = "WASICM2";
	private static final String DB_DRIVER_NAME_MOD_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	public DBConfiguration createDBForPCHModTest(){
		
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_MOD_TEST);
		testDbConfig.setDatabaseUser(DB_USER_MOD_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_MOD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_MOD_TEST);
		return testDbConfig;
		
	}
	
	public DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		basicConfiguration.setStateWideAlgorithms(false);
		return basicConfiguration;
	}
	
	@Test
	public void testISoConForHO() throws Exception{
		
		
		/*Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					execute();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t1.join();
		
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					execute();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		t2.start();
		t2.join();*/
		execute();
		
	}
	
	public void execute() throws Exception{
		Reader algoReader = new FileReader(testGLAZFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForPCHModTest(), null);
		
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		Reader contentFileReader1 = new FileReader(new File(glContentFile1));
		Reader contentFileReader2 = new FileReader(new File(glContentFile2));
		Reader contentFileReader3 = new FileReader(new File(glContentFile3));
		//Reader contentFileReader3 = new FileReader(new File(caContentFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		conentFileReaderList.add(contentFileReader3);
		//conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
	
	
	public static void method(String name){
		name = name.toUpperCase();
	}
	
	public static void main(String[] args) {
		String name = new String("nilkanth");
		String []nameArr = new String[1];
		nameArr[0]= name;
		new TestPublishGL_CW_ALG().method(nameArr);
		//TestPublishCA_NY_ALG.method(name);
		//System.out.println(name);
	}
	
	public void method(String arr[]){
		String name = null;
		try{
			name = arr[3];
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("exception throws");
			name = arr[0];
		}
		
		System.out.println("name is ="+name);
	}
	
	
	
	
	
}
