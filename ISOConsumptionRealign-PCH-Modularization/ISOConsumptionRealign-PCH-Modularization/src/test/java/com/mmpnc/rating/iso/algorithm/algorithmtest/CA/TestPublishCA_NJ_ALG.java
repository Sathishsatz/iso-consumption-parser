package com.mmpnc.rating.iso.algorithm.algorithmtest.CA;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.algorithmtest.TestHelper;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author Nilkanth9581
 *
 */
public class TestPublishCA_NJ_ALG {
	private static final String caContentFile1 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\RC-CA-NY-10012012-V01\\RC-CA-CW1-06012010-V05.xml";
	private static final String caContentFile2 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\RC-CA-NY-10012012-V01\\RC-CA-CW2-06012010-V05.xml";
	private static final String caContentFile3 = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\RC-CA-NY-10012012-V01\\RC-CA-NY-10012012-V01.xml";
	//private static final String caAlgoFile = "D:\\files-by-rajesh\\ALG-CA-NY-10012012-V01_MR_P3-2.xml";
	private static final String caAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\ALG-CA-NY-10012012-V01_MR.xml";
	private static final String testCAFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\CA_NY_TEST.xml";
	private static final String minitest = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-NY-10012012-V01\\minitest.xml";
	private static final String PUBLISH_GLOBAL_LOOKUPS = "Y";
	private static final String PUBLISH_ALGORITHM_STEPS = "Y";
	String str ="Created expression";
	private static final String LINE_OF_BUSINESS = "CA";
	private static final boolean CREATE_LANGUAGE_TEXT_FILE = true;
	private static final String RATING_CATEGORY_NAME = "RatingCategory";
	String s = "StartsWith";
	
	//TARGET DATABASE CONFIGURATION [ICM_DEV]
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.209.197:1601:dvebpp01";
	private static final String DB_USER_ICM_DEV = "dv_icm_01";
	private static final String DB_PASSWD_ICM_DEV = "dv_icm_01";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	/*private static final String DB_URL_ICM_DEV = "jdbc:oracle:thin:@172.16.244.144:1521:DEVBID";
	private static final String DB_USER_ICM_DEV = "stg_core";
	private static final String DB_PASSWD_ICM_DEV = "stg_core";
	private static final String DB_DRIVER_NAME_ICM_DEV = "oracle.jdbc.driver.OracleDriver";*/
	
	private static final String LANGUAGE_TEXT_FILE_DIRECTORY = "D://DEMO_ISO_ERC_29th_JAN//intermediary-language-text-file-created";
	private static final String LANGUAGE_TEXT_FILE_NAME = "language-text-file";
	
	/*public DBConfiguration  createDbConfig(){
		DBConfiguration dbConfiguration = DBConfiguration.getInstance();
		dbConfiguration.setDbUrl("jdbc:oracle:thin:@172.16.209.201:1601:dvisrt01");
		dbConfiguration.setDatabaseUser("ISORATING_BASE");
		dbConfiguration.setDatabasePassword("ISORATING_BAS123E");
		dbConfiguration.setDatabaseDriverName("oracle.jdbc.driver.OracleDriver");
		return dbConfiguration;
	}*/
	
	
	private static final String DB_URL_TEST = "jdbc:oracle:thin:@172.16.209.165:1521:stgsdk";
	private static final String DB_USER_TEST = "SRE";
	private static final String DB_PASSWD_TEST = "SRE";
	private static final String DB_DRIVER_NAME_TEST = "oracle.jdbc.driver.OracleDriver";
	
	
	public DBConfiguration createDBForTest(){
		DBConfiguration testDbConfig = DBConfiguration.getInstance();
		testDbConfig.setDbUrl(DB_URL_TEST);
		testDbConfig.setDatabaseUser(DB_USER_TEST);
		testDbConfig.setDatabasePassword(DB_PASSWD_TEST);
		testDbConfig.setDatabaseDriverName(DB_DRIVER_NAME_TEST);
		return testDbConfig;
	}
	private BasicConfiguration setBasicConfigurationForPublishingData(){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setPublishAlgorithmSteps(PUBLISH_ALGORITHM_STEPS);
		basicConfiguration.setPublishLookupModels(PUBLISH_GLOBAL_LOOKUPS);
		basicConfiguration.setCreateLangeuageTextFile(CREATE_LANGUAGE_TEXT_FILE);
		basicConfiguration.setLanguageTextFileDirectory(LANGUAGE_TEXT_FILE_DIRECTORY);
		basicConfiguration.setLanguageTextFileName(LANGUAGE_TEXT_FILE_NAME);
		basicConfiguration.setIcmCategoryNameForRatingLookup(RATING_CATEGORY_NAME);
		return basicConfiguration;
	}
	
	@Test
	public void testISoConForCA() throws Exception{
		
		Reader algoReader = new FileReader(testCAFile);
		TestHelper testHelper = new TestHelper(algoReader);
		Configurer.getInstance().buildConfigurer(setBasicConfigurationForPublishingData() , createDBForTest(), null);
		
		List<Reader> conentFileReaderList = new ArrayList<Reader>();
		Reader contentFileReader1 = new FileReader(new File(caContentFile1));
		Reader contentFileReader2 = new FileReader(new File(caContentFile2));
		Reader contentFileReader3 = new FileReader(new File(caContentFile3));
		//CREATING CONTENT FILE READERS LIST
		conentFileReaderList.add(contentFileReader1);
		conentFileReaderList.add(contentFileReader2);
		conentFileReaderList.add(contentFileReader3);
		testHelper.texasAlgoTesting(conentFileReaderList,RATING_CATEGORY_NAME,LINE_OF_BUSINESS);
	}
	
	
	public static void method(String name){
		name = name.toUpperCase();
	}
	
	public static void main(String[] args) {
		String name = new String("nilkanth");
		String []nameArr = new String[1];
		nameArr[0]= name;
		new TestPublishCA_NJ_ALG().method(nameArr);
		
		//TestPublishCA_NY_ALG.method(name);
		//System.out.println(name);
		
	}
	
	public void method(String arr[]){
		String name = null;
		try{
			name = arr[3];
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("exception throws");
			name = arr[0];
		}
		
		System.out.println("name is ="+name);
	}
	
	
	
	
	
}
