package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.io.Reader;
import java.util.List;

import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.wrapper.ICMRequiredData;
import com.mmpnc.rating.iso.wrapper.ISOConsuptionWrapper;

/**
 * @author nilkanth9581
 * This will be the client class for ISOConsuption utility
 * 
 */
public class TestISOConsuption extends ISOConsuptionWrapper{

	
	//Pass a reader for algorithm file as a constructor parameter
	public TestISOConsuption(Reader algoReader) {
		super(algoReader);
	}

	
	
	//Create a method and give call to readAlgoFileAndCreateICMInputXml method
	public void cosumeAlgorithmFileAndCreateRatingSteps(List<Reader>contentFileReaderList,String ratingcategoryName,String lineOfBusiness)throws Exception{
		readAlgoFileAndCreateICMInputXml(contentFileReaderList, ratingcategoryName, lineOfBusiness,BasicConfiguration.getInstance().isStateWideAlgorithms());
	}
	
	//override Consuption poss process where you will get the output of the ISO ERC tool
	@Override
	public void cosumptionPostProcess(ICMRequiredData icmRequiredData) {
		System.out.println("Say Hi");
	}

}
