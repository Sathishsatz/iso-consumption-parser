package com.mmpnc.rating.iso.algorith.file.cleansing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Premium;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.rating.iso.datastructure.AlgorithmDataStructure;

/**
 * @author nilkanth9581
 * 
 *         This test case will do following 
 *       - Replace PCH name GetInteger {..} with GetInteger / while processing these PCH consumption-parser will ignore these PCH 
 *       - Replace PCH name GetDeductible{...} with  GetDeductible / While processing these PCH consumption-parser will ignore these PCH 
 *       - Replace PCH name domainTableLookup{..} with  domainTableLookup / while processing these PCH consumption-parser will ignore these PCH 
 *       - Replace PCH name integerAmount_.. with integerAmount / while processing these PCH consumption-parser will ignore these PCH       
 *       - Replace PCH name deductibleAmount_.. with deductibleAmount / while processing these PCH consumption-parser will ignore these PCH 
 *       - Replace PCH name starts with ../A/B with B 
 *       - Replace PCH name starts with ancestor by removing xpath and keeping last parameter as the PCH name
 *         At the end of this process a new modified algorithm file will be
 *         created with all above changes
 * 
 */
public class FindAndReplacePCHNamesWithXpathsAndLookupFunctions implements AlgoFileCleansingInterface{

	public LOB getLOBObject(FileReader algoFileReader) {

		JAXBContext jaxbContext;
		try {

			jaxbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshaller.unmarshal(algoFileReader);
			populateFolwChartContentMap(lob);
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	Map<String,List<Object>> flowChartContent =  new HashMap<String, List<Object>>();
	
	
	private void populateFolwChartContentMap(LOB lob){
		
		for(Object ref: lob.getContent()){
			if(ref instanceof Reference){
				Reference reference = (Reference)ref;
				
				if("Execution Flow".equals(reference.getType())){
					for(Object classs:reference.getContent()){
						if(classs instanceof Class){
							Class classss = (Class)classs;
							readClass((Class)classs,classss.getPass());
						}
						
					}
				}
			}
		}
	
	}
	
	private void readClass(Class classs,String pass){
		
		for(Object cont: classs.getContent()){
			
			if(cont instanceof Class){
				Class newClass = (Class)classs;
				readClass((Class) cont,newClass.getPass());
			}else if(cont instanceof Rating){
				Rating rating = (Rating)cont;
				checkAndAddInotMap(pass, rating.getClassName(), rating.getType(), rating);
			}else if(cont instanceof Premium){
				Premium premium = (Premium) cont;
				checkAndAddInotMap(pass, premium.getClassName(), premium.getType(), premium);
			}
			
		}
	}
	
	private void checkAndAddInotMap(String pass,String coverageName,String type,Object ratingOrPremium){
		String key = coverageName+"_"+type+"_"+pass;
		if(flowChartContent.containsKey(key)){
			flowChartContent.get(key).add(ratingOrPremium);
		}else{
			List<Object> list = new ArrayList<Object>();
			list.add(ratingOrPremium);
			flowChartContent.put(key, list);
		}
	}
	
	
	public void readLOB(LOB lob) {
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference) ref;
				// CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if ("Common Rating".equals(reference.getType())
						|| "Premium Calculation".equals(reference.getType())) {
					readReference((Reference) ref);
				}

			}
		}
	}

	public void writeLOB(LOB lob) throws IOException, Exception {
		File f = new File(fileNameNEw);
		FileOutputStream fileOutputStream = new FileOutputStream(f);
		fileOutputStream.write(JAXBUtils.writeFromObject(lob).getBytes());
	}

	private void readReference(Reference reference) {

		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope) scope;
				readScope(curScope, reference.getType());
				// currentScopeDB.closeDatabase();
			}
		}
	}

	private void readScope(Scope scope, String referencetype) {
		// setting the current scope path

		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj, scope, referencetype);
			} else if (obj instanceof If) {
				// System.out.println("NOT EXPECTED THIS SITUATION");
				System.err
						.println("Statemets out of PCH block Please check in scope!!"
								+ scope.getDbTables());
				// readIf((If) obj );
			} else if (obj instanceof Assign) {
				// System.out.println("NOT EXPECTED THIS SITUATION");
				System.err
						.println("Statemets out of PCH block Please check in scope!!"
								+ scope.getDbTables());
				// readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				// System.out.println("NOT EXPECTED THIS SITUATION");
				System.err
						.println("Statemets out of PCH block Please check in scope!!"
								+ scope.getDbTables());
				// readLoop((Loop) obj );
			} else if (obj instanceof Class) {
			}
		}
	}

	/**
	 * @param lookupName
	 * @param referencedFrom
	 * @param scopeName
	 * @return This method decides lookup name also populated PCHInfo object for
	 *         this particular lookup function
	 */
	private String decideLookupName(String lookupName, String referencedFrom,
			String scopeName) {

		if (lookupPCHNameAndItsReplacement.containsKey(lookupName)) {
			PCHInfo pchInfo = lookupPCHNameAndItsReplacement.get(lookupName);
			if (referencedFrom != null
					&& !pchInfo.getChangedPCHName().equals(referencedFrom))
				if (pchInfo.getReferencedFrom() != null)
					pchInfo.getReferencedFrom().append(",")
							.append(referencedFrom);
			return pchInfo.getChangedPCHName();
		}
		String lookup = lookupName.substring(0, lookupName.indexOf("{"));
		// this needs to be revisited as two different strings can have same
		// hash code

		lookup = lookup.trim() + "_" + idGenerator(lookupName);
		PCHInfo pchInfo = new PCHInfo();
		pchInfo.setPchName(lookupName);
		pchInfo.setChangedPCHName(lookup);
		if (referencedFrom != null)
			pchInfo.setReferencedFrom(new StringBuffer(referencedFrom));
		pchInfo.setScopeName(scopeName);
		lookupPCHNameAndItsReplacement.put(lookupName, pchInfo);
		return lookup;
	}

	/**
	 * @param pass
	 * @return\ This id generator we are using to match name of the lookup
	 *          function in country wide and state wide algorithm files as we
	 *          are processing these two files separately
	 * 
	 */
	public static String idGenerator(String pass) {
		byte[] passBytes = pass.getBytes();
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(passBytes);
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(passBytes);

			// return messageDigest.toString();
			BigInteger number = new BigInteger(1, messageDigest);
			return number.toString(32).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			throw new Error("invalid JRE: have not 'MD5' impl.", e);
		}
	}

	public static void main(String args[]){
		 	System.out.println("         \\,,,/");
	        System.out.println("         (o o)");
	        System.out.println("-----oOOo-(_)-oOOo-----");
	        System.out.println("----- HA-HA NO LOOKUP FUNCTION FOUND ----");
		
		System.out.println(idGenerator("LookupLossDevelopmentFactor {&quot;PremOps&quot; , MaturityYear1}"));
	}
	
	private void replaceInFlowChart(String pchName, String scopeName,String refType,short pass,String modifiedPCHName){
		
		String key = scopeName+"_"+refType+"_"+pass;
		List<Object> list = flowChartContent.get(key);
		if(list != null){
			for(Object object:list){
				if(object instanceof Rating){
					Rating rating = (Rating)object;
					String variables = rating.getVariables();
					if(variables.contains(pchName)){
						variables = variables.replace(pchName, modifiedPCHName);
						rating.setVariables(variables);
					}
				}
				if(object instanceof Premium){
					Premium premium = (Premium)object;
					String variable = premium.getVariables();
					if(variable.contains(pchName)){
						variable = variable.replace(pchName, modifiedPCHName);
						premium.setVariables(variable);
					}
				}
			}
		}
		
		
	}
	
	
	
	private String removeXpathFromPCHName(String pchName, String scopeName,String refType,short pass) {
		String modifiedPchName = pchName
				.substring(pchName.lastIndexOf("/") + 1);
		
		modifiedPchName = modifiedPchName + "_" +idGenerator(pchName);
		
		replaceInFlowChart(pchName, scopeName, refType, pass, modifiedPchName);
		PCHInfo pchInfo = new PCHInfo();
		pchInfo.setPchName(pchName);
		pchInfo.setChangedPCHName(modifiedPchName);
		pchInfo.setScopeName(scopeName);
		lookupPCHNameAndItsReplacement.put(pchName, pchInfo);
		return modifiedPchName;
	}

	class PCHInfo {
		private String pchName;
		private String changedPCHName;
		private String scopeName;
		private StringBuffer referencedFrom;

		public String getPchName() {
			return pchName;
		}

		public void setPchName(String pchName) {
			this.pchName = pchName;
		}

		public String getChangedPCHName() {
			return changedPCHName;
		}

		public void setChangedPCHName(String changedPCHName) {
			this.changedPCHName = changedPCHName;
		}

		public String getScopeName() {
			return scopeName;
		}

		public void setScopeName(String scopeName) {
			this.scopeName = scopeName;
		}

		public StringBuffer getReferencedFrom() {
			return referencedFrom;
		}

		public void setReferencedFrom(StringBuffer referencedFrom) {
			this.referencedFrom = referencedFrom;
		}

	}

	/**
	 * @param pch
	 *            This method will rename having domain table lookup function
	 *            GetDedctuible function , GetInteger function , integerAmount_
	 *            dedcutlbleAmount_ in This functionality is added to handle
	 *            changes in ISO-parser 3.2.2
	 */

	private void checkAndAdd(Map<String, StringBuffer> map, String dbTable,
			String pchName) {
		if (map.containsKey(dbTable)) {
			map.get(dbTable).append(",").append(pchName);
		} else {
			map.put(dbTable, new StringBuffer(pchName));
		}
	}

	private void renamePCHNames(PCH pch, Scope scope) {
		String pchName = pch.getName();
		String dbTable = scope.getDbTables();
		if (pchName.startsWith("domainTableLookup") && pchName.contains("{")) {
			checkAndAdd(pchNamesWithdomainTableLookup, dbTable, pchName);
			pchName = pchName.substring(0, pchName.indexOf("{"));
			pch.setName(pchName.trim());
		}else if(pchName.startsWith("domain")){
			String originalPCHName = pchName;
			if(pchName.contains("{")){
				checkAndAdd(pchNamesWithdomainTableLookup, dbTable, pchName);
				pchName = pchName.substring(0, pchName.indexOf("{"));
				//setting domain table lookup for such pch
				//PCH name found in WC-CW file domainZipCodeTableLookup
				//to handle this condition we are directly setting domainTableLookup for that PCH
				if(pchName != null && pchName.trim().endsWith("TableLookup") && originalPCHName.contains("{"))
				pch.setName("domainTableLookup");
			}
		} 
		else if (pchName.startsWith("GetDeductible") && pchName.contains("{")) {
			checkAndAdd(pchNamesWithGetDeductibleFunctionName, dbTable, pchName);
			pchName = pchName.substring(0, pchName.indexOf("{"));
			pch.setName(pchName.trim());
		} else if (pchName.startsWith("GetInteger") && pchName.contains("{")) {
			checkAndAdd(pchNamesWithGetIntegerFunctionName, dbTable, pchName);
			pchName = pchName.substring(0, pchName.indexOf("{"));
			pch.setName(pchName.trim());
		} else if (pchName.startsWith("integerAmount_")) {
			checkAndAdd(pchNamesWithintegerAmount, dbTable, pchName);
			pchName = pchName.substring(0, pchName.indexOf("_"));
			pch.setName(pchName.trim());
		} else if (pchName.startsWith("deductibleAmount_")) {
			checkAndAdd(pchNamesWithDedcutibleAmount, dbTable, pchName);
			pchName = pchName.substring(0, pchName.indexOf("_"));
			pch.setName(pchName.trim());
		}
	}

	private void readPCH(PCH pch, Scope scope, String referenceType) {
		renamePCHNames(pch, scope);
		String name = pch.getName().trim();
		String originalPCHName = pch.getName();

		if (name.startsWith("../")) {
			name = removeXpathFromPCHName(name, scope.getDbTables(),referenceType,scope.getPass());
			pch.setName(name);
		} else if (name.contains("ancestor")) {
			name = removeXpathFromPCHName(name, scope.getDbTables(),referenceType,scope.getPass());
			pch.setName(name);
		} else if (name.contains("{") && name.startsWith("Lookup")) {

			name = decideLookupName(name, null, scope.getDbTables());
			// we need to set overridden attribute as true for this PCH
			// name in country wide and state wide algorithm file may be
			// different
			String key = scope.getDbTables() + "_" + referenceType;
			if (lookupNamesAndReplacement.containsKey(key)) {
				lookupNamesAndReplacement.get(key).append(",PCH NAME:[")
						.append(originalPCHName).append(" ]~").append("\t")
						.append("Replaced with:[" + name + "]");
			} else {
				lookupNamesAndReplacement.put(
						key,
						new StringBuffer()
								.append("ORIGINAL PCH:[" + originalPCHName
										+ "]~").append("\t")
								.append("Replaced with:[" + name + "]"));

			}
			pch.setName(name);
		}

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj, name, scope.getDbTables());
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj, name, scope.getDbTables());
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj, name, scope.getDbTables());
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	}

	private void readIf(If _if, String pchName, String scopeDbTable) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj, pchName, scopeDbTable);
			} else if (obj instanceof Then) {
				readThen((Then) obj, pchName, scopeDbTable);
			} else if (obj instanceof Else) {
				readElse((Else) obj, pchName, scopeDbTable);
			}
		}
	}

	private void readCondition(Condition condition, String pchName,
			String scopeDbTable) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj, pchName, scopeDbTable);
			}
		}
	}

	private void readThen(Then then, String pchName, String scopeDbTable) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj, pchName, scopeDbTable);
			} else if (obj instanceof If) {
				readIf((If) obj, pchName, scopeDbTable);
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj, pchName, scopeDbTable);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	// private int loopIndex = 0;

	private void readLoop(Loop loop, String pchName, String scopeDbTable) {
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj, pchName, scopeDbTable);
			} else if (obj instanceof If) {
				readIf((If) obj, pchName, scopeDbTable);
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj, pchName, scopeDbTable);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}

	private void readElse(Else _else, String pchName, String scopeDbTable) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj, pchName, scopeDbTable);
			} else if (obj instanceof If) {
				readIf((If) obj, pchName, scopeDbTable);
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj, pchName, scopeDbTable);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}

	private void readAssign(Assign assign, String pchName, String scopeDbTable) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());
		if (assign.getLValue().startsWith("Lookup")
				&& assign.getLValue().contains("{")) {
			String lValue = decideLookupName(assign.getLValue(), pchName,
					scopeDbTable);
			assign.setLValue(lValue);
		}
		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj, pchName, scopeDbTable);
			}
		}
	}

	private void readExpression(Expression expression, String pchName,
			String scopeDbTable) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
			// sb.append(" expression -> " + expression.getOp());
			readBothSides(expression, pchName, scopeDbTable);
		} else {
			readContent(expression, pchName, scopeDbTable);
		}
	}

	private void readBothSides(Expression expression, String pchName,
			String scopeDbTable) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex, pchName, scopeDbTable);
			} else if (obj instanceof Bracket) {
				Bracket bracket = (Bracket) obj;
				readBracket(bracket, pchName, scopeDbTable);
			} else if (obj instanceof Function) {
				// readFunction((Function) obj );
			} else if (obj instanceof Ratetable) {
				// readRatetable((Ratetable) obj );
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	private void readContent(Expression expression, String pchName,
			String scopeDbTable) {

		Expression expressionLoc = new Expression();
		expressionLoc.getContent().addAll(expression.getContent());
		for (Object obj : expressionLoc.getContent()) {

			if (obj instanceof String) {
				String expressionContent = (String) obj;
				if (expressionContent.trim().startsWith("Lookup")
						&& expressionContent.contains("{")) {
					// THIS CODE IS TO REPLACE THE ALGORITHM CONTENT
					String expressionCont = decideLookupName(expressionContent,
							pchName, scopeDbTable);
					List<String> contentList = new ArrayList<String>();
					contentList.add(expressionCont);
					expression.getContent().clear();
					expression.getContent().addAll(contentList);
				}

			} else if (obj instanceof Expression) {
				readExpression((Expression) obj, pchName, scopeDbTable);
			} else if (obj instanceof Ratetable) {
				// readRatetable((Ratetable) obj );
			} else if (obj instanceof Function) {
				// ((Function) obj );
			}
		}
	}

	private Database dataStructureDB;

	private void buildDatabase(LOB lob) {
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				if (((Reference) ref).getName().equals("Table Objects")) {
					AlgorithmDataStructure ds = new AlgorithmDataStructure(
							(Reference) ref);
					StringBuffer xmlDS = ds.simplifyDataStrucure();

					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("DSFile", xmlDS.toString());

					try {
						this.dataStructureDB = DBFactoryBuilder.getXMLDatabase(
								BasexDatabaseNames.RUNTIMEDSDB, xmlMap);
						this.dataStructureDB.buildDatabase();
					} catch (BasexException e) {
						System.out.println("BasexException" + e.getMessage());
					} finally {
						try {
							dataStructureDB.closeDatabase();
						} catch (BasexException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}

			}
		}

	}

	private void readBracket(Bracket bracket, String pchName,
			String scopeDbTable) {
		for (Object obj : bracket.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj, pchName, scopeDbTable);
			}
		}
	}

	private Map<String, PCHInfo> lookupPCHNameAndItsReplacement = new HashMap<String, PCHInfo>();
	private Map<String, StringBuffer> lookupNamesAndReplacement = new HashMap<String, StringBuffer>();
	private Map<String, StringBuffer> pchNamesWithdomainTableLookup = new HashMap<String, StringBuffer>();
	private Map<String, StringBuffer> pchNamesWithGetDeductibleFunctionName = new HashMap<String, StringBuffer>();
	private Map<String, StringBuffer> pchNamesWithGetIntegerFunctionName = new HashMap<String, StringBuffer>();
	private Map<String, StringBuffer> pchNamesWithintegerAmount = new HashMap<String, StringBuffer>();
	private Map<String, StringBuffer> pchNamesWithDedcutibleAmount = new HashMap<String, StringBuffer>();

	private void printReport() {
		
		int counter = 0;
		System.out.println("--------------------------## CONSOLIDATED REPORT ##-------------------------------");
		for (String scopeName : lookupNamesAndReplacement.keySet()) {
			++counter;
			StringBuffer sb = lookupNamesAndReplacement.get(scopeName);
			System.out.println("--------------- SCOPE:[" + scopeName
					+ "] ---------------------");

			for (String str : sb.toString().split(",")) {
				for (String msg : str.split("~"))
					System.out.println(msg);
			}
			// String str = sb.toString().replace(",", "\n")
		}
		
		if(counter == 0){
			System.out.println("         \\,,,/");
	        System.out.println("         (o o)");
	        System.out.println("-----oOOo-(_)-oOOo-----");
	        System.out.println("----- HA-HA NO LOOKUP FUNCTION FOUND ----");
		}else{
			System.out.println("### NOTE: KEEP THIS REPORT TO VERIFY CHANGES AND FOR FUTURE REFERENCE ###");
		}
		
		System.out.println("----------------------------------------------------------------------------------");
		
	}

	@Test
	public void testFindAnnDateAndReplaceItWithXpath() {

		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			buildDatabase(lob);
			readLOB(lob);
			writeLOB(lob);
			// Printing report of the modifications done in the algorithm file
			printReport();
		} catch (Exception e) {
			System.out.println("Problem in reading algorithm file");
			e.printStackTrace();
		}

	}

	private String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\WC\\ALG-WC-CW-01012015-V02_MR_NEW\\ALG-WC-CW-01012015-V02_MR_NEW.xml";
	private static final String fileNameNEw = "D:\\\\files-by-shahsi\\\\ISO-ERC-ALG-FILES\\\\ISO-Parser-3.2.3\\\\WC\\\\ALG-WC-CW-01012015-V02_MR_NEW\\ALG-WC-CW-01012015-V02_MR_NEW_SECOND_PERIOD.xml";


	@Override
	public Object execute(LOB countryWideLOB, LOB stateWideLOB) {
		// TODO Auto-generated method stub
		return null;
	}


	
}
