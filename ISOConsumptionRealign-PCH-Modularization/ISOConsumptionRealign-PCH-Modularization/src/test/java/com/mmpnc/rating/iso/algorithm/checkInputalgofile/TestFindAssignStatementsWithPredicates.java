package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;

/**
 * @author nilkanth9581
 *
 */
public class TestFindAssignStatementsWithPredicates {
	private static Map<String,String> knownFucntions = new HashMap<String, String>();
	private Map<String,String> newFunctionsFound = new HashMap<String, String>();
	
	static {
		knownFucntions.put("domainTableLookup", "domainTableLookup");
	}
	
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public void readLOB(LOB lob) {
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref );
				}
				
			}
		}
	}
	
	
	

	private void readReference(Reference reference ) {
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope)scope;
				readScope(curScope );
				//currentScopeDB.closeDatabase();
			}
		}
	}
	
	
	private void readScope(Scope scope ) {
		//setting the current scope path 
		currentScopePath = scope.getDbTables();
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj );
			} else if (obj instanceof If) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readIf((If) obj );
			} else if (obj instanceof Assign) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readLoop((Loop) obj );
			} else if (obj instanceof Class) {
			}
		}
	}
	
	private void readPCH(PCH pch ) {

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj );
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	}
	
	
	private void readIf(If _if  ) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj );
			} else if (obj instanceof Then) {
				readThen((Then) obj );
			} else if (obj instanceof Else) {
				readElse((Else) obj );
			}
		}
	}

	private void readCondition(Condition condition ) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,false );
			}
		}
	}

	private void readThen(Then then  ) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj  );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	
	//private int loopIndex = 0;
	private String currentScopePath = null;
	
	private void readLoop(Loop loop  ) {
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void readElse(Else _else ) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}


	private void readAssign(Assign assign ) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj , true );
			}
		}
	}

	private void readExpression(Expression expression,boolean fromAssign ) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
//			sb.append(" expression -> " + expression.getOp());
			readBothSides(expression,fromAssign );
		} else {
			readContent(expression ,fromAssign );
		}
	}

	private void readBothSides(Expression expression ,boolean fromAssign  ) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex,fromAssign );
			} else if (obj instanceof Function) {
				//readFunction((Function) obj );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	private void readContent(Expression expression,boolean fromAssign ) {
		for (Object obj : expression.getContent()) {
			
			if (obj instanceof String) {
				String expressionContent = (String) obj;
				if(fromAssign && expressionContent.contains("[")){
					//System.out.println("SCOPE:["+currentScopePath+"] FUNCTION USED :["+expressionContent+"]");
					newFunctionsFound.put(expressionContent, currentScopePath);
				}
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj ,false );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
			} else if (obj instanceof Function) {
				//((Function) obj );
			}
		}
	}

				

	//private static final String fileName = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-MD-09012012-V01/ALG-BP-MD-09012012-V01_MR.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\ALG-CA-NY-10012012-V01_MR_REARRANGED.xml";
	/*private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";*/
	private static final String fileName ="D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-CA-NY-10012012-V01/ALG-CA-NY-10012012-V01_MR_REARRANGED.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\CA_NY_TEST.xml";
	@Test
	public void testAlgoFileForNewFunctions(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			readLOB(lob);
			System.out.println("-----------------NEW FUNCTIONS FOUND ----------------");
			for(Map.Entry<String, String> entry : newFunctionsFound.entrySet()){
				System.out.println("SCOPE:["+entry.getValue()+"] USES FUNCTION:["+entry.getKey()+"]");
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
