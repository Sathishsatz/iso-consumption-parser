package com.mmpnc.rating.iso.algorithm.algorithmtest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.content.vo.AscendantOne;

/**
 * @author nilkanth9581
 *
 */
public class Test {
	
	
	public List<String> getXpathAndfiltersList(String xpathWithPredicates){
		
		Pattern pp = Pattern.compile(".*?\\[.*?\\],?.*");
		Matcher regexMatcher = pp.matcher(xpathWithPredicates);
		List<String> xpathFilterList = new ArrayList<String>();
		
		String xPathLoc = "";
		while (regexMatcher.find()) {
				
				String filter = "";
				String str = regexMatcher.group();
				Pattern pridicatePatt = Pattern.compile("\\[.+?\\],?\\s*");
				Matcher matcher = pridicatePatt.matcher(str);
				while(matcher.find()){
					filter = matcher.group();
					
					if(xPathLoc.equals("")){
						xPathLoc = str.replace(filter, "").trim();
					}
					else{
						xPathLoc = xPathLoc + str.replace(filter, "").trim();
					}
					
					filter = filter.replace("[", "").replace("]", "");
					String xpathAndFilter = xPathLoc+":"+filter;
					xpathFilterList.add(xpathAndFilter);
				}
		}
	
		return xpathFilterList;
	
	}
	
	
	public void method(){
		//String xpath = "CCSRERequest.BOP.BOPLocation.BOPStructure.BOPClassification.ClassPropertyType";
		
	}
	
	public static void testPredicateIssue(){
		String xpath = "../BOPContrctrsInstalltnToolsAndEquipmtScheduledPropCovDetail[Limit &gt; 0]/Limit[a=b]/id";
		Pattern pp = Pattern.compile(".*?\\[.*?\\]");
		//Pattern removePre = Pattern.compile("\\[[^\\]]*\\]");
		Matcher regexMatcher = pp.matcher(xpath);
		while(regexMatcher.find()){
			String filter = regexMatcher.group();
			filter = filter.replaceAll("\\[.*?\\]", "");
			System.out.println("filter value"+filter);
		}
		
		
		
	}
	
	public static void testPredicates(){
		
		String xpath = "../State[Id=../StateId]/Name[Id=../Id]/Name";
		String predicate ="Id=../StateId";
		
		
		if(xpath.contains(predicate)){
			//String[] arr = xpath.split(predicate);
			//String currentPredicateLevel = arr[0];
			
		}
		else{
			
		}
		
		
	}
	
	private static void testDomainTableHandling(){
		JAXBContext jbContext;
		try {	
			Reader reader = new FileReader(new File("D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\HO-ALGORITHM-FILES\\ALG-HO-IL-06012012-V01_MR\\RC-HO-IL-06012012-V01\\DomaintableEntry.xml"));
			jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.content.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			AscendantOne ascendantOne = (AscendantOne) unmarshal.unmarshal(reader);
			ascendantOne.getDomainTable();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		testDomainTableHandling();
		/*testPredicates();
		testPredicateIssue();*/
		//testPredicateIssue();
		/*String xpath = "../BOPContrctrsInstalltnToolsAndEquipmtScheduledPropCovDetail[Limit &gt; 0]/Limit[a=b]/id";
		xpath = xpath.replace("/", "~");
		
		System.out.println(xpath.substring(xpath.lastIndexOf("~")));
		//SSSystem.out.println(xpath.substring(xpath.indexOff("//")));
		
		Pattern pp = Pattern.compile(".*?\\[.*?\\]");
		Matcher regexMatcher = pp.matcher(xpath);
		List<String> xpathFilterList = new ArrayList<String>();
		
		String xPathLoc = "";
		while (regexMatcher.find()) {
				
				String filter = "";
				String str = regexMatcher.group();
				Pattern pridicatePatt = Pattern.compile("\\[.+?\\],?\\s*");
				Matcher matcher = pridicatePatt.matcher(str);
				while(matcher.find()){
					filter = matcher.group();
					
					if(xPathLoc.equals("")){
						xPathLoc = str.replace(filter, "").trim();
					}
					else{
						xPathLoc = xPathLoc + str.replace(filter, "").trim();
					}
					
					filter = filter.replace("[", "").replace("]", "");
					String xpathAndFilter = xPathLoc+":"+filter;
					xpathFilterList.add(xpathAndFilter);
				}
		}
	
		
		
		for(String strs:xpathFilterList){
			System.out.println(strs);
		}
*/	
		
	
	}
}
