package com.mmpnc.rating.iso.algorithm.algorithmtest.WC;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({TestPublish_WC_NJ_Algorithms.class,
	TestPublish_WC_PA_Algorithms.class,TestPublish_WC_TexasAlgorithms.class})
public class TestSuiteForWCAlgoFiles {
	
	public void testSuite(){
		
	}
}
