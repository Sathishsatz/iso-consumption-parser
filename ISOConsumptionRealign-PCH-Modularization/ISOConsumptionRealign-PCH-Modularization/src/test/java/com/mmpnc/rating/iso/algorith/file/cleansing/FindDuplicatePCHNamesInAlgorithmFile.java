package com.mmpnc.rating.iso.algorith.file.cleansing;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlDatabase;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581 This Test case finds duplicate PCH available in the
 *         algorithm file These PCH needs to be merged or removed from algorithm
 *         file
 * 
 */
public class FindDuplicatePCHNamesInAlgorithmFile implements
		AlgoFileCleansingInterface {

	/**
	 * Map to hold base-X database names
	 */
	ConcurrentMap<String, String> basexDBNameHolder = new ConcurrentHashMap<String, String>();

	/**
	 * @UsereInput Algorithm file location
	 */

	public LOB getLOBObject(FileReader algoFileReader) {

		JAXBContext jaxbContext;
		try {

			jaxbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshaller.unmarshal(algoFileReader);

			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Database getXMLDatabase(String dbName,
			Map<String, String> xmlMap) {
		XmlDatabase db = new XmlDatabase(dbName, xmlMap);
		return db.buildDatabase();
	}

	private Database createDataBaseForLOB(LOB lob) {
		Database dsDB = null;
		try {
			String lobStr = JAXBUtils.writeFromObject(lob);

			Map<String, String> xmlMap = new HashMap<String, String>();
			String dbName = "DSFile" + new Random().nextInt();
			if (checkIfTheBasexDBPresent(dbName)) {
				dbName = dbName + new Random().nextInt();
			}
			xmlMap.put(dbName, lobStr);
			dsDB = getXMLDatabase(dbName, xmlMap);
			dsDB.buildDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dsDB;
	}

	private boolean checkIfTheBasexDBPresent(String dbName) {
		if (basexDBNameHolder.containsKey(dbName))
			return true;
		return false;
	}

	private void readLOB(LOB lob) {
		System.out.println();
		System.out.println("\t\t\t *** NOTE:These PCH needs to be removed or merged *** ");
		System.out.println();
		Database LOB_DB = null;
		try {
			LOB_DB = createDataBaseForLOB(lob);
			for (Object ref : lob.getContent()) {
				if (ref instanceof Reference) {
					Reference reference = (Reference) ref;
					// CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN
					// CALCULATION
					if ("Common Rating".equals(reference.getType())
							|| "Premium Calculation"
									.equals(reference.getType())) {
						readReference((Reference) ref, LOB_DB);
					}
				}
			}
		} finally {
			if (LOB_DB != null)
				LOB_DB.closeDatabase();

			if (duplicatePCHCounter == 0) {
				System.out.println("\t\t    !    \t\t\t\t\t     !");
				System.out.println("\t\t-< @ @ >- \t\t\t\t	 -< @ @ >-");
				System.out
						.println("\t\t  ( ^ )   NO DUPLICATE PCH FOUND IN ALGORIHM FILE  ( ^ )          ");
				System.out.println("\t\t    -     \t\t\t\t\t     -");
			}
		}

	}

	class DupPCHInfo {

		String pchName;
		int noOFOccurance;
		String pchStateAttribute;
		boolean pchOverridenAttribute;

		public String getPchName() {
			return pchName;
		}

		public void setPchName(String pchName) {
			this.pchName = pchName;
		}

		public int getNoOFOccurance() {
			return noOFOccurance;
		}

		public void setNoOFOccurance(int noOFOccurance) {
			this.noOFOccurance = noOFOccurance;
		}

		public String getPchStateAttribute() {
			return pchStateAttribute;
		}

		public void setPchStateAttribute(String pchStateAttribute) {
			this.pchStateAttribute = pchStateAttribute;
		}

		public boolean getPchOverridenAttribute() {
			return pchOverridenAttribute;
		}

		public void setPchOverridenAttribute(boolean pchOverridenAttribute) {
			this.pchOverridenAttribute = pchOverridenAttribute;
		}

	}
	//final StringBuffer duplicatePCHInScope = new StringBuffer();
	private int duplicatePCHCounter = 0;

	private void readReference(Reference reference, Database LOB_DB) {

		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Map<String, DupPCHInfo> map = new HashMap<String, DupPCHInfo>();
				Scope curScope = (Scope) scope;
				readScope(curScope, LOB_DB, reference.getType(), map);
				// currentScopeDB.closeDatabase();
				if (!map.isEmpty()) {
					StringBuffer duplicatePCHInScope = new StringBuffer();
					duplicatePCHInScope.append("\t\t   @@ SCOPE:["
							+ ((Scope) scope).getDbTables() + "]  TYPE:["
							+ reference.getType() + "] @@");
					duplicatePCHInScope.append("\n");

					for (String key : map.keySet()) {
						if (key.equals("GetDeductible")
								|| key.equals("deductibleAmount")
								|| key.equals("domainTableLookup")
								|| key.equals("GetInteger")
								|| key.equals("itegerAmount"))
							// as these pch will be ignored by consumption
							// parser
							// it is not required to add in the duplicate PCH
							// name list
							duplicatePCHInScope
									.append("\t\t\t NOTE:This Duplicate PCH[")
									.append(key)
									.append("] will be ignore by consumpion parser no need of manually modify it")
									.append("\n");

						++duplicatePCHCounter;

						DupPCHInfo dupPCHInfo = map.get(key);
						duplicatePCHInScope.append("\t\t\t PCH NAME:[");
						duplicatePCHInScope
								.append(dupPCHInfo.getPchName())
								.append("] NUMBER OF APPEARENCE:["
										+ dupPCHInfo.getNoOFOccurance() + "]")
								.append(" PCH-STATE:["
										+ dupPCHInfo.getPchStateAttribute()
										+ "]")
								.append(" PHC-OVERRIDDEN:["
										+ dupPCHInfo.getPchOverridenAttribute()
										+ "]").append("\n");
					}
					System.out.println(duplicatePCHInScope);
					System.out.println();
				}
			}
		}
	}

	private void readScope(Scope scope, Database dsDB, String referenceType,
			Map<String, DupPCHInfo> map) {
		// setting the current scope path
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj, dsDB, scope.getDbTables(), referenceType,
						map);
			}
		}
	}

	private void readPCH(PCH pch, Database dsDB, String scopeDbTable,
			String referenceType, Map<String, DupPCHInfo> map) {

		StringBuffer queryString = new StringBuffer();
		queryString.append("count(//reference[@type='" + referenceType
				+ "']/scope[@dbTables='" + scopeDbTable + "']/PCH[@name='"
				+ pch.getName() + "'])");
		Connection tableObjectsCon = new XMLConnection(dsDB);
		Query query = new XmlQuery(tableObjectsCon);
		query.createQuery(queryString.toString());
		Object returnObject = query.execute();
		int count = Integer.parseInt((String) returnObject);

		if (count > 1) {
			DupPCHInfo dupPCHInfo = new DupPCHInfo();
			dupPCHInfo.setPchName(pch.getName());
			dupPCHInfo.setPchOverridenAttribute(pch.getOverridden());
			dupPCHInfo.setNoOFOccurance(count);
			dupPCHInfo.setPchStateAttribute(pch.getState());
			map.put(pch.getName(), dupPCHInfo);
		}

	}

	@Override
	public Object execute(LOB countryWideLOB, LOB stateWideLOB) {
		// TODO Auto-generated method stub
		if (countryWideLOB != null){
			System.out.println("    ------------o00o-- FILE-RATEBOOKID:["+countryWideLOB.getRatebookId()+"]------o00o-------");
			readLOB(countryWideLOB);
		}
			
		if (stateWideLOB != null){
			System.out.println("    ------------o00o-- FILE-RATEBOOKID:["+stateWideLOB.getRatebookId()+"]---------o00o-------");
			readLOB(stateWideLOB);
		}
		
		return "";
	}

}
