package com.mmpnc.rating.iso.datastructure;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;

import org.junit.Assert;
import org.junit.Test;

public class DataStructureTest {

	@Test
	public void testSimplifyDataStrucure() {
		Reader reader = null;
		try {
			reader = new FileReader(
					new File(
							"C:\\Users\\shashikant460032\\Mastek\\ISO\\ISO Updates\\RC-GL-NY-07012012-V02\\RC-GL-NY-07012012-V02\\DS-GL-09012011-V02.xml"));

			Assert.assertNotNull(reader);

			DataStructure ds = new DataStructure(reader);

			Writer fileWriter = new FileWriter(new File("C:\\Users\\shashikant460032\\Mastek\\ISO\\ISO Updates\\RC-GL-NY-07012012-V02\\RC-GL-NY-07012012-V02\\SimplifiedDS.xml"));
			fileWriter.write(ds.simplifyDataStrucure().toString());
			fileWriter.flush();			
			fileWriter.close();

		} catch (Exception _ex) {
			System.out.println("error " + _ex);
		}
	}

}
