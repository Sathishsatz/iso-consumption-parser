package com.mmpnc.rating.iso.algorith.file.cleansing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mmpnc.rating.iso.algorithm.vo.LOB;

/**
 * @author Nilkanth9581
 * 
 */
public class FileCleansingRunner {

	private static final Properties properties = new Properties();

	private void loadPropertiesFile() {
		try {
			properties.load(this.getClass().getResourceAsStream(
					"iso-consumption-file-cleansing.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getProperty(PropertyKeys propertyKey) {
		return properties.getProperty(propertyKey.getValue());
	}

	AlgoFileCleansingInterface dupPCH = new FindDuplicatePCHNamesInAlgorithmFile();

	// AlgoFileCleansingInterface pchNameMod = new Algo

	private void cleasingStateWideAlgoFile(LOB countryWideLOB, LOB stateWideLOB) {

		// step 1:
		// find duplicate PCH

		// step 2:
		// Find and replace PCH names with xpaths and lookup functions

		// step 3:
		// Find PCH in state which are not present in country wide file

		// step 4:
		// Inherited PCH with state specific steps

	}

	private void cleanseCountrywideAlgoFile(LOB countryWideLOB) {
		// step 1:
		// find duplicate PCH

		// step 2:
		// Find and replace PCH with xpaths in its names

	}

	private static final FindDuplicatePCHNamesInAlgorithmFile DUPLICATE_PCH_NAMES_IN_ALGORITHM_FILE = new FindDuplicatePCHNamesInAlgorithmFile();

	private void cleaseBothAlgoFiles(LOB countryWideLOB, LOB stateWideLOB) {

		System.out
				.println("---------------o00o- #### Starting process: Find Duplicate PCH in Algorithm file #### -o00o-----------------");
		System.out.println();
		// step 1:
		// find duplicate PCH
		// getting both LOBS
		StringBuffer report = (StringBuffer) DUPLICATE_PCH_NAMES_IN_ALGORITHM_FILE
				.execute(countryWideLOB, stateWideLOB);

		System.out.println();
		System.out
				.println("---------------o00o- ##### Ending process : Find Duplicate PCH in Algorithm file #### -o00o--------------------");

		// step 2:
		// find and replace PCH names with xpaths and lookup functions

		// step 3:
		// Find PCH in state which are not present in country wide file

		// step 4:
		// Inherited PCH with state specific steps

	}

	private LOB getLOBObject(String algoFileLocation)
			throws FileNotFoundException {

		Reader algoFileReader = new FileReader(new File(algoFileLocation));
		JAXBContext jaxbContext;
		try {

			jaxbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshaller.unmarshal(algoFileReader);
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			System.err.println("UNABLE TO MAP ALGO FILE WITH JAXB LOB OBJECT "
					+ algoFileLocation);
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("EROR WHILE READING ALGORITHM FILE "
					+ algoFileLocation);
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) throws FileNotFoundException {
		System.out.println("         				   \\,,,/");
		System.out.println("         				   (o o)");
		System.out
				.println("---------------------------------------oOOo-(_)-oOOo-------------------------------------------------------------");
		System.out
				.println("  -------o00o--- STARTING CLEANSING PROCESS SIT BACK AND WAIT TILL THIS PROCESS COMPLETES !! ----o00o---");
		System.out.println();
		System.out.println();
		FileCleansingRunner runner = new FileCleansingRunner();
		runner.loadPropertiesFile();

		String stateWideFileLocation = runner
				.getProperty(PropertyKeys.state_wide_file_location);
		String countryWideFileLocation = runner
				.getProperty(PropertyKeys.country_wide_file_location);

		LOB stateWideLOB = runner.getLOBObject(stateWideFileLocation);
		LOB countryWideLOB = runner.getLOBObject(countryWideFileLocation);

		String cleaseFiles = runner.getProperty(PropertyKeys.file_to_cleanse);
		if ("both".equalsIgnoreCase(cleaseFiles)) {
			runner.cleaseBothAlgoFiles(countryWideLOB, stateWideLOB);

		} else if ("countryWide".equalsIgnoreCase(cleaseFiles)) {
			runner.cleanseCountrywideAlgoFile(countryWideLOB);
		} else if ("stateWide".equalsIgnoreCase(cleaseFiles)) {
			runner.cleasingStateWideAlgoFile(countryWideLOB, stateWideLOB);
		}

		System.out
				.println("  -------o00o---------------  CLEANSING PROCESS COMPLETES SUCCESSFULLY !! -----------------------o00o---");

	}

}
