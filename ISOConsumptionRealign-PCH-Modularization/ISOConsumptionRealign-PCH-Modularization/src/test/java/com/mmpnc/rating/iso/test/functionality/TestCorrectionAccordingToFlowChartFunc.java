package com.mmpnc.rating.iso.test.functionality;

import org.junit.Test;


public class TestCorrectionAccordingToFlowChartFunc {
	
	
	/**
	 * This method checks duplicate entries in an array and create a new array with 
	 * unique pch names
	 * 
	 */
	public void checkDuplicatePCHNames(String arr[]){
		
		StringBuffer pchName = new StringBuffer();
		for(int i=0;i<arr.length;i++){
			boolean isduplicate = false;
			
			for(int j=i+1;j<arr.length;j++){
				if(arr[i].equals(arr[j])){
					isduplicate = true;
					break;
				}
			}
			if(!isduplicate){
				pchName.append(arr[i]).append(" ");
				
			}
		}
		String newArr[] = pchName.toString().split(" ");
		
		for(String pchNameStr:newArr){
			System.out.println(pchNameStr);
		}
	}
	
	@Test
	public void testCorrectionAccordingToflowChartFunctionality(){
		String arr[] = new String[]{"Premium","Premium","HiredAutoFactor","AccidentPreventionDiscountFactor","FleetFactor","PIPFactor","PIPFactor"};
		//pch name array with duplicates
		checkDuplicatePCHNames(arr);
		
		String arrWithNoDuplicate[] = new String[] {"Premium","HiredAutoFactor","AccidentPreventionDiscountFactor","FleetFactor","PIPFactor"};
		//pch name array without duplicates
		checkDuplicatePCHNames(arrWithNoDuplicate);
	}
	
}
