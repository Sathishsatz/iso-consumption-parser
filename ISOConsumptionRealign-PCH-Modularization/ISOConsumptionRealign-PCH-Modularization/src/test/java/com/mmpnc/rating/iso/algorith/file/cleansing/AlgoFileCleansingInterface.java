package com.mmpnc.rating.iso.algorith.file.cleansing;

import com.mmpnc.rating.iso.algorithm.vo.LOB;

/**
 * @author Nilkanth9581
 *
 */
public interface AlgoFileCleansingInterface {
	
	public Object execute(LOB countryWideLOB,LOB stateWideLOB);

}
