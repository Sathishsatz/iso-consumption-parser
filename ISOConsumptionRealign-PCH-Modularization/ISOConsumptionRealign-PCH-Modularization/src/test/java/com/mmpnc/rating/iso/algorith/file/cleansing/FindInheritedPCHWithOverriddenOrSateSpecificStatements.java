package com.mmpnc.rating.iso.algorith.file.cleansing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;

/**
 * 
 * @author nilkanth9581 
 * In some cases we found that a algorithm file has some state specific 
 * statements in an inherited PCH.
 * As the pch is inherited consumption parser will ignore that PCH
 * from processing 
 * We need to find such PCH and set there overridden attribute as true
 * before processing this file by consumption parser
 * 
 */
public class FindInheritedPCHWithOverriddenOrSateSpecificStatements {


	
	/**
	 *  Algorithm file location  
	 *  @UserInput
	 */
	String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\WC\\RC-WC-TX-07012015-V02\\ALG-WC-TX-07012015-V02_MR-NEW.xml";

	@Test
	public void testAlgoFileForPCh() throws Exception {

		checkSingleAlgorithmFile(fileName);

	}
	
	public LOB getLOBObject(FileReader algoFileReader) {

		JAXBContext jaxbContext;
		try {

			jaxbContext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshaller.unmarshal(algoFileReader);
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private void printReport(){
		
		int pchCounter = 0;
		System.out.println("------------------------### Consolidated Report ###--------------------- ");
		System.out.println();
		System.out.println("### Inherited PCH With Overridden Or State Specific Statements###");
		System.out.println("NOTE: These PCH needs to be marked as overriden true ");
		System.out.println();
		for(ScopeInfo scopeInfo:scopeInfos){
			Map<String, PCHInfo> map = scopeInfo.getPCHInfoMap();
			
			for(String pchName:map.keySet()){
				PCHInfo pchInfo = (PCHInfo)map.get(pchName);
				if(pchInfo.isHasStateSpecifcStmts()){
					++pchCounter;
					System.out.println("Scope :["+scopeInfo.getScopeDbTable()+"] type:["+scopeInfo.getReferenceType()+"]" +
							"PCH name:["+pchInfo.getPchName()+"]");	
				}
				
			}
		}
		
		if(pchCounter == 0){
			System.out.println("\t\t    !    \t\t\t\t\t     !");
			System.out.println("\t\t-< @ @ >- \t\t\t\t	 -< @ @ >-");
			System.out.println("\t\t  ( ^ )   NO SUCH PCH FOUND IN ALGORIHM FILE       ( ^ )          ");
			System.out.println("\t\t    -     \t\t\t\t\t     -");
		}
		System.out.println();
		System.out.println("### Inherited PCH With Overridden Or State Specific Statements###");
		System.out.println();
		System.out.println("------------------------### Consolidated Report ###--------------------- ");
	}
	
	private void readLOB(LOB lob){
		List<Object> content = lob.getContent();
		
		for(Object obj:content){
			if(obj instanceof Reference){
				Reference reference = (Reference)obj;
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference(reference);
				}
			}
		}
		printReport();
	}
	
	List<ScopeInfo> scopeInfos = new ArrayList<ScopeInfo>();
	
	private void readReference(Reference reference) {

		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object obj : scopeList) {
			if (obj instanceof Scope) {
				Scope scope = (Scope)obj;
				ScopeInfo scopeInfo = new ScopeInfo();
				scopeInfo.setReferenceType(reference.getType());
				scopeInfo.setScopeDbTable(scope.getDbTables());
				scopeInfos.add(scopeInfo);
				readScope(scope, scopeInfo);
			}
		}
	}

	private void readScope(Scope scope,ScopeInfo scopeInfo) {
		// setting the current scope path
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				PCH pch = (PCH)obj;
				if(!pch.getOverridden() && pch.getState().equals("CW")){
					PCHInfo pchInfo = new PCHInfo();
					pchInfo.setPchName(pch.getName());
					pchInfo.setStatementExcelSheetNo(pch.getExcelSheet());
					scopeInfo.putInPCHInfo(pchInfo);
					readPCH((PCH) obj,scopeInfo);
				}
				
			}
		}
	}

	
	private void readPCH(PCH pch, ScopeInfo scopeInfo) {
		
		for (Object statement : pch.getContent()) {
			readContent(statement, scopeInfo,pch.getName());
		}
	}
	
	private void readIf(If ifSatement,ScopeInfo scopeInfo,String pchName){
		if(!ifSatement.getState().equals("CW")){
			PCHInfo pchInfo = scopeInfo.getFromPchInfoMap(pchName);
			pchInfo.setHasStateSpecifcStmts(true);
			//no need to read remaining statements as we have found one overridden satement
			
		}else{
			List<Object> ifcontent = ifSatement.getContent();
			for(Object content:ifcontent){
				readContent(content, scopeInfo, pchName);
			}
		}
	}
	
	private void readContent(Object content, ScopeInfo scopeInfo,String pchName)
			{
		// if(!checkEmptyStatementBlock(content))
		if (content instanceof If) {
			readIf((If) content,scopeInfo,pchName );
		} else if (content instanceof Condition) {
			//now required to process the condition as 
			//readCondition((Condition) content,scopeInfo,pchName);
		} else if (content instanceof Then) {
			readThen((Then) content, scopeInfo,pchName);
		} else if (content instanceof Else) {
			readElse((Else) content, scopeInfo,pchName);
		} else if (content instanceof Assign) {
			readAssign((Assign) content, scopeInfo,pchName);
		} else if (content instanceof Loop) {
			readLoop((Loop) content, scopeInfo,pchName);
		} else if (content instanceof Expression) {
			readExpression((Expression) content, scopeInfo,pchName);
		} else if (content instanceof String) {
			//do nothing
		}
	
	}
	
	private void readThen(Then then,ScopeInfo scopeInfo,String pchName) {
		List<Object> contentList =  then.getContent();
		for(Object content:contentList){
			readContent(content, scopeInfo, pchName);
		}
	}
	
	private void readElse(Else elseStmt,ScopeInfo scopeInfo,String pchName) {
		List<Object> contentList =  elseStmt.getContent();
		for(Object content:contentList){
			readContent(content, scopeInfo, pchName);
		}
	}
	
	private void readAssign(Assign assign,ScopeInfo scopeInfo,String pchName) {
		if(!assign.getState().equals("CW")){
			PCHInfo pchInfo = scopeInfo.getFromPchInfoMap(pchName);
			pchInfo.setHasStateSpecifcStmts(true);
		}
	}

	private void readLoop(Loop loop,ScopeInfo scopeInfo,String pchName){
		if(!loop.getState().equals("CW")){
			PCHInfo pchInfo = scopeInfo.getFromPchInfoMap(pchName);
			pchInfo.setHasStateSpecifcStmts(true);
		}else{
			List<Object> conteList = loop.getContent();
			for(Object content:conteList){
				readContent(content, scopeInfo, pchName);
			}
		}
	}
	
	private void readExpression(Expression expression,ScopeInfo scopeInfo,String pchName){
		List<Object> expressions = expression.getContent();
		for(Object content:expressions){
			readContent(content, scopeInfo, pchName);
		}
	}
	
	

	private void checkSingleAlgorithmFile(String fileName) {
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			readLOB(lob);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This inner class will hold PCH related information
	 * 
	 */
	class PCHInfo {

		String pchName;
		String statementExcelSheetNo;
		String scopeDbTable;
		String refernceType;
		boolean hasStateSpecifcStmts;

		public boolean isHasStateSpecifcStmts() {
			return hasStateSpecifcStmts;
		}

		public void setHasStateSpecifcStmts(boolean hasStateSpecifcStmts) {
			this.hasStateSpecifcStmts = hasStateSpecifcStmts;
		}

		public String getStatementExcelSheetNo() {
			return statementExcelSheetNo;
		}

		public void setStatementExcelSheetNo(String statementExcelSheetNo) {
			this.statementExcelSheetNo = statementExcelSheetNo;
		}

		public String getPchName() {
			return pchName;
		}

		public void setPchName(String pchName) {
			this.pchName = pchName;
		}

	}
	
	class ScopeInfo{
		
		String scopeDbTable;
		String referenceType;
		Map<String, PCHInfo> pchInfoMap = new ConcurrentHashMap<String, PCHInfo>();
		
		public String getScopeDbTable() {
			return scopeDbTable;
		}
		public void setScopeDbTable(String scopeDbTable) {
			this.scopeDbTable = scopeDbTable;
		}
		public String getReferenceType() {
			return referenceType;
		}
		public void setReferenceType(String referenceType) {
			this.referenceType = referenceType;
		}
		
		public void putInPCHInfo(PCHInfo pchInfo){
			pchInfoMap.put(pchInfo.getPchName(), pchInfo);
		}
		
		public PCHInfo getFromPchInfoMap(String pchName){
			return pchInfoMap.get(pchName);
		}
		
		public Map<String, PCHInfo> getPCHInfoMap(){
			return pchInfoMap;
		}
	}
	
}
