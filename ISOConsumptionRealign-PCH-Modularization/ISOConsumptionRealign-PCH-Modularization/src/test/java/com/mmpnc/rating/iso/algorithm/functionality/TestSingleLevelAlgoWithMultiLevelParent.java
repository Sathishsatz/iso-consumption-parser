package com.mmpnc.rating.iso.algorithm.functionality;

import org.junit.Test;

/**
 * @author nilkanth9581
 * THIS TEST CASE HAS A FUNCTIONALITY TO FIND DIFFERENT LEVEL OF AN ALGORITHM 
 */
public class TestSingleLevelAlgoWithMultiLevelParent {
	
	public void extractMultipleLevelsOfAlgo(String xpath1,String xpath2){
			//CONSIDERING XPATH CONTAINS MORE THAN ONE ELEMENTS
		String [] xpath1arr = xpath1.split("\\.");
		String [] xpath2arr = xpath2.split("\\.");
		int arrlength = xpath1arr.length>xpath2arr.length ?xpath1arr.length:xpath2arr.length;
		int xpathDiffIndex = 0;
		
		for(int i=0;i<arrlength;i++){
			if(xpath1arr[i].equals(xpath2arr[i])){
				xpathDiffIndex = xpathDiffIndex+xpath1arr[i].length()+1;
				continue;
			}else{
				//xpathDiffIndex = xpathDiffIndex+1;
				break;
			}
		}
		
		String xpath = xpath1.substring(xpathDiffIndex);
		String secondXapth = xpath2.substring(xpathDiffIndex);
		System.out.println("xpath:["+xpath+"]");
		System.out.println("xpath:["+secondXapth+"]");
		
			
	}
	
	
	@Test
	public void testHowToFindMultiLevel(){
		String xpath1 = "Policy.CommercialAutoGarage.CommercialAutoAdditionalPersonalInjuryProtectionNewYork.CommercialAutoAdditionalPersonalInjuryProtectionNewYorkAddedCoverage";
		String xpath2 = "Policy.CommercialAuto.CommercialAutoPrivatePassenger.CommercialAutoAdditionalPersonalInjuryProtectionNewYork.CommercialAutoAdditionalPersonalInjuryProtectionNewYorkAddedCoverage";
		extractMultipleLevelsOfAlgo(xpath1, xpath2);
		
	}
}
