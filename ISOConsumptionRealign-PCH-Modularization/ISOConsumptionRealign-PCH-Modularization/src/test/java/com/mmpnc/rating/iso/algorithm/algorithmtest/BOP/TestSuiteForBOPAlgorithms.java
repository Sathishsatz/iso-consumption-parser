package com.mmpnc.rating.iso.algorithm.algorithmtest.BOP;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestPublishMDAlgorithms.class,
		TestPublishPSAlgorithms.class,
		//TestPublishTexasAlgorithms.class,
		TestPublishWAAlgorithms.class})
public class TestSuiteForBOPAlgorithms {
	
}
