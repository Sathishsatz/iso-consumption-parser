package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;

/**
 * @author nilkanth9581
 *	THIS TEST CASE WILL FIND ALL THE FUNCTIONS USED IN THE ALGORTIHM FILE
 *
 */
public class TestFindNewFunctionsUsedInAlgorithmFile {

	private static Map<String,String> knownFucntions = new HashMap<String, String>();
	private Map<String,String> newFunctionsFound = new HashMap<String, String>();
	private boolean doUWantToWriteModifiedFile = false;
	private boolean isThereAnyLookupFunction = false;
	
	static {
		knownFucntions.put("domainTableLookup", "domainTableLookup");
	}
	
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void writeModifiedLOBObject(LOB lob) {
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(lob, new File(modifiedFilename));
			FileOutputStream fileOutputStream = new FileOutputStream(new File(modifiedFilename));
			fileOutputStream.write(JAXBUtils.writeFromObject(lob).getBytes());	
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void readLOB(LOB lob) {
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref );
				}
				
			}
		}
		
		//WIRTING MODIFIED FILE WITH REMOVED LOOKUP FUNCTION PARAMETERS
		if(doUWantToWriteModifiedFile && isThereAnyLookupFunction)
		writeModifiedLOBObject(lob);
	}
	
	
	

	private void readReference(Reference reference ) {
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope)scope;
				readScope(curScope,reference.getType());
				//currentScopeDB.closeDatabase();
			}
		}
	}
	
	
	private void readScope(Scope scope ,String refType) {
		//setting the current scope path 
		currentScopePath = scope.getDbTables()+" Type:["+refType+"]";
		for (Object obj : scope.getContent()) {
			if (obj instanceof PCH) {
				readPCH((PCH) obj );
			} else if (obj instanceof If) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readIf((If) obj );
			} else if (obj instanceof Assign) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				//System.out.println("NOT EXPECTED THIS SITUATION");
				readLoop((Loop) obj );
			} else if (obj instanceof Class) {
			}
		}
	}
	
	private void readPCH(PCH pch ) {

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj );
			} else if (obj instanceof Loop) {
				// System.out.println("loop");
				readLoop((Loop) obj );
			} else if (obj instanceof Class) {
				// System.out.println("class");
			}
		}
	}
	
	
	private void readIf(If _if  ) {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj );
			} else if (obj instanceof Then) {
				readThen((Then) obj );
			} else if (obj instanceof Else) {
				readElse((Else) obj );
			}
		}
	}

	private void readCondition(Condition condition ) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj );
			}
		}
	}

	private void readThen(Then then  ) {
		// System.out.println("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj  );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// System.out.println("}");
	}

	
	//private int loopIndex = 0;
	private String currentScopePath = null;
	
	private void readLoop(Loop loop  ) {
		// System.out.println("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void readElse(Else _else ) {
		// System.out.println("else");
		// System.out.println("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj  );
			} else if (obj instanceof If) {
				readIf((If) obj );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// System.out.println("}");
	}


	private void readAssign(Assign assign ) {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj );
			}
		}
	}

	private void readExpression(Expression expression  ) {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
//			sb.append(" expression -> " + expression.getOp());
			readBothSides(expression );
		} else {
			readContent(expression  );
		}
	}
	
	private void readBracket(Bracket bracket){
		for(Object obj : bracket.getContent()){
			if(obj instanceof Expression){
				readExpression((Expression)obj);
			}
		}
	}

	private void readBothSides(Expression expression  ) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex );
			} 
			else if (obj instanceof Bracket){
				Bracket bracket = (Bracket)obj;
				readBracket(bracket);
			}else if (obj instanceof Function) {
				//readFunction((Function) obj );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	private void readContent(Expression expression ) {
		for (Object obj : expression.getContent()) {
			
			if (obj instanceof String) {
				String expressionContent = (String) obj;
				if(expressionContent.contains("{") && ! expressionContent.startsWith("domainTableLookup")){
					//System.out.println("SCOPE:["+currentScopePath+"] FUNCTION USED :["+expressionContent+"]");
					//Setting this flag as true so that this test case will write a new file with removed 
					//lookup parameters
					isThereAnyLookupFunction = true;
					//THIS CODE IS TO REPLACE THE ALGORITHM CONTENT
					String expressionCont = expressionContent.replaceAll("\\{.*?\\}","").trim();
					List<String> contentList = new ArrayList<String>();
					contentList.add(expressionCont);
					expression.getContent().clear();
					expression.getContent().addAll(contentList);
					
					newFunctionsFound.put(expressionContent, currentScopePath);
				}
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
			} else if (obj instanceof Function) {
				//((Function) obj );
			}
		}
	}
	
	
private void readAndReplaceLookupFunctionInExpContent(Expression expression){
		for (Object obj : expression.getContent()) {
			
			if (obj instanceof String) {
				String expressionContent = (String) obj;
				if(expressionContent.contains("{") && ! expressionContent.startsWith("domainTableLookup")){
					//System.out.println("SCOPE:["+currentScopePath+"] FUNCTION USED :["+expressionContent+"]");
					//Setting this flag as true so that this test case will write a new file with removed 
					//lookup parameters
					isThereAnyLookupFunction = true;
					//THIS CODE IS TO REPLACE THE ALGORITHM CONTENT
					String expressionCont = expressionContent.replaceAll("\\{.*?\\}","").trim();
					List<String> contentList = new ArrayList<String>();
					contentList.add(expressionCont);
					expression.getContent().clear();
					expression.getContent().addAll(contentList);
					
					newFunctionsFound.put(expressionContent, currentScopePath);
				}
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj );
			} else if (obj instanceof Ratetable) {
				//readRatetable((Ratetable) obj );
			} else if (obj instanceof Function) {
				//((Function) obj );
			}
		}
	}
	
	private void testReplaceOpeningClosingBraces(){
		
	}
				

	//private static final String fileName = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-MD-09012012-V01/ALG-BP-MD-09012012-V01_MR.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\ALG-CA-NY-10012012-V01_MR_REARRANGED.xml";
	/*private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ALG-WC-TX-06012013-V01_MR\\ALG-WC-TX-06012013-V01_MR.xml";*/
	//private static final String fileName ="D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-CA-NY-10012012-V01/ALG-CA-NY-10012012-V01_MR_REARRANGED.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-CA-NY-10012012-V01\\CA_NY_TEST.xml";
	//private static final String fileName ="D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-WC-PA-04012013-V01/ALG-WC-PA-04012013-V01_MR.xml";
	//private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\RC-HO-AZ-09012012-V01\\ALG-HO-AZ-09012012-V01_MR - REARRANGED.xml";
	//private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\ALG-GL-CA-06012013-V01_MR\\ALG-GL-CA-06012013-V01_MR.xml";
	
	//private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-WA-08012013-V01\\New folder\\ALG-BP-WA-08012013-V01_MR.xml";
	//private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\BOP-ALGORITHM-FILES\\RC-BP-CO-11012013-V01\\ALG-BP-CO-11012013-REARRANGED_V01_MR.xml";
	//String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\CA-ALGORITHMS-FILES\\RC-CA-MD-02012014-V01\\ALG-CA-MD-02012014-V01_MR.xml";
	/*String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\WC-ALGORITHM-FILES\\RB-WC-TX-09072013-V01\\ALG-WC-TX-09072013-V01_MR.xml";*/
	String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\ALG-GL-NY-10012014-V01_MR.xml";
	String modifiedFilename = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\RC-GL-NY-10012014-V01\\TEST_GL_CW_NEW_NNN.xml";
	@Test
	public void testAlgoFileForNewFunctions(){
		
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			LOB lob = getLOBObject(fileReader);
			readLOB(lob);
			Map<String,List<String>> scopeAndCustomFunction = new HashMap<String, List<String>>();
		
			System.out.println("-----------------NEW FUNCTIONS FOUND ----------------");
			for(Map.Entry<String, String> entry : newFunctionsFound.entrySet()){
				String scopeName = entry.getValue().trim();
				String custFunction = entry.getKey().trim();
				
				if(scopeAndCustomFunction.containsKey(scopeName)){
					List<String>functionList = scopeAndCustomFunction.get(scopeName);
					functionList.add(custFunction);
				}else{
					List<String>functionList = new ArrayList<String>();
					functionList.add(custFunction);
					scopeAndCustomFunction.put(scopeName, functionList);
				}
			}
			
			for(Map.Entry<String, List<String>> entry : scopeAndCustomFunction.entrySet()){
				System.out.println("--------SCOPE NAME : ["+entry.getKey()+"]----------");
				for(String functionName:entry.getValue()){
					System.out.println(functionName);
				}
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
