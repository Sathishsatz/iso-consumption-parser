package com.mmpnc.rating.iso.algorith.file.cleansing;


import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.merge.scope.IMergeScopeWithMultiplePass;
import com.mmpnc.rating.iso.algorithm.merge.scope.ImplMergeScopeWithMultiplePass;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author Nilkanth9581
 * 
 *	This case finds all those PCH whose state attribute is set to CW and overridden is set
 *  to false but these PCH are not available in the country wide algorithm file
 *  These PCH needs to be marked as overridden true as these PCH needs to
 *  be processed at state level which otherwise will be ignored by the consumption
 *  parser as these PCH are neither overridden nor state specific PCH
 *   
 */
public class FindInheritedPCHINStateWhichAreNotPresentInCountryWide {

	//constant names used for basex databases created for this test case
	private static final String SW_LOB_DATABASE = "SW_LOB_DATABASE";
	private static final String TABLE_OBJECTS = "Table Objects";
	private static final String FLOW_CHART = "FlowChart";

	/**
	 * State wide algorithm file location
	 * @UsereInput
	 */
	private static final String stateWideFileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\WC\\RC-WC-TX-07012015-V02\\ALG-WC-TX-07012015-V02_MR-NEW.xml";
	/**
	 * @UsereInput
	 * Country wide algorithm file location
	 */
	private static final String countryWideFileName ="D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\ISO-Parser-3.2.3\\WC\\RC-WC-CW-01012015-V02\\ALG-WC-CW-01012015-V02_MR_NEW.xml"; 
	
	/**
	 * This buffer will hold the scope names which are present in state wide algorithm
	 * file but they are not present in country wide algorithm file
	 *  
	 */
	private StringBuffer newScopeInStateFile = new StringBuffer();
	/**
	 * @throws Exception
	 */
	@Test
	public void testMultipleAlgorithmFiles() throws Exception {

		testMergeCWAndSWAlgorithms();

	}

	/**
	 * @throws Exception
	 */
	public void testMergeCWAndSWAlgorithms() throws Exception {

		final Reader stateAlgoReader = new FileReader(new File(
				stateWideFileName));
		final Reader countryWideAlgoeReader = new FileReader(new File(
				countryWideFileName));
		Database stateALDB = null;
		try {
			// CONVERTING INPUT ALGO FILES INTO LOB OBJECTS
			LOB stateLOB = unmarshalAlgorithmFile(stateAlgoReader);
			LOB countryWideLOB = unmarshalAlgorithmFile(countryWideAlgoeReader);

			mergeScopesWithDiffernetPass(stateLOB);
			mergeScopesWithDiffernetPass(countryWideLOB);

			stateALDB = createBasexDatabase(stateLOB, "STATE_LOB_DB",
					SW_LOB_DATABASE);
			// Database cwALDB = createBasexDatabase(countryWideLOB,
			// "COUNTRY_LOB_DB", CW_LOB_DATABASE);

			// POPULATING A MAP WITH SCOPE REFTYPE_SCOPEDBTABLE_SCOPEPASS AS KEY
			// AND SCOPE OBJECT AS VALUE
			Map<String, Object> cWScopeMap = populateAllScopeMapOfStatewideAlgo(countryWideLOB);

			StringBuffer report = new StringBuffer();
			iterateAndFindInheritedPCH(report, stateLOB, countryWideLOB,
					stateALDB, cWScopeMap);

			printReport(report);

			// WRITING ALGORITHM FILE AFTER MERGING STATE WDIE OVERRIDDEN PCH
			// WITH COUNTRYWIDE ALGORITHMS
			// writeNewLOBToFile(stateLOB,properties,stateFileName);

		} finally {
			closeBasexDatabase(stateALDB);
		}
	}

	/**
	 * This method will print report in readable format
	 */
	private void printReport(StringBuffer report){
		System.out.println("---------------------------### Consolidated Report ###------------------------------------------");
		System.out.println();
		System.out.println("### INHERITED PCH IN STATE ALGO FILE WHICH ARE NOT PRESENT IN COUNRY ALGO FILE ###");
		System.out.println();
		System.out.println("ACTION: Mark all such PCH as overridden (i.e. overridden='true')");
		System.out.println();
		if(report.length() >0){
			System.out.println(report);
		}
		else{
			System.out.println("\t\t    !    \t\t\t\t\t     !");
			System.out.println("\t\t < @ @ >  \t\t\t\t	  < @ @ > ");
			System.out.println("\t\t  ( ^ )   NO SUCH PCH FOUND IN ALGORIHM FILE       ( ^ )          ");
			System.out.println("\t\t    -     \t\t\t\t\t     -");
		}
		System.out.println("### INHERITED PCH IN STATE ALGO FILE WHICH ARE NOT PRESENT IN COUNRY ALGO FILE ###");
		System.out.println();
		System.out.println("### NEW SCOPES IN STATE WIDE ALGORIHM FILE WHICH ARE NOT PRESENT IN COUNTRYWIDE ALGORIHM FILE ###");
		System.out.println();
		System.out.println("ACTION: Please check if all the PCH in these scopes marked as overridden or state specific (i.e. state='stateName')");
		System.out.println();
		if(newScopeInStateFile.length()>0){
			System.out.println(newScopeInStateFile);
		}else{
			System.out.println("\t\t    !    \t\t\t\t\t     !");
			System.out.println("\t\t < @ @ >  \t\t\t\t	  < @ @ >");
			System.out.println("\t\t  ( ^ )   NO SUCH SCOPE FOUND IN ALGORIHM FILE      ( ^ )          ");
			System.out.println("\t\t    -     \t\t\t\t\t     -");
		}
		System.out.println("### NEW SCOPES IN STATE WIDE ALGORIHM FILE WHICH ARE NOT PRESENT IN COUNTRYWIDE ALGORIHM FILE ###");
		System.out.println();
		System.out.println("---------------------------### Consolidated Report ###------------------------------------------");
	}
	
	private void closeBasexDatabase(Database database) {
		database.closeDatabase();
	}


	private void mergeScopesWithDiffernetPass(LOB lob) {
		IMergeScopeWithMultiplePass merger = new ImplMergeScopeWithMultiplePass();
		merger.mergeScopesWithMultiplePass(lob);
	}

	/**
	 * @param lob
	 * @param properties
	 * @param stateFileName
	 * @throws Exception
	 *             WRITING STATE LEVEL ALGORITHM LOB
	 */

	private void iterateAndFindInheritedPCH(StringBuffer report,
			LOB stateWideLOB, LOB countryWideLOB, Database stateALDB,
			Map<String, Object> cWScopeMap) throws JAXBException {

		for (Object ref : stateWideLOB.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference) ref;
				String refTyep = reference.getType();
				// String refName = reference.getName();

				String refDbTable = reference.getDbTables();

				if ("Premium Calculation".equals(refTyep)
						|| "Common Rating".equals(refTyep)) {
					for (Object scopeObj : ((Reference) ref).getContent()) {

						if (scopeObj instanceof Scope) {

							// Map<String,PCH> overriddenPCHNameAndPCHObjMap =
							// new HashMap<String, PCH>();
							// THIS MAP WILL HAVE ALL THE PCH FROM STAEWIDE ALGO
							// FILE WHOSE OVERRIDEEN
							// ATTRIBUTE IS FALSE [KEY-[PCH NAME] VALUE-[PCH
							// OBEJECT]]

							Map<String, PCH> inheritedPCHNamesAndPCHObjMap = new HashMap<String, PCH>();

							Scope scope = (Scope) scopeObj;
							String dbTable = scope.getDbTables();
							short pass = scope.getPass();

							// Scope scopeWithOverrPCH =
							// getOverriddenPCHFromStateWideAlgorithmFile(refDbTable,
							// refTyep, dbTable,pass,stateALDB);
							// THIS IS MAP OF PCH WHOSE OVERRIDDEN ATTRIBUTE IS
							// FALSE

							Scope scopeWithInhertitedPCH = getInheritedPCHFromStateWideAlgorithmFile(scope);
							// IF SCOPE HAS OVERRIDDEN PCH THEN DO FOLLOWING
							// ELSE IGNORE
							if (scopeWithInhertitedPCH != null
									&& scopeWithInhertitedPCH.getContent()
											.size() > 0) {
								// THIS LOOP WILL POPULATE A MAP WITH KEY AS PCH
								// NAME AND VALUE AS PCH OBJECT ITSELF
								for (Object pchObj : scopeWithInhertitedPCH
										.getContent()) {
									if (pchObj instanceof PCH) {
										PCH pch = (PCH) pchObj;
										inheritedPCHNamesAndPCHObjMap.put(pch
												.getName().trim(), pch);
									}
								}

								// GETTING ORGINAL SCOPE FROM CONTRYWIDE
								// ALGORITHM FILE
								String cWScopeMapKey = refTyep + "_" + dbTable
										+ "_" + pass;
								Scope scopeFromCWalgo = (Scope) cWScopeMap
										.get(cWScopeMapKey);
								// REPLACE PCH FROM COUNTRYWIDE ALGO FILE AND
								// ADD IT TO
								// replaceCountrywidePCHwithOverriddenPCHFromStateAlgo(countryWideLOB,refTyep,refDbTable,overriddenPCHNameAndPCHObjMap,scopeFromCWAlgo);
								findInheritedPCHInStateWideWhichAreNotPresentInCountryWideAlgo(
										report, stateWideLOB, refTyep,
										refDbTable,
										inheritedPCHNamesAndPCHObjMap,
										scopeFromCWalgo, scope);
							}

						}

					}
				}
			}
		}
	}

	
	
	private String checkPCHInStateAlgoFile(Scope scopeFromStateWideAlgo){
		StringBuffer pchNames = new StringBuffer();
		for(Object content:scopeFromStateWideAlgo.getContent()){
			if(content instanceof PCH){
				PCH pch = (PCH) content;
				if("CW".equals(pch.getState()) && !pch.getOverridden()){
					pchNames.append(pch.getName()).append(",");
				}
			}
		}
		if(pchNames.length() == 0){
			pchNames.append(" All PCH are overridden or state specific");
		}
		return pchNames.toString();
	}
	
	
	/**
	 * This method will try to find all inherited pch in state file which are not 
	 * present in the countrywide algorithm file
	 * @param report
	 * @param stateWideLOB
	 * @param referenceType
	 * @param refDbTable
	 * @param inheritedPCHMap
	 * @param scopeFromCWAlgo
	 * @param scopeFromSWAlgo 
	 */
	private void findInheritedPCHInStateWideWhichAreNotPresentInCountryWideAlgo(
			StringBuffer report, LOB stateWideLOB, String referenceType,
			String refDbTable, Map<String, PCH> inheritedPCHMap,
			Scope scopeFromCWAlgo, Scope scopeFromSWAlgo) {

		StringBuffer buffer = new StringBuffer();
		// StringBuffer pchInMergedAlgorithmFile = new StringBuffer();
		// THIS MAP WILL HOLD PCH NAMES AND PCH OBJECTS FROM COUNTRYWIDE
		// ALGORITHM FILE
		Map<String, PCH> pchNameAndPCHinCWAlgo = new HashMap<String, PCH>();
		if (scopeFromCWAlgo == null) {
			
			newScopeInStateFile.append("SCOPE :["
					+ scopeFromSWAlgo.getDbTables() + "] reftype["
					+ referenceType + "]").append("\n");
			newScopeInStateFile.append("\t").append(checkPCHInStateAlgoFile(scopeFromSWAlgo)).append("\n");
			int counter = 0;
			for (String pchNameFromStateWideFile : inheritedPCHMap.keySet()) {
				if (!pchNameAndPCHinCWAlgo
						.containsKey(pchNameFromStateWideFile)) {
					buffer.append(++counter).append(":")
							.append(pchNameFromStateWideFile).append("\n");
				}
			}
		} else {

			for (Object scopeCont : scopeFromCWAlgo.getContent()) {
				if (scopeCont instanceof PCH) {
					PCH sWScopePCH = (PCH) scopeCont;
					pchNameAndPCHinCWAlgo.put(sWScopePCH.getName().trim(),
							sWScopePCH);
				}
			}

			int counter = 0;

			for (String pchNameFromStateWideFile : inheritedPCHMap.keySet()) {
				if (!pchNameAndPCHinCWAlgo
						.containsKey(pchNameFromStateWideFile)) {
					buffer.append(++counter).append(":")
							.append(pchNameFromStateWideFile).append("\n");
				}
			}

		}
		if (buffer.length() > 0) {
			report.append("\n");
			report.append("\n");
			report.append("--------------- scope :["
					+ scopeFromSWAlgo.getDbTables() + "] type :["
					+ referenceType + "]------------");
			report.append("\n");
			report.append(buffer.toString().trim());
		}

	}

	private Map<String, Object> populateAllScopeMapOfStatewideAlgo(
			LOB countryWideLOB) {

		Map<String, Object> scopeMap = new HashMap<String, Object>();

		for (Object lobCont : countryWideLOB.getContent()) {
			if (lobCont instanceof Reference) {
				for (Object refContent : ((Reference) lobCont).getContent()) {
					String refType = ((Reference) lobCont).getType();
					String refName = ((Reference) lobCont).getName();
					if ("Common Rating".equals(refType)
							|| "Premium Calculation".equals(refType)) {
						if (refContent instanceof Scope) {
							Scope scope = (Scope) refContent;
							String scopeDbTable = scope.getDbTables();
							int pass = scope.getPass();

							String scopeMapKey = refType + "_" + scopeDbTable
									+ "_" + pass;
							scopeMap.put(scopeMapKey, scope);

						}
					} else if (TABLE_OBJECTS.equals(refName)
							|| FLOW_CHART.equals(refName)) {
						scopeMap.put(refName, lobCont);
					}
				}
			}

		}

		return scopeMap;
	}

	/**
	 * @param algoReader
	 * @return
	 * @throws JAXBException
	 */
	private LOB unmarshalAlgorithmFile(Reader algoReader) throws JAXBException {
		JAXBContext jbContext = JAXBContext
				.newInstance("com.mmpnc.rating.iso.algorithm.vo");
		Unmarshaller unmarshal = jbContext.createUnmarshaller();
		LOB lob = (LOB) unmarshal.unmarshal(algoReader);
		return lob;
	}

	/**
	 * @param refDbTable
	 * @param refType
	 * @param scopeName
	 * @param pass
	 * @param stateWideLOBDatabsae
	 * @return
	 * @throws JAXBException
	 */
	private Scope getInheritedPCHFromStateWideAlgorithmFile(Scope stateWideScope)
			throws JAXBException {

		Scope scope = new Scope();
		scope.setDbTables(stateWideScope.getDbTables());
		scope.setPass(stateWideScope.getPass());
		scope.setRecordType(stateWideScope.getRecordType());
		scope.setRuleReference(stateWideScope.getRuleReference());
		List<Object> content = new ArrayList<Object>();

		scope.getContent().add(content);
		for (Object scopeCont : stateWideScope.getContent()) {
			if (scopeCont instanceof PCH) {
				PCH pch = (PCH) scopeCont;
				if (pch.getOverridden() == false && "CW".equals(pch.getState())) {
					scope.getContent().add(pch);
				} else {
					// NOTHING FOR NOW WILL CHECK IT LATER
				}
			}
		}
		return scope;
	}

	/**
	 * @param lob
	 * @param xmlMapName
	 * @param databaseName
	 * @return
	 */
	private Database createBasexDatabase(LOB lob, String xmlMapName,
			String databaseName) {

		String objInXml = null;
		try {
			objInXml = JAXBUtils.writeFromObject(lob);
		} catch (Exception e) {
			throw new RuntimeException("UNABLE TO MARSHAL OBJECT "
					+ e.getStackTrace());
		}
		Map<String, String> xmlLoopMap = new HashMap<String, String>();
		xmlLoopMap.put(xmlMapName, objInXml);

		return DBFactoryBuilder.getXMLDatabase(databaseName, xmlLoopMap)
				.buildDatabase();

	}

}
