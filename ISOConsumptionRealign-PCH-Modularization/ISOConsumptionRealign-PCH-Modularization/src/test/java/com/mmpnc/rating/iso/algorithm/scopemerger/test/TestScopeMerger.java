package com.mmpnc.rating.iso.algorithm.scopemerger.test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.merge.scope.IMergeScopeWithMultiplePass;
import com.mmpnc.rating.iso.algorithm.merge.scope.ImplMergeScopeWithMultiplePass;
import com.mmpnc.rating.iso.algorithm.vo.LOB;

public class TestScopeMerger {
	
	
	private static final String testCAFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\Test-cases-for-CA-MD-Algorithms-AND-CHANGES-MADE-IN-PARSER\\CA_MD_TEST_10_SCOPE_MERGING_FUNCTIONALITY.xml";
	
	private LOB createLOBObject(){
		
		try {
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshal.unmarshal(new File(testCAFile));
			return lob;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Test
	public void testScopeMerger(){
		LOB lob = createLOBObject();
		IMergeScopeWithMultiplePass merger = new ImplMergeScopeWithMultiplePass();
		merger.mergeScopesWithMultiplePass(lob);
	}
}
