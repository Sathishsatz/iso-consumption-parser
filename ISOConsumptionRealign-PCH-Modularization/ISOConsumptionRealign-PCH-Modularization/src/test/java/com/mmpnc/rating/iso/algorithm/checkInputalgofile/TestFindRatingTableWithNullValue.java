package com.mmpnc.rating.iso.algorithm.checkInputalgofile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.datastructure.vo.AscendantOne;
import com.mmpnc.rating.iso.datastructure.vo.RateTable;
import com.mmpnc.rating.iso.datastructure.vo.RateTableValue;



public class TestFindRatingTableWithNullValue {
	
	private AscendantOne unmarshalAscendantOne(Reader algoReader ){
		JAXBContext jbContext;
		AscendantOne ascendantOne = null ;
		try {
			jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.content.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
		    ascendantOne = (AscendantOne) unmarshal.unmarshal(algoReader);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return ascendantOne;
	}
	
	StringBuffer rateTablesWithoutValue = new StringBuffer();
	
	private void readAscendentOne(AscendantOne ascendantOne){
		
		List<RateTable> rateTableList = ascendantOne.getRateTable();
		for(RateTable ratetable:rateTableList){
			List<RateTableValue> rateTableValueList = ratetable.getRateTableValue();
			if(rateTableValueList == null || rateTableList.size()== 0){
					rateTablesWithoutValue.append("RATE TABLE:["+ratetable.getName()+"] DO NOT HAVE ANY VALUE").append("\n");
			}
		}
	}
	
	
	
	@Test
	public void testRatingContentFile(){
		try {
			FileReader fileReader = new FileReader(new File(fileName));
			AscendantOne ascendantOne = unmarshalAscendantOne(fileReader);
			readAscendentOne(ascendantOne);
			System.out.println(rateTablesWithoutValue);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	private static final String fileName = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\RC-GL-NY-10012014-V01\\RC-GL-NY-10012014-V01\\RC-GL-CW1-05012014-V01.xml";
	
	
}
