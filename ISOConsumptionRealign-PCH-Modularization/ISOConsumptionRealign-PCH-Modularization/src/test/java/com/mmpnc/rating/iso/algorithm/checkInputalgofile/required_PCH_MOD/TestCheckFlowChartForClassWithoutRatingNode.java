package com.mmpnc.rating.iso.algorithm.checkInputalgofile.required_PCH_MOD;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Premium;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

/**
 * @author nilkanth9581
 * This test case will print all such scopes/classes which do not have
 * rating or premium node in it
 * 
 */
public class TestCheckFlowChartForClassWithoutRatingNode {
	
	private final String txAlgoFile = "D:\\files-by-shahsi\\ISO-ERC-ALG-FILES\\GL-ALGORITHM-FILES\\3.2.2-iso-parser-files\\RC-GL-TX-04012015-V01\\RC-GL-TX-04012015-V01\\ALG-GL-TX-04012015-V01_MR_MODIFIED.xml";
	
	public LOB getLOBObject(){
		
		LOB lob = null;
		try{
			Reader algoReader = new FileReader(new File(txAlgoFile));
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			lob  = (LOB) unmarshal.unmarshal(algoReader);	
			readFlowChart(lob);
			if(classWithoutRatOrPreNodes.length()>1){
				System.out.println("----------- classes without rating and premium node---------");
				System.out.println(classWithoutRatOrPreNodes.toString());
			}
			if(classWithMultiplePremNode.length() >1){
				System.out.println("----------- classes with multiple premium node--------------");
				System.out.println(classWithMultiplePremNode.toString());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Marshalling excpetion:");
		}
		return lob;
	}

	private void readFlowChart(LOB lob){
		List<Object> content = lob.getContent();
		
		for(Object object:content){
			if(object instanceof Reference){
				Reference ref = (Reference) object;
				if("FlowChart".equals(ref.getName())){
					readReference((Reference)object);
				}
				
			}
			
		}
	}
	
	private void readReference(Reference reference){
		List<Object> refContentList = reference.getContent();
		
		for(Object refContent:refContentList){
			if(refContent instanceof Class){
				readClass((Class) refContent);
			}
		}
		
	}
	
	private StringBuffer classWithoutRatOrPreNodes = new StringBuffer();
	private StringBuffer classWithMultiplePremNode = new StringBuffer();
	
	private void readClass(Class classs){
		
		List<Object> classCont = classs.getContent();
		int premCount = 0;
		int ratingCount = 0;
		for(Object obj: classCont){
			if(obj instanceof Premium ){
				premCount = premCount +1;
			}
			if( obj instanceof Rating){
				ratingCount = ratingCount +1;
			}
			if(obj instanceof Class){
				readClass((Class)obj);
			}
		}
		
		if(premCount == 0 && ratingCount == 0){
			classWithoutRatOrPreNodes.append("Class without Rating or Premium Node:"+classs.getName()+":"+classs.getType());
			classWithoutRatOrPreNodes.append("\n");
		}
		
		if(premCount > 1){
			classWithMultiplePremNode.append("Class with mulitple Premium nodes:"+classs.getName()+":"+classs.getType());
			classWithMultiplePremNode.append("\n");
		}
		System.out.println("class Name:["+classs.getName()+"]class type:["+classs.getType()+"] premium count:["+premCount+"] rating count:["+ratingCount+"]");
	}
	
	@Test
	public void testFindRatingNodesWithoutRatingOrPremium(){
			getLOBObject();
	}


}
