package com.mmpnc.basex.database;

import java.util.HashMap;
import java.util.Map;

import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;

/**
 * @author nilkanth9581
 *
 */
public class SimplifiedDSBasexDatabase implements IBaseXDatabase<String>{

	
	private static SimplifiedDSBasexDatabase simplifiedDSBasexDatabase = null;
	
	private SimplifiedDSBasexDatabase(){
		
	}
	
	public static SimplifiedDSBasexDatabase getInstance(){
		if (simplifiedDSBasexDatabase == null){
			simplifiedDSBasexDatabase = new SimplifiedDSBasexDatabase();
		}
		return simplifiedDSBasexDatabase;
	}
	@Override
	public Database createDatabase(String t) throws BasexException {
		Map<String, String> xmlMap = new HashMap<String, String>();
		xmlMap.put("SIMPLIFIEDBASEXDATABSE",t);
		return DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEDSDB, xmlMap);
	}

}
