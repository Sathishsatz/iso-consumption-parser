package com.mmpnc.basex.database;

import java.util.HashMap;
import java.util.Map;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

/**
 * @author nilkanth9581
 *
 */
public class FlowChartBasexDatabase implements IBaseXDatabase<Reference>{

	private static FlowChartBasexDatabase flowChartBasexDatabase = null;
	
	private FlowChartBasexDatabase(){}
	
	/**
	 * @return
	 */
	public static FlowChartBasexDatabase getInstance(){
		if(flowChartBasexDatabase == null){
			flowChartBasexDatabase = new FlowChartBasexDatabase();
		}
		return flowChartBasexDatabase;
	}
	
	
	@Override
	public Database createDatabase(Reference t) throws BasexException {
		Map<String, String> xmlMap = new HashMap<String, String>();
		try {
			xmlMap.put("FLOWCHARTDATABASE", JAXBUtils.writeFromObject(t));
		} catch (Exception e) {
			ExceptionHandler.raiseBasexException("Exception while crating BASEX database", e);
		}
		return DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEFLOWCHARTDB, xmlMap);
	}
	

}
