package com.mmpnc.basex.database;

import java.util.HashMap;
import java.util.Map;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

/**
 * @author nilkanth9581
 *
 */
public class TableObjectBasexDatabase implements IBaseXDatabase<Reference>{

	private static TableObjectBasexDatabase tableObjectBasexDatabase = null;
	
	private TableObjectBasexDatabase(){}
	
	public static TableObjectBasexDatabase getInstance(){
		if(tableObjectBasexDatabase == null){
			tableObjectBasexDatabase = new TableObjectBasexDatabase();
		}
		return tableObjectBasexDatabase;
	}
	
	
	@Override
	public Database createDatabase(Reference t) throws BasexException{
		Map<String, String> xmlMap = new HashMap<String, String>();
		try {
			xmlMap.put("TABLEOBJECTSDB", JAXBUtils.writeFromObject(t));
		} catch (Exception e) {
			ExceptionHandler.raiseBasexException("Exception while creating Table Object BaseX database",e);
		}
		return DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEDSFORLOOP, xmlMap);
	}

}
