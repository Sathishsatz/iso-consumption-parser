package com.mmpnc.basex.database;

import java.util.HashMap;
import java.util.Map;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public class ScopeBasexDatabase implements IBaseXDatabase<Scope>{

	
	private static ScopeBasexDatabase scopeBasexDatabase = null;
	
	private ScopeBasexDatabase(){}
	
	/**
	 * @return
	 */
	public static ScopeBasexDatabase getIntance(){
		if(scopeBasexDatabase == null){
			scopeBasexDatabase = new ScopeBasexDatabase();
		}
		return scopeBasexDatabase;
	}
	
	
	@Override
	public Database createDatabase(Scope t) throws BasexException{
		Map<String, String> xmlMap = new HashMap<String, String>();
		try {
			xmlMap.put("SCOPEDATABASE", JAXBUtils.writeFromObject(t));
		} catch (Exception e) {
			ExceptionHandler.raiseBasexException("Exception while creating SCOPE BaseX database",e);
		}
		return DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.CURRENTSCOPEDB, xmlMap);
	}

}
