package com.mmpnc.basex.database;

import java.util.HashMap;
import java.util.Map;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.vo.LOB;

/**
 * @author nilkanth9581
 *
 */
public class LOBBasexDatabase implements IBaseXDatabase<LOB>{

	private static LOBBasexDatabase lobBasexDatabase = null;
	
	private LOBBasexDatabase(){
		
	}
	
	public static LOBBasexDatabase getInstance(){
		if(lobBasexDatabase == null){
			lobBasexDatabase = new LOBBasexDatabase();
		}
		return lobBasexDatabase;
	}
	
	
	@Override
	public Database createDatabase(LOB t) throws BasexException {
		Map<String, String> xmlMap = new HashMap<String, String>();
		try {
			xmlMap.put("LOBDATABASE", JAXBUtils.writeFromObject(t));
		} catch (Exception e) {
			ExceptionHandler.raiseBasexException("Exception while creating LOB BaseX database",e);
		}
		return DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMELOBDATABASE, xmlMap);
	}

}
