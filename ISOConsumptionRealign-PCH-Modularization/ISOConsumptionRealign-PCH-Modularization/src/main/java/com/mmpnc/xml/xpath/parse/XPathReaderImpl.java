package com.mmpnc.xml.xpath.parse;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import com.mmpnc.rating.iso.algorithm.exception.XPathParserException;
import com.mmpnc.xml.xpath.parse.XPathLexer;
import com.mmpnc.xml.xpath.parse.XPathParser;
import com.mmpnc.xml.xpath.parse.XPathParser.xpath_return;


public class XPathReaderImpl implements XPathActivity {
	private StringBuffer buffer;

	public XPathReaderImpl(StringBuffer buffer) {
		this.buffer = buffer;
	}
	@Override
	public Object execute() throws XPathParserException {
		CharStream stream = new ANTLRStringStream(buffer.toString());
		XPathLexer lexer = new XPathLexer(stream);
		XPathParser parser = new XPathParser(new CommonTokenStream(lexer));
		
		CommonTreeNodeStream commontreenode;
		try {
			xpath_return xpath = parser.xpath();
			commontreenode = new CommonTreeNodeStream(xpath.tree);
//			System.out.println("XML Tree after parsing -> " + xpath.tree.toStringTree());
			return commontreenode;
		} catch (RecognitionException e) {
			throw new XPathParserException(e.getMessage());
		}
	}

}
