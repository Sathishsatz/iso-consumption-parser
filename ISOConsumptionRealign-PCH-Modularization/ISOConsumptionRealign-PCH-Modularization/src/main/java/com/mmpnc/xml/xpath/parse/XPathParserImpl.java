package com.mmpnc.xml.xpath.parse;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.TreeNodeStream;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class XPathParserImpl implements XPathActivity {

	private Context context;
	private StringBuffer buffer;

	public XPathParserImpl(Context context, StringBuffer buffer) {
		this.context = context;
		this.buffer = buffer;
	}
	
	@Override
	public Object execute() throws Exception {
		XPathActivity reader = new XPathReaderImpl(buffer);	
		TreeNodeStream nodeStream = (TreeNodeStream) reader.execute();
		
		XPathWalker tree = new XPathWalker(
				nodeStream);
		Evaluator.Type type = null;
		try {
//			System.out.println(tree);
			type = tree.xpath(context);
		} catch (RecognitionException e) {
			throw new ParserException(e.getMessage());
		}
		
		return type;
	}

}
