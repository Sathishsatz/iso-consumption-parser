package com.mmpnc.xml.xpath.parse;

public interface XPathActivity {
	Object execute() throws Exception;
}
