package com.mmpnc.context;

public interface Context {
	Object getValue(String key);
	void putValue(String key, Object value);
	Object getValue(ContextParam key);
	void putValue(ContextParam key, Object value);
	void remove(ContextParam key);
}
