package com.mmpnc.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.LookupModels;

/**
 * @author nilkanth9581
 * Setting all the required parameters for new context
 * 
 */
public class ContextInitializer {
	
	static int localvaraibleCount = 1;
	
	/**
	 * @param context
	 * @return
	 */
	
	private static Map<String,String>aliasNameMap =  new HashMap<String, String>();
	
	public static void intializeContext(Context context){
		//context.putValue(ContextParam.LOCALVARIABLECOUNTER, localvaraibleCount);
		context.putValue(ContextParam.STEPS, new ArrayList<AlgoStep>());
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		context.putValue(ContextParam.GLOBALLOOKUPMODLELIST,new LookupModels());
		//THIS MAP IS ADDED TO HANDLE THE LOCAL VAIABLES ALIAS NAMES
		context.putValue(ContextParam.ALIASNAMEMAP,aliasNameMap);
		context.putValue(ContextParam.DAYS_DIFF_FUNCTION, false);
	}

}
