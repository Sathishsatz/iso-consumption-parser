package com.mmpnc.util;

/**
 * @author Nilkanth9581
 *
 */
public enum VariableType {
	COLUMN_NUMERIC, 
	COLUMN_STRING, 
	UI, 
	RT,
	CONSTANT, 
	LOCAL, 
	LV_NUMERIC, 
	LV_STRING, 
	LV_DOUBLE, 
	LV_INTEGER, 
	LV_BOOLEAN, 
	LV_TIMESPAN, 
	LV_DATE, 
	LV_VALUE, 
	XPATH, 
	XPATH_STRING, 
	XPATH_NUMERIC;
}