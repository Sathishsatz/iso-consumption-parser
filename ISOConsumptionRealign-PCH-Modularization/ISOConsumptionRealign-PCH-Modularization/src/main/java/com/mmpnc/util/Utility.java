package com.mmpnc.util;

import java.util.List;

public class Utility {

	// update common object list

	public static <T> T updateList(List<T> list, T object) {
		for (T t : list) {
			if (t.equals(object)) {
				return t;
			}
		}
		list.add(object);
		return object;
	}

	public static <T> boolean isPresent(List<T> list, T object) {
		for (T t : list) {
			if (t.equals(object)) {
				return true;
			}
		}
		return false;
	}

	public static <T> T getObject(List<T> list, T object) {
		for (T t : list) {
			if (t.equals(object)) {
				return t;
			}
		}
		return null;
	}
}
