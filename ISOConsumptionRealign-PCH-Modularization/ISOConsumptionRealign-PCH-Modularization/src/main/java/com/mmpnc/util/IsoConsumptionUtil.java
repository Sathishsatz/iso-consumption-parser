package com.mmpnc.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class IsoConsumptionUtil {
	
	private static Properties prop;
	
	private static final String propFileName = "/iso-consumption.properties";
	
	private static IsoConsumptionUtil isoConsumptionUtil;
	
	public static IsoConsumptionUtil getInstance(){
		if(isoConsumptionUtil==null){
			isoConsumptionUtil = new IsoConsumptionUtil();
			loadISOConsumptionProp();
		}
		return isoConsumptionUtil;
	}
	
	private static void loadISOConsumptionProp(){
		if(prop==null){
			prop=new Properties();
			try {
				prop.load(IsoConsumptionUtil.class.getResourceAsStream(propFileName));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getProperty(String key){
		return prop.getProperty(key);
	}
	
}
