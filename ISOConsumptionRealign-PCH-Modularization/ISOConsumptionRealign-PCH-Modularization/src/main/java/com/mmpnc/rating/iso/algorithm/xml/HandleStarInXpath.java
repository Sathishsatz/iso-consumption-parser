package com.mmpnc.rating.iso.algorithm.xml;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

/**
 * @author nilkanth9581
 *
 */
public class HandleStarInXpath {
	
	/**
	 * @param loop
	 * @param context
	 * @return
	 */
	public static String handlehStarInXpath(String xpathToChange,Context context){
		
		//THIS CONDITION IS ADDED TO HANDLE XPATH AS BELOW 
		//ancestor::*[name(.)='CommercalAuto' and not(@offset='Deleted')]/*[not(@offset='Deleted')]/VehicleNumber
		//THIS xpath must change to following xpath
		//current scope/CommercialAuto/VehicleNumber
		//Same this is applied for if and assign conditions which have /* xpath in it
			if(xpathToChange.contains("/*[")){
				
				String [] xpathAttrArr = xpathToChange.split("/");
				StringBuffer changedXpath = new StringBuffer();
				for(String xpathAttr : xpathAttrArr){
					if(!xpathAttr.startsWith("ancestor") && ! xpathAttr.equals("*[not(@offset='Deleted')]")){
						//changedXpath.append(xpathAttr.replace("ancestor::*[name(.)=", "").split(" ")[0].replace("'", ""));
						changedXpath.append(xpathAttr).append("/");
					}
					//THIS condition is added because in CA algorithm
					//xpath  ../*[not(@offset='Deleted')]/UnitNumber is getting converted to ../UnitNumber 
					//we need above xpath to get converted to ../*/UnitNumber
					else if(xpathAttr.equals("*[not(@offset='Deleted')]")){
						changedXpath.append("*").append("/");
					}
					/*else if(){
						//changedXpath.append("/").append(scopePath);
					}else{
						
						
					}*/
				}
			String changedXpathStr = changedXpath.toString();
			changedXpathStr = changedXpathStr.substring(0,changedXpathStr.lastIndexOf("/"));
			ISOConsumptionLogger.info("loop through original stmt:["+xpathToChange+"] changed to ["+changedXpathStr+"]");
			return changedXpathStr;
			}
			
			return xpathToChange;
		
	}
	
	/*public static void main(String args[]){
		String xpathStr = "ancestor::*[name(.)='CommercialAuto' and not(@offset='Deleted')]/*[not(@offset='Deleted')]/VehicleNumber\r\n";  
		
		Loop loop = new Loop();
		loop.setThrough(xpathStr);
		ContextBase context = new ContextBase();
		context.putValue(ContextParam.SCOPE, "CurrentScopeForAlgorithm");
		LoopThoroughXpathHandler.handleLoopThroughStarInXpath(loop, context);
		
		
		//Convert it to CommercialAuto/.VehicleNumebr
		//This is the condition check for multiple returned xpaths in loop through condition
		
			
	}*/
}
