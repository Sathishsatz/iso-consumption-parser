package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

/**
 * @author nilkanth9581
 * THIS CALSS WILL HANDLE  LookupLossDevelopmentFactorFunction IN THE ALGORITHM FILE 
 * THIS FUNCTION IS USED IN CA NY ALGORITHM FILE
 */
public class LookupLossDevelopmentFactorFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String varaible, Context context)throws RecognitionException {
		String lookupLossDevelopmentFactor = null;
		for(Evaluator eval : evallist){
			eval.setContext(context);
			 lookupLossDevelopmentFactor = eval.evaluate().toString();
		}
		return "LookupLossDevelopmentFactor("+lookupLossDevelopmentFactor+")";
	}

}
