package com.mmpnc.rating.iso.icmxml.creater;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.LookupNameDecider;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 * 
 */
public interface CreateRatingEngineSteps {
	
	public void createICMInputXml(String pchName,IcmRatingNodeContent icmRatingNodeContent , StringBuffer languagetext,Database dsDatabase,String scopePath,Database dsForLoop,LookupNameDecider lookupNameDecider,LocalVariableCounterHolder localVariableCounterHolder) throws Exception;
	public void createICMInputXml(IcmRatingNodeContent icmRatingNodeContent , StringBuffer languagetext,Database dsDatabase,String scopePath,Database dsForLoop) throws Exception;

}
