package com.mmpnc.rating.iso.algorithm.correction;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.util.IsoConsumptionUtil;

/**
 * @author nilkanth9581
 *
 */
public class PrepareActualVsProcessedPchReport {
	
	Map<String, HashMap<String, String>>commonRatingDataMap = new HashMap<String, HashMap<String, String>>();
	Map<String, HashMap<String, String>>premiumCalculationDataMap = new HashMap<String, HashMap<String, String>>();
	Map<String, HashMap<String, List<String>>>ovierriddenPchNamesMap = new HashMap<String, HashMap<String,List<String>>>();
	
	
	private PrepareActualVsProcessedPchReport(){}
	
	private static PrepareActualVsProcessedPchReport prepareActualVsProcessedPchReport; 
	
	
	public static PrepareActualVsProcessedPchReport getInstance(){
		if(prepareActualVsProcessedPchReport == null){
			prepareActualVsProcessedPchReport= new PrepareActualVsProcessedPchReport();
		}
		return prepareActualVsProcessedPchReport;
	}
	
	
	//private static final String fileName = "D:\\ICM\\ERC\\ERC-Files\\RC-HO-AZ-09012012-V01\\ALG-HO-AZ-09012012-V01_MR.xml";
	
	public void process(Context context,LOB lobObject) throws BasexException, IOException{
		//LOB lobObject = getLOBObject(new FileReader(new File(fileName)));
		readScope(lobObject);
		context.putValue(ContextParam.COMMONRRATING_DATA, commonRatingDataMap);
		context.putValue(ContextParam.PREMIUMCALCULATION_DATA, premiumCalculationDataMap);
	}
	
	public LOB getLOBObject(FileReader algoFileReader){
		
		JAXBContext jaxbContext;
		try {
			
			jaxbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			LOB lob = (LOB)unmarshaller.unmarshal(algoFileReader); 
			return lob;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void readScope(LOB lob){
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				String type = reference.getType();
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(type) || "Premium Calculation".equals(type)){
					List<Object> scopeList = new ArrayList<Object>();
					scopeList.addAll(reference.getContent());
					for (Object obj : scopeList) {
						if (obj instanceof Scope) {
							Scope scope = (Scope) obj;
							if("Common Rating".equals(type)){
								commonRatingDataMap.put(scope.getDbTables(), new HashMap<String,String>());
							} else if ("Premium Calculation".equals(type)){
								premiumCalculationDataMap.put(scope.getDbTables(), new HashMap<String,String>());
							}
							readPCH(scope, type);
						}
					}
				}
			}
		}
	}
	
	private void readPCH(Scope scope, String type){
		ArrayList<Object>pchList = new ArrayList<Object>();
		pchList.addAll(scope.getContent());
		String scopeName = scope.getDbTables();
		for(Object obj : pchList){
			if(obj instanceof PCH){
				PCH pch = (PCH) obj;
				HashMap<String, String>tempMap = null;
				
				if("Common Rating".equals(type)){
					if(commonRatingDataMap.get(scopeName)!=null){
						tempMap = commonRatingDataMap.get(scopeName);
					}
				} else if ("Premium Calculation".equals(type)){
					tempMap = premiumCalculationDataMap.get(scopeName);
				}
				
				if(tempMap==null){
					ISOConsumptionLogger.info("PCH: " + pch.getName() + " is out of Scope");
				} else {
					tempMap.put(pch.getName(),"false");
				}
			}
		}
	}
	
	public void prepareReport() throws Exception {
		String fileName = IsoConsumptionUtil.getInstance().getProperty(
				Constants.PCH_REPORT_PATH_KEY);
		if (ExceptionHandler.checkNullOrEmpty(fileName)) {
			ExceptionHandler
					.raiseException(
							"ProcessedPchReportPath key not present in iso-consumption.properties",
							new Exception(
									"ProcessedPchReportPath key not present in iso-consumption.properties"));
		}
		ISOConsumptionLogger.info("Processed PCH Report File: " + fileName);
		File reportFile = new File(fileName);
		if (!reportFile.exists()) {
			reportFile.createNewFile();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(reportFile));
		writer.write("Type,Scope,PCH,Status\n");
		writer.write("Common Rating\n\n");
		ISOConsumptionLogger.info("Number of coverage in CR: "
				+ commonRatingDataMap.size());
		writeDataToFile(commonRatingDataMap, writer);
		writer.write("\nPremium Calculation\n\n");
		ISOConsumptionLogger.info("Number of coverage in PC: "
				+ premiumCalculationDataMap.size());
		writeDataToFile(premiumCalculationDataMap, writer);
		writer.flush();
		writer.close();
	}
	
	private void writeDataToFile(Map<String, HashMap<String,String>> dataMap, BufferedWriter writer) throws IOException{
		for(Entry<String, HashMap<String,String>> entry : dataMap.entrySet()){
			String scopeName = entry.getKey();
			HashMap<String,String>pchMap = entry.getValue();
			
			writer.write("," + scopeName);
			writer.write("\n");
			
			for( Entry<String, String> e : pchMap.entrySet()){
				String pchName =  e.getKey();
				writer.write(",," + pchName + "," + e.getValue() + "\n");
			}
		}
		
	}
	
	public void addToOverridenPchMap(String scopeName,String pchName,boolean isOverridden){
		
			if(ovierriddenPchNamesMap.get(scopeName) == null){
				HashMap<String, List<String>> hashMap = new HashMap<String, List<String>>();
				List<String> overridePCHNameList = new ArrayList<String>();
				List<String> inheritedPCHNameList = new ArrayList<String>();
				hashMap.put("OVERRIDDEN_PCH", overridePCHNameList);
				hashMap.put("INHERITED_PCH", inheritedPCHNameList);
				ovierriddenPchNamesMap.put(scopeName,hashMap);
			}
		
		if(isOverridden) {	
			ovierriddenPchNamesMap.get(scopeName).get("OVERRIDDEN_PCH").add(pchName);
		} else {
			ovierriddenPchNamesMap.get(scopeName).get("INHERITED_PCH").add(pchName);
		}
	}
	
	
	public void printOverridenPCHMapReports() {

		for (String scopeName : ovierriddenPchNamesMap.keySet()) {
			List<String> overriddenPchList = ovierriddenPchNamesMap.get(
					scopeName).get("OVERRIDDEN_PCH");
			List<String> inheritedPchList = ovierriddenPchNamesMap.get(
					scopeName).get("INHERITED_PCH");
			if (overriddenPchList.size() > 0) {
				System.out.println("SCOPE NAME:[" + scopeName + "]");
				System.out.println("OVERRIDDEN PCH IN THIS SCOPE");
				for (String pchType : overriddenPchList) {
					System.out.print(pchType);
					System.out.print(",");
				}
				System.out.println();
			}
			if (inheritedPchList.size() > 0) {
				System.out.println("INHERITED PCH IN THIS SCOPE");
				for (String pchType : inheritedPchList) {
					System.out.print(pchType);
					System.out.print(",");
				}
				System.out.println();
			}
		}
	}
	
	public void printOverridenPCHMapReports(String scopeName){
		
			List<String> overriddenPchList = ovierriddenPchNamesMap.get(
					scopeName).get("OVERRIDDEN_PCH");
			List<String> inheritedPchList = ovierriddenPchNamesMap.get(
					scopeName).get("INHERITED_PCH");
			if (overriddenPchList.size() > 0) {
				System.out.println("SCOPE NAME:[" + scopeName + "]");
				System.out.println("OVERRIDDEN PCH IN THIS SCOPE");
				for (String pchType : overriddenPchList) {
					System.out.print(pchType);
					System.out.print(",");
				}
				System.out.println();
			}
			if (inheritedPchList.size() > 0) {
				System.out.println("INHERITED PCH IN THIS SCOPE");
				for (String pchType : inheritedPchList) {
					System.out.print(pchType);
					System.out.print(",");
				}
				System.out.println();
			}
	}
	
	
	
}
