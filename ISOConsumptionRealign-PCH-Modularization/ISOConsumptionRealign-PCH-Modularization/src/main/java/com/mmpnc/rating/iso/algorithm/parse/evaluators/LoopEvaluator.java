package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ForStep;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.IncorrectXpathException;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.rating.iso.helper.ForStepCreater;
import com.mmpnc.rating.iso.helper.LoopThroughInfo;
import com.mmpnc.rating.iso.helper.PredicateInfoLevel;

public class LoopEvaluator implements Evaluator {

	private Context context;
	private String loopThrough;
	private List<Evaluator> evaluatorList;	
	private Map <String,PredicateInfoLevel> map = new HashMap<String, PredicateInfoLevel>();
	
	public LoopEvaluator(Context context, String string, List<Evaluator> s1) {
		this.context = context;
		this.loopThrough = string;
		this.evaluatorList = s1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Object evaluate()throws RecognitionException {
		
		
		String loopThroughCondition = loopThrough;
		
		if(loopThrough.contains("\"")){
			loopThrough = loopThrough.replace("\"", "");
		}
		
		boolean predicateInLoopthrough = false;
		
		
		String scopePath = (String)context.getValue(ContextParam.SCOPE);
		Database dsDb = (Database)context.getValue(ContextParam.OBJECTDATABASE);
		//GETTING FULLY QUALIFIED PATH AND THEN TESTING THE PREDICATE CONDITION IN LOOP THROUGH
		loopThrough = XpathUtil.getFullyQualifiedXpath(dsDb, scopePath, loopThrough.replace("\"", ""),true).trim();
		
		
		if ("".equals(loopThrough)) {
			loopThrough = loopThroughCondition;
			if(loopThrough.startsWith("\"descendant::") || loopThrough.startsWith("/descendant::")){
				throw new IncorrectXpathException("Incorrect xpath : ["+loopThroughCondition +"] In scope: ["+scopePath+"]");
			}
			if (loopThrough.contains("[")) {

				// REMOVING THE PREDICATE CONDITION FROM THE XPATH
				if (handlePredictaes(loopThrough, dsDb, scopePath)) {
					loopThrough = loopThrough.replaceAll("\\[.*?\\]", "");
					predicateInLoopthrough = true;
				}

				loopThrough = XpathUtil.getFullyQualifiedXpath(dsDb, scopePath, loopThrough.replace("\"", ""),true).trim();
			}else{
				//throw new IncorrectXpathException("Incorrect xpath : ["+loopThroughCondition +"] In scope: ["+scopePath+"]");
			}
		}
		
		//IF WE GET MULTIPLE XPATHS THAT MEANS WE NEED TO CREATE MULTIPLE RATING FOR STEPS
		
		String [] arr = loopThrough.split(" ");
		List<AlgoStep> steps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);	
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		
		ForStepCreater forStepCreater = ForStepCreater.getInstance();
		
		for(int i=0;i<arr.length;i++){
			
			ForStep forStep = forStepCreater.createForLoopStep(arr[i],predicateInLoopthrough,map,parentCondition,context);
			steps.add(forStep);
			context.putValue(ContextParam.STEPS, steps);
			//AFTER CREATION OF STEP NEED TO CALL THE EVALUATORS
			Context loopContext = new ContextBase();
			
			createContextForLoopChildSteps(loopContext, arr, i);
			
			for(Evaluator evaluator : evaluatorList){
				evaluator.setContext(loopContext);
				evaluator.evaluate();
			}
			
			//GETTING THE LOOP CHILD STEPS AND ADDING THEM INTO FORSTEP 
			List<AlgoStep> loopChildsteps = (List<AlgoStep>)loopContext.getValue(ContextParam.STEPS);
			forStep.setAlgoStepList(loopChildsteps);
			
			LookupModels loopStepLookups = (LookupModels)loopContext.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
			LookupModels mainContextLookupList = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
			//ADDING LOOKUP STEPS IN CHILD IF STEPS
			mainContextLookupList.getLookupModels().addAll(loopStepLookups.getLookupModels());
			
		}	
		
		context.remove(ContextParam.LOOPTHOUGHREPLACEVARIABLE);
		context.remove(ContextParam.LOOPTHROUGHREPLACEBY);
		return null;
	}
	
	
	
	/**
	 * @param loopContext
	 * @param arr
	 * @param i
	 * @throws RecognitionException 
	 * @throws Exception 
	 * This method will create a context for the loop child steps
	 */
	private void createContextForLoopChildSteps(Context loopContext,String arr[], int i) throws RecognitionException {
		
		String currentLoopLevel = getCurrentScopeForTheLoopChildSteps(arr[i]);
		//THIS CODE IS ADDED TO HANDLE THE LOOP IN LOOP CONDITION WHERE CURRENT CONTEXT FOR THE LOOP WILL CHANGE TO 
		//THE OUTER LOOP THROUGH LEVEL
		loopContext.putValue(ContextParam.LOOP_THROUGH_CURRENT_CONTEXT_LEVLE, currentLoopLevel);
		loopContext.putValue(ContextParam.OBJECTDATABASE,context.getValue(ContextParam.OBJECTDATABASE));
		loopContext.putValue(ContextParam.OBJECTBASEFORLOOP,context.getValue(ContextParam.OBJECTBASEFORLOOP));
		loopContext.putValue(ContextParam.SCOPE, context.getValue(ContextParam.SCOPE));
		loopContext.putValue(ContextParam.PARENTCONDITION, Constants.FOR_STEPTYPE);
		loopContext.putValue(ContextParam.CURRENTPROCESS,context.getValue(ContextParam.CURRENTPROCESS));
		loopContext.putValue(ContextParam.ISLOOPTHROUGHSTATEMENTS, true);
		loopContext.putValue(ContextParam.LOOKUP_NAME_DECIDER, context.getValue(ContextParam.LOOKUP_NAME_DECIDER));
		loopContext.putValue(ContextParam.LOCAL_VARIABLE_COUNTER, context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER));
		if(arr.length > 1){
			loopContext.putValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE, loopThrough);
			String loopThroughCondition = arr[i];
			if(loopThroughCondition.trim().startsWith("Policy")){
				loopThroughCondition = " "+loopThroughCondition;
				loopThroughCondition = loopThroughCondition.replace(" Policy", "CCSRERequest").trim();
			}
			loopContext.putValue(ContextParam.LOOPTHROUGHREPLACEBY,loopThroughCondition);
		}
	
	}
	
	/**
	 * @param qualPath
	 * @return
	 * @throws RecognitionException
	 * AS loops child steps will be executed in new context i.e loops context we ned to get
	 * current scope for the loop child steps
	 * @throws Exception 
	 * 
	 */
	private String getCurrentScopeForTheLoopChildSteps(String qualPath) throws RecognitionException{
		qualPath = qualPath.replace("\"", "");
		//System.out.println("Qualified Path = "+qualPath);
		if(qualPath.contains("/")){
			String arr[] = qualPath.split("/");
			qualPath = arr[arr.length-1];
			
		}
		Database forLoopDb = (Database)context.getValue(ContextParam.OBJECTBASEFORLOOP);
		Connection conn = DBFactoryBuilder.getXmlConnection(forLoopDb);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		String arr[] = qualPath.replace(".","~").split("~");
		String currentLevleForLoop = null;
		//String retString = (String) query.execute();
		query = query.createQuery("//TableName[@name='"+arr[arr.length-1]+"']");
		Object result = query.execute();
		if("".equals(result)){
			try{
			currentLevleForLoop = arr[arr.length-2];
			}catch(ArrayIndexOutOfBoundsException e){
				//StackTraceElement stackTraceElement = new StackTraceElement("LoopEvaluator", "getCurrentScopeForTheLoopChildSteps", "LOOPEval", 138);
				//RecognitionException rec = new ParserException("INCORRECT LOOP THROUGH CONDITION");
				throw new ParserException("INCORRECT LOOP THROUGH CONDITION"+qualPath);
			}
		}else{
			currentLevleForLoop = arr[arr.length-1];
		}
		return currentLevleForLoop;
	}
	
	
	
	
	/**
	 * @param loopThrough
	 * @param dsDb
	 * @param scopePath
	 * @return
	 * This method will handle predicates in for loop through condition
	 * @throws Exception 
	 * 
	 */
	private  boolean handlePredictaes(String loopThrough,Database dsDb,String scopePath) {
		
		//String originalXpath =  loopThrough;
		if(loopThrough.contains("ancestor::*") || loopThrough.contains("@offset=")){
			loopThrough  = removeAncestorFromXpath(loopThrough);	
		}else{
			//return false;
		}
		
		List<String> list = createAndReturnPredicateList();
		
		Iterator <String>iterator = list.iterator();
		while(iterator.hasNext()){
			String predicateLevle = iterator.next();
			String predicateCondtion = null;
			while (iterator.hasNext()) {
				predicateCondtion = iterator.next();
			}
			if (predicateCondtion == null){
				break;
			}
			String [] predicateArr = null;
			
			//ADDING HANDLING FOR THE AND OPERATION IN THE CONDITION
			if(predicateCondtion.contains(" and ")){
				predicateArr = predicateCondtion.split(" and ");
			}
			else{
				predicateArr = new String[]{predicateCondtion};
			}
			
			for(String predicateString : predicateArr){
				boolean isModelAttribute = false;
				LoopThroughInfo loopThroughInfo = new LoopThroughInfo();
				 
				//if(!predicateString.contains("not(@offset='Deleted')") || !predicateString.contains("(@offset='Deleted')")){
					Iterator<String>prdList = splitPredicate(predicateString, loopThroughInfo);
					
					while(prdList.hasNext()){
						String predicateFilterLHS = prdList.next();
						String predictateFilterRHS = prdList.next();
						if(predicateFilterLHS.contains("ancestor::") || predicateFilterLHS.contains("@offset")){
							continue;
						}
						String qualifiedPath =  predicateLevle+"/"+predicateFilterLHS;
						predicateLevle = predicateLevle.replaceAll("\"", "");
						qualifiedPath = XpathUtil.getFullyQualifiedXpath(dsDb, scopePath+"/"+predicateLevle , predicateFilterLHS,true);
						if(!qualifiedPath.equals("")){
							
							predicateFilterLHS = qualifiedPath;
							
						}
						qualifiedPath = XpathUtil.getFullyQualifiedXpath(dsDb, scopePath+"/"+predicateLevle, predictateFilterRHS,false);
						if(!qualifiedPath.equals("")){
							isModelAttribute = true;
							predictateFilterRHS = qualifiedPath;
						}
						//This check has been added to handle the xpath like \*[PremiumIndicator = '1'] 
						//where it will return multiple predicates xpath
						
						if(predicateLevle.equals("/*")){
							handleSlashStarPredicateInLoopThrough(loopThroughInfo, predicateFilterLHS, predictateFilterRHS);
						}
						else{
							loopThroughInfo.setPredicateLHS(predicateFilterLHS);
							loopThroughInfo.setPredicateRHS(predictateFilterRHS);
							loopThroughInfo.setModelAttribute(isModelAttribute);
							String mapKey = getMapKey(predicateLevle);
							addToPredicateMap(mapKey, map, loopThroughInfo);
						}
						
					}
			}
			
		}
		
		return true;
	}
	
	/**
	 * @param loopThroughInfo
	 * @param predicateFilterLHS
	 * @param predictateFilterRHS
	 * In HO algorithms we have found an xpath which contains \*[PremiumIndicator=1] to handle this method will handle such perticular conditions
	 * 
	 */
	private void handleSlashStarPredicateInLoopThrough(LoopThroughInfo loopThroughInfo,String predicateFilterLHS,String predictateFilterRHS){
		String []predLSHArr = predicateFilterLHS.split(" ");
		for(String predicateLHS : predLSHArr){
			try {
				LoopThroughInfo loopThroughInfo2 = loopThroughInfo.clone();
				loopThroughInfo2.setPredicateLHS(predicateLHS);
				loopThroughInfo2.setPredicateRHS(predictateFilterRHS);
				String []xpathArr = predicateLHS.split("\\.");
				String mapKey = xpathArr[xpathArr.length-2];
				addToPredicateMap(mapKey, map, loopThroughInfo2);
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * @return
	 * This method will return list of predicates in loop through condition
	 * 
	 */
	private List<String> createAndReturnPredicateList(){
		String completePath = null;
		String [] arr = loopThrough.split("\\]");
		List<String> list = new ArrayList<String>();
				
		for(int i=0;i<arr.length;i++ ){
			String xpathWithPred = arr[i];
			
			String [] pred = xpathWithPred.split("\\[");
			
			String key = null;
			//String value = null;
			if(pred.length>1)
			for(int j=0;j<pred.length;j++){
				
				if(j%2 == 0){
					list.add(pred[j]);
					key = pred[j];
					if(completePath == null){
						completePath = key;
					}
					else{
						completePath = completePath+key;
					}
				}
				else {
					String predicateCond = pred[j];
					if(predicateCond.contains("\\")){
						predicateCond = predicateCond.substring(predicateCond.lastIndexOf("\\"));
					}
					//value = predicateCond;	
					list.add(pred[j]);
				}
			}
		}
		return list;
	}
	
	/**
	 * THIS METHOD WILL SPLIT THE PREDICATE USING TEH OPREATOR 
	 * @param predicateCondtion
	 * @param loopThroughInfo
	 * @return
	 */
	private Iterator<String> splitPredicate(String predicateCondtion ,LoopThroughInfo loopThroughInfo ){
		Iterator<String> prdListIterator= null;
		if(predicateCondtion.contains(">")){
			prdListIterator = Arrays.asList(predicateCondtion.split(">")).iterator();
			 loopThroughInfo.setOperator(">");
		}
		else if(predicateCondtion.contains("<")){
			prdListIterator = Arrays.asList(predicateCondtion.split("<")).iterator();
			loopThroughInfo.setOperator(">");
		}
		else if(predicateCondtion.contains("<=")){
			prdListIterator = Arrays.asList(predicateCondtion.split("<=")).iterator();
			loopThroughInfo.setOperator("<=");
		}
		else if(predicateCondtion.contains(">=")){
			prdListIterator = Arrays.asList(predicateCondtion.split(">=")).iterator();
			loopThroughInfo.setOperator(">=");
		}
		else if(predicateCondtion.contains("!=")){
			prdListIterator = Arrays.asList(predicateCondtion.split("!=")).iterator();
			loopThroughInfo.setOperator("!=");
		}
		else if(predicateCondtion.contains("=")){
			prdListIterator = Arrays.asList(predicateCondtion.split("=")).iterator();
			loopThroughInfo.setOperator("=");
		}
		return prdListIterator;
	}
	
	
	
	/**
	 * @param key
	 * @param predicateMap
	 * @param loopThroughInfo
	 */
	private  void  addToPredicateMap(String key,Map<String,PredicateInfoLevel>predicateMap,LoopThroughInfo loopThroughInfo){
		if(predicateMap.containsKey(key.trim())){
			PredicateInfoLevel predicateInfoLevel = predicateMap.get(key);
			List<LoopThroughInfo> listOfCond = predicateInfoLevel.getLoopThroughList();
			listOfCond.add(loopThroughInfo);
		}else{
			List<LoopThroughInfo> listOfCond = new ArrayList<LoopThroughInfo>();
			listOfCond.add(loopThroughInfo);
			PredicateInfoLevel predicateInfoLevel = new PredicateInfoLevel();
			predicateInfoLevel.setLoopThroughList(listOfCond);
			predicateMap.put(key, predicateInfoLevel);
		}
	}
	
	/**
	 * @param loopThrough
	 * @return
	 */
	private static String getMapKey(String loopThrough){
		if(loopThrough.contains("/")){
			loopThrough = loopThrough.replaceAll("/", "~");
			loopThrough = loopThrough.substring(loopThrough.lastIndexOf("~")+1);
		}
		return loopThrough;
	}
	
	
	/**
	 * @param xPathStr
	 * @return
	 */
	private String removeAncestorFromXpath(String xPathStr){
		String fullXpath = xPathStr;
		if(xPathStr.contains("ancestor::*[name(.)")){
			//String ancestorString = null;
			StringBuffer xpathWithoutAncestor = new StringBuffer();
			if(xPathStr.contains("/")){
				String arrs[] = xPathStr.split("/");
				for(String xpathStr: arrs){
					if(xpathStr.trim().startsWith("ancestor::*[name(.)")){
				//		ancestorString  = xpathStr;
						String arncestorName = xPathStr.split("'")[1];
						xpathWithoutAncestor.append(arncestorName);
						xpathWithoutAncestor.append("/");
					}
					else
					{
						xpathWithoutAncestor.append(xpathStr);
						xpathWithoutAncestor.append("/");
					}
				}
				fullXpath =  xpathWithoutAncestor.toString().substring(0,xpathWithoutAncestor.length()-1);
				
			}
			else{
				String arncestorName = xPathStr.split("'")[1];
				fullXpath =  xPathStr.replace(xPathStr, arncestorName);
			}
		}
		if(xPathStr.contains("[not(@offset=")){
			fullXpath = xPathStr.replace("[not(@offset='Deleted')]", "");
		}
		return fullXpath;
	}
	
	public static void main(String[] args) {
		
		String theString = "../BOPContrctrsInstalltnToolsAndEquipmtScheduledPropCovDetail[Limit &gt; 0]/BOL[Lim &lt; 0]";
		ISOConsumptionLogger.info(theString.replaceAll("\\[.*?\\]", ""));
		
		//handlePredictaes("../BOPContrctrsInstalltnToolsAndEquipmtScheduledPropCovDetail[Limit &gt; 0]/BOL[Lim &lt; 0]");
		
		 
	}
}