package com.mmpnc.rating.iso.algorithm.exception;

public class XPathParserRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XPathParserRuntimeException(String string) {
		super(string);
	}

}
