package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class IntegerEvaluator implements Evaluator {

	private String vairable;
	Context context;
	public IntegerEvaluator(String string) {
		this.vairable = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		context.putValue(ContextParam.ISSOURCESTATICVAR, true);
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		
		return new StringBuffer(vairable);
	}

}
