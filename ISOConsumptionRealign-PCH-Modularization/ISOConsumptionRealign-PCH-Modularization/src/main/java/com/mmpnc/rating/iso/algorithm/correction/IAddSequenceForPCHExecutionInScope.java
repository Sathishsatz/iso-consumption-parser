package com.mmpnc.rating.iso.algorithm.correction;

import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *	This interface has methods related to addition of execution sequence of PCH 
 *  for the scope
 *  
 */
public interface IAddSequenceForPCHExecutionInScope {
	public void addListOFModulesNeedsToBeExecutedForTheScope(String referenceType,List<Scope>newScopeList,Map<String,PCHInfo> processedPCHMap);
}
