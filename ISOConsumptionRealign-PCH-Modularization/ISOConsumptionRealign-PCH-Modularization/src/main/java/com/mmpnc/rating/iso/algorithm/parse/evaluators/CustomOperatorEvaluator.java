package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class CustomOperatorEvaluator implements Evaluator{
	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	
	public CustomOperatorEvaluator(Evaluator e1,Evaluator e2){
		this.left = e1;
		this.right = e2;
	}

	@Override
	public Object evaluate() throws RecognitionException {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" ( ");		
		this.left.setContext(context);
		buffer.append(this.left.evaluate());
		buffer.append(" CustomOperator ");
		this.right.setContext(context);
		buffer.append(this.right.evaluate());
		buffer.append(" ) ");
		return buffer;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
		
	}

	
}
