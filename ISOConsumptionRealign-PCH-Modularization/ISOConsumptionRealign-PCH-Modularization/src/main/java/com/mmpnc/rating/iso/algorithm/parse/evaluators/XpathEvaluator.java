package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.ArrayList;
import java.util.List;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.rating.iso.algorithm.ArgumentVariable;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.DateYearMonthHandlerUtil;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.rating.iso.helper.MemoryLookupStepCreater;
import com.mmpnc.xml.xpath.parse.XPathActivity;
import com.mmpnc.xml.xpath.parse.XPathParserImpl;

public class XpathEvaluator implements Evaluator {

	String xpathAttribute;
	Context context;

	public XpathEvaluator(String string) {
		this.xpathAttribute = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws ParserException {

		// System.out.println("xpathAttribute="+xpathAttribute);
		Object countFunctionFlag = context
				.getValue(ContextParam.PREDICATEWITHCOUNTFUNCTION);
		Object ifConditionXpath = context
				.getValue(ContextParam.IFCONDITIONXPATH);
		Object isAssignRhsXpath = context
				.getValue(ContextParam.ISASSIGNEMENTRHSXPATH);

		// IF COUNT FUNCTION PREICATES THEN WE NEED TO REMOVE @Offset PREDICATE
		if (countFunctionFlag != null && (Boolean) countFunctionFlag) {
			xpathAttribute = removeAncestorFromXpath(xpathAttribute);
		}

		// THIS CHECK IS ADDED IF XPATH CONTAINS PREDICATES THEN WE NEED TO
		// HDNALE THEM
		if (xpathAttribute.contains("[")) {

			// ADDED TO HANDLE NEW CONDITION FOUND IN CA-CA ALGO FILE
			// ../CommercialAutoNonOwnedAuto/CommercialAutoNonOwnedAutoLiabilityCoverage[not(@offset='Deleted')
			// and PremiumIndicator = '1']/Premium
			xpathAttribute = removeNotOffsetDeleteFromXpath(xpathAttribute);
			// IF XPATH CONTINAS ANCESTOR OR NOT(@OFFSET) THEN WE DO NOT HAVE TO
			// CONISDER IT AS PREDICATE
			// (xpathAttribute.contains("name(.)") ||
			// xpathAttribute.contains("(name(.)")) added to ignore predicate
			// evaluation if it contains name(.) in predicate xpath
			if (!xpathAttribute.contains("ancestor::*")
					&& !xpathAttribute.contains("not(@offset=")
					&& !xpathAttribute.contains("descendant::")
					&& !xpathAttribute.contains("not(@offset =")
					&& !(xpathAttribute.contains("name(.)") || xpathAttribute
							.contains("(name(.)"))) {

				if (countFunctionFlag != null) {

					if ((Boolean) countFunctionFlag) {
						// predicateContext.putValue(ContextParam.PREDICATEWITHCOUNTFUNCTION,booleanFlag);
						return handleCountFunctionPredicate();
					}

				}

				String xpathStr = xpathAttribute.replaceAll("^\"|\"$", "");
				StringBuffer xpathBuffer = new StringBuffer(xpathStr.trim());
				Context predicateContext = createContextForPredicate(
						xpathBuffer, xpathStr, ifConditionXpath,
						isAssignRhsXpath);
				// EXECUTING PREDICATE EVALUATOR TO GET THE PREDICATES XPATH
				executePredicateEval(predicateContext, xpathBuffer);

				if (!checkXpathForIfConditionOrCountFunction(countFunctionFlag,
						predicateContext, ifConditionXpath)) {
					// AS FOR COUNT FUNCTION XPATH PREDICATE AND IF CONDITION
					// PREDICATE WE DO NOT NEED A MEMORY LOOKUP
					return createMemoryLookupStep(xpathStr, predicateContext);
				}

				xpathAttribute = xpathAttribute.replaceAll("\\[.*?\\]", "");
			}
		}

		Database dsDB = (Database) context
				.getValue(ContextParam.OBJECTDATABASE);
		String scopePath = (String) context.getValue(ContextParam.SCOPE);
		// Object preidcateInCountFun =
		// context.getValue(ContextParam.PREDICATEWITHCOUNTFUNCTION);
		// This code has been added to handle the for loop multiple xpath
		// conditions[star in loop through]
		String qualifiedPath = null;
		if (context.getValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE) != null) {
			if (xpathAttribute.startsWith("\"")) {
				xpathAttribute = xpathAttribute.replace("\"", "");
			}
			return handleLoopThroughStarCondition(qualifiedPath, dsDB,
					scopePath);
		}
		// REMOVING OFFSET AND ANCESTOR ENTRIES IN THE XPATH NEED TO CHECK FOR
		// THE CORRECTNESS OF IT
		xpathAttribute = removeAncestorFromXpath(xpathAttribute);
		xpathAttribute = xpathAttribute.replace("\"", "");

		String fullyQualifiedPath = null;
		if (xpathAttribute.endsWith(".Date")
				|| xpathAttribute.endsWith(".Year")
				|| xpathAttribute.endsWith(".Month")) {
			fullyQualifiedPath = DateYearMonthHandlerUtil
					.handleDateYearOrMonthInXpath(dsDB, xpathAttribute,
							scopePath);
			return new StringBuffer(fullyQualifiedPath);
		}

		// Database dsDB =
		// (Database)context.getValue(ContextParam.OBJECTDATABASE);
		// String scopePath = (String)context.getValue(ContextParam.SCOPE);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		context.putValue(ContextParam.MODELATTRIBUTE, true);

		fullyQualifiedPath = XpathUtil.getFullyQualifiedXpath(dsDB, scopePath,
				xpathAttribute, true);
		if ("Policy".equals(xpathAttribute) && null == qualifiedPath) {
			fullyQualifiedPath = Constants.CURRENT_CONTEXT_OBJECT_NAME;
		}

		if (ExceptionHandler.checkNullOrEmpty(fullyQualifiedPath)) {
			ExceptionHandler.raiseParserException("XPATH not found:  "
					+ scopePath + "/" + xpathAttribute);
		}

		return new StringBuffer(fullyQualifiedPath);

	}

	// ADDED TO HANDLE NEW CONDITION FOUND IN CA-CA ALGO FILE
	// ../CommercialAutoNonOwnedAuto/CommercialAutoNonOwnedAutoLiabilityCoverage[not(@offset='Deleted')
	// and PremiumIndicator = '1']/Premium
	private String removeNotOffsetDeleteFromXpath(String xpathAttribute) {

		if (xpathAttribute.contains("not(@offset=")
				&& (xpathAttribute.contains(" and ") || xpathAttribute
						.contains(" or "))) {
			String predicate = xpathAttribute.substring(
					xpathAttribute.indexOf("["), xpathAttribute.indexOf("]"));
			if (predicate.contains(" and ") || predicate.contains(" or ")) {
				xpathAttribute = removeAncestorFromXpath(xpathAttribute);
			}
		}
		return xpathAttribute;

	}

	private StringBuffer handleLoopThroughStarCondition(String qualifiedPath,
			Database dsDB, String scopePath) {

		qualifiedPath = XpathUtil.getFullyQualifiedXpath(dsDB, scopePath,
				xpathAttribute, true);
		if ("".equals(qualifiedPath)) {
			qualifiedPath = xpathAttribute;
		} else if (qualifiedPath.equals(context
				.getValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE))) {
			qualifiedPath = (String) context
					.getValue(ContextParam.LOOPTHROUGHREPLACEBY);
			if (qualifiedPath.startsWith("Policy")) {
				qualifiedPath = " ".concat(qualifiedPath);
				qualifiedPath = qualifiedPath.replace(" Policy.",
						"CCSRERequest.");
			}
			context.putValue(ContextParam.ISSOURCESTATICVAR, false);
			context.putValue(ContextParam.MODELATTRIBUTE, true);
		} else {
			qualifiedPath = findExactQualifiedPathInLoopStarCondition(qualifiedPath);
			context.putValue(ContextParam.ISSOURCESTATICVAR, false);
			context.putValue(ContextParam.MODELATTRIBUTE, true);
		}
		return new StringBuffer(qualifiedPath);

	}

	// THIS METHOD ADDED TO HANDLE LOOP THROUGH CONDITION WHICH RESULT INTO
	// MULTIPLE STEPS
	private String findExactQualifiedPathInLoopStarCondition(
			String qualifiedPath) {
		// THIS CONDITION ADDED TO HANDLE A LOOP THROUGH CONDITION WHERE LOOP
		// THROUGH CONDITION XPATH IS NOT EQUAL TO
		// XPATH IN LOOP STATEMENTS
		// FOUND IN CA_CA ALGO FILE FINANCIALRESPOSIBILITY COVERAGE
		// loop through:../*[(name(.)='CommercialAutoTruck' or
		// name(.)='CommercialAutoZoneRated' or
		// name(.)='CommercialAutoPrivatePassenger' or
		// name(.)='CommercialAutoPublicTransportation' or
		// name(.)='CommercialAutoSpecialTypes')]
		// if condition xpath:../*[(name(.)='CommercialAutoTruck' or
		// name(.)='CommercialAutoZoneRated' or
		// name(.)='CommercialAutoPrivatePassenger' or
		// name(.)='CommercialAutoPublicTransportation' or
		// name(.)='CommercialAutoSpecialTypes')]/CommercialAutoVehicleLiabilityCoverage/Premium
		String loopthroughCondition = (String) context
				.getValue(ContextParam.LOOPTHROUGHREPLACEBY);
		// SPLITING QUALIFIEDPATH BY SPACE
		if (qualifiedPath.contains(" ")) {
			String[] xPathArr = qualifiedPath.split(" ");
			for (String xpath : xPathArr) {
				if (xpath.trim().startsWith("Policy")) {
					xpath = " ".concat(xpath);
					xpath = xpath.replace(" Policy", "CCSRERequest");
				}
				if (xpath.startsWith(loopthroughCondition)) {
					return xpath;
				}
			}
			// At this point we are confirm that the xpath with * in loop
			// through condition
			// does not match with any of the xpaths in xpath array
			// there can be possibility that xpath inside loop through condition
			// is for another attribute
			qualifiedPath = checkIfTheXpathISForAnotherAttributeAtSameLevel(
					xPathArr, loopthroughCondition, qualifiedPath);

		}
		return qualifiedPath;
	}

	/**
	 * This method is added to check if the xpath contains * and is inside a
	 * loop then check if that xpath is for another attribute at the same level
	 * as loop through condition ex:in CA algo we have loop through:
	 * '../star/VehicleNumber' inside this loop we have ../star/UnitNumber
	 * 
	 * @param xpaths
	 * @param loopThrough
	 * @return
	 */
	private String checkIfTheXpathISForAnotherAttributeAtSameLevel(
			String[] xpaths, String loopThrough, String qualifiedPath) {
		String loopThroughWithoutAttribute = loopThrough.substring(0,
				loopThrough.lastIndexOf("."));
		// if multiple xpaths returned for the given query
		if (xpaths.length > 1) {
			for (String xpath : xpaths) {
				String xPathWithoutAttr = xpath.substring(0,
						xpath.lastIndexOf("."));
				xPathWithoutAttr = xPathWithoutAttr.replace("Policy.",
						"CCSRERequest.");
				if (xPathWithoutAttr.equals(loopThroughWithoutAttribute)) {
					// that means xpath is of another attribute at the loop
					// through condition level
					xpath = " " + xpath;
					qualifiedPath = xpath.replace(" Policy.", "CCSRERequest.");
					break;
				}
			}

		}
		return qualifiedPath;
	}

	/**
	 * @param countFunctionFlag
	 * @param predicateContext
	 * @param ifConditionXpath
	 * @return
	 */
	private boolean checkXpathForIfConditionOrCountFunction(
			Object countFunctionFlag, Context predicateContext,
			Object ifConditionXpath) {
		boolean flag = false;
		// THIS HANDLING IS ADDED TO HANDLE COUNT FUNCTION PREDICATES AS WE DO
		// NOT NEED THE MEMORY LOOKUP FOR THEM
		if (countFunctionFlag != null && (Boolean) countFunctionFlag) {
			context.putValue(ContextParam.COUNTFUNCTIONPREDICATEXPATH,
					predicateContext
							.getValue(ContextParam.COUNTFUNCTIONPREDICATEXPATH));
			flag = true;
		}
		// HANDLING IS ADDED TO HANDLE THE NEXT STEPS AFTER HANDLING PREDICATES
		// IN IF CONDITION XPATH
		else if (ifConditionXpath != null && (Boolean) ifConditionXpath) {
			// Getting the if condition predicate and putting it in the main
			// context
			context.putValue(ContextParam.IFCONDITIONPREDICATEXPATH,
					predicateContext
							.getValue(ContextParam.IFCONDITIONPREDICATEXPATH));
			// THIS CONTEXT PARAM WILL INDICATE THAT IF CONDITION HAS A
			// PREDICATE AND NEED TO ADD AN EXTRA CONDITION IF
			// IF CONDITION [THIS HANDLING IS ADDED IN IFEVALUATOR]
			context.putValue(ContextParam.IS_IFCONDITIONPREDICATE, true);
			// removing predicates from if condition
			flag = true;
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	private StringBuffer createMemoryLookupStep(String xpathStr,
			Context predicateContext) throws ParserException {
		// Creating MemeoryLookupStep and adding it to the list of algo steps
		String returnAttributeName = xpathStr.replaceAll("\\[.*?\\]", "");
		returnAttributeName = XpathUtil.getFullyQualifiedXpath(
				(Database) context.getValue(ContextParam.OBJECTDATABASE),
				(String) context.getValue(ContextParam.SCOPE),
				returnAttributeName, true);
		if (ExceptionHandler.checkNullOrEmpty(returnAttributeName)) {
			ExceptionHandler
					.raiseParserException("Incorrect XPath while creating memory lookup"
							+ returnAttributeName);
		}
		returnAttributeName = returnAttributeName.replace("CCSRERequest.", "");
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		String aliasName = MemoryLookupStepCreater.createMemoryLookup(context,
				(String) context.getValue(ContextParam.PARENTCONDITION),
				(List<SingleFieldConstraintEBLeftSide>) predicateContext
						.getValue(ContextParam.LISTOFMEMORYLOOKFILTERS),
				returnAttributeName);
		return new StringBuffer(aliasName);
	}

	private void executePredicateEval(Context predicateContext,
			StringBuffer xpathBuffer) throws ParserException {
		XPathActivity parse = new XPathParserImpl(predicateContext, xpathBuffer);
		Evaluator.Type type;
		try {
			type = (Type) parse.execute();
			StringBuffer value = new StringBuffer();
			if (type.equals(Type.PATH)) {
				value.append("XPATH:\"")
						.append(context.getValue(ContextParam.XPATHSTRING))
						.append("\"");
			} else if (type.equals(Type.ATTRIBUTE)) {
				String xpathString = context.getValue(ContextParam.XPATHSTRING) != null ? context
						.getValue(ContextParam.XPATHSTRING).toString() : null;
				value.append(ArgumentVariable.assignType((Database) context
						.getValue(ContextParam.OBJECTDATABASE),
						(String) context.getValue(ContextParam.REFERENCE),
						(String) context.getValue(ContextParam.SCOPE),
						xpathString));
			} else {
				value.append(context.getValue(ContextParam.XPATHSTRING));
			}
		} catch (Exception e) {
			ExceptionHandler.raiseParserException(e.getMessage());
		}

	}

	private Context createContextForPredicate(StringBuffer xpathBuffer,
			String xpathStr, Object ifConditionXpathFlag,
			Object assignRhsXpathFlag) {
		List<SingleFieldConstraintEBLeftSide> fitlerList = new ArrayList<SingleFieldConstraintEBLeftSide>();
		Context predicateContext = new ContextBase();
		predicateContext.putValue(ContextParam.OBJECTDATABASE,
				context.getValue(ContextParam.OBJECTDATABASE));
		predicateContext.putValue(ContextParam.XPATHSTRING, xpathBuffer);
		predicateContext.putValue(ContextParam.ISPREDICATE, false);
		predicateContext.putValue(ContextParam.ISFUNCTION, true);
		predicateContext.putValue(ContextParam.REFERENCE, "reference");
		predicateContext.putValue(ContextParam.SCOPE,
				context.getValue(ContextParam.SCOPE));
		predicateContext.putValue(ContextParam.PREDICATEPARSER, true);
		predicateContext.putValue(ContextParam.LISTOFMEMORYLOOKFILTERS,
				fitlerList);
		predicateContext.putValue(ContextParam.XPATHWITHPREDICATES, xpathStr);
		predicateContext.putValue(ContextParam.PREDICATEFILTERORDER, 0);
		predicateContext.putValue(ContextParam.IFCONDITIONXPATH,
				ifConditionXpathFlag);
		predicateContext.putValue(ContextParam.ISASSIGNEMENTRHSXPATH,
				assignRhsXpathFlag);
		predicateContext.putValue(ContextParam.LOOKUP_NAME_DECIDER,
				context.getValue(ContextParam.LOOKUP_NAME_DECIDER));
		predicateContext.putValue(ContextParam.LOCAL_VARIABLE_COUNTER,
				context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER));
		return predicateContext;
	}

	private StringBuffer handleCountFunctionPredicate() throws ParserException {
		String[] xpathArr = xpathAttribute.split("\\[");
		String predicateLevel = xpathArr[0];
		predicateLevel = predicateLevel.replace("\"", "");
		String predLevQualPath = XpathUtil.getFullyQualifiedXpath(
				(Database) context.getValue(ContextParam.OBJECTDATABASE),
				(String) context.getValue(ContextParam.SCOPE), predicateLevel,
				true);
		if (ExceptionHandler.checkNullOrEmpty(predLevQualPath)) {
			ExceptionHandler
					.raiseParserException("Incorrect xpath inside count function - "
							+ predicateLevel);
		}
		xpathAttribute = xpathAttribute
				.replace(predicateLevel, predLevQualPath);
		return new StringBuffer(xpathAttribute);
	}

	/**
	 * @param xPathStr
	 * @return
	 * 
	 */
	private String removeAncestorFromXpath(String xPathStr) {
		String fullXpath = xPathStr;
		if (xPathStr.contains("ancestor::*[name(.)")) {
			@SuppressWarnings("unused")
			String ancestorString = null;
			StringBuffer xpathWithoutAncestor = new StringBuffer();
			if (xPathStr.contains("/")) {
				String arrs[] = xPathStr.split("/");
				for (String xpathStr : arrs) {
					if (xpathStr.trim().startsWith("ancestor::*[name(.)")) {
						ancestorString = xpathStr;
						String arncestorName = xPathStr.split("'")[1];
						xpathWithoutAncestor.append(arncestorName);
						xpathWithoutAncestor.append("/");
					} else {
						xpathWithoutAncestor.append(xpathStr);
						xpathWithoutAncestor.append("/");
					}
				}
				fullXpath = xpathWithoutAncestor.toString().substring(0,
						xpathWithoutAncestor.length() - 1);
			} else {
				String arncestorName = xPathStr.split("'")[1];
				fullXpath = xPathStr.replace(xPathStr, arncestorName);
			}
		}

		else if (xPathStr.contains("[not(@offset=")) {
			String predicate = null;
			if (xPathStr.contains("not(@offset=\\'Deleted\\')")) {
				predicate = xPathStr.substring(
						xPathStr.indexOf("[not(@offset=\\'Deleted\\')")
								+ ("[not(@offset='Deleted')".length())).trim();
			} else {
				predicate = xPathStr.substring(
						xPathStr.indexOf("[not(@offset='Deleted')")
								+ ("[not(@offset='Deleted')".length())).trim();
			}

			if (predicate.startsWith("and")) {
				fullXpath = xPathStr.replace("not(@offset='Deleted') and ", "");
			} else if (predicate.startsWith("or")) {
				fullXpath = xPathStr.replace("not(@offset='Deleted') or ", "");
			} else {
				if (xPathStr.contains("not(@offset=\\'Deleted\\')")) {
					fullXpath = xPathStr.replace(
							"[not(@offset=\\'Deleted\\')]", "");
				} else {
					fullXpath = xPathStr
							.replace("[not(@offset='Deleted')]", "");
				}
			}
		}
		return fullXpath;
	}

}