package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.AlgoUtil;
import com.mmpnc.rating.iso.algorithm.util.DateYearMonthHandlerUtil;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.util.VariableType;

public class VarEvaluator implements Evaluator, IEvaluatorValidator {
	private String variable;
	private String variableType;
	private Context context;

	public VarEvaluator(String variableType, String string) {
		this.variableType = variableType;
		this.variable = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void validate() throws RecognitionException {
		if (this.variable == null && this.variableType == null) {
			throw new ParserException(
					"Varibale and Variable type is NULL in VAR evaluator");
		} else if (this.variable == null || this.variable.equals("")) {
			throw new ParserException(" Variable is NULL in VAR evaluator");
		} else if (this.variableType == null) {
			throw new ParserException("Variable type is NULL in VAR evaluator");
		}
	}

	@Override
	public StringBuffer evaluate() throws RecognitionException {

		// validate();

		StringBuffer returnValue = new StringBuffer();

		String currentProcess = (String) this.context
				.getValue(ContextParam.CURRENTPROCESS);

		boolean isPointerToAnotherPCH = false;
		// below code added to check the PCH block for local variables only
		if (this.variableType != null) {

			returnValue.append(" ").append(this.variableType).append(":")
					.append(this.variable).append(" ");

			VariableType varType = VariableType.valueOf(variableType);
			if (varType.equals(VariableType.LV_VALUE)
					|| varType.equals(VariableType.LV_INTEGER)
					|| varType.equals(VariableType.LV_NUMERIC)
					|| varType.equals(VariableType.LV_DOUBLE)
					|| varType.equals(VariableType.LV_STRING)
					|| varType.equals(VariableType.LV_BOOLEAN)
					|| varType.equals(VariableType.LV_DATE)
					|| varType.equals(VariableType.LV_TIMESPAN)
					|| varType.equals(VariableType.COLUMN_NUMERIC)
					|| varType.equals(VariableType.COLUMN_STRING)) {

				if (!this.variable.equals("null")
						&& !(currentProcess != null && currentProcess
								.equals(this.variable))) {

					isPointerToAnotherPCH = findAndAddPCHBolckForTheVariable(currentProcess);

					if (!isPointerToAnotherPCH) {
						Database dsDb = (Database) context
								.getValue(ContextParam.OBJECTDATABASE);
						String scopePath = (String) context
								.getValue(ContextParam.SCOPE);
						String xpath = XpathUtil.getFullyQualifiedXpath(dsDb,
								scopePath, variable, false);
						if (ExceptionHandler.checkNullOrEmpty(xpath)) {
							if (variable.endsWith(".Days")
									|| variable.endsWith(".Month")
									|| variable.endsWith(".Year")
									|| variable.endsWith(".Day")) {
								// CHECK IF THE VARIABLE IS AN XPATH IF NOT THEN
								// CHECK IF VARIABLE IS A POINTER TO A LOCAL
								// PCH IF BOTH IS NOT TRUE THEN THROW THE
								// EXCEPTION
								//variable = variable.replace(".Days","");
								String functionReturnStr = DateYearMonthHandlerUtil
										.handleDateYearOrMonthInXpath(dsDb,
												variable, scopePath);
								variable = variable.replace(".Days", "");
								isPointerToAnotherPCH = findAndAddPCHBolckForTheVariable(variable);
								if(isPointerToAnotherPCH){
									return new StringBuffer(functionReturnStr);
								}
								
								variable = variable.concat(".Days");
								
								if ((functionReturnStr == null)
										|| ("".equals(functionReturnStr))) {
									variable = variable.replace(".Days", "");
									isPointerToAnotherPCH = findAndAddPCHBolckForTheVariable(currentProcess);
									variable = variable.concat(".Days");
									// CHECK IF THE VARIABLE IS POINTER TO THE
									// LOCAL PCH
									if (isPointerToAnotherPCH) {
										ExceptionHandler
												.raiseParserException("Unable to find variable ["
														+ variable
														+ "] of type ["
														+ variableType + "]");
									}

								}else{
									//this is added when there is a function return string
									returnValue = new StringBuffer(functionReturnStr);
								}
							} else if (varType.equals(VariableType.LV_DATE)
									&& variable.endsWith(".Date")) {
								//if variable type is LV_DATE then we need to find if that
								//variable points to a PCH
								variable = variable.replace(".Date", "");
								isPointerToAnotherPCH = findAndAddPCHBolckForTheVariable(currentProcess);
								variable = variable.concat(".Date");
								if (!isPointerToAnotherPCH) {
									ExceptionHandler
											.raiseParserException("Unable to find variable ["
													+ variable
													+ "] of type ["
													+ variableType + "]");
								}
							} else {
								ExceptionHandler
										.raiseParserException("Unable to find variable ["
												+ variable
												+ "] of type ["
												+ variableType + "]");
							}
						}
					}
				}
			}

		} else {
			// This handling is added for statements like ARDSpan.Days where
			// ARDSpan is the PCH
			if (variable.endsWith(".Days")) {
				variable = variable.replace(".Days", "");
				isPointerToAnotherPCH = findAndAddPCHBolckForTheVariable(currentProcess);
				variable = variable.concat(".Days");

			}

			returnValue.append(" ").append(this.variable).append(" ");

		}

		// IF THE LOCAL VARIABLE IS POINTING TO AOTHER PCH THEN ADDING A CALL
		// STATEMENT TO THE LOCAL VARIABLE
		if (isPointerToAnotherPCH) {
			String returnVal = " getProgram (" + this.variable + ")";
			return new StringBuffer(returnVal);
		}

		return returnValue;
	}

	private boolean findAndAddPCHBolckForTheVariable(String currentProcess)
			throws ParserException {

		boolean isVarPointsToPCH = AlgoUtil.searchAndProcess(this.variableType,
				this.variable, context);
		return isVarPointsToPCH;

	}

}