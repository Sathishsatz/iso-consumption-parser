package com.mmpnc.rating.iso.algorithm.correction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ExecuteModuleStep;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.helper.ExecuteModelStepCreater;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public class CorrectionForPCHMoularizationImpl implements ICorrectionForPCHModularization{

	
	
	/* (non-Javadoc)
	 * This method is finds out of PCH statements and create a new PCH with name Premium
	 */
	@Override
	public PCH createPremiumPCHAndAddOutOFPCHStatements(Scope scope)throws ParserException {
		PCH premiumPCH = new PCH();
		premiumPCH.setName("Premium");
		boolean outOfPCHContent = false;
		List<Object> scopeContent = scope.getContent();
		for(Object content:scopeContent){
			if (content instanceof If || content instanceof Assign || content instanceof Loop ||content instanceof Expression) {
				premiumPCH.getContent().add(content);
				outOfPCHContent = true; 
			}
		}
		if(!outOfPCHContent){
			premiumPCH = null;
		}
		return premiumPCH;
	}

	/* (non-Javadoc)
	 * This method will resolve xpath of the PCH with / in its name
	 * 
	 */
	@Override
	public void resolvePCHScopeWithSlashInIt(String scopeName,PCH pchWithSlashOrAncestor,LOB lob,Database xPathDB) throws ParserException{
		String pchXpath = resolveScopeOfthePCH(pchWithSlashOrAncestor.getName(), xPathDB);
		
		
		
		
		String []arr = pchXpath.replace(".", "~").split("~");
		//String scopeName = arr[arr.length-2];
		@SuppressWarnings("unused")
		String pchName = arr[arr.length-1];
		
		
	}

	
	private String resolveScopeOfthePCH(String pchName,Database xPathDB) throws ParserException{
		String xpath  = XpathUtil.getScopePath(xPathDB, pchName);
		if("".equals(xpath)){
			//@TODO if the pch does not point to any scope then what to do in this case
			throw new ParserException("PCH WITH SLASH DO NOT POINT TO ANY EXISTING SCOPE");
		}
		return xpath;
	}

	/* (non-Javadoc)
	 * @see com.mmpnc.rating.iso.algorithm.correction.ICorrectionForPCHModularization#addListOFModulesNeedsToBeExecutedForTheScope(java.util.List, java.util.Map)
	 *	This method delegates call to the add pch execution sequence inerface
	 */
	@Override
	public void addPCHExecutionSequence(
			String referenceType,List<Scope> newScopeList, Map<String, PCHInfo> processedPCHMap) {
		IAddSequenceForPCHExecutionInScope addSeq = new AddSequenceForPCHExecutionInScopeImpl();
		addSeq.addListOFModulesNeedsToBeExecutedForTheScope(referenceType,newScopeList, processedPCHMap);
		
	}

	/* (non-Javadoc)
	 * Create execute model steps for scope pch
	 */
	@Override
	public void createExecuteModelStepsAndAddToIcmNodeContent(String referenceType,List<String> pchNameList,IcmRatingNodeContent icmRatingNodeContent, Scope scope) throws ParserException {
		//checkForTheExecuteModuleStepsAndTheModulesAvaialbeInMap(pchNameList, icmRatingNodeContent, scope);
		//GETTING ALL PCH INCLUDED IN THIS SCOPE.
		ScopeInfoHolder scopeInfoHolder = ScopeInfoHolder.getInstance();
		ScopeInfo scopeInfo = scopeInfoHolder.getScopeInfo(referenceType+scope.getDbTables()+scope.getSequenceNo()+scope.getPass());
		List<AlgoStep> algoStepList = new ArrayList<AlgoStep>();
		icmRatingNodeContent.setAlgoStepList(algoStepList);
		ExecuteModelStepCreater executeModelStepCreater = ExecuteModelStepCreater.getInstance();
		
	/*	if(scopeInfo.getPchExecSeq().size() != pchNameList.size()){
			System.err.println("Something wrong with rating node content :"+icmRatingNodeContent.getDbTables());
		}*/
		
		int executeModuleSeq = 1;
		//Creating execute model steps for the PCH in rating nodes
		for(String pchName: scopeInfo.getPchExecSeq()){
			//Creating execute module statement and adding it to the icmRatingNodeContent
			if(pchNameList.contains(pchName)){
				if(!"domainTableLookup".equals(pchName.trim())){
					ExecuteModuleStep moduleStep = executeModelStepCreater.createExecuteModelStep(scope.getFullyQualifiedPath(),pchName,executeModuleSeq++,null);
					icmRatingNodeContent.getAlgoStepList().add(moduleStep);
				}
			}else{
				//@TODO needs to throw the exception here
				//IF IT IS STATEWIDE ALGO FILE THEN YOU DO NOT NEED TO ADD THIS CHECK AS IN STATEWIDE ALGORITHMS WE DO NOT PROCESS OR CONSUME
				//ALL PCH . ONLY OVERRIDDEN PCH WILL BE CONSUMED
				//We are setting pass 10 and above for the algorithms which has been created for PCH with name like A/B 
				//for such algorithms we do not need to check the rating nodes in flowchart
				if(!BasicConfiguration.getInstance().isStateWideAlgorithms() && ! (scope.getPass() >= 10)){
					//IF IT IS NOT A STATEWIDE ALGO FILE THEN WE NEED TO THROW THE EXCEPTION 
					ExceptionHandler.raiseParserException("PCH IS NOT PROCESSED IN THIS SCOPE PLEASE CHEK WITH TOOLS TEAM :["+pchName+"]");
				}
				
			}
			
		}
		//StringBuffer sb = new StringBuffer();
		//creating execute model steps for unused pch list
		for(String pchName: scopeInfo.getUnUsedPCH()){
			//sb.append(pchName).append(",");
			if(!"domainTableLookup".equals(pchName.trim())){
				ExecuteModuleStep moduleStep = executeModelStepCreater.createExecuteModelStep(scope.getFullyQualifiedPath(),pchName,executeModuleSeq++,null);
				icmRatingNodeContent.getAlgoStepList().add(moduleStep);
			}
		}
/*	ISOConsumptionLogger.info("Execute module steps");
	ISOConsumptionLogger.info(sb);*/
	}
	
	@Override
	public void createExecuteModelStepsForStateWideAlgorithms(String referenceType,IcmRatingNodeContent icmRatingNodeContent, Scope scope) throws ParserException {
		
		ScopeInfoHolder scopeInfoHolder = ScopeInfoHolder.getInstance();
		ScopeInfo scopeInfo = scopeInfoHolder.getScopeInfo(referenceType+scope.getDbTables()+scope.getSequenceNo()+scope.getPass());
		List<AlgoStep> algoStepList = new ArrayList<AlgoStep>();
		icmRatingNodeContent.setAlgoStepList(algoStepList);
		ExecuteModelStepCreater executeModelStepCreater = ExecuteModelStepCreater.getInstance();
		
	/*	if(scopeInfo.getPchExecSeq().size() != pchNameList.size()){
			System.err.println("Something wrong with rating node content :"+icmRatingNodeContent.getDbTables());
		}*/
		
		int executeModuleSeq = 1;
		//Creating execute model steps for the PCH in rating nodes
		for(String pchName: scopeInfo.getPchExecSeq()){
			//Creating execute module statement and adding it to the icmRatingNodeContent
				if(!"domainTableLookup".equals(pchName.trim())){
					ExecuteModuleStep moduleStep = executeModelStepCreater.createExecuteModelStep(scope.getFullyQualifiedPath(),pchName,executeModuleSeq++,null);
					icmRatingNodeContent.getAlgoStepList().add(moduleStep);
				}	
		}
		//we need execute module steps for unused PCH if there are no rating nodes for a coverage 
		//in flow chart so keeping this functionality but adding a condition that scope info do not have 
		//PCH execution sequence (i.e rating or premium node)
		if(scopeInfo.getPchExecSeq().size() == 0)
		 for(String pchName: scopeInfo.getUnUsedPCH()){
			//sb.append(pchName).append(",");
			if(!"domainTableLookup".equals(pchName.trim())){
				ExecuteModuleStep moduleStep = executeModelStepCreater.createExecuteModelStep(scope.getFullyQualifiedPath(),pchName,executeModuleSeq++,null);
				icmRatingNodeContent.getAlgoStepList().add(moduleStep);
			}
		}
	}
	
}
