package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class StringEvaluator implements Evaluator {

	private Context context;
	private String string;
	public StringEvaluator(String string) {
		this.string = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException {
		context.putValue(ContextParam.ISSOURCESTATICVAR, true);
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		return new StringBuffer(string);
	}

}
