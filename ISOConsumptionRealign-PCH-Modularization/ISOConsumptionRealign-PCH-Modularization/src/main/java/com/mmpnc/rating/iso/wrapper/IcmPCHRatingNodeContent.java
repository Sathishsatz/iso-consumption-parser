package com.mmpnc.rating.iso.wrapper;

import java.util.List;

import com.mmpnc.icm.common.ide.models.AlgoStep;

public class IcmPCHRatingNodeContent {
	
	private String pchName;
	private List< AlgoStep> algoStepList;
	
	
	public String getPchName() {
		return pchName;
	}
	public void setPchName(String pchName) {
		this.pchName = pchName;
	}
	public List<AlgoStep> getAlgoStepList() {
		return algoStepList;
	}
	public void setAlgoStepList(List<AlgoStep> algoStepList) {
		this.algoStepList = algoStepList;
	}
}
