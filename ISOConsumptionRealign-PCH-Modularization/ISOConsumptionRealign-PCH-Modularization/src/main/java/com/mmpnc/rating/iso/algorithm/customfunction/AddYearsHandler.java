package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;

public class AddYearsHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context) throws RecognitionException{
		StringBuffer addYears = new StringBuffer();
		for(Evaluator eval : evallist){
			eval.setContext(context);
			Object evalObject = eval.evaluate();
			if(ExceptionHandler.checkNullOrEmpty(evalObject)){
				ExceptionHandler.raiseParserException("Argument is EMPTY or NULL in AddYears Function");
			}
			addYears.append(evalObject.toString());
			addYears.append(",");
		}
		String getAddYears = addYears.substring(0,addYears.lastIndexOf(","));
		return "AddYears ( "+getAddYears+" )";
	}

}
