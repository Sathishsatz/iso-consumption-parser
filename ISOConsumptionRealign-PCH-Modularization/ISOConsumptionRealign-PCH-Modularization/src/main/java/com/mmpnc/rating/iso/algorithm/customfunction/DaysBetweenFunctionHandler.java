package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class DaysBetweenFunctionHandler implements FunctionHandler {

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context)
			throws RecognitionException {
		StringBuffer daysBetween = new StringBuffer();
		for(Evaluator eval : evallist){
			eval.setContext(context);
			daysBetween.append(eval.evaluate().toString());
			daysBetween.append(",");
		}
		String getAddYears = daysBetween.substring(0,daysBetween.lastIndexOf(","));
		return "days_between ( "+getAddYears+" )";
	}

}
