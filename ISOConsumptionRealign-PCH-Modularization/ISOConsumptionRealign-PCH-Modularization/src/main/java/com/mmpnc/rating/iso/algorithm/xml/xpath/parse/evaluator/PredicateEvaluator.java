package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.PredicatesUtil;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.rating.iso.helper.PredicateFilterCreater;

public class PredicateEvaluator implements Evaluator {
	private Context context;
	private Evaluator expr;

	public PredicateEvaluator(Evaluator expr) {
		this.expr = expr;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		// System.out.println("Predicate Evaluator called");

		Boolean predicateParse = (Boolean)context.getValue(ContextParam.PREDICATEPARSER);
		Boolean isIfConditionPredicate =(Boolean) context.getValue(ContextParam.IFCONDITIONXPATH);
		Boolean isAssignRhsXpath = (Boolean)context.getValue(ContextParam.ISASSIGNEMENTRHSXPATH);
		
		if(isIfConditionPredicate == null)
			isIfConditionPredicate = false;
		
		if(isAssignRhsXpath == null)
			isAssignRhsXpath = false;
		
		String predicateStrOrig = null;
		
		if (!predicateParse) {
			nonPredicateParser();
		} 
		
		else {
			predicateStrOrig = predicateParser(predicateStrOrig, isIfConditionPredicate, isAssignRhsXpath);
		}
		
		/*if(isAssignRhsXpath){
			context.putValue(ContextParam.ASSIGNSATEMENTRHSPREDICATEXPATH,new StringBuffer(predicateStrOrig));
		}*/
		
		if(isIfConditionPredicate){
			context.putValue(ContextParam.IFCONDITIONPREDICATEXPATH, predicateStrOrig);
		}
		
		return Type.PATH;
	}
	
	
	private void nonPredicateParser()throws RecognitionException{
		StringBuffer buffer = (StringBuffer) this.context
		.getValue(ContextParam.XPATHSTRING);
		this.context.putValue(ContextParam.ISPREDICATE, true);
		buffer.append("[ ");
		this.expr.setContext(context);
		this.expr.evaluate();
		buffer.append(" ]");
		this.context.putValue(ContextParam.ISPREDICATE, false);
	}
	
	
	private String predicateParser(String predicateStrOrig,boolean isIfConditionPredicate,boolean isAssignRhsXpath)throws RecognitionException{
		String priorbuffer = (String) this.context.getValue(ContextParam.XPATHWITHPREDICATES);
		//priorbuffer will give you the object xpath
		//process this
		StringBuffer buffer = new StringBuffer();
		this.context.putValue(ContextParam.XPATHSTRING,buffer);
		this.context.putValue(ContextParam.ISPREDICATE,true);			
		this.expr.setContext(context);
		this.expr.evaluate();
		
		//Buffer will return the predicates/condition statements.
		// create the filters here
		
		StringBuffer preidcateExpr =(StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		
		predicateStrOrig = preidcateExpr.toString();
		
		String [] predicatesArr = PredicatesUtil.createAndReturnPredicateArray(predicateStrOrig);
		
		int index = 0;
		for(String  predicateStr : predicatesArr){
			String predicateLevel = null;
			
			//THIS HAS BEEN DONE TO AVOID INCONSISTANCY IN THE ISO ALGO FILE [SOME PRDICATES HAVE SPACES BETWEEN LHS AND THE OPERATOR AND SOME DONT] 
			predicateStr = PredicatesUtil.removeWhiteSpacesOutsideQuotes(predicateStr);
			priorbuffer = PredicatesUtil.removeWhiteSpacesOutsideQuotes(priorbuffer);
			
			
			//THIS HANDLING HAS BEEN ADDED TO REMOVE STRING FUNCTION IN THE PREDICATES
			if(predicateStr.startsWith("string")){
				predicateStr =PredicatesUtil.removeStringFunctionFromPredicate(predicateStr);
				priorbuffer = PredicatesUtil.removeStringFunctionFromPredicate(priorbuffer);
				predicateStrOrig = PredicatesUtil.removeStringFunctionFromPredicate(predicateStrOrig);
			}
			
			if(priorbuffer.contains(predicateStr)){
				//Finding current level of the predicates to get the fully qualified path
				predicateLevel = PredicatesUtil.findThePredicateLevel(predicateStr, predicateLevel, priorbuffer);
			}
			
			//Create a Filter for Memory Lookup
			//Getting LHS and RHS for the memory lookup filter
			
			//String operator = null;
			String [] predicates = null; 
			predicates = getPredicatesArrays(expr, predicates, predicateStr, index);
			
			index++;
			
			
			Database objectDataBase = (Database)context.getValue(ContextParam.OBJECTDATABASE);
			String scope = (String)context.getValue(ContextParam.SCOPE);
			
			//GETTING LHS AND RHS OF THE PREDICATE FILTER
			String filterLHS = XpathUtil.getFullyQualifiedXpath(objectDataBase, scope, PredicatesUtil.getFilterLHS(predicates, predicateLevel),true);
			String filterRHS = XpathUtil.getFullyQualifiedXpath(objectDataBase, scope, PredicatesUtil.getFilterRHS(predicates, predicateLevel),false);
			
			
			boolean isModelAttribute = false;
			//IF RHS IS NOT A MODEL ATTRIBUTE THEN WE NEED TO ASSIGN RHS CONSTANT VALUE TO THE RHS OF FILTER
			if(!"".equals(filterRHS)){
				isModelAttribute = true;
			}else{
				filterRHS = predicates[1];
			}
			
			//In case of if condition predicates we do not need to create memory lookup instead we have to add this
			//condition in the if condition
			if(isIfConditionPredicate ){
				predicateStrOrig = handlePredicatesInIfCondition(predicateStrOrig, filterLHS, filterRHS, predicates[0], predicates[1]);
				predicateStrOrig = predicateStrOrig.replaceAll(" and(?=([^\"']*[\"'][^\"']*[\"'])*[^\"']*$)", " && ");
				predicateStrOrig = predicateStrOrig.replaceAll(" or(?=([^\"']*[\"'][^\"']*[\"'])*[^\"']*$)", " || ");
				if(predicateStrOrig.contains(" = ")){
					predicateStrOrig = predicateStrOrig.replace(" = ", " == ");
				}
			}
				
			else{
				createAndAddTheFilterForMemLookup(filterLHS, filterRHS, isModelAttribute);
			}
			//Setting ispredicate flag to false 
			this.context.putValue(ContextParam.ISPREDICATE,false);
			
			
		}
		return predicateStrOrig;
	}
	
	/**
	 * 
	 * This method will create filters for the memory lookup we are creating for the xpath predicate
	 * 
	 * @param filterLHS
	 * @param filterRHS
	 * @param isModelAttribute
	 */
	private void createAndAddTheFilterForMemLookup(String filterLHS,String filterRHS,boolean isModelAttribute){
		
		int filterOrder = (Integer)context.getValue(ContextParam.PREDICATEFILTERORDER)+1;
		context.putValue(ContextParam.PREDICATEFILTERORDER,filterOrder);
		SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = PredicateFilterCreater.createMemoryLookupFilter(context, operator, filterLHS, filterRHS, isModelAttribute,filterOrder);
		@SuppressWarnings("unchecked")
		List<SingleFieldConstraintEBLeftSide> filterList = (List<SingleFieldConstraintEBLeftSide>)context.getValue(ContextParam.LISTOFMEMORYLOOKFILTERS);
		filterList.add(singleFieldConstraintEBLeftSide);
	}
	
	private String prevlhsCont = null;
	private String prevRhsCont = null;
	/**
	 * @param predicateStrOrig
	 * @param filterLHS
	 * @param filterRHS
	 * @param lhsContent
	 * @param rhsContent
	 * @return
	 * This method will handle predicates in if condition
	 */
	private String handlePredicatesInIfCondition(String predicateStrOrig ,String filterLHS ,String filterRHS,String lhsContent,String rhsContent){
		if(!lhsContent.equals(prevlhsCont))
		predicateStrOrig = predicateStrOrig.replace(lhsContent, filterLHS);
		if(!rhsContent.equals(prevRhsCont))
		predicateStrOrig = predicateStrOrig.replace(rhsContent, filterRHS);
		prevlhsCont = lhsContent;
		prevRhsCont = rhsContent;
		//String n = "customXpathRatingFunction";
		return predicateStrOrig;
	}
	
	/**
	 * @param expr
	 * @param predicates
	 * @param predicateStr
	 * @param index
	 * @return
	 */
	private String[] getPredicatesArrays(Evaluator expr,String []predicates,String predicateStr ,int index){
		
		if(expr instanceof EqualsEvaluator){
			predicates = predicateStr.split("=");
			operator = "==";
		}
		else if (expr instanceof NotEqualsEvaluator){
			predicates = predicateStr.split("!=");
			operator = "!=";
		}
		else if(expr instanceof GreaterThanEvaluator){
			predicates = predicateStr.split(">");
			operator = ">";
		}
		else if(expr instanceof GreaterThanEqualsEvaluator){
			predicates = predicateStr.split(">=");
			operator = ">=";
		}
		else if(expr instanceof AndEvaluator){
			AndEvaluator andEvaluator = (AndEvaluator)expr;
			if(index == 0){
				expr = andEvaluator.left;
				predicates = getPredicatesArrays(expr, predicates, predicateStr, index);
			}
			else if(index == 1 || index >1){
				expr = andEvaluator.right;
				predicates = getPredicatesArrays(expr, predicates, predicateStr, index);
			}
		}
		else if(expr instanceof OrEvaluator){
			OrEvaluator orEvaluator = (OrEvaluator)expr;
			if(index == 0){
				expr = orEvaluator.left;
				predicates = getPredicatesArrays(expr, predicates, predicateStr, index);
			}
			else if(index == 1 || index >1){
				expr = orEvaluator.right;
				predicates = getPredicatesArrays(expr, predicates, predicateStr, index);
			}
		}
		return predicates;
	}
	
	private String operator;
	

}
