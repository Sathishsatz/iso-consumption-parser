package com.mmpnc.rating.iso.config;

import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;



/**
 * @author nilkanth9581
 *
 */
public class Configurer implements IConfiguration{

	private static Configurer configurer = null;
	private BasicConfiguration basicConfiguration;
	private DBConfiguration dbConfiguration;
	private RatingCategoryConfiguration ratingCategoryConfiguration = null;
	private Configurer() {
		
	}
	
	@Override
	public Configurer buildConfigurer(BasicConfiguration basicConfiguration,DBConfiguration dbConfiguration,RatingCategoryConfiguration ratingCategoryConfiguration){
		this.basicConfiguration = basicConfiguration;
		if(basicConfiguration == null){
			ISOConsumptionLogger.info("CONFIGURATION NOT FOUND");
		}
		this.dbConfiguration = dbConfiguration;
		this.ratingCategoryConfiguration = ratingCategoryConfiguration;
		return this;
	}
	
	
	public static Configurer getInstance(){
		if(configurer == null){
			configurer = new Configurer();
		}
		return configurer;
	}
	
	
	private void readBasicProperties(String fileLcoation) {
		Configuration basicConfiguration = BasicConfiguration.getInstance();
		this.basicConfiguration =(BasicConfiguration) basicConfiguration.createConfiguration(fileLcoation);
	}

	public void readDatabaseProperties(String fileLocation) {
		Configuration dbConfiguration = DBConfiguration.getInstance();
		this.dbConfiguration =(DBConfiguration)dbConfiguration.createConfiguration(fileLocation);
	} 
	
	private void readRatingCategoryProperties(String fileLocation) {
		Configuration raingCatConfig = RatingCategoryConfiguration.getInstance();
		this.ratingCategoryConfiguration = (RatingCategoryConfiguration)raingCatConfig.createConfiguration(fileLocation);
	}

	@Override
	public void configure(String fileLocation) {
		readBasicProperties(fileLocation);
		readDatabaseProperties(fileLocation);
		readRatingCategoryProperties(fileLocation);
		
	}
	
	
	public void configure(Configurer configurer) {
		this.basicConfiguration = configurer.getBaseConfig() != null ? configurer.getBaseConfig() : null;
		this.dbConfiguration = configurer.getDBConfig() != null ? configurer.getDBConfig() : null;
		this.ratingCategoryConfiguration = configurer.getRatingCatConfig() != null ? configurer.getRatingCatConfig() : null;
	}

	@Override
	public BasicConfiguration getBaseConfig() {
		// TODO Auto-generated method stub
		return basicConfiguration;
	}

	@Override
	public DBConfiguration getDBConfig() {
		// TODO Auto-generated method stub
		return dbConfiguration;
	}

	@Override
	public RatingCategoryConfiguration getRatingCatConfig() {
		// TODO Auto-generated method stub
		return ratingCategoryConfiguration;
	}

	
}
