package com.mmpnc.rating.iso.wrapper;

import java.util.List;
import java.util.Map;

import com.mmpnc.icm.common.ide.models.LookupModels;

/**
 * @author nilkanth9581
 *
 */
public class ICMRequiredData {
	
	//private LookupModels lookupModels;
	LookupModels  lookupModels;
	//KEY of this map will be the algorithm type and is the first node in ICM 
	Map<String, List<IcmRatingNodeContent>> icmRatingContent;
	//List<IcmRatingNodeContent> ratingNodeContents;
	
	StringBuffer lookupNames;
	
	//PROPERTIES ADDED FOR MULI STATE RATING REQUESTS
	
	private boolean isMulitState;
	private int productIDLevel;
	
	
	
	public boolean isMulitState() {
		return isMulitState;
	}


	public void setMulitState(boolean isMulitState) {
		this.isMulitState = isMulitState;
	}


	public int getProductIDLevel() {
		return productIDLevel;
	}


	public void setProductIDLevel(int productIDLevel) {
		this.productIDLevel = productIDLevel;
	}


	
	
	private ICMRequiredData(){
		
	}
	
	private static ICMRequiredData icmRequiredData ;
	
	public static ICMRequiredData getInstance(){
		if(icmRequiredData == null){
			icmRequiredData = new ICMRequiredData();
		}
		return icmRequiredData;
	}
	
	
	public StringBuffer getLookupNames() {
		return lookupNames;
	}

	public void setLookupNames(StringBuffer lookupNames) {
		this.lookupNames = lookupNames;
	}

	public void setIcmRatingContent(
			Map<String, List<IcmRatingNodeContent>> icmRatingContent) {
		this.icmRatingContent = icmRatingContent;
	}

	public Map<String, List<IcmRatingNodeContent>> getIcmRatingContent() {
		return icmRatingContent;
	}

	public LookupModels getLookupModels() {
		return lookupModels;
	}
	
	public void setLookupModels(LookupModels lookupModels) {
		this.lookupModels = lookupModels;
	}
	
}
