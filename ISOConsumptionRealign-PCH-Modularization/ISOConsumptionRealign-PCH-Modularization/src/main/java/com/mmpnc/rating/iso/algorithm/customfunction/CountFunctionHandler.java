package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;
import org.apache.commons.lang.StringUtils;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.VarEvaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.XpathEvaluator;

/**
 * @author nilkanth9581
 *	THIS CLASS WILL HANDLE COUNT FUNCTION IN ALGORITHM FILE
 */
public class CountFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String varaible, Context context) throws RecognitionException{
		String count = null;
		for(Evaluator eval : evallist){
			eval.setContext(context);
			context.putValue(ContextParam.PREDICATEWITHCOUNTFUNCTION,true);
			count = eval.evaluate().toString();
			if(ExceptionHandler.checkNullOrEmpty(count)){
				ExceptionHandler.raiseParserException("Argument is EMPTY or NULL in Count Function");
			}
			int dotcount = StringUtils.countMatches(count, ".");
			if((eval instanceof XpathEvaluator || eval instanceof VarEvaluator) && dotcount>1 && !count.contains("["))
				count = "\""+count+"\"";
			
		}
		return "Count(SRERequest ,"+count+")";	
	}

}
