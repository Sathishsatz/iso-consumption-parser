package com.mmpnc.rating.iso.wrapper;

import java.util.List;
import java.util.Map;

import com.mmpnc.icm.common.ide.models.AlgoStep;


/**
 * @author nilkanth9581
 *
 */
public class IcmRatingNodeContent {
	
	
	private String algorithmLevel;
	private List<AlgoStep> algoStepList;
	private int pass;
	private String dbTables;
	private StringBuffer algorithmText;
	private String effectiveDate;
	private String expirationDate;
	private String programName;
	private String state;
	private String productName;
	private String refType;
	private int sequenceNo;
	
	private  Map<String,List<AlgoStep>> pchNameAndAlgoSteps;
	
	
	public Map<String, List<AlgoStep>> getPchNameAndAlgoSteps() {
		return pchNameAndAlgoSteps;
	}
	public void setPchNameAndAlgoSteps(
			Map<String, List<AlgoStep>> pchNameAndAlgoSteps) {
		this.pchNameAndAlgoSteps = pchNameAndAlgoSteps;
	}
	/*public String getPchName() {
		return pchName;
	}
	public void setPchName(String pchName) {
		this.pchName = pchName;
	}*/
	public int getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public StringBuffer getAlgorithmText() {
		return algorithmText;
	}
	public void setAlgorithmText(StringBuffer algorithmText) {
		this.algorithmText = algorithmText;
	}
	public String getDbTables() {
		return dbTables;
	}
	public void setDbTables(String scopePath) {
		this.dbTables = scopePath;
	}
	public int getPass() {
		return pass;
	}
	public void setPass(int pass) {
		this.pass = pass;
	}
	public String getAlgorithmLevel() {
		return algorithmLevel;
	}
	public void setAlgorithmLevel(String algorithmLevel) {
		this.algorithmLevel = algorithmLevel;
	}
	public List<AlgoStep> getAlgoStepList() {
		return algoStepList;
	}
	public void setAlgoStepList(List<AlgoStep> algoStepList) {
		this.algoStepList = algoStepList;
	}
}
