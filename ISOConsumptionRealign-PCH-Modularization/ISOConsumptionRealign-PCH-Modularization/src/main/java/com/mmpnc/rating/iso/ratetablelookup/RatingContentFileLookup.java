package com.mmpnc.rating.iso.ratetablelookup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.content.vo.RateTable;
import com.mmpnc.rating.iso.content.vo.RateTableKey;
import com.mmpnc.rating.iso.content.vo.RateTableValue;
import com.mmpnc.rating.iso.helper.Constants;
import com.thoughtworks.xstream.XStream;

/**
 * @author nilkanth9581
 *
 */
public class RatingContentFileLookup {
	private static Map<String,String> xmlMap = null;
	private static Database db = null;
	private static RatingContentFileLookup ratingContentFileLookup = null;
	private RatingContentFileLookup(){}
	
	
	public static synchronized RatingContentFileLookup getInstance(){
		if(ratingContentFileLookup == null){
			ratingContentFileLookup =  new RatingContentFileLookup();
		}
		return ratingContentFileLookup;
	}
	
	/**
	 * @param contentFileMap
	 */
	private static int counterForContentFile ;
	
	public Database buildXmlDatabase(List <String>contentFileList) throws BasexException{
		xmlMap = new HashMap<String, String>();
		for(String contentFile:contentFileList){
			counterForContentFile = counterForContentFile +1;
			String contentFileName = "contentFileName"+counterForContentFile;
			xmlMap.put(contentFileName,contentFile);
		}
		db = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMERCDB, xmlMap);
		return db;
	}
	
	/**
	 * @param functionName
	 * @return
	 * THIS METHOD WILL CHECK WHETHER ONE OF THE RATETABLE KEY  IS FOR INTERPOLATE OR THE EXACT MATCH
	 * OR GREATERTHANOREQUAL OR LESSTHANOREQUAL 
	 * @throws Exception 
	 * 
	 */
	public Map<RTKEY_LOOKUP_TYPE,List<String>> getInterPolateKey(String functionName) throws Exception{
		Connection con = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(con);
		String queryStr = "//RateTable[Name='"+functionName.trim()+"']/RateTableKey";
		query.createQuery(queryStr);
		
		String rateTableKeys = "<RateTable>"+(String) query.execute()+"</RateTable>";
		//CREATING A MAP WHICH WILL HOLD THE LOOKUP_TYPE AND RELATED KEY LIST
		Map<RTKEY_LOOKUP_TYPE,List<String>> lookuptypemap = new HashMap<RTKEY_LOOKUP_TYPE,List<String>>();
		XStream xstream = new XStream();
		xstream.alias("RateTable", RateTable.class);
		xstream.alias("RateTableKey", RateTableKey.class);
		xstream.alias("RateTableValue", RateTableValue.class);
		xstream.addImplicitCollection(RateTable.class, "rateTableKey", RateTableKey.class);
		xstream.addImplicitCollection(RateTable.class, "rateTableValue", RateTableValue.class);
		//xstream.addImplicitCollection(RateTable.class, "rateTableKey");
		//xstream.addImplicitCollection(RateTable.class, "rateTableValue");
		
		RateTable ratetable  = (RateTable)xstream.fromXML(rateTableKeys);
		
		for(RateTableKey rateTableKey:ratetable.getRateTableKey()){
			String lookupType = rateTableKey.getLookupType().trim();
			if("Interpolate".equals(lookupType)){
				List<String> interpolatedKeyList = new ArrayList<String>();
				interpolatedKeyList.add(rateTableKey.getName());
				lookuptypemap.put(RTKEY_LOOKUP_TYPE.INTERPOLATE, interpolatedKeyList); 
			}else if("LessThanOrEqual".equals(lookupType)){
				List<String> lessThanOrEqualKeyList = new ArrayList<String>();
				lessThanOrEqualKeyList.add(rateTableKey.getName());
				lookuptypemap.put(RTKEY_LOOKUP_TYPE.LESSTHANOREQUAL, lessThanOrEqualKeyList);
			}else if("GreaterThanOrEqual".equals(lookupType)){
				List<String> greaterThanOrEqualKeyList = new ArrayList<String>();
				greaterThanOrEqualKeyList.add(rateTableKey.getName());
				lookuptypemap.put(RTKEY_LOOKUP_TYPE.GREATERTHANOREQUAL, greaterThanOrEqualKeyList);
			}
		}
		return lookuptypemap;
	}
	
	
	/**
	 * @param functionName
	 * @return
	 * THIS METHOD WILL CHECK WHETHER THE KEY IS INTERPOLATE 
	 * @throws Exception 
	 * 
	 */
	public boolean isKeyInterpolate(String functionName,String keyName) throws Exception{
		Connection con = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(con);
		String queryStr = "//RateTable[Name='"+functionName.trim()+"']/RateTableKey[Name='"+keyName+"']/LookupType";
		query.createQuery(queryStr);
		Object returnObject  = query.execute();
		if("Interpolate".equals(returnObject.toString())){
			return true;
		}
		return false;
	}
	
	public Map<String,Object> getRatingTableKeysAndValues(String functionName) throws Exception{
		
		if(db == null){
			throw new Exception(" ---- XML DATABSE IS NOT CREATED ------");
		}
		
		Map <String,Object> ratetablekeyvalue = new HashMap<String, Object>(); 
		
		Connection con = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(con);
		
		String queryStr = "//RateTable[Name='"+functionName.trim()+"']/RateTableKey";
		query.createQuery(queryStr);
		
		String rateTableKeys = "<RateTable>"+(String) query.execute()+"</RateTable>";
		
		XStream xstream = new XStream();
		xstream.alias("RateTable", RateTable.class);
		xstream.alias("RateTableKey", RateTableKey.class);
		xstream.alias("RateTableValue", RateTableValue.class);
		xstream.addImplicitCollection(RateTable.class, "rateTableKey", RateTableKey.class);
		xstream.addImplicitCollection(RateTable.class, "rateTableValue", RateTableValue.class);
		//xstream.addImplicitCollection(RateTable.class, "rateTableKey");
		//xstream.addImplicitCollection(RateTable.class, "rateTableValue");
		
		RateTable ratetable  = (RateTable)xstream.fromXML(rateTableKeys);
		
		//Set <String>keysSet = new HashSet<String>();
		Map<Integer,String> ratetableKeysMap = new TreeMap<Integer, String>();
		
		if(ratetable.getRateTableKey() != null )
			for(RateTableKey rateTableKey : ratetable.getRateTableKey()){
				ratetableKeysMap.put(rateTableKey.getSequence(),rateTableKey.getName());
				//keysSet.add(rateTableKey.getName());
			}
		
		
		
		String valueQuery = "//RateTable[Name='"+functionName+"']/RateTableValue";
		query.createQuery(valueQuery);
		String ratetableValue = "<RateTable>"+(String) query.execute()+"</RateTable>";
		
		RateTable ratetableValues  = (RateTable)xstream.fromXML(ratetableValue);
		
	/*	if(ratetableValues .getRateTableValues().size() > 1){
			throw new Exception("MORE THAN ONE VALUE FOUND FOR A RATE TABLE!!!!");
		}*/
		if(ratetableValues.getRateTableValue() != null)
		for(RateTableValue rateTableValue : ratetableValues.getRateTableValue()){
			ratetablekeyvalue.put(Constants.LOOKUP_RATETABLE_VALUE, rateTableValue.getColumnName());
		}
		ratetablekeyvalue.put(Constants.LOOKUP_RATETABLE_KEYS, ratetableKeysMap);
		
		return ratetablekeyvalue;
	}
	
	public String getRateTableValue(){
		return null;
	}
}
