package com.mmpnc.rating.iso.config;

import java.util.Arrays;
import java.util.List;

import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.propertyreader.PropertyFileReader;
import com.mmpnc.rating.iso.propertyreader.PropertyParam;

/**
 * 
 * @author nilkanth9581
 *
 */
public class BasicConfiguration implements Configuration {
	
	private BasicConfiguration(){}
	
	private static BasicConfiguration configuration = new BasicConfiguration();
	
	public static BasicConfiguration getInstance(){
			return configuration;
	}
	
	private boolean createLangeuageTextFile;
	private String languageTextFileName;
	private String languageTextFileDirectory;
	private String icmInputXmlFileLocation;
	private String sortedAlgoFileLocation;
	private List<String> ratingContentFilesList;
	private String globalLookupFileLocation;
	private String icmCategoryNameForRatingLookup;
	private String lineOfBusiness;
	private String publishLookupModels;
	private String dsFileLocation;
	private String publishAlgorithmSteps;
	private String fileName;
	private boolean isStateWideAlgorithms;
	
	
	public boolean isStateWideAlgorithms() {
		return isStateWideAlgorithms;
	}

	public void setStateWideAlgorithms(boolean isStateWideAlgorithms) {
		this.isStateWideAlgorithms = isStateWideAlgorithms;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPublishAlgorithmSteps() {
		return publishAlgorithmSteps;
	}

	public void setPublishAlgorithmSteps(String publishAlgorithmSteps) {
		this.publishAlgorithmSteps = publishAlgorithmSteps;
	}

	public String getDsFileLocation() {
		return dsFileLocation;
	}

	public void setDsFileLocation(String dsFileLocation) {
		this.dsFileLocation = dsFileLocation;
	}

	public String getPublishLookupModels() {
		return publishLookupModels;
	}

	public void setPublishLookupModels(String publishLookupModels) {
		this.publishLookupModels = publishLookupModels;
	}

	
	public String getIcmCategoryNameForRatingLookup() {
		return icmCategoryNameForRatingLookup;
	}

	public void setIcmCategoryNameForRatingLookup(String icmCategoryNameForRaingLookup) {
		this.icmCategoryNameForRatingLookup = icmCategoryNameForRaingLookup;
	}
		
	public String getGlobalLookupFileLocation() {
		return globalLookupFileLocation;
	}
	
	public void setGlobalLookupFileLocation(String globalLookupFileLocation) {
		this.globalLookupFileLocation = globalLookupFileLocation;
	}
	
	public List<String> getRatingContentFilesList() {
		return ratingContentFilesList;
	}

	public void setRatingContentFilesList(List<String> ratingContentFilesList) {
		this.ratingContentFilesList = ratingContentFilesList;
	}
	
	public boolean isCreateLangeuageTextFile() {
		return createLangeuageTextFile;
	}
	public void setCreateLangeuageTextFile(boolean createLangeuageTextFile) {
		this.createLangeuageTextFile = createLangeuageTextFile;
	}
	public String getLanguageTextFileName() {
		return languageTextFileName;
	}
	public void setLanguageTextFileName(String languageTextFileName) {
		this.languageTextFileName = languageTextFileName;
	}
	public String getLanguageTextFileDirectory() {
		return languageTextFileDirectory;
	}
	public void setLanguageTextFileDirectory(String languageTextFileDirectory) {
		this.languageTextFileDirectory = languageTextFileDirectory;
	}
	public String getIcmInputXmlFileLocation() {
		return icmInputXmlFileLocation;
	}
	public void setIcmInputXmlFileLocation(String icmInputXmlFileLocation) {
		this.icmInputXmlFileLocation = icmInputXmlFileLocation;
	}
	public String getSortedAlgoFileLocation() {
		return sortedAlgoFileLocation;
	}
	public void setSortedAlgoFileLocation(String sortedAlgoFileLocation) {
		this.sortedAlgoFileLocation = sortedAlgoFileLocation;
	}
	
	public String getLineOfBusiness() {
		return lineOfBusiness;
	}
	
	public void setLineOfBusiness(String lineOfBusiness) {
		this.lineOfBusiness = lineOfBusiness;
	}

	/*public static Configuration getInstance(){
		return new Configuration();
	}*/


	@Override
	public BasicConfiguration createConfiguration(String fileLocation) {
		
		PropertyFileReader propertyFileReader = new PropertyFileReader(fileLocation);
		BasicConfiguration configuration = new BasicConfiguration();
		//propertyFileReader.readPropertyFile();
		if("Y".equals(propertyFileReader.getProperty(PropertyParam.CREATE_LANGUAGE_TEXT_FILE))){
			configuration.setCreateLangeuageTextFile(true);    
		}
		
		configuration.setIcmInputXmlFileLocation(propertyFileReader.getProperty(PropertyParam.ICM_INPUT_XML_LOCATION));
		configuration.setLanguageTextFileDirectory(propertyFileReader.getProperty(PropertyParam.LANGUAGE_TEXT_FILE_DIRECTORY));
		configuration.setLanguageTextFileName(propertyFileReader.getProperty(PropertyParam.LANGUAGE_TEXT_FILE_NAME));
		configuration.setSortedAlgoFileLocation(propertyFileReader.getProperty(PropertyParam.SORTED_ALGORITHM_XML_FILE_LOCATION));
		configuration.setGlobalLookupFileLocation(propertyFileReader.getProperty(PropertyParam.ICM_INPUT_GLOBAL_LOOKUP_LOCATION));
		configuration.setIcmCategoryNameForRatingLookup(propertyFileReader.getProperty(PropertyParam.ICM_CATEGORY_FOR_RATING_LOOKUP));
		configuration.setLineOfBusiness(propertyFileReader.getProperty(PropertyParam.LINE_OF_BUSINESS));
		configuration.setPublishLookupModels(propertyFileReader.getProperty(PropertyParam.PUBLISH_LOOKUP_MODELS));
		configuration.setDsFileLocation(propertyFileReader.getProperty(PropertyParam.DTAT_STRUCTURE_FILE_LOCATION));
		
		String contentFiles = propertyFileReader.getProperty(PropertyParam.RATING_CONTENT_FILE_LOCATION);
		if(contentFiles == null){
			ISOConsumptionLogger.info("------CONTENT FILE LOCATION IS NOT SPECIFIED----");
		}
		
		String files[] = contentFiles.split(",");
		configuration.setRatingContentFilesList(Arrays.asList(files));
		return configuration;
		
	}
	
	
}
