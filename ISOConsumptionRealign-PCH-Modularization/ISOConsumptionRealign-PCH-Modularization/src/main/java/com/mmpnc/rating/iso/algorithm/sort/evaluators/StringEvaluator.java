package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class StringEvaluator implements Evaluator, IEvaluatorValidator {
	private String string;
	
	public StringEvaluator(String string) {
		this.string = string;
	}

	@Override
	public void setContext(Context context) {
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		StringBuffer returnValue = new StringBuffer();
		returnValue.append(" ").append(this.string).append(" ");
		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.string==null){
			throw new ParserException("String value is NULL in String evaluator");
		}
	}

}
