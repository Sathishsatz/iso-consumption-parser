package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.PredicatesUtil;

public class GreaterThanEvaluator implements Evaluator {

	private Evaluator left;
	private Evaluator right;
	private Context context;
	
	public GreaterThanEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		left.setContext(context);
		StringBuffer greaterThan = new StringBuffer();
		greaterThan.append(" ( ");
		greaterThan.append(left.evaluate());
		greaterThan.append(" > ");
		right.setContext(context);
		greaterThan.append(right.evaluate());
		PredicatesUtil.addIfPredicateConition(context, greaterThan);
		greaterThan.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return greaterThan;
	}

}
