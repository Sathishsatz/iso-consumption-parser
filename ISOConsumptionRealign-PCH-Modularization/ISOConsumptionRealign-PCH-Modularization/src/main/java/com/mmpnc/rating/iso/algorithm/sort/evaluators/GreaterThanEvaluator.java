package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class GreaterThanEvaluator implements Evaluator, IEvaluatorValidator {
	private Evaluator left;
	private Evaluator right;
	private Context context;
	
	public GreaterThanEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		
		//returnValue.append(" ( ");
		this.left.setContext(context);
		Object lhsObject = this.left.evaluate();
		if(ExceptionHandler.checkNullOrEmpty(lhsObject)){
			ExceptionHandler.raiseParserException("LHS object can not be NULL or EMPTY in > evaluator");
		}
		returnValue.append(lhsObject);
		
		returnValue.append(">");
	
		this.right.setContext(context);
		Object rhsObject = this.right.evaluate();
		if(ExceptionHandler.checkNullOrEmpty(rhsObject)){
			ExceptionHandler.raiseParserException("RHS object can not be NULL or EMPTY in > evaluator");
		}
		returnValue.append(rhsObject);
		//returnValue.append(" ) ");
		
		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if((this.left == null) && (this.right==null)){
			throw new ParserException("Left and Right evaluators are NULL in > evaluator");
		}
		if(this.left == null){
			throw new ParserException("Left evaluator is NULL in > evaluator");
		}
		if(this.right == null){
			throw new ParserException("Right evaluator is NULL in > evaluator");
		}			
	}
}
