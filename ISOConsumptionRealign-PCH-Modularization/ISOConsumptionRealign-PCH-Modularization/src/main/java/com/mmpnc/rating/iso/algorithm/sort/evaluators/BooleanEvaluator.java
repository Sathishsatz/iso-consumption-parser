package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class BooleanEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private Evaluator left;
	public BooleanEvaluator(Evaluator bool) {
		this.left = bool;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.left == null){
			throw new ParserException("Left evaluator is NULL in BOOLEAN evaluator");
		}
	}
	
	@Override
	public Object evaluate() throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append(" (");
		
		this.left.setContext(context);
		Object lhsObject = this.left.evaluate();
		if(ExceptionHandler.checkNullOrEmpty(lhsObject.toString())){
			ExceptionHandler.raiseParserException("Argument can not be NULL in Boolean evaluator");
		}
		returnValue.append(lhsObject).append(") ");
		
		
		return returnValue;
	}
}
