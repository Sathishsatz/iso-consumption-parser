package com.mmpnc.rating.iso.config;




/**
 * @author nilkanth9581
 *
 */
public interface IConfiguration {
	void configure(String fileLocation);
	public Configurer buildConfigurer(BasicConfiguration basicConfiguration,DBConfiguration dbConfiguration,RatingCategoryConfiguration ratingCategoryConfiguration);
	BasicConfiguration getBaseConfig();
	DBConfiguration getDBConfig();
	RatingCategoryConfiguration getRatingCatConfig();
}
