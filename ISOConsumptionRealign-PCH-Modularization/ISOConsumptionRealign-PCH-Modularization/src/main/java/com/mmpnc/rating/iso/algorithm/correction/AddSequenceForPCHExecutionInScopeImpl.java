package com.mmpnc.rating.iso.algorithm.correction;

import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.vo.Scope;

public class AddSequenceForPCHExecutionInScopeImpl  implements IAddSequenceForPCHExecutionInScope{

	@Override
	public void addListOFModulesNeedsToBeExecutedForTheScope(String referenceType,
			List<Scope> newScopeList, Map<String,PCHInfo> processedPCHMap) {
		
		
		for(Scope scope:newScopeList){
			//Get the list of pch in from processedPCHMap
			
			ScopeInfo scopeInfo = new ScopeInfo();
			scopeInfo.setPass(scope.getPass());
			scopeInfo.setScopeName(scope.getDbTables());
			scopeInfo.setSequenceNo(scope.getSequenceNo());
			String mapKey = referenceType+scope.getDbTables()+scope.getSequenceNo()+scope.getPass();
			scopeInfo.setPchExecSeq(processedPCHMap.get(mapKey).getPchListInRatingNode());
			scopeInfo.setIntermediaryUsedPCH(processedPCHMap.get(mapKey).getIntermediaryUsedPCH());
			scopeInfo.setUnUsedPCH(processedPCHMap.get(mapKey).getUnusedPCH());
			
			ScopeInfoHolder scopeInfoHolder = ScopeInfoHolder.getInstance();
			scopeInfoHolder.addToScopeInfoMap(mapKey, scopeInfo);
		}
		
	}

}
