package com.mmpnc.rating.iso.algorithm.correction;

import java.util.List;
import java.util.Map;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *	This interface will hold all the functionality added for pch modularization
 *  while correcting algorithms using flow chart
 *  
 */
public interface ICorrectionForPCHModularization {
	/**
	 * @param scope
	 * @return
	 * This method is added to include out of PCH statements and create a new PCH with name Premium
	 * 
	 */
	public PCH createPremiumPCHAndAddOutOFPCHStatements(Scope scope) throws ParserException;
	/**
	 * THis method will resolve actual Scope of the PCH which name has slash in it
	 * The solution is to find actual scope of that PCH and add that PCH in that particular scope
	 * e.g. if CommercialAuto 
	 */
	public void resolvePCHScopeWithSlashInIt(String scopeName,PCH pchWithSlashOrAncestor,LOB lob,Database xPathDB) throws ParserException;
	
	
	/**
	 *This method will be used to add the execute model steps using flowchart
	 * 
	 *//*
	public void addExecuteModelStepsToCoverage();
	
	
	/**
	 * Every scope will have a queue for the modules we need to execute 
	 * 
	 * 
	 */
	public void addPCHExecutionSequence(String referenceType,List<Scope>newScopeList,Map<String,PCHInfo> processedPCHMap);
	
	/**
	 * @param icmRatingNodeContent
	 * @param scope
	 * 
	 * This method will create execute model steps according to flowchart rating nodes
	 * 
	 */
	public void createExecuteModelStepsAndAddToIcmNodeContent(String referenceType,List<String> pchNameList,IcmRatingNodeContent icmRatingNodeContent,Scope scope) throws ParserException;
	
	/**
	 * @param referenceType
	 * @param pchQueue
	 * @param icmRatingNodeContent
	 * @param scope
	 * @throws ParserException
	 */
	public void createExecuteModelStepsForStateWideAlgorithms(String referenceType,IcmRatingNodeContent icmRatingNodeContent, Scope scope) throws ParserException;
	
}	
