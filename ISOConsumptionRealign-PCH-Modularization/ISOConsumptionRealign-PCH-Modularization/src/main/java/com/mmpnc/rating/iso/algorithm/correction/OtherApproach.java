package com.mmpnc.rating.iso.algorithm.correction;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.RatingHolder;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.util.VariableType;
import com.thoughtworks.xstream.XStream;

/**
 * @author Nilkanth9581
 *
 */
public class OtherApproach {
	private Map<String,List<String>> processedPCHMap ;
	private Database createDataBaseForFlowChart(LOB lob) throws BasexException{
	
		Database flowChartDb = null;
		for(Object lobContent:lob.getContent())
		{
			
			if(lobContent instanceof Reference){
				Reference ref = (Reference)lobContent;
				
				
				if (((Reference) ref).getName().equals("FlowChart")) {
					
					Reference reference =  (Reference)ref;
						//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
					String refInXml = null;
					try {
						refInXml = JAXBUtils.writeFromObject(reference);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("FLWOCHARTDATABSE", refInXml);

					flowChartDb = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEFLOWCHARTDB, xmlMap);
					flowChartDb.buildDatabase();
					
				}
			}

		}
		return flowChartDb;
	}
	
	/**
	 * @param database
	 * Method is for closing the basex database
	 * @throws BasexException 
	 * 
	 */
	private void closeDatabase(Database database,String dbName) throws BasexException{
		database.closeDatabase();
		//database.dropDatabase(dbName);
	}
	
	/**
	 * @param reference
	 * @return
	 * THIS METHOD WILL CREATE BASEX DATABASE FOR THE CURRENT REFERENCE
	 * @throws BasexException 
	 * 
	 */
	private Database createCurrentScopeDtabase(Scope scope) throws BasexException{
		//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
		Database currentReferenceDatabase = null;
		String refInXml = null;
		try {
			refInXml = JAXBUtils.writeFromObject(scope);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Map<String, String> xmlMap = new HashMap<String, String>();
		xmlMap.put("CURRENTSCOPE", refInXml);
	
		currentReferenceDatabase = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.CURRENTSCOPEDB, xmlMap);
		currentReferenceDatabase.buildDatabase();
		return currentReferenceDatabase;
	}
	
	
	/**
	 * @param lob
	 * @return
	 * THIS METHOD WILL REARRAGE THE ALGORITHM ACCORDING TO THE FLOW CHART VARIABLE NAMES
	 * @throws Exception 
	 * 
	 */
	public LOB rearrageLOBUsingFlowChart(LOB lob) throws Exception {
		StringBuffer nonProcessedScopes = new StringBuffer();
		ISOConsumptionLogger.info("************************* Starting Rearranging LOB using FlowChart ************");
		processedPCHMap = new HashMap<String, List<String>>();
		cosolidatedReportList = new ArrayList<String>(); 
		Database flowChartDatabase = createDataBaseForFlowChart(lob);
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference)ref;
				//CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType())){
					readReference((Reference) ref,flowChartDatabase,nonProcessedScopes);
				}
				
			}
		}
		flowChartDatabase.closeDatabase();
		//PRINTING THE LOG FOR OWN REFERENCE
		printReport(true,nonProcessedScopes);
		//WRTING COMPLETE REPORT
		return lob;
	}

	private void readReference(Reference reference, Database flowChartDB,StringBuffer nonProcessedScopes) throws Exception {
		
		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {
				Scope curScope = (Scope)scope;
				//int indexOfScope = scopeList.indexOf(curScope);
				//reference.getContent().remove(curScope);
				//CREATING BASEX DATABSE FOR THE CURRENT SCOPE
				Database currentScopeDB = createCurrentScopeDtabase(curScope);
				readScope(reference,(Scope) scope,flowChartDB,currentScopeDB,nonProcessedScopes);
				//CLOSING DATABASE AS THE CURRENT SCOPE ALGORITHM SPLIT IS COMPLETE
				closeDatabase(currentScopeDB,"CURRENTSCOPE");
				//currentScopeDB.closeDatabase();
			}
		}
	}
	
	
	
	private void printReport(boolean yesNo,StringBuffer nonProcessedScopes){
		Iterator<Map.Entry<String, List<String>>> map = processedPCHMap.entrySet().iterator();
		StringBuffer report = new StringBuffer();
		while(map.hasNext()){
			Map.Entry<String, List<String>> entry = map.next();
			Iterator<String> itr = entry.getValue().iterator();
			report.append("---------------[Algortihm Name:["+entry.getKey()+"]-----------------");
			report.append("\n");
			report.append(" TOTAL NUMBER OF PCH INCLUDED :["+entry.getValue().size()+"]");
			report.append("\n");
			report.append("PCH NAMES [");
			while(itr.hasNext()){
				report.append(itr.next());
				report.append(",");
			}
			report.append("]");
			report.append("\n");
			report.append("\n");
			report.append("---------------------------------------------------------------------");
			report.append("\n");
			report.append("\n");
		}
		ISOConsumptionLogger.info(report.toString());
		ISOConsumptionLogger.info("----------------------- ALGORITHM NAMES WIHTOUT RATING IN FLOWCHART ---------------");
		ISOConsumptionLogger.info("\n");
		ISOConsumptionLogger.info(nonProcessedScopes.toString());
		ISOConsumptionLogger.info("\n");
		ISOConsumptionLogger.info(" ----------------------------------------------------------------------------------");
		
		/*if(cosolidatedReportList != null){
			Iterator<String> consolidateRepItr = cosolidatedReportList.iterator();
			while (consolidateRepItr.hasNext()) {
				ISOConsumptionLogger.info(consolidateRepItr.next());
			}
		}*/
		ISOConsumptionLogger.info("************************* Ending Rearranging LOB using FlowChart ************");
	}

	List<String> cosolidatedReportList = null;
	
	
	/**
	 * @param ratingHolder
	 * @param currentScopeDB
	 * @param ratingHolderNew
	 * THIS METHOD WILL REMOVE THE RATING VARIABLE NAMES FROM THE RATINGHOLDER WHICH ARE NOT IN THIS SCOPE
	 * THIS IS ADDED BECAUSE IN FLOWCHART THERE ARE TWO CLASS NODES WITH SAME NAME AND DIFFERENT RATING VARIABLES
	 * @throws BasexException 
	 * 
	 */
	private void checkAndRemoveRatingNodeWithSameName(RatingHolder ratingHolder,Database currentScopeDB,RatingHolder ratingHolderNew ) throws BasexException{
		
		if(ratingHolder.getRating()!= null && ratingHolder.getRating().size()>1){
			
			for(Rating ratingFromHolder: ratingHolder.getRating()){
					String PCHToCheck = ratingFromHolder.getVariables().split(",")[0];
					Connection mainScopeCon = new XMLConnection(currentScopeDB);
					Query queryPch = new XmlQuery(mainScopeCon); 
					StringBuffer queryString1 = new StringBuffer();
					//class[@name='BOP']/rating
					queryString1.append("//PCH[@name='").append(PCHToCheck).append("']");
					queryPch.createQuery(queryString1.toString());
					Object returnPCH  = "<scope>"+queryPch.execute()+"</scope>";
					try {
						Scope scoprForPchCheck = JAXBUtils.readFromSource(returnPCH.toString(), Scope.class);
						if(scoprForPchCheck.getContent() != null && scoprForPchCheck.getContent().size()>0){
							ratingHolderNew.getRating().add(ratingFromHolder);
						} else{
							;
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
	}
	
	
	/**
	 * @param database
	 * @param queryString
	 * @return
	 * @throws BasexException 
	 */
	private Object createQueryAndReturnResult(Database database,String queryString) throws BasexException{
		Connection connection = new XMLConnection(database);
		Query query = new XmlQuery(connection); 
		query.createQuery(queryString);
		Object returnObj = query.execute();
		return returnObj;
	}
	
	
	/**
	 * @param sequenceNo
	 * @param currentScopeDB
	 * @param pchArryList
	 * THIS METHOD WILL GET ALL THE PCH NAMES HAVING / IN IT AS THESE VARIABLE NAMES WILL NOT BE IN 
	 * THE VARIABLE LIST
	 * @throws BasexException 
	 * 
	 */
	private void addPCHWithNameContainingSlash(int sequenceNo,Database currentScopeDB,List<String>pchArryList) throws BasexException{
		if(sequenceNo == 1){
			StringBuffer queryString1 = new StringBuffer();
			//GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			queryString1.append("//PCH[@name[contains(.,'/')]]");
			String returnPCHList = createQueryAndReturnResult(currentScopeDB,queryString1.toString()).toString();
			if(returnPCHList != null && !"".equals(returnPCHList)){
				returnPCHList = "<scope>"+returnPCHList+"</scope>";
				try {
					Object object = JAXBUtils.readFromSource(returnPCHList, Scope.class);
					if(object instanceof Scope){
						Scope scopeNewObj = (Scope)object;
						for(Object pchobject:scopeNewObj.getContent()){
							if(pchobject instanceof PCH){
								//ADDING PCH NAMES TO PCH NAME ARRAY SO THIS WILL BE INCLUDED IN THE FIRST SCOPE WE SPLIT
								pchArryList.add(((PCH) pchobject).getName());
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * @param ratingHolderStr
	 * @param scope
	 * @param flowChartDB
	 * @return
	 * CREATE AND RETURN RATINGHOLDER OBJECT FOR THE RATING NODES IN FLOWCHART FOR THE CURRENT SCOPE DBTABLE 
	 * @throws BasexException 
	 */
	private RatingHolder createAndReturnRatingHolderObject(Scope scope,Database flowChartDB) throws BasexException{
		StringBuffer queryString = new StringBuffer();
		//class[@name='BOP']/rating
		queryString.append("//class[@name='").append(scope.getDbTables()).append("']/rating");
		Object returnObj = "<RatingHolder>"+createQueryAndReturnResult(flowChartDB, queryString.toString())+"</RatingHolder>";
		//CONVERTING STRING INTO OBJECT AGAIN USING XSTREAM FOR THIS
		XStream xstream = new XStream();
		xstream.registerConverter(new RatingObjectConverter());
		xstream.alias("RatingHolder", RatingHolder.class);
		xstream.alias("Rating", Rating.class);
		xstream.addImplicitCollection(RatingHolder.class, "rating",Rating.class);
		
		/*xstream.useAttributeFor("variable", String.class);
		xstream.useAttributeFor("rating", String.class);*/
		
		RatingHolder ratingHolder  = (RatingHolder)xstream.fromXML(returnObj.toString());
		return ratingHolder;
		
	}
	/**
	 * @param scope
	 * @param flowChartDB
	 * @param currentScopeDB
	 * @return
	 * THIS METHOD WILL SPLIT SCOPE INTO MULTIPLE SCOPES IF ALGROTIHM EXECUTES LIST OF PCH AT DIFFERENT LEVELS
	 * @throws BasexException 
	 */
	private List<Scope> splitScopeUsingFlowChart(Scope scope,Database flowChartDB,Database currentScopeDB,StringBuffer nonProcessedScopes) throws BasexException{
		
		/*xstream.useAttributeFor("variable", String.class);
		xstream.useAttributeFor("rating", String.class);*/
		
		RatingHolder ratingHolder  = createAndReturnRatingHolderObject(scope, flowChartDB);
		
		//CREATING NEW RATINGHOLDER AND LOOPING THROUGH IT TO AVOID CONCURRANCY ERROR
		RatingHolder ratingHolderNew = new RatingHolder();
		List<Rating> ratingList = new ArrayList <Rating>();
		ratingHolderNew.setRating(ratingList);
		
		//CHECKING IF THERE ARE TWO NODES WITH SAME NAME
		checkAndRemoveRatingNodeWithSameName(ratingHolder, currentScopeDB, ratingHolderNew);
		
		List<Scope> newScopeList = new ArrayList<Scope>();
		int sequenceNo = 0;
		/*RatingHolder ratingHolderNew1 = new RatingHolder();	
		ratingHolderNew1.setRating(ratingHolderNew.getRating());
		*/
		if(ratingHolderNew.getRating()!= null && ratingHolderNew.getRating().size() != 0){
			
			for(Rating ratingObj:ratingHolderNew.getRating()){
				if(ratingObj instanceof Rating){
					//CREATING SEPERATE SCOPE FOR EVERY RATING NODE UNDER THE CLASS 
					Scope newScope = new Scope();
					newScope.setDbTables(scope.getDbTables());
					newScope.setSequenceNo(++sequenceNo);
					newScope.setPass(scope.getPass());
					Rating rating = (Rating)ratingObj;
					String variable = rating.getVariables();
					String pchArr [] = null;
					if(variable.contains(",")){
						pchArr = variable.split(",");
					}else{
						pchArr = new String[]{variable};
					}
					List<String> pchNameListOfRatingNodes = Arrays.asList(pchArr);
					List<String> pchArryList = new ArrayList<String>();
					pchArryList.addAll(pchNameListOfRatingNodes);
					//ADDING PCH TO ARRAY WHICH CONTAINS / IN IT WE NEED TO ADD THIS PCH ONLY IN ONE SCOPE
					//NO NEED TO ADD THIS IN MULTIPLE SCOPES SO CHECKING THE SQUENCE AND IF IT IS O THEN ADDING THESE PCHS
					addPCHWithNameContainingSlash(sequenceNo, currentScopeDB, pchArryList);
					//CREATING NEW PCHNAMELIST AND ADDING TO THE PROCESSED PCHMAP 
					//THIS HAS BEEN ADDED BSE IF SAME PCH REFERENCED BY MULTIPLE SPLITTED SCOPE
					List<String> pchNameListForScope = new ArrayList<String>();
					processedPCHMap.put(scope.getDbTables()+sequenceNo+scope.getPass(),pchNameListForScope);
					
					//GETTING ALL THE PCH DECLARED IN THE VARAIBLES AND ADDING THOSE IN NEWLY CREATE SCOPE OBJECT
					
					getPCHFromScopeAndAddTonewScope(scope, pchArryList, currentScopeDB, sequenceNo, newScope);
					
					newScopeList.add(newScope);
				}
			}
			ISOConsumptionLogger.info("SPLIT FOR ALGORITHM =["+scope.getDbTables()+"] NUMBER OF NEW ALGORITHMS CREATED =["+sequenceNo+"]");
		}
		else{
			//ENLISINTG SCOPE NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART
			addOriginalScopeToNewScopeList(nonProcessedScopes, scope, newScopeList, sequenceNo);
		}
		
		return newScopeList;
	}
	
	
	/**
	 * @param scope
	 * @param pchArryList
	 * @param currentScopeDB
	 * @param sequenceNo
	 * @param newScope
	 * THIS MEHTO WILL LOOP THROUGH THE PCH NAMES DECLARED IN RATING NODE AND GET THOSE PCH FROM THE MAIN SCOPE
	 * AND ADD IT TO THE NEWLY CREATED SCOPE
	 * @throws BasexException 
	 */
	private void getPCHFromScopeAndAddTonewScope(Scope scope,List<String>pchArryList,Database currentScopeDB,int sequenceNo,Scope newScope) throws BasexException{
		for(String pchName : pchArryList){
			StringBuffer queryString1 = new StringBuffer();
			//GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			queryString1.append("//(scope[@dbTables='").append(scope.getDbTables()).append("']/PCH[@name='"+pchName+"'])[1]");
			Object returnPCH = createQueryAndReturnResult(currentScopeDB, queryString1.toString());
			try {
				if(!"".equals(returnPCH) ){
					Object pchObj  = JAXBUtils.readFromSource(returnPCH.toString().trim(),PCH.class);
					PCH pch = (PCH) pchObj;
					processedPCHMap.get(scope.getDbTables()+sequenceNo+scope.getPass()).add(pch.getName());
					//pch.setExecutedAtScopeSplitLevel(true);
					newScope.getContent().add(pch);
				}
			} catch (Exception e) {
				ISOConsumptionLogger.info(returnPCH.toString());
				e.printStackTrace();
			}
		
		}
	}
	
	/**
	 * 
	 */
	private void addOriginalScopeToNewScopeList(StringBuffer nonProcessedScopes,Scope scope,List<Scope> newScopeList,int sequenceNo){
		//ENLISINTG SCOPE NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART
		nonProcessedScopes.append("ALGORITHM NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART =[");
		nonProcessedScopes.append(scope.getDbTables());
		nonProcessedScopes.append("]");
		nonProcessedScopes.append("\n");
		//ADDIGN SEQUENCE NUMBER TO THE NON PROCESSED SCOPE
		scope.setSequenceNo(++sequenceNo);
		
		newScopeList.add(scope);
	}
	
	private void readScope(Reference reference ,Scope scope,Database flowChartDB,Database currentScopeDB,StringBuffer nonProcessedScopes) throws Exception {
		//Scope originalScope = scope;
		
		List<Scope>newScopeList = splitScopeUsingFlowChart(scope,flowChartDB,currentScopeDB,nonProcessedScopes);
		
		//CHECKING WHETHER SCOPE IS SPLITT INTO MULTIPLE ALGORITHMS
		if(newScopeList.size()>1){
			//AS WE NEED TO SPLIT THE SCOPE REMOVING SCOPE FROM THE REFERENCE
			reference.getContent().remove(scope);
			//int originanNumberOfPCH = 0;
			//int numberOFPCHAfterProcess = 0;
				for(Scope newScope:newScopeList){
					List<Object> pchList = new ArrayList<Object>();
					pchList.addAll(newScope.getContent());
					
					
					//originanNumberOfPCH = pchList.size();
					for (Object obj : pchList) {
						if (obj instanceof PCH) {
							readPCH((PCH) obj,currentScopeDB,newScope);
						} else if (obj instanceof If) {
							ISOConsumptionLogger.info("NOT EXPECTED THIS SITUATION");
							readIf((If) obj,currentScopeDB,newScope);
						} else if (obj instanceof Assign) {
							ISOConsumptionLogger.info("NOT EXPECTED THIS SITUATION");
							readAssign((Assign) obj,currentScopeDB,newScope);
						} else if (obj instanceof Loop) {
							ISOConsumptionLogger.info("NOT EXPECTED THIS SITUATION");
							readLoop((Loop) obj,currentScopeDB,newScope);
						} else if (obj instanceof Class) {
						}
					}
				}
				
		    }else{
		    	//REMOVING SCOPE FROM THE REFERENCE AS WE ARE ADDING WHILE ITERATING THROUGH NEWSCOPELIST
		    	reference.getContent().remove(scope); 
		    }
		List<String> processdPchInTheScope = new ArrayList<String>();
		//ADDING NEWLY CREATED SCOPES IN CURRENT REFERENCE
		for(Scope newScope:newScopeList){
			reference.getContent().add(newScope);
			List<String> procssedPCHList = processedPCHMap.get(newScope.getDbTables()+newScope.getSequenceNo()+newScope.getPass()); 
			if(procssedPCHList != null)
			processdPchInTheScope.addAll(procssedPCHList);
		}
		
		//THIS METHOD IS ADDED TO CHECK IF THERE ARE ANY PCH WIHTOUT ANY REFERENCE OR ENTRY IN VARAIBLE TYPE
		checkForExcluedePCH(currentScopeDB, scope, processdPchInTheScope, newScopeList.get(0));
		//AFTER EXECUTING ALL THE SCOPES WE NEED TO CHECK FOR ANY REMAINING PCH WHOSE NAME IS NOT
		//INCLUDE IN THE VARIABLE TYPE AND IT IS NOT A INTERMEDIARY PCH USED
			
		
	}

	/**
	 * @param currentScopeDB
	 * @param mainScope
	 * @throws Exception 
	 */
	private void checkForExcluedePCH(Database currentScopeDB,Scope mainScope,List<String> processedPchList,Scope newScope) throws Exception{
		if(processedPchList.size() == 0)
			return;
		Connection currentScopeCon = new XMLConnection(currentScopeDB);
		Query query = new XmlQuery(currentScopeCon); 
		StringBuffer queryString = new StringBuffer();
		//class[@name='BOP']/rating
		queryString.append("//PCH/@name");
		Set<String> pchSet = new HashSet<String>();
		query.createQuery(queryString.toString());
		Object returnObj = query.execute();
		if(returnObj != null){
			String pchName = returnObj.toString();
			String [] arr = pchName.split("name=");
			for(String pchNam : arr){
				if(!"".equals(pchNam) && pchNam.contains("\""))
				{
					pchNam = pchNam.replace("\"", "").trim();
					 pchSet.add(pchNam);
				}
				 
			}
		}
		for(String pchLeft : pchSet){
			if(!processedPchList.contains(pchLeft)){
				ISOConsumptionLogger.info("NO REFERENCE FOR PCH WITH NAME=["+pchLeft+"] IN ALGORITHM=[ "+newScope.getDbTables()+"]");
				queryString = new StringBuffer();
				queryString.append("//PCH[@name='").append(pchLeft).append("'][1]");
				query.createQuery(queryString.toString());
				Object returnObject = query.execute();
				if(returnObject != null && !"".equals(returnObject)){
					try {
						PCH pch =(PCH)JAXBUtils.readFromSource(returnObject.toString(), PCH.class);
						processedPCHMap.get(newScope.getDbTables()+newScope.getSequenceNo()+newScope.getPass()).add(pchLeft);
						newScope.getContent().add(pch);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}
	
	}
	
	private void readPCH(PCH pch,Database currentScopeDB,Scope currentScope) throws Exception {

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope);
			} else if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope);
			} else if (obj instanceof Loop) {
				// ISOConsumptionLogger.info("loop");
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Class) {
				// ISOConsumptionLogger.info("class");
			}
		}
	}

	/*private void readPCHContent(Object obj,Database currentScopeDB,Scope currentScope ) {
//		ISOConsumptionLogger.info(obj.getClass().getSimpleName());
		if (obj instanceof If) {
//			ISOConsumptionLogger.info("***** If");
			readIf((If) obj,currentScopeDB,currentScope );
		} else if (obj instanceof Assign) {
//			ISOConsumptionLogger.info("**** Assign");
			readAssign((Assign) obj,currentScopeDB,currentScope );
		} else if (obj instanceof Loop) {
//			ISOConsumptionLogger.info("**** Loop");
			readLoop((Loop) obj,currentScopeDB,currentScope );
		} else if (obj instanceof Class) {
//			ISOConsumptionLogger.info("**** Class");
		}else if(obj instanceof String){
			;
		}else{
			ISOConsumptionLogger.info("We will need to handle this case -> " + obj.getClass());
		}
	}*/

	private void readIf(If _if,Database currentScopeDB,Scope currentScope ) throws Exception {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition((Condition) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Then) {
				readThen((Then) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Else) {
				readElse((Else) obj,currentScopeDB,currentScope );
			}
		}
	}

	private void readCondition(Condition condition,Database currentScopeDB,Scope currentScope ) throws Exception {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,currentScopeDB,currentScope );
			}
		}
	}

	private void readThen(Then then,Database currentScopeDB,Scope currentScope ) throws Exception {
		// ISOConsumptionLogger.info("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope );
			} else if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// ISOConsumptionLogger.info("}");
	}

	
	//private int loopIndex = 0;
	
	private void readLoop(Loop loop,Database currentScopeDB,Scope currentScope ) throws Exception {
		// ISOConsumptionLogger.info("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope );
			} else if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}
	
	private void readElse(Else _else,Database currentScopeDB,Scope currentScope ) throws Exception {
		// ISOConsumptionLogger.info("else");
		// ISOConsumptionLogger.info("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign((Assign) obj,currentScopeDB,currentScope );
			} else if (obj instanceof If) {
				readIf((If) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Loop) {
				readLoop((Loop) obj,currentScopeDB,currentScope );
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// ISOConsumptionLogger.info("}");
	}


	private void readAssign(Assign assign,Database currentScopeDB,Scope currentScope ) throws Exception {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj,currentScopeDB,currentScope );
			}
		}
	}

	private void readExpression(Expression expression,Database currentScopeDB,Scope currentScope ) throws Exception {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
//			sb.append(" expression -> " + expression.getOp());
			readBothSides(expression,currentScopeDB,currentScope );
		} else {
			readContent(expression,currentScopeDB,currentScope );
		}
	}

	private void readBothSides(Expression expression,Database currentScopeDB,Scope currentScope ) throws Exception {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(ex,currentScopeDB,currentScope );
			} else if (obj instanceof Function) {
				readFunction((Function) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj,currentScopeDB,currentScope );
				// rateTableList.add((Ratetable)obj);
			}
		}
	}

	private void readContent(Expression expression,Database currentScopeDB,Scope currentScope ) throws Exception {
		for (Object obj : expression.getContent()) {
			
			if (obj instanceof String) {
				checkVariableName(obj.toString(), currentScopeDB, expression.getVariableType(),currentScope,false );
			} else if (obj instanceof Expression) {
				readExpression((Expression) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj,currentScopeDB,currentScope );
			} else if (obj instanceof Function) {
				readFunction((Function) obj,currentScopeDB,currentScope );
			}
		}
	}

	/**
	 * @param variableName
	 * @param currentScopeDB
	 * @param variableType
	 * 
	 * THIS METHOD WILL CHECK THE VARAIBLE IS POINTING TO PCH IN SCOPE
	 * @throws Exception 
	 *  
	 */
	private void checkVariableName(String variableName,Database currentScopeDB,String variableType,Scope currentScope,boolean isFromRatetable ) throws Exception{
		if(variableType != null)
		if ( variableType.equals(VariableType.LV_VALUE)|| variableType.equals(VariableType.LV_INTEGER) || variableType.equals(VariableType.LV_NUMERIC) || variableType.equals(VariableType.LV_DOUBLE) || variableType.equals(VariableType.LV_STRING) || variableType.equals(VariableType.LV_BOOLEAN) || variableType.equals(VariableType.LV_DATE) || variableType.equals(VariableType.LV_TIMESPAN) || isFromRatetable){
			//CHECK WHTHER THIS VARIABLE POINTS TO PCH IF SO ADD THIS PCH TO THE CURRENT NEW SCOPE
			Connection curScopeCon = new XMLConnection(currentScopeDB);
			Query query = new XmlQuery(curScopeCon); 
			StringBuffer queryString = new StringBuffer();
			//class[@name='BOP']/rating
			queryString.append("//(PCH[@name='").append(variableName.trim()).append("'])[1]");
			query.createQuery(queryString.toString());
			Object returnPCH = query.execute();
			if(returnPCH != null && !returnPCH.equals("")){
				try {
					Object pchObj  = JAXBUtils.readFromSource(returnPCH.toString(), PCH.class);
					PCH pch = (PCH)pchObj;
					String preceedPCHMapKey = currentScope.getDbTables()+currentScope.getSequenceNo()+currentScope.getPass(); 
					if(!processedPCHMap.get(preceedPCHMapKey).contains(pch.getName().trim())){
						currentScope.getContent().add(pch);
						processedPCHMap.get(preceedPCHMapKey).add(pch.getName().trim());
						//READING NEWLY ADDED PCH
						readPCH(pch, currentScopeDB, currentScope);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		
		}
	}
	
	private void readFunction(Function function,Database currentScopeDB,Scope currentScope ) throws Exception {
		// System.out.print(function.getType());
		// System.out.print("( ");
		// sb.append(":");
		for (Object obj : function.getContent()) {
			if(obj instanceof Arg){
				Arg arg = (Arg) obj;
				String variableType = arg.getVariableType();
				if(variableType == null)
					variableType = "dummy";
				checkVariableName(arg.getContent(), currentScopeDB,variableType,currentScope,true );
			}
		}
		
	}

	private void readRatetable(Ratetable ratetable,Database currentScopeDB,Scope currentScope) throws Exception {

		for (Object obj : ratetable.getContent()) {

			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				String content = arg.getContent().trim();
				String variableType = arg.getVariableType();
				if(variableType == null)
					variableType = "dummy";
				checkVariableName(content, currentScopeDB, variableType, currentScope,true );
			}

		}
	}


	
	private void testMehtod(){
		String completeAlgoFile = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-TX-05012013-V01/ALG-BP-TX-05012013-V01_MR_REARRANGED.xml";
		String testXmlFile = "D:/files-by-shahsi/ISO-ERC-ALG-FILES/RC-BP-TX-05012013-V01/TestTXAlgoFile.xml";
		try{
			Reader algoReader = new FileReader(new File(completeAlgoFile));
			JAXBContext jbContext = JAXBContext.newInstance("com.mmpnc.rating.iso.algorithm.vo");
			Unmarshaller unmarshal = jbContext.createUnmarshaller();
			LOB lob = (LOB) unmarshal.unmarshal(algoReader);	
			rearrageLOBUsingFlowChart(lob);
		}catch(Exception e){
			e.printStackTrace();
			ISOConsumptionLogger.info("Excpetion in the test mehtod");
		}
	}
	
	
	public static void main(String []args) {
		new OtherApproach().testMehtod();
	}
}
