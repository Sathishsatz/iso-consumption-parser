package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class IntegerEvaluator implements Evaluator, IEvaluatorValidator {

	private String integerValue;
	
	
	public IntegerEvaluator(String string) {
		this.integerValue = string;
	}

	@Override
	public void setContext(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		return returnValue.append(" ").append(this.integerValue).append(" ");
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.integerValue==null || this.integerValue.equals("")){
			throw new ParserException("Interger value is NULL or empty in INTEGER evaluator");
		}
		
	}

}
