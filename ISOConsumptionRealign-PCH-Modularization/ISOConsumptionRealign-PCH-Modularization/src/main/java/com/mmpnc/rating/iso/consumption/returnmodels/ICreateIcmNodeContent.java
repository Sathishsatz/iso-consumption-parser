package com.mmpnc.rating.iso.consumption.returnmodels;

import java.util.List;

import com.mmpnc.rating.iso.wrapper.ICMRequiredData;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public interface ICreateIcmNodeContent {
	public void createIcmRatingNodeContent(IcmRatingNodeContent icmRatingNodeContent,List<IcmRatingNodeContent>icmNodeContentList,ICMRequiredData icmRequiredData);
	//public void createIcmRatingNodeContent(IcmRatingNodeContent icmRatingNodeContent,List<IcmRatingNodeContent>icmNodeContentList,ICMRequiredData icmRequiredData);
}
