package com.mmpnc.rating.iso.algorithm.parse;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.TreeNodeStream;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.AlgorithmProcessor;
import com.mmpnc.rating.iso.algorithm.AlgorithmReader;
import com.mmpnc.rating.iso.algorithm.AlgorithmReaderImpl;
import com.mmpnc.rating.iso.algorithm.EvaluatorFactory;
import com.mmpnc.rating.iso.algorithm.EvaluatorFactory.EvaluatorType;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.exception.SyntaxRecognitionException;

public class AlgorithmParser implements AlgorithmProcessor {
	private Context context;
	private StringBuffer buffer;

	public AlgorithmParser(Context context, StringBuffer buffer) {
		this.context = context;
		this.buffer = buffer;
	}

	@Override
	public Object execute() throws Exception {
		AlgorithmReader reader = new AlgorithmReaderImpl(buffer);
		AlgorithmWalker tree = new AlgorithmWalker((TreeNodeStream) reader.execute());
		EvaluatorFactory factory = new EvaluatorFactory(EvaluatorType.PARSE);
		tree.setEvaluatorFactory(factory);
		try {
			tree.algorithm(context);
		} 
		catch(ParserException e){
			StringBuilder sb = new StringBuilder();
			sb.append(e.getMessage()).append("\n");
			sb.append("Algo Text: ");
			sb.append("\n").append(buffer.toString());
			if(e.getMessage()!=null && !("".equals(e.getMessage()))){
				sb.append("Error Message: ").append(e.getMessage());
			}
			throw new ParserException(sb.toString());
		}
		catch(RecognitionException re){
			StringBuilder sb = new StringBuilder();
			sb.append(re.getMessage()).append("\n");
			sb.append("Algo Text: ");
			sb.append("\n").append(buffer.toString());
			String errMsg = tree.getErrorHeader(re) + " : " + tree.getErrorMessage(re, tree.getTokenNames());
			sb.append("Error Message: ").append(errMsg);
			throw new SyntaxRecognitionException(sb.toString());
		}
		catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb.append(e.getMessage()).append("\n");
			sb.append("Algo Text: ");
			sb.append("\n").append(buffer.toString());
			throw new Exception(sb.toString());
		}
		return null;
	}
}
