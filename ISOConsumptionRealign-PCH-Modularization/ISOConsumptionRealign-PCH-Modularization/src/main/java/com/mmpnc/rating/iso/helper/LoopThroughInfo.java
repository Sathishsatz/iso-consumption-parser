package com.mmpnc.rating.iso.helper;

/**
 * @author nilkanth9581
 *
 */
public class LoopThroughInfo implements Cloneable{
	private String predicateLHS;
	private String operator;
	private String predicateRHS;
	private boolean isModelAttribute;
	
	public boolean isModelAttribute() {
		return isModelAttribute;
	}
	public void setModelAttribute(boolean isModelAttribute) {
		this.isModelAttribute = isModelAttribute;
	}
	public String getPredicateRHS() {
		return predicateRHS;
	}
	public void setPredicateRHS(String predicateRHS) {
		this.predicateRHS = predicateRHS;
	}
	
	public String getPredicateLHS() {
		return predicateLHS;
	}
	public void setPredicateLHS(String predicateLHS) {
		this.predicateLHS = predicateLHS;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public LoopThroughInfo clone() throws CloneNotSupportedException{
		return (LoopThroughInfo)super.clone();
	}
}
