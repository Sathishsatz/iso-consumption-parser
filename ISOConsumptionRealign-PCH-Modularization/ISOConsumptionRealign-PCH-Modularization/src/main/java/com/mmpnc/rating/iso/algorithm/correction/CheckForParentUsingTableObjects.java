package com.mmpnc.rating.iso.algorithm.correction;

import java.util.ArrayList;
import java.util.List;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.RatingHolder;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 * 
 */
public class CheckForParentUsingTableObjects implements
		ICheckForParentUsingTableObjects {

	@Override
	public List<Scope> checkForParentUsingTableObjects(Scope scope,
			Database tableObjectsDB) throws Exception {
		List<Scope> scopeList = new ArrayList<Scope>();
		StringBuffer queryString = new StringBuffer();
		// class[@name='BOP']/rating
		queryString.append("(distinct-values(//TableName[TableName[@name='")
				.append(scope.getDbTables()).append("']]/@name/data()))");
		Object returnObj = createQueryAndReturnResult(tableObjectsDB,
				queryString.toString());
		String arr[] = returnObj.toString().split(" ");
		int sequenceNumber = scope.getSequenceNo();
		if (arr.length > 1) {
			ISOConsumptionLogger.info(scope.getDbTables()
					+ " algorithm is present at[" + arr.length + "]");
			createScopeListForMultilevelAlgorithms(scope, scope.getDbTables(),
					arr, sequenceNumber, scopeList);
		} else {
			queryString = new StringBuffer();
			queryString.append("count((//TableName[TableName[@name='")
					.append(scope.getDbTables()).append("']]/@name/data()))");
			Object levelCount = createQueryAndReturnResult(tableObjectsDB,
					queryString.toString());
			Integer count = new Integer(levelCount.toString());
			if (count > 1) {
				ISOConsumptionLogger
						.info("PARENT IS PRESENT AT MULTIPLE LEVEL ...."
								+ returnObj);
				checkForMultiLevelParent(scope, returnObj.toString(),
						tableObjectsDB, scopeList);
			} else {
				scopeList.add(scope);
			}
		}

		return scopeList;

		// CODE ADDED FOR CA ALGORITHM CONSUMPTION WITH LESS STEPS
		/*
		 * List<Scope> arrangementForCADemo = new ArrayList<Scope>(1);
		 * arrangementForCADemo.add(scopeList.get(0)); return
		 * arrangementForCADemo;
		 */
	}

	private Object createQueryAndReturnResult(Database database,
			String queryString) throws Exception {
		Connection connection = new XMLConnection(database);
		Query query = new XmlQuery(connection);
		query.createQuery(queryString);
		Object returnObj = query.execute();
		return returnObj;
	}

	private void createScopeListForMultilevelAlgorithms(Scope scope,
			String scopeName, String[] arr, int sequenceNumber,
			List<Scope> scopeList) {
		for (String parent : arr) {
			Scope newScope1 = new Scope();
			newScope1.setDbTables(parent + "/" + scopeName);
			newScope1.setPass(scope.getPass());
			newScope1.setRecordType(scope.getRecordType());
			newScope1.setRuleReference(scope.getRuleReference());
			sequenceNumber++;
			newScope1.setSequenceNo(sequenceNumber);
			// newScope1.getContent().addAll(scope.getContent());
			addScopecontentToNewlyCreatedScope(scope, newScope1);
			scopeList.add(newScope1);
		}
	}

	private void addScopecontentToNewlyCreatedScope(Scope scope, Scope newScope) {

		List<Object> scopeContent = scope.getContent();

		for (Object scopeCont : scopeContent) {
			if (scopeCont instanceof Assign) {
				newScope.getContent().add(scopeCont);
			} else if (scopeCont instanceof Loop) {
				newScope.getContent().add(scopeCont);
			} else if (scopeCont instanceof If) {
				newScope.getContent().add(scopeCont);
			} else if (scopeCont instanceof PCH) {
				PCH pc = (PCH) scopeCont;
				PCH pch = new PCH();
				pch.setExcelRow(pc.getExcelRow());
				pch.setExcelSheet(pc.getExcelSheet());
				pch.setExecuted(pc.getExecuted());
				// pch.setExecutedAtScopeSplitLevel(pc.getExecutedAtScopeSplitLevel());
				pch.setName(pc.getName());
				pch.setOverridden(pc.getOverridden());
				pch.setRecordType(pc.getRecordType());
				pch.setState(pc.getState());
				pch.getContent().addAll(pc.getContent());
				newScope.getContent().add(pch);
			}
		}
	}

	private void checkForMultiLevelParent(Scope scope, String scopeName,
			Database tableObjectsDB, List<Scope> scopeList) throws Exception {
		StringBuffer queryString = new StringBuffer();
		// class[@name='BOP']/rating
		queryString.append("(distinct-values(//TableName[TableName[@name='")
				.append(scopeName).append("']]/@name/data()))");
		Object returnObj = createQueryAndReturnResult(tableObjectsDB,
				queryString.toString());
		String arr[] = returnObj.toString().split(" ");
		int sequenceNumber = scope.getSequenceNo();
		createScopeListForMultilevelAlgorithms(scope,
				scopeName + "/" + scope.getDbTables(), arr, sequenceNumber,
				scopeList);
	}

	/**
	 * @param ratingHolder
	 * @param currentScopeDB
	 * @param ratingHolderNew
	 *            THIS METHOD WILL REMOVE THE RATING VARIABLE NAMES FROM THE
	 *            RATINGHOLDER WHICH ARE NOT IN THIS SCOPE THIS IS ADDED BECAUSE
	 *            IN FLOWCHART THERE ARE TWO CLASS NODES WITH SAME NAME AND
	 *            DIFFERENT RATING VARIABLES
	 * @throws Exception
	 * 
	 */
	@Override
	public void checkAndRemoveRatingNodeWithSameName(RatingHolder ratingHolder,
			Database currentScopeDB, RatingHolder ratingHolderNew)
			throws Exception {
		if (ratingHolder.getRating() != null
				&& ratingHolder.getRating().size() > 1) {
			for (Rating ratingFromHolder : ratingHolder.getRating()) {
				String PCHToCheck = ratingFromHolder.getVariables().split(",")[0];
				Connection mainScopeCon = new XMLConnection(currentScopeDB);
				Query queryPch = new XmlQuery(mainScopeCon);
				StringBuffer queryString1 = new StringBuffer();
				// class[@name='BOP']/rating
				queryString1.append("//PCH[@name='").append(PCHToCheck)
						.append("']");
				queryPch.createQuery(queryString1.toString());
				Object returnPCH = "<scope>" + queryPch.execute() + "</scope>";
				try {
					Scope scoprForPchCheck = JAXBUtils.readFromSource(
							returnPCH.toString(), Scope.class);
					if (scoprForPchCheck.getContent() != null
							&& scoprForPchCheck.getContent().size() > 0) {
						ratingHolderNew.getRating().add(ratingFromHolder);
					} else {
						;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
}
