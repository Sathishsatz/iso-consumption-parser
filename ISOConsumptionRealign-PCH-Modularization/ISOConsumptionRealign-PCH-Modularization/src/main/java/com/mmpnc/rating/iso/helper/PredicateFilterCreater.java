package com.mmpnc.rating.iso.helper;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.IdGeneration;
import com.mmpnc.icm.common.ide.models.ConstraintType;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;

/**
 * @author nilkanth9581
 *
 */
public class PredicateFilterCreater {
	
	public static SingleFieldConstraintEBLeftSide createMemoryLookupFilter(Context context , String operator ,String lhsOfFilter,String rhsOfFilter,boolean isModelAttribtue,int order){
		
		SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
		singleFieldConstraintEBLeftSide.setOperator(operator);
		//singleFieldConstraintEBLeftSide.set
		LookupNameDecider lookupNameDecider = (LookupNameDecider)context.getValue(ContextParam.LOOKUP_NAME_DECIDER);
		String lookupName =  lookupNameDecider.decideLookupName((String)context.getValue(ContextParam.SCOPE)+"memoryLookup");
		singleFieldConstraintEBLeftSide.setOrderNo(order);
		
		if(isModelAttribtue){
			
			singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_M);
			singleFieldConstraintEBLeftSide.setFieldName(lhsOfFilter);
			lhsOfFilter = lhsOfFilter.replace("CCSRERequest.", "");
			singleFieldConstraintEBLeftSide.setComAttrVal(lhsOfFilter);
			String sourceObject = rhsOfFilter.replace(".", "~").split("~")[0];
			singleFieldConstraintEBLeftSide.setSourceObject(sourceObject);
			String sourceObjectAttr = rhsOfFilter.replace(sourceObject+".", "").trim();
			singleFieldConstraintEBLeftSide.setSourceObjectAttr(sourceObjectAttr);
			singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_UNDEFINED);
			singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
		}
		else {
			
			singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_S);
			lhsOfFilter = lhsOfFilter.replace("CCSRERequest.", "");
			singleFieldConstraintEBLeftSide.setComAttrVal(lhsOfFilter);
			singleFieldConstraintEBLeftSide.setFieldName(lhsOfFilter);
			if(rhsOfFilter.startsWith("'")){
				rhsOfFilter = rhsOfFilter.substring(1,rhsOfFilter.length()-1);
			}
			singleFieldConstraintEBLeftSide.setValue(rhsOfFilter);
			singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
			singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
		}
		
		String lookupFilterID = Integer.toString(order) +lookupName; 
		String id= IdGeneration.idGenerator(lookupFilterID);
		singleFieldConstraintEBLeftSide.setId(id);
		return singleFieldConstraintEBLeftSide;
	}
	
	
}
