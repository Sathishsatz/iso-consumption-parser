package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class OrEvaluator implements Evaluator, IEvaluatorValidator {
	private Evaluator left;
	private Evaluator right;
	private Context context;
	
	public OrEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		
		
		this.left.setContext(context);
		returnValue.append(this.left.evaluate());
		
		returnValue.append(" Or ");
	
		
		if(this.right.getClass() == OrEvaluator.class){
			ISOConsumptionLogger.info(this.right.getClass().toString());
			returnValue.append(" ( " );
			this.right.setContext(context);
			returnValue.append(this.right.evaluate());
			returnValue.append(" ) " );
		} else {
			this.right.setContext(context);
			returnValue.append(this.right.evaluate());
		}
		
		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if((this.left == null) && (this.right==null)){
			throw new ParserException("Left and Right evaluators are NULL in OR evaluator");
		}
		if(this.left == null){
			throw new ParserException("Left evaluator is NULL in OR evaluator");
		}
		if(this.right == null){
			throw new ParserException("Right evaluator is NULL in OR evaluator");
		}			
	}
}