package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class SpecialFunctionEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private Evaluator right;
	private String functionName;
	
	public SpecialFunctionEvaluator(Context context, String string, Evaluator e1) {
		this.context = context;
		this.functionName = string;
		this.right = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append(" ").append(this.functionName).append(" (");
		
		this.right.setContext(context);
		
		returnValue.append(this.right.evaluate());
		
		returnValue.append(") ");
		
		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if((this.functionName == null) && (this.right==null)){
			throw new ParserException("Function name and Right evaluators are NULL in SpecialFunction evaluator");
		}
		if(this.functionName == null){
			throw new ParserException("Function name is NULL in SpecialFunction evaluator");
		}
		if(this.right == null){
			throw new ParserException("Right evaluator is NULL in SpecialFunction evaluator");
		}	
		
	}

}
