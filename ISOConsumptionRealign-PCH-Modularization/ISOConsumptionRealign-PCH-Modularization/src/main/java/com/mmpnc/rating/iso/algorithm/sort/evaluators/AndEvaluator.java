package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class AndEvaluator implements Evaluator, IEvaluatorValidator {
	private Evaluator left;
	private Evaluator right;
	private Context context;

	public AndEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException {

		validate();

		StringBuffer returnValue = new StringBuffer();

		this.left.setContext(context);
		returnValue.append(this.left.evaluate());

		returnValue.append(" And ");
		
		this.right.setContext(context);
		Object obj = this.right.evaluate();
		if (obj == null || obj.toString().equals("")) {
			ExceptionHandler
					.raiseParserException("Condition can not be NULL or EMPTY within boolean evaluator");
		}

		if (this.right.getClass() == OrEvaluator.class) {
			ISOConsumptionLogger.info(this.right.getClass().toString());
			returnValue.append(" ( ");
			this.right.setContext(context);
			returnValue.append(obj);
			returnValue.append(" ) ");
		} else {
			this.right.setContext(context);
			returnValue.append(obj);
		}

		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if ((this.left == null) && (this.right == null)) {
			ExceptionHandler
					.raiseParserException("Left and Right evaluators are NULL in AND evaluator");
		}
		if (this.left == null) {
			ExceptionHandler
					.raiseParserException("Left evaluator is NULL in AND evaluator");
		}
		if (this.right == null) {
			ExceptionHandler
					.raiseParserException("Right evaluator is NULL in AND evaluator");
		}
	}

}
