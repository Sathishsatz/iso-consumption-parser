package com.mmpnc.rating.iso.algorithm.merge.scope;

import java.util.List;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.ScopeMergingException;
import com.mmpnc.rating.iso.algorithm.vo.LOB;

public abstract class ScopeMergeOpSequence {
	
	public void execute(LOB lob)throws ScopeMergingException{
		Database database = null;
		List<String> refNameList = null;
		try{
			database = createLOBDatabase(lob);
			refNameList = getReferenceNamesWithPass2Scope(database);
		}catch(ScopeMergingException e){
			throw e;
		}finally{
			if(database != null)
				database.closeDatabase();
		}
		//IF THE LOB HAS REFERENCE WITH SCOPE HAVING PASS GREATER THAN 1 THEN 
		//ONLY WE NEED TO GO FORWARD
		if(refNameList != null && refNameList.size()>0) 
			iterateInLOBAndMergeScopes(lob, refNameList);
	}
	
	abstract Database createLOBDatabase(LOB lob)throws ScopeMergingException;
	abstract List<String> getReferenceNamesWithPass2Scope(Database database)throws ScopeMergingException;
	abstract void iterateInLOBAndMergeScopes(LOB lob,List<String> refNameList)throws ScopeMergingException;

}
