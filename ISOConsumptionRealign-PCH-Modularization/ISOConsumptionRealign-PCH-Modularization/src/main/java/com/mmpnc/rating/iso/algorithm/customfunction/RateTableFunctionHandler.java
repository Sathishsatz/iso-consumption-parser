package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;
import java.util.Map;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionMessages;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.LookupStepCreater;
import com.mmpnc.rating.iso.ratetablelookup.RTKEY_LOOKUP_TYPE;
import com.mmpnc.rating.iso.ratetablelookup.RatingContentFileLookup;

/**
 * @author nilkanth9581
 * THIS CLASS WILL HANDLE RATETABLE FUNCTION IN THE ALGORITHM FILE
 * 
 */
public class RateTableFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String varaible, Context context) throws RecognitionException{
		
		LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
		int localCounter = locHolder.getNextLocVarCounter();
		
		String aliasName = "localVariable"+localCounter;
		//localCounter = localCounter+1;*/
		Object lookupStep = null;
		//context.putValue(ContextParam.LOCALVARIABLECOUNTER, localCounter+1);
		try{
			
			LookupStepCreater lookupStepCreater = LookupStepCreater.getInstance();
			Map<RTKEY_LOOKUP_TYPE,List<String>> interPolatedKeyMap = RatingContentFileLookup.getInstance().getInterPolateKey(function);
			List<String> interPolatedKeyList = interPolatedKeyMap.get(RTKEY_LOOKUP_TYPE.INTERPOLATE); 
			List<String> greaterThanOrEqualList =  interPolatedKeyMap.get(RTKEY_LOOKUP_TYPE.GREATERTHANOREQUAL);
			List<String> lessThanOrEqualList =interPolatedKeyMap.get(RTKEY_LOOKUP_TYPE.LESSTHANOREQUAL);
			
			if(interPolatedKeyList != null || greaterThanOrEqualList != null || lessThanOrEqualList != null){
				if(interPolatedKeyList != null && interPolatedKeyList.size() >0){
					//AS WE ARE SPLITTING ONE STEP INTO FIVE STEPS WE ARE ADDING THESE STEPS IN THE ALGOSTEPS IN THE HANDLER ITSELF
					lookupStep = lookupStepCreater.createInterpolateLookupStep(context, aliasName, function, evallist,interPolatedKeyList);
				}else if(greaterThanOrEqualList != null && greaterThanOrEqualList.size()>0){
					//IF LOOKUP TYPE IS GREATER THAN OR EQUAL TO THEN WE ARE CREATING TWO LOOKUP STEPS
					lookupStep = lookupStepCreater.createGreateThanOREqualLookupStep(context, aliasName, function, evallist,greaterThanOrEqualList);
				}else if(lessThanOrEqualList != null && lessThanOrEqualList.size()>0){
					//IF LOOKUP TYPE IS LESS THAN OR EQUAL TO THEN WE ARE CREATING TWO LOOKUP STEPS
					lookupStep = lookupStepCreater.createLessThanOREqualLookupStep(context, aliasName, function, evallist,lessThanOrEqualList);
				}
			}
			else{
				lookupStep = lookupStepCreater.createDBLookupStep(context,aliasName,function,evallist);
			}
			
		}catch(Exception e){
			throw new ParserException(ExceptionMessages.RATETABLE_LOOKUP_STEP);
		}
		
		//context.putValue(ContextParam.LOCALVARIABLECOUNTER, localCounter);
		@SuppressWarnings("unchecked")
		List<AlgoStep> algoStep = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		
		if(lookupStep != null && lookupStep instanceof LookupStep){
			algoStep.add((LookupStep)lookupStep);
		}
		else if(lookupStep instanceof String)
		{	
			aliasName = (String) lookupStep;
		}
		
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		return aliasName;
	}

}
