package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.IdGeneration;
import com.mmpnc.icm.common.ide.models.ConstraintType;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.LookupCriteriaModel;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.util.IsoConsumptionUtil;

public class DomaintableLookupHandler {
	
	//@SuppressWarnings("unused")
	public static LookupStep createDomainTableLookup( Context context,String aliasName,List<Evaluator> evallist)throws RecognitionException{
		//CREATING RATING ENGINE LOOKUP STEP
		
		String function = getFunctionName(evallist, context);
		
		
		if(function.contains("\"")){
			function = function.replaceAll("\"", "");
		}
		
		LookupNameDecider lookupNameDecider = (LookupNameDecider)context.getValue(ContextParam.LOOKUP_NAME_DECIDER);
		String lookupName = lookupNameDecider.decideLookupName((String)context.getValue(ContextParam.SCOPE)+function);
		
		LookupStep lookupStep = new LookupStep();
		lookupStep.setName(lookupName);
		lookupStep.setRemark(Constants.DOMAIN_LOOKUP_REMARK);
		lookupStep.setStepType(Constants.LOOKUP_STEPTYPE);
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		
		if(parentCondition != null)
		lookupStep.setParentCondition(parentCondition);
		
		
		
		//This is required for global lookup and can be changed so kept in  the properties files
		LookupModel globalLookupModel  = createGlobalLookupModel(lookupName, function);
		
		//Creating local lookup model
		 LookupModel localLookupModel = createLocalLookupModel(lookupName, function);
		
		
		LookupCriteriaModel globalLookupCriteriaModel = new LookupCriteriaModel();
		LookupCriteriaModel localLookupCriteriaModel = new LookupCriteriaModel();
		
		
		FactPattern globalFactPattern = new FactPattern();
		FactPattern localLookupFactPattern = new FactPattern();
		
		
		//LookupCriteriaModel lookupCriteriaModel = new LookupCriteriaModel();
		//lookupModel.setLookupCriteriaModel(lookupCriteriaModel);
		
		FactPattern factPattern = new FactPattern();
		
		//creating filters for domain table 
		createFiltersForDomainTableLookup(evallist, context, globalFactPattern, localLookupFactPattern, factPattern, lookupName);
		
		
		globalLookupModel.setLookupCriteriaModel(globalLookupCriteriaModel);
		globalLookupCriteriaModel.addLhsItem(globalFactPattern);
		LookupModels lookupModels = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
		//ADDING GLOBAL LOOKUP MODEL INTO THE LOOKUP MODEL LIST
		lookupModels.add(globalLookupModel);
				
		//context.putValue(ContextParam.GLOBALLOOKUPMODLELIST, lookupModels);
				
		//THIS WILL BE THE PART OF ALGO STEP LIST FOR REFERENCE TO GLOBAL LOOKUP
		Attribute assignedAttribute = new Attribute();
		List<Attribute> assignedAttributes = new ArrayList<Attribute>();
		assignedAttributes.add(assignedAttribute);
		//SETTING VALUES TO ASSIGNED ATTRIBUTE
		//IF THREE NAMES IN DOMAIN TABLE THEN RETURN COLUMN NAME IS DATAVALUE
		//AND IF THE NUMBER OF CONTENTS IN DOMAIN TABLE DEFINATION IS 4 THEN GET THE LAST NAME AS RETURN
			
		decideReturnAttrOfDomainTableLookup(evallist, assignedAttribute, context);
		
		assignedAttribute.setAlias(aliasName);
		assignedAttribute.setLocalIndicator(Constants.LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR);
				
		//ADDING ID TO THE RETURN ATTRIBUTE
		assignedAttribute.setId(IdGeneration.idGenerator(lookupName));
				
		globalLookupModel.setAssignedAttributes(assignedAttributes);
		localLookupModel.setAssignedAttributes(assignedAttributes);
				
		if(localLookupFactPattern.getFieldConstraints().length>0){
			localLookupCriteriaModel.addLhsItem(localLookupFactPattern);
		}
				
		String lookupAliasName = lookupName+aliasName;
		String lookupId = IdGeneration.idGenerator(lookupAliasName);
		globalLookupModel.setLookuoID(lookupId);
		localLookupModel.setLookuoID(lookupId);
			
		localLookupModel.setLookupCriteriaModel(localLookupCriteriaModel);
		//lookupModel2.setLookuoID(lookupId);
				
		//Setting id for LookupCriteria 
		lookupStep.setLookupmodel(localLookupModel);
		//Setting id for LookupCriteria 
		return lookupStep;
		
	}
	
	private static String getFunctionName(List<Evaluator>evallist,Context context)throws RecognitionException{
		Evaluator functionNameevaluator = evallist.get(0);
		functionNameevaluator.setContext(context);
		StringBuffer functions =(StringBuffer)functionNameevaluator.evaluate();
		//REMOVING THE NAME OF THE DOMAIN TABLE
		evallist.remove(0);
		return functions.toString();
	}
	
	
	private static LookupModel createGlobalLookupModel(String lookupName,String function){
		LookupModel globalLookupModel = new LookupModel();
		globalLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		globalLookupModel.setLookupDesc("DOMAIN TABLE LOOKUP STEP");
		globalLookupModel.setBaseLookUpCriteria(lookupName);
		String lobValue = IsoConsumptionUtil.getInstance().getProperty(Configurer.getInstance().getBaseConfig().getLineOfBusiness().replaceAll(" ", ""));
		globalLookupModel.setBaseModel(lobValue+function);
		globalLookupModel.setLookupName(lookupName);
		globalLookupModel.name = lookupName;
		globalLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
		globalLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
		globalLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		globalLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		return globalLookupModel;
	}
	
	private static LookupModel createLocalLookupModel(String lookupName,String function){
		LookupModel localLookupModel = new LookupModel();
		localLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		localLookupModel.setLookupDesc("DOMAIN TABLE LOOKUP STEP");
		
		localLookupModel.setBaseLookUpCriteria(lookupName);
		String lobValue = IsoConsumptionUtil.getInstance().getProperty(Configurer.getInstance().getBaseConfig().getLineOfBusiness().replaceAll(" ", ""));
		localLookupModel.setBaseModel(lobValue+function);
		localLookupModel.setLookupName(lookupName);
		localLookupModel.name = lookupName;
		localLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
		localLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
		localLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		localLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		return localLookupModel;
	}
	
	/**
	 * @param evallist
	 * @param assignedAttribute
	 * @param context
	 * @throws RecognitionException
	 * 
	 * This method will decide the return attribute of the domain table
	 * 
	 */
	private static void decideReturnAttrOfDomainTableLookup(List<Evaluator>evallist,Attribute assignedAttribute,Context context)throws RecognitionException{
		if(evallist.size() % 2 == 0)
			assignedAttribute.setName(Constants.DOMAIN_TABLE_DEFAULT_LOOKUP_RETURN);
		else{
			Evaluator returnValEval = evallist.get(evallist.size()-1); 
			returnValEval.setContext(context);
			String returnAttributeName = ((StringBuffer)returnValEval.evaluate()).toString();
			if(returnAttributeName.contains("\"")){
				returnAttributeName = returnAttributeName.replace("\"", "");
			}
			assignedAttribute.setName(returnAttributeName);
			
		}
	}
	
	/**
	 * @param evallist
	 * @param context
	 * @param globalFactPattern
	 * @param localLookupFactPattern
	 * @param factPattern
	 * @param lookupName
	 * @throws RecognitionException
	 * 
	 * Creating filters for domain table lookup
	 * 
	 */
	private static void createFiltersForDomainTableLookup(List<Evaluator> evallist,Context context,FactPattern globalFactPattern,FactPattern localLookupFactPattern,FactPattern factPattern,String lookupName)throws RecognitionException{
		int order = 1;
		
		Iterator<Evaluator> itr = evallist.iterator();
		int iteratorIdextToExit = getListIndextToExitTheIterator(evallist);		
		int interatorCounter = 0;
		if(evallist.size()>1)
				while(itr.hasNext()){
						SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
						Evaluator lhsEvaluator = itr.next();
						interatorCounter++;
						lhsEvaluator.setContext(context);
						String filterLHS = lhsEvaluator.evaluate().toString();
						if(filterLHS.contains("\"")){
							filterLHS = filterLHS.replaceAll("\"", "");
						}
						//singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
						singleFieldConstraintEBLeftSide.setOperator(Constants.LOOKUP_OPERATOR);
						//singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_RUNTIME_VARIABLE);
						
						//Getting next evaluator
						Evaluator rhsEvaluator = itr.next();
						interatorCounter++;
						rhsEvaluator.setContext(context);
						
						String filterValue = rhsEvaluator.evaluate().toString();
						//singleFieldConstraintEBLeftSide.setValue(filterValue);
						singleFieldConstraintEBLeftSide.setOrderNo(order);
						
						
						boolean isModelAttribtue = false;
						boolean isSourceStaticValue = false;
						Object sourcestaticVal = context.getValue(ContextParam.ISSOURCESTATICVAR);
						Object modelAttr = context.getValue(ContextParam.MODELATTRIBUTE);
						
						if(sourcestaticVal!= null){
							isSourceStaticValue = (Boolean)sourcestaticVal;
						}
						if(modelAttr != null){
							isModelAttribtue = (Boolean)modelAttr;
						}
						
						if(isModelAttribtue){
							
							singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_M);
							singleFieldConstraintEBLeftSide.setValue(filterLHS);
							singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
							String sourceObject = filterValue.replace(".", "~").split("~")[0];
							singleFieldConstraintEBLeftSide.setSourceObject(sourceObject);
							String sourceObjectAttr = filterValue.replace(sourceObject+".", "").trim();
							singleFieldConstraintEBLeftSide.setSourceObjectAttr(sourceObjectAttr);
							singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_UNDEFINED);
							singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
							globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
						}
						else if(isSourceStaticValue){
							
							singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_S);
							singleFieldConstraintEBLeftSide.setValue(filterValue);
							singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
							singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
							singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
							globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
							
						
						}
						else{
							singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_A);
							singleFieldConstraintEBLeftSide.setValue(filterValue);
							singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
							localLookupFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
							singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
						}
						order++;
						
						String lookupFilterID = Integer.toString(order) +lookupName; 
						String id= IdGeneration.idGenerator(lookupFilterID);
						factPattern.addConstraint(singleFieldConstraintEBLeftSide);
						singleFieldConstraintEBLeftSide.setId(id);
						
						//if the number of domain table arguments are 
						if(iteratorIdextToExit == interatorCounter)
						 break;
				
				}
				
	}
	
	private static int getListIndextToExitTheIterator(List<Evaluator> evallist){
		
		int indexToExit = 0;
		int size = evallist.size();
		if(size % 2 == 0 ){
			indexToExit = evallist.size();
		}else{
			indexToExit = evallist.size()-1;
		}
		return indexToExit;
	
	}
	
}
