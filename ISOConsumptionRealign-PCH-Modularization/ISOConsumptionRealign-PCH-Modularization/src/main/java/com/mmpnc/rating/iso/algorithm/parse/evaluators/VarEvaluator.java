package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.DateYearMonthHandlerUtil;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;

/**
 * @author nilkanth9581
 *
 */
public class VarEvaluator implements Evaluator {

	private Context context;
	private String constant;
	private String variable;
	
	public VarEvaluator(String string, String string2) {
		this.constant = string;
		this.variable = string2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException {
		
		Database dsDb = (Database)context.getValue(ContextParam.OBJECTDATABASE);
		String scopePath = (String)context.getValue(ContextParam.SCOPE);
		//boolean daysFunction = false;
		
		if(variable.equals("null")){
			return "null";
		}
		
		
		//IF THE VARIABLE NAME CONTAINS .Days or .Month of .Year WE NEED TO CONVERT THAT VALRIABLE INTO
		//A RATING CUSTOM FUNCTION
		if(variable.endsWith(".Days") || variable.endsWith(".Month") || variable.endsWith(".Year")){
			
			String functionReturnStr = DateYearMonthHandlerUtil.handleDateYearOrMonthInXpath(dsDb, variable, scopePath); 
			return new StringBuffer(functionReturnStr);
			
		}
		
		//IF VARIABLE TYPE LV_TIMESTAMP MEANS ITS DAYS DIFF CUSTOM FUNCTION
		if(constant != null && constant.equals("LV_TIMESPAN")){
			context.putValue(ContextParam.DAYS_DIFF_FUNCTION, true);
		}
		
		if(variable.endsWith(".Date") && (Boolean)context.getValue(ContextParam.DAYS_DIFF_FUNCTION)){
			variable = variable.replace(".Date", "");
		}
		
		String qualifiedPath = null;
		//IF LOOP THORUGH HAS MULTIPLE LEVEL LOOPING THEN WE NEED TO MANAGE A SINGLE LEVEL LOOPING AT ONE TIME
		
		if(context.getValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE) != null){
			return handleLoopThroughStarCondition(qualifiedPath, dsDb, scopePath);
		}
		else{
			
			if(!("true".equals(variable.trim()) || ("false".equals(variable.trim()))) && (constant == null || "COLUMN_NUMERIC".equals(constant) || "COLUMN_STRING".equals(constant))){
				qualifiedPath = XpathUtil.getFullyQualifiedXpath(dsDb, scopePath, variable,false);
				if("".equals(qualifiedPath)){
					qualifiedPath = variable;
				}
				if(!qualifiedPath.equals(variable))
				context.putValue(ContextParam.MODELATTRIBUTE, true);
				context.putValue(ContextParam.ISSOURCESTATICVAR, false);
				return new StringBuffer(qualifiedPath);
			}
			
			context.putValue(ContextParam.ISSOURCESTATICVAR, false);
			context.putValue(ContextParam.MODELATTRIBUTE, false);
			return new StringBuffer(variable);
			
			
		}
		
	}
	
	private StringBuffer handleLoopThroughStarCondition(String qualifiedPath,Database dsDB,String scopePath) {
		
		qualifiedPath = XpathUtil.getFullyQualifiedXpath(dsDB, scopePath, variable,false);
		if("".equals(qualifiedPath)){
			qualifiedPath = variable;
		}else if(qualifiedPath.equals(context.getValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE))){
			qualifiedPath =(String) context.getValue(ContextParam.LOOPTHROUGHREPLACEBY);
			context.putValue(ContextParam.ISSOURCESTATICVAR, false);
			context.putValue(ContextParam.MODELATTRIBUTE, true);
		}else{
			findExactQualifiedPathInLoopStarCondition(qualifiedPath);
			context.putValue(ContextParam.ISSOURCESTATICVAR, false);
			context.putValue(ContextParam.MODELATTRIBUTE, true);
		}
		
		return new StringBuffer(qualifiedPath);
	
	}
	
	//THIS METHOD ADDED TO HANDLE LOOP THROUGH CONDITION WHICH RESULT INTO MULTIPLE STEPS
	private String findExactQualifiedPathInLoopStarCondition(String qualifiedPath){
		//THIS CONDITION ADDED TO HANDLE A LOOP THROUGH CONDITION WHERE LOOP THROUGH CONDITION XPATH IS NOT EQUAL TO 
		//XPATH IN LOOP STATEMENTS 
		//FOUND IN CA_CA ALGO FILE FINANCIALRESPOSIBILITY COVERAGE
		//loop through:../*[(name(.)='CommercialAutoTruck' or name(.)='CommercialAutoZoneRated' or name(.)='CommercialAutoPrivatePassenger' or name(.)='CommercialAutoPublicTransportation' or name(.)='CommercialAutoSpecialTypes')]
		//if condition xpath:../*[(name(.)='CommercialAutoTruck' or name(.)='CommercialAutoZoneRated' or name(.)='CommercialAutoPrivatePassenger' or name(.)='CommercialAutoPublicTransportation' or name(.)='CommercialAutoSpecialTypes')]/CommercialAutoVehicleLiabilityCoverage/Premium
		String loopthroughCondition = (String) context.getValue(ContextParam.LOOPTHROUGHREPLACEBY);
		//SPLITING QUALIFIEDPATH BY SPACE
		if(qualifiedPath.contains(" ")){
			String []xPathArr = qualifiedPath.split(" ");
			for(String xpath:xPathArr){
				if(xpath.trim().startsWith("Policy")){
					xpath = " ".concat(xpath);
					xpath = xpath.replace(" Policy", "CCSRERequest");
				}
				if(xpath.startsWith(loopthroughCondition)){
					return xpath;
				}
			}
		}
		return qualifiedPath;
	}
	

}