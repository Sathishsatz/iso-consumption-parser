package com.mmpnc.rating.iso.config;

/**
 * @author nilkanth9581
 *
 */
public interface Configuration {
	public Configuration createConfiguration(String fileLocation);
}
