package com.mmpnc.rating.iso.publish.lookup;

import java.sql.Connection;

import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.server.common.dao.LookupDAO;

/**
 * @author nilkanth9581
 *
 */
public class LookupPersistHelper extends LookupDAO {
	//LookupModel lookupModel;

	public LookupPersistHelper(Connection connection, String userName) {
		super(connection,userName);
	}

	public void persist_flow(LookupModel lookupModel, Connection con)
			throws Exception {
		deleteLookupCriteriaFilter(lookupModel);
		deleteLookupCriteriaReturnAttr(lookupModel);
		deleteLookupCriteria(lookupModel);
		insertLookupCriteria(lookupModel);
		insertLookupCriteriaFilter(lookupModel);
		insertLookupCriteriaReturnAttr(lookupModel);
	}
}
