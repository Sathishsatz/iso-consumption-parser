package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.ArgumentVariable;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class FunctionEvaluator implements Evaluator {
	private Evaluator functionName;
	private List<Evaluator> evalList;
	private Context context;
	
	public FunctionEvaluator(Evaluator fname, List<Evaluator> evallist) {
		this.functionName = fname;
		this.evalList = evallist;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		boolean flag = false;
		
//		System.out.println("Function Evaluator called");
		
		this.context.putValue(ContextParam.ISFUNCTION, true);
		this.functionName.setContext(this.context);
		this.functionName.evaluate();		
		String functionName = (String) context.getValue(ContextParam.FUNCTIONNAME);		
		this.context.putValue(ContextParam.ISFUNCTION, false);
		
		if(functionName.equals("Count")){
			ISOConsumptionLogger.info("Function Name " + functionName);
			this.context.putValue(ContextParam.ISPREDICATE, true);
		}
		
		StringBuffer buffer = (StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		
		buffer.append("(");
		for(Evaluator eval : evalList){
			if(flag){
				buffer.append(" , ");
			}
			
			StringBuffer evalBuffer = new StringBuffer();
			this.context.putValue(ContextParam.XPATHSTRING, evalBuffer);
			eval.setContext(this.context);
			Type type = (Type) eval.evaluate();
			
			if(type.equals(Type.PATH)){
				buffer.append("XPATH:\"").append(context.getValue(ContextParam.XPATHSTRING)).append("\"");
			} else if(type.equals(Type.ATTRIBUTE)){
				buffer.append(ArgumentVariable.assignType((Database) context
						.getValue(ContextParam.OBJECTDATABASE),
						(String) context.getValue(ContextParam.REFERENCE),
						(String) context.getValue(ContextParam.SCOPE),
						context.getValue(ContextParam.XPATHSTRING).toString()));
			} else{
				buffer.append(context.getValue(ContextParam.XPATHSTRING));
			}
				
			flag = true;
		}
		buffer.append(")");
		
		if(functionName.equals("Count")){
			context.putValue(ContextParam.ISPREDICATE, false);
		}
		context.putValue(ContextParam.XPATHSTRING, buffer);
		
		return Type.OPERATION;
	}

}
