package com.mmpnc.rating.iso.helper;

import com.mmpnc.icm.common.ide.models.ArithmeticStep;

/**
 * @author nilkanth9581
 *
 */
public class ArithmaticStepCreater extends BaseStepCreater {
	
	/**
	 * @param parentCondition
	 * @param expression
	 * @param aliasName
	 * @return
	 */
	private static ArithmaticStepCreater arithmaticStepCreater = null;
	private ArithmaticStepCreater(){
		
	}
	
	/**
	 * @return
	 */
	public static ArithmaticStepCreater getInstance(){
		if(arithmaticStepCreater == null){
			arithmaticStepCreater = new ArithmaticStepCreater();
		}
		return arithmaticStepCreater;
	}
	
	
	/**
	 * @param parentCondition
	 * @param aliasName
	 * @param expression
	 * @param precisionScale
	 * @return
	 */
	public ArithmeticStep createArithmeticStep(String parentCondition,String aliasName,String expression,String precisionScale){
		ArithmeticStep arithmeticStep = new ArithmeticStep();
		arithmeticStep.setName(Constants.EXP_STEP_NAME+"#"+getCounter(STEPTYPE.EXP));
		arithmeticStep.setRemark("DefaultRemark");
		if(aliasName != null)
		arithmeticStep.setAlias(aliasName);
		if(expression != null)
		arithmeticStep.setExpression(expression);
		if(parentCondition != null)
		arithmeticStep.setParentCondition(parentCondition);
		if(precisionScale != null)
		arithmeticStep.setPrecisionScale(precisionScale);
		if(precisionScale != null)
		arithmeticStep.setPrecisionType("R");
		arithmeticStep.setStepType(Constants.IF_EXP);
		return arithmeticStep;
		
	}
}
