package com.mmpnc.rating.iso.algorithm.sort;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.TreeNodeStream;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.AlgorithmProcessor;
import com.mmpnc.rating.iso.algorithm.AlgorithmReader;
import com.mmpnc.rating.iso.algorithm.AlgorithmReaderImpl;
import com.mmpnc.rating.iso.algorithm.EvaluatorFactory;
import com.mmpnc.rating.iso.algorithm.EvaluatorFactory.EvaluatorType;
import com.mmpnc.rating.iso.algorithm.exception.SyntaxRecognitionException;
import com.mmpnc.rating.iso.algorithm.parse.AlgorithmWalker;

public class AlgorithmSorter implements AlgorithmProcessor {
	private Context context;
	private StringBuffer buffer;

	public AlgorithmSorter(Context context, StringBuffer buffer) {
		this.context = context;
		this.buffer = buffer;
	}

	@Override
	public Object execute() throws Exception {
		AlgorithmReader reader = new AlgorithmReaderImpl(buffer);
		AlgorithmWalker tree = new AlgorithmWalker(
				(TreeNodeStream) reader.execute());
		
		EvaluatorFactory factory = new EvaluatorFactory(EvaluatorType.SORT);
		tree.setEvaluatorFactory(factory);
		
		try {
//			System.out.println(tree);
			tree.algorithm(context);
			
		} catch (RecognitionException e) {
			StringBuilder sb = new StringBuilder();
			sb.append("Algo Text: ");
			sb.append("\n").append(buffer.toString());
			//String errMsg = tree.getErrorHeader(e) + " : " + tree.getErrorMessage(e, tree.getTokenNames());
			sb.append("Error Message: ").append(e.getMessage());
			throw new SyntaxRecognitionException(sb.toString());
		}
		return null;
	}

}
