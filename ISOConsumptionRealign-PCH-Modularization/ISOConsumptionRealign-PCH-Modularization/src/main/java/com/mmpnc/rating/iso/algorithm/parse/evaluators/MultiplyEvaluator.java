package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class MultiplyEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	
	public MultiplyEvaluator(Evaluator e1, Evaluator e2) {
		// TODO Auto-generated constructor stub
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		//System.out.println("left.getClass().getSimpleName()="+left.getClass().getSimpleName());
		StringBuffer multipy = new StringBuffer();
		multipy.append(" ( ");
		left.setContext(context);
		multipy.append(left.evaluate());
		multipy.append(" * ");
		right.setContext(context);
		multipy.append(right.evaluate());
		multipy.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return multipy;
	}

}
