package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class BooleanEvaluator implements Evaluator {

	private Context context ;
	private Evaluator condition;
	
	public BooleanEvaluator(Evaluator bool) {
		this.condition = bool;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		this.condition.setContext(context);
		//System.out.println("condition.getClass().getSimpleName()="+condition.getClass().getSimpleName());
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return this.condition.evaluate();
	}

}
