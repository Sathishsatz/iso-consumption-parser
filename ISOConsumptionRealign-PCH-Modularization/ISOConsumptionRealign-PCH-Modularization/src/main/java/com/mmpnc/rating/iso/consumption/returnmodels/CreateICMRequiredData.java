package com.mmpnc.rating.iso.consumption.returnmodels;

import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public class CreateICMRequiredData implements ICreateIcmRequireData{

	private static CreateICMRequiredData icmRequiredData;
	
	public static CreateICMRequiredData getInstance(){
		if(icmRequiredData == null){
			icmRequiredData = new CreateICMRequiredData();
		}
		return icmRequiredData;
	}
	
	@Override
	public void createIcmRequiredDataModel(String referenceType,Map<String,List<IcmRatingNodeContent>> ratingNodeMap,List<IcmRatingNodeContent> icmNodeContentList) {
		
		specialHandlingForPremOpsCov(referenceType, ratingNodeMap, icmNodeContentList);
	
	}
	
	
	private void handleIcmRequiredDataModel(String referenceType,Map<String,List<IcmRatingNodeContent>> ratingNodeMap,List<IcmRatingNodeContent> icmNodeContentList){
		
		List<IcmRatingNodeContent> nodeList  = ratingNodeMap.containsKey(referenceType)?ratingNodeMap.get(referenceType):null;
		if(nodeList == null && icmNodeContentList!= null && icmNodeContentList.size()>0){
			
			ratingNodeMap.put(referenceType,icmNodeContentList);
		}
		else if(icmNodeContentList!=  null && icmNodeContentList.size()>0){
			nodeList.addAll(icmNodeContentList);	
		}
	
	}
	
	
	private void  specialHandlingForPremOpsCov(String referenceType,Map<String,List<IcmRatingNodeContent>>ratingNodeMap,List<IcmRatingNodeContent> icmNodeContentList){
		
		/*COMMENTED AS MERGING FUNCTIONALITY WILL GO AND MERGE THESE ALGORITHMS
		 * List<IcmRatingNodeContent> commonRatingList = new ArrayList<IcmRatingNodeContent>();
		List<IcmRatingNodeContent> premiumCalculationList = new ArrayList<IcmRatingNodeContent>();
		
		for(IcmRatingNodeContent icmRatingNodeContent: icmNodeContentList){
			int pass = icmRatingNodeContent.getPass();
			String dbTables = icmRatingNodeContent.getDbTables();
			if(Constants.GL_PREMOPS_COVERAGE.equals(dbTables) || Constants.GL_PRODOPS_COVERAGE.equals(dbTables)){
				if(pass == 1){
					commonRatingList.add(icmRatingNodeContent);
				}
				else if(pass == 2){
					premiumCalculationList.add(icmRatingNodeContent);
				}
			}
		}
		if (commonRatingList.size()>0) {
			handleIcmRequiredDataModel(Constants.REFERENCE_TYPE_COMMON_RATING, ratingNodeMap, commonRatingList);
		}
		
		if(premiumCalculationList.size()>0){
			handleIcmRequiredDataModel(Constants.PRFERENCE_TYPE_PREMIUM_CALCULATION, ratingNodeMap, premiumCalculationList);
		}
		
		if(commonRatingList.size() == 0 && premiumCalculationList.size()==0){
*/			
		handleIcmRequiredDataModel(referenceType, ratingNodeMap, icmNodeContentList);
//		}
	}
	
}
