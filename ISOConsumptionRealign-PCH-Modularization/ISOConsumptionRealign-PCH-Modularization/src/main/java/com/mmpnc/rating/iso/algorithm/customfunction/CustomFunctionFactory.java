package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.HashMap;

import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;


/**
 * @author nilkanth9581
 *
 */
public class CustomFunctionFactory {
	
	private CustomFunctionFactory(){
		
	}
	
	private HashMap<String, CUSTOM_FUNCTION> functioNameMap = new HashMap<String, CUSTOM_FUNCTION>();
	private static CustomFunctionFactory customFunctionFactory = null;
	/**
	 * @return
	 */
	public static CustomFunctionFactory getInstance(){
		if(customFunctionFactory == null){
			customFunctionFactory = new CustomFunctionFactory();
			customFunctionFactory.populateFunctionNameMap();
		}
		return customFunctionFactory;
	}
	
	
	private void populateFunctionNameMap(){
		functioNameMap.put("RateTable", CUSTOM_FUNCTION.RATE_TABLE);
		functioNameMap.put("domainTableLookup", CUSTOM_FUNCTION.DOMAIN_TABLE);
		functioNameMap.put("domainCountyTableLookup", CUSTOM_FUNCTION.DOMAIN_TABLE);
		functioNameMap.put("domainZipCodeTableLookup", CUSTOM_FUNCTION.DOMAIN_TABLE);
		functioNameMap.put("Count", CUSTOM_FUNCTION.COUNT);
		functioNameMap.put("GetLeapDays", CUSTOM_FUNCTION.GET_LEAP_DAYS);
		functioNameMap.put("GetInteger", CUSTOM_FUNCTION.GET_INTEGER);
		functioNameMap.put("GetYear", CUSTOM_FUNCTION.GET_YEAR);
		functioNameMap.put("GetMonth", CUSTOM_FUNCTION.GET_MONTH);
		functioNameMap.put("GetDate", CUSTOM_FUNCTION.GET_DATE);
		functioNameMap.put("BusPrsnl", CUSTOM_FUNCTION.BUSN_PRSNL);
		functioNameMap.put("defaule", CUSTOM_FUNCTION.DEFAULT_HANDLER);
		functioNameMap.put("AddYears", CUSTOM_FUNCTION.ADD_YEARS);
		functioNameMap.put("startsWith", CUSTOM_FUNCTION.STARTS_WITH);
		functioNameMap.put("getProgram", CUSTOM_FUNCTION.GET_PROGRAM);
		functioNameMap.put("days_between",CUSTOM_FUNCTION.DAYS_BETWEEN);
		functioNameMap.put("Contains",CUSTOM_FUNCTION.CONTAINS);
		functioNameMap.put("GetStringNodeValue",CUSTOM_FUNCTION.GET_STRING_NODE_VALUE);
		functioNameMap.put("GetDeductible", CUSTOM_FUNCTION.GET_DEDUCTIBLE);
		functioNameMap.put("GetDays", CUSTOM_FUNCTION.GET_DAYS);
		functioNameMap.put("GetDayOfMonth", CUSTOM_FUNCTION.GET_DAY);
	}
	
		
	public FunctionHandler getHandler(String funName)throws ParserException{
		FunctionHandler functionHandler = null;
		
		CUSTOM_FUNCTION cust = functioNameMap.get(funName);
		if (cust == null){
			throw new ParserException("CUSTOM FUNCTION NOT FOUND="+funName);
		}
		switch(cust){
		
			case RATE_TABLE:
				functionHandler = new RateTableFunctionHandler();
				break;
			case DOMAIN_TABLE:
				functionHandler = new DomainTableFunctionHandler();
				break;
			case GET_YEAR: 
				functionHandler = new GetYearFunctionHandler();
				break;
			case GET_MONTH:
				functionHandler = new GetMonthFunctionHandler();
				break;
			case GET_DATE:
				functionHandler = new GetDateFunctionHandler();
				break;
			case GET_INTEGER:
				functionHandler = new GetIntegerFunctionHandler();
				break;
			case BUSN_PRSNL:
				functionHandler = new BusnPrsnPropLimitOfInsRelativityFactorFunctionHandler();
				break;
			case COUNT:
				functionHandler = new CountFunctionHandler();
				break;
			case LOOKUP_LOSS_DEV:
				functionHandler = new LookupLossDevelopmentFactorFunctionHandler();
				break;
			case ADD_YEARS:
				functionHandler = new AddYearsHandler();
				break;
			case GET_LEAP_DAYS:
				functionHandler = new GetLeapDaysHandler();
				break;
			case STARTS_WITH:
				functionHandler = new StartsWithHandler();
				break;
			case GET_PROGRAM:
				functionHandler = new CallFunctionHandler();
				break;
			case DAYS_BETWEEN:
				functionHandler = new DaysBetweenFunctionHandler();
				break;
			case CONTAINS:
				functionHandler = new ContainsFunctionHandler();
				break;
			case GET_STRING_NODE_VALUE:
				functionHandler = new GetStringNodeValueFunctionHandler();
				break;
			case GET_DEDUCTIBLE:
				functionHandler = new GetDeductibleFunctionHandler();
				break;
			case GET_DAYS:
				functionHandler = new GetDaysFunctionHandler();
				break;
			case GET_DAY:
				functionHandler = new GetDayFunctionHandler();
				break;
			default:
				ISOConsumptionLogger.info("CUSTOM FUNCTION NEEDS TO BE HANDLED "+funName);
		}
		
		return functionHandler;
	}
	
	
}