package com.mmpnc.rating.iso.algorithm.correction;

import com.mmpnc.rating.iso.algorithm.vo.Premium;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @author nilkanth9581
 * This converter will be used by xstram api to convert string into premium object
 */
public class PremiumObjectConverter implements Converter{
	
	public void marshal(Object value, HierarchicalStreamWriter writer,
			MarshallingContext context) {

		Premium premium = (Premium) value;
		writer.addAttribute("variables", premium.getVariables());
		writer.addAttribute("className", premium.getClassName());
		writer.setValue(premium.getClassName());
	}

	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {

		Premium premium = new Premium();
		premium.setClassName(reader.getAttribute("className"));
		premium.setVariables(reader.getAttribute("variables"));
		return premium;
	}

	public boolean canConvert(@SuppressWarnings("rawtypes") Class clazz) {
		return clazz.equals(Premium.class);
	}
}
