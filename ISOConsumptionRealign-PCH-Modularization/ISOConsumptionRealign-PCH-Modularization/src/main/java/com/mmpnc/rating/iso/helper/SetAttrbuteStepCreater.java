package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.List;

import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.ide.models.ComplexAttribute;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;

/**
 * @author nilkanth9581
 *
 */
public class SetAttrbuteStepCreater extends BaseStepCreater{
	
	private static SetAttrbuteStepCreater setAttrbuteStepCreater;
	
	private SetAttrbuteStepCreater(){
		
	}
	
	public static SetAttrbuteStepCreater getInstance(){
		if(setAttrbuteStepCreater == null){
			setAttrbuteStepCreater = new SetAttrbuteStepCreater();
		}
		return setAttrbuteStepCreater;
	}
	
	public LookupStep createSetAttributeStep(String LHS, String RHS,String parentStepCondition,boolean rhsModelAttrFlag){
		
		LookupStep lookupStep = new LookupStep();
		ComplexAttribute attribute = new ComplexAttribute();
		List<Attribute> assingedAttributes = new ArrayList<Attribute>();
		LookupModel lookupModel =  new LookupModel();
		lookupStep.setLookupmodel(lookupModel);
		lookupModel.setAssignedAttributes(assingedAttributes);
		
		
		lookupStep.setName(Constants.SETATTR_NAME+"#"+getCounter(STEPTYPE.SET_ATTRIBUTE));
		lookupStep.setRemark(Constants.SETATTR_REAMRK);
		lookupStep.setStepType(Constants.SETATTR_STEP_TYPE);
		if(parentStepCondition != null)
		lookupStep.setParentCondition(parentStepCondition);
		
		lookupModel.setBaseModel(Constants.SET_ATTR_BASE_MODEL);	
		
		
		
		assingedAttributes.add(attribute);
		
		//if(rhsModelAttrFlag)
		attribute.setAlias(RHS);  //This alias in case of set attribute will be a sourcevariable in algorithm detail
		
		attribute.setLocalIndicator(Constants.SETATTR_LOCAL_INDICATOR);
		//attribute.setAlias(alias)
				
		SingleFieldConstraintEBLeftSide singleFieldConstraint = new SingleFieldConstraintEBLeftSide();
		//boolean flag =(Boolean) context.getValue(ContextParam.MODELATTRIBUTE);
		//if()
		
		String [] arr = LHS.replace(".","~").split("~");
		String sourceObject  = arr[0];
		StringBuffer sb = new StringBuffer();
		for(int i=1;i<arr.length;i++){
			sb.append(arr[i]);
			if(i<arr.length-1)
			sb.append(".");
		}
		singleFieldConstraint.setValue(sb.toString());
		singleFieldConstraint.setSourceObject(sourceObject);
		attribute.setSingleFieldConstraint(singleFieldConstraint);
		return lookupStep;
	
	}
	
}
