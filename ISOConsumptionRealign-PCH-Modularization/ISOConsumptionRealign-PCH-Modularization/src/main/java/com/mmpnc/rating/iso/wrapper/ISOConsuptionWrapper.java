package com.mmpnc.rating.iso.wrapper;

import java.io.FileNotFoundException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.icm.common.BaseICMException;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.exception.UnInitializeContextException;
import com.mmpnc.rating.iso.algorithm.xml.AlgorithmXMLProcessor;
import com.mmpnc.rating.iso.algorithm.xml.NodeReference;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.LookupNameDecider;
import com.mmpnc.rating.iso.icmxml.creater.AlgorithmStepLookupPublisher;
import com.mmpnc.rating.iso.icmxml.creater.CreateIcmInputXml;
import com.mmpnc.rating.iso.icmxml.creater.IAlgorithmStepLookupPublisher;
import com.mmpnc.rating.iso.ratetablelookup.RatingContentFileLookup;

/**
 * @author nilkanth9581
 *
 */
public abstract class ISOConsuptionWrapper  extends AlgorithmXMLProcessor implements IISOConsuptionWrapper{
	
	//protected List<IcmRatingNodeContent> list = new ArrayList<IcmRatingNodeContent>();

	public ISOConsuptionWrapper(Reader algoReader) {
		super(algoReader);
	}
	
	//client program for iso consumption tool should override this method .
	public abstract void cosumptionPostProcess(ICMRequiredData icmRequiredData);
	
	@Override
	public void processAlgorithmTextFile(String pchName,IcmRatingNodeContent icmRatingNodeContent,StringBuffer algo,Database dsDatabase,NodeReference nodeReference,Database dsForLoop,LookupNameDecider lookupNameDecider,LocalVariableCounterHolder localVariableCounterHolder)throws Exception{
		//writeLanguageTextFile(algo);
		//System.out.println("After sorting algorithm text file="+algo);
		process(pchName,icmRatingNodeContent, algo,dsDatabase,nodeReference.getScope(),dsForLoop,lookupNameDecider,localVariableCounterHolder);
		//AFTER PROCESS ADDING THE GLOBAL LOOKUPS IN ICM REQUIRED DATA
		addLookupsFromtThisPCHToIcmRequiredData();
			
	}
	
	public void addLookupsFromtThisPCHToIcmRequiredData(){
		
		List<LookupModel> lookupModlist = CreateIcmInputXml.getInstance().getLookupModels().getLookupModels();
		if(lookupModlist.size()> 0)
			for(LookupModel lookupModel : lookupModlist)
			icmRequiredData.getLookupModels().add(lookupModel);
		
	}
	@Override
	public void publishAlgorithm(IcmRatingNodeContent icmRatingNodeContent)throws ParserException{
		
		IAlgorithmStepLookupPublisher ipuPublisher = AlgorithmStepLookupPublisher.getInstance();
		ipuPublisher.publishLookupModelsNew(icmRequiredData.getLookupModels());
		try{
		ipuPublisher.publishAlgorithmSteps(icmRatingNodeContent, icmRatingNodeContent.getDbTables());
		}catch(BaseICMException e){
			throw new ParserException("Problem while publishing the algorithm steps");
		}
	}
	
	@Override
	public void readSortedAlgoAndCreateICMInputXml(String propertiesFileLocation) throws FileNotFoundException, JAXBException, UnInitializeContextException,Exception{
		Configurer configurer = Configurer.getInstance();
		configurer.configure(propertiesFileLocation);
		Database db = createAndOpenXmlDBConnection(configurer);
		buildAlgorithm();
		db.closeDatabase();
		cosumptionPostProcess(icmRequiredData);
	}

	@Override
	public void readSortedAlgoAndCreateICMInputXml(Configurer config)throws Exception {
		config.configure(config);
		Database db = createAndOpenXmlDBConnection(config);
		buildAlgorithm();
		db.closeDatabase();
		cosumptionPostProcess(icmRequiredData);  
	}
	

	@Override
	public void process(String pchName,IcmRatingNodeContent icmRatingNodeContent,StringBuffer sb,Database dsDb,String scopePath,Database dsForLoop,LookupNameDecider lookupNameDecider,LocalVariableCounterHolder localVariableCounterHolder) throws Exception  {
		 
		CreateIcmInputXml.getInstance().createICMInputXml(pchName,icmRatingNodeContent,sb,dsDb,scopePath,dsForLoop,lookupNameDecider,localVariableCounterHolder);
	}
	
	@Override
	public void readAlgoFileAndCreateICMInputXml(List<Reader> contentFileReaderList,String ratingcategoryName,String lineOfBusiness,boolean isStateWideAlgorithms)throws Exception{
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		basicConfiguration.setLineOfBusiness(lineOfBusiness);
		//SETTING THE VALUE OF STATE WIDE ALGORITHMS FLAG IN BASIC CONFIGURAITON WHICH WILL BE USED TO 
		//DECIDE WHTTHER TO CONSUME ONLY OVERRIDEN PCH IN THE CURRENT SCOPE
		basicConfiguration.setStateWideAlgorithms(isStateWideAlgorithms);
		
		basicConfiguration.setIcmCategoryNameForRatingLookup(ratingcategoryName);
		//Configurer.getInstance().readDatabaseProperties("D:\\DEMO_ISO_ERC_29th_JAN\\iso-consuption-properties\\ISO_CONSUPTION.properties");
		Configurer.getInstance().buildConfigurer(basicConfiguration, Configurer.getInstance().getDBConfig(),null);
		Database db = createAndOpenXmlDBConnection(contentFileReaderList);
		//try{
		buildAlgorithm();
		//}catch(RecognitionException e){
		//	e.printStackTrace();
		//	throw e;
		//}
		db.closeDatabase();
		cosumptionPostProcess(icmRequiredData);
	}

	
	private Database createAndOpenXmlDBConnection(Configurer configurer) throws BasexException{
		List<String>ratingList = configurer.getBaseConfig().getRatingContentFilesList();
		Database db =  RatingContentFileLookup.getInstance().buildXmlDatabase(ratingList);
		return db;
	}
	
	
	/**
	 * @param reader
	 * @return
	 * @throws Exception
	 */
	public Database createAndOpenXmlDBConnection(List<Reader> reader)throws Exception{
		List<String> ratingContentFileList = new ArrayList<String>();
		//int counter = 0;
		for(Reader reader2 :  reader){
			//String str = IOUtils.toString(reader2);
			ratingContentFileList.add(IOUtils.toString(reader2));
			/*counter = counter+1;
			Writer writer = new FileWriter(new File("D:\\DEMO_ISO_ERC_29th_JAN\\input-xml-sorted-algo-file\\ContentFile1"+counter));
			writer.write(str);
			writer.flush();
			writer.close();*/
		}
		Database db =  RatingContentFileLookup.getInstance().buildXmlDatabase(ratingContentFileList);
		return db;
	}
	
}
