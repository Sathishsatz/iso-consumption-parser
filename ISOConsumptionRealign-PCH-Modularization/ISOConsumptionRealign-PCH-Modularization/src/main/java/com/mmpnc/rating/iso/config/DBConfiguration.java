package com.mmpnc.rating.iso.config;

import com.mmpnc.rating.iso.propertyreader.PropertyFileReader;
import com.mmpnc.rating.iso.propertyreader.PropertyParam;

/**
 * @author nilkanth9581
 *
 */
public class DBConfiguration implements Configuration{
	
	private String dbUrl;
	private String databaseUser;
	private String databasePassword;
	private String databaseDriverName;
	private DBConfiguration(){}
	
	private static DBConfiguration dbConfiguration = null;
	
	public static DBConfiguration getInstance(){
		if(dbConfiguration == null){
			dbConfiguration = new DBConfiguration();
		}
		return dbConfiguration;
	} 

		
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getDatabaseUser() {
		return databaseUser;
	}
	public void setDatabaseUser(String databaseUser) {
		this.databaseUser = databaseUser;
	}
	public String getDatabasePassword() {
		return databasePassword;
	}
	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}
	public String getDatabaseDriverName() {
		return databaseDriverName;
	}
	public void setDatabaseDriverName(String databaseDriverName) {
		this.databaseDriverName = databaseDriverName;
	}
	
	@Override
	public DBConfiguration createConfiguration(String fileLocation) {
		PropertyFileReader propertyFileReader = new PropertyFileReader(fileLocation);
		DBConfiguration dbConfiguration = new DBConfiguration();
		
		if("Y".equals(propertyFileReader.getProperty(PropertyParam.PUBLISH_LOOKUP_MODELS))){
			dbConfiguration.setDatabaseDriverName(propertyFileReader.getProperty(PropertyParam.DATABASE_DRIVER));
			dbConfiguration.setDatabasePassword(propertyFileReader.getProperty(PropertyParam.DATABASE_PASSWORD));
			dbConfiguration.setDatabaseUser(propertyFileReader.getProperty(PropertyParam.DATABASE_USERNAME));
			dbConfiguration.setDbUrl(propertyFileReader.getProperty(PropertyParam.DATABASE_URL));
		}
		return dbConfiguration;
	}
	
}
