package com.mmpnc.rating.iso.algorithm.correction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author Nilkanth9581
 *
 */
public class ProcessedPHCNameHolder {
	
	@SuppressWarnings("unused")
	private Map<String,List<String>>processedPCHMap = null;
	private ProcessedPHCNameHolder processedPHCNameHolder = null;
	
	private ProcessedPHCNameHolder(){
		if(processedPHCNameHolder == null){
			processedPHCNameHolder = new ProcessedPHCNameHolder();
			processedPCHMap = new HashMap<String, List<String>>();
		}
	}

	public void createUniqueIdForMap(Scope currentScope,int sequenceNo){
		@SuppressWarnings("unused")
		String idForMap = currentScope.getDbTables()+sequenceNo+currentScope.getPass();
	}
	
	public void addPCHListToMap(Scope currentScope,int sequenceNo,List<PCH> pchList){
		//current
	}
	
	
}
