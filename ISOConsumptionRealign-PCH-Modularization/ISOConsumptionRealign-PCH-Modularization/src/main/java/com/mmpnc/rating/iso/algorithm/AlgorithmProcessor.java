package com.mmpnc.rating.iso.algorithm;

public interface AlgorithmProcessor {
	Object execute() throws Exception;
}
