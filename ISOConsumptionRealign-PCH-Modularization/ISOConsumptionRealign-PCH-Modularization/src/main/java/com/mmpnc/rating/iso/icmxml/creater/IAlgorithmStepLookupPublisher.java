package com.mmpnc.rating.iso.icmxml.creater;

import com.mmpnc.icm.common.BaseICMException;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public interface IAlgorithmStepLookupPublisher {
	void publishAlgorithmSteps(IcmRatingNodeContent icmRatingNodeContent,String algorithmLevle) throws  BaseICMException;
	void publishLookupModelsNew(LookupModels lookupModels);
	void publishLookupModels(LookupModels lookupModels);
	void printAlgoSteps(IcmRatingNodeContent icmRatingNodeContent);
	void printLookupModels(LookupModels lookupModels);
}
