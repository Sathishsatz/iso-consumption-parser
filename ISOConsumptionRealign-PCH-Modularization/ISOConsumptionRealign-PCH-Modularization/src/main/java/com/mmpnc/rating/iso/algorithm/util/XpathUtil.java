package com.mmpnc.rating.iso.algorithm.util;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.rating.iso.helper.Constants;

/**
 * @author nilkanth9581
 *
 */
public class XpathUtil {
	
	
	public static String getFullyQualifiedXpath(Database db,String scope, String attribute,boolean isXpathEvalCall) {
		//System.out.println("getting xpath for ="+scope+"/"+attribute);
		Connection conn = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		
		StringBuffer qry = new StringBuffer();
		if(attribute.startsWith("/AscendantOne")){
			attribute = attribute.replace("/AscendantOne/", "");
			qry.append("//(").append(attribute).append(")/@xpath/data()");
		}
		//THIS CONDITION IS ADDED SPECIFICALLY FOR HO ALGORITHMS WHERE WE HAVE A SATEMENT LIKE /*/Premium
		else if(attribute.startsWith("/*/")){
			qry.append("(//").append(scope).append(attribute).append(")/@xpath/data()");
		}
		// TODO THis need to be tested for CA algorithms and xpath having // in start
		else if(attribute.startsWith("/")){
			if(attribute.startsWith("//")){
				attribute = attribute.substring(2);
			}else{
				attribute = attribute.substring(1);
			}
			
			qry.append("//(").append(attribute).append(")/@xpath/data()");
		}
		else{
			qry.append("(//").append(scope).append("/").append(attribute).append(")/@xpath/data()");
		}
		
		query.createQuery(qry.toString());
		String retString = (String) query.execute();
		
		if(retString != null){
			if(retString.startsWith("Policy")){
				String arr[] = retString.split("/");
				StringBuffer xpath = new StringBuffer();
				xpath.append(Constants.CURRENT_CONTEXT_OBJECT_NAME);
				xpath.append("/");
				for(int i=1;i<arr.length;i++)
				{
					xpath.append(arr[i]);
					if(i != arr.length-1)
					xpath.append("/");	
				}
				retString = xpath.toString();
			}
			retString = retString.replaceAll("/", ".");
			//THIS CONDITION IS ADDED TO HANDL MULTIPLE LEVEL ALGORITHM XPATHS
			//AS ALL THE XPATHS IN ALGORITHM WILL NOT BE CORRECT FOR ALL THE LEVELS THAT ALGORITHM IS GOING TO EXECUTE
			//AS PER MANISH ADVICE ADDED N/A IN SUCH ALGORITHMS
			if ("".equals(retString) && scope.contains("/") && isXpathEvalCall){
				retString = "\"NA\"";
			}
			return retString;
		}else{
			//System.err.println("WRONG XPATH: MODEL:["+scope+"] ATTRIBUTE:["+attribute+"]");
			return "";
		}		
	}

	public static String getScopePath(Database db,String scopeDbTable) {
		Connection conn = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		
		StringBuffer qry = new StringBuffer();
		qry.append("(//").append(scopeDbTable).append(")/@xpath/data()");
		
		query.createQuery(qry.toString());
		String retString = (String) query.execute();
		
		if(retString != null){
			String arr[] = retString.split("/");
			StringBuffer xpath = new StringBuffer();
			xpath.append(Constants.CURRENT_CONTEXT_OBJECT_NAME);
			xpath.append("/");
			for(int i=1;i<arr.length;i++)
			{
				xpath.append(arr[i]);
				if(i != arr.length-1)
				xpath.append("/");	
			}
			retString = xpath.toString();
			retString = retString.replaceAll("/", ".");
			return retString;
		}else{
			//@TODO need to throw exception if we are not able to find the path of the scope
			return "";
		}
	}
	

	
}