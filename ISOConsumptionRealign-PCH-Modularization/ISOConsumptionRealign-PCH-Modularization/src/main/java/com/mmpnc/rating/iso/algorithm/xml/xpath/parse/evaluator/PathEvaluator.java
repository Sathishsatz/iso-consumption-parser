package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class PathEvaluator implements Evaluator {
	private String seperator;
	private Evaluator left;
	private Evaluator right;
	private Context context;
	
	public PathEvaluator(String string, Evaluator e1, Evaluator e2) {
		this.seperator = string;
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
//		System.out.println("Path Evaluator called");
		
		this.left.setContext(context);
		this.left.evaluate();
		
		StringBuffer buffer = (StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		
		if(this.seperator.equals("PATH")){
			buffer.append("/");
		}else{
			buffer.append("//");
		}
		
		this.right.setContext(context);
		this.right.evaluate();
		
		return Type.PATH;
	}

}
