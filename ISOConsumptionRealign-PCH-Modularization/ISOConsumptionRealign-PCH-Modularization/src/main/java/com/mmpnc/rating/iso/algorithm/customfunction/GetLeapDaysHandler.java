package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class GetLeapDaysHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context)
			throws RecognitionException {
		
		StringBuffer getLeapDays= new StringBuffer();
		for(Evaluator eval : evallist){
			eval.setContext(context);
			 getLeapDays = getLeapDays.append(eval.evaluate().toString());
			 getLeapDays.append(",");
		}
		String getLeapDaysStr = getLeapDays.substring(0,getLeapDays.lastIndexOf(","));
		return "GetLeapDays("+ getLeapDaysStr +")";
	}

}
