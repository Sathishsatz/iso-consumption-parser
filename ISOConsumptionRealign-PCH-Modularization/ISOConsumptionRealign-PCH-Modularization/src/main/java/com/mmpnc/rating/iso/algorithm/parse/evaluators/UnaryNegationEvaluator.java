package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class UnaryNegationEvaluator implements Evaluator {

	private Context context;
	private Evaluator right;
	
	public UnaryNegationEvaluator(Evaluator e1) {
		this.right = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append("-");
		
		this.right.setContext(context);
		
		returnValue.append(this.right.evaluate()).append(" ");
		
		return returnValue;
		
	}

}
