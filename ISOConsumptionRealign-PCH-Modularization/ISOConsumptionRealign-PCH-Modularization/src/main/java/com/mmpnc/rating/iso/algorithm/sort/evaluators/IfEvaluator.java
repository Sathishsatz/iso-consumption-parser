package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class IfEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private List<Evaluator> thenlist;
	private List<Evaluator> elselist;
	private Evaluator booleanEvaluator;
	//private Logger logger = LoggerFactory.getLogger(IfEvaluator.class);
	
	public IfEvaluator(Context context, Evaluator booleanEvaluator,
			List<Evaluator> s1, List<Evaluator> s2) {
		this.context = context;
		this.booleanEvaluator = booleanEvaluator;
		this.thenlist = s1;
		this.elselist = s2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
		@SuppressWarnings("unused")
		int count = 0;
		StringBuffer ifStatement = new StringBuffer();
		StringBuffer currentAlgoSteps = (StringBuffer) context.getValue(ContextParam.ALGO);
		Integer spaceCount = (Integer) context.getValue(ContextParam.SPACECOUNT);
	
		//append spaces
		appendSpace(ifStatement, spaceCount);
		
		ifStatement.append("IF");
		
		//this is algo step stringbuffer
		StringBuffer conditionAlgo = new StringBuffer();		
		context.putValue(ContextParam.ALGO, conditionAlgo);
		
		this.booleanEvaluator.setContext(context);
		Object conditionObject = this.booleanEvaluator.evaluate();
		if(ExceptionHandler.checkNullOrEmpty(conditionObject)){
			ExceptionHandler.raiseParserException("Condition can not be NULL or EMPTY in IF evaluator");
		}
		ifStatement.append(conditionObject);
		
		ifStatement.append(" THEN\n");		
		
		/*if(logger.isInfoEnabled()){
			logger.info("we received conditionAlgo : "+ conditionAlgo);
		}*/
		
		if(thenlist != null && thenlist.size() > 0 ){
			for(Evaluator eval : thenlist){
				
				//this is algo step stringbuffer
				StringBuffer thenStatementAlgo = new StringBuffer();		
				Integer nextSpaceCount = spaceCount + 1;
				
				context.putValue(ContextParam.ALGO, thenStatementAlgo);
				context.putValue(ContextParam.SPACECOUNT, nextSpaceCount);
				
				/*if(logger.isInfoEnabled()){
					logger.info("We are executing " + count++ + " then statement" );
				}*/
				
				eval.setContext(context);
				eval.evaluate();

				if(thenStatementAlgo.length() > 0){
					ifStatement.append(thenStatementAlgo);
				}
			}
		}
		
		count = 0;
		
		if(elselist != null && elselist.size() > 0 ){
			
			//append spaces
			appendSpace(ifStatement, spaceCount);
			ifStatement.append("ELSE").append("\n");
			
			for(Evaluator eval : elselist){
				
				//this is algo step stringbuffer
				StringBuffer elseStatementAlgo = new StringBuffer();		
				Integer nextSpaceCount = spaceCount + 1;
				
				context.putValue(ContextParam.ALGO, elseStatementAlgo);
				context.putValue(ContextParam.SPACECOUNT, nextSpaceCount);
				
				/*if(logger.isInfoEnabled()){
					logger.info("We are executing " + count++ + " else statement" );
				}*/
				
				eval.setContext(context);
				eval.evaluate();
				
				if(elseStatementAlgo.length() > 0){
//					System.out.println("getting prior else " + priorToElseStatementAlgo);
					ifStatement.append(elseStatementAlgo);
				}
			}
		}
		
		//append spaces
		appendSpace(ifStatement, spaceCount);
		ifStatement.append("END IF\n");
		
		//append the prior PCHAlgo before we append the IF algo to the context
		currentAlgoSteps.append(ifStatement);
		
		//reset the context spacecount to prior
		context.putValue(ContextParam.ALGO, currentAlgoSteps);
		context.putValue(ContextParam.SPACECOUNT, spaceCount);
		
		return "";
	}
	
	
	private void appendSpace(StringBuffer buffer, int count) {
		for (int x = 0; x < count; x++) {
			buffer.append(" ");
		}
	}

	@Override
	public void validate() throws RecognitionException {
		if((this.booleanEvaluator == null) && (this.thenlist==null)){
			throw new ParserException("Left and Right evaluators are NULL in >= evaluator");
		}
		if(this.booleanEvaluator == null){
			throw new ParserException("Boolean Evaluator is NULL in IF evaluator");
		}
		if(this.thenlist == null){
			throw new ParserException("Then evaluator is NULL in IF evaluator");
		}			
	}

}
