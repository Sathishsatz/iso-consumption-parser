package com.mmpnc.rating.iso.algorithm;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
//import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import com.mmpnc.rating.iso.algorithm.exception.SyntaxRecognitionException;
import com.mmpnc.rating.iso.algorithm.parse.AlgorithmGrammarLexer;
import com.mmpnc.rating.iso.algorithm.parse.AlgorithmGrammarParser;
import com.mmpnc.rating.iso.algorithm.parse.AlgorithmGrammarParser.algorithm_return;


public class AlgorithmReaderImpl implements AlgorithmReader {
	private StringBuffer buffer;

	public AlgorithmReaderImpl(StringBuffer buffer) {
		this.buffer = buffer;
	}
	
	@Override
	public Object execute() throws SyntaxRecognitionException {
		CharStream stream = new ANTLRStringStream(buffer.toString());
		AlgorithmGrammarLexer asLexer = new AlgorithmGrammarLexer(stream);
		AlgorithmGrammarParser asParser = new AlgorithmGrammarParser(
				new CommonTokenStream(asLexer));
		algorithm_return algorithm;
		CommonTreeNodeStream commontreenode;
		try {
			algorithm = asParser.algorithm();
			commontreenode = new CommonTreeNodeStream(algorithm.getTree());
			
//			System.out.println(((CommonTree) algorithm.getTree()).toStringTree());
			
			return commontreenode;
		} catch (RecognitionException e) {
			StringBuilder sb = new StringBuilder();
			sb.append("Algo Text: ");
			sb.append("\n").append(buffer.toString());
			String errMsg = asParser.getErrorHeader(e) + " : " + asParser.getErrorMessage(e, asParser.getTokenNames());
			sb.append("Error Message: ").append(errMsg);
			throw new SyntaxRecognitionException(sb.toString());
		}

	}

}
