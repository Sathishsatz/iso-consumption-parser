package com.mmpnc.rating.iso.helper;

/**
 * @author nilkanth9581
 *
 */
public class Constants {
	
	/* Constants for if step */
	public static final String IF_EXECUTE_STEP = "EXECUTE_STEP";
	public static final String IF_STEP_TYPE = "IF";
	public static final String IF_NAME = "IF_STEP";
	public static final String IF_REMARK = "IF STEP";
	public static final String IF_EXP = "EXP";
	public static final String IF_ALIASNAMEPREFIX = "local";
	public static final String IF_THEN = "THEN";
	public static final String IF_ELSE = "ELSE";
	
	
	public static final String CURRENT_CONTEXT_OBJECT_NAME = "CCSRERequest";
	public static final String REQUEST_OBJECT_NAME = "SRERequest";
	
	//EXPRESSSION STEP CONSTANTS
	
	public static final String EXP_STEP_NAME = "EXPRESSION_STEP";
	public static final String EXECUTE_MODULE = "EXECUTE_MODULE";
	
	
	//SET_ATTIBUTE_STEP
	public static final String SETATTR_NAME= "SET-ATTRIBUTE STEP";
	public static final String SETATTR_REAMRK = "SET-ATTRIBUTE STEP";
	public static final String SETATTR_STEP_TYPE = "SET_ATTRIBUTE";
	public static final String SETATTR_LOCAL_INDICATOR = "L";
	public static final String SET_ATTR_BASE_MODEL = CURRENT_CONTEXT_OBJECT_NAME;
	public static final String SET_ATTR_SOURCE_OBJECT = CURRENT_CONTEXT_OBJECT_NAME;
	
	//FOR LOOP STEP
	public static final String FOR_NAME = "FOR_STEP";
	public static final String FOR_REMARK = "sample remark";
	public static final String FOR_STEPTYPE="FOR";
	public static final String FOR_BASE_MODEL="SRERequest";
	public static final String FOR_CONSTRAINT_VALUE_TYPE="TYPE_LITERAL";
	public static final String FOR_OPERATOR = "==";
	public static final String FOR_LOCAL_INDICATOR = "L";
	public static final String FOR_VALUE_TYPE_INDICATOR_M ="M";     //It will always be M
	public static final String FOR_VALUE_TYPE_INDICATOR_S ="S";
	public static final String FOR_SOURCE_OBJECT = CURRENT_CONTEXT_OBJECT_NAME;
	public static final String FOR_FIELD_TYPE = "java.lang.String";
	
	public static final String MEMORY_LOOKKUP_NAME = "MEMORY_LOOKUP_STEP";
	public static final String MEMORY_LOOKUP_REMARK = "Memory Lookup Step";
	public static final String MEMORY_LOOKUP_STEPTYPE = "M_LOOKUP";
	public static final String MEMORY_LOOKUP_CONSTRAINT_VALUE_TYPE_RUNTEIM_VAR= "TYPE_RUNTIME_VARIABLE";
	public static final String MEMORY_LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR = "L";
	public static final String MEMORY_LOOKUP_VALUE_TYPE_INDICATOR_A = "A";
	public static final String MEMORY_LOOKUP_VALUE_TYPE_INDICATOR_S = "S";
	public static final String MEMORY_LOOKUP_VALUE_TYPE_INDICATOR_M = "M";
	public static final String MEMORY_LOOKUP_LOOKUP_TYPE = "M";
	public static final String MEMORY_LOOKUP_TYPE_INDICATOR = "G";
	public static final String MEMORY_LOOKUP_TYPE_LOCAL_INDICATOR = "L";
	public static final String MEMORY_LOOKUP_ADD_EFFECTIVE_FILTER = "Y";
	public static final String MEMORY_LOOKUP_MULTIPLE_ROW_CHECK = "N";
	public static final String MEMORY_LOOKUP_LOCAL_KEYWORD = "LOCAL";
	public static final String MEMORY_LOOKUP_BASE_MODEL_NAME = "CCSRERequest";
	
	
	//RATE TABEL LOOKUP LOOKUP STEP CONSTANTS
	public static final String LOOKUP_NAME = "LOOKUP_STEP";
	public static final String LOOKUP_REMARK = "Lookup Step";
	public static final String LOOKUP_STEPTYPE = "DB_LOOKUP";
	public static final String LOOKUP_OPERATOR = "==";
	public static final String LOOKUP_CONSTRAINT_VALUE_TYPE_RUNTEIM_VAR= "TYPE_RUNTIME_VARIABLE";
	public static final String LOOKUP_RATETABLE_KEYS = "RATETABLEKEYS";
	public static final String LOOKUP_RATETABLE_VALUE = "RATETABLEVALUE";
	public static final String LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR = "L";
	public static final String LOOKUP_VALUE_TYPE_INDICATOR_A = "A";
	public static final String LOOKUP_VALUE_TYPE_INDICATOR_S = "S";
	public static final String LOOKUP_VALUE_TYPE_INDICATOR_M = "M";
	public static final String LOOKUP_LOOKUP_TYPE = "DB";
	public static final String LOOKUP_TYPE_INDICATOR = "G";
	public static final String LOOKUP_TYPE_LOCAL_INDICATOR = "L";
	
	public static final String LOOKUP_ADD_EFFECTIVE_FILTER = "Y";
	public static final String LOOKUP_MULTIPLE_ROW_CHECK = "N";
	public static final String LOOKUP_LOCAL_KEYWORD = "LOCAL";
	public static final String LOOKUP_CONSTRAINT_VALUE_TYPE_LITERAL = "TYPE_LITERAL"; //FOR STATIC VALUE
	public static final String LOOKUP_CONSTRATINT_VALUE_TYPE_UNDEFINED = "TYPE_UNDEFINED"; // FOR MODEL ATTRIBUTE
	
	//DOMAIN TABLE LOOKUP STEP CONSTATNS
	public static final String DOMAIN_LOOKUP_NAME = "DOMAIN_LOOKUP_STEP";
	public static final String DOMAIN_LOOKUP_REMARK = "Domain Table Lookup Step";
	public static final String DOMAIN_TABLE_DEFAULT_LOOKUP_RETURN = "DataValue";
	
	
	//Special Function constants
	
	public static final String GET_INTEGER = "GetInteger";
	public static final String GET_DEDUCTIBLE = "GetDeductible";
	
	public static final String ROUND_UP_DOLLAR = "RoundDollar";
	public static final String ROUND_UP_HUNDREADTH = "RoundHundredth";
	public static final String ROUND_UP_HUNDRED_THOUSANDTH = "RoundHundredThousandth";
	public static final String ROUND_UP_THOUSANDS = "RoundThousandth";
	public static final String ROUND_TEN_THOUSANDTH = "RoundTenThousandth";
	public static final String CEILING = "Ceiling";
	
	
	
	
	public static final String DOMAIN_TABLE_LOOKUP = "domainTableLookup";
	public static final String RATE_TABLE = "RateTable";
	public static final String ADD_YEARS = "AddYears";
	
	
	public static final String REFERENCE_TYPE_COMMON_RATING = "Common Rating";
	public static final String PRFERENCE_TYPE_PREMIUM_CALCULATION = "Premium Calculation";
	
	//COSNTANTS FOR PROD AND PREMCOMPLTDOPERATIONS
	public static final String 	GL_PREMOPS_COVERAGE = "GeneralLiabilityClassificationPremOpsCoverage";
	public static final String GL_PRODOPS_COVERAGE = "GeneralLiabilityClassificationProdsCompldOpsCoverage";
	
	
	public static final String LOCAL_KEYWORD = "LOCAL";
	
	//ADDED FOR MULTISTATE REQUEST AND PRODUCT ID LEVEL
	public static final String MULTISTATE_KEY = "isMultiState";
	public static final String PRODUCTID_FILTER_KEY = "productIdLevel";
	
	public static final String PCH_REPORT_PATH_KEY = "ProcessedPchReportPath";
	
}
