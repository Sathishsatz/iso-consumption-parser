package com.mmpnc.rating.iso.datastructure;

import java.io.Reader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mmpnc.rating.iso.content.vo.AscendantOne;
import com.mmpnc.rating.iso.content.vo.Column;
import com.mmpnc.rating.iso.content.vo.Table;
import com.mmpnc.rating.iso.content.vo.Tables;
import com.mmpnc.rating.iso.helper.Constants;

public class DataStructure {
	private Reader reader;

	public DataStructure(Reader reader) {
		this.reader = reader;
	}

	public StringBuffer simplifyDataStrucure() {
		StringBuffer content = new StringBuffer();
		try {
			JAXBContext jbcontext = JAXBContext
					.newInstance("com.mmpnc.rating.iso.content.vo");
			Unmarshaller um = jbcontext.createUnmarshaller();
			AscendantOne asc = (AscendantOne) um.unmarshal(reader);

			Tables tables = asc.getTables();
			
			content.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
			content.append("<CCSRERequest xpath='CCSRERequest'>");
			content.append(createWrapperNodeContent());
			readTable(content, tables.getTable(), new StringBuffer(
					Constants.CURRENT_CONTEXT_OBJECT_NAME), new StringBuffer(Constants.CURRENT_CONTEXT_OBJECT_NAME));
			content.append("</CCSRERequest>");

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return content;
	}

	private static void readTable(StringBuffer content, Table table,
			StringBuffer priorXPath, StringBuffer priorQualPath) {

		StringBuffer xpath = getPath(table.getName(), priorXPath, "/");
		// StringBuffer qualpath = getPath(table.getName(),priorQualPath,".");

		StringBuffer qualpath = new StringBuffer();

		content.append("<").append(table.getName());
		content.append(" ").append("xpath='").append(xpath).append("'");
		// content.append(" ").append("qualPath='").append(qualpath).append("'");
		content.append(" ").append("type='").append(table.getType())
				.append("'");
		content.append(">\n");

		// read the columns here
		for (Column column : table.getColumn()) {
			readColumn(content, column, xpath, qualpath);
		}

		// read the other content here
		for (Tables tables : table.getTables()) {
			readTable(content, tables.getTable(), xpath, qualpath);
		}

		content.append("</").append(table.getName()).append(">\n");
	}

	private static void readColumn(StringBuffer content, Column column,
			StringBuffer xpath, StringBuffer qualpath) {
		content.append("<").append(column.getName());
		content.append(" ").append("xpath='").append(xpath).append("/")
				.append(column.getName()).append("'");
		// content.append(" ").append("qualPath='").append(qualpath).append(".").append(column.getName()).append("'");
		content.append("/>\n");
	}

	private static StringBuffer getPath(String nodeName,
			StringBuffer priorPath, String joiner) {
		// System.out.println("joiner");
		StringBuffer buffer = new StringBuffer();
		if (priorPath == null) {
			buffer.append(nodeName);
		} else {
			buffer.append(priorPath).append(joiner).append(nodeName);
		}
		return buffer;
	}

	private static StringBuffer createWrapperNodeContent() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("	<EffDate xpath='CCSRERequest/EffDate'/>");
		buffer.append("	<ExpDate xpath='CCSRERequest/ExpDate'/>");
		buffer.append("	<Premium xpath='CCSRERequest/Premium'/>");
		buffer.append("	<State xpath='CCSRERequest/State'>");
		buffer.append("		<Code xpath='CCSRERequest/State/Code'/>");
		buffer.append("	</State>");
		//THIS ANN DATE ATTRIBUTE IS ADDED FOR WORKER COMPENSATION ALGORITHMS
		buffer.append(" <AnnDate xpath='CCSRERequest/AnnDate'/>");
		return buffer;
	}

}
