package com.mmpnc.rating.iso.algorithm.exception;

public interface ExceptionMessages {
	
	String RATETABLE_LOOKUP_STEP = "Exception while creating rate table lookup step";
	String DOMAINTABLE_LOOKUP_STEP = "Exception while creating domain table lookup step";
}
