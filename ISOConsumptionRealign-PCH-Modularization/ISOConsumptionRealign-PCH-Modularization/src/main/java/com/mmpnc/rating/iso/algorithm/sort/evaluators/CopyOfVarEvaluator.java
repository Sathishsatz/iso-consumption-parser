package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import java.util.Map;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.util.AlgoUtil;
import com.mmpnc.util.VariableType;

public class CopyOfVarEvaluator implements Evaluator, IEvaluatorValidator {
	private String variable;
	private String variableType;
	private Context context;
	
	/*private static final Logger logger = LoggerFactory
			.getLogger(VarEvaluator.class);*/
	
	public CopyOfVarEvaluator(String variableType, String string) {
		this.variableType = variableType;
		this.variable = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void validate() throws RecognitionException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public StringBuffer evaluate()throws RecognitionException {

		validate();
		StringBuffer returnValue = new StringBuffer();
		
		/*if(logger.isInfoEnabled()){
			logger.info("check if this variable has a process block [" + this.variable + " ]");
		}*/
		
		String currentProcess = (String) this.context.getValue(ContextParam.CURRENTPROCESS);

		
		//below code added to check the PCH block for local variables only
		if(this.variableType != null){
			
			returnValue.append(" ").append(this.variableType).append(":").append(this.variable).append(" ");
			
			VariableType varType = VariableType.valueOf(variableType);
			if ( varType.equals(VariableType.LV_VALUE)|| varType.equals(VariableType.LV_INTEGER) || varType.equals(VariableType.LV_NUMERIC) || varType.equals(VariableType.LV_DOUBLE) || varType.equals(VariableType.LV_STRING) || varType.equals(VariableType.LV_BOOLEAN) || varType.equals(VariableType.LV_DATE) || varType.equals(VariableType.LV_TIMESPAN) ){
				
				if( !this.variable.equals("null") && !(currentProcess != null && currentProcess.equals(this.variable))){
					
					findAndAddPCHBolckForTheVariable(currentProcess);
				}
			}			

		} else{
			//This handling is added for ARDSpan.Days like statement where ARDSpan is the PCH 
			if(variable.endsWith(".Days")){
				variable = variable.replace(".Days", "");
				findAndAddPCHBolckForTheVariable(currentProcess);
			}	
			
			returnValue.append(" ").append(this.variable).append(" ");
		}
		
		return returnValue;
	}

	
	private void findAndAddPCHBolckForTheVariable(String currentProcess)throws RecognitionException{
		@SuppressWarnings("unchecked")
		Map<String,String> processedBlock = (Map<String, String>) context.getValue(ContextParam.PROCESSEDBLOCK);
		
		StringBuffer programPath = new StringBuffer(); 
		programPath.append(context.getValue(ContextParam.SCOPE)).append("/");
		programPath.append(context.getValue(ContextParam.PARENTPROCESS)).append("/");
		//we have commented this --- please uncomment if the program execution order is incorrect
		// TODO
		programPath.append(context.getValue(ContextParam.CURRENTPROCESS)).append("/");
		programPath.append(this.variable);
		
		
		if(processedBlock.containsKey(programPath.toString())){
			return;
		} else {
			//logger.info("Current process we are processing is " + currentProcess + " and will check the program block for " + this.variable);
			processedBlock.put(programPath.toString(), programPath.toString());
			StringBuffer priorPCHAlgo = (StringBuffer) this.context.getValue(ContextParam.PCHALGO);
			StringBuffer priorAlgo = (StringBuffer) this.context.getValue(ContextParam.ALGO);
			Boolean isLoopStatementExecution = (Boolean) this.context.getValue(ContextParam.LOOPSTATEMENTEXECUTION);
			Integer spaceCount = (Integer) this.context.getValue(ContextParam.SPACECOUNT); 
			
			//set the new algo and pchalgo for this variable call
			this.context.putValue(ContextParam.PCHALGO, new StringBuffer());
			this.context.putValue(ContextParam.ALGO, new StringBuffer());
			
			if(isLoopStatementExecution != null && !isLoopStatementExecution){
				/*if(logger.isInfoEnabled()){
					logger.info("We defaulted the spacecount to 0");
				}*/
				this.context.putValue(ContextParam.SPACECOUNT, new Integer(0));
			}
			
			AlgoUtil.searchAndProcess(this.variableType, this.variable, context);
			
			//after execution of variable pch block get the algo statements and append them to pchalgo block
			priorPCHAlgo.append(context.getValue(ContextParam.ALGO));
			
			//set the algo and pchAlgo back to context
			this.context.putValue(ContextParam.ALGO, priorAlgo);
			this.context.putValue(ContextParam.PCHALGO, priorPCHAlgo);
			this.context.putValue(ContextParam.CURRENTPROCESS, currentProcess);
			this.context.putValue(ContextParam.SPACECOUNT, spaceCount);
		}					
		
		
	}

}
