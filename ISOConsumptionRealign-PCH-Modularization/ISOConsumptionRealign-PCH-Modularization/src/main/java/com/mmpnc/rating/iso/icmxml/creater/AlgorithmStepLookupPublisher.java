package com.mmpnc.rating.iso.icmxml.creater;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.BaseICMException;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.CompositeFieldConstraint;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.IConstraint;
import com.mmpnc.icm.common.ide.models.IPattern;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.icm.common.ide.models.RatingAlgorithm;
import com.mmpnc.icm.common.ide.models.RatingTemplate;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraint;
import com.mmpnc.rating.connection.db.impl.OracleDbConnection;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.config.BasicConfiguration;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.publish.lookup.LookupPersistHelper;
import com.mmpnc.rating.iso.publish.ratingalgo.AlgorithmPublisheHelper;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;
import com.thoughtworks.xstream.XStream;

/**
 * @author nilkanth9581
 *
 */
public class AlgorithmStepLookupPublisher implements IAlgorithmStepLookupPublisher{
	
	private Map<String,String> algorithmMap = new HashMap<String,String>();
	//private static int counter = 10;
	private static AlgorithmStepLookupPublisher algorithmStepLookupPublisher = null;
	private AlgorithmStepLookupPublisher(){
		
	}
	
	public static AlgorithmStepLookupPublisher getInstance(){
		if(algorithmStepLookupPublisher == null){
			algorithmStepLookupPublisher = new AlgorithmStepLookupPublisher();
		}
		return algorithmStepLookupPublisher;
	}
	
	
	public Map<String,String> getAlgorithmNameAndID(){
		return algorithmMap;
	}
	
	
	
	@Override
	public void publishAlgorithmSteps(IcmRatingNodeContent icmRatingNodeContent,String algorithmLevle) throws BaseICMException{
		BasicConfiguration baseConfig = Configurer.getInstance().getBaseConfig();
		
		if(!"Y".equals(baseConfig.getPublishAlgorithmSteps())){
			return;
		}
		//publishing algorithm steps into database
		AlgorithmPublisheHelper algorithmPublisheHelper = new AlgorithmPublisheHelper();
		
		RatingAlgorithm ratingAlgorithm = new RatingAlgorithm();
		ratingAlgorithm.setName(icmRatingNodeContent.getAlgorithmLevel());
		ratingAlgorithm.setAgendaName(icmRatingNodeContent.getAlgorithmLevel());
		
		Map<String,List<AlgoStep>> stepMap = icmRatingNodeContent.getPchNameAndAlgoSteps();
		List<AlgoStep> algoSteps = new ArrayList<AlgoStep>();
		algoSteps.addAll(icmRatingNodeContent.getAlgoStepList());
		
		for(String pchName:stepMap.keySet()){
			algoSteps.addAll(stepMap.get(pchName));
		}
		
		
		
		ratingAlgorithm.setSteps(algoSteps);
		
		OracleDbConnection oracleDbConnection = OracleDbConnection.getInstance();
		Connection connection = oracleDbConnection.getConnection();
			
		
		Map<String,RatingAlgorithm> finalRatingDataM = new HashMap<String, RatingAlgorithm>();
		//creating algorithm id depending on the input provided by user 
		AlgorithmIdCreater algorithmIdCreater = AlgorithmIdCreater.getInstance();
		String algorithmId = algorithmIdCreater.createAlgorithmId(icmRatingNodeContent);
		
		ISOConsumptionLogger.info("Publishing steps for algorithm ID:"+algorithmId);
		algorithmMap.put(algorithmId, algorithmLevle);
		
		finalRatingDataM.put(algorithmId, ratingAlgorithm);
		List<Map<String,RatingAlgorithm>> finalRatingData = new ArrayList<Map<String,RatingAlgorithm>>();
		finalRatingData.add(finalRatingDataM);
		List <String> ratingStage = new ArrayList<String>();  
		ratingStage.add(algorithmLevle);
		algorithmPublisheHelper.publishAlgorithm(connection, "nilkanth", finalRatingData, ratingStage,"BACKEND-PUBSLISHED-DATA");
		oracleDbConnection.commitAndClose();
		
		
	}
	
	public String createAlgorithmId(IcmRatingNodeContent ratingNodeContent){
		
		String[] ratingNodeNames = ratingNodeContent.getAlgorithmLevel().split(" ");
		 String templateName = null;
        
		 for (String ratingNodeName : ratingNodeNames) {
          if(ratingNodeName==""){
            continue;
          }
          
          templateName  = ratingNodeName;
          String resolvedModelPath = ratingNodeName;
          if (ratingNodeName.contains(".")) {
            templateName = ratingNodeName
                .substring(ratingNodeName.lastIndexOf(".") + 1);
            templateName = templateName + StringUtils.countMatches(resolvedModelPath, ".");
          }
         
        }
        
        return templateName;
        }
	
	/**
	 * @param icmRatingNodeContent
	 */
	public void printAlgoSteps(IcmRatingNodeContent icmRatingNodeContent){
		if(Configurer.getInstance().getBaseConfig().getIcmInputXmlFileLocation() == null){
			return;
		}
		
		XStream xStream = new XStream();
	
		RatingTemplate ratingTemplate = new RatingTemplate();
		
		RatingAlgorithm ratingAlgorithm = new RatingAlgorithm();
		ratingAlgorithm.setName(icmRatingNodeContent.getAlgorithmLevel());
		ratingAlgorithm.setSteps(icmRatingNodeContent.getAlgoStepList());
		ratingTemplate.setRatingAlgorithm(ratingAlgorithm);
		
		try {
			OutputStream outputStream = new FileOutputStream(new File(Configurer.getInstance().getBaseConfig().getIcmInputXmlFileLocation()));
			xStream.toXML(ratingTemplate, outputStream);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public void publishLookupModelsNew(LookupModels lookupModels){
		BasicConfiguration baseConfig = Configurer.getInstance().getBaseConfig();
		if(!"Y".equals(baseConfig.getPublishLookupModels())){
			return; 
		}
		
		OracleDbConnection oracleDbConnection = OracleDbConnection.getInstance();
		Connection connection = oracleDbConnection.getConnection();
		LookupPersistHelper lookupPersistHelper = new LookupPersistHelper(connection, "nilkanth");
		StringBuffer sb = new StringBuffer();
		for(LookupModel lookupModel : lookupModels.getLookupModels()){
			try{
			lookupPersistHelper.persist_flow(lookupModel, connection);
			sb.append(lookupModel.getLookupName()+" ");
			}catch(Exception e){
				ISOConsumptionLogger.info("ERROR:EXCEPTION WHILE SAVING GLOBAL LOOKUP WITH NAME"+lookupModel.getLookupName());
				e.printStackTrace();
			}
		}
		
		oracleDbConnection.commitAndClose();
	}
	
	/**
	 * @param lookupModels
	 */
	public void printLookupModels(LookupModels lookupModels){
		if(BasicConfiguration.getInstance().getGlobalLookupFileLocation() == null)
			return;
		XStream xStream = new XStream();
		
		try {
			OutputStream outputStream = new FileOutputStream(new File(BasicConfiguration.getInstance().getGlobalLookupFileLocation()));
			xStream.toXML(lookupModels, outputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	
	/**
	 * @param lookupModels
	 */
	@SuppressWarnings("unchecked")
	public void publishLookupModels(LookupModels lookupModels){
		OracleDbConnection oracleDbConnection = OracleDbConnection.getInstance();
		Connection connection = oracleDbConnection.getConnection();
		LookupPersistHelper lookupPersistHelper = new LookupPersistHelper(connection, "nilkanth");
		int criteria_id =1000;
		int lookup_filter_id = 2000;
		int look_crtr_ret_attr = 2000;
		for(LookupModel lookupModel : lookupModels.getLookupModels()){
			try {
				lookupModel.setLookuoID(Integer.toString(criteria_id));
				IPattern[] ft =  lookupModel.lookupCriteriaModel.lhs;
				if (ft != null && ft.length > 0) {
					for (IPattern iPattern : ft) {
						FactPattern factPattern = (FactPattern) iPattern;
						CompositeFieldConstraint compositeFieldConstraint = factPattern.constraintList;
				
						if (compositeFieldConstraint != null) {

							IConstraint singleFieldConstraints[] = (IConstraint[]) compositeFieldConstraint.constraints;
							for (IConstraint iConstraint : singleFieldConstraints) {
									if (iConstraint instanceof SingleFieldConstraint) {
										SingleFieldConstraint constraint = (SingleFieldConstraint)iConstraint;
										constraint.setId(Integer.toString(lookup_filter_id));
										lookup_filter_id = lookup_filter_id+1;
									}
							}
						}
					}
				}
				List<Attribute> list = lookupModel.getAssignedAttributes();
				for (Attribute attr : list) {
					attr.setId(Integer.toString(look_crtr_ret_attr));
					look_crtr_ret_attr = look_crtr_ret_attr+1;
					
				}
				criteria_id = criteria_id+1;
				lookupPersistHelper.persist_flow(lookupModel, connection);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		oracleDbConnection.commitAndClose();
	
	}

	
	
}
