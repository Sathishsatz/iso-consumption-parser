package com.mmpnc.rating.connection.db.helper;

/**
 * @author nilkanth9581
 *
 */
public interface OracleDB {
	OracleDB openDatabase();
	void closeDatabase();
}
