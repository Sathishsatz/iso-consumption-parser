package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class StepEvaluator implements Evaluator {
	private Context context;
	private Evaluator nodeTest;
	private List<Evaluator> predicates;
	private String axisName;

	public StepEvaluator(String string, Evaluator n1,
			List<Evaluator> predicateEval) {
		this.axisName = string;
		this.nodeTest = n1;
		this.predicates = predicateEval;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
//		System.out.println("Step Evaluator Called");
		StringBuffer buffer = (StringBuffer) this.context
				.getValue(ContextParam.XPATHSTRING);
		
		
		if (this.axisName != null && ! this.axisName.equals("AT")) {
			buffer.append(this.axisName).append("::");
		} else if(this.axisName != null && this.axisName.equals("AT")){
			buffer.append("@");
		}

		this.nodeTest.setContext(context);
		Type type = (Type) this.nodeTest.evaluate();
		
		for(Evaluator eval : this.predicates){
			eval.setContext(context);
			eval.evaluate();
			ISOConsumptionLogger.info(context.getValue(ContextParam.XPATHSTRING).toString());
		}		
		if (this.axisName != null && this.predicates == null ) {
			return Type.PATH;
		} else {
			return type;
		}
	}

}
