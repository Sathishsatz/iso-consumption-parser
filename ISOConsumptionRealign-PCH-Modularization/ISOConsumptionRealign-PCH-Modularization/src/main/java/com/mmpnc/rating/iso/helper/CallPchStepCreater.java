package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ExecuteModuleStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;

/**
 * @author nilkanth9581
 *	This is Call_Pch Step create
 *
 */
public class CallPchStepCreater extends BaseStepCreater{
	
	private static CallPchStepCreater pchStepCreater = new CallPchStepCreater();;
	
	private CallPchStepCreater(){}
	
	public static CallPchStepCreater getInstance(){
		
		return pchStepCreater;
	}
	
	
	public String createCallPchStep(List<Evaluator> evallist,
			String function, String constant, Context context)throws RecognitionException{
		
		//Create call pch step and return the name of the pch from here
		//System.out.println(" In CallFunction handler ");
		
		Object pchName = null;
		for(Evaluator evaluator: evallist){
			evaluator.setContext(context);
			pchName = evaluator.evaluate();
		}
		
		ExecuteModelStepCreater executeModelStepCreater = ExecuteModelStepCreater.getInstance();
		ExecuteModuleStep moduleStep = executeModelStepCreater.createExecuteModelStep("", pchName.toString(), 0,context);
		//ExecuteModuleStep moduleStep = executeModelStepCreater.createExecuteModelStep(pchName.toString(), context);
		//ADDING MODULE STEP TO THE MAIN ALGO STEP LIST
		@SuppressWarnings("unchecked")
		List<AlgoStep> algosteps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		
		//ADDED TO AVOID CREATING MULIPLE EXECUTE_MODULE STEP IN SINGLE PCH CALL
		
		Object isLoopthroughLocVarObj = context.getValue(ContextParam.ISLOOPTHROUGHSTATEMENTS);
		boolean isLoopThorughLocVar = false;
		if(isLoopthroughLocVarObj != null ){
			isLoopThorughLocVar = (Boolean) isLoopthroughLocVarObj;
		}else{
			isLoopThorughLocVar = false;
		}
		
		@SuppressWarnings("unchecked")
		ArrayList<String> pchNameList = (ArrayList<String>)context.getValue(ContextParam.PCHREFFROMCURRENTPROCESS);
		String currentProcess = (String)context.getValue(ContextParam.CURRENTPROCESS);
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		String listEntryName  = null;
		/*if(parentCondition != null)
			listEntryName = currentProcess+"-"+pchName+"-"+parentCondition;
		else*/
		listEntryName = currentProcess+"-"+pchName;
		
		//IF EXECUTE MODULE STEP IS A CHILD LOOP THROUGH STATEMENT THEN INCLUDE IT
		//ELSE CHECK IF THE CURRENT PCH HAS ALREADY HAS EXECUTE MODULE STEP FOR THAT VARIBALE IF SO THEN DO NOT ADD
		//SAME STEP IN SAME PCH MULTIPLE TIMES
		if(isLoopThorughLocVar || parentCondition != null){
			algosteps.add(moduleStep);
		}else if(!pchNameList.contains(listEntryName)){
			algosteps.add(moduleStep);
			//ADDING PCH NAME TO LIST AS WE HAVE CREATED EXECUTEMODULE STEP FOR THIS LOCAL VARIABLE
			pchNameList.add(listEntryName);
		}
		
		return pchName.toString();
	}
}
