package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
//import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
//import com.mmpnc.rating.iso.algorithm.util.AlgoUtil;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class XpathEvaluator implements Evaluator, IEvaluatorValidator {
	private String xpath;
	@SuppressWarnings("unused")
	private Context context;
	
	public XpathEvaluator(String string) {
		this.xpath = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public StringBuffer evaluate()throws RecognitionException {
//		System.out.println("check if this variable has a process block [" + this.xpath + " ]");
		
//		String currentProcess = (String) this.context.getValue(ContextParam.CURRENTPROCESS);

//		System.out.println("Current process we are processing " + currentProcess);
		
//		String correctedXpathEntry = this.xpath.replaceAll("^\"|\"$", "").replaceAll("(\\[).[^name][^\\]]*(\\])", "");
		
		
		/*
		 * Below code commented to stop xpath string to search for the PCH blocks
		 */
		
//		if( !correctedXpathEntry.equals("") && !(currentProcess != null && currentProcess.equals(correctedXpathEntry))){
//			AlgoUtil.searchAndProcess("XPATH", correctedXpathEntry, context);
//			this.context.putValue(ContextParam.CURRENTPROCESS, currentProcess);
//		}
		
		
		validate();
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append(" ").append("XPATH:").append(this.xpath).append(" ");
		
		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.xpath==null || this.xpath.equals("")){
			throw new ParserException("XPath is NULL or EMPTY in XPath evaluator");
		}
		
	}

}
