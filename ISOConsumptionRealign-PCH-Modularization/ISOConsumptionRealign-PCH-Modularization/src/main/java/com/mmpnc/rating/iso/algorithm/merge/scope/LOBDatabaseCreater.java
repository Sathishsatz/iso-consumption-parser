package com.mmpnc.rating.iso.algorithm.merge.scope;

import java.util.Arrays;
import java.util.List;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.rating.iso.algorithm.correction.BasexDatabaseCreater;
import com.mmpnc.rating.iso.algorithm.vo.LOB;

public abstract class LOBDatabaseCreater extends ScopeMergeOpSequence{

	@Override
	Database createLOBDatabase(LOB lob) {
		return BasexDatabaseCreater.createLOBDtabase(lob);
	}

	/*
	 * THIS METHOD WILL CREATE A BASEX QUERY TO GET THE REFERENCE WHICH CONTAINS 
	 * SCOPES WITH PASS GREATER THAN ONE 
	 */
	@Override
	List<String> getReferenceNamesWithPass2Scope(Database database) {
		Connection conn = DBFactoryBuilder.getXmlConnection(database);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		
		StringBuffer qry = new StringBuffer();
		qry.append("//reference[scope[@pass>1] and (@type='Common Rating' or @type='Premium Calculation')]/@dbTables/data()");
		
		query.createQuery(qry.toString());
		String retString = (String) query.execute();
		
		if(retString != null){
			String arr[] = retString.split(" ");
			return Arrays.asList(arr);
		}
		return null;
	}

	
	
}
