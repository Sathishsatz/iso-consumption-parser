package com.mmpnc.rating.iso.wrapper;


/**
 * @author nilkanth9581
 *
 */
public interface ISOConsuptionWrapperInterface {
	/**
	 * @param propertiesFileLocation
	 */
	public void readSortedAlgoAndCreateICMInputXml(String propertiesFileLocation);
	/**
	 * @param config
	 */
	//public void  readSortedAlgoAndCreateICMInputXml(String config);
}
