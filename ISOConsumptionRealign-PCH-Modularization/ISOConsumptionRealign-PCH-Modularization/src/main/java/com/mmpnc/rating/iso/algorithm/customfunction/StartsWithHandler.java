package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class StartsWithHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context)
			throws RecognitionException {
		
		StringBuffer startsWith= new StringBuffer();
		for(Evaluator eval : evallist){
			eval.setContext(context);
			 startsWith = startsWith.append(eval.evaluate().toString());
			 startsWith.append(",");
		}
		String getLeapDaysStr = startsWith.substring(0,startsWith.lastIndexOf(","));
		return " startsWith ("+ getLeapDaysStr +")";
	}

}