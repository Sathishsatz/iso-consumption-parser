package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class ProgramEvaluator implements Evaluator {

	private String pchName;
	@SuppressWarnings("unused")
	private Context context;
	
	public ProgramEvaluator(Context context, String pchName) {
		this.context = context;
		this.pchName = pchName;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() {
		//Call to the method which will create a 
		//EXECUTE-PCH statement
		
		ISOConsumptionLogger.info("Call PCH:"+pchName);
		return pchName;
	}

}
