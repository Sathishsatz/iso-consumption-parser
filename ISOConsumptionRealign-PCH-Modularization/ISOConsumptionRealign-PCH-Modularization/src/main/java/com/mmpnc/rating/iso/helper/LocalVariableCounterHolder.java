package com.mmpnc.rating.iso.helper;

import java.util.HashSet;
import java.util.Set;

/**
 * @author nilkanth9581
 *
 */
public class LocalVariableCounterHolder {
	
	private int counter ;
	private Set<Integer> variableCounterSet = new HashSet<Integer>();
	
	public LocalVariableCounterHolder(){
		counter = 0;
	}
	
	public int getNextLocVarCounter(){
		
		counter = counter + 1 ;
		
		while(variableCounterSet.contains(counter)){
			counter = counter + 1;
		}
		variableCounterSet.add(counter);
		return counter;
	}

}
