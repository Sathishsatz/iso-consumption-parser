package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class OrEvaluator implements Evaluator {

	private Evaluator left ;
	private Evaluator right;
	private Context context; 
	
	public OrEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		StringBuffer orEvaluator = new StringBuffer(); 
		left.setContext(context);
		orEvaluator.append(" ( ");
		orEvaluator.append(left.evaluate());
		orEvaluator.append(" || ");
		right.setContext(context);
		orEvaluator.append(right.evaluate());
		orEvaluator.append(" ) "); 
		return orEvaluator;
	}

}
