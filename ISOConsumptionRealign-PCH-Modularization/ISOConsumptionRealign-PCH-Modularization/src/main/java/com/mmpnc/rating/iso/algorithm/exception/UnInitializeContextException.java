package com.mmpnc.rating.iso.algorithm.exception;

public class UnInitializeContextException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UnInitializeContextException(String string) {
		super(string);
	}
}
