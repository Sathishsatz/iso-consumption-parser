package com.mmpnc.rating.iso.algorithm.merge.scope;

import com.mmpnc.rating.iso.algorithm.exception.ScopeMergingException;
import com.mmpnc.rating.iso.algorithm.vo.LOB;

public class ImplMergeScopeWithMultiplePass implements IMergeScopeWithMultiplePass{

	@Override
	public void mergeScopesWithMultiplePass(LOB lob)throws ScopeMergingException {
		
		LOBObjectRearranger lobObjectRearranger = new LOBObjectRearranger();
		lobObjectRearranger.execute(lob);
	}
	
}
