package com.mmpnc.rating.iso.algorithm.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;

/**
 * @author nilkanth9581
 * 
 */
public class PredicatesUtil {

	/**
	 * @param context
	 * @param buffer
	 * This method will add predicate condition to existing condition
	 * 
	 */
	public static void addIfPredicateConition(Context context,
			StringBuffer buffer) {
		if (context.getValue(ContextParam.IS_IFCONDITIONPREDICATE) != null
				&& (Boolean) context
						.getValue(ContextParam.IS_IFCONDITIONPREDICATE)
				&& context.getValue(ContextParam.IFCONDITIONPREDICATEXPATH) != null) {
			
			String predicateCondition = context.getValue(
					ContextParam.IFCONDITIONPREDICATEXPATH).toString();
			buffer.append(" && ");
			boolean multipleCondition = false;

			if (predicateCondition.contains(" or ")
					|| predicateCondition.contains(" and ")
					|| predicateCondition.contains(" || ")
					|| predicateCondition.contains(" && ")) {
				multipleCondition = true;
				buffer.append(" ( ");
			}
			buffer.append(context
					.getValue(ContextParam.IFCONDITIONPREDICATEXPATH));

			if (multipleCondition) {
				buffer.append(" ) ");
			}
			// once added if condition predicate we need to set it to false so
			// it will not be added multiple times in a if condition
			context.putValue(ContextParam.IS_IFCONDITIONPREDICATE, false);
		}
	}

	/**
	 * @param inputText
	 * @return This method will remove white spaces out of single quote
	 * 
	 */
	public static String removeWhiteSpacesOutsideQuotes(String inputText) {
		Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
		Matcher regexMatcher = regex.matcher(inputText);
		StringBuffer newStr = new StringBuffer();
		while (regexMatcher.find()) {
			newStr.append(regexMatcher.group());
		}
		return newStr.toString();
	}

	public static String getFilterLHS(String[] predicates, String predicateLevel) {
		String filterLHS = null;
		if (".".equals(predicates[0].trim())) {
			filterLHS = predicateLevel;
		} else {
			filterLHS = predicateLevel + "/" + predicates[0].trim();
		}
		return filterLHS;
	}

	public static String getFilterRHS(String[] predicates, String predicateLevel) {
		String filterRHS = null;
		if (".".equals(predicates[0].trim())) {
			filterRHS = predicateLevel + "/" + predicates[1].trim();
		} else {
			filterRHS = predicateLevel + "/" + predicates[1].trim();
		}
		return filterRHS;
	}

	/**
	 * @param predicatesStr
	 * @return
	 */
	public static String[] createAndReturnPredicateArray(String predicatesStr) {
		String[] predicatesArr = null;
		if (predicatesStr.contains(" and ")) {
			predicatesArr = predicatesStr
					.split("and(?=([^\\\"']*[\\\"'][^\\\"']*[\\\"'])*[^\\\"']*$)");
		} else if (predicatesStr.contains(" or ")) {
			predicatesArr = predicatesStr
					.split("or(?=([^\\\"']*[\\\"'][^\\\"']*[\\\"'])*[^\\\"']*$)");
		} else {
			predicatesArr = new String[] { predicatesStr };
		}
		return predicatesArr;
	}

	/**
	 * @param predicateStr
	 * @param predicateLevel
	 * @param priorbuffer
	 *            this function will find the level for a predicate which is
	 *            needed to get xpath for the predicate
	 */
	public static String findThePredicateLevel(String predicateStr,
			String predicateLevel, String priorbuffer) {
		String[] arr = priorbuffer.split(predicateStr);
		predicateLevel = arr[0];
		predicateLevel = predicateLevel.replaceAll("\\[.*?\\]", "");
		// int index = ;
		predicateLevel = predicateLevel.substring(0,
				predicateLevel.lastIndexOf("["));
		predicateLevel = predicateLevel.replaceAll("\\[.*? and ", "");
		predicateLevel = predicateLevel.replaceAll("\\[.*? or ", "");
		predicateLevel = predicateLevel.replaceAll("\\[", "");
		return predicateLevel;
	}

	public static String removeStringFunctionFromPredicate(String predicateStr) {
		predicateStr = predicateStr.replace("string", "");
		predicateStr = predicateStr.replace("(", "").replace(")", "");
		return predicateStr;
	}

}
