package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class AndEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public AndEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}
	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		left.setContext(context);
		StringBuffer string = new StringBuffer();
		string.append(" ( ");
		string.append(left.evaluate());
		string.append(" && ");
		right.setContext(context);
		string.append(right.evaluate());
		string.append(" ) ");
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return string;
	}

	public static void main(String[] args) {
		ISOConsumptionLogger.info(" && ");
	}

}
