package com.mmpnc.rating.iso.consumption.returnmodels;

import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public interface ICreateIcmRequireData {
	public void createIcmRequiredDataModel(String referenceType,Map<String,List<IcmRatingNodeContent>> ratingNodeMap,List<IcmRatingNodeContent> icmNodeContentList);
}
