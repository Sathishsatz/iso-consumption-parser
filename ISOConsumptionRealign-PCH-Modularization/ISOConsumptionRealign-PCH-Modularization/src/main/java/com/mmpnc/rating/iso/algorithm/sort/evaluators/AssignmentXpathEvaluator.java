package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class AssignmentXpathEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public AssignmentXpathEvaluator(Context context,
			Evaluator stringEvaluator, Evaluator e1) {
		this.context = context;
		this.left = stringEvaluator;
		this.right = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void validate() throws RecognitionException {
		if((this.left == null) && (this.right==null)){
			throw new ParserException("Left and Right evaluators are NULL in ASSIGN evaluator");
		}
		if(this.left == null){
			throw new ParserException("Left evaluator is NULL in ASSIGN evaluator");
		}
		if(this.right == null){
			throw new ParserException("Right evaluator is NULL in ASSIGN evaluator");
		}
	}

	
	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		StringBuffer currentAlgoSteps = (StringBuffer) context.getValue(ContextParam.ALGO);
		
		Integer spaceCount = (Integer) context.getValue(ContextParam.SPACECOUNT);
		
		//append spaces
		appendSpace(returnValue, spaceCount);
		
		this.left.setContext(context);
		Object lhsObject = this.left.evaluate();
		if(ExceptionHandler.checkNullOrEmpty(lhsObject)){
			ExceptionHandler.raiseParserException("LHS can not be NULL or EMPTY in ASSIGN evaluator");
		}
		returnValue.append(lhsObject);
		
		returnValue.append("=");
	
		this.right.setContext(context);
		Object rhsObject = this.right.evaluate();
		if(ExceptionHandler.checkNull(rhsObject)){
			ExceptionHandler.raiseParserException("RHS can not be NULL in ASSIGN evaluator");
		}
		returnValue.append(rhsObject);
		
		currentAlgoSteps.append(returnValue).append("\n");
		
		return "";
	}
	
	private void appendSpace(StringBuffer buffer, int count) {
		for (int x = 0; x < count; x++) {
			buffer.append(" ");
		}
	}

	
}
