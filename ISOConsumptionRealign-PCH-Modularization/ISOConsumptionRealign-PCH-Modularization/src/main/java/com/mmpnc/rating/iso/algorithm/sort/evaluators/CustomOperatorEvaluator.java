package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class CustomOperatorEvaluator implements Evaluator, IEvaluatorValidator {

	private Evaluator left;
	private Evaluator right;
	private Context context;
	
	public CustomOperatorEvaluator(Evaluator left, Evaluator right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void validate() throws RecognitionException {
		if((this.left == null) && (this.right==null)){
			throw new ParserException("Left and Right evaluators are NULL in StartsWith evaluator");
		}
		if(this.left == null){
			throw new ParserException("Left evaluator is NULL in StartsWith evaluator");
		}
		if(this.right == null){
			throw new ParserException("Right evaluator is NULL in StartsWith evaluator");
		}
	}
	
	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append("startsWith");
		returnValue.append(" ( ");
		
		this.left.setContext(context);
		Object arg1 = this.left.evaluate();
		if(ExceptionHandler.checkNull(arg1)){
			ExceptionHandler.raiseParserException("Arg1 is NULL in startsWith Function");
		}
		returnValue.append(arg1);
		
		returnValue.append(", ");
		
		this.right.setContext(context);
		Object arg2 = this.right.evaluate();
		if(ExceptionHandler.checkNull(arg2)){
			ExceptionHandler.raiseParserException("Arg2 is NULL in startsWith Function");
		}
		returnValue.append(arg2);
		returnValue.append(" ) ");
		
		return returnValue;
	}

	

}
