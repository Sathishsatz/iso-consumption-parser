package com.mmpnc.rating.iso.algorithm.util;

import java.util.HashMap;
import java.util.Map;

import com.mmpnc.connection.helper.Database;

/**
 * @author nilkanth9581
 * 
 */
public class DateYearMonthHandlerUtil {

	private static final Map<String, String> custFunctionMap = new HashMap<String, String>();

	static {
		custFunctionMap.put(".Day", "GetDayOfMonth");
		custFunctionMap.put(".Year", "GetYear");
		custFunctionMap.put(".Month", "GetMonth");
		custFunctionMap.put(".Days", "GetDays");
	}

	public static String handleDateYearOrMonthInXpath(Database dsDB,
			String expressionConent, String scopePath) {

		String key = null;
		if (expressionConent.endsWith(".Day")) {
			key = ".Day";
		} else if (expressionConent.endsWith(".Year")) {
			key = ".Year";
		} else if (expressionConent.endsWith(".Month")) {
			key = ".Month";
		} else if (expressionConent.endsWith(".Days")) {
			key = ".Days";
		}

		if (key != null) {
			// String actulExpression = expressionConent;
			expressionConent = expressionConent.replace(key, "");
			String qualPath = XpathUtil.getFullyQualifiedXpath(dsDB, scopePath,
					expressionConent, true);
			if ("".equals(qualPath)) {
				// if .Days prefix is not an xpath then it must be a local
				// variable
				// so if it is a local variable we need to add execute module
				// step for
				// that variable
				expressionConent = custFunctionMap.get(key) + "( getProgram( "
						+ expressionConent + " ) )";
			} else {
				expressionConent = custFunctionMap.get(key) + "( " + qualPath
						+ " )";
			}

		}
		return expressionConent.toString();
	}
}
