//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.04.25 at 03:04:05 PM EDT 
//


package com.mmpnc.rating.iso.algorithm.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IgnorePCH" type="{}Programs"/>
 *         &lt;element name="ProgramPCH" type="{}Programs"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "IgnorePCH",
    "ProgramPCH"
})
@XmlRootElement(name = "Configuration")
public class Configuration {

    @XmlElement(name = "IgnorePCH", required = true)
    protected Programs IgnorePCH;
    @XmlElement(name = "ProgramPCH", required = true)
    protected Programs ProgramPCH;

    /**
     * Gets the value of the ignorePCH property.
     * 
     * @return
     *     possible object is
     *     {@link Programs }
     *     
     */
    public Programs getIgnorePCH() {
        return IgnorePCH;
    }

    /**
     * Sets the value of the ignorePCH property.
     * 
     * @param value
     *     allowed object is
     *     {@link Programs }
     *     
     */
    public void setIgnorePCH(Programs value) {
        this.IgnorePCH = value;
    }

    /**
     * Gets the value of the programPCH property.
     * 
     * @return
     *     possible object is
     *     {@link Programs }
     *     
     */
    public Programs getProgramPCH() {
        return ProgramPCH;
    }

    /**
     * Sets the value of the programPCH property.
     * 
     * @param value
     *     allowed object is
     *     {@link Programs }
     *     
     */
    public void setProgramPCH(Programs value) {
        this.ProgramPCH = value;
    }

}
