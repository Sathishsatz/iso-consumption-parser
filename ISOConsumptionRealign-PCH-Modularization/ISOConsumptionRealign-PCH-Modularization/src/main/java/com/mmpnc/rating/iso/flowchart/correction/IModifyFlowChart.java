package com.mmpnc.rating.iso.flowchart.correction;

import com.mmpnc.rating.iso.algorithm.vo.LOB;

/**
 * @author nilkanth9581
 *	As in 3.2.2 iso parser they added premium node in flow chart 
 *  To address it this functionality has been developed
 *   
 *   case-1 : if flowchart has both rating and premium nodes then add premium node variables at the
 *   		  end of rating node variables of that class
 *   case-2 : if class has only premium node then create one new rating node add all variables from premium
 *   		  node to the rating node 
 *   case-3 : if there is only rating node for this class then do not modify that class attributes
 *   case-4 : if both nodes are not present then do not modify anything
 *   case-5 : if there are two premium nodes for the same class then it should throw an exception
 *     
 */
public interface IModifyFlowChart {
	/**
	 * @param lob
	 * Modify flow chart so that algorithm split functionality will work the same way
	 * it worked in past.
	 * 
	 */
	public void modifyFlowChart(LOB lob); 
}
