package com.mmpnc.rating.iso.algorithm;

import org.antlr.runtime.RecognitionException;


public interface IEvaluatorValidator {
	void validate() throws RecognitionException;
}
