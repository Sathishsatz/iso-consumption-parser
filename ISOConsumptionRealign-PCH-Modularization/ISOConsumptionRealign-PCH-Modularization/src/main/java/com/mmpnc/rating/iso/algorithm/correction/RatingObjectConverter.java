package com.mmpnc.rating.iso.algorithm.correction;

import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @author Nilkanth9581
 *
 */
public class RatingObjectConverter implements Converter{
	
	    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
	        
	    	Rating rating = (Rating) value;
	        writer.addAttribute("variables", rating.getVariables());
	        writer.addAttribute("className", rating.getClassName());
	        writer.setValue(rating.getClassName());
	    }
	    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
	    	
	        Rating rating = new Rating();
	        rating.setClassName(reader.getAttribute("className"));
	        rating.setVariables(reader.getAttribute("variables"));
	        return rating;
	    }

	    public boolean canConvert(@SuppressWarnings("rawtypes") Class clazz) {
	        return clazz.equals(Rating.class);
	    }

}
