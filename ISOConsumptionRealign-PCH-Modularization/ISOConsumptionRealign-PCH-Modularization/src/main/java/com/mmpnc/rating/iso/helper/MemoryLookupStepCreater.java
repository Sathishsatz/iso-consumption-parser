
package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.List;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.IdGeneration;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.LookupCriteriaModel;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.rating.iso.config.Configurer;


/**
 * @author nilkanth9581
 *
 */
public class MemoryLookupStepCreater {
	
	
	
	
	@SuppressWarnings("unchecked")
	public static String createMemoryLookup(Context context,String parentCondition,List<SingleFieldConstraintEBLeftSide> filterList,String returnAttribute){
		
				
		LookupNameDecider lookupNameDecider = (LookupNameDecider)context.getValue(ContextParam.LOOKUP_NAME_DECIDER);
		String lookupName =  lookupNameDecider.getMemoryLookupName();
		LookupStep lookupStep = new LookupStep();
		lookupStep.setName(lookupName);
		lookupStep.setRemark(Constants.MEMORY_LOOKUP_REMARK);
		lookupStep.setStepType(Constants.MEMORY_LOOKUP_STEPTYPE);
		
		if(parentCondition != null)
		lookupStep.setParentCondition(parentCondition);
		//LOOKUPMODEL
		LookupModel globalLookupModel = new LookupModel();
		
		//This is required for global lookup and can be changed so kept in  the properties files
		globalLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		globalLookupModel.setLookupDesc("MEMORY LOOKUP STEP");
		
		globalLookupModel.setBaseLookUpCriteria(lookupName);
		globalLookupModel.setBaseModel(Constants.MEMORY_LOOKUP_BASE_MODEL_NAME);
		globalLookupModel.setLookupName(lookupName);
		globalLookupModel.name = lookupName;
		globalLookupModel.setLookupType(Constants.MEMORY_LOOKUP_LOOKUP_TYPE);
		globalLookupModel.setGlobalIndicator(Constants.MEMORY_LOOKUP_TYPE_INDICATOR);
		globalLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		globalLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		
		
		//LookupModel localLookupModel = new LookupModel();
		/*localLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		localLookupModel.setLookupDesc("DefaultDescription");
		
		localLookupModel.setBaseLookUpCriteria(lookupName);
		localLookupModel.setBaseModel(Constants.MEMORY_LOOKUP_BASE_MODEL_NAME);
		localLookupModel.setLookupName(lookupName);
		localLookupModel.name = lookupName;
		localLookupModel.setLookupType(Constants.MEMORY_LOOKUP_LOOKUP_TYPE);
		localLookupModel.setGlobalIndicator(Constants.MEMORY_LOOKUP_TYPE_INDICATOR);
		localLookupModel.setAddEffectiveFilter(Constants.MEMORY_LOOKUP_ADD_EFFECTIVE_FILTER);
		localLookupModel.setMultipleRowsCheck(Constants.MEMORY_LOOKUP_MULTIPLE_ROW_CHECK);
		*/
		
		
		LookupCriteriaModel globalLookupCriteriaModel = new LookupCriteriaModel();
		
		
		globalLookupModel.setLookupCriteriaModel(globalLookupCriteriaModel);
		
		FactPattern globalFactPattern = new FactPattern();
		
		
			
		for(SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide : filterList){
			globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
		}
			
		globalLookupCriteriaModel.addLhsItem(globalFactPattern);
		
		
		//LookupModel lookupModel2 = new LookupModel();
		//Attribute assignedAttribute = new Attribute();
		com.mmpnc.icm.common.ide.models.ComplexAttribute  assignedAttribute = new com.mmpnc.icm.common.ide.models.ComplexAttribute();
		List<Attribute> assignedAttributes = new ArrayList<Attribute>();
		assignedAttributes.add(assignedAttribute);
		
		
		SingleFieldConstraintEBLeftSide singleFieldConstraint = new SingleFieldConstraintEBLeftSide();
		singleFieldConstraint.setValue(returnAttribute);
		assignedAttribute.setSingleFieldConstraint(singleFieldConstraint);
		
		//SETTING VALUES TO ASSIGNED ATTRIBUTE
		assignedAttribute.setName(returnAttribute);
		assignedAttribute.setValue(returnAttribute);
		String aliasName = lookupNameDecider.getMemoryLookupAlaisName();
		
		assignedAttribute.setAlias(aliasName);
		assignedAttribute.setLocalIndicator(Constants.LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR);
		
		
		
		//ADDING ID TO THE RETURN ATTRIBUTE
		assignedAttribute.setId(IdGeneration.idGenerator(lookupName));
		globalLookupModel.setAssignedAttributes(assignedAttributes);
		
		//globalLookupModel.setLookupCriteriaModel(globalLookupCriteriaModel);
		//globalLookupCriteriaModel.addLhsItem(globalFactPattern);
		//LookupModels lookupModels = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
		//ADDING GLOBAL LOOKUP MODEL INTO THE LOOKUP MODEL LIST
		//lookupModels.add(globalLookupModel);
		
		String lookupAliasName = lookupName+aliasName;
		String lookupId = IdGeneration.idGenerator(lookupAliasName);
		globalLookupModel.setLookuoID(lookupId);
		
		//lookupModel2.setLookuoID(lookupId);
		
		//Setting id for LookupCriteria 
		lookupStep.setLookupmodel(globalLookupModel);
		
		List<AlgoStep> algoSteps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		algoSteps.add(lookupStep);
		return aliasName;
	}
	
	
}
