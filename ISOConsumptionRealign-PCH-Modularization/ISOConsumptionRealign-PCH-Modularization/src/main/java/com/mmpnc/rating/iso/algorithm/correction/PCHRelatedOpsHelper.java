package com.mmpnc.rating.iso.algorithm.correction;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public class PCHRelatedOpsHelper {
	
	public static void addPCHWithNameContainingSlash(int sequenceNo,Database currentScopeDB,List<String>pchArryList,Map<String,PCH> pchWithAncestor) throws Exception{
		if(sequenceNo == 1){
			StringBuffer queryString1 = new StringBuffer();
			//GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			queryString1.append("//PCH[@name[contains(.,'/')]]");
			String returnPCHList = createQueryAndReturnResult(currentScopeDB,queryString1.toString()).toString();
			if(returnPCHList != null && !"".equals(returnPCHList)){
				returnPCHList = "<scope>"+returnPCHList+"</scope>";
				try {
					Object object = JAXBUtils.readFromSource(returnPCHList, Scope.class);
					if(object instanceof Scope){
						Scope scopeNewObj = (Scope)object;
						for(Object pchobject:scopeNewObj.getContent()){
							if(pchobject instanceof PCH){
								String pchName = ((PCH) pchobject).getName();
								
								//ADDING PCH NAMES TO PCH NAME ARRAY SO THIS WILL BE INCLUDED IN THE FIRST SCOPE WE SPLIT
								if(pchName.startsWith("ancestor")){
									pchWithAncestor.put(pchName, (PCH)pchobject);
								}
								pchArryList.add(pchName);
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void addPCHWithNameContainingSlash(Database currentScopeDB,Map<String,PCH> pchWithAncestor) throws BasexException{
			StringBuffer queryString1 = new StringBuffer();
			//GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			queryString1.append("//PCH[@name[contains(.,'/')]]");
			Object returnPCHList = createQueryAndReturnResult(currentScopeDB,queryString1.toString());
			if(returnPCHList != null && !"".equals(returnPCHList.toString())){
				returnPCHList = "<scope>"+returnPCHList+"</scope>";
				try {
					Object object = JAXBUtils.readFromSource(returnPCHList.toString(), Scope.class);
					if(object instanceof Scope){
						Scope scopeNewObj = (Scope)object;
						for(Object pchobject:scopeNewObj.getContent()){
							if(pchobject instanceof PCH){
								String pchName = ((PCH) pchobject).getName();
								
								//ADDING PCH NAMES TO PCH NAME ARRAY SO THIS WILL BE INCLUDED IN THE FIRST SCOPE WE SPLIT
								if(pchName.startsWith("ancestor")){
									pchWithAncestor.put(pchName, (PCH)pchobject);
								}
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	}
	
	private static Object createQueryAndReturnResult(Database database,String queryString) throws BasexException {
		Connection connection = new XMLConnection(database);
		Query query = new XmlQuery(connection); 
		query.createQuery(queryString);
		Object returnObj = query.execute();
		return returnObj;
	}
	
	/**
	 * @param ScopeContentList
	 * THIS WILL SORT THE PCH LIST ACCORDING TO THE EXCEL ROW NUMBER INT THE PCH
	 */
	public static void sortPCHListAccordingToExcelRow(Scope scope){
		
		Comparator<Object> comparator = new Comparator<Object>() {

			@Override
			public int compare(Object o1, Object o2) {
				if(o1 instanceof PCH && o2 instanceof PCH){
					PCH pch1 = (PCH) o1;
					PCH pch2 = (PCH) o2;
					if(pch1.getExcelRow() > pch2.getExcelRow())
						return 1;
				}
				return 0;
			}
		};
		
		Collections.sort(scope.getContent(), comparator);
	
	}
	
	public static void checkForExcluedePCH(String referenceType,Map<String,PCHInfo> processedPCHMap,Database currentScopeDB,Scope mainScope,List<String> processedPchList,Scope newScope) throws Exception{
		if(processedPchList.size() == 0)
			return;
		Connection currentScopeCon = new XMLConnection(currentScopeDB);
		Query query = new XmlQuery(currentScopeCon); 
		StringBuffer queryString = new StringBuffer();
		//class[@name='BOP']/rating
		queryString.append("//PCH/@name");
		Set<String> pchSet = new HashSet<String>();
		query.createQuery(queryString.toString());
		Object returnObj = query.execute();
		if(returnObj != null){
			String pchName = returnObj.toString();
			String [] arr = pchName.split("name=");
			for(String pchNam : arr){
				if(!"".equals(pchNam) && pchNam.contains("\""))
				{
					pchNam = pchNam.replace("\"", "").trim();
					 pchSet.add(pchNam);
				}
				 
			}
		}
		for(String pchLeft : pchSet){
			if(!processedPchList.contains(pchLeft)){
				ISOConsumptionLogger.info("NO REFERENCE FOR PCH WITH NAME=["+pchLeft+"] IN ALGORITHM=[ "+newScope.getDbTables()+"]");
				queryString = new StringBuffer();
				queryString.append("//PCH[@name='").append(pchLeft).append("'][1]");
				query.createQuery(queryString.toString());
				Object returnObject = query.execute();
				if(returnObject != null && !"".equals(returnObject)){
					try {
						PCH pch =(PCH)JAXBUtils.readFromSource(returnObject.toString(), PCH.class);
						//pchLeft = handleAncestorInXpath(pchLeft);
						processedPCHMap.get(referenceType+newScope.getDbTables()+newScope.getSequenceNo()+newScope.getPass()).getUnusedPCH().add(pchLeft.trim());
						newScope.getContent().add(pch);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}
		//SORTING NEWLY CREATED SCOPE ACCORDING TO THE EXCEL ROW NUMBER
		//FOLLWING HAS BEEN COMMENTED AFTER SHASHIS SUGGESTION ABOUT THIS
		//PCHRelatedOpsHelper.sortPCHListAccordingToExcelRow(newScope);
	}
	
	
	
	/**
	 * @param scope
	 * @param pchArryList
	 * @param currentScopeDB
	 * @param sequenceNo
	 * @param newScope
	 * THIS MEHTO WILL LOOP THROUGH THE PCH NAMES DECLARED IN RATING NODE AND GET THOSE PCH FROM THE MAIN SCOPE
	 * AND ADD IT TO THE NEWLY CREATED SCOPE
	 * @throws Exception 
	 */
	public static void getPCHFromScopeAndAddTonewScope(String referenceType,Map<String,PCH> pchWithAncestor,Map<String,PCHInfo> processedPCHMap,Scope scope,List<String>pchArryList,Database currentScopeDB,int sequenceNo,Scope newScope) throws Exception{
		for(String pchName : pchArryList){
			StringBuffer queryString1 = new StringBuffer();
			//HANDLING ANCESTOR IN XPATH STRING
			//pchName = handleAncestorInXpath(pchName);
			///GETTING ONLY SINGLE PCH WITH THE NAME IN BOPSTRUCTURE THERE ARE MULTIPLE PCH WITH SAME NAME
			if(pchName.startsWith("ancestor")){
				newScope.getContent().add(pchWithAncestor.get(pchName));
			}
			else{
				queryString1.append("//(scope[@dbTables='").append(scope.getDbTables()).append("']/PCH[@name='"+pchName+"'])[1]");
				Object returnPCH = createQueryAndReturnResult(currentScopeDB, queryString1.toString());
				try {
					//this condition added because in case of split algorithms actual scope dbTable is without slash
					if("".equals(returnPCH) && scope.getDbTables().contains("/")){
						StringBuffer newQuery = new StringBuffer();
						String dbTable = scope.getDbTables().substring(scope.getDbTables().lastIndexOf("/")+1);
						newQuery.append("//(scope[@dbTables='").append(dbTable).append("']/PCH[@name='"+pchName+"'])[1]");
						returnPCH = createQueryAndReturnResult(currentScopeDB, newQuery.toString());
					}
					if(!"".equals(returnPCH) ){
						Object pchObj  = JAXBUtils.readFromSource(returnPCH.toString().trim(),PCH.class);
						PCH pch = (PCH) pchObj;
						//String pchNameWithoutAncestor = handleAncestorInXpath(pch.getName());
						processedPCHMap.get(referenceType+scope.getDbTables()+sequenceNo+scope.getPass()).addToPCHListInRatingNode(pch.getName().trim());
						//pch.setExecutedAtScopeSplitLevel(true);
						newScope.getContent().add(pch);
					}
				} catch (Exception e) {
					ExceptionHandler.raiseException("Exception while adding PCH to new scope", e);
				}
			}
		
		}
	}
}