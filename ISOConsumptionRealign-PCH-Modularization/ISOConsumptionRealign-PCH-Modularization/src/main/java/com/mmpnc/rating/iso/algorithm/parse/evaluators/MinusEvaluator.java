package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class MinusEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public MinusEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		StringBuffer minus = new StringBuffer();
		minus.append(" ( ");
		left.setContext(context);
		minus.append(left.evaluate());
		minus.append(" - ");
		right.setContext(context);
		minus.append(right.evaluate());
		minus.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return minus;
	}

}
