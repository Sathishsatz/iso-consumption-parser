package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionMessages;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.LookupStepCreater;

/**
 * @author nilkanth9581
 * THIS CLASS WILL HANDLE THE DOMAIN TABLE FUNCTION IN ALGORITHM FILE
 * 
 */
public class DomainTableFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String functionName, String varaible, Context context) throws RecognitionException {
		
		LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
		int localCounter = locHolder.getNextLocVarCounter();
		String aliasName = "localVariable"+localCounter;
		localCounter = localCounter+1;
		LookupStep lookupStep = null;
		try{
			LookupStepCreater lookupStepCreater = LookupStepCreater.getInstance();
			lookupStep = lookupStepCreater.createDomainTableLookup(context,aliasName,evallist);
		}catch(Exception e){
			ExceptionHandler.raiseParserException(ExceptionMessages.DOMAINTABLE_LOOKUP_STEP);
		}
		
		//context.putValue(ContextParam.LOCALVARIABLECOUNTER, localCounter);
		@SuppressWarnings("unchecked")
		List<AlgoStep> algoStep = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		if(lookupStep != null)
		algoStep.add(lookupStep);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		return aliasName;
	}

}
