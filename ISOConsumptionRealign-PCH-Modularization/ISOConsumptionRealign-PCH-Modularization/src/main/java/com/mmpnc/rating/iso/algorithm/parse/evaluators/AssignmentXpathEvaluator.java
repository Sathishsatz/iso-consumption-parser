package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.icm.common.ide.models.IFStatementStep;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.AssignRhsXpathPredicateUtil;
import com.mmpnc.rating.iso.helper.ArithmaticStepCreater;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.rating.iso.helper.IFStepCreater;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.SetAttrbuteStepCreater;

public class AssignmentXpathEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public AssignmentXpathEvaluator(Context context, Evaluator x, Evaluator e1) {
		this.left = x;
		this.right = e1;
		this.context = context;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;

	}

	@Override
	public Object evaluate() throws RecognitionException{
		
		left.setContext(context);
		String LHS = left.evaluate().toString();
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		//boolean lhsModelAttrFlag = (Boolean) context.getValue(ContextParam.MODELATTRIBUTE);
		right.setContext(context);
		//PUTTING A FLAG IN THE CONTEXT TO INDICATE THAT THIS XPATH IS OF ASSIGN RHS STMT
		context.putValue(ContextParam.ISASSIGNEMENTRHSXPATH, true);
		//PUTTING WHETHER THE VARIABLE IS LOOCAL OR MODEL ATTRIBUTE
		Object object = right.evaluate();
		context.putValue(ContextParam.ISASSIGNEMENTRHSXPATH, false);
		
		Object isAssignmetnRhsHasPredicateObj = context.getValue(ContextParam.IS_ASSIGNSTATEMENTRHSXPATH);
		
		if(isAssignmetnRhsHasPredicateObj == null)
			isAssignmetnRhsHasPredicateObj = false;
		Boolean isAssignmetnRhsHasPredicate = (Boolean)isAssignmetnRhsHasPredicateObj;
		
		IFStatementStep ifStatementStep = null;
		
		parentCondition = handleAssignementRhsPredicate(ifStatementStep, isAssignmetnRhsHasPredicate, parentCondition);
		
		boolean rhsModelAttrFlag =(Boolean) context.getValue(ContextParam.MODELATTRIBUTE);
		String RHS = getRHS(object);
		
		//IF VAR IS A MODEL AND IS CALLED FROM ASSIGNMENT EVALUATOR WE NEED TO CREATE
		//SETATTRIBUTE STEP AND RETURN ALIAS NAME OF THE SET ATTRIBUTE STEP
		handleStepcreation(rhsModelAttrFlag, RHS, parentCondition, LHS, isAssignmetnRhsHasPredicate, ifStatementStep);
		context.putValue(ContextParam.MODELATTRIBUTE, false);
			
		
		return null;
	}
	
	
	private void handleStepcreation(boolean rhsModelAttrFlag,String RHS,String parentCondition,String LHS,boolean isAssignmetnRhsHasPredicate,IFStatementStep ifStatementStep){
		@SuppressWarnings("unchecked")
		List<AlgoStep> stepList = (List<AlgoStep>) context.getValue(ContextParam.STEPS);
		
		if(rhsModelAttrFlag || (RHS.contains("+") || RHS.contains("-")|| RHS.contains("*") || RHS.contains("/")) || right instanceof StringEvaluator){
			//IN CASE OF SPECIAL FUNCTION (WHICH NEED PRECISIONS)
			//Nullifying precision scale as we do not need this for other expression steps
			LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
			int localCounter = locHolder.getNextLocVarCounter();
			String aliasName = "localVariable"+localCounter;
			ArithmeticStep arithmeticStep = ArithmaticStepCreater.getInstance().createArithmeticStep(parentCondition,aliasName,RHS,null);
			RHS = arithmeticStep.getAlias();
			LookupStep lookupStep  = SetAttrbuteStepCreater.getInstance().createSetAttributeStep(LHS,RHS,parentCondition,rhsModelAttrFlag);
			AssignRhsXpathPredicateUtil.createAndAddExpressionAndSetAttributeStep(lookupStep, isAssignmetnRhsHasPredicate, ifStatementStep, stepList, arithmeticStep);
		}
		else{
			LookupStep lookupStep  = SetAttrbuteStepCreater.getInstance().createSetAttributeStep(LHS,RHS,parentCondition,rhsModelAttrFlag);
			AssignRhsXpathPredicateUtil.createAndAddSetAttributeStep(lookupStep,isAssignmetnRhsHasPredicate,ifStatementStep,stepList);
		}
	}
	
	/**
	 * @param evalReturn
	 * @return
	 * @TODO NEED TO CHECK THIS AS SOME OF THE EVALUATOR RETURNING STRING AND SOME RETURNING STRINGBUFFER
	 */
	private String getRHS(Object evalReturn){
		String RHS = null;
		if(evalReturn instanceof String){
			RHS = evalReturn.toString();
		}else{
			RHS = ((StringBuffer)evalReturn).toString();
		}
		return RHS;
	}
	
	
	private String handleAssignementRhsPredicate(IFStatementStep ifStatementStep,boolean isAssignmetnRhsHasPredicate,String parentCondition){
		if(isAssignmetnRhsHasPredicate){
			Object objectAssignmentPredicateCondition = context.getValue(ContextParam.ASSIGNSATEMENTRHSPREDICATEXPATH);
			IFStepCreater ifStepCreater = IFStepCreater.getInstance();
			ifStatementStep = ifStepCreater.createIfStatement(new StringBuffer(objectAssignmentPredicateCondition.toString()), parentCondition);
			ifStatementStep.setElseSelectKeyWord("NA");
			//ADDING A LIST TO IF CHILD STEPS
			List<AlgoStep> ifChildStepList =  new ArrayList<AlgoStep>();
			ifStatementStep.setAlgoStepList(ifChildStepList);
			//CHANGING PARENT CONDITION AS THE ASSIGN STEP IS GOING TO BE THE PART OF INTERMEDIARY IF CONDITION
			parentCondition = Constants.IF_THEN;
			//System.out.println(objectAssignmentPredicateCondition.toString());
		}
		return parentCondition;
	}

}
