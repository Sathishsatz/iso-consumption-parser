package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

public class AxisNameEvaluator implements Evaluator {
	
	private Context context;
	private String axisName;
	
	public AxisNameEvaluator(String string) {
		this.axisName = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()  throws RecognitionException{
		ISOConsumptionLogger.info("AxisNameEvaluator Evaluator called ");
		
		StringBuffer buffer = new StringBuffer();
		buffer = (StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		
		if(this.axisName.equals("AT")){
			buffer.append("@");
			return Type.CONSTANT;
		} else{
			buffer.append(this.axisName);
			return Type.ATTRIBUTE;
		}
		
	}

}
