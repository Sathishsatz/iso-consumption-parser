package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class FunctionEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private List<Evaluator> evallist;
	private String constant;
	private String functionName;
	private boolean isDaysDiffFunction = false;
	
	public FunctionEvaluator(String constant, String string,
			List<Evaluator> evallist) {
		this.constant = constant;
		this.functionName = string;
		this.evallist = evallist;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
		StringBuffer returnValue = new StringBuffer();
		boolean isDomainTable = false;
		
		if (this.constant == null) {

			if (isXPathFunction(this.functionName)) {
				returnValue.append(" ").append(this.functionName).append(" (");
			} else if (isCustomFunction(this.functionName)) {
				returnValue.append(" ").append(this.functionName).append(" (");
			} 
			else if("days_between".equals(functionName)){
				returnValue.append(" ").append(this.functionName).append(" { ");
				isDaysDiffFunction = true;
			}
			else {
				
				// we assume that this is domain table executor style				
				returnValue.append(" ").append(this.functionName).append(" {");
				isDomainTable = true;
			}
		} else if (this.constant.equals("RateTable")) {
			returnValue.append(" RateTable:").append(this.functionName).append(" (");
		} else {
			returnValue.append(" (");
		}
		
		boolean repetationFlag = false;
		
		
		for(Evaluator eval : evallist){
			if(repetationFlag){
				returnValue.append(",");
			}
			
			eval.setContext(context);
			returnValue.append(eval.evaluate());

			repetationFlag = true;
		}
		
		if(isDomainTable){
			returnValue.append("} ");
		} 
		else if(isDaysDiffFunction){
			String s = returnValue.toString().replace("-", ",").replace("{  (", "{");
			s = s.substring(0, s.lastIndexOf(")"));
			returnValue = new StringBuffer(s).append(" }");
		}
		else {
			returnValue.append(") ");
		}
		
		return returnValue;
	}
	
	
	
	private boolean isXPathFunction(String functionName) {
		try {
			Function.valueOf(functionName);
		} catch (IllegalArgumentException _ex) {
			return false;
		}

		return true;
	}

	private boolean isCustomFunction(String functionName) {
		try {
			CustomFunction.valueOf(functionName);
		} catch (IllegalArgumentException _ex) {
			return false;
		}

		return true;
	}

	private enum CustomFunction {
		GetDeductible;
	}

	private enum Function {

		Sum("Sum"), Count("Count"), Min("Min"), Max("Max"), NA("NA");

		Function(String value) {
			this.value = value;
		}

		@SuppressWarnings("unused")
		String getValue() {
			return value;
		}

		private String value;
	}

	@Override
	public void validate() throws RecognitionException {
		if(ExceptionHandler.checkNullOrEmpty(this.functionName)){
			throw new ParserException("Function evaluator is NULL");
		}
	}
	
}