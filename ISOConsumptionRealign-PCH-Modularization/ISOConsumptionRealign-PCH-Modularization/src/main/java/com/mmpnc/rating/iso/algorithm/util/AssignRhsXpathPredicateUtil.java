package com.mmpnc.rating.iso.algorithm.util;

import java.util.List;

import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.icm.common.ide.models.IFStatementStep;
import com.mmpnc.icm.common.ide.models.LookupStep;

/**
 * @author nilkanth9581
 *
 */
public class AssignRhsXpathPredicateUtil {
	
	public static void createAndAddExpressionStep(ArithmeticStep arithmeticStep,boolean isAssignmetnRhsHasPredicate,IFStatementStep ifStatementStep,List<AlgoStep>stepList){
		if(isAssignmetnRhsHasPredicate){
			ifStatementStep.getAlgoStepList().add(arithmeticStep);
			stepList.add(ifStatementStep);
		}else{
			stepList.add(arithmeticStep);
		}
	}
	
	public static void createAndAddExpressionAndSetAttributeStep(LookupStep lookupStep,boolean isAssignmetnRhsHasPredicate,IFStatementStep ifStatementStep,List<AlgoStep>stepList,ArithmeticStep arithmeticStep){
		//List<AlgoStep> stepList = (List<AlgoStep>) context.getValue(ContextParam.STEPS);
		if(isAssignmetnRhsHasPredicate){
			ifStatementStep.getAlgoStepList().add(arithmeticStep);
			ifStatementStep.getAlgoStepList().add(lookupStep);
			stepList.add(ifStatementStep);
		}else{
			stepList.add(arithmeticStep);
			stepList.add(lookupStep);
		}
	}
	
	public static void createAndAddSetAttributeStep(LookupStep lookupStep,boolean isAssignmetnRhsHasPredicate,IFStatementStep ifStatementStep,List<AlgoStep>stepList){
		
		if(isAssignmetnRhsHasPredicate){
			ifStatementStep.getAlgoStepList().add(lookupStep);
			stepList.add(ifStatementStep);
		}else{
			stepList.add(lookupStep);
		}
	}
}
