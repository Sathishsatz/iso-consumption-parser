package com.mmpnc.rating.iso.algorithm.correction;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;

/**
 * @author nilkanth9581
 * Helper class for printing the split report
 * 
 */
public class PrintFlowChartSplitReport {
	/**
	 * @param yesNo
	 * @param nonProcessedScopes
	 * @param processedPCHMap
	 */
	public static void printReport(boolean yesNo,StringBuffer nonProcessedScopes,Map<String,List<String>> processedPCHMap){
		Iterator<Map.Entry<String, List<String>>> map = processedPCHMap.entrySet().iterator();
		StringBuffer report = new StringBuffer();
		while(map.hasNext()){
			Map.Entry<String, List<String>> entry = map.next();
			Iterator<String> itr = entry.getValue().iterator();
			report.append("---------------[Algortihm Name:["+entry.getKey()+"]-----------------");
			report.append("\n");
			report.append(" TOTAL NUMBER OF PCH INCLUDED :["+entry.getValue().size()+"]");
			report.append("\n");
			report.append("PCH NAMES [");
			while(itr.hasNext()){
				report.append(itr.next());
				report.append(",");
			}
			report.append("]");
			report.append("\n");
			report.append("\n");
			report.append("---------------------------------------------------------------------");
			report.append("\n");
			report.append("\n");
		}
		ISOConsumptionLogger.info(report.toString());
		ISOConsumptionLogger.info("----------------------- ALGORITHM NAMES WIHTOUT RATING IN FLOWCHART ---------------");
		ISOConsumptionLogger.info("\n");
		ISOConsumptionLogger.info(nonProcessedScopes.toString());
		ISOConsumptionLogger.info("\n");
		ISOConsumptionLogger.info(" ----------------------------------------------------------------------------------");
		
		/*if(cosolidatedReportList != null){
			Iterator<String> consolidateRepItr = cosolidatedReportList.iterator();
			while (consolidateRepItr.hasNext()) {
				ISOConsumptionLogger.info(consolidateRepItr.next());
			}
		}*/
		ISOConsumptionLogger.info("************************* Ending Rearranging LOB using FlowChart ************");
	}
	
	/**
	 * 	This method will print report for rearrangement of the scopes using flowchart
	 */
	public static void printReport(){
		
		ScopeInfoHolder scopeInfoHolder = ScopeInfoHolder.getInstance();
		for(String key :scopeInfoHolder.getScopeInfoMap().keySet()){
			ScopeInfo scopeInfo = scopeInfoHolder.getScopeInfo(key);
			
			StringBuffer pchMentionedInRatingNode  = new StringBuffer();
			StringBuffer intermediaryUsedPCHNames  = new StringBuffer();
			StringBuffer unusedPCHNames = new StringBuffer();
			
			for(String pchInRatingNode :scopeInfo.getPchExecSeq()){
				pchMentionedInRatingNode.append(pchInRatingNode).append("\t");
			}
			
			for(String intermediaryPCHNames:scopeInfo.getIntermediaryUsedPCH()){
				intermediaryUsedPCHNames.append(intermediaryPCHNames).append("\t");
			}
			
			for(String unusedPCHNamesStr: scopeInfo.getUnUsedPCH()){
				unusedPCHNames.append(unusedPCHNamesStr).append("\t");
			}
			
			ISOConsumptionLogger.info(" ----- Scope ["+key+"] -------");
				if(pchMentionedInRatingNode.length() == 0){
					ISOConsumptionLogger.info("SCOPE DO NOT HAVE RATING NODE IN THE FLOW CHART");
				}else{
					ISOConsumptionLogger.info("PCH NAMES IN RATING NODE : -["+pchMentionedInRatingNode+"]");
				}
				
				if(intermediaryUsedPCHNames.length() == 0){
					ISOConsumptionLogger.info("NO ITERMEDIARY PCH USED IN THIS SCOPE");
				}else{
					ISOConsumptionLogger.info("INTERMEDIARY PCH USED:-["+intermediaryUsedPCHNames+"]");
				}
				
				if(unusedPCHNames.length() == 0){
					ISOConsumptionLogger.info("NO UNUSED PCH IN THIS SCOPE");
				}else{
					ISOConsumptionLogger.info("UNUSED PCH NAMES: -["+unusedPCHNames+"]");
				}
				
			ISOConsumptionLogger.info(" --------- end ------------------ ");
			
			
			
			
			
		}
	
	}
}
