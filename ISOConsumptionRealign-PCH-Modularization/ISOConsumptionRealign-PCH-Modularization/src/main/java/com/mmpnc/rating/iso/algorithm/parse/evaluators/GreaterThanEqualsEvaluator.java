package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.PredicatesUtil;

public class GreaterThanEqualsEvaluator implements Evaluator {

	private Evaluator left;
	private Evaluator right;
	Context context ;
	
	public GreaterThanEqualsEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		left.setContext(context);
		StringBuffer greaterThanEquals = new StringBuffer();
		greaterThanEquals.append(" ( ");
		greaterThanEquals.append(left.evaluate());
		greaterThanEquals.append(" >= ");
		right.setContext(context);
		greaterThanEquals.append(right.evaluate());
		PredicatesUtil.addIfPredicateConition(context, greaterThanEquals);
		greaterThanEquals.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return greaterThanEquals;
	}

}
