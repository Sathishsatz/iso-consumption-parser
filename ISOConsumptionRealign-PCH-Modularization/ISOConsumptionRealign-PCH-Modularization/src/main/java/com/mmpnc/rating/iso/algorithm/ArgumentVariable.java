package com.mmpnc.rating.iso.algorithm;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;

public class ArgumentVariable {
		
	public static String assignType(Database db, String reference, String scope, String attribute){
		Connection conn = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
				
		if(isPresent(query, scope, attribute)){
			return attribute;
		}else{
			return "LV_VALUE:" + attribute;
		}		
	}
	
	private static boolean isPresent(Query query,String scope, String attribute){
		StringBuffer qry = new StringBuffer();
		qry.append("(//").append(scope).append("/").append(attribute).append(")[1]/name()");
		
		query.createQuery(qry.toString());
		String retString = (String) query.execute();
		
		if(retString != null && (retString.equals(attribute))){
			return true;
		}else{
			return false;
		}		
	}

	public static boolean isVariablePresent(Database db,String reference, String scope, String attribute){
		Connection conn = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		
		StringBuffer qry = new StringBuffer();
		qry.append("(//").append(scope).append("/").append(attribute).append(")[1]/name()");
		
		query.createQuery(qry.toString());
		String retString = (String) query.execute();
		
		if(retString != null){
			return true;
		}else{
			return false;
		}		
	}
	
	public static String getXpath(Database db,String reference, String scope, String attribute){
		Connection conn = DBFactoryBuilder.getXmlConnection(db);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		
		StringBuffer qry = new StringBuffer();
		qry.append("(//").append(scope).append("/").append(attribute).append(")[1]/@xpath/data()");
		
		query.createQuery(qry.toString());
		String retString = (String) query.execute();
		
		if(retString != null){
			return retString;
		}else{
			return "";
		}		
	}
	
}
