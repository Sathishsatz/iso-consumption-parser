package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class ContainsFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context)
			throws RecognitionException {
		// TODO Auto-generated method stub
		StringBuffer contains = new StringBuffer();
		for(Evaluator eval : evallist){
			eval.setContext(context);
			contains.append(eval.evaluate().toString());
			contains.append(",");
		}
		String func= contains.toString().substring(0,contains.lastIndexOf(","));
		return "Contains ("+ func +" )";
	}

}
