package com.mmpnc.rating.iso.publish.lookup;



/**
 * @author nilkanth9581
 *
 */
public interface ILookupDAO {

	void insertLookupCriteria(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void insertLookupCriteriaFilter(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void insertLookupCriteriaReturnAttr(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void deleteLookupCriteria(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void deleteLookupCriteriaFilter(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void deleteLookupCriteriaReturnAttr(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	/*LookupModel selectLookupCriteria(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void selectLookupCriteriaFilter(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void delete_all(com.mmpnc.icm.common.ide.models.LookupModel model);
	
	void insert(com.mmpnc.icm.common.ide.models.LookupModel model); 
	
	void delete(LookupModel lookupModel,SingleFieldConstraintEBLeftSide single,Attribute attr);
	
	LookupModel find(String id);
	SingleFieldConstraintEBLeftSide find2(String id);
	Attribute find3(String id);
	void update(SingleFieldConstraint single,LookupModel lookupModel,Attribute attr);
	*/
	//void update_filter(LookupModel lookupModel);
	
	
	

}
