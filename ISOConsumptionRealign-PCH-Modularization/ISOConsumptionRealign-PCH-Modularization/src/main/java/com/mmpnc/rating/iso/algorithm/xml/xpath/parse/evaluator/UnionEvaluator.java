package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class UnionEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;

	public UnionEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		StringBuffer buffer = (StringBuffer) this.context
				.getValue(ContextParam.XPATHSTRING);

		String operator = Operator.union;

		this.left.setContext(context);
		this.left.evaluate();

		buffer.append(operator);

		this.right.setContext(context);
		this.right.evaluate();

		return Type.PATH;
	}

}
