package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class LoopEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private List<Evaluator> evallist;
	private String loopString;
	
	public LoopEvaluator(Context context, String string, List<Evaluator> s1) {
		this.context = context;
		this.loopString = string;
		this.evallist = s1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {

		validate();
		
		StringBuffer currentAlgoSteps = (StringBuffer) context.getValue(ContextParam.ALGO);
		StringBuffer localVariablePCHAlgoSteps = (StringBuffer) context.getValue(ContextParam.PCHALGO);
		Integer spaceCount = (Integer) context.getValue(ContextParam.SPACECOUNT);
		Boolean isLoopStatementExecution = (Boolean) this.context.getValue(ContextParam.LOOPSTATEMENTEXECUTION);
		
		//append spaces
		if(ExceptionHandler.checkNullOrEmpty(loopString)){
			ExceptionHandler.raiseParserException("Filter condition can not be null in LOOP evaluator");
		}
		appendSpace(currentAlgoSteps, spaceCount);	
		currentAlgoSteps.append("LOOP THROUGH  ").append(loopString).append("\n");
		
		for(Evaluator eval : evallist){
			StringBuffer loopStatement = new StringBuffer();
			
//			appendSpace(currentAlgoSteps, spaceCount);
			
			context.putValue(ContextParam.PCHALGO, new StringBuffer());
			context.putValue(ContextParam.ALGO, loopStatement);
			context.putValue(ContextParam.SPACECOUNT, spaceCount + 1);
			context.putValue(ContextParam.LOOPSTATEMENTEXECUTION, true);
			
			eval.setContext(context);			
			eval.evaluate();
			
			StringBuffer locatPCHVariableAlgo = (StringBuffer) context.getValue(ContextParam.PCHALGO);
			
			currentAlgoSteps.append(locatPCHVariableAlgo);
			currentAlgoSteps.append(loopStatement);
		}
		
		//append spaces
		appendSpace(currentAlgoSteps, spaceCount);
		currentAlgoSteps.append("END LOOP\n");
		
		
		//set the context to prior algo steps
		context.putValue(ContextParam.PCHALGO, localVariablePCHAlgoSteps);
		context.putValue(ContextParam.ALGO, currentAlgoSteps);
		context.putValue(ContextParam.SPACECOUNT, spaceCount);
		context.putValue(ContextParam.LOOPSTATEMENTEXECUTION, isLoopStatementExecution);
		
//		System.out.println("LOOP Statement - " + returnValue);
		
		return "";
	}
	
	private void appendSpace(StringBuffer buffer, int count) {
		for (int x = 0; x < count; x++) {
			buffer.append(" ");
		}
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.loopString==null || this.loopString.equals("")){
			throw new ParserException("Loop condition value is NULL or empty in LOOP evaluator");
		}
		
	}

}
