package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.ArgumentVariable;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class EqualsEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;

	public EqualsEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		StringBuffer buffer = (StringBuffer) this.context
				.getValue(ContextParam.XPATHSTRING);
		boolean isPredicate = (Boolean) this.context
				.getValue(ContextParam.ISPREDICATE);

		String operator = Operator.equals;
		
		if (!isPredicate) {

			// Evaluate left
			StringBuffer leftBuffer = new StringBuffer();
			this.context.putValue(ContextParam.XPATHSTRING, leftBuffer);
			this.left.setContext(context);
			Type leftType = (Type) this.left.evaluate();

			// Evaluate Right
			StringBuffer rightBuffer = new StringBuffer();
			this.context.putValue(ContextParam.XPATHSTRING, rightBuffer);
			this.right.setContext(context);
			Type rightType = (Type) this.right.evaluate();

			if (leftType.equals(Type.ATTRIBUTE)) {
				buffer.append(ArgumentVariable.assignType((Database) context
						.getValue(ContextParam.OBJECTDATABASE),
						(String) context.getValue(ContextParam.REFERENCE),
						(String) context.getValue(ContextParam.SCOPE),
						leftBuffer.toString()));
			} else if (leftType.equals(Type.PATH)) {
				buffer.append("XPATH:\"").append(leftBuffer).append("\"");
			} else {
				buffer.append(leftBuffer);
			}

			buffer.append(operator);

			if (rightType.equals(Type.ATTRIBUTE)) {
				buffer.append(ArgumentVariable.assignType((Database) context
						.getValue(ContextParam.OBJECTDATABASE),
						(String) context.getValue(ContextParam.REFERENCE),
						(String) context.getValue(ContextParam.SCOPE),
						rightBuffer.toString()));
			} else if (rightType.equals(Type.PATH)) {
				buffer.append("XPATH:\"").append(rightBuffer).append("\"");
			} else {
				buffer.append(rightBuffer);
			}

			this.context.putValue(ContextParam.XPATHSTRING, buffer);

			return Type.OPERATION;
		} else {

			//context.putValue(ContextParam., value)
			this.left.setContext(context);
			this.left.evaluate();
			
			buffer.append(operator);

			this.right.setContext(context);
			this.right.evaluate();
			
			return Type.PATH;
		}
	}

}
