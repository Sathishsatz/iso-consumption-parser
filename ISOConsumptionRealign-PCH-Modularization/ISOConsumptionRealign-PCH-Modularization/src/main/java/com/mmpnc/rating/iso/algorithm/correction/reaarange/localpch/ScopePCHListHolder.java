package com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public class ScopePCHListHolder {
	private static final ScopePCHListHolder scopePCHListHolder = new ScopePCHListHolder();
	//private Map<S>
	
	private ScopePCHListHolder(){}
	
	public static ScopePCHListHolder getInstance(){
		return scopePCHListHolder;
	}
	
	private Map<String, PCHNameAndContentHolder> scopeAndPchName = new ConcurrentHashMap<String, PCHNameAndContentHolder>();
	
	protected boolean contains(String entryName){
		return scopeAndPchName.containsKey(entryName);
	}
	
	private StringBuffer buffer = new StringBuffer();
	
	protected void createNewEntryInScopePCHListMap(Scope originalScope,Scope refScope){
		String scopeAndPCHNameKey = originalScope.getDbTables()+"_"+refScope.getDbTables();
		
		if(!scopePCHListHolder.contains(scopeAndPCHNameKey)){
			buffer.append(scopeAndPCHNameKey);
			buffer.append(",");
			PCHNameAndContentHolder pchContentHolder = new PCHNameAndContentHolder();
			pchContentHolder.setOriginalScope(originalScope);
			pchContentHolder.setRefScope(refScope);
			//adding all the pch from original scope to the map
			//maintained by pchContentHolder object
			pchContentHolder.addAllPCHFromOriginalScopeToPCHList();
			scopeAndPchName.put(scopeAndPCHNameKey, pchContentHolder);
		}else{
			return;
		}
	}
	
	
	
	/**
	 * @param scopedbTable
	 * @param pch
	 * scopedbTable should be unique 
	 * we have observed that there are pch with same name having slash in different scopes
	 * so to keep the entries unique we have to use original-scope dbTable_refScope_dbTable
	 * scopedbTalbe = originalscopeDbtable_refScopeDbTable
	 */
	protected void addLocalVariableToRefScope(String variableName,String variableType,String refScopeDbTable,String orginalScopedbtable){
		String scopeAndPCHNameKey = orginalScopedbtable+"_"+refScopeDbTable;
		if(contains(scopeAndPCHNameKey)){
			scopeAndPchName.get(scopeAndPCHNameKey).addLocalVariableToRefScope(refScopeDbTable, variableName,variableType);
		}
	}
	
	
	protected void deleteSplitAlgoUsedPCHFromOriginalScope(){
		
		for(String keyName:scopeAndPchName.keySet()){
			PCHNameAndContentHolder holder = scopeAndPchName.get(keyName);
			//System.out.println("KEY_NAME"+keyName);
			//System.out.println("localPCH Set Size:");
			holder.deleteLocalVarPCHFromOriginalScope(holder);
		}
		
		
	}
}
