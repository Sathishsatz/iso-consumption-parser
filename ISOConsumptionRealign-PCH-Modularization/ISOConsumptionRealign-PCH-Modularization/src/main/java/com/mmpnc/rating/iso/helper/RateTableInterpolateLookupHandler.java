package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.IdGeneration;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.icm.common.ide.models.ConstraintType;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.LookupCriteriaModel;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.DivideEvaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.MinusEvaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.MultiplyEvaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.PlusEvaluator;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.ratetablelookup.RatingContentFileLookup;
import com.mmpnc.util.IsoConsumptionUtil;

/**
 * @author Nilkanth9581
 *
 */
public class RateTableInterpolateLookupHandler {
	
	
	enum UPPERORLOWERLIMIT{
		LOWER,UPPER
	}
	
	private static final String UPPER_LIMIT_PREFIX = "Upper";
	private static final String LOWER_LIMIT_PREFIX = "Lower";
	/**
	 * @param context
	 * @param aliasNameForUpperLimit
	 * @param function
	 * @param evallist
	 * @return
	 * @throws Exception
	 */
	public static String createInterPolateLookupSteps(Context context,String aliasNameForUpperLimit,String function,List<Evaluator> evallist,List<String>interpolatedKeyList)throws Exception{
		
		String interPolatedKeyName = null;
		if(interpolatedKeyList.size() > 1){
			ISOConsumptionLogger.info("*************************** MULTIPLE INTERPOLATED KEYS FOUND IN LOOKUP *************************"+function);
		}else{
			interPolatedKeyName = interpolatedKeyList.get(0);
		}
		//ASSIGNED ATTRIBUTE NAME IS THE RETURN OF THE LOOKUP 
		
		//CREATING LOOKUP FOR LOWER LIMIT
		String upperLimitAlias = createDBLookupStepForUpperAndLowerLimit(context,function, evallist,UPPERORLOWERLIMIT.UPPER,interPolatedKeyName);
		//CREATING LOOKUP FOR UPPER LIMIT
		String lowerLimitAlias = createDBLookupStepForUpperAndLowerLimit(context, function, evallist, UPPERORLOWERLIMIT.LOWER,interPolatedKeyName);
		//CREATING LOOKUP FOR LOWER FACTOR
		String upperFactorAlias = createDBLookupStepForUpperAndLowerFactor(context, function, evallist, UPPERORLOWERLIMIT.UPPER,upperLimitAlias,interPolatedKeyName);
		//CREATING LOOKUP FOR UPPER FACTOR
		String lowerFactorAlias = createDBLookupStepForUpperAndLowerFactor(context, function, evallist, UPPERORLOWERLIMIT.LOWER,lowerLimitAlias,interPolatedKeyName);
		//CREATING EXPRESSION STEP FOR INTERPOLATED LOOKUP
		String returnFromInterpolated = createExpressionStepForInterPolatevalue(context,interpolatedAttrXpath,upperLimitAlias,lowerLimitAlias,upperFactorAlias,lowerFactorAlias);
		
		return returnFromInterpolated;
	}
	
	
	private static String interpolatedAttrXpath = null;
	//
	//THIS METHOD WILL CREATE THE LOOKUP FOR GETTING THE UPPER LIMIT VALUE FOR THE FACTOR
	private static String createDBLookupStepForUpperAndLowerLimit(Context context,String function,List<Evaluator> evallist,UPPERORLOWERLIMIT upperOrLower,String interPolatedKeyName)throws Exception{
				
			LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
			String lobValue = IsoConsumptionUtil.getInstance().getProperty(Configurer.getInstance().getBaseConfig().getLineOfBusiness().replaceAll(" ", ""));	
			String assignedAttributeName = null;
			String baseModelName = null;
			String aliasNameForUpper = null;
			String oerator = null;;
			
			if(UPPERORLOWERLIMIT.UPPER == upperOrLower){
				assignedAttributeName = UPPER_LIMIT_PREFIX+interPolatedKeyName;
				baseModelName = UPPER_LIMIT_PREFIX+interPolatedKeyName+lobValue+function;
				aliasNameForUpper = UPPER_LIMIT_PREFIX+interPolatedKeyName+locHolder.getNextLocVarCounter();
				oerator = "<=";
			}
			else if(UPPERORLOWERLIMIT.LOWER == upperOrLower){
				assignedAttributeName = LOWER_LIMIT_PREFIX+interPolatedKeyName;
				baseModelName = LOWER_LIMIT_PREFIX+interPolatedKeyName+lobValue+function;
				aliasNameForUpper = LOWER_LIMIT_PREFIX+interPolatedKeyName+locHolder.getNextLocVarCounter();
				oerator = ">=";
			}
			
			//String targetObjectAttribute = interPolatedKeyName;
			//CREATING RATING ENGINE LOOKUP STEP
			//function = "GL"+function;
			context.putValue(ContextParam.MODELATTRIBUTE, false);
			context.putValue(ContextParam.ISSOURCESTATICVAR, false);
			
			Map<String, Object>keyValueMap = RatingContentFileLookup.getInstance().getRatingTableKeysAndValues(function);
			@SuppressWarnings("unchecked")
			Map <Integer,String> ratetableKeyMap = (Map<Integer,String>)keyValueMap.get(Constants.LOOKUP_RATETABLE_KEYS);
			
			
			
			LookupNameDecider lookupNameDecider = (LookupNameDecider)context.getValue(ContextParam.LOOKUP_NAME_DECIDER);
			String lookupName =  lookupNameDecider.decideLookupName((String)context.getValue(ContextParam.SCOPE)+interPolatedKeyName);
			//THIS IS BASE MODEL NAME FOR LOOKUP STEP  [UPPER+INTERPOLATEKEYNAME+FUNCTION] 
			//String baseModelName = "UPPER"+interPolatedKeyName+function;
			
			
			LookupStep lookupStep = new LookupStep();
			lookupStep.setName(lookupName);
			lookupStep.setRemark(Constants.LOOKUP_REMARK);
			lookupStep.setStepType(Constants.LOOKUP_STEPTYPE);
			String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
			
			if(parentCondition != null)
			lookupStep.setParentCondition(parentCondition);
			//LOOKUPMODEL
			LookupModel globalLookupModel = new LookupModel();
			
			//This is required for global lookup and can be changed so kept in  the properties files
			globalLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
			globalLookupModel.setLookupDesc("DefaultDescription");
			
			globalLookupModel.setBaseLookUpCriteria(lookupName);
			globalLookupModel.setBaseModel(baseModelName);
			globalLookupModel.setLookupName(lookupName);
			globalLookupModel.name = lookupName;
			globalLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
			globalLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
			globalLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
			globalLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
			
			
			LookupModel localLookupModel = new LookupModel();
			localLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
			localLookupModel.setLookupDesc("DefaultDescription");
			
			localLookupModel.setBaseLookUpCriteria(lookupName);
			localLookupModel.setBaseModel(baseModelName);
			localLookupModel.setLookupName(lookupName);
			localLookupModel.name = lookupName;
			localLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
			localLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
			localLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
			localLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
			
			
			
			LookupCriteriaModel globalLookupCriteriaModel = new LookupCriteriaModel();
			LookupCriteriaModel localLookupCriteriaModel = new LookupCriteriaModel();
			
			
			FactPattern globalFactPattern = new FactPattern();
			FactPattern localLookupFactPattern = new FactPattern();
			
			int order = 1;
			
			
			if(ratetableKeyMap.size() == 0)
				lookupNameDecider.addToKeyNotFoundLookupList(function);
			int index = 0;
		
			if(ratetableKeyMap.size() == evallist.size()){
				Iterator<Map.Entry<Integer,String>> itr = ratetableKeyMap.entrySet().iterator();
				
				
				while(itr.hasNext()){
					SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
					Evaluator evaluator = evallist.get(index);
					index = index+1;
					evaluator.setContext(context);
					
					context.putValue(ContextParam.MODELATTRIBUTE, false);
					context.putValue(ContextParam.ISSOURCESTATICVAR, false);
					
					String filterLHS = evaluator.evaluate().toString();
					if(filterLHS.contains("\"")){
						filterLHS = filterLHS.replaceAll("\"", "");
					}
					//HANDLING EXPRESSION IN THE RATETABLE ARGS
					if(evaluator instanceof PlusEvaluator || evaluator instanceof MinusEvaluator || evaluator instanceof MultiplyEvaluator || evaluator instanceof DivideEvaluator){
						String expression  = filterLHS;
						ArithmaticStepCreater arithmaticStepCreater = ArithmaticStepCreater.getInstance();
						int localCounter = locHolder.getNextLocVarCounter();
						//localCounter = localCounter+1;
						filterLHS = "localVariable"+localCounter;
						ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition, filterLHS, expression, null);
						@SuppressWarnings("unchecked")
						List<AlgoStep> steps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
						steps.add(arithmeticStep);
						/*localCounter = localCounter+1;*/
						//context.putValue(ContextParam.LOCALVARIABLECOUNTER, localCounter);
						
					}
					//singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
					
					//singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_RUNTIME_VARIABLE);
					
					String keyName = itr.next().getValue();
					//singleFieldConstraintEBLeftSide.setValue(filterValue);
					singleFieldConstraintEBLeftSide.setOrderNo(order);
					
					//SETTING OPERATOR VALUE TO THE SINGLEFIELD CONSTRAINT 
					if(keyName.equals(interPolatedKeyName) ){
						singleFieldConstraintEBLeftSide.setOperator(oerator);
						interpolatedAttrXpath = filterLHS;
					}
					else{
						singleFieldConstraintEBLeftSide.setOperator(Constatnts.LOOKUP_OPERATOR);
					}
					
					
					boolean isModelAttribtue = false;
					boolean isSourceStaticValue = false;
					Object sourcestaticVal = context.getValue(ContextParam.ISSOURCESTATICVAR);
					Object modelAttr = context.getValue(ContextParam.MODELATTRIBUTE);
					
					if(sourcestaticVal!= null){
						isSourceStaticValue = (Boolean)sourcestaticVal;
					}
					if(modelAttr != null){
						isModelAttribtue = (Boolean)modelAttr;
					}
					
					
					
					if(isModelAttribtue){
						
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_M);
						//singleFieldConstraintEBLeftSide.setValue(filterLHS);
						singleFieldConstraintEBLeftSide.setFieldName(keyName);
						String sourceObject = filterLHS.replace(".", "~").split("~")[0];
						singleFieldConstraintEBLeftSide.setSourceObject(sourceObject);
						String sourceObjectAttr = filterLHS.replace(sourceObject+".", "").trim();
						singleFieldConstraintEBLeftSide.setSourceObjectAttr(sourceObjectAttr);
						singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_UNDEFINED);
						singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
						globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					}
					else if(isSourceStaticValue){
						
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_S);
						singleFieldConstraintEBLeftSide.setValue(filterLHS);
						singleFieldConstraintEBLeftSide.setFieldName(keyName);
						singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
						singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
						globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					}
					else{
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_A);
						singleFieldConstraintEBLeftSide.setValue(filterLHS);
						singleFieldConstraintEBLeftSide.setFieldName(keyName);
						singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
						localLookupFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					}
					order++;
					
					String lookupFilterID = Integer.toString(order) +lookupName; 
					String id= IdGeneration.idGenerator(lookupFilterID);
					singleFieldConstraintEBLeftSide.setId(id);
				}
			}else{
				throw new Exception("ERROR:KEYS AND VALUE LIST MISMATCHES WHILE CREATING LOOKUP STEP FOR TABLE="+function);
			}
			
			
			//if(globalFactPattern.getFieldConstraints().length>0){
				globalLookupModel.setLookupCriteriaModel(globalLookupCriteriaModel);
				globalLookupCriteriaModel.addLhsItem(globalFactPattern);
				LookupModels lookupModels = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
				//ADDING GLOBAL LOOKUP MODEL INTO THE LOOKUP MODEL LIST
				lookupModels.add(globalLookupModel);
			//}else{
				
			//}
			
			
			//context.putValue(ContextParam.GLOBALLOOKUPMODLELIST, lookupModels);
			
			//LookupModel lookupModel2 = new LookupModel();
			Attribute assignedAttribute = new Attribute();
			
			List<Attribute> assignedAttributes = new ArrayList<Attribute>();
			assignedAttributes.add(assignedAttribute);
			//SETTING VALUES TO ASSIGNED ATTRIBUTE
			//FOR INTERPOLATE FIRST LOOKUP NAME WILL BE UPPER+KEYNAME [ATTRIBUTE NAME OF THE MODEL WHICH HOLDS UPPER AND LOWER LIMITS]
			assignedAttribute.setName(assignedAttributeName);
			assignedAttribute.setAlias(aliasNameForUpper);
			assignedAttribute.setLocalIndicator(Constants.LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR);
			
			//ADDING ID TO THE RETURN ATTRIBUTE
			assignedAttribute.setId(IdGeneration.idGenerator(lookupName));
			globalLookupModel.setAssignedAttributes(assignedAttributes);
			localLookupModel.setAssignedAttributes(assignedAttributes);
			/*//the assigned attribute should be set in the lookup model
			lookupModel2.setAssignedAttributes(assignedAttributes);
			
			lookupModel2.setBaseLookUpCriteria(lookupName);
			lookupModel2.setBaseModel(function);
			lookupModel2.setLookupName(lookupName);
			lookupModel2.name = lookupName;
			//lookupStep.setLookupmodel(lookupModel2);
			 */		
			if(localLookupFactPattern.getFieldConstraints().length>0){
				localLookupCriteriaModel.addLhsItem(localLookupFactPattern);
			}
			//IPattern ip [] = lookupModel.lhs;
			//lookupModel2.setLookupCriteriaModel(localLookupCriteriaModel);
			String lookupAliasName = lookupName+aliasNameForUpper;
			String lookupId = IdGeneration.idGenerator(lookupAliasName);
			globalLookupModel.setLookuoID(lookupId);
			localLookupModel.setLookuoID(lookupId);
			
			localLookupModel.setLookupCriteriaModel(localLookupCriteriaModel);
			//lookupModel2.setLookuoID(lookupId);
			
			//Setting id for LookupCriteria 
			lookupStep.setLookupmodel(localLookupModel);
			
			//ADDING CREATED LOOKUP STEP IN THE ALGO STEPS LIST
			
			@SuppressWarnings("unchecked")
			List<AlgoStep> algoStep = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
			
			if(lookupStep != null && lookupStep instanceof LookupStep){
				algoStep.add((LookupStep)lookupStep);
			}
			//return lookupStep;
		return aliasNameForUpper;
	}
	
	
	
	/**
	 * @param context
	 * @param function
	 * @param evallist
	 * @param upperOrLower
	 * @return
	 * @throws Exception
	 */
	private static String createDBLookupStepForUpperAndLowerFactor(Context context,String function,List<Evaluator> evallist,UPPERORLOWERLIMIT upperOrLower,String returnFromLookup,String interPolatedKeyName)throws Exception{
		
		LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
		String lobValue = IsoConsumptionUtil.getInstance().getProperty(Configurer.getInstance().getBaseConfig().getLineOfBusiness().replaceAll(" ", ""));
		String baseModelName = lobValue+function;
		String aliasNameForUpper = null;
		String oerator = null;;
		
		if(UPPERORLOWERLIMIT.UPPER == upperOrLower){
			aliasNameForUpper = UPPER_LIMIT_PREFIX+locHolder.getNextLocVarCounter();
			oerator = "=";
		}
		else if(UPPERORLOWERLIMIT.LOWER == upperOrLower){
			aliasNameForUpper = LOWER_LIMIT_PREFIX+locHolder.getNextLocVarCounter();
			oerator = "=";
		}
		
		//String targetObjectAttribute = interPolatedKeyName;
		//CREATING RATING ENGINE LOOKUP STEP
		//function = "GL"+function;
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		
		Map<String, Object>keyValueMap = RatingContentFileLookup.getInstance().getRatingTableKeysAndValues(function);
		@SuppressWarnings("unchecked")
		Map <Integer,String> ratetableKeyMap = (Map<Integer,String>)keyValueMap.get(Constants.LOOKUP_RATETABLE_KEYS);
		String assignedAttributeName = keyValueMap.get(Constants.LOOKUP_RATETABLE_VALUE).toString();
		
		aliasNameForUpper = aliasNameForUpper + assignedAttributeName;
		
		LookupNameDecider lookupNameDecider = (LookupNameDecider)context.getValue(ContextParam.LOOKUP_NAME_DECIDER);
		String lookupName =  lookupNameDecider.decideLookupName((String)context.getValue(ContextParam.SCOPE)+interPolatedKeyName);
		//THIS IS BASE MODEL NAME FOR LOOKUP STEP  [UPPER+INTERPOLATEKEYNAME+FUNCTION] 
		//String baseModelName = "UPPER"+interPolatedKeyName+function;
		
		
		LookupStep lookupStep = new LookupStep();
		lookupStep.setName(lookupName);
		lookupStep.setRemark(Constants.LOOKUP_REMARK);
		lookupStep.setStepType(Constants.LOOKUP_STEPTYPE);
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		
		if(parentCondition != null)
		lookupStep.setParentCondition(parentCondition);
		//LOOKUPMODEL
		LookupModel globalLookupModel = new LookupModel();
		
		//This is required for global lookup and can be changed so kept in  the properties files
		globalLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		globalLookupModel.setLookupDesc("DefaultDescription");
		
		globalLookupModel.setBaseLookUpCriteria(lookupName);
		globalLookupModel.setBaseModel(baseModelName);
		globalLookupModel.setLookupName(lookupName);
		globalLookupModel.name = lookupName;
		globalLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
		globalLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
		globalLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		globalLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		
		
		LookupModel localLookupModel = new LookupModel();
		localLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		localLookupModel.setLookupDesc("DefaultDescription");
		
		localLookupModel.setBaseLookUpCriteria(lookupName);
		localLookupModel.setBaseModel(baseModelName);
		localLookupModel.setLookupName(lookupName);
		localLookupModel.name = lookupName;
		localLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
		localLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
		localLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		localLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		
		LookupCriteriaModel globalLookupCriteriaModel = new LookupCriteriaModel();
		LookupCriteriaModel localLookupCriteriaModel = new LookupCriteriaModel();
		
		
		FactPattern globalFactPattern = new FactPattern();
		FactPattern localLookupFactPattern = new FactPattern();
		
		int order = 1;
		
		if(ratetableKeyMap.size() == 0)
			lookupNameDecider.addToKeyNotFoundLookupList(function);
		int index = 0;
	
		if(ratetableKeyMap.size() == evallist.size()){
			Iterator<Map.Entry<Integer,String>> itr = ratetableKeyMap.entrySet().iterator();
			
			
			while(itr.hasNext()){
				SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
				Evaluator evaluator = evallist.get(index);
				index = index+1;
				evaluator.setContext(context);
				
				
				String filterLHS = evaluator.evaluate().toString();
				if(filterLHS.contains("\"")){
					filterLHS = filterLHS.replaceAll("\"", "");
				}
				//HANDLING EXPRESSION IN THE RATETABLE ARGS
				if(evaluator instanceof PlusEvaluator || evaluator instanceof MinusEvaluator || evaluator instanceof MultiplyEvaluator || evaluator instanceof DivideEvaluator){
					String expression  = filterLHS;
					ArithmaticStepCreater arithmaticStepCreater = ArithmaticStepCreater.getInstance();
					int localCounter = locHolder.getNextLocVarCounter();
					//localCounter = localCounter+1;
					filterLHS = "localVariable"+localCounter;
					ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition, filterLHS, expression, null);
					@SuppressWarnings("unchecked")
					List<AlgoStep> steps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
					steps.add(arithmeticStep);
					/*localCounter = localCounter+1;*/
					//context.putValue(ContextParam.LOCALVARIABLECOUNTER, localCounter);
					
				}
				//singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
				singleFieldConstraintEBLeftSide.setOperator(oerator);
				//singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_RUNTIME_VARIABLE);
				
				String keyName = itr.next().getValue();
				//singleFieldConstraintEBLeftSide.setValue(filterValue);
				singleFieldConstraintEBLeftSide.setOrderNo(order);
				
				boolean isModelAttribtue = false;
				boolean isSourceStaticValue = false;
				Object sourcestaticVal = context.getValue(ContextParam.ISSOURCESTATICVAR);
				Object modelAttr = context.getValue(ContextParam.MODELATTRIBUTE);
				
				if(sourcestaticVal!= null){
					isSourceStaticValue = (Boolean)sourcestaticVal;
				}
				if(modelAttr != null){
					isModelAttribtue = (Boolean)modelAttr;
				}
				
				//IF THE KEY IS INTERPOLATED THEN YOU NEED TO ADD A FILTER WITH THE RETURN FROM PREVIOUS LOOKUPS
				if(keyName.equals(interPolatedKeyName)){
					singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_A);
					singleFieldConstraintEBLeftSide.setValue(returnFromLookup);
					singleFieldConstraintEBLeftSide.setFieldName(keyName);
					singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
					localLookupFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					
				}else{
					if(isModelAttribtue){
						
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_M);
						//singleFieldConstraintEBLeftSide.setValue(filterLHS);
						singleFieldConstraintEBLeftSide.setFieldName(keyName);
						String sourceObject = filterLHS.replace(".", "~").split("~")[0];
						singleFieldConstraintEBLeftSide.setSourceObject(sourceObject);
						String sourceObjectAttr = filterLHS.replace(sourceObject+".", "").trim();
						singleFieldConstraintEBLeftSide.setSourceObjectAttr(sourceObjectAttr);
						singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_UNDEFINED);
						singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
						globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					}
					else if(isSourceStaticValue){
						
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_S);
						singleFieldConstraintEBLeftSide.setValue(filterLHS);
						singleFieldConstraintEBLeftSide.setFieldName(keyName);
						singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
						singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
						globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					}
					else{
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_A);
						singleFieldConstraintEBLeftSide.setValue(filterLHS);
						singleFieldConstraintEBLeftSide.setFieldName(keyName);
						singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
						localLookupFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
					}
				}
				
				
				order++;
				
				String lookupFilterID = Integer.toString(order) +lookupName; 
				String id= IdGeneration.idGenerator(lookupFilterID);
				singleFieldConstraintEBLeftSide.setId(id);
			}
		}else{
			throw new Exception("ERROR:KEYS AND VALUE LIST MISMATCHES WHILE CREATING LOOKUP STEP FOR TABLE="+function);
		}
		
		
			globalLookupModel.setLookupCriteriaModel(globalLookupCriteriaModel);
			globalLookupCriteriaModel.addLhsItem(globalFactPattern);
			LookupModels lookupModels = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
			//ADDING GLOBAL LOOKUP MODEL INTO THE LOOKUP MODEL LIST
			lookupModels.add(globalLookupModel);
		Attribute assignedAttribute = new Attribute();
		
		List<Attribute> assignedAttributes = new ArrayList<Attribute>();
		assignedAttributes.add(assignedAttribute);
		//SETTING VALUES TO ASSIGNED ATTRIBUTE
		//FOR INTERPOLATE FIRST LOOKUP NAME WILL BE UPPER+KEYNAME [ATTRIBUTE NAME OF THE MODEL WHICH HOLDS UPPER AND LOWER LIMITS]
		assignedAttribute.setName(assignedAttributeName);
		assignedAttribute.setAlias(aliasNameForUpper);
		assignedAttribute.setLocalIndicator(Constants.LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR);
		
		//ADDING ID TO THE RETURN ATTRIBUTE
		assignedAttribute.setId(IdGeneration.idGenerator(lookupName));
		globalLookupModel.setAssignedAttributes(assignedAttributes);
		localLookupModel.setAssignedAttributes(assignedAttributes);
		/*//the assigned attribute should be set in the lookup model
		lookupModel2.setAssignedAttributes(assignedAttributes);
		
		lookupModel2.setBaseLookUpCriteria(lookupName);
		lookupModel2.setBaseModel(function);
		lookupModel2.setLookupName(lookupName);
		lookupModel2.name = lookupName;
		//lookupStep.setLookupmodel(lookupModel2);
		 */		
		if(localLookupFactPattern.getFieldConstraints().length>0){
			localLookupCriteriaModel.addLhsItem(localLookupFactPattern);
		}
		//IPattern ip [] = lookupModel.lhs;
		//lookupModel2.setLookupCriteriaModel(localLookupCriteriaModel);
		String lookupAliasName = lookupName+aliasNameForUpper;
		String lookupId = IdGeneration.idGenerator(lookupAliasName);
		globalLookupModel.setLookuoID(lookupId);
		localLookupModel.setLookuoID(lookupId);
		
		localLookupModel.setLookupCriteriaModel(localLookupCriteriaModel);
		//lookupModel2.setLookuoID(lookupId);
		
		//Setting id for LookupCriteria 
		lookupStep.setLookupmodel(localLookupModel);
		
		//ADDING CREATED LOOKUP STEP IN THE ALGO STEPS LIST
		
		@SuppressWarnings("unchecked")
		List<AlgoStep> algoStep = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		
		if(lookupStep != null && lookupStep instanceof LookupStep){
			algoStep.add((LookupStep)lookupStep);
		}
		//return lookupStep;
	return aliasNameForUpper;
	}
	
	//THIS METHODS RETURN WILL BE THE RETURN FROM THE LOOKUP STEP
	private static String createExpressionStepForInterPolatevalue(Context context,String qualifiedPathForRateTableKey,String upperLimit,String lowerLimit,String upperFactor,String lowerFactor){
		LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
		ArithmaticStepCreater arithmaticStepCreater = ArithmaticStepCreater.getInstance();
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		String LHS = "interpolatedReturnValue"+locHolder.getNextLocVarCounter();
		String RHS = "interpolate("+qualifiedPathForRateTableKey+","+upperLimit+","+lowerLimit+","+upperFactor+","+lowerFactor+")";
		ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition,LHS,RHS,null);
		//context.putValue(ContextParam.LOCALVARIABLECOUNTER , localCounter);
		@SuppressWarnings("unchecked")
		List<AlgoStep> steps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		steps.add(arithmeticStep);
		return LHS;
	
	}
}
