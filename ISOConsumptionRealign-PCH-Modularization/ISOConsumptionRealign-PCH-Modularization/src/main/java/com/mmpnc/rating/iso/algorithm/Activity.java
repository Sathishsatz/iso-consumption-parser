package com.mmpnc.rating.iso.algorithm;

public interface Activity {

	<T> T process(T t) throws Exception;	
}
