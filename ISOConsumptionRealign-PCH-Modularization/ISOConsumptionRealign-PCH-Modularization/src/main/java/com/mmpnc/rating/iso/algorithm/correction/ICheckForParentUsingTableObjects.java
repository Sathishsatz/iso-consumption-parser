package com.mmpnc.rating.iso.algorithm.correction;

import java.util.List;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.vo.RatingHolder;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public interface ICheckForParentUsingTableObjects {
	public List<Scope> checkForParentUsingTableObjects(Scope scope,Database tableObjectsDB) throws Exception;
	public void checkAndRemoveRatingNodeWithSameName(RatingHolder ratingHolder,Database currentScopeDB,RatingHolder ratingHolderNew ) throws Exception;
}
