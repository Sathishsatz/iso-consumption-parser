package com.mmpnc.rating.iso.config;

import com.mmpnc.rating.iso.propertyreader.PropertyFileReader;
import com.mmpnc.rating.iso.propertyreader.PropertyParam;

/**
 * @author nilkanth9581
 *
 */
public class RatingCategoryConfiguration implements Configuration {
	
	private String archiveInfoJarNames;
	private String globMapReqMgr;
	private String globMapRuleRet;
	private static RatingCategoryConfiguration ratingCategoryConfiguration =null;
	
	private RatingCategoryConfiguration(){}
	
	
	/**
	 * @return
	 */
	public static RatingCategoryConfiguration getInstance(){
		if(ratingCategoryConfiguration == null){
			ratingCategoryConfiguration = new RatingCategoryConfiguration();
		} 
		return ratingCategoryConfiguration;
	}
	
	public String getArchiveInfoJarNames() {
		return archiveInfoJarNames;
	}
	public void setArchiveInfoJarNames(String archiveInfoJarNames) {
		this.archiveInfoJarNames = archiveInfoJarNames;
	}
	public String getGlobMapReqMgr() {
		return globMapReqMgr;
	}
	public void setGlobMapReqMgr(String globMapReqMgr) {
		this.globMapReqMgr = globMapReqMgr;
	}
	public String getGlobMapRuleRet() {
		return globMapRuleRet;
	}
	public void setGlobMapRuleRet(String globMapRuleRet) {
		this.globMapRuleRet = globMapRuleRet;
	}


	@Override
	public Configuration createConfiguration(String fileLocation) {
		PropertyFileReader propertyFileReader = new PropertyFileReader(fileLocation);
		RatingCategoryConfiguration raingCatConfig = new RatingCategoryConfiguration();
		
		raingCatConfig.setArchiveInfoJarNames(propertyFileReader.getProperty(PropertyParam.ARCHIVE_INFO_REQUIRED_JARS));
		raingCatConfig.setGlobMapReqMgr(propertyFileReader.getProperty(PropertyParam.GLOBAL_MAPPING_REQUEST_MANAGER));
		raingCatConfig.setGlobMapRuleRet(propertyFileReader.getProperty(PropertyParam.GLOBLA_MAPPING_RULE_RETURN));
		
		return raingCatConfig;
	}
	
}
