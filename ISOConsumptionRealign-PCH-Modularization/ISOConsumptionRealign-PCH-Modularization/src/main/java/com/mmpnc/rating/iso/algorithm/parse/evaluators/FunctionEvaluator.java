package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.customfunction.CustomFunctionFactory;
import com.mmpnc.rating.iso.algorithm.customfunction.FunctionHandler;
import com.mmpnc.rating.iso.helper.Constants;

/**
 * @author Nilkanth9581
 *
 */
public class FunctionEvaluator implements Evaluator {

	private Context context;
	private String constant;
	private String function;
	List<Evaluator> evallist;
	
	public FunctionEvaluator(String constant, String function,List<Evaluator> evallist) {
		this.constant = constant;
		this.function = function;
		this.evallist = evallist;
	}

	
	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		
		FunctionHandler functionHandler = null;
		if(Constants.RATE_TABLE.equals(constant)){
			functionHandler = CustomFunctionFactory.getInstance().getHandler(constant);
		}
		else {
			functionHandler = CustomFunctionFactory.getInstance().getHandler(function);
		}
		/*else if(Constants.ADD_YEARS.equals(function)){
			
		}*/
		
		if(functionHandler == null)
			return function;
		return functionHandler.handleCustomFunction(evallist, function, constant, context);
	}

}
