package com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch;

import com.mmpnc.rating.iso.algorithm.vo.Scope;

public class ImplAddLocPCHInPCHRearrangedAlgo implements IAddLocPCHInPCHRearrangedAlgo{

	/* (non-Javadoc)
	 * @see com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch.IAddLocPCHInPCHRearrangedAlgo#checkAndCreateNewPCHList(com.mmpnc.rating.iso.algorithm.vo.Scope, com.mmpnc.rating.iso.algorithm.vo.Scope)
	 */
	@Override
	public void checkAndCreateNewPCHList(Scope scope, Scope refScope) {
		//WE NEED TO CHECK IF THE SCOPEPCHLISTHOLDER CONTAINS THIS ENTRY FOR
		//ABOVE SCOPE AND IF IT HAS THEN NO NEED TO CREATE ENTIRY FOR IT
		ScopePCHListHolder scopePCHListHolder = ScopePCHListHolder.getInstance();
		scopePCHListHolder.createNewEntryInScopePCHListMap(scope, refScope);
	}

	@Override
	public void checkAndAddLocalPch(String variableName,String variableType,String refScopeDbTable,String orginalScopedbtable) {
		// TODO Auto-generated method stub
		ScopePCHListHolder scopePCHListHolder = ScopePCHListHolder.getInstance();
		scopePCHListHolder.addLocalVariableToRefScope(variableName, variableType, refScopeDbTable,orginalScopedbtable);
	}

	@Override
	public void deleteLocalPCHUsedFromOriginalScope() {
		ScopePCHListHolder scopePCHListHolder = ScopePCHListHolder.getInstance();
		scopePCHListHolder.deleteSplitAlgoUsedPCHFromOriginalScope();
		
	}

}
