package com.mmpnc.rating.iso.propertyreader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * @author nilkanth9581
 *
 */
public class PropertyFileReader {
	
	private Properties properties = null;
	private static final String propertiesFileName ="ISO_CONSUPTION.properties";
	private String propertiesFileLcoation = null;
	
	public PropertyFileReader(String propertiesFileLcoation){
		this.propertiesFileLcoation = propertiesFileLcoation;
		readPropertyFile();
	}
	
	/**
	 * @param propertiesFileLocation
	 */
	public PropertyFileReader readPropertyFile(){
			
		this.properties = new Properties();
			 
		try {
			
			properties.load(new FileReader(new File(propertiesFileLcoation+"\\"+propertiesFileName)));
		
		} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
					// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return this;	
	}
	
		/**
		 * @param propertyName
		 * @return
		 */
	public String getProperty(PropertyParam propertyName){
			return this.properties.getProperty(propertyName.getValue());
		}
	
	
	
}
