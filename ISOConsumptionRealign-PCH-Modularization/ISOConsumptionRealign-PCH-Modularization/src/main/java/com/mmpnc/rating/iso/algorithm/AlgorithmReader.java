package com.mmpnc.rating.iso.algorithm;

public interface AlgorithmReader {
	Object execute() throws Exception;
}
