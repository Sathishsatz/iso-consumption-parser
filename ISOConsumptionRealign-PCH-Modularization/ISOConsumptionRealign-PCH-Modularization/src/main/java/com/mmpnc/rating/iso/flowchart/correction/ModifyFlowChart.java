package com.mmpnc.rating.iso.flowchart.correction;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Premium;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

/**
 * @author nilkanth9581
 *
 */
public class ModifyFlowChart implements IModifyFlowChart {

	/**
	 * this map will be used to keep ClassInfo of all classes in the flow chart
	 */
	private Map<String, ClassInfo> map = null;

	private ModifyFlowChart() {
		map = new ConcurrentHashMap<String, ClassInfo>();
	}

	public static ModifyFlowChart getInstance(){
		return new ModifyFlowChart();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mmpnc.rating.iso.flowchart.correction.IModifyFlowChart#modifyFlowChart
	 * (com.mmpnc.rating.iso.algorithm.vo.LOB)
	 */
	@Override
	public void modifyFlowChart(LOB lob) {
		for (Object object : lob.getContent()) {
			if (object instanceof Reference) {
				Reference reference = (Reference) object;
				if (reference.getName().equals("FlowChart")) {
					List<Object> content = reference.getContent();
					for (Object classObj : content) {
						if (classObj instanceof Class) {
							Class cl = (Class) classObj;
							readClass(cl);
							
						}
						
					}
					
				}
				
			}
		}
	}

	/**
	 * @param classs
	 *            This method reads the class node of flowchart and creates
	 *            ClasInfo object for premium as well as rating nodes
	 * @throws ParserException
	 * 
	 */
	private void readClass(Class classs) {
		List<Object> list = classs.getContent();
		StringBuffer ratingVars = new StringBuffer();
		StringBuffer premiumVar = new StringBuffer();
		int premiumNodeCount = 0;
		for (Object object : list) {

			if (object instanceof Class) {
				Class classNew = (Class) object;
				String classKey = classs.getName() + classs.getType()
						+ classs.getPass();
				if (!map.containsKey(classKey)) {
					map.put(classKey, new ClassInfo());
				}
				readClass(classNew);
			}
			if (object instanceof Premium) {
				premiumNodeCount = premiumNodeCount + 1;
				// if there are more than one premium node for the class then
				// thats an erroneous condition as
				// we will not be able to find which variables need to be
				// executed first
				if (premiumNodeCount > 1) {
					ExceptionHandler
							.raiseFlowchartException("IN FLOWCHART TWO PREMIUM NODES FOUND FOR SAME CLASS"
									+ classs.getName());
				}
				
				Premium premium = (Premium) object;
				String classKey = classs.getName() + classs.getType()
						+ classs.getPass();
				if (map.containsKey(classKey)) {
					ClassInfo classInfo = map.get(classKey);
					if (classInfo.getRating() == null)
						classInfo.setPremium(premium.getVariables());
					else
						classInfo.setPremium(classInfo.getPremium().concat(",")
								.concat(premium.getVariables()));
				}
				premiumVar.append(premium.getVariables());
			}
			if (object instanceof Rating) {
				Rating rating = (Rating) object;
				String classKey = classs.getName() + classs.getType()
						+ classs.getPass();
				if (map.containsKey(classKey)) {
					ClassInfo classInfo = map.get(classKey);
					if (classInfo.getRating() == null)
						classInfo.setRating(rating.getVariables());
					else
						classInfo.setRating(classInfo.getRating().concat(",")
								.concat(rating.getVariables()));
				}
				ratingVars.append(rating.getVariables());
			}
		}

		// Now modifying flowchart
		modifyClassInFlowChart(ratingVars, premiumVar, classs);
	}

	/**
	 * @param ratingVars
	 * @param premiumVars
	 * @param classs
	 *            This method will modify class object of the flow chart
	 *            depending
	 */
	private void modifyClassInFlowChart(StringBuffer ratingVars,
			StringBuffer premiumVars, Class classs) {

		// case1: when we have both rating and premium node then add premium
		// node variables at
		// the end of the rating node variables
		if (ratingVars.length() > 1 && premiumVars.length() > 1) {
			// now adding functionality for modifying
			// 1- if a class has both rating and premium nodes
			if (ratingVars.length() > 1 && premiumVars.length() > 1) {
				List<Object> content = classs.getContent();
				for (Object cont : content) {
					if (cont instanceof Rating) {
						Rating rating = (Rating) cont;
						rating.setVariables(rating.getVariables().concat(",")
								.concat(premiumVars.toString()));
					}
				}
			}
		}
		// case 2: When there isn't any rating node but a premium node then we
		// convert that premium node into
		// rating node
		else if (ratingVars.length() == 0 && premiumVars.length() > 1) {
			List<Object> content = classs.getContent();
			Rating rating = null;
			for (Object cont : content) {
				if (cont instanceof Premium) {
					Premium premium = (Premium) cont;
					// creating a rating node and adding it to the class
					rating = new Rating();
					rating.setClassName(premium.getClassName());
					rating.setType(premium.getType());
					rating.setVariables(premium.getVariables());
					// content.add(rating);
				}
			}
			classs.getContent().add(rating);

		}
		// case 3: if both nodes are not present then we can keep class as it is
		// case 4: if there is only a rating node then we will keep as it is so
		// that our current functionality will work
		// without any changes

	}

	/**
	 * Inner class used to hold rating and premium node for a class
	 * 
	 */
	class ClassInfo {
		String rating;
		String premium;

		public String getRating() {
			return rating;
		}

		public void setRating(String rating) {
			this.rating = rating;
		}

		public String getPremium() {
			return premium;
		}

		public void setPremium(String premium) {
			this.premium = premium;
		}

	}

}
