package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class NotBooleanEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private Evaluator bool;
	public NotBooleanEvaluator(Evaluator bool) {
		this.bool = bool;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		
		validate();
		
//		System.out.println("Not Boolean Evaluator called");
		this.bool.setContext(context);
		this.bool.evaluate();
		
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append(" NOT (");
		
		this.bool.setContext(context);
		returnValue.append(this.bool.evaluate()).append(") ");
		
		return returnValue;
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.bool== null){
			throw new ParserException("Boolean evaluator is NULL in NOT evaluator");
		}
		
	}

}
