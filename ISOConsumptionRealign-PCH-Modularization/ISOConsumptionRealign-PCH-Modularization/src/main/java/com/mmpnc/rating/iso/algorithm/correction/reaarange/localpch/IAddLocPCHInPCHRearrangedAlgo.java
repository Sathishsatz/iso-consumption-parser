package com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch;

import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public interface IAddLocPCHInPCHRearrangedAlgo {
	public void checkAndCreateNewPCHList(Scope scope,Scope refScope);
	public void checkAndAddLocalPch(String variableName,String variableType,String refScopeDbTable,String orginalScopedbtable);
	
	/**
	 * @param refScopeDbTable
	 * @param orginalScopedbtable
	 * THIS METHOD WILL DELETE PCH FROM ORIGINAL SCOPE WHICH ARE REFERENCED FROM THE PCH SPLIT ALGORITHMS
	 * THIS HAS BEEN IMPLEMENTED AFTER OBSERVING THE ISSUES IN BOP-DE-CW ALGORITHMS IN BOPUtilitySrvcsTimeElement
	 * SCOPE WITH LOCAL VARIABLES parentWaterSupply 
	 *  
	 *  THIS METHOD SHOULD BE EXECUTED AT THE LEVEL OF LOB AND NOT AT SCOPE
	 */
	public void deleteLocalPCHUsedFromOriginalScope();
	
}
