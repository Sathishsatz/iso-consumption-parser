package com.mmpnc.rating.connection.db.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mmpnc.rating.connection.db.helper.DBConnection;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.config.DBConfiguration;

/**
 * @author nilkanth9581
 *
 */
public class OracleDbConnection implements DBConnection{

	private ThreadLocal<Connection> localConnection = new ThreadLocal<Connection>();
	private static OracleDbConnection oracleDbConnection = null;
	
	private OracleDbConnection(){
		
	}
	/**
	 * @return
	 */
	public static OracleDbConnection getInstance(){
		if (oracleDbConnection == null){
			oracleDbConnection = new OracleDbConnection();
		}
		return oracleDbConnection;
	}
	
	/**
	 * @return
	 */
	public Connection openConnectionToDB() {
		DBConfiguration dbConfiguration = Configurer.getInstance().getDBConfig();
		try {
			Class.forName(dbConfiguration.getDatabaseDriverName());
			Connection connection = DriverManager.getConnection(dbConfiguration.getDbUrl(), 
			    	dbConfiguration.getDatabaseUser(), dbConfiguration.getDatabasePassword());
			if(connection==null){
				throw new RuntimeException("Could not connect to the database.");
			}
			connection.setAutoCommit(false);
			localConnection.set(connection);
			
		} catch (ClassNotFoundException e) {
			ISOConsumptionLogger.error(e);
			e.printStackTrace();
		} catch (SQLException e) {
			ISOConsumptionLogger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public Connection getConnection(){
		openConnectionToDB();
		if (localConnection.get() == null){
			throw new RuntimeException("Connection is not open !!!");
		}
		return localConnection.get();
	}
	
	public void commitAndClose(){
		try {
			localConnection.get().commit();
			localConnection.get().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*public static void main(String[] args)throws Exception {
		Connection connection  = new OracleDbConnection().openConnectionToDB();
		ISOConsumptionLogger.info(connection.getAutoCommit());
	}*/

}
