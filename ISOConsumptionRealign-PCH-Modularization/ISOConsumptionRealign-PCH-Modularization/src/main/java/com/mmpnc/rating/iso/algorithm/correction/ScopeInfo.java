package com.mmpnc.rating.iso.algorithm.correction;

import java.util.List;

public class ScopeInfo {
	private String scopeName;
	private int sequenceNo;
	private int pass;
	private List <String> pchExecSeqInRatingNode;
	private List<String> intermediaryUsedPCH;
	private List<String> unUsedPCH;
	
	public List<String> getIntermediaryUsedPCH() {
		return intermediaryUsedPCH;
	}
	public void setIntermediaryUsedPCH(List<String> intermediaryUsedPCH) {
		this.intermediaryUsedPCH = intermediaryUsedPCH;
	}
	public List<String> getUnUsedPCH() {
		return unUsedPCH;
	}
	public void setUnUsedPCH(List<String> unUsedPCH) {
		this.unUsedPCH = unUsedPCH;
	}
	public String getScopeName() {
		return scopeName;
	}
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	public int getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public int getPass() {
		return pass;
	}
	public void setPass(int pass) {
		this.pass = pass;
	}
	public List<String> getPchExecSeq() {
		return pchExecSeqInRatingNode;
	}
	public void setPchExecSeq(List<String> pchExecSeq) {
		this.pchExecSeqInRatingNode = pchExecSeq;
	}
	
}
