package com.mmpnc.rating.iso.algorithm.correction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.ATTACH;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Premium;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Rating;
import com.mmpnc.rating.iso.algorithm.vo.RatingHolder;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.util.VariableType;
import com.thoughtworks.xstream.XStream;

/**
 * @author Nilkanth9581
 * 
 */
enum SCOPEHOLDERMAP {
	SCOPELIST, DISTINCTPCHINSCOPE
}

public class CorrectionAccordingToExecutionFlow {

	// private Database flowChartDb ;
	// @TODO
	private static CorrectionAccordingToExecutionFlow corrExecutionFlow;

	// private Map<String,PCHInfo> processedPCHMap ;

	private CorrectionAccordingToExecutionFlow() {

	}

	public static CorrectionAccordingToExecutionFlow getInstance() {
		if (corrExecutionFlow == null)
			corrExecutionFlow = new CorrectionAccordingToExecutionFlow();
		return corrExecutionFlow;
	}

	public LOB rearrageLOBUsingFlowChart(LOB lob) throws Exception {
		StringBuffer nonProcessedScopes = new StringBuffer();
		ISOConsumptionLogger
				.info("************************* Starting Rearranging LOB using FlowChart ************");
		Map<String, PCHInfo> processedPCHMap = new HashMap<String, PCHInfo>();
		cosolidatedReportList = new ArrayList<String>();
		// Database flowChartDatabase = createDataBaseForFlowChart(lob);
		Map<BasexDatabaseNames, Database> databaseMap = BasexDatabaseCreater
				.createDataBaseForFlowChartAndTableObjects(lob);
		Database flowChartDatabase = databaseMap
				.get(BasexDatabaseNames.RUNTIMEFLOWCHARTDB);
		Database tableObjectsDatabase = databaseMap
				.get(BasexDatabaseNames.RUNTIMETABLEOBJECTSDB);
		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				Reference reference = (Reference) ref;
				// CHECKING REFERENCE FOR COMMON RATING OR PREMIUMN CALCULATION
				if ("Common Rating".equals(reference.getType())
						|| "Premium Calculation".equals(reference.getType())) {
					readReference(processedPCHMap, (Reference) ref,
							flowChartDatabase, nonProcessedScopes,
							tableObjectsDatabase);
				}

			}
		}
		flowChartDatabase.closeDatabase();
		tableObjectsDatabase.closeDatabase();
		// PRINTING THE LOG FOR OWN REFERENCE
		PrintFlowChartSplitReport.printReport();
		// WRTING COMPLETE REPORT
		return lob;
	}

	private void readReference(Map<String, PCHInfo> processedPCHMap,
			Reference reference, Database flowChartDB,
			StringBuffer nonProcessedScopes, Database tableObjectsDb)
			throws Exception {

		List<Object> scopeList = new ArrayList<Object>();
		scopeList.addAll(reference.getContent());
		for (Object scope : scopeList) {
			if (scope instanceof Scope) {

				Scope curScope = (Scope) scope;
				// ADDING CHECK FOR PCH NAMES WITH ANCESTOR XPATH IN IT
				// handleAncestorPCHName(curScope);
				// int indexOfScope = scopeList.indexOf(curScope);
				// reference.getContent().remove(curScope);
				// CREATING BASEX DATABSE FOR THE CURRENT SCOPE
				Database currentScopeDB = BasexDatabaseCreater
						.createCurrentScopeDtabase(curScope);
				// THIS HANDLING IS ADDED TO CHECK IF THE CURRENT ALGORITHM IS
				// PRESENT AT DIFFERENT LEVELS
				// IF SO THEN THE CURRENT SCOPE WILL SPLIT INTO NUMBER OF SCOPES
				// AND THEN SENT TO PROCESS
				// THIS IS ADDED TO CHECK THE PARENT FOR SAME ALGORITHM AT
				// DIFFERENT LEVEL
				beforReadingScopeCheckForMultipleLevelAlgo(processedPCHMap,
						reference, (Scope) scope, flowChartDB, currentScopeDB,
						nonProcessedScopes, tableObjectsDb);
				// ORIGINAL CODE WITHOUT SAME ALGO AT DIFF LEVEL HANDLING
				// readScope(reference,(Scope)scope,null,flowChartDB,currentScopeDB,nonProcessedScopes);

				// CLOSING DATABASE AS THE CURRENT SCOPE ALGORITHM SPLIT IS
				// COMPLETE
				// closeDatabase(currentScopeDB,"CURRENTSCOPE");
				currentScopeDB.closeDatabase();
			}
		}
	}

	List<String> cosolidatedReportList = null;

	/**
	 * @param database
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	private Object createQueryAndReturnResult(Database database,
			String queryString) throws Exception {
		Connection connection = new XMLConnection(database);
		Query query = new XmlQuery(connection);
		query.createQuery(queryString);
		Object returnObj = query.execute();
		return returnObj;
	}

	/**
	 * @param sequenceNo
	 * @param currentScopeDB
	 * @param pchArryList
	 *            THIS METHOD WILL GET ALL THE PCH NAMES HAVING / IN IT AS THESE
	 *            VARIABLE NAMES WILL NOT BE IN THE VARIABLE LIST
	 * 
	 */
	private Map<String, PCH> pchWithAncestor = new HashMap<String, PCH>();

	/**
	 * @param ratingHolderStr
	 * @param scope
	 * @param flowChartDB
	 * @return CREATE AND RETURN RATINGHOLDER OBJECT FOR THE RATING NODES IN
	 *         FLOWCHART FOR THE CURRENT SCOPE DBTABLE
	 * @throws Exception
	 */
	private RatingHolder createAndReturnRatingHolderObject(Scope scope,
			Database flowChartDB, String referenceType) throws Exception {
		StringBuffer queryString = new StringBuffer();
		// class[@name='BOP']/rating
		if (scope.getDbTables().contains("/"))
			createQueryStringForAlgosAtDiffLevle(scope, queryString,
					referenceType);
		else {
			queryString.append("//class[@name='").append(scope.getDbTables())
					.append("']/rating[@type='" + referenceType + "']");
		}

		Object returnObj = "<RatingHolder>"
				+ createQueryAndReturnResult(flowChartDB,
						queryString.toString()) + "</RatingHolder>";
		// CONVERTING STRING INTO OBJECT AGAIN USING XSTREAM FOR THIS
		XStream xstream = new XStream();
		xstream.registerConverter(new RatingObjectConverter());
		xstream.alias("RatingHolder", RatingHolder.class);
		xstream.alias("Rating", Rating.class);
		xstream.addImplicitCollection(RatingHolder.class, "rating",
				Rating.class);

		/*
		 * xstream.useAttributeFor("variable", String.class);
		 * xstream.useAttributeFor("rating", String.class);
		 */

		RatingHolder ratingHolder = (RatingHolder) xstream.fromXML(returnObj
				.toString());

		return ratingHolder;

	}

	/**
	 * @param scope
	 * @param queryString
	 */
	private void createQueryStringForAlgosAtDiffLevle(Scope scope,
			StringBuffer queryString, String referenceType) {
		String dbTable[] = scope.getDbTables().split("/");
		queryString.append("//");
		for (String dbTablename : dbTable) {
			queryString.append("class[@name='");
			queryString.append(dbTablename);
			queryString.append("']");
			queryString.append("/");
		}
		queryString.append("rating[@type='" + referenceType + "']");

	}

	/**
	 * @param scope
	 * @param flowChartDB
	 * @param currentScopeDB
	 * @return THIS METHOD WILL SPLIT SCOPE INTO MULTIPLE SCOPES IF ALGROTIHM
	 *         EXECUTES LIST OF PCH AT DIFFERENT LEVELS
	 * @throws Exception
	 */
	private List<Scope> splitScopeUsingFlowChart(
			Map<String, PCHInfo> processedPCHMap, Scope scope,
			Database flowChartDB, Database currentScopeDB,
			StringBuffer nonProcessedScopes, String referenceType)
			throws Exception {

		/*
		 * xstream.useAttributeFor("variable", String.class);
		 * xstream.useAttributeFor("rating", String.class);
		 */

		RatingHolder ratingHolder = createAndReturnRatingHolderObject(scope,
				flowChartDB, referenceType);

		// CREATING NEW RATINGHOLDER AND LOOPING THROUGH IT TO AVOID CONCURRANCY
		// ERROR
		RatingHolder ratingHolderNew = new RatingHolder();
		ratingHolderNew.setRating(new ArrayList<Rating>());
		ratingHolderNew.setPremium(new ArrayList<Premium>());

		ICheckForParentUsingTableObjects iCheckForParentUsingTableObjects = new CheckForParentUsingTableObjects();
		// CHECKING IF THERE ARE TWO NODES WITH SAME NAME
		iCheckForParentUsingTableObjects.checkAndRemoveRatingNodeWithSameName(
				ratingHolder, currentScopeDB, ratingHolderNew);
		
		//THIS CODE ADDED BECAUSE IF THERE IS A SINGLE COVERAGE WITH SINGLE RATING NODE THEN WE NEED
		//TO ADD THE SEQUENCE OF PCH FROM THAT RATING NODE
		//EX:GeneralLiabilitySpecialProtectiveHighwayPremiumToReachMinCoverage-PC
		//circular:GeneralLiabilitySpecialProtectiveHighwayPremiumToReachMinCoverage
		/*if (ratingHolder.getRating() != null
				&& ratingHolder.getRating().size() == 1
				&& ratingHolderNew.getRating().size() == 0) {
			ratingHolderNew.getRating().add(ratingHolder.getRating().get(0));
		}*/

		List<Scope> newScopeList = new ArrayList<Scope>();
		int sequenceNo = 0;
		/*
		 * RatingHolder ratingHolderNew1 = new RatingHolder();
		 * ratingHolderNew1.setRating(ratingHolderNew.getRating());
		 */

		if (ratingHolderNew.getRating() != null
				&& ratingHolderNew.getRating().size() != 0) {

			for (Rating ratingObj : ratingHolderNew.getRating()) {
				if (ratingObj instanceof Rating) {
					// creating pchinfo object one each for split scopes
					PCHInfo pchInfo = new PCHInfo();
					// CREATING SEPERATE SCOPE FOR EVERY RATING NODE UNDER THE
					// CLASS
					Scope newScope = new Scope();
					newScope.setDbTables(scope.getDbTables());
					newScope.setSequenceNo(++sequenceNo);
					newScope.setPass(scope.getPass());
					newScope.setRecordType(scope.getRecordType());

					Rating rating = (Rating) ratingObj;
					String variable = rating.getVariables();
					String pchArr[] = null;
					if (variable.contains(",")) {
						pchArr = variable.split(",");
					} else {
						pchArr = new String[] { variable };
					}
					List<String> pchNameListOfRatingNodes = Arrays
							.asList(pchArr);
					List<String> pchArryList = new ArrayList<String>();
					pchArryList.addAll(pchNameListOfRatingNodes);
					// ADDING PCH TO ARRAY WHICH CONTAINS / IN IT WE NEED TO ADD
					// THIS PCH ONLY IN ONE SCOPE
					// NO NEED TO ADD THIS IN MULTIPLE SCOPES SO CHECKING THE
					// SQUENCE AND IF IT IS O THEN ADDING THESE PCHS
					PCHRelatedOpsHelper.addPCHWithNameContainingSlash(
							sequenceNo, currentScopeDB, pchArryList,
							pchWithAncestor);
					// CREATING NEW PCHNAMELIST AND ADDING TO THE PROCESSED
					// PCHMAP
					// THIS HAS BEEN ADDED BSE IF SAME PCH REFERENCED BY
					// MULTIPLE SPLITTED SCOPE
					// List<String> pchNameListForScope = new
					// ArrayList<String>();

					putInProcessedPCHMap(processedPCHMap, referenceType,
							scope.getDbTables() + sequenceNo + scope.getPass(),
							pchInfo);
					// processedPCHMap.put(,pchInfo);
					// GETTING ALL THE PCH DECLARED IN THE VARAIBLES AND ADDING
					// THOSE IN NEWLY CREATE SCOPE OBJECT

					PCHRelatedOpsHelper.getPCHFromScopeAndAddTonewScope(
							referenceType, pchWithAncestor, processedPCHMap,
							scope, pchArryList, currentScopeDB, sequenceNo,
							newScope);

					// ADDING NEWLY CREATED SCOPE TO THE SCOPE LIST
					newScopeList.add(newScope);

				}
			}
			// ADDING OUT OF PCH STATEMENTS TO THE FIRST SPLIT SCOPE
			//
			addOutOfPCHStatementsInFirstSplitAlgorithm(processedPCHMap,
					referenceType, scope, newScopeList.get(0));

			ISOConsumptionLogger.info("SPLIT FOR ALGORITHM =["
					+ scope.getDbTables()
					+ "] NUMBER OF NEW ALGORITHMS CREATED =[" + sequenceNo
					+ "]");
		} else {
			/*
			 * List<String> pchArryList = new ArrayList<String>();
			 * pchArryList.addAll(pchNameListOfRatingNodes); //ADDING PCH TO
			 * ARRAY WHICH CONTAINS / IN IT WE NEED TO ADD THIS PCH ONLY IN ONE
			 * SCOPE //NO NEED TO ADD THIS IN MULTIPLE SCOPES SO CHECKING THE
			 * SQUENCE AND IF IT IS O THEN ADDING THESE PCHS
			 */
			// TODO needs to check this approach once pch with xpath in its name
			// is decided
			PCHRelatedOpsHelper.addPCHWithNameContainingSlash(currentScopeDB,
					pchWithAncestor);
			// CHECKING IF THE
			// ENLISINTG SCOPE NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART
			//OR HAVE JUST SINGLE RATING NODE IN THE FLOW CHART
			addOriginalScopeToNewScopeList(processedPCHMap, referenceType,
					nonProcessedScopes, scope, newScopeList, sequenceNo,
					currentScopeDB,ratingHolder);
			// IF THE SCOPE DO NOT HAVE ANY PCH THEN ALL THE OUT OF PCH
			// STATEMENTS WILL BE INCLUDED IN THE ORIGINAL SCOPE
			// addOutOfPCHStatementsInFirstSplitAlgorithm(processedPCHMap,referenceType,scope,
			// scope);
		}

		return newScopeList;
	}

	/**
	 * @param scope
	 * @param firstSplitScope
	 *            THIS METHOD WILL ADD OUT OF PCH STATEMENTS TO THE FIRST SPLIT
	 *            ALGORITHM
	 * @throws ParserException
	 * 
	 */
	private void addOutOfPCHStatementsInFirstSplitAlgorithm(
			Map<String, PCHInfo> processedPCHMap, String referenceType,
			Scope scope, Scope firstSplitScope) throws ParserException {

		ICorrectionForPCHModularization correction = new CorrectionForPCHMoularizationImpl();
		PCH pch = correction.createPremiumPCHAndAddOutOFPCHStatements(scope);
		if (pch != null) {
			firstSplitScope.getContent().add(pch);
			// ADDING THIS PCH TO THE MAP
			// processedPCHMap.get(firstSplitScope.getDbTables()+firstSplitScope.getSequenceNo()+firstSplitScope.getPass()).adToUnusedPCHList(pch.getName());
			getFromProcessedPCHMap(
					processedPCHMap,
					referenceType,
					firstSplitScope.getDbTables()
							+ firstSplitScope.getSequenceNo()
							+ firstSplitScope.getPass()).adToUnusedPCHList(
					pch.getName());
		}

	}

	/**
	 * WHEN YOU ARE ADDING ORIGINAL SCOPE AS IT IS AS FLOWCHART DO NOT HAVE
	 * RATING NODE FOR THIS SCOPE FOLLOWING THINGS REQUIRED 1- CHECK FOR OUT OF
	 * PCH STATEMENTS AND IF ANY CREATE A NEW PREMIUM PCH AND ADD THOSE
	 * STATEMENTS IN THAT PCH 2- NEED TO UPDATE PROCESSEDPCHMAP WHITH ALL THE
	 * PCH AVAILABE IN THAT SCOPE
	 * 
	 * ONE THING TO NOTE IS IF THE SCOPE HAS OUT OF PCH STATEMENTS THEN PREMIUM
	 * PCH WILL BE INCLUDED AT THE END OF THE SCOPE
	 * 
	 * ALSO IF CLASS HAS A SINGLE RATING NODE THEN YOU NEED TO TAKE CARE OF PCH EXECUTION
	 * SEQUENCE
	 * 
	 */
	/**
	 * @param nonProcessedScopes
	 * @param scope
	 * @param newScopeList
	 * @param sequenceNo
	 * @param currentScopeDB
	 * @throws ParserException
	 */
	private void addOriginalScopeToNewScopeList(
			Map<String, PCHInfo> processedPCHMap, String referenceType,
			StringBuffer nonProcessedScopes, Scope scope,
			List<Scope> newScopeList, int sequenceNo, Database currentScopeDB,RatingHolder ratingHolder)
			throws ParserException {
		// ENLISINTG SCOPE NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART
		nonProcessedScopes
				.append("ALGORITHM NAMES WHICH DO NOT HAVE RATING NODE IN FLOWCHART =[");
		nonProcessedScopes.append(scope.getDbTables());
		nonProcessedScopes.append("]");
		nonProcessedScopes.append("\n");

		// CREATING NEW SCOPE AND ADDING ALL THE PCH FROM ORIGINAL SCOPE TO NEW
		// SCOPE
		// THIS IS IMPORTANT AS OUT OF PCH STATEMENTS WILL STILL BE THERE IN THE
		// SCOPE
		Scope newScope = new Scope();
		if (!(scope.getSequenceNo() == 1 || scope.getSequenceNo() > 1)) {
			newScope.setSequenceNo(++sequenceNo);
		} else {
			newScope.setSequenceNo(scope.getSequenceNo());
		}

		newScope.setDbTables(scope.getDbTables());
		newScope.setFullyQualifiedPath(scope.getFullyQualifiedPath());
		newScope.setPass(scope.getPass());
		newScope.setRuleReference(scope.getRuleReference());
		newScope.setRecordType(scope.getRecordType());

		// Creating a PCHInfo object for
		String mapKey = newScope.getDbTables() + newScope.getSequenceNo()
				+ newScope.getPass();
		PCHInfo pchInfo = new PCHInfo();
		// processedPCHMap.put(mapKey, pchInfo);
		putInProcessedPCHMap(processedPCHMap, referenceType, mapKey, pchInfo);
		Connection curScopeCon = new XMLConnection(currentScopeDB);
		Query query = new XmlQuery(curScopeCon);
		StringBuffer queryString = new StringBuffer();

		// THIS FIRST FINDS PCH IN THE SCOPE THEN IT GETS ALL THE PCH FROM THE
		// SCOPE AND THEN CHECK IF THE SCOPE INCLUDES
		// OUT OF PCH STATEMENTS

		queryString
				.append("string-join(distinct-values(//PCH/@name/data()),',')");

		query.createQuery(queryString.toString());
		Object returnPCHNames = query.execute();
		if (returnPCHNames == null) {
			throw new ParserException(
					"EMPTY SCOPE FOUND IN ALGORIHM FILE PLEASE CHECK"+scope.getDbTables());
		}
		//IF THE CLASS HAS SINGLE RATING NODE THEN ADDING THAT NODE IN THE PCHINFO IN EXEC SEQ
		//UPDATING PCH INFO GET PCHLIST IN RATING NODE LIST ACCORDING TO THE RATING NODE IN 
		//ALGORITHM FLOW CHART
		if(ratingHolder.getRating() != null && ratingHolder.getRating().size() == 1){
			String variablesSeq = ratingHolder.getRating().get(0).getVariables();
			for(String str : variablesSeq.split(",")){
				pchInfo.getPchListInRatingNode().add(str);
			}
		}
			
		
		// THIS CONDITION IS ADDED TO EXCLUE THOSE SCOPES WHICH DO NOT HAVE A
		// SINGLE PCH IN IT
		// BOP-DE-CW has BOPLimitedFungiBacteriaCov coverage but all the pch in
		// this algorithm has / in its name
		// and executes at other levels
		if (returnPCHNames.toString().length() == 0) {
			System.err.println("WARNING:SOCPE :[" + scope.getDbTables()
					+ "] DO NOT HAVE ANY PCH IN IT ");
			// IF A SCOPE DO NOT HAVE ANY PCH THEN WE HAVE TO CHECK WHTHER THAT
			// SCOPE HAS OUT OF PCH STATEMENTS
			addOutOfPCHStatementsInFirstSplitAlgorithm(processedPCHMap,
					referenceType, scope, newScope);
			// ADDIGN SEQUENCE NUMBER TO THE NON PROCESSED SCOPE
			newScopeList.add(newScope);
			return;
		}
		String[] pchNameArr = returnPCHNames.toString().split(",");

		// CHECKING WHETHER THE PCH NAME ARRAY HAS ANY DUPLICATE ENTIRY IF SO WE
		// NEED TO REMOVE DUPLICATE ELEMENTS
		// IN PCH NAME ARRAY
		// pchNameArr = checkDuplicatePCHNames(pchNameArr);
		
		for (String pchName : pchNameArr) {
			if(pchInfo.getPchListInRatingNode() != null && pchInfo.getPchListInRatingNode().size() == 0)
			pchInfo.getUnusedPCH().add(pchName);
			StringBuffer queryForPCH = new StringBuffer();
			queryForPCH.append("//PCH[@name='" + pchName.trim() + "'][1]");
			query.createQuery(queryForPCH.toString());
			Object returnPCH = query.execute();

			if (returnPCH != null && !returnPCH.equals("")) {
				try {
					Object pchObj = JAXBUtils.readFromSource(
							returnPCH.toString(), PCH.class);
					PCH pch = (PCH) pchObj;
					newScope.getContent().add(pch);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// TODO need to check this once the strategy for pch name with xpath
			// is decided
			else if (pchName.contains("ancestor")) {
				Object pchObject = pchWithAncestor.get(pchName);
				PCH pch = (PCH) pchObject;
				newScope.getContent().add(pch);

			} else {
				ExceptionHandler.raiseParserException("NOT ABLE TO GET PCH ["
						+ pchName + "] FROM SCOPE:" + scope.getDbTables());
			}

		}

		// CHECKING OUT OF PCH STATEMENTS AND CREATING A NEW PREMIUM PCH AND
		// ADDING THEM TO THAT PCH
		addOutOfPCHStatementsInFirstSplitAlgorithm(processedPCHMap,
				referenceType, scope, newScope);
		// ADDIGN SEQUENCE NUMBER TO THE NON PROCESSED SCOPE
		newScopeList.add(newScope);
	}

	/**
	 * @param reference
	 * @param scope
	 * @param flowChartDB
	 * @param currentScopeDB
	 * @param nonProcessedScopes
	 * @throws Exception
	 */
	private void beforReadingScopeCheckForMultipleLevelAlgo(
			Map<String, PCHInfo> processedPCHMap, Reference reference,
			Scope scope, Database flowChartDB, Database currentScopeDB,
			StringBuffer nonProcessedScopes, Database tableObjectDB)
			throws Exception {
		ICheckForParentUsingTableObjects checkForParentUsingTableObjects = new CheckForParentUsingTableObjects();
		List<Scope> scopeAtDiffLevelList = checkForParentUsingTableObjects
				.checkForParentUsingTableObjects((Scope) scope, tableObjectDB);
		if (scopeAtDiffLevelList.size() > 1) {
			for (Scope scopes : scopeAtDiffLevelList) {
				readScope(processedPCHMap, reference, scope, scopes,
						flowChartDB, currentScopeDB, nonProcessedScopes);
			}
		} else {
			readScope(processedPCHMap, reference, scope, null, flowChartDB,
					currentScopeDB, nonProcessedScopes);
		}

	}

	/**
	 * @param reference
	 * @param scope
	 * @param withParentScope
	 * @param flowChartDB
	 * @param currentScopeDB
	 * @param nonProcessedScopes
	 * @throws Exception
	 */
	private void readScope(Map<String, PCHInfo> processedPCHMap,
			Reference reference, Scope scope, Scope withParentScope,
			Database flowChartDB, Database currentScopeDB,
			StringBuffer nonProcessedScopes) throws Exception {

		List<Scope> newScopeList = null;
		// Scope originalScope = scope;
		if (withParentScope != null) {
			newScopeList = splitScopeUsingFlowChart(processedPCHMap,
					withParentScope, flowChartDB, currentScopeDB,
					nonProcessedScopes, reference.getType());
		} else {
			newScopeList = splitScopeUsingFlowChart(processedPCHMap, scope,
					flowChartDB, currentScopeDB, nonProcessedScopes,
					reference.getType());
		}
		String referenceType = reference.getType();
		// CHECKING WHETHER SCOPE IS SPLITT INTO MULTIPLE ALGORITHMS
		if (newScopeList.size() > 1) {
			// AS WE NEED TO SPLIT THE SCOPE REMOVING SCOPE FROM THE REFERENCE
			reference.getContent().remove(scope);
			// int originanNumberOfPCH = 0;
			// int numberOFPCHAfterProcess = 0;

			for (Scope newScope : newScopeList) {
				List<Object> pchList = new ArrayList<Object>();
				pchList.addAll(newScope.getContent());

				// originanNumberOfPCH = pchList.size();
				for (Object obj : pchList) {
					if (obj instanceof PCH) {
						readPCH(processedPCHMap, referenceType, (PCH) obj,
								currentScopeDB, newScope);
					} else if (obj instanceof If) {
						ISOConsumptionLogger
								.info("NOT EXPECTED THIS SITUATION");
						readIf(processedPCHMap, referenceType, (If) obj,
								currentScopeDB, newScope);
					} else if (obj instanceof Assign) {
						ISOConsumptionLogger
								.info("NOT EXPECTED THIS SITUATION");
						readAssign(processedPCHMap, referenceType,
								(Assign) obj, currentScopeDB, newScope);
					} else if (obj instanceof Loop) {
						ISOConsumptionLogger
								.info("NOT EXPECTED THIS SITUATION");
						readLoop(processedPCHMap, referenceType, (Loop) obj,
								currentScopeDB, newScope);
					} else if (obj instanceof Class) {
					}
				}
			}

		} else {
			// REMOVING SCOPE FROM THE REFERENCE AS WE ARE ADDING WHILE
			// ITERATING THROUGH NEWSCOPELIST
			reference.getContent().remove(scope);
		}
		List<String> processdPchInTheScope = new ArrayList<String>();
		// ADDING NEWLY CREATED SCOPES IN CURRENT REFERENCE
		for (Scope newScope : newScopeList) {
			// SORTING THE PCH LIST IN SCOPE ACCORDING TO THE EXCELL ROW AS THE
			// SORTING FUNCTIONALITY DEPENDS ON THIS
			// PCHRelatedOpsHelper.sortPCHListAccordingToExcelRow(newScope);
			reference.getContent().add(newScope);
			// PCHInfo procssedPCHList =
			// processedPCHMap.get(newScope.getDbTables()+newScope.getSequenceNo()+newScope.getPass());
			PCHInfo procssedPCHList = getFromProcessedPCHMap(processedPCHMap,
					reference.getType(),
					newScope.getDbTables() + newScope.getSequenceNo()
							+ newScope.getPass());
			if (procssedPCHList != null) {
				processdPchInTheScope.addAll(procssedPCHList
						.getPchListInRatingNode());
				processdPchInTheScope.addAll(procssedPCHList
						.getIntermediaryUsedPCH());
				processdPchInTheScope.addAll(procssedPCHList.getUnusedPCH());
			}
		}

		// THIS METHOD IS ADDED TO CHECK IF THERE ARE ANY PCH WIHTOUT ANY
		// REFERENCE OR ENTRY IN VARAIBLE TYPE
		if (newScopeList.size() > 1)
			PCHRelatedOpsHelper.checkForExcluedePCH(reference.getType(),
					processedPCHMap, currentScopeDB, scope,
					processdPchInTheScope, newScopeList.get(0));
		// AFTER EXECUTING ALL THE SCOPES WE NEED TO CHECK FOR ANY REMAINING PCH
		// WHOSE NAME IS NOT
		// INCLUDE IN THE VARIABLE TYPE AND IT IS NOT A INTERMEDIARY PCH USED

		// ADDING PCH EXECUTION SEQUENCE TO THE EVERY SCOPE
		ICorrectionForPCHModularization pchModularization = new CorrectionForPCHMoularizationImpl();
		pchModularization.addPCHExecutionSequence(reference.getType(),
				newScopeList, processedPCHMap);
	}

	/**
	 * @param currentScopeDB
	 * @param mainScope
	 * @throws Exception
	 */

	private void readPCH(Map<String, PCHInfo> processedPCHMap,
			String referenceType, PCH pch, Database currentScopeDB,
			Scope currentScope) throws Exception {

		for (Object obj : pch.getContent()) {
			if (obj instanceof If) {
				readIf(processedPCHMap, referenceType, (If) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Assign) {
				readAssign(processedPCHMap, referenceType, (Assign) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Loop) {
				// ISOConsumptionLogger.info("loop");
				readLoop(processedPCHMap, referenceType, (Loop) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Class) {
				// ISOConsumptionLogger.info("class");
			} else if (obj instanceof Bracket) {
				readBracket(processedPCHMap, referenceType, (Bracket) obj,
						currentScopeDB, currentScope);

			}
		}
	}

	private void readBracket(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Bracket bracket, Database currentScopeDB,
			Scope currentScope) throws Exception {
		List<Object> content = bracket.getContent();
		for (Object obj : content) {
			if (obj instanceof Expression) {
				Expression expression = (Expression) obj;
				readExpression(processedPCHMap, referenceType, expression,
						currentScopeDB, currentScope);
				/*
				 * if(!specialFunctionFlag)
				 * buffer.append(AlgoConstant.RightRoundBracket);
				 */
			}
		}
		// specialFunctionFlag = false;
	}

	private void readIf(Map<String, PCHInfo> processedPCHMapm,
			String referenceType, If _if, Database currentScopeDB,
			Scope currentScope) throws Exception {
		// System.out.print("if ");

		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				readCondition(processedPCHMapm, referenceType, (Condition) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Then) {
				readThen(processedPCHMapm, referenceType, (Then) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Else) {
				readElse(processedPCHMapm, referenceType, (Else) obj,
						currentScopeDB, currentScope);
			}
		}
	}

	private void readCondition(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Condition condition, Database currentScopeDB,
			Scope currentScope) throws Exception {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression(processedPCHMap, referenceType,
						(Expression) obj, currentScopeDB, currentScope);
			}
		}
	}

	private void readThen(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Then then, Database currentScopeDB,
			Scope currentScope) throws Exception {
		// ISOConsumptionLogger.info("{ ");
		for (Object obj : then.getContent()) {
			if (obj instanceof Assign) {
				readAssign(processedPCHMap, referenceType, (Assign) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof If) {
				readIf(processedPCHMap, referenceType, (If) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Loop) {
				readLoop(processedPCHMap, referenceType, (Loop) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}

		// ISOConsumptionLogger.info("}");
	}

	// private int loopIndex = 0;

	private void readLoop(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Loop loop, Database currentScopeDB,
			Scope currentScope) throws Exception {
		// ISOConsumptionLogger.info("{ ");
		for (Object obj : loop.getContent()) {
			if (obj instanceof Assign) {
				readAssign(processedPCHMap, referenceType, (Assign) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof If) {
				readIf(processedPCHMap, referenceType, (If) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Loop) {
				readLoop(processedPCHMap, referenceType, (Loop) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
	}

	private void readElse(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Else _else, Database currentScopeDB,
			Scope currentScope) throws Exception {
		// ISOConsumptionLogger.info("else");
		// ISOConsumptionLogger.info("{ ");
		for (Object obj : _else.getContent()) {
			if (obj instanceof Assign) {
				readAssign(processedPCHMap, referenceType, (Assign) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof If) {
				readIf(processedPCHMap, referenceType, (If) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Loop) {
				readLoop(processedPCHMap, referenceType, (Loop) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof ATTACH) {
				// level + 1
			}
		}
		// ISOConsumptionLogger.info("}");
	}

	private void readAssign(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Assign assign, Database currentScopeDB,
			Scope currentScope) throws Exception {
		// System.out.print(assign.getLValue() + " = ");
		// sb.append(assign.getLValue());

		for (Object obj : assign.getContent()) {
			if (obj instanceof Expression) {
				readExpression(processedPCHMap, referenceType,
						(Expression) obj, currentScopeDB, currentScope);
			}
		}
	}

	private void readExpression(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Expression expression,
			Database currentScopeDB, Scope currentScope) throws Exception {

		if (expression.getContent().size() > 1
				&& !(expression.getVariableType() != null && expression
						.getVariableType().equals("RT"))) {
			// sb.append(" expression -> " + expression.getOp());
			readBothSides(processedPCHMap, referenceType, expression,
					currentScopeDB, currentScope);
		} else {
			readContent(processedPCHMap, referenceType, expression,
					currentScopeDB, currentScope);
		}
	}

	private void readBothSides(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Expression expression,
			Database currentScopeDB, Scope currentScope) throws Exception {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression ex = (Expression) obj;
				readExpression(processedPCHMap, referenceType, ex,
						currentScopeDB, currentScope);
			} else if (obj instanceof Function) {
				readFunction(processedPCHMap, referenceType, (Function) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Ratetable) {
				readRatetable(processedPCHMap, referenceType, (Ratetable) obj,
						currentScopeDB, currentScope);
				// rateTableList.add((Ratetable)obj);
			} else if (obj instanceof Bracket) {
				readBracket(processedPCHMap, referenceType, (Bracket) obj,
						currentScopeDB, currentScope);
			}
		}
	}

	private void readContent(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Expression expression,
			Database currentScopeDB, Scope currentScope) throws Exception {
		for (Object obj : expression.getContent()) {

			if (obj instanceof String) {
				checkVariableName(processedPCHMap, referenceType,
						obj.toString(), currentScopeDB,
						expression.getVariableType(), currentScope, false);
			} else if (obj instanceof Expression) {
				readExpression(processedPCHMap, referenceType,
						(Expression) obj, currentScopeDB, currentScope);
			} else if (obj instanceof Ratetable) {
				readRatetable(processedPCHMap, referenceType, (Ratetable) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Function) {
				readFunction(processedPCHMap, referenceType, (Function) obj,
						currentScopeDB, currentScope);
			} else if (obj instanceof Bracket) {
				readBracket(processedPCHMap, referenceType, (Bracket) obj,
						currentScopeDB, currentScope);
			}
		}
	}

	/**
	 * @param variableName
	 * @param currentScopeDB
	 * @param variableType
	 * 
	 *            THIS METHOD WILL CHECK THE VARAIBLE IS POINTING TO PCH IN
	 *            SCOPE
	 * @throws Exception
	 * 
	 */
	private void checkVariableName(Map<String, PCHInfo> processedPCHMap,
			String referenceType, String variableName, Database currentScopeDB,
			String variableType, Scope currentScope, boolean isFromRatetable)
			throws Exception {
		if (variableType != null)
			if (variableType.equals(VariableType.LV_VALUE.toString())
					|| variableType.equals(VariableType.LV_INTEGER.toString())
					|| variableType.equals(VariableType.LV_NUMERIC.toString())
					|| variableType.equals(VariableType.LV_DOUBLE.toString())
					|| variableType.equals(VariableType.LV_STRING.toString())
					|| variableType.equals(VariableType.LV_BOOLEAN.toString())
					|| variableType.equals(VariableType.LV_DATE.toString())
					|| variableType.equals(VariableType.LV_TIMESPAN.toString())
					|| variableType.equals(VariableType.COLUMN_NUMERIC.toString())
					|| variableType.equals(VariableType.COLUMN_STRING.toString())
					|| isFromRatetable) {
				// CHECK WHTHER THIS VARIABLE POINTS TO PCH IF SO ADD THIS PCH
				// TO THE CURRENT NEW SCOPE
				Connection curScopeCon = new XMLConnection(currentScopeDB);
				Query query = new XmlQuery(curScopeCon);
				StringBuffer queryString = new StringBuffer();
				// class[@name='BOP']/rating
				queryString.append("//(PCH[@name='")
						.append(variableName.trim()).append("'])[1]");
				query.createQuery(queryString.toString());
				Object returnPCH = query.execute();
				if (returnPCH != null && !returnPCH.equals("")) {
					try {
						Object pchObj = JAXBUtils.readFromSource(
								returnPCH.toString(), PCH.class);
						PCH pch = (PCH) pchObj;
						String oldKey = currentScope.getDbTables()
								+ currentScope.getSequenceNo()
								+ currentScope.getPass();
						if (!getFromProcessedPCHMap(processedPCHMap,
								referenceType, oldKey).getIntermediaryUsedPCH()
								.contains(pch.getName().trim())) {
							currentScope.getContent().add(pch);
							String pchName = pch.getName().trim();
							// pchName = handleAncestorInXpath(pchName);
							getFromProcessedPCHMap(processedPCHMap,
									referenceType, oldKey)
									.getIntermediaryUsedPCH().add(pchName);
							// READING NEWLY ADDED PCH
							readPCH(processedPCHMap, referenceType, pch,
									currentScopeDB, currentScope);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
	}

	private void readFunction(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Function function, Database currentScopeDB,
			Scope currentScope) throws Exception {
		// System.out.print(function.getType());
		// System.out.print("( ");
		// sb.append(":");
		for (Object obj : function.getContent()) {
			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				String variableType = arg.getVariableType();
				if (variableType == null)
					variableType = "dummy";
				checkVariableName(processedPCHMap, referenceType,
						arg.getContent(), currentScopeDB, variableType,
						currentScope, true);
			}
		}

	}

	private void readRatetable(Map<String, PCHInfo> processedPCHMap,
			String referenceType, Ratetable ratetable, Database currentScopeDB,
			Scope currentScope) throws Exception {

		for (Object obj : ratetable.getContent()) {

			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				String content = arg.getContent().trim();
				String variableType = arg.getVariableType();
				if (variableType == null)
					variableType = "dummy";
				checkVariableName(processedPCHMap, referenceType, content,
						currentScopeDB, variableType, currentScope, true);
			}

		}
	}

	private void putInProcessedPCHMap(Map<String, PCHInfo> processedPCHMap,
			String referenceType, String oldKey, PCHInfo pchInfo) {
		processedPCHMap.put(referenceType + oldKey, pchInfo);
	}

	private PCHInfo getFromProcessedPCHMap(
			Map<String, PCHInfo> processedPCHMap, String referenceType,
			String oldKey) {
		return processedPCHMap.get(referenceType + oldKey);
	}

}
