package com.mmpnc.rating.iso.consumption.returnmodels;

import java.util.List;

import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.rating.iso.icmxml.creater.CreateIcmInputXml;
import com.mmpnc.rating.iso.wrapper.ICMRequiredData;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public class CreateIcmRatingNodeContent implements ICreateIcmNodeContent{

	private static CreateIcmRatingNodeContent icmRatingNodeContent;
	
	private CreateIcmRatingNodeContent(){}
	
	public static CreateIcmRatingNodeContent getInstance(){
		if(icmRatingNodeContent == null){
			icmRatingNodeContent = new CreateIcmRatingNodeContent();
		}
		return icmRatingNodeContent;
	}
	
	
	@Override
	public void createIcmRatingNodeContent(IcmRatingNodeContent icmRatingNodeContent,
			List<IcmRatingNodeContent>icmNodeContentList,ICMRequiredData icmRequiredData) {
		
		List<LookupModel> lookupModlist = CreateIcmInputXml.getInstance().getLookupModels().getLookupModels();
		if(lookupModlist.size()> 0)
			for(LookupModel lookupModel : lookupModlist)
			icmRequiredData.getLookupModels().add(lookupModel);
		
	}
	
	/*@Override
	public void createIcmRatingNodeContent(String pchName,IcmRatingNodeContent icmRatingNodeContent,
			List<IcmRatingNodeContent>icmNodeContentList,ICMRequiredData icmRequiredData) {
		
		List<LookupModel> lookupModlist = CreateIcmInputXml.getInstance().getLookupModels().getLookupModels();
		if(lookupModlist.size()> 0)
			for(LookupModel lookupModel : lookupModlist)
			icmRequiredData.getLookupModels().add(lookupModel);
		
		icmNodeContentList.add(icmRatingNodeContent);
	}*/

}
