package com.mmpnc.rating.iso.algorithm.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;



/**
 * @author Nilkanth9581
 *	THIS CLASS WILL ACT A HOLDER FOR THE UNITIALIZED VARAIABLES
 */
public class UnInitializedVariableListHolder {
	private static UnInitializedVariableListHolder variableListHolder = null;
	private static Map <String,List<String>> variableListMap = null;
	
	private UnInitializedVariableListHolder(){
		variableListMap = new HashMap<String, List<String>>();
	}
	
	
	public static UnInitializedVariableListHolder getInstance(){
		if(variableListHolder == null){
			variableListHolder = new UnInitializedVariableListHolder();
		}
		return variableListHolder;
	}
	
	/**
	 * @param coverageName
	 * @param variableName
	 */
	public void addVariableToUnIniList(String coverageName,String variableName){
		if(variableListMap.get(coverageName) == null){
			ArrayList<String> variableList = new ArrayList<String>();
			variableList.add(variableName);
			variableListMap.put(coverageName, variableList);
		}else{
			variableListMap.get(coverageName).add(variableName);
		}	
	}
	
	/**
	 * THIS METHOD WILL PRINT UNITIALIZED VARAIBEL AND THE COVERAGE NAMES THEY USED
	 * TO GET THIS REPORT JUST CALL THIS METHOD CALL IT FROM THE CLASS WHICH CALLING THE ISO CONSUPTION WRAPPER CLASS
	 * 
	 */
	public static void printUnInitializedVaraibleList(){
		
		//Set<String> keySet = variableListMap.entrySet();
		
		for(Entry<String, List<String>>entry  :variableListMap.entrySet()){
			ISOConsumptionLogger.info("CoverageName :"+entry.getKey()+"]");
			List<String> unintVarList = entry.getValue();
			for(String varName : unintVarList){
				ISOConsumptionLogger.info(varName);
				ISOConsumptionLogger.info(",");
			}
			ISOConsumptionLogger.info("\n");
		}
	}
}
