package com.mmpnc.rating.iso.publish.ratingalgo;

import java.sql.Connection;
import java.util.List;

import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.RatingAlgorithm;

/**
 * @author nilkanth9581
 *
 */
public interface IPublishAlgorithm {
	public String addAlgorithmMaster(RatingAlgorithm algorithm, Connection con,
			String userName, String inputId, int executionOrder, String ratingStage);

	public void addAlgoSteps(List<AlgoStep> stepList, Connection con,
			String userName, String algorithmId, String parentStepId,
			String inputId);

	public String deleteAlgorithm(String algorithmID, Connection con);

	public String deleteAllAlgorithmSteps(String algorithmID, Connection con);

	public String deleteAlgorithmStep(String algorithmStepID, Connection con);
}
