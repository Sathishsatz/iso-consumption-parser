package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.PredicatesUtil;

public class EqualsEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public EqualsEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException {
		left.setContext(context);
		StringBuffer equals = new StringBuffer();
		equals.append(" ( ");
		equals.append(left.evaluate());
		equals.append(" == ");
		right.setContext(context);
		equals.append(right.evaluate());
		PredicatesUtil.addIfPredicateConition(context, equals);
		equals.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return equals;
	}


}
