package com.mmpnc.rating.iso.algorithm;

import java.util.ArrayList;
import java.util.List;

public class IgnorePCHHolder {
	private static List <String> pchList = new ArrayList<String>();
	
	static{
		//PCH GetDeductilbe and GetInteger has been used in GL-NY algorithms 
		//These PCH actually used to find integer amount or deductible amount
		//as GetInteger and GetDedcutible functions has been implemented in rating engine these four PCH has been ignored
		//for consumption process
		pchList.add("integerAmount");
		pchList.add("deductibleAmount");
		pchList.add("GetInteger");
		pchList.add("GetDeductible");
		pchList.add("domainTableLookup");
		pchList.add("domainZipCodeTableLookup");
		pchList.add("domainCountyTableLookup");
		pchList.add("BusnPrsnPropLimitOfInsRelativityFactor");
		//pchList.add("classVltnBPPLimit");
		//pchList.add("LookUpBusnPrsnPropLimitofInsuranceRelativityFactor");
	}
	
	
	public static boolean isAvailable(String pchName){
		return pchList.contains(pchName.trim());
	}
	
	public void addPCH(String pchName){
		pchList.add(pchName);
	}
	
	/*public boolean isAvailable(String pchName){
		return pchList.contains(pchName.trim());
	}*/
	
	
}
