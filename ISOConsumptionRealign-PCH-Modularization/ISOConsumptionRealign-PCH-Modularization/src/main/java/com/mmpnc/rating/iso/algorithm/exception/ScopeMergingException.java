package com.mmpnc.rating.iso.algorithm.exception;


public class ScopeMergingException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String message = "Scope Merging Exception: ";
	
	public ScopeMergingException(String message){
		this.message = this.message.concat(message);
	}
	
	
	public String getMessage(){
		return message;
	}
}
