package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.connection.helper.Query;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.ConstraintType;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.ForStep;
import com.mmpnc.icm.common.ide.models.LookupCriteriaModel;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.rating.iso.algorithm.util.XpathUtil;
import com.mmpnc.rating.iso.wrapper.ICMRequiredData;

/**
 * @author nilkanth9581
 *
 */
public class ForStepCreater extends BaseStepCreater{
	
	private static ForStepCreater forStepCreater;
	
	private ForStepCreater(){}
	
	/**
	 * @return
	 */
	public static ForStepCreater getInstance(){
		if(forStepCreater == null){
			forStepCreater = new ForStepCreater();
		}
		return forStepCreater;
	}
	
	/**
	 * @param loopthrough
	 * @return
	 */
	public ForStep createForLoopStep(String loopthrough,boolean isPredicate,Map<String,PredicateInfoLevel> predicateMap,String parentCondition,Context context){
		
		ForStep forStep = new ForStep();
		forStep.setName(Constants.FOR_NAME+"#"+getCounter(STEPTYPE.FOR));
		forStep.setStepType(Constants.FOR_STEPTYPE);
		forStep.setRemark(Constants.FOR_REMARK);
		
		LookupModel lookupModel = new LookupModel();
		lookupModel.setBaseModel(Constants.FOR_BASE_MODEL);
		List<String> markList = new ArrayList<String>();
		String description = "FOR LOOP STEP";
		
		markList.add(description);
		forStep.setNoteList(markList);
		
		
		//RETURN ATTRIBUTES
		forStep.setParentCondition(parentCondition);
		forStep.setLookupModel(lookupModel);
		//LOOKUP CRITERIA MODEL
		LookupCriteriaModel lookupCriteriaModel = new LookupCriteriaModel();
		
		lookupModel.setLookupCriteriaModel(lookupCriteriaModel);
		
		FactPattern factPattern = new FactPattern();
		
		//loopthrough = "CCSRERequest.GeneralLiability.GeneralLiabilityLocation.GeneralLiabilityClassification";
		//String filter = loopthrough;
		//CHEKCING WHETHER THE LOOP IS INSIDE ANOTHER LOOP OR NOT
		
		String outerLoopLevel = (String)context.getValue(ContextParam.LOOP_THROUGH_CURRENT_CONTEXT_LEVLE);
		String scopePath = null;
		if(outerLoopLevel != null){
			scopePath = outerLoopLevel;
		}else{
			scopePath= (String)context.getValue(ContextParam.SCOPE);
		}
		
		Database dbForLoop = (Database)context.getValue(ContextParam.OBJECTBASEFORLOOP);
		String [] ccXpathFilters = getXpathForSRERequest(loopthrough,scopePath,dbForLoop,context);
		List<String> filterList = Arrays.asList(ccXpathFilters);
		
		int order = 1;
		
		
		filterList = addProductIdFilterToSpecifiedIndex(filterList);
		
		
		Database dbObjectBase = (Database)context.getValue(ContextParam.OBJECTDATABASE);
		String scopeFullyQualifiedPath = XpathUtil.getScopePath(dbObjectBase,scopePath);
		//int arrayorder = ccXpathFilters.length-1;
		//int fieldorder = 0;
		
		for(String filter:filterList){
			SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
			boolean addSourceObject = false;
			if(filter.contains(Constants.CURRENT_CONTEXT_OBJECT_NAME)){
				String filterwithoutAttr = filter.substring(0,filter.lastIndexOf("."));
				if(scopeFullyQualifiedPath.contains(filterwithoutAttr)){
					addSourceObject = true;
				}
				filter = filter.replace(Constants.CURRENT_CONTEXT_OBJECT_NAME+".","");
				//filter = filter.substring(0,filter.lastIndexOf("."));
			}
			singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
			//@TODO how to decide the field type
			singleFieldConstraintEBLeftSide.setFieldType(Constants.FOR_FIELD_TYPE);
			
			
			singleFieldConstraintEBLeftSide.setComAttrVal(filter);
			singleFieldConstraintEBLeftSide.setOperator(Constants.FOR_OPERATOR);
			singleFieldConstraintEBLeftSide.setOrderNo(order++);
			singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.FOR_LOCAL_INDICATOR);
			singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.FOR_VALUE_TYPE_INDICATOR_M);
			//if(arrayorder != filterList.size()-1)
			
			if(addSourceObject){
				singleFieldConstraintEBLeftSide.setSourceObject(Constants.FOR_SOURCE_OBJECT);
				singleFieldConstraintEBLeftSide.setSourceObjectAttr(filter);
			}
			//singleFieldConstraintEBLeftSide.setComAttrVal(filter);
			singleFieldConstraintEBLeftSide.setType(Constants.FOR_FIELD_TYPE);
			String filterForCurrentLevel = null;
			if(filter.contains(".Id")){
				filterForCurrentLevel = filter.replace(".Id", "");
			}else{
				filterForCurrentLevel = filter;
			}
			String currentFilterLevel = filterForCurrentLevel.contains(".")?filterForCurrentLevel.substring(filterForCurrentLevel.lastIndexOf(".")+1):filterForCurrentLevel;
			
			//if(fieldorder == arrayorder)
			factPattern.addConstraint(singleFieldConstraintEBLeftSide);
			order = addLoopThroughPredicateFilters(factPattern,order,currentFilterLevel, predicateMap);
		}
		
		
		lookupCriteriaModel.addLhsItem(factPattern);
		
		return forStep;
	}

	
	/**
	 * @param filterList
	 * @return
	 * 
	 * THIS MEHTOD WILL ADD PRODUCT ID FILTER IN FOR LOOP STEP
	 * THIS IS USEFUL IN MULTI STATE REQUEST WHERE PRODUCT ID WILL BE AT DIFFERENT LEVELS IN RATING REQUEST
	 * 
	 */
	private List<String> addProductIdFilterToSpecifiedIndex(List<String>filterList){
	
		if(ICMRequiredData.getInstance().isMulitState()){
			int index = ICMRequiredData.getInstance().getProductIDLevel();
			
			String xpath  = filterList.get(index-1);
			List<String> newFilterList = new ArrayList<String>();
			newFilterList.addAll(filterList);
			if(xpath != null && xpath.contains(".Id")){
				xpath = xpath.replace(".Id", ".ProductId");
				newFilterList.add(index,xpath);
			}
			return newFilterList;
		}
		return filterList;
	}
	
	
	/**
	 * @param factPattern
	 * @param currentorder
	 * @param levle
	 * @param predicateMap
	 */
	private int addLoopThroughPredicateFilters(FactPattern factPattern , int currentorder,String levle , Map<String,PredicateInfoLevel> predicateMap){
		if(predicateMap.containsKey(levle)){
			
				PredicateInfoLevel predicateInfoLevel = predicateMap.get(levle);
				//for(Map.Entry<String, PredicateInfoLevel> entry : predicateMap.entrySet()){
				
				//PredicateInfoLevel predicateInfoLevel = entry.getValue();
				//AT THE SAME LEVEL THERE CAN BE MORE THAN ONE PREDICATES SO THE PREDICATEINFOLEVEL OBJECT
				//HAS THE LIST OF LOOPTHROUGHINFO OBJECTS IN IT.
				List<LoopThroughInfo> predicatesAtCurrentLevle = predicateInfoLevel.getLoopThroughList();
				for(LoopThroughInfo loopThroughInfo : predicatesAtCurrentLevle){
					SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
					String filter = loopThroughInfo.getPredicateLHS();
					if(filter.contains(Constants.CURRENT_CONTEXT_OBJECT_NAME)){
						filter = filter.replace(Constants.CURRENT_CONTEXT_OBJECT_NAME+".","");
						//filter = filter.substring(0,filter.lastIndexOf("."));
					}
					if(filter.contains("Policy")){
						filter = filter.replace("Policy"+".", "");
					}
					singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
					//@TODO how to decide the field type
					singleFieldConstraintEBLeftSide.setFieldType(Constants.FOR_FIELD_TYPE);
					singleFieldConstraintEBLeftSide.setFieldName(filter);
					singleFieldConstraintEBLeftSide.setOperator(loopThroughInfo.getOperator());
					singleFieldConstraintEBLeftSide.setOrderNo(currentorder++);
					singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.FOR_LOCAL_INDICATOR);
					if(loopThroughInfo.isModelAttribute()){
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.FOR_VALUE_TYPE_INDICATOR_M);
						singleFieldConstraintEBLeftSide.setSourceObject(Constants.FOR_SOURCE_OBJECT);
						singleFieldConstraintEBLeftSide.setSourceObjectAttr(filter);
					}
					else{
						singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.FOR_VALUE_TYPE_INDICATOR_S);
						String sourceStaticValue = loopThroughInfo.getPredicateRHS().trim();
						if(sourceStaticValue.startsWith("'")){
							sourceStaticValue = sourceStaticValue.replace("'", "");
						}
						singleFieldConstraintEBLeftSide.setValue(sourceStaticValue);
					}
					
					singleFieldConstraintEBLeftSide.setComAttrVal(filter);
					singleFieldConstraintEBLeftSide.setType(Constants.FOR_FIELD_TYPE);
					//if(fieldorder == arrayorder)
					factPattern.addConstraint(singleFieldConstraintEBLeftSide);
				}
				
			//}
			return currentorder;
			
		}
		else{
			return currentorder;
		}
	}
	
	/**
	 * @param loopThrough
	 * @return
	 */
	private static String[] getXpathForSRERequest(String loopThrough,String scopePath,Database dbForLoop,Context context){
		

		
		String [] pathArr;
		if(loopThrough.contains("."))
		{
			pathArr = getPathArray(loopThrough,scopePath,context);
		}
		else
		{
			pathArr = new String[]{loopThrough};
		}
		return pathArr;
	}
	
	/**
	 * @return
	 */
	private static String[] getPathArray(String throughOfLoop,String scopePath,Context context){
		
		if(!throughOfLoop.contains("."))
			return null;
		String []strArr = throughOfLoop.replace(".","~").split("~");
		strArr = rearrangeArrayForLoop(strArr,scopePath,context);
		/*String []xPathArr = new String[strArr.length-1];
		String xPath = "";
		
		for(int i=0;i<strArr.length;i++){
				xPath = xPath+strArr[i]+".";
				if(i>=1){
					xPathArr[i-1] = xPath;
				}
		}*/
		return strArr;
		
	
	}
	
	private static String[] rearrangeArrayForLoop(String []strArr,String scopePath,Context context){
		
		String arr[] = null;
		int j=0;
		for(int i=0;i<strArr.length;){
			//if(scopePath.equals(strArr[i])){
				arr = new String[strArr.length-i-1];
				i=i+1;
				for(int k=i;k<strArr.length;k++){
					arr[j++] = strArr[k];
				}
			break;
			//}
		}
		
		Database db = (Database)context.getValue(ContextParam.OBJECTDATABASE);
		Database forLoopDb = (Database)context.getValue(ContextParam.OBJECTBASEFORLOOP);
		Connection conn = DBFactoryBuilder.getXmlConnection(forLoopDb);
		Query query = DBFactoryBuilder.getXMLQuery(conn);
		
		//THIS HANDLING IS ADDED TO ADD ID ATTRIBUTE TO THE XPATH
		//String retString = (String) query.execute();
		String qualifiedPath = XpathUtil.getScopePath(db,arr[0]);
		qualifiedPath = checkIFTheQualifiedPathHasMultipleXpaths(qualifiedPath);
		query = query.createQuery("//TableName[@name='"+arr[0]+"']");
		Object returnStringX = query.execute();
		if("".equals(returnStringX)){
			//IF EMPTY STRING THEN ITS A MODEL ATTRIBUTE NO NEED TO ADD ID TO IT
			arr[0] = qualifiedPath;
		}else{
			//IF NOT THEN IT IS A MODEL NAME AND NEEDS TO ADD ID TO IT
			arr[0] = qualifiedPath+"."+"Id";
		}
		
		//qualifiedPath = qualifiedPath;
		//arr[0] = qualifiedPath;
		for(int i=1;i<arr.length;i++){
			String path = arr[i];
			query = query.createQuery("//TableName[@name='"+path+"']");
			Object returnString = query.execute();
			if("".equals(returnString)){
				//IF EMPTY STRING THEN ITS A MODEL ATTRIBUTE NO NEED TO ADD ID TO IT
				qualifiedPath = qualifiedPath+"."+path;
				arr[i] = qualifiedPath;
				
			}else{
				//IF NOT THEN IT IS A MODEL NAME AND NEEDS TO ADD ID TO IT
				qualifiedPath = qualifiedPath+"."+path; 
				arr[i] = qualifiedPath+"."+"Id";
			}
		}
		
		return arr;
	}
	
	/**
	 * @param qualifiedPath
	 * @return
	 * THIS METHOD IS ADDED TO HANDLE /*\\premium in ho algorithms
	 */
	private static String checkIFTheQualifiedPathHasMultipleXpaths(String qualifiedPath){
		if(qualifiedPath.trim().contains(" ")){
			String qualArr [] =  qualifiedPath.split(" ");
			qualifiedPath = qualArr[0];
		}
			
		return qualifiedPath;
	}
	/*private static void handlePredictaes(String loopThrough){
		//String predicateLevel = null;
		// predicateCondition = null;
		String completePath = null;
		String [] arr = loopThrough.split("\\]");
		List<String> list = new ArrayList<String>();
				
		for(int i=0;i<arr.length;i++ ){
			String xpathWithPred = arr[i];
			
			String [] pred = xpathWithPred.split("\\[");
			
			String key = null;
			String value = null;
			for(int j=0;j<pred.length;j++){
				
				if(j%2 == 0){
					list.add(pred[j]);
					key = pred[j];
					if(completePath == null){
						completePath = key;
					}
					else{
						completePath = completePath+key;
					}
				}
				else {
					String predicateCond = pred[j];
					if(predicateCond.contains("\\")){
						predicateCond = predicateCond.substring(predicateCond.lastIndexOf("\\"));
					}
					value = predicateCond;	
					list.add(pred[j]);
				}
				if (value != null){
					
				}
			}
		}
		
		int numberOfPredicates = loopThrough.replaceAll("[^\\[]", "").length();
		Iterator <String>iterator = list.iterator();
		while(iterator.hasNext()){
			String predicateLevle = iterator.next();
			String predicateCondtion = iterator.next();
			
			if(predicateCondtion.contains("&gt;")){
				Iterator<String>prdList = Arrays.asList(predicateCondtion.split("&gt;")).iterator();
				while(prdList.hasNext()){
					String predicateFilterLHS = prdList.next();
					String predictateFilterRHS = prdList.next();
					//get the fully qualified path for LHS and RHS
					
					
				}
				
				
			}
		}
			
			//loopThrough.
		
	}
	
	private String[] createPredicate(String xpathWithPredicate){
		
		return null;
	}

	public static void main(String[] args) {
		
		handlePredictaes("../BOPContrctrsInstalltnToolsAndEquipmtScheduledPropCovDetail[Limit &gt; 0]/BOL[Lim &lt; 0]");
		
		 
	}*/

}
