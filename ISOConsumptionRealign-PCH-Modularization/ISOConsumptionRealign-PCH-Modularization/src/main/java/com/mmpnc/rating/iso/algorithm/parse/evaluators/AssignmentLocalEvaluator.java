package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.icm.common.ide.models.IFStatementStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.AssignRhsXpathPredicateUtil;
import com.mmpnc.rating.iso.helper.ArithmaticStepCreater;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.rating.iso.helper.IFStepCreater;

public class AssignmentLocalEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;

	public AssignmentLocalEvaluator(Context context, Evaluator v, Evaluator e1) {
		this.context = context;
		this.left = v;
		this.right = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Object evaluate()throws RecognitionException {
		
		
		left.setContext(context);
		String var = left.evaluate().toString();
		right.setContext(context);
		
		context.putValue(ContextParam.ISASSIGNEMENTRHSXPATH, true);
		//PUTTING WHETHER THE VARIABLE IS LOOCAL OR MODEL ATTRIBUTE
		String expression = right.evaluate().toString();
		context.putValue(ContextParam.ISASSIGNEMENTRHSXPATH, false);
		
		Object isAssignmetnRhsHasPredicateObj = context.getValue(ContextParam.IS_ASSIGNSTATEMENTRHSXPATH);
		
		if(isAssignmetnRhsHasPredicateObj == null)
			isAssignmetnRhsHasPredicateObj = false;
		Boolean isAssignmetnRhsHasPredicate = (Boolean)isAssignmetnRhsHasPredicateObj;
		
		context.putValue(ContextParam.DAYS_DIFF_FUNCTION, false);
		String parentCondition = (String) context.getValue(ContextParam.PARENTCONDITION);
		
		IFStatementStep ifStatementStep = null;
		parentCondition = handleAssignementRhsPredicate(ifStatementStep, isAssignmetnRhsHasPredicate, parentCondition);
		
		ArithmeticStep arithmeticStep = ArithmaticStepCreater.getInstance().createArithmeticStep(parentCondition, var, expression,null);
		
		List<AlgoStep> algoStepList = (List<AlgoStep>) context.getValue(ContextParam.STEPS);
		AssignRhsXpathPredicateUtil.createAndAddExpressionStep(arithmeticStep, isAssignmetnRhsHasPredicate, ifStatementStep, algoStepList);
		//algoStepList.add(arithmeticStep);
		return null;
	}

	
	
	/**
	 * @param ifStatementStep
	 * @param isAssignmetnRhsHasPredicate
	 * @param parentCondition
	 * @return
	 * 
	 * This method handles the creation of if Step if Assign rhs xpath has predicate in it
	 */
	private String handleAssignementRhsPredicate(IFStatementStep ifStatementStep,boolean isAssignmetnRhsHasPredicate,String parentCondition){
		if(isAssignmetnRhsHasPredicate){
			Object objectAssignmentPredicateCondition = context.getValue(ContextParam.ASSIGNSATEMENTRHSPREDICATEXPATH);
			IFStepCreater ifStepCreater = IFStepCreater.getInstance();
			ifStatementStep = ifStepCreater.createIfStatement(new StringBuffer(objectAssignmentPredicateCondition.toString()), parentCondition);
			ifStatementStep.setElseSelectKeyWord("NA");
			//ADDING A LIST TO IF CHILD STEPS
			List<AlgoStep> ifChildStepList =  new ArrayList<AlgoStep>();
			ifStatementStep.setAlgoStepList(ifChildStepList);
			//CHANGING PARENT CONDITION AS THE ASSIGN STEP IS GOING TO BE THE PART OF INTERMEDIARY IF CONDITION
			parentCondition = Constants.IF_THEN;
			//System.out.println(objectAssignmentPredicateCondition.toString());
		}
		return parentCondition;
	}
	
	
}
