package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class AttributeEvaluator implements Evaluator {
	private Context context;
	private String attribName;
	
	public AttributeEvaluator(String string) {
		this.attribName = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
//		System.out.println("Attribute Evaluator called ");
		StringBuffer buffer = new StringBuffer();
		buffer = (StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		
		if(context.getValue(ContextParam.ISFUNCTION) != null && (Boolean) context.getValue(ContextParam.ISFUNCTION)){
//			System.out.println("Function " + this.attribName);
			context.putValue(ContextParam.FUNCTIONNAME, this.attribName);
		}
		
		if(this.attribName.equals("*")){
			buffer.append("*");
			return Type.CONSTANT;
		}else{
			buffer.append(this.attribName);
			return Type.ATTRIBUTE;
		}
		
	}

}
