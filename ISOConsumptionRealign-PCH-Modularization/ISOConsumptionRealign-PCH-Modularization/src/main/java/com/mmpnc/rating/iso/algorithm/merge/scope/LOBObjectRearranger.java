package com.mmpnc.rating.iso.algorithm.merge.scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Reference;

public class LOBObjectRearranger extends LOBOperationsForScopeMerging {

	@Override
	void removeOriginalReferenceAndAddNewReferenceWithMergedScopes(
			Map<Integer, Reference> removeRefMap, LOB lob) {
		StringBuffer mergingReport = new StringBuffer();
		//IF THE ALGORITHM FILE HAS SCOPES WITH PASS 2
		if(removeRefMap.size() > 0){
			for(Integer index : removeRefMap.keySet()){
				int ind = index;
				lob.getContent().remove(ind);
				lob.getContent().add(index,removeRefMap.get(index));
				mergingReport.append("MERGED SCOPE REFERENCE :"+removeRefMap.get(index).getDbTables()+" TYPE:"+removeRefMap.get(index).getType());
				mergingReport.append("\n");
			}
		}else{
			mergingReport.append("NO SCOPES WITH PASS-2 IN THIS ALGORIHM FILE :"+lob.getRatebookId());
		}	
		
		printReport(mergingReport);
	}
 
	void printReport(StringBuffer mergingReport){
		System.out.println(mergingReport);
	}
	
	
	public static void main(String args[]){
		
		LOB lob = new LOB();
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("6");
		list.add("7");
		list.add("8");
		list.add("9");
		list.add("10");
		list.add("11");
		list.add("12");
		list.add("13");
		list.add("14");
		
		lob.getContent().addAll(list);
		
		List<Object> lis = lob.getContent();
		for(Object object:lis){
			System.out.println("index:"+lis.indexOf(object)+" --- object :"+object.toString());
		}
		
		Map <Integer,String> map =  new HashMap<Integer, String>();
		map.put(4, "44");
		map.put(12, "122");
		map.put(14,"144");
		
		for(Integer ind:map.keySet()){
			lob.getContent().remove(ind);
			lob.getContent().add(ind, map.get(ind));
		}
		
		List<Object> lists = lob.getContent();
		for(Object object:lists){
			System.out.println("index:"+lis.indexOf(object)+" --- object :"+object.toString());
		}
	}
	
	
	
	
}
