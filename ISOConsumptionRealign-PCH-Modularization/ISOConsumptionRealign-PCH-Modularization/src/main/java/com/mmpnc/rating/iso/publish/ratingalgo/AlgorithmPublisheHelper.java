package com.mmpnc.rating.iso.publish.ratingalgo;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import com.mmpnc.icm.common.BaseICMException;
import com.mmpnc.icm.common.ide.models.RatingAlgorithm;
import com.mmpnc.icm.common.ide.models.RatingTemplate;
import com.mmpnc.icm.server.common.dao.AlgorithmHelper;
import com.mmpnc.icm.server.util.RatingXMLPersistence;

/**
 * @author nilkanth9581
 *
 */
public class AlgorithmPublisheHelper {
	
	/**
	 * @param connection
	 * @param userName
	 * @param finalRatingData
	 * @param ratingStage
	 * @throws BaseICMException 
	 */
	public void publishAlgorithm(Connection connection, String userName,
			List<Map<String, RatingAlgorithm>> finalRatingData, List<String> ratingStage,String product_id) throws BaseICMException{
		
		AlgorithmHelper algorithmHelper = new AlgorithmHelper();
		algorithmHelper.publishAlgorithm(connection, userName, finalRatingData, ratingStage,product_id);
		//algorithmHelper.publishAlgorithm(connection, userName, finalRatingData, ratingStage, "");
		
	}
	

	/**
	 * @param xmlContent
	 * @return
	 */
	public RatingTemplate populate(String xmlContent) {
		RatingTemplate ratingModel = RatingXMLPersistence.getInstance()
				.unmarshal(xmlContent);
		return ratingModel;
	}
}
