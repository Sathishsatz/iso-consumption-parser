package com.mmpnc.rating.iso.datastructure;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.vo.ColumnNode;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.TableName;
import com.mmpnc.rating.iso.config.BasicConfiguration;

/**
 * @author Nilkanth9581
 *
 */
public class AlgorithmDataStructure {
	Reference ref; 
	
	private static Map<String, String> javaKeyWordsAndReplacements = null;
	
	
	
	public AlgorithmDataStructure(Reference ref) {
		this.ref = ref;
		
	}

	/**
	 *  
	 */
	private static void intializeKeyWordsMap(){
		javaKeyWordsAndReplacements = new HashMap<String, String>();
		javaKeyWordsAndReplacements.put("Class", "Classs");
		javaKeyWordsAndReplacements.put("Default", "Defaultt");
	}
	
	/**
	 * @param column
	 * @return
	 * THIS METHOD IS ADDED TO HANNDLE JAVA KEYWORDS RELATED MODEL NAMES 
	 * 
	 */
	private static String getColumnContent(ColumnNode column){
		if(javaKeyWordsAndReplacements.containsKey(column.getContent().trim())){
			return javaKeyWordsAndReplacements.get(column.getContent().trim());
		}
		return column.getContent();
	}
	
	public StringBuffer simplifyDataStrucure() {
		
		intializeKeyWordsMap();
		
		StringBuffer content = new StringBuffer();
		content.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
		content.append("<Policy xpath='Policy'>");
		content.append(createWrapperNodeContent());
		for(Object obj : this.ref.getContent())
		{
			if(obj instanceof TableName){
				readTable(content, (TableName)obj, new StringBuffer(
						"Policy"), new StringBuffer("Policy"));
			}
		
		}
		content.append("</Policy>");
		//@TODO Comment this condition while releasing the iso consumption war
		writeDataStrucutreFile(content);
		return content;
	}

	private static void readTable(StringBuffer content, TableName table,
			StringBuffer priorXPath, StringBuffer priorQualPath) {

		StringBuffer xpath = getPath(table.getName(), priorXPath, "/");
		// StringBuffer qualpath = getPath(table.getName(),priorQualPath,".");

		StringBuffer qualpath = new StringBuffer();

		content.append("<").append(table.getName());
		content.append(" ").append("xpath='").append(xpath).append("'");
		// content.append(" ").append("qualPath='").append(qualpath).append("'");
		content.append(" ").append("multiple='").append(table.isMultipleAllowed()).append("'");
		content.append(">\n");

		for( Object obj : table.getContent()){
			if(obj instanceof ColumnNode){
				// read the columns here
				readColumn(content, (ColumnNode) obj, xpath, qualpath);
			}else if (obj instanceof TableName){
				// read the other content here
				readTable(content, (TableName) obj, xpath, qualpath);
			}
		}
		
		content.append("</").append(table.getName()).append(">\n");
	}

	
	
	private static void readColumn(StringBuffer content, ColumnNode column,
			StringBuffer xpath, StringBuffer qualpath) {
		content.append("<").append(column.getContent());
		content.append(" ").append("xpath='").append(xpath).append("/")
				.append(getColumnContent(column)).append("'");
		// content.append(" ").append("qualPath='").append(qualpath).append(".").append(column.getContent()).append("'");
		content.append(" ").append("multiple='false'");
		content.append("/>\n");
	}

	private static StringBuffer getPath(String nodeName,
			StringBuffer priorPath, String joiner) {
		// System.out.println("joiner");
		StringBuffer buffer = new StringBuffer();
		if (priorPath == null) {
			buffer.append(nodeName);
		} else {
			buffer.append(priorPath).append(joiner).append(nodeName);
		}
		return buffer;
	}

	private static StringBuffer createWrapperNodeContent() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("	<EffDate xpath='Policy/EffDate'/>");
		buffer.append("	<ExpDate xpath='Policy/ExpDate'/>");
		buffer.append("	<Premium xpath='Policy/Premium'/>");
		buffer.append(" <StateId xpath='CCSRERequest/StateId'/>");
		buffer.append(" <SecondPeriod xpath='CCSRERequest/SecondPeriod'/>");
		buffer.append(" <PackageId xpath='Policy/PackageId'/>");
		buffer.append("	<State xpath='Policy/State'>");
		buffer.append("		<Code xpath='Policy/State/Code'/>");
		buffer.append("		<Id xpath='Policy/State/Id'/>");
		buffer.append("		<Name xpath='Policy/State/Name'/>");
		buffer.append("	</State>");
		//TRANSACTION MODEL IS ADDED AS IT IS USED INT CA FL ALGORITHM FILE
		buffer.append(" <Transaction xpath='Policy/Transaction'>");
		buffer.append("		<UnitNumber xpath='Policy/Transaction/UnitNumber'/>");
		buffer.append("		<Status xpath='Policy/Transaction/Status'/>");
		buffer.append("	</Transaction>");
		//THIS XPATH IS ADDED FOR WC-PA ALGORITHMS
		buffer.append(" <AnnDate xpath='Policy/AnnDate'/>");
		//THIS XPATH IS ADDED FOR WC-TX ALGORITHMGS
		//buffer.append("<ARDDate xpath='Policy/ARDDate'/>");
		buffer.append("<Renewal xpath='Policy/Renewal'/>");
		
		return buffer;
	}
	
	
	
	private void writeDataStrucutreFile(StringBuffer xmlDS){
		
		BasicConfiguration basicConfiguration = BasicConfiguration.getInstance();
		if("Y".equals(basicConfiguration.getPublishAlgorithmSteps())){
			try {
				Writer writer = new FileWriter(new File("D:\\ds\\ds-xml.xml"));
				writer.write(xmlDS.toString());
				writer.flush();
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
