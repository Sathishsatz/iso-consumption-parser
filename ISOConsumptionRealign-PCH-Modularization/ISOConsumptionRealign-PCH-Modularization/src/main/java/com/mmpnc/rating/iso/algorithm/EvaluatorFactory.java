package com.mmpnc.rating.iso.algorithm;

import java.util.List;

import com.mmpnc.context.Context;

public class EvaluatorFactory {

	public enum EvaluatorType {
		SORT, PARSE
	}

	private EvaluatorType type;

	public EvaluatorFactory(EvaluatorType type) {
		this.type = type;
	}

	public Evaluator getGreaterThanEqualsEvaluator(Evaluator left,
			Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.GreaterThanEqualsEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.GreaterThanEqualsEvaluator(
				left, right);
	}

	public Evaluator getGreaterThanEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.GreaterThanEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.GreaterThanEvaluator(
				left, right);
	}

	public Evaluator getLessThanEqualsEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.LessThanEqualsEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.LessThanEqualsEvaluator(
				left, right);
	}

	public Evaluator getLessThanEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.LessThanEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.LessThanEvaluator(
				left, right);
	}

	public Evaluator getNotEqualsEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.NotEqualsEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.NotEqualsEvaluator(
				left, right);
	}

	public Evaluator getEqualsEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.EqualsEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.EqualsEvaluator(
				left, right);
	}

	public Evaluator getOrEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.OrEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.OrEvaluator(
				left, right);
	}

	public Evaluator getAndEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.AndEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.AndEvaluator(
				left, right);
	}

	public Evaluator getUnaryNegationEvaluator(Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.UnaryNegationEvaluator(
					right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.UnaryNegationEvaluator(
				right);
	}

	public Evaluator getModEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.ModEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.ModEvaluator(
				left, right);
	}

	public Evaluator getDivideEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.DivideEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.DivideEvaluator(
				left, right);
	}

	public Evaluator getMultiplyEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.MultiplyEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.MultiplyEvaluator(
				left, right);
	}

	public Evaluator getMinusEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.MinusEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.MinusEvaluator(
				left, right);
	}

	public Evaluator getPlusEvaluator(Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.PlusEvaluator(
					left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.PlusEvaluator(
				left, right);
	}

	public Evaluator getIntegerEvaluator(String string) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.IntegerEvaluator(
					string);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.IntegerEvaluator(
				string);
	}

	public Evaluator getStringEvaluator(String string) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.StringEvaluator(
					string);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.StringEvaluator(
				string);
	}

	public Evaluator getVarEvaluator(String constant, String string) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.VarEvaluator(
					constant, string);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.VarEvaluator(
				constant, string);
	}

	public Evaluator getXpathEvaluator(String string) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.XpathEvaluator(
					string);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.XpathEvaluator(
				string);
	}

	public Evaluator getLoopEvaluator(Context context, String string,
			List<Evaluator> evalList) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.LoopEvaluator(
					context, string, evalList);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.LoopEvaluator(
				context, string, evalList);
	}

	public Evaluator getBooleanEvaluator(Evaluator bool) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.BooleanEvaluator(
					bool);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.BooleanEvaluator(
				bool);
	}

	public Evaluator getIfEvaluator(Context context,
			Evaluator booleanEvaluator, List<Evaluator> thenEvalList,
			List<Evaluator> elseEvalList) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.IfEvaluator(
					context, booleanEvaluator, thenEvalList, elseEvalList);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.IfEvaluator(context, booleanEvaluator, thenEvalList, elseEvalList);
	}

	public Evaluator getSpecialFunctionEvaluator(Context context,
			String functionName, Evaluator parameter) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.SpecialFunctionEvaluator(
					context, functionName, parameter);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.SpecialFunctionEvaluator(
				context, functionName, parameter);
	}

	public Evaluator getFunctionEvaluator(String constant, String functionName,
			List<Evaluator> evallist) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.FunctionEvaluator(
					constant, functionName, evallist);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.FunctionEvaluator(
				constant, functionName, evallist);
	}

	public Evaluator getAssignmentXpathEvaluator(Context context,
			Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.AssignmentXpathEvaluator(
					context, left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.AssignmentXpathEvaluator(
				context, left, right);
	}

	public Evaluator getAssignmentLocalEvaluator(Context context,
			Evaluator left, Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.AssignmentLocalEvaluator(
					context, left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.AssignmentLocalEvaluator(
				context, left, right);
	}

	public Evaluator getAssignmentEvaluator(Context context, Evaluator left,
			Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.AssignmentEvaluator(
					context, left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.AssignmentEvaluator(
				context, left, right);
	}

	public Evaluator getProgramEvaluator(Context context, String programName) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.ProgramEvaluator(
					context, programName);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.ProgramEvaluator(
				context, programName);
	}

	public Evaluator getNotBooleanEvaluator(Evaluator left) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.NotBooleanEvaluator(left);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.NotBooleanEvaluator(left);
	}
	
	public Evaluator getCustomOperatorEvaluator(Evaluator left,
			Evaluator right) {
		if (this.type.equals(EvaluatorType.SORT)) {
			return new com.mmpnc.rating.iso.algorithm.sort.evaluators.CustomOperatorEvaluator(left, right);
		}
		return new com.mmpnc.rating.iso.algorithm.parse.evaluators.CustomOperatorEvaluator(	left, right);
	}
	

}
