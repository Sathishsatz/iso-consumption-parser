package com.mmpnc.rating.iso.helper;

import java.util.List;


public class PredicateInfoLevel{
	
	private List<LoopThroughInfo> loopThroughList;

	public List<LoopThroughInfo> getLoopThroughList() {
		return loopThroughList;
	}

	public void setLoopThroughList(List<LoopThroughInfo> loopThroughList) {
		this.loopThroughList = loopThroughList;
	}
	

} 
