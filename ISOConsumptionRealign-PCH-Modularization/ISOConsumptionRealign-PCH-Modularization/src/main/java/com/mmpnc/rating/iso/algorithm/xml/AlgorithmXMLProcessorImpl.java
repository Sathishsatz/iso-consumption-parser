package com.mmpnc.rating.iso.algorithm.xml;

import java.io.Reader;
import java.io.Writer;

/**
 * @author Nilkanth9581
 *
 */
public abstract class AlgorithmXMLProcessorImpl extends AlgorithmXMLProcessor {
	private Writer writer;
	
	public AlgorithmXMLProcessorImpl(Reader algoReader) {
		super(algoReader);
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	public Writer getWriter() {
		return writer;
	}

	/*@Override
	public void process(StringBuffer algorithm, NodeReference nodeReference) {
		try {
			writer.append(algorithm);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
*/
}
