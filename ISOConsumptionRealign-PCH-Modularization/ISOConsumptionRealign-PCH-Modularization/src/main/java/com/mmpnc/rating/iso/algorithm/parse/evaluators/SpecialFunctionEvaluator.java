package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.helper.ArithmaticStepCreater;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.PrecisionScales;

public class SpecialFunctionEvaluator implements Evaluator {

	private String functionName;
	private Evaluator evaluator;
	private Context context;
	
	public SpecialFunctionEvaluator(Context context, String string, Evaluator e1) {
		this.functionName = string;
		this.evaluator = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		
			evaluator.setContext(context);
			
			Object returnObj = null;
			//Handling for Special Functions
			String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
			StringBuffer returnStr = null;
			//if(Constants.ROUND_UP_DOLLAR.equals(functionName.trim()) ){
			returnObj = evaluator.evaluate();
			if( returnObj instanceof String){
				returnStr = new StringBuffer((String)returnObj);
			}
			else{
				returnStr = (StringBuffer)returnObj;
			}
			
			String precisionScale = PrecisionScales.getPrecisionScale(functionName.trim());
			//IF there is a ROUND-UP Function used we need to put the create an expression step and return the alias name for the 
			//Expression step Also need to add the precision scale for that step
			//Nullifying precision scale as we do not need this for other expression steps
			ArithmaticStepCreater arithmaticStepCreater = ArithmaticStepCreater.getInstance();
			LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
			int localCounter = locHolder.getNextLocVarCounter();
			String aliasName = "localVariable"+localCounter;
			ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition,aliasName,returnStr.toString(),precisionScale);
			//context.putValue(ContextParam.LOCALVARIABLECOUNTER , localCounter);
			@SuppressWarnings("unchecked")
			List<AlgoStep> steps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
			steps.add(arithmeticStep);
			
	
		
		return aliasName;
	}

}
