package com.mmpnc.rating.iso.algorithm.exception;

public class FlowchartException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public FlowchartException(String message, Throwable ex) {
		super(message, ex);
	}

	public FlowchartException(String message) {
		super(message);
	}

	public FlowchartException(Throwable e) {
		super(e);
	}
}

