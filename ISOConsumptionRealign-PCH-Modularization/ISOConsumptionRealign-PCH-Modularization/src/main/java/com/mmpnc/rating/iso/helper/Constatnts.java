package com.mmpnc.rating.iso.helper;

/**
 * @author nilkanth9581
 *
 */
public class Constatnts {
	
	/* Constants for if step */
	public static final String EXECUTE_STEP = "EXECUTE_STEP";
	public static final String STEP_TYPE = "IF";
	public static final String NAME = "Default Description";
	public static final String REMARK = "Default Description";
	public static final String EXP = "EXP";
	public static final String ALIASNAMEPREFIX = "local";
	public static final String THEN = "THEN";
	public static final String ELSE = "ELSE";
	
	
	//SET_ATTIBUTE_STEP
	public static final String SETATTR_NAME= "SET-ATTRIBUTE STEP";
	public static final String SETATTR_REAMRK = "SET-ATTRIBUTE STEP";
	public static final String SETATTR_STEP_TYPE = "SET_ATTRIBUTE";
	public static final String SETATTR_LOCAL_INDICATOR = "L";
	public static final String SET_ATTR_BASE_MODEL = "CCSRERequest";
	public static final String SET_ATTR_SOURCE_OBJECT = "CCSRERequest";
	
	//FOR LOOP STEP
	public static final String FOR_NAME = "TestFor";
	public static final String FOR_REMARK = "sample remark";
	public static final String FOR_STEPTYPE="FOR";
	public static final String FOR_BASE_MODEL="CCSRERequest";
	public static final String FOR_CONSTRAINT_VALUE_TYPE="TYPE_LITERAL";
	public static final String FOR_OPERATOR = "==";
	public static final String FOR_LOCAL_INDICATOR = "L";
	public static final String FOR_VALUE_TYPE_INDICATOR ="M";     //It will always be M
	public static final String FOR_SOURCE_OBJECT = "CCSRERequest";
	public static final String FOR_FIELD_TYPE = "java.lang.String";
	
	//LOOKUP STEP CONSTANTS
	public static final String LOOKUP_NAME = "Lookup step";
	public static final String LOOKUP_REMARK = "Lookup Step";
	public static final String LOOKUP_STEPTYPE = "DB_LOOKUP";
	public static final String LOOKUP_OPERATOR = "==";
	public static final String LOOKUP_CONSTRAINT_VALUE_TYPE = "TYPE_RUNTIME_VARIABLE";
	public static final String LOOKUP_RATETABLE_KEYS = "RATETABLEKEYS";
	public static final String LOOKUP_RATETABLE_VALUE = "RATETABLEVALUE";
	public static final String LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR = "L";
	public static final String LOOKUP_VALUE_TYPE_INDICATOR_A = "A";
	public static final String LOOKUP_VALUE_TYPE_INDICATOR_S = "S";
	public static final String LOOKUP_VALUE_TYPE_INDICATOR_M = "M";
	public static final String LOOKUP_LOOKUP_TYPE = "DB";
	public static final String LOOKUP_TYPE_INDICATOR = "G";
	public static final String LOOKUP_ADD_EFFECTIVE_FILTER = "Y";
	
	
}
