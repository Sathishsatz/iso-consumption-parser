package com.mmpnc.rating.iso.algorithm.correction;

import java.util.ArrayList;
import java.util.List;

public class PCHInfo {
	
	private List<String> pchListInRatingNode;
	private List<String> intermediaryUsedPCH;
	private List<String> unusedPCH;
	
	public PCHInfo(){
		pchListInRatingNode = new ArrayList<String>();
		intermediaryUsedPCH = new ArrayList<String>();
		unusedPCH = new ArrayList<String>();
	}
	
	public List<String> getPchListInRatingNode() {
		return pchListInRatingNode;
	}

	public List<String> getIntermediaryUsedPCH() {
		return intermediaryUsedPCH;
	}

	public List<String> getUnusedPCH() {
		return unusedPCH;
	}

	public void addToPCHListInRatingNode(String pchName){
		pchListInRatingNode.add(pchName);
	}
	
	public void addTOIntermeiaryUsedPCHList(String pchName){
		intermediaryUsedPCH.add(pchName);
	}
	
	public void adToUnusedPCHList(String pchName){
		unusedPCH.add(pchName);
	}
	
}
