package com.mmpnc.rating.connection.db.helper;

import java.sql.Connection;

/**
 * @author nilkanth9581
 *
 */
public interface DBConnection {
	Connection getConnection();
}
