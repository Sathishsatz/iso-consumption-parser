package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class GetDaysFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context)
			throws RecognitionException {
		// TODO Auto-generated method stub
		String getDaysPath = null;
		for(Evaluator eval : evallist){
			eval.setContext(context);
			 getDaysPath = eval.evaluate().toString();
		}
		return getDaysPath;
	}

}
