package com.mmpnc.rating.iso.algorithm.correction;

import java.util.HashMap;
import java.util.Map;

public class ScopeInfoHolder {
	private Map<String,ScopeInfo> scopeMap;
	private static final ScopeInfoHolder scopeInfoHolder = new ScopeInfoHolder();
	
	private ScopeInfoHolder(){
		scopeMap = new HashMap<String, ScopeInfo>();
	}
	
	
	public static ScopeInfoHolder getInstance(){
		return scopeInfoHolder;
	}
	
	public void addToScopeInfoMap(String key,ScopeInfo scopeInfo){
		scopeMap.put(key, scopeInfo);
	}
	
	public ScopeInfo getScopeInfo(String key){
		return scopeMap.get(key);
	}
	
	public Map<String,ScopeInfo> getScopeInfoMap(){
		return scopeMap;
	}
}
