package com.mmpnc.rating.iso.helper;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.ExecuteModuleStep;

public class ExecuteModelStepCreater extends BaseStepCreater {
	
	private static ExecuteModelStepCreater creater = new ExecuteModelStepCreater();
	
	private ExecuteModelStepCreater(){
		
	}
	
	public static ExecuteModelStepCreater getInstance(){
		return creater;
	}
	
	public ExecuteModuleStep createExecuteModelStep(String scopeFullyQualifiedPath,String moduleName,int executionOrder,Context context){
		
		/*ModuleStep moduleStep  = new ModuleStep();
		moduleStep.setModuleName(moduleName);
		moduleStep.setName("EXECUTE_MODULE STEP");
		moduleStep.setRemark("Execute Module step");
		moduleStep.setStepType(Constants.EXECUTE_MODULE);*/
		
		ExecuteModuleStep executeModuleStep = new ExecuteModuleStep();
		executeModuleStep.setExecutionOrder(executionOrder);
		
		if(context != null){
			String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
			executeModuleStep.setParentCondition(parentCondition);
		}
		
		//ADDED TO HANDLE MODULE NAME HAVING FULLY QUALIFIED PATH
		if(moduleName.contains(".")){
			if("".equals(scopeFullyQualifiedPath)){
				scopeFullyQualifiedPath = moduleName.substring(0,moduleName.lastIndexOf("."));
			}
			moduleName = moduleName.substring(moduleName.lastIndexOf(".")+1);
		}
		
		executeModuleStep.setModuleName(moduleName);
		String stepName = Constants.EXECUTE_MODULE+"#"+getCounter(STEPTYPE.EXECUTE_MODULE);
		executeModuleStep.setName(stepName+"-"+moduleName);
		executeModuleStep.setModuleLocation(scopeFullyQualifiedPath+"."+moduleName);
		executeModuleStep.setStepType(Constants.EXECUTE_MODULE);
		executeModuleStep.setRemark("EXECUTE MODULE STEP");
		return executeModuleStep;
	
	}
	
	public ExecuteModuleStep createExecuteModelStep(String moduleName,Context context){
		ExecuteModuleStep moduleStep  = new ExecuteModuleStep();
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		moduleStep.setParentCondition(parentCondition);
		String stepName = Constants.EXECUTE_MODULE+"#"+getCounter(STEPTYPE.EXECUTE_MODULE);
		moduleStep.setModuleName(stepName+"-"+moduleName);
		moduleStep.setName("EXECUTE_MODULE STEP");
		moduleStep.setRemark("Execute Module step");
		moduleStep.setStepType(Constants.EXECUTE_MODULE);
		return moduleStep;
	}
	
	
}
