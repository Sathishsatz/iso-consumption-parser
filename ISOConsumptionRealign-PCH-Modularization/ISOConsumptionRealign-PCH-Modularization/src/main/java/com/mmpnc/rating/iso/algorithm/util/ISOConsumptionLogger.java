package com.mmpnc.rating.iso.algorithm.util;

import org.apache.log4j.Logger;

public class ISOConsumptionLogger {

	private static Logger infoLogger = Logger.getLogger("file");
	private static Logger errorLogger = Logger.getLogger("errorLog");
	
	public static void debug(Object message){
		infoLogger.debug(message);
	}
	
	public static void info(Object message){
		infoLogger.info(message);
	}
	
	public static void error(Object message){
		infoLogger.error(message);
	}
	
	public static void error(Throwable e){
		infoLogger.error(e);
		errorLogger.error(e);
	}
	
	public static void error(Object message, Throwable e){
		infoLogger.error(message, e);
		errorLogger.error(message, e);
	}
	
	public static void errorInfo(String message){
		errorLogger.info(message);
	}
	
}
