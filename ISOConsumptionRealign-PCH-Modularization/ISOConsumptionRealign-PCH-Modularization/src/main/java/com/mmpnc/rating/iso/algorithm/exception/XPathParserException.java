package com.mmpnc.rating.iso.algorithm.exception;

public class XPathParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XPathParserException(String string) {
		super(string);
	}

	
}
