package com.mmpnc.rating.iso.algorithm.customfunction;

/**
 * @author nilkanth9581
 *
 */
enum CUSTOM_FUNCTION {
	RATE_TABLE("RateTable"),
	DOMAIN_TABLE("domainTableLookup"),
	GET_YEAR("GetYear"),
	GET_MONTH("GetMonth"),
	GET_DATE("GetDate"),
	GET_INTEGER("GetInteger"),
	BUSN_PRSNL("BUSN_PRSNL"),
	COUNT("Count"),
	LOOKUP_LOSS_DEV("Lookup_Lostt"),
	DEFAULT_HANDLER("DefaultHandler"),
	GET_LEAP_DAYS("GetLeapDays"),
	ADD_YEARS("AddYears"),
	STARTS_WITH("startsWith"),
	GET_PROGRAM("getProgram"),
	CONTAINS("Contains"),
	GET_DAYS("GetDays"),
	GET_STRING_NODE_VALUE("GetStringNodeValue"),
	GET_DEDUCTIBLE("GetDeductible"),
	DAYS_BETWEEN("days_between"),
	GET_DAY("GetDayOfMonth");
	
	
	
	private String name;
	
	private CUSTOM_FUNCTION(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
}