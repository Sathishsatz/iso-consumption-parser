package com.mmpnc.rating.iso.algorithm.merge.scope;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

public abstract class LOBOperationsForScopeMerging extends LOBDatabaseCreater{
	
	/* 
	 * THIS METHOD WILL FIND THE THOSE REFERENCES IN LOB AND MERGE SCOPES
	 * WITH MULTIPLE SCOPES HAVING PASS GREATER THAN ONE IN IT
	 *  
	 */
	@Override
	void iterateInLOBAndMergeScopes(LOB lob, List<String> refNameList){
		//THIS MAP WILL HOLD THE INDEX OF REFERENCE WE NEED TO REMOVE FROM THE LOB AND THE REPLACING REFERENCE
		Map<Integer, Reference> removeRefWithNewRef = new HashMap<Integer, Reference>();
		List<Object> lobContent = lob.getContent();
		for(Object object : lobContent){
			
			if(object instanceof Reference){
				Reference ref = (Reference) object;
				String refName = ref.getDbTables();
				String refType = ref.getType();
				if("Common Rating".equals(refType) || "Premium Calculation".equals(refType)){
					//IF THE REFERENCE NAME IS IN THE LIST OF REF NAMES WITH MULTIPLE PASS SCOPES
					if(refNameList.contains(refName)){
						
						//CREATE A NEW REFERENCE WITH ALL THE ATTIBUTES
						//SAME AS THE ORIGINAL SCOPE
						Reference newReferece = createReferenceClone(ref);
						Scope mergedScope = null;
						//GET THE SCOPES FROM THE REFERENCE
						List<Object> refContentList = ref.getContent();
						for(Object refContent:refContentList){
							
							boolean isThisRefContainsPass2Scope = false;
							
							if(refContent instanceof Scope){
								Scope scope = (Scope)refContent;
								//IF PASS IS ONE WE NEED TO CREATE CLONE OF THE SCOPE 
								//AND THEN ADD THE CONTENTS OF ORIGINAL SCOPE TO NEW SCOPE
								if(scope.getPass() == 1){
									//CREATIN CLONE OF THE SCOPE
									mergedScope = createScopeClone(scope);
									//ADDING CONTENTS OF THE SCOPE TO MERGED SCOPE
									mergedScope.getContent().addAll(scope.getContent());
								}
								//IF THE PASS VALUE IS GREATER THAN ONE THEN ADD CONTENTS
								//OF PASS-2/3/4...  ALGORITHM TO PASS-1 ALGORIHM
								else if(scope.getPass() >1){
									isThisRefContainsPass2Scope = true;
									if(mergedScope == null)
										ExceptionHandler.raiseScopeMergingException("Algorithm File Do not have scope ["+scope.getDbTables()+"] pass -1 Algorithm plz check]");
									mergedScope.getContent().addAll(scope.getContent());
								}
								
							}//end of ref content iterations
							
							//HERE WE NEED TO CHECK IF THE REF HAS SCOPE WITH PASS-2 AND IF SO
							//UPDATE THE MAP WITH REF INDEX AND THE NEWLY CREATED REFERENCE
							if(isThisRefContainsPass2Scope){
								//ADDING MERGED SCOPE TO COLNED REFERENCE
								newReferece.getContent().add(mergedScope);
								//GETTING THE INDEX OF ORIGINAL REFERENCE
								int indexOfOriginalRef = lob.getContent().indexOf(ref);
								//ADING INDEX AND THE NEWLY CREATED REFERENCE TO THE MAP
								removeRefWithNewRef.put(indexOfOriginalRef, newReferece);
							}
						}
						
					}
				}
				
			}
		}//END OF LOB CONTENT LOOP
		
		//AFTER PROCESSIN COMPLETE LOB WE HAVE TO REPLACE ORIGINAL REFERENCE WITH
		//NEWLY CREATED REFERENCE HAVEING MERGED SCOPES
		removeOriginalReferenceAndAddNewReferenceWithMergedScopes(removeRefWithNewRef, lob);
		
		
	}
	
	abstract void removeOriginalReferenceAndAddNewReferenceWithMergedScopes(Map<Integer,Reference>removeRefMap,LOB lob);
	
	private Scope createScopeClone(Scope originalScope){
		Scope scope = new Scope();
		scope.setDbTables(originalScope.getDbTables());
		scope.setFullyQualifiedPath(originalScope.getFullyQualifiedPath());
		scope.setPass(originalScope.getPass());
		scope.setRecordType(originalScope.getRecordType());
		scope.setRuleReference(originalScope.getRuleReference());
		scope.setSequenceNo(originalScope.getSequenceNo());
		return scope;
	}
	
	
	private Reference createReferenceClone(Reference originalReference){
		Reference reference = new Reference();
		reference.setDbTables(originalReference.getDbTables());
		reference.setName(originalReference.getName());
		reference.setNumber(originalReference.getNumber());
		reference.setState(originalReference.getState());
		reference.setType(originalReference.getType());
		return reference;
	}
}
