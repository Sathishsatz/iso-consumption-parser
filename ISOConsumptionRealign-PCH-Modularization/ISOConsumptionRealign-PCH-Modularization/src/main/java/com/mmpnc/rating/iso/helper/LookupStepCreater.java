package com.mmpnc.rating.iso.helper;

import java.util.List;

import com.mmpnc.context.Context;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;

/**
 * @author nilkanth9581
 *
 */
public class LookupStepCreater extends BaseStepCreater{
	
	//private static int db_lookup_counter = 1;
	
	private static LookupStepCreater lookupStepCreater = new LookupStepCreater();
	
	private LookupStepCreater(){
		
	}
	
	
	/**
	 * @return
	 */
	public static LookupStepCreater getInstance(){
		
		return lookupStepCreater;
	}
	
	/**
	 * @param context
	 * @param aliasName
	 * @param function
	 * @param evallist
	 * @return
	 * @throws Exception
	 */
	public Object createDBLookupStep(Context context,String aliasName,String function,List<Evaluator> evallist)throws Exception{
		return RatetableLookupHandler.createDBLookupStep(context, aliasName, function, evallist);
		
	}
	
	public LookupStep createDomainTableLookup(Context context,String aliasName,List<Evaluator> evallist)throws Exception{
		return DomaintableLookupHandler.createDomainTableLookup(context, aliasName, evallist);
	}
	
	public String createInterpolateLookupStep(Context context,String aliasName,String function,List<Evaluator> evallist,List<String> interPolatedKeyName)throws Exception{
		return RateTableInterpolateLookupHandler.createInterPolateLookupSteps(context, aliasName, function, evallist,interPolatedKeyName);
	}
	
	public String createLessThanOREqualLookupStep(Context context,String aliasName,String function,List<Evaluator> evallist,List<String> leqKeyList)throws Exception{
		return RatetableLEQLookupHandler.createLEQLookupSteps(context, aliasName, function, evallist, leqKeyList,"LEQ");
	}
	
	public String createGreateThanOREqualLookupStep(Context context,String aliasName,String function,List<Evaluator> evallist,List<String> geqKeyList)throws Exception{
		return RatetableLEQLookupHandler.createLEQLookupSteps(context, aliasName, function, evallist, geqKeyList,"GEQ");
	}

}
