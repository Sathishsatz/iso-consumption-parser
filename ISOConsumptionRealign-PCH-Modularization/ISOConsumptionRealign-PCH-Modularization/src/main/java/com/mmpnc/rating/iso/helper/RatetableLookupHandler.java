package com.mmpnc.rating.iso.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.IdGeneration;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.icm.common.ide.models.ConstraintType;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.LookupCriteriaModel;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraintEBLeftSide;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.DivideEvaluator;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.MinusEvaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.MultiplyEvaluator;
import com.mmpnc.rating.iso.algorithm.parse.evaluators.PlusEvaluator;
import com.mmpnc.rating.iso.config.Configurer;
import com.mmpnc.rating.iso.ratetablelookup.RatingContentFileLookup;
import com.mmpnc.util.IsoConsumptionUtil;

/**
 * @author nilkanth9581
 *
 */
public class RatetableLookupHandler {
	

	@SuppressWarnings("unchecked")
	public static Object createDBLookupStep(Context context,String aliasName,String function,List<Evaluator> evallist)throws Exception{
		
		//CHECKING WHTHER THE LOOKUP IS FOR INTERPOLATE LOOKUP 
		
		context.putValue(ContextParam.MODELATTRIBUTE, false);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		//putting a map into the context which will hold the variable name and its type
		LookupNameDecider lookupNameDecider = (LookupNameDecider)context.getValue(ContextParam.LOOKUP_NAME_DECIDER);
		String lookupName =  lookupNameDecider.decideLookupName((String)context.getValue(ContextParam.SCOPE)+function);
		
		LookupStep lookupStep = new LookupStep();
		lookupStep.setName(lookupName);
		lookupStep.setRemark(Constants.LOOKUP_REMARK);
		lookupStep.setStepType(Constants.LOOKUP_STEPTYPE);
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		
		if(parentCondition != null)
		lookupStep.setParentCondition(parentCondition);
		//LOOKUPMODEL
		LookupModel globalLookupModel = new LookupModel();
		
		//This is required for global lookup and can be changed so kept in  the properties files
		globalLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		globalLookupModel.setLookupDesc("DefaultDescription");
		
		globalLookupModel.setBaseLookUpCriteria(lookupName);
		String lobValue = IsoConsumptionUtil.getInstance().getProperty(Configurer.getInstance().getBaseConfig().getLineOfBusiness().replaceAll(" ", ""));
		globalLookupModel.setBaseModel(lobValue+function);
		globalLookupModel.setLookupName(lookupName);
		globalLookupModel.name = lookupName;
		globalLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
		globalLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
		globalLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		globalLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		
		
		LookupModel localLookupModel = new LookupModel();
		localLookupModel.setCategory(Configurer.getInstance().getBaseConfig().getIcmCategoryNameForRatingLookup());
		localLookupModel.setLookupDesc("DefaultDescription");
		
		localLookupModel.setBaseLookUpCriteria(lookupName);
		localLookupModel.setBaseModel(lobValue+function);
		localLookupModel.setLookupName(lookupName);
		localLookupModel.name = lookupName;
		localLookupModel.setLookupType(Constants.LOOKUP_LOOKUP_TYPE);
		localLookupModel.setGlobalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
		localLookupModel.setAddEffectiveFilter(Constants.LOOKUP_ADD_EFFECTIVE_FILTER);
		localLookupModel.setMultipleRowsCheck(Constants.LOOKUP_MULTIPLE_ROW_CHECK);
		
		LookupCriteriaModel globalLookupCriteriaModel = new LookupCriteriaModel();
		LookupCriteriaModel localLookupCriteriaModel = new LookupCriteriaModel();
		
		FactPattern globalFactPattern = new FactPattern();
		FactPattern localLookupFactPattern = new FactPattern();
		
		int order = 1;
		
		Map<String, Object>keyValueMap = RatingContentFileLookup.getInstance().getRatingTableKeysAndValues(function);
		Map <Integer,String> ratetableKeyMap = (Map<Integer,String>)keyValueMap.get(Constants.LOOKUP_RATETABLE_KEYS);
		if(ratetableKeyMap.size() == 0)
			lookupNameDecider.addToKeyNotFoundLookupList(function);
		int index = 0;
	
		if(ratetableKeyMap.size() == evallist.size()){
			Iterator<Map.Entry<Integer,String>> itr = ratetableKeyMap.entrySet().iterator();
			
			
			while(itr.hasNext()){
				SingleFieldConstraintEBLeftSide singleFieldConstraintEBLeftSide = new SingleFieldConstraintEBLeftSide();
				Evaluator evaluator = evallist.get(index);
				index = index+1;
				LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
				//ADDED TO OVERCOME ISSUE FOUND IN
				context.putValue(ContextParam.MODELATTRIBUTE, false);
				context.putValue(ContextParam.ISSOURCESTATICVAR, false);
				evaluator.setContext(context);
				String filterLHS = evaluator.evaluate().toString();
				if(filterLHS.contains("\"")){
					filterLHS = filterLHS.replaceAll("\"", "");
				}
				//HANDLING EXPRESSION IN THE RATETABLE ARGS
				if(evaluator instanceof PlusEvaluator || evaluator instanceof MinusEvaluator || evaluator instanceof MultiplyEvaluator || evaluator instanceof DivideEvaluator){
					String expression  = filterLHS;
					ArithmaticStepCreater arithmaticStepCreater = ArithmaticStepCreater.getInstance();
					int localCounter = locHolder.getNextLocVarCounter();
					//localCounter = localCounter+1;
					filterLHS = "localVariable"+localCounter;
					ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition, filterLHS, expression, null);
					List<AlgoStep> steps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
					steps.add(arithmeticStep);
					//When ever there is a plus operator or any expression operator used in right
					//then it is going to be the local variable and not the module attribute
					context.putValue(ContextParam.MODELATTRIBUTE, false);
					
				}
				//singleFieldConstraintEBLeftSide.setFieldName(filterLHS);
				singleFieldConstraintEBLeftSide.setOperator(Constants.LOOKUP_OPERATOR);
				//singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_RUNTIME_VARIABLE);
				
				String keyName = itr.next().getValue();
				//singleFieldConstraintEBLeftSide.setValue(filterValue);
				singleFieldConstraintEBLeftSide.setOrderNo(order);
				
				
				boolean isModelAttribtue = false;
				boolean isSourceStaticValue = false;
				Object sourcestaticVal = context.getValue(ContextParam.ISSOURCESTATICVAR);
				Object modelAttr = context.getValue(ContextParam.MODELATTRIBUTE);
				
				if(sourcestaticVal!= null){
					isSourceStaticValue = (Boolean)sourcestaticVal;
				}
				if(modelAttr != null){
					isModelAttribtue = (Boolean)modelAttr;
				}
				
				if(isModelAttribtue){
					
					singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_M);
					//singleFieldConstraintEBLeftSide.setValue(filterLHS);
					singleFieldConstraintEBLeftSide.setFieldName(keyName);
					String sourceObject = filterLHS.replace(".", "~").split("~")[0];
					singleFieldConstraintEBLeftSide.setSourceObject(sourceObject);
					String sourceObjectAttr = filterLHS.replace(sourceObject+".", "").trim();
					singleFieldConstraintEBLeftSide.setSourceObjectAttr(sourceObjectAttr);
					singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_UNDEFINED);
					singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
					globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
				}
				else if(isSourceStaticValue){
					
					singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_S);
					singleFieldConstraintEBLeftSide.setValue(filterLHS);
					singleFieldConstraintEBLeftSide.setFieldName(keyName);
					singleFieldConstraintEBLeftSide.setConstraintValueType(ConstraintType.TYPE_LITERAL);
					singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_INDICATOR);
					globalFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
				}
				else{
					singleFieldConstraintEBLeftSide.setValueTypeInd(Constants.LOOKUP_VALUE_TYPE_INDICATOR_A);
					singleFieldConstraintEBLeftSide.setValue(filterLHS);
					singleFieldConstraintEBLeftSide.setFieldName(keyName);
					singleFieldConstraintEBLeftSide.setLocalIndicator(Constants.LOOKUP_TYPE_LOCAL_INDICATOR);
					localLookupFactPattern.addConstraint(singleFieldConstraintEBLeftSide);
				}
				order++;
				
				String lookupFilterID = Integer.toString(order) +lookupName; 
				String id= IdGeneration.idGenerator(lookupFilterID);
				singleFieldConstraintEBLeftSide.setId(id);
			}
		}else{
			throw new ParserException("ERROR:KEYS AND VALUE LIST MISMATCHES WHILE CREATING LOOKUP STEP FOR TABLE="+function);
		}
		
		
		//if(globalFactPattern.getFieldConstraints().length>0){
			globalLookupModel.setLookupCriteriaModel(globalLookupCriteriaModel);
			globalLookupCriteriaModel.addLhsItem(globalFactPattern);
			LookupModels lookupModels = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
			//ADDING GLOBAL LOOKUP MODEL INTO THE LOOKUP MODEL LIST
			lookupModels.add(globalLookupModel);
		//}else{
			
		//}
		
		//context.putValue(ContextParam.GLOBALLOOKUPMODLELIST, lookupModels);
		
		//LookupModel lookupModel2 = new LookupModel();
		Attribute assignedAttribute = new Attribute();
		
		List<Attribute> assignedAttributes = new ArrayList<Attribute>();
		assignedAttributes.add(assignedAttribute);
		//SETTING VALUES TO ASSIGNED ATTRIBUTE
		assignedAttribute.setName(keyValueMap.get(Constants.LOOKUP_RATETABLE_VALUE).toString());
		assignedAttribute.setAlias(aliasName);
		assignedAttribute.setLocalIndicator(Constants.LOOKUP_ASSIGNED_TYPE_LOCATION_INDICATOR);
		
		//ADDING ID TO THE RETURN ATTRIBUTE
		assignedAttribute.setId(IdGeneration.idGenerator(lookupName));
		globalLookupModel.setAssignedAttributes(assignedAttributes);
		localLookupModel.setAssignedAttributes(assignedAttributes);
		if(localLookupFactPattern.getFieldConstraints().length>0){
			localLookupCriteriaModel.addLhsItem(localLookupFactPattern);
		}
		//IPattern ip [] = lookupModel.lhs;
		//lookupModel2.setLookupCriteriaModel(localLookupCriteriaModel);
		String lookupAliasName = lookupName+aliasName;
		String lookupId = IdGeneration.idGenerator(lookupAliasName);
		
		
		
		globalLookupModel.setLookuoID(lookupId);
		localLookupModel.setLookuoID(lookupId);
		
		localLookupModel.setLookupCriteriaModel(localLookupCriteriaModel);
		//lookupModel2.setLookuoID(lookupId);
		
		//Setting id for LookupCriteria 
		lookupStep.setLookupmodel(localLookupModel);
		return lookupStep;
		
	}
}
