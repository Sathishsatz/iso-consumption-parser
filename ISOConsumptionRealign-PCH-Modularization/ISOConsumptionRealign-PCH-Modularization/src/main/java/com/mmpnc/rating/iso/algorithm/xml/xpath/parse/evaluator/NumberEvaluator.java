package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class NumberEvaluator implements Evaluator {
	private String number;
	private Context context;
	
	public NumberEvaluator(String string) {
		this.number = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		StringBuffer buffer = (StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		buffer.append(this.number);
		return Type.CONSTANT;
	}

}
