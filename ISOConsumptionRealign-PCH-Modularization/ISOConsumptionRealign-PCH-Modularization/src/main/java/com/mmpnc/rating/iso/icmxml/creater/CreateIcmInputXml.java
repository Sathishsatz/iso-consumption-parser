package com.mmpnc.rating.iso.icmxml.creater;

import java.util.ArrayList;
import java.util.List;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.rating.iso.algorithm.parse.AlgorithmParser;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.LookupNameDecider;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author nilkanth9581
 *
 */
public class CreateIcmInputXml implements CreateRatingEngineSteps{
	
	private static CreateIcmInputXml createIcmInputXml = null;
	private LookupModels lookupModels ;
	
	
	private CreateIcmInputXml(){
		
	}
	
	public static CreateIcmInputXml getInstance(){
		if (createIcmInputXml == null){
			createIcmInputXml = new CreateIcmInputXml();
		}
		return createIcmInputXml;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void createICMInputXml(String pchName,IcmRatingNodeContent icmRatingNodeContent,StringBuffer languagetext,Database dsDatabase,String scopePath,Database dsForLoop,LookupNameDecider lookupNameDecider,LocalVariableCounterHolder localVariableCounterHolder) throws Exception {
		
		lookupModels = new LookupModels();
		
		Context context = new ContextBase();
		//Parent path is needed for the xpath calculation
		context.putValue(ContextParam.SCOPE, scopePath);
		context.putValue(ContextParam.OBJECTDATABASE, dsDatabase);
		context.putValue(ContextParam.OBJECTBASEFORLOOP, dsForLoop);
		context.putValue(ContextParam.CURRENTPROCESS, pchName);
		context.putValue(ContextParam.LOOKUP_NAME_DECIDER, lookupNameDecider);
		context.putValue(ContextParam.LOCAL_VARIABLE_COUNTER, localVariableCounterHolder);
		
		
		//Adding a list of variables used in the pch statements
		List<String> pchRefFromCurrentProcess = new ArrayList<String>();
		
		context.putValue(ContextParam.PCHREFFROMCURRENTPROCESS, pchRefFromCurrentProcess);
		
		AlgorithmParser parse = new AlgorithmParser(context, languagetext);
		//THIS CONDITION IS ADDED TO CHECK WHETHER THE SCOPE HAS ANY STEPS OR NOT
		//IF NOT THEN JUST DO NOT PARSE THAT ALGORITHM
		//System.out.println(languagetext);
		if (languagetext.length() != 0) 
		parse.execute();
		
		List<AlgoStep> algoSteps =(List<AlgoStep>)context.getValue(ContextParam.STEPS);
		//ADDING STEPS TO THE ICMRATINGCONTENT MODEL
		
		LookupModels lookupModels1 = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
		if(lookupModels1.getLookupModels() != null)
		lookupModels.getLookupModels().addAll(lookupModels1.getLookupModels());
		
		if(icmRatingNodeContent.getPchNameAndAlgoSteps().get(pchName)!= null)
			icmRatingNodeContent.getPchNameAndAlgoSteps().get(pchName).addAll(algoSteps);
			
		
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void createICMInputXml(IcmRatingNodeContent icmRatingNodeContent,StringBuffer languagetext,Database dsDatabase,String scopePath,Database dsForLoop) throws Exception {
		
		lookupModels = new LookupModels();
		
		Context context = new ContextBase();
		//Parent path is needed for the xpath calculation
		context.putValue(ContextParam.SCOPE, scopePath);
		context.putValue(ContextParam.OBJECTDATABASE, dsDatabase);
		context.putValue(ContextParam.OBJECTBASEFORLOOP, dsForLoop);
		
		AlgorithmParser parse = new AlgorithmParser(context, languagetext);
		//THIS CONDITION IS ADDED TO CHECK WHETHER THE SCOPE HAS ANY STEPS OR NOT
		//IF NOT THEN JUST DO NOT PARSE THAT ALGORITHM
		//System.out.println(languagetext);
		if (languagetext.length() != 0) 
		parse.execute();
		
		List<AlgoStep> algoSteps =(List<AlgoStep>)context.getValue(ContextParam.STEPS);
		//ADDING STEPS TO THE ICMRATINGCONTENT MODEL
		
		LookupModels lookupModels1 = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
		if(lookupModels1.getLookupModels() != null)
		lookupModels.getLookupModels().addAll(lookupModels1.getLookupModels());
		
		if(icmRatingNodeContent.getAlgoStepList()!= null)
			icmRatingNodeContent.getAlgoStepList().addAll(algoSteps);
		
		
	}
	/**
	 * @param algoSteps
	 */
	//this map will hold the algorithm id and and the name of the algorithm
	
	public LookupModels getLookupModels(){
		return lookupModels;
	}
	
	
	
}
