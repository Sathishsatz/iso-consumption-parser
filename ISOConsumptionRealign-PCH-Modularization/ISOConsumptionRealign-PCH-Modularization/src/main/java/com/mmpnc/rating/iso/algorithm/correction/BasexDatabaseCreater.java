package com.mmpnc.rating.iso.algorithm.correction;

import java.util.HashMap;
import java.util.Map;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public class BasexDatabaseCreater {

	/**
	 * @param lob
	 * @return
	 */
	public static Map<BasexDatabaseNames, Database>createDataBaseForFlowChartAndTableObjects(LOB lob) throws BasexException{
		
		Map<BasexDatabaseNames,Database> basexDatabaseMap = new HashMap<BasexDatabaseNames, Database>();
		Database flowChartDb = null;
		Database tableObjectsDb = null;
		for(Object lobContent:lob.getContent())
		{
			
			if(lobContent instanceof Reference){
				Reference ref = (Reference)lobContent;
				
				if (((Reference) ref).getName().equals("FlowChart")) {
					
					Reference reference =  (Reference)ref;
					String refInXml = null;
					try {
						refInXml = JAXBUtils.writeFromObject(reference);
					} catch (Exception e) {
						ExceptionHandler.raiseBasexException("Exception while creating FlowChart database",e);
					}
					
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("FLWOCHARTDATABSE", refInXml);

					flowChartDb = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMEFLOWCHARTDB, xmlMap);
					flowChartDb.buildDatabase();
					
					basexDatabaseMap.put(BasexDatabaseNames.RUNTIMEFLOWCHARTDB, flowChartDb);
					//break;
				}
				if (((Reference) ref).getName().equals("Table Objects")) {
					
					Reference reference =  (Reference)ref;
						//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
					String refInXml = null;
					try {
						refInXml = JAXBUtils.writeFromObject(reference);
					} catch (Exception e) {
						ExceptionHandler.raiseBasexException("Exception while creating Table Objects database",e);
					}
					
					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("TABLEOBJECTSDB", refInXml);

					tableObjectsDb = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.RUNTIMETABLEOBJECTSDB, xmlMap);
					tableObjectsDb.buildDatabase();
					basexDatabaseMap.put(BasexDatabaseNames.RUNTIMETABLEOBJECTSDB, tableObjectsDb);
					//break;
				}
				
			}

		}
		if (flowChartDb == null) {
			ExceptionHandler.raiseBasexException("Unable to create Flowchart Basex Database Please check input algorithm file for FlowChart reference",new RuntimeException());
		}
		if (tableObjectsDb == null) {
			ExceptionHandler.raiseBasexException("Unable to create TableObjects Basex Database Please check input algorithm file for TableObjects reference",new RuntimeException());
		}
		return basexDatabaseMap;
	}
	
 
	
	public static Database createCurrentScopeDtabase(Scope scope) throws BasexException{
		//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
		Database currentReferenceDatabase = null;
		String refInXml = null;
		try {
			refInXml = JAXBUtils.writeFromObject(scope);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, String> xmlMap = new HashMap<String, String>();
		xmlMap.put("CURRENTSCOPE", refInXml);
	
		currentReferenceDatabase = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.CURRENTSCOPEDB,xmlMap);
		currentReferenceDatabase.buildDatabase();
		
		return currentReferenceDatabase;
	}
	
	public static Database createLOBDtabase(LOB lob){
		//JAXBContext jaxbContext = JAXBContext.newInstance(Reference.class);
		Database currentReferenceDatabase = null;
		String refInXml = null;
		try {
			refInXml = JAXBUtils.writeFromObject(lob);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, String> xmlMap = new HashMap<String, String>();
		xmlMap.put("LOBDATABASE", refInXml);
	
		currentReferenceDatabase = DBFactoryBuilder.getXMLDatabase(BasexDatabaseNames.CURRENTSCOPEDB,xmlMap);
		currentReferenceDatabase.buildDatabase();
		
		return currentReferenceDatabase;
		
		
		
	}
	
	
}
