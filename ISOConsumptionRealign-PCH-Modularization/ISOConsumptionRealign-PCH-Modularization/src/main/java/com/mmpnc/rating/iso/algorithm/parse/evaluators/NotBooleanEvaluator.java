package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class NotBooleanEvaluator implements Evaluator {

	private Context context;
	private Evaluator condition;
	
	public NotBooleanEvaluator(Evaluator bool) {
		this.condition = bool;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		condition.setContext(context);
		return new StringBuffer("( ! ("+condition.evaluate()+" ) )");
	}


}
