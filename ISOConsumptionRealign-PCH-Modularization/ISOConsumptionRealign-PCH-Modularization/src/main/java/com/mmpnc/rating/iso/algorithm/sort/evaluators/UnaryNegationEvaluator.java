package com.mmpnc.rating.iso.algorithm.sort.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.IEvaluatorValidator;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.ParserException;

public class UnaryNegationEvaluator implements Evaluator, IEvaluatorValidator {
	private Context context;
	private Evaluator right;
	
	public UnaryNegationEvaluator(Evaluator e1) {
		this.right = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate()throws RecognitionException {
		validate();
		StringBuffer returnValue = new StringBuffer();
		
		returnValue.append("-");
		
		this.right.setContext(context);
		Object negationObject = this.right.evaluate(); 
		if(ExceptionHandler.checkNull(negationObject)){
			ExceptionHandler.raiseParserException("RHS can not be NULL in Unary Negation Evaluator");
		}
		returnValue.append(negationObject).append(" ");
		
		return returnValue;
		
	}

	@Override
	public void validate() throws RecognitionException {
		if(this.right == null){
			throw new ParserException("Right evaluator is NULL in UnaryNegation evaluator");
		}		
	}

}
