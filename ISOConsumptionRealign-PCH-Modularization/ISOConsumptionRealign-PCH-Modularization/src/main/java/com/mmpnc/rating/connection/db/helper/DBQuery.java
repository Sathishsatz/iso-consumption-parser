package com.mmpnc.rating.connection.db.helper;

/**
 * @author nilkanth9581
 *
 */
public interface DBQuery {
	DBQuery createDbQuery(String query);
	Object executeDBQuery();
}
