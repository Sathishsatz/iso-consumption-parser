package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.ArithmeticStep;
import com.mmpnc.icm.common.ide.models.IFStatementStep;
import com.mmpnc.icm.common.ide.models.LookupStep;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.AssignRhsXpathPredicateUtil;
import com.mmpnc.rating.iso.helper.ArithmaticStepCreater;
import com.mmpnc.rating.iso.helper.LocalVariableCounterHolder;
import com.mmpnc.rating.iso.helper.SetAttrbuteStepCreater;

public class AssignmentEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public AssignmentEvaluator(Context context, Evaluator v, Evaluator e1) {
		this.context = context;
		this.left = v;
		this.right = e1;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object evaluate()throws RecognitionException{

		left.setContext(context);
		String LHS = left.evaluate().toString();

		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		//PUTTING A FLAG IN THE CONTEXT TO INDICATE THAT THIS XPATH IS OF ASSIGN RHS STMT
		context.putValue(ContextParam.ISASSIGNEMENTRHSXPATH, true);
		right.setContext(context);
		
		boolean lhsModelAttrFlag = (Boolean) context.getValue(ContextParam.MODELATTRIBUTE);
		//PUTTING WHETHER THE VARIABLE IS LOOCAL OR MODEL ATTRIBUTE
		Object object = right.evaluate();
		context.putValue(ContextParam.ISASSIGNEMENTRHSXPATH, false);
		
		Object isAssignmetnRhsHasPredicateObj = context.getValue(ContextParam.IS_ASSIGNSTATEMENTRHSXPATH);
		
		if(isAssignmetnRhsHasPredicateObj == null)
			isAssignmetnRhsHasPredicateObj = false;
		Boolean isAssignmetnRhsHasPredicate = (Boolean)isAssignmetnRhsHasPredicateObj;
		
		//THIS HANDLING IS ADDED IF ASSIGN RHS HAS PREDICATE IN IT
		IFStatementStep ifStatementStep = null;
		
		
		boolean rhsModelAttrFlag =(Boolean) context.getValue(ContextParam.MODELATTRIBUTE);
		//boolean isRhsLocalvariable = (Boolean)context.getValue(ContextParam.ISSOURCESTATICVAR);
		String RHS = getRHS(object);
		
		//IF VAR IS A MODEL AND IS CALLED FROM ASSIGNMENT EVALUATOR WE NEED TO CREATE
		//SETATTRIBUTE STEP AND RETURN ALIAS NAME OF THE SET ATTRIBUTE STEP
			List<AlgoStep> stepList = (List<AlgoStep>) context.getValue(ContextParam.STEPS);
			SetAttrbuteStepCreater setAttrbuteStepCreater = SetAttrbuteStepCreater.getInstance();
			ArithmaticStepCreater arithmaticStepCreater = ArithmaticStepCreater.getInstance();
			
			
			
			if(lhsModelAttrFlag && RHS!= null &&  RHS.startsWith("local")){
				LookupStep lookupStep  = setAttrbuteStepCreater.createSetAttributeStep(LHS,RHS,parentCondition,rhsModelAttrFlag);
				AssignRhsXpathPredicateUtil.createAndAddSetAttributeStep(lookupStep, isAssignmetnRhsHasPredicate, ifStatementStep, stepList);
			}
			else if(lhsModelAttrFlag){
				//IN CASE OF SPECIAL FUNCTION (WHICH NEED PRECISIONS)
				//Nullifying precision scale as we do not need this for other expression steps
				LocalVariableCounterHolder locHolder = (LocalVariableCounterHolder)context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER);
				String aliasName = "localVariable"+locHolder.getNextLocVarCounter();
				ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition,aliasName,RHS,null);
				LookupStep lookupStep  = setAttrbuteStepCreater.createSetAttributeStep(LHS,aliasName,parentCondition,rhsModelAttrFlag);
				AssignRhsXpathPredicateUtil.createAndAddExpressionAndSetAttributeStep(lookupStep, isAssignmetnRhsHasPredicate, ifStatementStep, stepList, arithmeticStep);
			}
			else{
				ArithmeticStep arithmeticStep = arithmaticStepCreater.createArithmeticStep(parentCondition,LHS,RHS,null);
				AssignRhsXpathPredicateUtil.createAndAddExpressionStep(arithmeticStep, isAssignmetnRhsHasPredicate, ifStatementStep, stepList);
			}
			
		  context.putValue(ContextParam.MODELATTRIBUTE, false);
		
		return null;
	}
	
	/**
	 * @param evalReturn
	 * @return
	 * @TODO NEED TO CHECK THIS AS SOME OF THE EVALUATOR RETURNING STRING AND SOME RETURNING STRINGBUFFER
	 */
	private String getRHS(Object evalReturn){
		String RHS = null;
		if(evalReturn instanceof String){
			RHS = evalReturn.toString();
		}else{
			RHS = ((StringBuffer)evalReturn).toString();
		}
		return RHS;
	}
	

}
