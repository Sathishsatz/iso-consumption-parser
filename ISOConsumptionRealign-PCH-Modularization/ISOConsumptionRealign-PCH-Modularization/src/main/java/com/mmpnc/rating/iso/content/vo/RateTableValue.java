//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.02.26 at 02:36:07 PM IST 
//


package com.mmpnc.rating.iso.content.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for RateTableValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RateTableValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RateTableId" type="{http://www.w3.org/2001/XMLSchema}ID"/>
 *         &lt;element name="ColumnId" type="{http://www.w3.org/2001/XMLSchema}ID"/>
 *         &lt;element name="ColumnName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Size" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Scale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateTableValue", propOrder = {
    "rateTableId",
    "columnId",
    "columnName",
    "type",
    "size",
    "scale",
    "TableName",
    "TableDescription",
    "ColumnType",
    "Type",
    "ColumnDescription",
})
public class RateTableValue {

    @XmlElement(name = "RateTableId", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String TableId;
    @XmlElement(name = "ColumnId", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String ColumnId;
    @XmlElement(name = "ColumnName", required = true)
    protected String ColumnName;
    @XmlElement(name = "Type", required = true)
    protected String TableType;
    @XmlElement(name = "Size")
    protected String Size;
    @XmlElement(name = "Scale")
    protected String Scale;
    @XmlElement(name = "TableName")
    protected String TableName;
    @XmlElement(name = "TableName")
    protected String TableDescription;
    @XmlElement(name = "ColumnType")
    protected String ColumnType;
    @XmlElement(name = "Type")
    protected String Type;
    @XmlElement(name = "ColumnDescription")
    protected String ColumnDescription;
    

    public String getColumnDescription() {
		return ColumnDescription;
	}

	public void setColumnDescription(String columnDescription) {
		ColumnDescription = columnDescription;
	}

	public String getTableId() {
		return TableId;
	}

	public void setTableId(String tableId) {
		TableId = tableId;
	}

	public String getTableType() {
		return TableType;
	}

	public void setTableType(String tableType) {
		TableType = tableType;
	}

	public String getTableName() {
		return TableName;
	}

	public void setTableName(String tableName) {
		TableName = tableName;
	}

	public String getTableDescription() {
		return TableDescription;
	}

	public void setTableDescription(String tableDescription) {
		TableDescription = tableDescription;
	}

	public String getColumnType() {
		return ColumnType;
	}

	public void setColumnType(String columnType) {
		ColumnType = columnType;
	}

	/**
     * Gets the value of the rateTableId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateTableId() {
        return TableId;
    }

    /**
     * Sets the value of the rateTableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateTableId(String value) {
        this.TableId = value;
    }

    /**
     * Gets the value of the columnId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnId() {
        return ColumnId;
    }

    /**
     * Sets the value of the columnId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnId(String value) {
        this.ColumnId = value;
    }

    /**
     * Gets the value of the columnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnName() {
        return ColumnName;
    }

    /**
     * Sets the value of the columnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnName(String value) {
        this.ColumnName = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return TableType;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.TableType = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSize() {
        return Size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSize(String value) {
        this.Size = value;
    }

    /**
     * Gets the value of the scale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScale() {
        return Scale;
    }

    /**
     * Sets the value of the scale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScale(String value) {
        this.Scale = value;
    }

}
