package com.mmpnc.rating.iso.publish.lookup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mmpnc.icm.common.Attribute;
import com.mmpnc.icm.common.ide.models.CompositeFieldConstraint;
import com.mmpnc.icm.common.ide.models.FactPattern;
import com.mmpnc.icm.common.ide.models.IConstraint;
import com.mmpnc.icm.common.ide.models.IPattern;
import com.mmpnc.icm.common.ide.models.LookupCriteriaModel;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.SingleFieldConstraint;

/**
 * @author nilkanth9581
 *
 */
public class LookupDAO  implements ILookupDAO{
	 LookupCriteriaModel lookupCriteriaModel;
	 List<SingleFieldConstraint> singleList = new ArrayList<SingleFieldConstraint>();
	 
	 HashMap<SingleFieldConstraint, String> operationMap = new HashMap<SingleFieldConstraint, String>();
	 
	SingleFieldConstraint singleFieldConstraint = new SingleFieldConstraint();
	

	private static String DEL_SQL_LK_C = "delete from LOOKUP_CRITERIA where CRITERIA_ID=?";
	private static String DEL_SQL_LK_F= "delete from LOOKUP_CRITERIA_FILTER where CRITERIA_ID=?";
	private static String DEL_SQL_LK_RET= "delete from LOOKUP_CRTR_RETURN_ATTRIBUTES where CRITERIA_ID=?";
	
	private static String INS_SQL_LK_C = "insert into LOOKUP_CRITERIA (CRITERIA_ID,CRITERIA_NAME,CRITERIA_DESCRIPTION,GLOBAL_LOCAL_INDICATOR,LOOKUP_TYPE,TARGET_OBJECT,ADD_EFFECTIVE_FILTER,CREATED_ON,CREATED_BY,MODIFIED_ON,MODIFIED_BY,MULTIPLE_ROWS) values(?,?,?,?,?,?,?,?,?,?,?,?)";
	private static String INS_SQL_LK_F = "insert into LOOKUP_CRITERIA_FILTER (CRITERIA_ID,CRITERIA_FILTER_ID,LOOKUP_ORDER,TARGET_OBJECT_ATTRIBUTE,OPERATOR,SOURCE_OBJECT,SOURCE_OBJECT_ATTRIBUTE,SOURCE_STATIC_VALUE,SOURCE_VARIABLE,CREATED_ON,CREATED_BY,MODIFIED_ON,MODIFIED_BY,VALUE_TYPE_INDICATOR) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static String INS_SQL_LK_R = "insert into LOOKUP_CRTR_RETURN_ATTRIBUTES (CRITERIA_ID,RETURN_ATTRIBUTE_ID,RETURN_ATTRIBUTE,STORE_AS_ALIAS_NAME,CREATED_ON,CREATED_BY,MODIFIED_ON,MODIFIED_BY,DEFAULT_VALUE) values(?,?,?,?,?,?,?,?,?)";
	
	
	Connection con ;
	String userName;
	
	public static ILookupDAO dao = new LookupDAO();
	
	public LookupDAO(Connection connection,String userName) {
		this.con = connection;
		this.userName = userName;
	}

	public LookupDAO() {
	}

	public static ILookupDAO getInstance(){
		return dao;
	}
	
	@Override
	public void insertLookupCriteria(LookupModel model) {
		this.lookupCriteriaModel = model.lookupCriteriaModel;
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(INS_SQL_LK_C);
			ps.setString(1, model.getLookuoID());
			ps.setString(2, model.getLookupName());
			if (model.getLookupDesc() != null && !"".equals(model.getLookupDesc())) {
				ps.setString(3, model.getLookupDesc().trim());
			} else {
				ps.setString(3, " ");
			}
			
			ps.setString(4, model.getGlobalIndicator());
			ps.setString(5, model.getLookupType());
			ps.setString(6, model.getBaseModel());
			ps.setString(7, model.getAddEffectiveFilter());
			java.sql.Date sqlDate = new java.sql.Date(new java.util.Date()
					.getTime());
			ps.setDate(8, sqlDate);
			ps.setString(9, userName);
			ps.setDate(10, null);
			ps.setString(11, null);
			ps.setString(12, model.getMultipleRowsCheck());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResource(null, ps);
		}
	}
	
	@Override
	public void insertLookupCriteriaFilter(LookupModel model) {
		this.lookupCriteriaModel = model.lookupCriteriaModel;
		PreparedStatement ps = null;
		// Connection con = null;
		try {
			// con = DataSource.getConneciton();
			if(lookupCriteriaModel==null){
				return;
			}
			IPattern[] ft = lookupCriteriaModel.lhs;
			
			if (ft != null && ft.length > 0) {
				for (IPattern iPattern : ft) {
					FactPattern factPattern = (FactPattern) iPattern;
					CompositeFieldConstraint compositeFieldConstraint = factPattern.constraintList;
					if (compositeFieldConstraint != null) {

						IConstraint singleFieldConstraints[] = (IConstraint[]) compositeFieldConstraint.constraints;
						for (IConstraint iConstraint : singleFieldConstraints) {
							if (iConstraint instanceof SingleFieldConstraint) {
								SingleFieldConstraint singleFieldConstraint = (SingleFieldConstraint) iConstraint;
								this.singleFieldConstraint = singleFieldConstraint;
								ps = null;
								ps = con.prepareStatement(INS_SQL_LK_F);
								ps.setString(1, model.getLookuoID());
								ps.setString(2, singleFieldConstraint.getId());
								int i = singleFieldConstraint.getOrderNo();
								ps.setString(3, Integer.toString(i));
								ps.setString(4, singleFieldConstraint										.getFieldName());
								ps.setString(5,
										getOperator(singleFieldConstraint
												.getOperator()));
								if (singleFieldConstraint.getSourceObject() != null) {
									ps.setString(6, singleFieldConstraint
											.getSourceObject());
									ps.setString(7, singleFieldConstraint
											.getSourceObjectAttr());
								} else {
									ps.setString(6, null);
									ps.setString(7, null);
								}

								if (singleFieldConstraint.getValue() != null) {
									ps.setString(8, singleFieldConstraint
											.getValue().trim());
								} else  {
									ps.setString(8, null);
								}
								
								ps.setString(9, null);
								java.sql.Date sqlDate = new java.sql.Date(
										new java.util.Date().getTime());
								ps.setDate(10, sqlDate);
								ps.setString(11, userName);
								ps.setDate(12, null);
								ps.setString(13, null);
								ps.setString(14, singleFieldConstraint
										.getValueTypeInd());
								ps.executeUpdate();

							}

						}
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResource(null, ps);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void insertLookupCriteriaReturnAttr(LookupModel model) {
		PreparedStatement ps = null;
		try {
			if(model.getAssignedAttributes()==null){
				return;
			}
			List<Attribute> list = model.getAssignedAttributes();
			for (Attribute attr : list) {

				ps = con.prepareStatement(INS_SQL_LK_R);
				ps.setString(1, model.getLookuoID());
				ps.setString(2, attr.getId());
				if (attr.getName() != null) {
					ps.setString(3, attr.getName().trim());
				} else {
					ps.setString(3, attr.getName());
				}
				if (attr.getName() != null) {
					ps.setString(4, attr.getAlias().trim());
				} else {
					ps.setString(4, attr.getAlias());
				}
				
				java.sql.Date sqlDate = new java.sql.Date(new java.util.Date()
						.getTime());
				ps.setDate(5, sqlDate);
				ps.setString(6, userName);
				ps.setDate(7, null);
				ps.setString(8, null);
				ps.setString(9, null);

				ps.executeUpdate();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResource(null, ps);
		}
	}
	
	/*@Override
	public void delete_all(LookupModel lookupModel) {


		PreparedStatement ps=null;
		Connection con=null;
		try{
			con = DataSource.getConneciton();
			
			ps = con.prepareStatement( DEL_SQL_LK_C );
			ps.executeUpdate();
			ps =null;
			
			ps = con.prepareStatement( DEL_SQL_LK_F );
			ps.executeUpdate();
			ps =null;
			
			ps = con.prepareStatement( DEL_SQL_LK_RET );
			ps.executeUpdate();
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
		finally{
			closeResource(null,ps);
		}

	}*/
	
	
	@Override
	public void deleteLookupCriteria(LookupModel lookupModel) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DEL_SQL_LK_C);
			ps.setString(1, lookupModel.getLookuoID());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResource(null, ps);
		}
	}
	
	@Override
	public void deleteLookupCriteriaFilter(LookupModel lookupModel) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DEL_SQL_LK_F);
			ps.setString(1, lookupModel.getLookuoID());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResource(null, ps);
		}
	}
	
	@Override
	public void deleteLookupCriteriaReturnAttr(LookupModel model) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(DEL_SQL_LK_RET);
			ps.setString(1, model.getLookuoID());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResource(null, ps);
		}
	}

	/*@Override
	public LookupModel selectLookupCriteria(LookupModel lookupModel){
		
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs=null;
		try{
			LookupModel testLookup = new LookupModel();
			con = DataSource.getConneciton();
			ps= con.prepareStatement(SEL_SQL);
			ps.setString(1, lookupModel.getLookuoID());
			rs=ps.executeQuery();
			while(rs.next()){
				testLookup.setLookuoID(rs.getString(1));
				testLookup.setLookupName(rs.getString(2));
				testLookup.setLookupDesc(rs.getString(3));
			}
			return testLookup;
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
		finally{
			
			closeResource(null, ps);
		}
		
		
	}*/
	
	
	
	/*@Override
	public void selectLookupCriteriaFilter(LookupModel model){
		
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		try{
			con = DataSource.getConneciton();
			ps= con.prepareStatement(SEL_SQL_FIL);
			ps.setString(1, model.getLookuoID());
			rs=ps.executeQuery();
			
		}
		catch(Exception e){
			
			throw new RuntimeException(e);
		}
		finally{
			closeResource(null, ps);
		}
		
		
	}*/
	
	
	/*@Override
	public void insert(LookupModel model ) {
		this.lookupCriteriaModel = model.lookupCriteriaModel;
		PreparedStatement ps = null;
		Connection con = null;
		try{
			con = DataSource.getConneciton();
			ps = con.prepareStatement( INS_SQL_LK_C );
			ps.setString(1,model.getLookuoID());
			ps.setString(2, model.getLookupName());
			ps.setString(3, model.getLookupDesc());
			ps.setString(4, model.getGlobalIndicator());
			ps.setString(5, model.getLookupType());
			ps.setString(6, model.getBaseModel());
			ps.setString(7, model.getAddEffectiveFilter());
			ps.setString(8, "12-02-2012");
			ps.setString(9, "USER");
			ps.setString(10, "12-02-2012");
			ps.setString(11, "USER");
			ps.setString(12, model.getMultipleRowsCheck());
			ps.executeUpdate();
			
			
			IPattern[] ft = lookupCriteriaModel.lhs;
			if(ft!=null && ft.length>0){
				for(IPattern iPattern:ft)
				{
					FactPattern factPattern=(FactPattern)iPattern;
					CompositeFieldConstraint compositeFieldConstraint=factPattern.constraintList;
					if(compositeFieldConstraint!=null)
					{
						
						IConstraint singleFieldConstraints[]=
								(IConstraint[])compositeFieldConstraint.constraints;
						for(IConstraint iConstraint:singleFieldConstraints)
						{
							if(iConstraint  instanceof SingleFieldConstraint){
								SingleFieldConstraint singleFieldConstraint=(SingleFieldConstraint)iConstraint;
								this.singleFieldConstraint = singleFieldConstraint;
								ps = null;
								ps = con.prepareStatement( INS_SQL_LK_F );
								ps.setString(1,model.getLookuoID());
								ps.setString(2, singleFieldConstraint.getId());
								int i = singleFieldConstraint.getOrderNo();
								ps.setString(3, Integer.toString(i));
								ps.setString(4, singleFieldConstraint.getFieldName());
								ps.setString(5, singleFieldConstraint.getOperator());
								if(singleFieldConstraint.getSourceObject()!=null){
								ps.setString(6, singleFieldConstraint.getSourceObject());
								ps.setString(7, singleFieldConstraint.getSourceObjectAttr());
								}
								
								ps.setString(8, singleFieldConstraint.getValue());
								ps.setString(9, null);
								ps.setString(10, "12-02-2012");
								ps.setString(11, "USER");
								ps.setString(12, "12-02-2012");
								ps.setString(13, "USER");
								ps.setString(14, singleFieldConstraint.getValueTypeInd());
								ps.executeUpdate();
								
							}
							
						}
					}
				}
			}
			
			List<Attribute> list = model.getAssignedAttributes();
			for(Attribute attr :list){
			
				
			
			ps = null;
			ps = con.prepareStatement( INS_SQL_LK_R );
			ps.setString(1,model.getLookuoID());
			ps.setString(2, attr.getId());
			ps.setString(3, attr.getName());
			ps.setString(4, attr.getAlias());
			ps.setString(5, "12-02-2012");
			ps.setString(6, "USER");
			ps.setString(7, "12-02-2012");
			ps.setString(8, "USER");
			ps.setString(9, null);
			ps.executeUpdate();
			
			}
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
		finally{
			closeResource(null,ps);
		}
		
	}*/

	private void closeResource(ResultSet rs,Statement st){
		try{
			if (rs != null){
				rs.close();
			}
			if (st != null){
				st.close();
			}
		}
		catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	private String getOperator (String operator) {
		if (operator != null && !"".equals(operator)) {
			if ("==".equals(operator)) {
				return "=";
			}
			
		}
		return null;
	}
	
/*	@Override
	public void update(SingleFieldConstraint single,LookupModel lookupModel,Attribute attr) {
			PreparedStatement ps=null;
			Connection con=null;
			try{
				con = DataSource.getConneciton();
				if(single!=null){
				ps = con.prepareStatement( UPD_SQL_FIL );
				ps.setString(1, single.getFieldName());
				ps.setString(2, single.getId());
				ps.executeUpdate();
			}
				if(lookupModel!=null){
					
					ps = con.prepareStatement( UPD_SQL );
					ps.setString(1, lookupModel.getLookupName());
					ps.setString(2, lookupModel.getLookupDesc());
					ps.executeUpdate();
					
					
				}
				
				if(attr!=null){
					
					ps = con.prepareStatement( UPD_SQL_Ret );
					ps.setString(1, attr.getName());
					ps.setString(2, attr.getId());
					ps.executeUpdate();
				}
				
			}
			catch(Exception e){
				throw new RuntimeException(e);
			}
			finally{
				closeResource(null,ps);
			}
>>>>>>> .r47199

	}*/
	


	/*@Override
	public LookupModel find(String id) {

			PreparedStatement ps=null;
			Connection con=null;
			ResultSet rs = null;
			try{
				con = DataSource.getConneciton();
				ps = con.prepareStatement( SEL_SQL );
				ps.setString(1, id);
				rs = ps.executeQuery();
				
				LookupModel testLookup = new LookupModel();
				while(rs.next()){
					testLookup.setLookuoID(rs.getString(1));
					testLookup.setLookupName(rs.getString(2));
					testLookup.setLookupDesc(rs.getString(3));
				
				}
				return testLookup;
			}
			catch(Exception e){
				throw new RuntimeException(e);
			}
			finally{
				closeResource(rs,ps);
			}

			
		}*/
	
	/*@Override
	public SingleFieldConstraintEBLeftSide find2(String id) {

		SingleFieldConstraintEBLeftSide single = new SingleFieldConstraintEBLeftSide();

			PreparedStatement ps=null;
			Connection con=null;
			ResultSet rs = null;
			try{
				con = DataSource.getConneciton();
				ps = con.prepareStatement( SEL_SQL_FIL );
				ps.setString(1, id);
				rs = ps.executeQuery();
				
				while(rs.next()){
				
					single.setId(rs.getString(2));
					
					//single.setOrderNo(Integer.toString(rs.getString(3));
					single.setFieldName(rs.getString(4));
					single.setOperator(rs.getString(5));
					single.setSourceObject(rs.getString(6));
					single.setSourceObjectAttr(rs.getString(7));
					
				}
				return single;
			}
			catch(Exception e){
				throw new RuntimeException(e);
			}
			finally{
				closeResource(rs,ps);
			}

			
		}*/

	
	
	/*@Override
	public Attribute find3(String id) {

			PreparedStatement ps=null;
			Connection con=null;
			ResultSet rs = null;
			try{
				con = DataSource.getConneciton();
				ps = con.prepareStatement( SEL_SQL_RET );
				ps.setString(1, id);
				rs = ps.executeQuery();
				
				Attribute attr = new Attribute();
				while(rs.next()){
					attr.setId(rs.getString(2));
					attr.setName(rs.getString(3));
					attr.setAlias(rs.getString(4));
					
				}
				return attr;
			}
			catch(Exception e){
				throw new RuntimeException(e);
			}
			finally{
				closeResource(rs,ps);
			}

			
		}*/

	/*@Override
	public void delete(LookupModel lookupModel,SingleFieldConstraintEBLeftSide single,Attribute attr) {


		PreparedStatement ps=null;
		Connection con=null;
		try{
			con = DataSource.getConneciton();
			if(lookupModel!=null){
			ps = con.prepareStatement( DEL_SQL );
			ps.setString(1, lookupModel.getLookuoID());
			ps.executeUpdate();
			}
			if(single!=null){
				ps = con.prepareStatement( DEL_SQL_FIL );
				ps.setString(1, single.getId());
				ps.executeUpdate();
				
			}
			if(attr!=null){
				ps = con.prepareStatement( DEL_SQL_RET );
				ps.setString(1, attr.getId());
				ps.executeUpdate();
				
			}
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
		finally{
			closeResource(null,ps);
		}

		
	}
*/

	
	/*public List<SingleFieldConstraint> filter_list(LookupModel lookupModel) {
		this.lookupCriteriaModel = lookupModel.lookupCriteriaModel;
		IPattern[] ft = lookupCriteriaModel.lhs;
		if (ft != null && ft.length > 0) {
			for (IPattern iPattern : ft) {
				FactPattern factPattern = (FactPattern) iPattern;
				CompositeFieldConstraint compositeFieldConstraint = factPattern.constraintList;
				if (compositeFieldConstraint != null) {

					IConstraint singleFieldConstraints[] = (IConstraint[]) compositeFieldConstraint.constraints;
					for (IConstraint iConstraint : singleFieldConstraints) {
						if (iConstraint instanceof SingleFieldConstraint) {
							SingleFieldConstraint singleFieldConstraint = (SingleFieldConstraint) iConstraint;
							this.singleFieldConstraint = singleFieldConstraint;
							SingleFieldConstraint singleConstraint = new SingleFieldConstraint();
							PreparedStatement ps = null;
							Connection con = null;
							ResultSet rs = null;
							try {
								con = DataSource.getConneciton();
								ps = con.prepareStatement(SEL_SQL_FIL);
								ps.setString(1, singleFieldConstraint.getId());
								rs = ps.executeQuery();

								while (rs.next()) {
									//singleConstraint.setId(rs.getString("CRITERIA_ID"));
									singleConstraint.setId(rs.getString("CRITERIA_FILTER_ID"));
									singleConstraint.setOrderNo(rs.getInt("LOOKUP_ORDER"));
									singleConstraint.setFieldName(rs.getString("TARGET_OBJECT_ATTRIBUTE"));
									if(rs.getString("SOURCE_OBJECT")!=null){
									singleConstraint.setSourceObject(rs.getString("SOURCE_OBJECT"));
									singleConstraint.setSourceObjectAttr(rs.getString("SOURCE_OBJECT_ATTRIBUTE"));
									}
									singleConstraint.setValue("SOURCE_STATIC_VALUE");
									singleConstraint.setValueTypeInd("VALUE_TYPE_INDICATOR");
									
									
								}
								singleList.add(singleConstraint);
								return singleList;
								
							} catch (Exception e) {
								throw new RuntimeException(e);
							} finally {
								closeResource(rs, ps);
							}

						}

					}

				}
			}
		}
		return singleList;
	}
*/


	/*@Override
	public void update_filter(LookupModel lookupModel) {
		this.lookupCriteriaModel = lookupModel.lookupCriteriaModel;

		List<SingleFieldConstraint> singleFieldDBList  = filter_list(lookupModel);
		for(SingleFieldConstraint singleFieldDBConstraint : singleFieldDBList){
			IPattern[] ft = lookupCriteriaModel.lhs;
			if (ft != null && ft.length > 0) {
				for (IPattern iPattern : ft) {
					
					FactPattern factPattern = (FactPattern) iPattern;
					CompositeFieldConstraint compositeFieldConstraint = factPattern.constraintList;
					if (compositeFieldConstraint != null) {
						IConstraint singleFieldConstraints[] = (IConstraint[]) compositeFieldConstraint.constraints;
						for (IConstraint iConstraint : singleFieldConstraints) {
							if (iConstraint instanceof SingleFieldConstraint) {
								SingleFieldConstraint singleFieldConstraint = (SingleFieldConstraint) iConstraint;
								
								if(singleFieldDBConstraint.getId().equals(singleFieldConstraint.getId())){
									
								
								if(singleFieldDBConstraint.getOrderNo()!=singleFieldConstraint.getOrderNo()){
									operationMap.put(singleFieldDBConstraint, "U");
								}
								else if(!singleFieldDBConstraint.getFieldName().equals(singleFieldConstraint.getOrderNo())){
									operationMap.put(singleFieldDBConstraint, "U");
								}
								else if(!singleFieldDBConstraint.getFieldName().equals(singleFieldConstraint.getFieldName())){
									operationMap.put(singleFieldDBConstraint, "U");
								}
								else if(!singleFieldDBConstraint.getOperator().equals(singleFieldConstraint.getOperator())){
									operationMap.put(singleFieldDBConstraint, "U");
								}
								else if(!singleFieldDBConstraint.getValue().equals(singleFieldConstraint.getValue())){
									operationMap.put(singleFieldDBConstraint, "U");	
								}
								else if(!singleFieldDBConstraint.getValueTypeInd().equals(singleFieldConstraint.getValueTypeInd())){
									operationMap.put(singleFieldDBConstraint, "U");	
								}
								
								}
						
							}
						
						}
					}
					
				}
				
			}
			
		}
		
	}*/
		

}
