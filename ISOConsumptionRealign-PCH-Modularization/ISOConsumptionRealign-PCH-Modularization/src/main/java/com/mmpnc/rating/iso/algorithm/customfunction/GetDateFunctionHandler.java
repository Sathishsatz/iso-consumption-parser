package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

/**
 * @author nilkanth9581
 * THIS FUNCTION HANDLER WILL HANDLE GETDATE FUCNTION
 */
public class GetDateFunctionHandler implements FunctionHandler{

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String varaible, Context context)throws RecognitionException {
		String getYearPath = null;
		for(Evaluator eval : evallist){
			eval.setContext(context);
			 getYearPath = eval.evaluate().toString();
		}
		return "GetDate ( "+getYearPath+" )";
	}

}
