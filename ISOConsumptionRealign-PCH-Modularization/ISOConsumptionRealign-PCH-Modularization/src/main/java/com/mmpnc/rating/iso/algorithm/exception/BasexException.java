package com.mmpnc.rating.iso.algorithm.exception;

public class BasexException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public BasexException(String message, Throwable ex) {
		super(message, ex);
	}

	public BasexException(String message) {
		super(message);
	}

	public BasexException(Throwable e) {
		super(e);
	}
}
