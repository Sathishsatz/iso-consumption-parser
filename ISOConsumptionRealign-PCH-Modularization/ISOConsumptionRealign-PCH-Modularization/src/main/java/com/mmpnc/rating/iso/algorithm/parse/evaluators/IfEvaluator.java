package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextBase;
import com.mmpnc.context.ContextParam;
import com.mmpnc.icm.common.ide.models.AlgoStep;
import com.mmpnc.icm.common.ide.models.IFStatementStep;
import com.mmpnc.icm.common.ide.models.LookupModel;
import com.mmpnc.icm.common.ide.models.LookupModels;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.helper.Constants;
import com.mmpnc.rating.iso.helper.IFStepCreater;

public class IfEvaluator implements Evaluator {

	private Context context ;
	private Evaluator condition;
	private List<Evaluator> thenStatements;
	private List<Evaluator> elseStatement;
	//public static StringBuffer str = new StringBuffer();
	
	public IfEvaluator(Context context, Evaluator evaluator,
			List<Evaluator> s1, List<Evaluator> s2) {
		this.condition = evaluator;
		this.thenStatements = s1;
		this.elseStatement = s2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override 	
	public Object evaluate() throws RecognitionException {
		
		condition.setContext(context);
		context.putValue(ContextParam.IFCONDITIONXPATH, true);
		Object condObj = condition.evaluate();
		if(condObj instanceof String)
			condObj = new StringBuffer(condObj.toString());
		StringBuffer conditionStr = (StringBuffer)condObj;
		context.putValue(ContextParam.IFCONDITIONXPATH, false);
		//Setting if condition predicate value as false 
		context.putValue(ContextParam.IS_IFCONDITIONPREDICATE, false);
		String parentCondition = (String)context.getValue(ContextParam.PARENTCONDITION);
		IFStepCreater ifStatem = IFStepCreater.getInstance();
		IFStatementStep ifStatementStep = ifStatem.createIfStatement(conditionStr,parentCondition);
		//Creating a list for saving then and else steps of the algorithm
		//CREATING NEW CONTEXT FOR THE LOCAL IF STEPS
		Context ifContext = new ContextBase();
		ifContext.putValue(ContextParam.OBJECTDATABASE, context.getValue(ContextParam.OBJECTDATABASE));
		ifContext.putValue(ContextParam.OBJECTBASEFORLOOP, context.getValue(ContextParam.OBJECTBASEFORLOOP));
		ifContext.putValue(ContextParam.SCOPE, context.getValue(ContextParam.SCOPE));
		ifContext.putValue(ContextParam.LOOP_THROUGH_CURRENT_CONTEXT_LEVLE, context.getValue(ContextParam.LOOP_THROUGH_CURRENT_CONTEXT_LEVLE));
		ifContext.putValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE, context.getValue(ContextParam.LOOPTHOUGHREPLACEVARIABLE));
		ifContext.putValue(ContextParam.LOOPTHROUGHREPLACEBY,context.getValue(ContextParam.LOOPTHROUGHREPLACEBY));
		ifContext.putValue(ContextParam.CURRENTPROCESS,context.getValue(ContextParam.CURRENTPROCESS));
		ifContext.putValue(ContextParam.PCHREFFROMCURRENTPROCESS,context.getValue(ContextParam.PCHREFFROMCURRENTPROCESS));
		ifContext.putValue(ContextParam.ISLOOPTHROUGHSTATEMENTS, context.getValue(ContextParam.ISLOOPTHROUGHSTATEMENTS));
		ifContext.putValue(ContextParam.LOOKUP_NAME_DECIDER, context.getValue(ContextParam.LOOKUP_NAME_DECIDER));
		ifContext.putValue(ContextParam.LOCAL_VARIABLE_COUNTER, context.getValue(ContextParam.LOCAL_VARIABLE_COUNTER));
		for(Evaluator evaluator:thenStatements){
			evaluator.setContext(ifContext);
			ifContext.putValue(ContextParam.PARENTCONDITION, Constants.IF_THEN);
			evaluator.evaluate();
		}
		
		if(elseStatement != null){
			for(Evaluator evaluator : elseStatement){
				evaluator.setContext(ifContext);
				ifContext.putValue(ContextParam.PARENTCONDITION, Constants.IF_ELSE);
				evaluator.setContext(ifContext);
				evaluator.evaluate();
			}
		}else{
			ifStatementStep.setElseSelectKeyWord("NA");
		}
		
		//ADDING CHILD ALGO STEPS
		addChildSteps(ifContext, ifStatementStep);
		
		return null;
	}

	@SuppressWarnings("unchecked")
	private void addChildSteps(Context ifContext,IFStatementStep ifStatementStep){
		
		List<AlgoStep> ifChildStepList = (List<AlgoStep>)ifContext.getValue(ContextParam.STEPS);
		ifStatementStep.setAlgoStepList(ifChildStepList);
		LookupModels ifStepLookups = (LookupModels)ifContext.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
		
		if(ifStepLookups.getLookupModels().size() > 0){
			LookupModels mainContextList = (LookupModels)context.getValue(ContextParam.GLOBALLOOKUPMODLELIST);
			for(LookupModel lModel : ifStepLookups.getLookupModels()){
				mainContextList.add(lModel);
			}
			context.putValue(ContextParam.GLOBALLOOKUPMODLELIST, mainContextList);
		}
		
		List<AlgoStep> algosteps = (List<AlgoStep>)context.getValue(ContextParam.STEPS);
		algosteps.add(ifStatementStep);
		
	}
	
}