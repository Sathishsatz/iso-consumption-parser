package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.util.PredicatesUtil;

public class NotEqualsEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	//private StringBuffer buffer = new StringBuffer();
	
	public NotEqualsEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;		
	}

	@Override
	public Object evaluate()throws RecognitionException {
		StringBuffer buffer = new StringBuffer();
		left.setContext(context);
		buffer.append(" ( ");
		buffer.append(left.evaluate());
		buffer.append(" != ");
		right.setContext(context);
		buffer.append(right.evaluate());
		PredicatesUtil.addIfPredicateConition(context, buffer);
		buffer.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return buffer;
	}
	
	
	/*private void addIfPredicateConition(){
		if(context.getValue(ContextParam.IS_IFCONDITIONPREDICATE) != null && (Boolean)context.getValue(ContextParam.IS_IFCONDITIONPREDICATE)){
			buffer.append(" && ");
			buffer.append(context.getValue(ContextParam.IFCONDITIONPREDICATEXPATH) + " ) ");
		}
	}*/

}
