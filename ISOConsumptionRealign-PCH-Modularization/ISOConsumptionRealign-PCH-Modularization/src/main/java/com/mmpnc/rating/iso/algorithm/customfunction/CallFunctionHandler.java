package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.helper.CallPchStepCreater;

public class CallFunctionHandler implements FunctionHandler {

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String constant, Context context)
			throws RecognitionException {
		// TODO Auto-generated method stub
		//Write a code to create a call PCH statement and then return the pch Name
		
		CallPchStepCreater stepCreater = CallPchStepCreater.getInstance();
		return stepCreater.createCallPchStep(evallist, function, constant, context);
		
	}

}
