package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class DivideEvaluator implements Evaluator {

	Context context;
	Evaluator left ;
	Evaluator right;
	
	public DivideEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
		
	}

	@Override
	public void setContext(Context context) {
		this.context = context;

	}

	@Override
	public Object evaluate() throws RecognitionException{
		left.setContext(context);
		StringBuffer divide = new StringBuffer();
		divide.append(" ( ");
		divide.append(left.evaluate());
		divide.append(" / ");
		right.setContext(context);
		divide.append(right.evaluate());
		divide.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return divide;
	}

}
