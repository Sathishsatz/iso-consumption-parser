package com.mmpnc.rating.iso.icmxml.creater;

import org.apache.commons.lang.StringUtils;
import com.mmpnc.icm.common.IdGeneration;
import com.mmpnc.rating.iso.wrapper.IcmRatingNodeContent;

/**
 * @author Nilkanth9581
 *
 */
public class AlgorithmIdCreater {
	
	
	private static AlgorithmIdCreater algorithmIdCreater = new AlgorithmIdCreater();
	
	private AlgorithmIdCreater(){
	}
	
	public static AlgorithmIdCreater getInstance(){
		return algorithmIdCreater;
	}
	
	private String productName;
	private String Program;
	private String state;
	private String effectiveDate;
	private String expirationDate;
	//private String refType;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProgram() {
		return Program;
	}
	public void setProgram(String program) {
		Program = program;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	
	//Algorithm ID has been created using Product Name,Program,State,Effective Date,Expiration Date,Rating Node Name
	
	public String createAlgorithmId(IcmRatingNodeContent icmRatingNodeContent){
		StringBuffer algoData = new StringBuffer();
		
		 algoData.append(icmRatingNodeContent.getProductName())
		.append(icmRatingNodeContent.getProgramName()).append(icmRatingNodeContent.getState())
		.append(icmRatingNodeContent.getEffectiveDate()).append(icmRatingNodeContent.getExpirationDate())
		.append(createRatingNodeName(icmRatingNodeContent))
		.append(icmRatingNodeContent.getRefType())
		.append(icmRatingNodeContent.getSequenceNo())
		.append(icmRatingNodeContent.getPass())
		;
		
		 String algoId=IdGeneration.idGenerator(algoData.toString());
		
		 return algoId;
	}
	
	
	private String createRatingNodeName(IcmRatingNodeContent ratingNodeContent){
		String[] ratingNodeNames = ratingNodeContent.getAlgorithmLevel().split(" ");
		 String templateName = null;
       
		 for (String ratingNodeName : ratingNodeNames) {
         if(ratingNodeName==""){
           continue;
         }
         
         templateName  = ratingNodeName;
         String resolvedModelPath = ratingNodeName;
         if (ratingNodeName.contains(".")) {
           templateName = ratingNodeName
               .substring(ratingNodeName.lastIndexOf(".") + 1);
           templateName = templateName + StringUtils.countMatches(resolvedModelPath, ".");
         }
        
       }
       
       return templateName;
	}
	
	

}
