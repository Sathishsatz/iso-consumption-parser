package com.mmpnc.rating.iso.algorithm.customfunction;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;

/**
 * @author nilkanth9581
 *  THIS CLASS WILL HANDLE GETINTEGER FUNCTION IN THE ALGORITHM FILE
 */
public class GetIntegerFunctionHandler implements FunctionHandler {

	@Override
	public String handleCustomFunction(List<Evaluator> evallist,
			String function, String varaible, Context context) throws RecognitionException{
		// TODO Auto-generated method stub
		String getInteger = null;
		for(Evaluator eval : evallist){
			eval.setContext(context);
			getInteger = eval.evaluate().toString();
		}
		return "GetInteger ( "+getInteger+" )";
		}

}
