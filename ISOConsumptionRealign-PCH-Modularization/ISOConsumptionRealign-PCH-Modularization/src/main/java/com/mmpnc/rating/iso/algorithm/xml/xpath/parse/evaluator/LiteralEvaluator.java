package com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class LiteralEvaluator implements Evaluator {
	private String literal;
	private Context context;
	
	public LiteralEvaluator(String string) {
		this.literal = string;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		StringBuffer buffer = (StringBuffer) this.context.getValue(ContextParam.XPATHSTRING);
		buffer.append(this.literal);
		return Type.CONSTANT;
	}
}
