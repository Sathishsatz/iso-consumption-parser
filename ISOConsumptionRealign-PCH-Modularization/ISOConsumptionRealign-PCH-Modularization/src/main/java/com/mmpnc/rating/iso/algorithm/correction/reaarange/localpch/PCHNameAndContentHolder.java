package com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mmpnc.rating.iso.algorithm.correction.RealignAlgorithmObject;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;

/**
 * @author nilkanth9581
 *
 */
public class PCHNameAndContentHolder {
	
	private Map<String, PCH> pchNameAndContMap = null;
	private Scope originalScope = null;
	private Scope refScope = null;
	private Set<String> localPCHToDeleteFromOriginalScope = null;
	private List<String> reprocessedLocalVariables = null;
	private Scope getOriginalScope() {
		return originalScope;
	}

	public void setOriginalScope(Scope originalScope) {
		this.originalScope = originalScope;
	}

	private Scope getRefScope() {
		return refScope;
	}

	public void setRefScope(Scope refScope) {
		this.refScope = refScope;
	}

	public PCHNameAndContentHolder(){
		pchNameAndContMap = new HashMap<String, PCH>();
		localPCHToDeleteFromOriginalScope = new HashSet<String>();
		reprocessedLocalVariables = new ArrayList<String>();
	}
	
	/*public void addToPCHNameAndConentHolder(String varaibleName,PCH pch){
		pchNameAndContMap.put(varaibleName, pch);
	}*/
	
	public Object getPCHFromPchNameAndContMap(String variableName){
		return pchNameAndContMap;
	}
	
	public boolean isPCHAvaialable(String pchName){
		return pchNameAndContMap.containsKey(pchName);
	}
	
	
	/**
	 * IN THIS METHOD WE WILL GET ALL THE PCH FROM THE ORINGINAL SCOPE
	 * AND CREATE A LIST OF THE PCH AVAIALBE IN THE ORIGINAL SCOPE
	 * 
	 */
	protected void addAllPCHFromOriginalScopeToPCHList(){
		for(Object content:getOriginalScope().getContent()){
			if(content instanceof PCH){
				PCH pch  = (PCH)content; 
				pchNameAndContMap.put(pch.getName(),pch);
				//System.out.println(pch.getName());
			}
		}
	}
	
	/**
	 * @param localVariableName
	 * THIS METHOD WILL CHECK IF THE LCOAL VARIABLE IS ACTUALLY PCH 
	 * IF SO IT WILL ADD PCH TO THE REFERENCE SCOPE
	 * 
	 */
	protected void addLocalVariableToRefScope(String refScoepDbTable,String variableName,String variableType){
		//FIRST CHECK IF THE LOCAL VARIABLE IS ACTUALLY INCLUED IN THE REF SCOPE
		if((variableName.contains("/")) && pchNameAndContMap.containsKey(variableName)){
			return;
		}
		else{
			
			if(variableName.contains("/")){
				variableName = variableName.split("/")[1];
			}
			
			if(pchNameAndContMap.containsKey(refScoepDbTable+"/"+variableName)){
				//DO NOTHING AS THE VARIABLE IS PRESENT AS SCOPE/VAR FORMAT PCH NAME
				return;
			}
			else if(pchNameAndContMap.containsKey(variableName)){
				this.localPCHToDeleteFromOriginalScope.add(variableName);
				Object pchObj = pchNameAndContMap.get(variableName);
				getRefScope().getContent().add(pchObj);
				
				PCH pch =(PCH) pchObj;
				//AS THE LOCAL VARIABLES SCOPES NEEDS TO BE REARRANGED WE NEED TO REPROCESS THOSE PCH AS WELL
				if(!reprocessedLocalVariables.contains(pch.getName())){
					RealignAlgorithmObject corrector = new RealignAlgorithmObject();
					reprocessedLocalVariables.add(pch.getName());
					corrector.reprocessPCH((PCH)pch, refScope, refScoepDbTable, originalScope.getDbTables());
				}
			}
			
			else{
				if("COLUMN_NUMERIC".equals(variableType) || "COLUMN_STRING".equals(variableType)){
					System.err.println("WARNING: NOT SURE WHTHER LOCAL PCH IS REQUIRED- PCH_SCOPE["+refScoepDbTable+"] VARIALBE_NAME:["+variableName+"]");
				}else{
					throw new RuntimeException("UNABLE TO FIND LOCAL VARIABLE IN PCH SPLIT SCOPE:[" + refScoepDbTable+ "] VARAIBLE NAME:[" + variableName+"]");
				}
				
			}
			
		}
		
	}
	
	/**
	 * THIS FUNCTIONALITY NEEDS TO BE CHECKED AS IN BOP_DE ALGORITHMS WE HAVE FOUND THAT
	 * 
	 */
	protected void deleteLocalVarPCHFromOriginalScope(PCHNameAndContentHolder pchHolder){
		Iterator<String> itr = this.localPCHToDeleteFromOriginalScope.iterator();
		while(itr.hasNext()){
			String keyName = itr.next();
			PCH pch = pchNameAndContMap.get(keyName);
			originalScope.getContent().remove(pch);
		}
	}
	
	protected void checkIfScopeHasAnyPCHOrEmpty(Reference reference ){
		if(originalScope.getContent().size() == 0){
			
		}
	}
}
