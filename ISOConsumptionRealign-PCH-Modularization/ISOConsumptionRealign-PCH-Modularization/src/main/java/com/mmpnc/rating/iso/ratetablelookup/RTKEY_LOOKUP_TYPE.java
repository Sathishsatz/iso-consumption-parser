package com.mmpnc.rating.iso.ratetablelookup;

/**
 * @author Nilkanth9581
 *
 */
public enum RTKEY_LOOKUP_TYPE {
	INTERPOLATE,LESSTHANOREQUAL,GREATERTHANOREQUAL
}
