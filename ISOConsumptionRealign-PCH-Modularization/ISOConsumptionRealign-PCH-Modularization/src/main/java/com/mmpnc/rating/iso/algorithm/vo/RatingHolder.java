package com.mmpnc.rating.iso.algorithm.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Nilkanth9581
 * THIS CLASS IS CREATED FOR HOLDING LIST OF THE RATING OBJECTS FROM CLASS
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rating","premium"
})
@XmlRootElement(name = "RatingHolder")
public class RatingHolder {
	@XmlElement(name = "Rating")
    List<Rating> rating;
	@XmlElement(name = "Premium")
    List<Premium> premium;

	public List<Premium> getPremium() {
		return premium;
	}

	public void setPremium(List<Premium> premium) {
		this.premium = premium;
	}

	public List<Rating> getRating() {
		return rating;
	}

	public void setRating(List<Rating> rating) {
		this.rating = rating;
	} 
} 
