package com.mmpnc.rating.iso.algorithm.correction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmpnc.rating.iso.algorithm.Activity;
import com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch.IAddLocPCHInPCHRearrangedAlgo;
import com.mmpnc.rating.iso.algorithm.correction.reaarange.localpch.ImplAddLocPCHInPCHRearrangedAlgo;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.util.Utility;
import com.mmpnc.util.VariableType;

/**
 * @author nilkanth9581
 *
 */
public class RealignAlgorithmObject implements Activity {
	
	String str = "algorithm is present at";
	private final IAddLocPCHInPCHRearrangedAlgo inPCHRearrangedAlgo = new ImplAddLocPCHInPCHRearrangedAlgo();
	
	public RealignAlgorithmObject(){
	}

	@Override
	public <T> T process(T lob) {
		
		realignLob((LOB) lob);
		return lob;
	}
	
	private void realignLob(LOB lob) {
		List<Reference> refList = new ArrayList<Reference>();
		List<Integer> removeIndex = new ArrayList<Integer>();
		Map<String,Integer> scopePassMap = new HashMap<String, Integer>();
		int counter = 0;
		
		for (Object ref : lob.getContent()) {

			if (ref instanceof Reference) {
				if(realignReference((Reference) ref, lob, refList,scopePassMap)){
					removeIndex.add(counter);
				}
			}
			counter++;
		}
		
		counter = 0;
		for(int removalIndex : removeIndex){
			//if(!(lob.getContent().get(removalIndex - counter) instanceof Reference)){
				lob.getContent().remove(removalIndex - counter);
				counter++;
			//}
			
		}
		
		//delete local used pch from original scope which has been referenced from the pch split algorithm
		inPCHRearrangedAlgo.deleteLocalPCHUsedFromOriginalScope();
		// update the lob
		lob.getContent().addAll(refList);
		
		
		
	}

	private boolean realignReference(Reference ref, LOB lob, List<Reference> refList,Map<String, Integer> scopePassMap) {
		boolean removalFlag = false;
		List<Object> scopeList = ref.getContent();
		List<Integer> removeIndex = new ArrayList<Integer>();
		
		int counter = 0;
		int stringObjectCount = 0;
		if(ref.getType().equals("Premium Calculation")){
			for (Object obj : scopeList) {
				if (obj instanceof Scope) {
					if(realignScope((Scope) obj, ref, lob, refList,scopePassMap)){
						removeIndex.add(counter);
					}
				} else if (obj instanceof String){
					stringObjectCount++;
				}
				counter ++;
			}
		}
		
		counter = 0;
		for(int removalIndex : removeIndex){
			//if(!(ref.getContent().get(removalIndex - counter) instanceof Scope)){
			ref.getContent().remove(removalIndex - counter);
			counter++;
			//}
		}
		
		if(stringObjectCount == ref.getContent().size()){
			removalFlag = true;
		}
		
		return removalFlag;
	}
	//private Map<String,Integer>scope_Pass_Map = new HashMap<String, Integer>();
	
	private boolean realignScope(Scope scope, Reference ref, LOB lob, List<Reference> refList,Map<String, Integer> scopePassMap) {
		
		boolean removalFlag = false;
		List<Object> pchList = null;
		pchList = scope.getContent();
		int counter = 0;
		List<Integer> removeIndex = new ArrayList<Integer>();
		int stringObjectCount = 0;
		for (Object obj : pchList) {
			if (obj instanceof PCH) {
				if(realignPCH((PCH) obj, scope, ref, lob, refList,scopePassMap)){
					removeIndex.add(counter);
				}
			} else if (obj instanceof If) {
				//readIf((If) obj);
			} else if (obj instanceof Assign) {
				//readAssign((Assign) obj);
			} else if (obj instanceof Loop) {
				//readLoop((Loop) obj);
			} else if (obj instanceof Class) {
				ISOConsumptionLogger.info("do we need to handle this?");
			} else if (obj instanceof String){
				stringObjectCount++;
			}
			counter++;
		}
		
		counter = 0;
		for(int removalIndex : removeIndex){
			scope.getContent().remove(removalIndex - counter);
			counter++;
		}
			
		if(stringObjectCount == scope.getContent().size()){
			removalFlag = true;
		}
//		System.out.println("String Object count " + stringObjectCount + " " + scope.getDbTables());
//		System.out.println("Other Object count " + scope.getContent().size() + " " + scope.getDbTables());
		
		return removalFlag;
	}

	
	/*private boolean realignPCHWithSameScopeDbTable(Scope scope,PCH object){
		
		
		return false;
	}*/
	
	private boolean realignPCH(PCH obj,Scope scope,Reference ref,LOB lob,List<Reference> refList,Map<String, Integer> scopePassMap) {
		boolean flag = false;
		if(obj.getName() != null)
		if(obj.getName().contains("/") && !obj.getName().startsWith("ancestor") && !obj.getName().startsWith("../"))
		{
			String args[] = obj.getName().split("/");
			//create reference
			Reference reference = new Reference();
			reference.setDbTables(args[0]);
			reference.setName(args[0]+".Premium");
			reference.setNumber(ref.getNumber());
			reference.setState(ref.getState());
			reference.setType(ref.getType());
			//update reference list
			reference = Utility.updateList(refList, reference);
			//create scope
			Scope refScope = new Scope();
			refScope.setDbTables(args[0]);
			
			
			
			//THIS CONDITION HAS BEEN ADDED TO HANDLE THE SITUATION WHERE AN ALGORITHM HAVE A PCH WITH / IN IT BUT THE
			//DBTABLE OF NEW SCOPE IS SAME AS THE EXISTING ALGORITHM SO IN THAT CASE WE NEED TO CHANGE THE SCOPE PASS
			//TO SAVE THESE TWO SCOPES 
			//if(args[0].equals(scope.getDbTables())){
			Integer newPass = 0;
			if(scopePassMap.containsKey(args[0])){
				newPass = scopePassMap.get(args[0])+1;
				scopePassMap.put(args[0], newPass);
			}else{
				scopePassMap.put(args[0], 10);
				newPass = 10;
			}
			Short newS = new Short(newPass.toString());
			refScope.setPass(newS);
			
			//THIS HANDLING IS ADDED TO CHECK IF THE SAME SCOPE AND REF HAS BEEN CREATED IS PRESENT IN THE 
			//REARRAGED PCH LIST IF SO WE NEED TO CHANGE SCOPE OF THE ALGORIHM
			
			refScope.setRecordType(scope.getRecordType());
			refScope.setSequenceNo(scope.getSequenceNo());
			//ADDING THIS CONDITION TO HANDLE IF WE HAVE A SCOPE WHERE IT HAS PCH NAMES WITH THE SAME SCOPE
			//AND SAME PASS AND SAME ALGORITHM NAME /THEN WE CAN CHAGE THE PASS FOR THAT PERTICULAR REFERENCE SCOPE
			//System.out.println("Scope created " + refScope.getDbTables());
			//add scope to reference
			refScope = (Scope) Utility.updateList(reference.getContent(), refScope);
			
			//CREATING NEW SCOPEPCHLISTHOLER
			inPCHRearrangedAlgo.checkAndCreateNewPCHList(scope, refScope);
			
			//reprocess PCH block
			reprocessPCH(obj,refScope,refScope.getDbTables(),scope.getDbTables());			
			
			//add pch to scope
			refScope.getContent().add(obj);		
			flag = true;
		}
		return flag;
	}
	
	
	public void reprocessPCH(PCH pch,Scope scope,String refScopeDbTable,String orginalScopedbtable){
		String scopeReplacement = scope.getDbTables()+"/";
		pch.setName(pch.getName().replace(scopeReplacement, ""));
		
		for (Object obj : pch.getContent()) {
			updateContent(obj,scopeReplacement,refScopeDbTable,orginalScopedbtable);
		}		
	}

	private void updateContent(Object obj, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		if (obj instanceof If) {
			updateIf((If) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
		} else if (obj instanceof Assign) {
			updateAssign((Assign) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
		} else if (obj instanceof Loop) {
			updateLoop((Loop) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
		} else if(obj instanceof Ratetable){
			updateRatetable((Ratetable)obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
		}
	}

	private void updateLoop(Loop loop, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		loop.setThrough(loop.getThrough().replace(scopeReplacement, ""));
		for (Object obj : loop.getContent()) {
			updateContent(obj,scopeReplacement,refScopeDbTable,orginalScopedbtable);
		}
	}

	
	private void updateAssign(Assign obj, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		obj.setLValue(obj.getLValue().replace(scopeReplacement, ""));
		
		for (Object content : obj.getContent()) {
			if (content instanceof Expression) {
				updateExpression((Expression) content, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			}
		}
		
	}
	
	/**
	 * @param bracket
	 * @param scopeReplacement
	 * Added handling for bracket 
	 */
	private void updateBracket(Bracket bracket,String scopeReplacement,String refScopeDbTable,String orginalScopedbtable){
		for(Object content : bracket.getContent()){
			if(content instanceof Expression){
				updateExpression((Expression)content, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			}
		}
	}
	
	
	private void updateExpression(Expression expression, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable ) {
		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression exp = (Expression) obj;
					updateExpression(exp, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			} else if (obj instanceof Function) {
				updateFunction((Function) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			} else if (obj instanceof Ratetable) {
				updateRatetable((Ratetable) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			}else if(obj instanceof Bracket){
				updateBracket((Bracket)obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			} else if (obj instanceof String){
				
				
				
				if(expression.getVariableType() != null && ! expression.getVariableType().equals("CONSTANT")){
					if(obj != null && !obj.equals("")){
						expression.getContent().set(0,((String)obj).replace(scopeReplacement, ""));
					}
				}//THIS CONDITION HAS ADDED TO MAKE THIS JAR COMPATIBLE WITH THE BOP ALGORITHMS 
				//IN BOP ALGORITHMS SOME STATEMTNS ARE LIKE BOPBusnIncomeChangesTimePeriod/Count (BOPBusnIncomeChangesTimePeriodBusnPrsnlPropCoverage)
				else if(expression.getVariableType() == null){
					if(obj != null && !obj.equals("")){
						expression.getContent().set(0,((String)obj).replace(scopeReplacement, ""));
					}
				}
				
				if(expression.getVariableType() != null){
					VariableType varType = VariableType.valueOf(expression.getVariableType());
					if ( varType.equals(VariableType.LV_VALUE)|| varType.equals(VariableType.LV_INTEGER) || varType.equals(VariableType.LV_NUMERIC) || varType.equals(VariableType.LV_DOUBLE) || varType.equals(VariableType.LV_STRING) || varType.equals(VariableType.LV_BOOLEAN) || varType.equals(VariableType.LV_DATE) || varType.equals(VariableType.LV_TIMESPAN) || varType.equals(VariableType.COLUMN_NUMERIC)|| varType.equals(VariableType.COLUMN_STRING) ){
						inPCHRearrangedAlgo.checkAndAddLocalPch(expression.getContent().get(0).toString().trim(),expression.getVariableType(), refScopeDbTable, orginalScopedbtable);
					}
				}
				
				
				
			}
		}
	}

	private void updateRatetable(Ratetable _Ratetable, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		for(Object obj : _Ratetable.getContent()){
			if(obj instanceof Arg){
				Arg arg = (Arg)obj;
				String argContnet = arg.getContent();
				arg.setContent(argContnet.replace(scopeReplacement, ""));
			}
		}
	}

	private void updateFunction(Function obj, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		
	}

	private void updateIf(If _if, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		for (Object obj : _if.getContent()) {
			if (obj instanceof Condition) {
				updateCondition((Condition) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			} else if (obj instanceof Then) {
				updateThen((Then) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			} else if (obj instanceof Else) {
				updateElse((Else) obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			}
		}
	}
	
	

	private void updateElse(Else _else, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		for (Object obj : _else.getContent()) {
			updateContent(obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
		}
	}

	private void updateThen(Then _then, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		for (Object obj : _then.getContent()) {
			updateContent(obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
		}
	}

	private void updateCondition(Condition condition, String scopeReplacement,String refScopeDbTable,String orginalScopedbtable) {
		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				updateExpression((Expression)obj, scopeReplacement,refScopeDbTable,orginalScopedbtable);
			}
		}
	}
	
}
