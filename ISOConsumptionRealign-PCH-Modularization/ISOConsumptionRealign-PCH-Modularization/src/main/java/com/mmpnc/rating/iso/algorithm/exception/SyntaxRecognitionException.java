package com.mmpnc.rating.iso.algorithm.exception;

public class SyntaxRecognitionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SyntaxRecognitionException(String str){
		super(str);
	}

}
