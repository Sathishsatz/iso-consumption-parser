package com.mmpnc.rating.iso.algorithm.xml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.ArgumentVariable;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.Evaluator.Type;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.exception.UnInitializeContextException;
import com.mmpnc.rating.iso.algorithm.util.ISOConsumptionLogger;
import com.mmpnc.rating.iso.algorithm.vo.Arg;
import com.mmpnc.rating.iso.algorithm.vo.Assign;
import com.mmpnc.rating.iso.algorithm.vo.Bracket;
import com.mmpnc.rating.iso.algorithm.vo.Class;
import com.mmpnc.rating.iso.algorithm.vo.Condition;
import com.mmpnc.rating.iso.algorithm.vo.Else;
import com.mmpnc.rating.iso.algorithm.vo.Expression;
import com.mmpnc.rating.iso.algorithm.vo.Function;
import com.mmpnc.rating.iso.algorithm.vo.If;
import com.mmpnc.rating.iso.algorithm.vo.Loop;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Ratetable;
import com.mmpnc.rating.iso.algorithm.vo.Then;
import com.mmpnc.xml.xpath.parse.XPathActivity;
import com.mmpnc.xml.xpath.parse.XPathParserImpl;

/**
 * @author Nilkanth9581
 * 
 */
public class XMLNodeReader {

	private Context context;

	static class AlgoConstant {
		static final String If = "IF ";
		static final String EndIf = "END IF";
		static final String Then = "THEN ";
		static final String Else = "ELSE ";
		static final String Local = "LOCAL";
		static final String Loop = "LOOP THROUGH ";
		static final String EndLoop = "END LOOP ";
		static final String xpath = "XPATH";
		static final String DoubleQuote = "\"";
		static final String Equals = " = ";
		static final String LeftRoundBracket = " ( ";
		static final String RightRoundBracket = " ) ";
		static final String Colon = ":";
		static final String Comma = " , ";
		static final String Space = " ";
		static final String RateTable = "RateTable";
		static final String RoundUpDollar = "RoundUpDollar";
		static final String RoundUpHundredth = "RoundUpHundredth";
		static final String RoundUpThousandth = "RoundUpThousandth";
		static final String RoundUpTenThousandth = "RoundUpTenThousandth";
		static final String RoundUpHundredThousandth = "RoundUpHundredThousandth";
		static final String RoundDollar = "RoundDollar";
		static final String RoundHundredth = "RoundHundredth";
		static final String RoundThousandth = "RoundThousandth";
		static final String RoundTenThousandth = "RoundTenThousandth";
		static final String RoundHundredThousandth = "RoundHundredThousandth";
		static final String Ceiling = "Ceiling";
		static final String ColumnNumeric = "COLUMN_NUMERIC";
		static final String Node_ownerDocument = "Node.OwnerDocument.DocumentElement.Attributes.GetNamedItem&quot;ratingIteration&quot;";
	}

	private Map<String, String> specialFunctionMap = new HashMap<String, String>();

	public void populateSpecialFunctionMap() {
		specialFunctionMap.put("Round Up Dollar", "RoundDollar");
		specialFunctionMap.put("Round Up Hundreds", "RoundHundredth");
		specialFunctionMap.put("Round Up Thousands", "RoundThousandth");
		specialFunctionMap.put("Round Up HundredThousands",
				"RoundHundredThousandth");
		specialFunctionMap.put("Round Up Hundred Thousands",
				"RoundHundredThousandth");
		specialFunctionMap.put("Round Up Ten Thousands", "RoundTenThousandth");
		specialFunctionMap.put("Round Up TenThousands", "RoundTenThousandth");
		specialFunctionMap.put("Round Up Thousand Millionths",
				"RoundThousandMillionths");
		specialFunctionMap.put("daysDiff", "days_between");
		specialFunctionMap.put("Ceiling", "Ceiling");
		// Adding these as special functions in to handle parse.ParseDouble
	}

	public XMLNodeReader(Context context) {
		populateSpecialFunctionMap();
		this.context = context;
	}

	public void readPCH(PCH pch, StringBuffer buffer)
			throws UnInitializeContextException {
		for (Object statement : pch.getContent()) {
			readContent(statement, buffer);
		}
	}

	// String n = "GetDayOfMonth";
	public void readContent(Object content, StringBuffer buffer)
			throws UnInitializeContextException {
		// if(!checkEmptyStatementBlock(content))
		if (content instanceof If) {
			readIf((If) content, buffer);
		} else if (content instanceof Condition) {
			readCondition((Condition) content, buffer);
		} else if (content instanceof Then) {
			readThen((Then) content, buffer);
		} else if (content instanceof Else) {
			readElse((Else) content, buffer);
		} else if (content instanceof Assign) {
			readAssign((Assign) content, buffer);
		} else if (content instanceof Loop) {
			readLoop((Loop) content, buffer);
		} else if (content instanceof Expression) {
			readExpression((Expression) content, buffer);
		} else if (content instanceof Class) {
			ISOConsumptionLogger.info("Class object");
		} else if (content instanceof String) {
			if (!((String) content).trim().equals("")) {
				ISOConsumptionLogger.info("Print Content ******************** "
						+ content);
			}
		} else {
			ISOConsumptionLogger.info("We will need to handle this case -> "
					+ content.getClass());
		}
	}

	private void readElse(Else _else, StringBuffer buffer)
			throws UnInitializeContextException {
		if (!checkEmptyStatementBlock(_else.getContent())) {
			buffer.append(AlgoConstant.Else);
			for (Object obj : _else.getContent()) {
				readContent(obj, buffer);
			}
		}
	}

	private void readThen(Then _then, StringBuffer buffer)
			throws UnInitializeContextException {
		if (!checkEmptyStatementBlock(_then.getContent())) {
			buffer.append(AlgoConstant.Then);
			for (Object obj : _then.getContent()) {
				readContent(obj, buffer);
			}
		}
	}

	private void readCondition(Condition condition, StringBuffer buffer)
			throws UnInitializeContextException {

		if (ExceptionHandler.checkNullOrSize(condition.getContent())) {
			ExceptionHandler
					.raiseUnInitializeContextException("Condition is missing in IF block");
		}

		buffer.append(AlgoConstant.LeftRoundBracket);

		for (Object obj : condition.getContent()) {
			if (obj instanceof Expression) {
				readExpression((Expression) obj, buffer);
			} else if (obj instanceof Bracket) {
				readBracket((Bracket) obj, buffer, false);
			}
		}

		buffer.append(AlgoConstant.RightRoundBracket).append("\n");
	}

	private void readExpression(Expression expression, StringBuffer buffer)
			throws UnInitializeContextException {
		// checkForBracket(expression);

		// This condition is added to handle AddYear operator in WC algorithm
		// files

		boolean isAddYearsOperator = checkOperator(expression, buffer);

		boolean usedFlag = false;
		boolean specialFunctionFlag = false;
		if (specialFunctionMap.containsKey(expression.getOp())) {
			buffer.append(AlgoConstant.Space)
					.append(specialFunctionMap.get(expression.getOp()))
					.append(AlgoConstant.Space);
			usedFlag = true;
			buffer.append(AlgoConstant.LeftRoundBracket);
			// specialFunctionFlag = true;

		}
		boolean containsOpFlag = false;
		if (expression.getOp() != null && "Contains".equals(expression.getOp())) {
			buffer.append(AlgoConstant.Space).append("Contains")
					.append(AlgoConstant.Space);
			buffer.append(AlgoConstant.LeftRoundBracket);
			containsOpFlag = true;
		}

		if (expression.getOp() != null
				&& (expression.getOp().trim().equals("-") || expression.getOp()
						.trim().equals("+"))) {
			buffer.append(AlgoConstant.LeftRoundBracket);
		}
		// THIS CONDITION IS ADDED FOR DOUBLE.PARSEDOUBLE AS IT IS USED AS THE
		// OPERATOR AND NOT THE CONTENT OF THE
		// FILE
		if (expression.getContent() != null
				&& expression.getContent().size() == 0
				&& expression.getOp() != null) {
			// buffer.append("CONSTANT:");
			// buffer.append(expression.getOp());
		}

		for (Object obj : expression.getContent()) {
			if (obj instanceof Expression) {
				Expression exp = (Expression) obj;
				boolean isStartWithOp = false;
				if ("StartsWith".equals(exp.getOp())
						&& "left".equals(exp.getSide())) {
					buffer.append(AlgoConstant.LeftRoundBracket);
					isStartWithOp = true;
				}

				// ADDED TO HANDLE CONTAINS CUSTOM OPERATOR USED IN CA-CA
				// ALGORITHM FILE
				// OPERATOR USED IN ALGORITHM FILE
				// vehicleType.Contains&quot;Gross Receipts Basis&quot;
				if (exp.getOp() != null && (exp.getOp().contains(".Contains"))) {
					buffer.append(handleContainsOperator(exp.getOp().trim()));
				}
				if (usedFlag == false
						&& expression.getOp() != null
						&& (exp.getSide() != null && exp.getSide().equals(
								"right"))
						&& !"Contains".equals(expression.getOp())) {

					buffer.append(AlgoConstant.Space)
							.append(updateOperator(expression.getOp().trim()))
							.append(AlgoConstant.Space);
					if ("StartsWith".equals(exp.getOp())
							&& "right".equals(exp.getSide())) {
						buffer.append(AlgoConstant.LeftRoundBracket);
						isStartWithOp = true;
					}
					if (expression.getOp().trim().equals("Not")) {
						buffer.append(AlgoConstant.LeftRoundBracket);
						usedFlag = true;
					}
				}
				readExpression(exp, buffer);
				if (isStartWithOp) {
					buffer.append(AlgoConstant.RightRoundBracket);
					isStartWithOp = false;
				}
				if (isAddYearsOperator && exp.getSide() != null
						&& exp.getSide().equals("left")) {
					buffer.append(",");
				}
				if (containsOpFlag && exp.getSide() != null
						&& exp.getSide().equals("left")) {
					buffer.append(",");
				}

			} else if (obj instanceof Bracket) {
				if (usedFlag == false && expression.getOp() != null) {
					// && expression.getSide() == null
					if ((expression.getContent().indexOf(obj) > 2)
							|| (expression.getContent().indexOf(obj) < 2 && "Not"
									.equals(expression.getOp()))) {
						buffer.append(AlgoConstant.Space)
								.append(updateOperator(expression.getOp()
										.trim())).append(AlgoConstant.Space);
					}

				}

				readBracket((Bracket) obj, buffer, specialFunctionFlag);
				buffer.append(AlgoConstant.RightRoundBracket);
			} else if (obj instanceof Function) {
				readFunction((Function) obj, buffer);
			} else if (obj instanceof Ratetable) {
				readRatetable((Ratetable) obj, buffer);
			} else if (obj instanceof String) {
				if (((String) obj).trim().equals("")) {
					continue;
				}

				if (expression.getVariableType() != null
						&& !(ignoreVariableTypeAssignment(expression
								.getVariableType()))) {

					if ("COLUMN_NUMERIC".equals(expression.getVariableType())
							&& "Total Premium".equals(obj.toString())) {
						buffer.append(updateContent(expression.getContent()
								.get(0).toString().trim()));
					} else {
						buffer.append(wrapVariableTypeAssignment(
								expression.getVariableType(), expression
										.getContent().get(0).toString().trim()));
					}

				}

				else if (expression.getVariableType() == null
						&& expression.getRuleReference() != null) {
					// added this condition to handle the statement where the
					// expression have rulereference.
					buffer.append(wrapVariableTypeAssignment("XPATH",
							expression.getContent().get(0).toString().trim()));

				} else {
					buffer.append(updateContent(expression.getContent().get(0)
							.toString().trim()));
				}
			}
		}

		if (expression.getOp() != null
				&& (expression.getOp().trim().equals("-") || expression.getOp()
						.trim().equals("+"))) {
			buffer.append(AlgoConstant.RightRoundBracket);
		}

		if (usedFlag) {
			buffer.append(AlgoConstant.RightRoundBracket);
		}

		if (containsOpFlag) {
			buffer.append(AlgoConstant.RightRoundBracket);
		}

		if (isAddYearsOperator) {
			buffer.append(")");
			isAddYearsOperator = false;
		}

	}

	/**
	 * @param bracket
	 * @param buffer
	 * @throws UnInitializeContextException
	 *             This element is added in new version of the algorithm
	 *             provided by ISO
	 * 
	 */
	private void readBracket(Bracket bracket, StringBuffer buffer,
			boolean specialFunctionFlag) throws UnInitializeContextException {
		List<Object> content = bracket.getContent();
		for (Object obj : content) {
			if (obj instanceof Expression) {
				if (!specialFunctionFlag) {
					buffer.append(AlgoConstant.Space);
					buffer.append(AlgoConstant.LeftRoundBracket);
				}
				Expression expression = (Expression) obj;
				readExpression(expression, buffer);
				/*
				 * if(!specialFunctionFlag)
				 * buffer.append(AlgoConstant.RightRoundBracket);
				 */
			}
		}
		// specialFunctionFlag = false;
	}

	private boolean checkOperator(Expression expression, StringBuffer buffer) {
		if ("AddYears".equals(expression.getOp())) {
			expression.setOp(null);
			buffer.append("AddYears");
			buffer.append(AlgoConstant.LeftRoundBracket);
			return true;
		} else if (expression.getOp() != null
				&& expression.getOp().contains(".")) {
			String arr[] = expression.getOp().split("\\.");
			if ("GetNamedItem\"ratingIteration\"".equals(arr[arr.length - 1])) {
				expression.getContent().add("RatingIteration");
				expression.setOp(null);
			}
			return false;
		}
		return false;
	}

	private String updateOperator(String op) {
		if (op.equals("Not")) {
			return "NOT";
		} else {
			return op;
		}
	}

	private String updateContent(String content) {
		if (content.equals("Total Premium")) {
			return "Premium";
		}
		/*
		 * if(content.contains("[")){ content = content.replace("[", "[\"");
		 * content = content.replace("]", "\"]"); }
		 */
		return content;
	}

	private void readFunction(Function function, StringBuffer buffer)
			throws UnInitializeContextException {
		boolean flag = false;

		buffer.append(function.getType());
		buffer.append(AlgoConstant.LeftRoundBracket);
		for (Object obj : function.getContent()) {
			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				if (flag) {
					buffer.append(AlgoConstant.Comma);
				}
				readArgument(arg, buffer);
				flag = true;
			}
		}
		buffer.append(AlgoConstant.RightRoundBracket);
	}

	private void readRatetable(Ratetable ratetable, StringBuffer buffer)
			throws UnInitializeContextException {
		boolean flag = false;

		buffer.append(AlgoConstant.RateTable).append(AlgoConstant.Colon);
		buffer.append(ratetable.getCode().trim());
		buffer.append(AlgoConstant.LeftRoundBracket);
		for (Object obj : ratetable.getContent()) {
			if (obj instanceof Arg) {
				Arg arg = (Arg) obj;
				if (flag) {
					buffer.append(AlgoConstant.Comma);
				}
				readArgument(arg, buffer);
				flag = true;
			}
		}
		buffer.append(AlgoConstant.RightRoundBracket);
	}

	private void readArgument(Arg arg, StringBuffer buffer)
			throws UnInitializeContextException {
		// this condition is added because in iso 3.2.3 version files
		// rate table has variable types such as UI and others
		// if(arg.getVariableType() == null){
		buffer.append(updateVariableType(arg.getContent()));
		// }else{
		// buffer.append(arg.getVariableType()+":"+arg.getContent());
		// }

	}

	private void readLoop(Loop loop, StringBuffer buffer)
			throws UnInitializeContextException {

		// THIS HANDLING IS ADDED TO TAKE CARE OF LOOP THROUGH CONDITIONS HAVING
		// /* IN THE XPATH
		// @LOOP THROUGH STAR CONDITION
		String loopThroughCond = loop.getThrough();
		if (ExceptionHandler.checkNullOrEmpty(loopThroughCond)) {
			ExceptionHandler
					.raiseUnInitializeContextException("XPath is missing in LOOP statement");
		}
		loopThroughCond = HandleStarInXpath.handlehStarInXpath(loopThroughCond,
				context);

		buffer.append(AlgoConstant.Loop);
		buffer.append(AlgoConstant.DoubleQuote).append(loopThroughCond)
				.append(AlgoConstant.DoubleQuote);
		buffer.append("\n");

		for (Object content : loop.getContent()) {
			readContent(content, buffer);
		}

		buffer.append(AlgoConstant.EndLoop);
		buffer.append("\n");
	}

	private boolean ignoreVariableTypeAssignment(String variableType) {
		if (variableType != null
				&& (variableType.equals("CONSTANT")
						// || variableType.equals("COLUMN_STRING")
						// || variableType.equals("COLUMN_NUMERIC")
						|| variableType.equals("UI")
						|| variableType.equals("UI_NUMERIC") || variableType
						.equals("UI_STRING"))) {
			return true;
		}
		return false;
	}

	/**
	 * @param variableType
	 * @return THIS METHOD IS ADDED TO HANDLE A CONDITION FOUND IN CA-NY
	 *         ALGORITHMS WHERE ZipCode = true
	 * 
	 */
	private boolean ignoreVariableTypeAssignmentNew(String variableType) {
		if (variableType != null
				&& (/*
					 * variableType.equals("CONSTANT") ||
					 */variableType.equals("COLUMN_STRING")
						|| variableType.equals("COLUMN_NUMERIC")
						|| variableType.equals("UI")
						|| variableType.equals("UI_NUMERIC") || variableType
						.equals("UI_STRING"))) {
			return true;
		}
		return false;
	}

	private String wrapVariableTypeAssignment(String variableType,
			String content) {
		StringBuffer buffer = new StringBuffer();
		content = updateContent(content);
		if (variableType != null
				&& (variableType.equals("XPATH")
						|| variableType.equals("XPATH_STRING") || variableType
						.equals("XPATH_NUMERIC"))
				|| variableType.equals("XPATH_DATE")) {

			// this condition is added to handle xpath date variable type in
			// 3.2.2 and 3.2.3 version circulars
			if (variableType.equals("XPATH_DATE")) {
				// as XPATH_DATE is not a variable type in grammar
				// we are simply replacing it with XPATH as they both works
				// same[as per ISO ]
				variableType = "XPATH";
			}

			// THIS CODE IS ADDED TO CHECK IF CONDITION WITH /* IN IT
			// @LOOP THROUGH STAR CONDITION
			content = HandleStarInXpath.handlehStarInXpath(content, context);

			buffer.append(variableType).append(AlgoConstant.Colon)
					.append(AlgoConstant.DoubleQuote).append(content)
					.append(AlgoConstant.DoubleQuote);
			return buffer.toString();
		} else if (content.contains("{")) {
			try {
				return buffer.append(updateVariableType(content)).toString();
			} catch (UnInitializeContextException e) {
				System.out
						.println("Error occured in processing @wrapVariableTypeAssignment");
				return buffer.append(content).toString();
			}
		} else if (variableType != null && !"RT".equals(variableType)) {
			return buffer.append(variableType).append(AlgoConstant.Colon)
					.append(content).toString();
		} else {
			return buffer.append(content).toString();
		}
	}

	private void readAssign(Assign assign, StringBuffer buffer)
			throws UnInitializeContextException {

		if (!(ignoreVariableTypeAssignmentNew(assign.getVariableType()))) {
			buffer.append(wrapVariableTypeAssignment(assign.getVariableType(),
					assign.getLValue().trim()));
		} else {
			buffer.append(updateContent(assign.getLValue().trim()));
		}

		buffer.append(AlgoConstant.Equals);

		// THIS ADDED TO CHECK IF THE VARAIBLE TYPE OF THE ASSIGN VARIABLE IS
		// TIMESPAN
		if ("LV_TIMESPAN".equals(assign.getVariableType())) {
			buffer.append(" days_between ");
			buffer.append("(");
		}

		for (Object content : assign.getContent()) {
			readContent(content, buffer);
		}

		if ("LV_TIMESPAN".equals(assign.getVariableType())) {
			buffer.append(")");
		}

		buffer.append("\n");
	}

	private void readIf(If _if, StringBuffer buffer)
			throws UnInitializeContextException {

		/*
		 * if(checkEmptyStatementBlock(_if.getContent())) return;
		 */
		if (!checkEmptyStatementBlock(_if.getContent())) {
			buffer.append(AlgoConstant.If);

			for (Object content : _if.getContent()) {
				readContent(content, buffer);
			}

			buffer.append(AlgoConstant.EndIf).append("\n");
		} else {
			ExceptionHandler
					.raiseUnInitializeContextException("Content inside IF is Empty");
		}

	}

	private boolean checkEmptyStatementBlock(List<Object> content) {

		// if(content instanceof List){
		if (content.size() > 0) {
			if (content.size() == 1) {
				if (content.get(0) instanceof String) {
					return true;
				}
			}
			return false;
		}

		return true;

	}

	/**
	 * @param functiontext
	 * @return ADDED TO HANDLE CONTAINS CUSTOM FUNCTION IN ALGORITHM FILE
	 * 
	 */
	private String handleContainsOperator(String functiontext) {

		String firstPart = functiontext.substring(0,
				functiontext.indexOf("Contains") - 1);
		String operator = "Contains";
		String secondPart = functiontext.substring(functiontext
				.indexOf(".Contains") + 9);
		// secondPart = secondPart.replace("&quot;","\"");
		return operator + "(" + firstPart + "," + secondPart + ")";

	}

	private String updateVariableType(String str)
			throws UnInitializeContextException {

		if (context == null) {
			ISOConsumptionLogger.info("Please set the Context");
			throw new UnInitializeContextException("UnInitializeContextError");
		}

		if (str.contains("Total Premium")) {
			return "Premium";
		} else {
			StringBuffer xpathBuffer = new StringBuffer();
			StringBuffer value = new StringBuffer();

			context.putValue(ContextParam.XPATHSTRING, xpathBuffer);
			context.putValue(ContextParam.ISPREDICATE, false);
			context.putValue(ContextParam.ISFUNCTION, false);
			context.putValue(ContextParam.FUNCTIONNAME, "");
			/*
			 * if(str != null && str.contains("["))
			 * context.putValue(ContextParam.PREDICATEPARSER, true);
			 */

			XPathActivity parse = new XPathParserImpl(context,
					new StringBuffer(str.trim()));
			Evaluator.Type type;
			try {
				type = (Type) parse.execute();
				if (type.equals(Type.PATH)) {
					value.append("XPATH:\"")
							.append(context.getValue(ContextParam.XPATHSTRING))
							.append("\"");
				} else if (type.equals(Type.ATTRIBUTE)) {
					value.append(ArgumentVariable.assignType((Database) context
							.getValue(ContextParam.OBJECTDATABASE),
							(String) context.getValue(ContextParam.REFERENCE),
							(String) context.getValue(ContextParam.SCOPE),
							context.getValue(ContextParam.XPATHSTRING)
									.toString()));
				} else {
					value.append(context.getValue(ContextParam.XPATHSTRING));
				}
			} catch (Exception e) {
				StringBuffer error = new StringBuffer();
				error.append("Error : ").append(e)
						.append(" During Processing ").append(str.trim())
						.append(" in ");
				error.append("Reference - [")
						.append(context.getValue(ContextParam.REFERENCE))
						.append("] ");
				error.append("Scope - [")
						.append(context.getValue(ContextParam.SCOPE))
						.append("]");
				// ISOConsumptionLogger.info(error);
				value.append("XPATH:\"").append(str.trim()).append("\"");
			}

			return value.toString();
		}
	}
}