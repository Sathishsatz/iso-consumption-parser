package com.mmpnc.rating.iso.algorithm.parse.evaluators;

import org.antlr.runtime.RecognitionException;

import com.mmpnc.context.Context;
import com.mmpnc.context.ContextParam;
import com.mmpnc.rating.iso.algorithm.Evaluator;

public class PlusEvaluator implements Evaluator {

	private Context context;
	private Evaluator left;
	private Evaluator right;
	
	public PlusEvaluator(Evaluator e1, Evaluator e2) {
		this.left = e1;
		this.right = e2;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public Object evaluate() throws RecognitionException{
		StringBuffer plus = new StringBuffer();
		plus.append(" ( ");
		left.setContext(context);
		plus.append(left.evaluate());
		plus.append(" + ");
		right.setContext(context);
		plus.append(right.evaluate());
		plus.append(" ) "); 
		context.putValue(ContextParam.MODELATTRIBUTE, true);
		context.putValue(ContextParam.ISSOURCESTATICVAR, false);
		return plus;
	}

}
