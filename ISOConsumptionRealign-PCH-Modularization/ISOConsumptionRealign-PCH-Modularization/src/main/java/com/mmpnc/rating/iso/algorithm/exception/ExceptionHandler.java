package com.mmpnc.rating.iso.algorithm.exception;

import java.util.List;

public class ExceptionHandler {
	public static void raiseParserException(String message)
			throws ParserException {
		throw new ParserException(message);
	}

	public static void raiseScopeMergingException(String message)
			throws ScopeMergingException {
		throw new ScopeMergingException(message);
	}

	public static void raiseUnInitializeContextException(String message)
			throws UnInitializeContextException {
		throw new UnInitializeContextException(message);
	}

	public static void raiseFlowchartException(String message)
			throws FlowchartException {
		throw new FlowchartException(message);
	}

	public static void raiseBasexException(String message, Throwable e)
			throws BasexException {
		throw new BasexException(message, e);
	}

	public static void raiseBasexException(Throwable e) throws BasexException {
		// throw new BasexException(e);
	}

	public static void raiseException(String message, Exception e)
			throws Exception {
		throw new Exception(message, e);
	}

	public static boolean checkNull(Object obj) {
		if (obj == null) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkNullOrEmpty(Object obj) {
		if (obj == null) {
			return true;
		} else if (obj.toString().equals("")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkNullOrSize(List<?> object) {
		if (object == null) {
			return true;
		} else if (object.size() == 0) {
			return true;
		}
		return false;
	}
}
