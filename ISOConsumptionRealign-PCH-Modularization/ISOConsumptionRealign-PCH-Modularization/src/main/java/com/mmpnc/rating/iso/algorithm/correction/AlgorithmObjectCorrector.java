package com.mmpnc.rating.iso.algorithm.correction;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.majescomastek.stgicd.utils.JAXBUtils;
import com.mmpnc.connection.helper.BasexDatabaseNames;
import com.mmpnc.connection.helper.DBFactoryBuilder;
import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.Activity;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;
import com.mmpnc.rating.iso.algorithm.merge.scope.IMergeScopeWithMultiplePass;
import com.mmpnc.rating.iso.algorithm.merge.scope.ImplMergeScopeWithMultiplePass;
import com.mmpnc.rating.iso.algorithm.vo.LOB;
import com.mmpnc.rating.iso.algorithm.vo.PCH;
import com.mmpnc.rating.iso.algorithm.vo.Reference;
import com.mmpnc.rating.iso.algorithm.vo.Scope;
import com.mmpnc.rating.iso.datastructure.AlgorithmDataStructure;
import com.mmpnc.rating.iso.flowchart.correction.IModifyFlowChart;
import com.mmpnc.rating.iso.flowchart.correction.ModifyFlowChart;

/**
 * @author nilkanth9581
 * 
 */
public abstract class AlgorithmObjectCorrector {

	private Reader algoReader;
	private LOB lob;
	private Database xPathDB;
	private Database dbForLoop;

	public AlgorithmObjectCorrector(Reader algoReader) {
		this.algoReader = algoReader;

	}

	private void buildDatabase() throws BasexException {

		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				if (((Reference) ref).getName().equals("Table Objects")) {
					AlgorithmDataStructure ds = new AlgorithmDataStructure(
							(Reference) ref);
					StringBuffer xmlDS = ds.simplifyDataStrucure();

					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("DSFile", xmlDS.toString());

					this.xPathDB = DBFactoryBuilder.getXMLDatabase(
							BasexDatabaseNames.RUNTIMEDSDB, xmlMap);
					this.xPathDB.buildDatabase();

					Reference reference = (Reference) ref;
					// JAXBContext jaxbContext =
					// JAXBContext.newInstance(Reference.class);
					String refInXml = null;
					try {
						refInXml = JAXBUtils.writeFromObject(reference);
					} catch (Exception e) {
						ExceptionHandler.raiseBasexException(
								"Unable to create dabase for LOOP", e);
					}

					Map<String, String> xmlLoopMap = new HashMap<String, String>();
					xmlLoopMap.put("DSFileForLoop", refInXml);

					this.dbForLoop = DBFactoryBuilder.getXMLDatabase(
							BasexDatabaseNames.RUNTIMEDSFORLOOP, xmlLoopMap);
					this.dbForLoop.buildDatabase();
				}

			}
		}

	}

	private void unmarshalLOB() throws Exception {
		JAXBContext jbContext = JAXBContext
				.newInstance("com.mmpnc.rating.iso.algorithm.vo");
		Unmarshaller unmarshal = jbContext.createUnmarshaller();

		lob = (LOB) unmarshal.unmarshal(this.algoReader);
	}

	private void rearrangeLOBUsingFlowChart() throws Exception {
		try {
			CorrectionAccordingToExecutionFlow cuExecutionFlow = CorrectionAccordingToExecutionFlow
					.getInstance();
			this.lob = cuExecutionFlow.rearrageLOBUsingFlowChart(lob);
		} catch (JAXBException e) {
			ExceptionHandler.raiseException("Unable to unmarshal XML File", e);
		}
	}

	public void process() throws Exception {
		unmarshalLOB();

		// This functionality added to address 3.2.2 and 3.2.3 circulars
		// flowchart related changes
		IModifyFlowChart modifyFlowChart = ModifyFlowChart.getInstance();
		modifyFlowChart.modifyFlowChart(lob);
		//System.out.println("Modifying flow chart has been completed");
		// This call is written to merge scopes with multiple passes into a
		// single scope
		// @TODO
		IMergeScopeWithMultiplePass merge = new ImplMergeScopeWithMultiplePass();
		merge.mergeScopesWithMultiplePass(lob);
		reAlignObject();
		rearrangeLOBUsingFlowChart();
		buildDatabase();
		
		// updateFulliQualifiedPath();
		output(this.lob, this.xPathDB, this.dbForLoop);
	}

	
	/**
	 * @param lob
	 */
	protected void printAlgorithmsAndPCHNames(LOB lob){
		
		StringBuffer sb = new StringBuffer();
		for(Object content:lob.getContent()){
			if(content instanceof Reference){
				Reference reference = (Reference)content;
				
				if("Common Rating".equals(reference.getType()) || "Premium Calculation".equals(reference.getType()))
				for(Object refCont:reference.getContent()){
					sb.append("Refrence Name:["+reference.getName()+"] referecne type:["+reference.getType()+"]").append("\n");
					
					if(refCont instanceof Scope){
					
						Scope scope = (Scope)refCont;
						sb.append("\t").append("Scope Name:["+scope.getDbTables()+"]").append("\n");
						for(Object scopeCont:scope.getContent()){
							if(scopeCont instanceof PCH){
								sb.append("\t\t").append("PCH Name:["+((PCH)scopeCont).getName()+"]").append("\n");
							}
						}
					}
				}
			}
		}
		System.out.println(sb.toString());
		
	}
	
	
	private void reAlignObject() throws Exception {
		Activity realign = new RealignAlgorithmObject();
		this.lob = realign.process(this.lob);

	}

	@SuppressWarnings("unused")
	private void updateFulliQualifiedPath() throws Exception {
		if (this.lob == null) {
			reAlignObject();
		}

		for (Object ref : lob.getContent()) {
			if (ref instanceof Reference) {
				if (((Reference) ref).getName().equals("Table Objects")) {
					AlgorithmDataStructure ds = new AlgorithmDataStructure(
							(Reference) ref);
					StringBuffer xmlDS = ds.simplifyDataStrucure();

					Map<String, String> xmlMap = new HashMap<String, String>();
					xmlMap.put("DSFile", xmlDS.toString());

					/*
					 * Database db =
					 * DBFactoryBuilder.getXMLDatabase("RuntimeDSDB", xmlMap);
					 * db.buildDatabase();
					 */

					Activity qlAlgo = new QualifiedAlgorithObject(xPathDB);

					this.lob = qlAlgo.process(this.lob);

					xPathDB.closeDatabase();
				}
			}
		}
	}

	public abstract void output(LOB lob, Database db, Database dbForLoop);

}