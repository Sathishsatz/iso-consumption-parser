package com.mmpnc.connection.helper;

import java.util.Map;

import com.mmpnc.connection.xmldb.XMLConnection;
import com.mmpnc.connection.xmldb.XmlDatabase;
import com.mmpnc.connection.xmldb.XmlQuery;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;


public class DBFactoryBuilder {

	public static Database getXMLDatabase(BasexDatabaseNames dbName,Map<String,String> xmlMap) throws BasexException{
		XmlDatabase db = new XmlDatabase(dbName.toString(),xmlMap);
		return db.buildDatabase();
	}
	
	public static Database getXMLDatabase(String dbName , Map<String,String>xmlMap) throws BasexException{
		XmlDatabase db = new XmlDatabase(dbName,xmlMap);
		return db.buildDatabase();
	}
	
	public static Connection getXmlConnection(Database db){
		return new XMLConnection(db);
	}
	
	public static Query getXMLQuery(Connection con){
		return new XmlQuery(con);
	}
}
