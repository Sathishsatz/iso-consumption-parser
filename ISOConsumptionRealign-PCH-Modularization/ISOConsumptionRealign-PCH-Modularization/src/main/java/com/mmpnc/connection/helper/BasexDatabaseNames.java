package com.mmpnc.connection.helper;

/**
 * @author nilkanth9581
 *
 */
public enum BasexDatabaseNames {
	RUNTIMEDSDB,RUNTIMEFLOWCHARTDB,CURRENTSCOPEDB,RUNTIMERCDB,RUNTIMEDSFORLOOP,RUNTIMELOBDATABASE,RUNTIMETABLEOBJECTSDB;
}
