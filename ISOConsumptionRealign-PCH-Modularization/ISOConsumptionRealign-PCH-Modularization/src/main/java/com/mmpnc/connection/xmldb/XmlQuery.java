package com.mmpnc.connection.xmldb;

import org.basex.core.BaseXException;
import org.basex.core.cmd.XQuery;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Query;

public class XmlQuery implements Query {
	private XMLConnection connection;
	private String xQuery;
	
	public XmlQuery(Connection con) {
		this.setConnection(con);
	}

	@Override
	public Query createQuery(String query) {
		this.xQuery = query;
		return this;
	}

	@Override
	public Object execute()  {
		XmlDatabase database = (XmlDatabase) connection.getDatabase();
		try {
			return new XQuery(this.xQuery).execute(database.getContext());
		} catch (BaseXException e) {
		}
		return null;
	}

	public void setConnection(Connection connection) {
		this.connection = (XMLConnection) connection;
	}

	public Connection getConnection() {
		return connection;
	}

}
