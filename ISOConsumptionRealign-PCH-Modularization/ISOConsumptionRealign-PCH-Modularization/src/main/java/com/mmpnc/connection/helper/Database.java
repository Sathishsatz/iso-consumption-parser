package com.mmpnc.connection.helper;

import com.mmpnc.rating.iso.algorithm.exception.BasexException;

public interface Database {
	Database buildDatabase() throws BasexException;
	void closeDatabase() throws BasexException;
	void dropDatabase(String dbName) throws BasexException;
}
