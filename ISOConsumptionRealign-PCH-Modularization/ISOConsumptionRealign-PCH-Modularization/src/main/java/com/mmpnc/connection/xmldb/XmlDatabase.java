package com.mmpnc.connection.xmldb;

import java.util.HashMap;
import java.util.Map;

import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.Add;
import org.basex.core.cmd.Close;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.DropDB;
import org.basex.core.cmd.Optimize;
import org.basex.core.cmd.Set;

import com.mmpnc.connection.helper.Database;
import com.mmpnc.rating.iso.algorithm.exception.BasexException;
import com.mmpnc.rating.iso.algorithm.exception.ExceptionHandler;

public class XmlDatabase implements Database {

	/** Local database context. */
	private Context context;
	private String dbName;
	private Map<String, String> xmlMap;
	
	public XmlDatabase(String databaseName, Map<String, String> xmlMap) {
		context = new Context();
		this.dbName = databaseName;
		if (xmlMap == null) {
			this.xmlMap = new HashMap<String, String>();
		} else {
			this.xmlMap = xmlMap;
		}
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public Map<String, String> getXmlMap() {
		return xmlMap;
	}

	public void setXmlMap(Map<String, String> xmlMap) {
		this.xmlMap = xmlMap;
	}
	
	/**
	 * 
	 * @param id
	 * @param xml String / remote/local xml File
	 * @return
	 */
	public XmlDatabase addXml(String id, String xml) {
		xmlMap.put(id, xml);
		return this;
	}

	@Override
	public Database buildDatabase() throws BasexException {
		try {
			new Set("intparse", true).execute(context);
			new CreateDB(this.dbName).execute(context);
		} catch (BaseXException e) {
			ExceptionHandler.raiseBasexException(e);
		}

		if (xmlMap != null && xmlMap.size() > 0) {
			// add the documents/xml string to open database
			for (String id : xmlMap.keySet()) {
				try {
					new Add(id, xmlMap.get(id)).execute(context);
				} catch (BaseXException e) {
					ExceptionHandler.raiseBasexException(e);
				}
			}
		}
		try {
			new Optimize().execute(context);
		} catch (BaseXException e) {
			ExceptionHandler.raiseBasexException(e);
		}

		return this;
	}

	@Override
	public void closeDatabase() throws BasexException {
		try {
			new Close().execute(context);
		} catch (BaseXException e) {
			ExceptionHandler.raiseBasexException("Unable to Close Database : " + this.dbName, e);
		}
	}

	@Override
	public void dropDatabase(String dbName) throws BasexException {
		try {
			new DropDB("dbName").execute(context);
		} catch (BaseXException e) {
			ExceptionHandler.raiseBasexException("Problem in dropping the databse",e);
		}
		
	}
	
	
}
