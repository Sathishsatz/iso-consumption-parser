package com.mmpnc.connection.helper;


public interface Query {
	Query createQuery(String query);
	Object execute();
}
