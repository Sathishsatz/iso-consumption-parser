package com.mmpnc.connection.xmldb;

import com.mmpnc.connection.helper.Connection;
import com.mmpnc.connection.helper.Database;

public class XMLConnection implements Connection {
	private Database database;

	public XMLConnection(Database db) {
		this.database = db;
	}

	@Override
	public Connection getConnetion() {
		return this;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public Database getDatabase() {
		return database;
	}
}
