tree grammar XPathWalker;

options {
  language = Java;
  tokenVocab = XPath;
  ASTLabelType = CommonTree;
}

@header {
package com.mmpnc.xml.xpath.parse;

import com.mmpnc.context.Context;
import com.mmpnc.rating.iso.algorithm.Evaluator;
import com.mmpnc.rating.iso.algorithm.xml.xpath.parse.evaluator.*;
}

@members {
private Context context = null;
}

xpath[Context context] returns [ Evaluator.Type type]
  : { this.context = context;}
  e1=expr 
      { 
        //System.out.println("xpath called");
        Evaluator eval = $e1.eval;
        eval.setContext(context);
        type = (Evaluator.Type)eval.evaluate();
      }
  ;

expr returns [Evaluator eval] : 
      ^('+' e1=expr e2=expr)  {$eval = new PlusEvaluator($e1.eval , $e2.eval);}
    | ^('-' e1=expr e2=expr)  {$eval = new MinusEvaluator($e1.eval , $e2.eval);}
    | ^('*' e1=expr e2=expr)  {$eval = new MultiplyEvaluator($e1.eval , $e2.eval);}
    | ^('div' e1=expr e2=expr){$eval = new DivideEvaluator($e1.eval , $e2.eval);}
    | ^('mod' e1=expr e2=expr){$eval = new ModEvaluator($e1.eval , $e2.eval);}
    | ^('and' e1=expr e2=expr){$eval = new AndEvaluator($e1.eval , $e2.eval);}
    | ^('or' e1=expr e2=expr) {$eval = new OrEvaluator($e1.eval , $e2.eval);}
    | ^('<' e1=expr e2=expr)  {$eval = new LessThanEvaluator($e1.eval , $e2.eval);}
    | ^('>' e1=expr e2=expr)  {$eval = new GreaterThanEvaluator($e1.eval , $e2.eval);}
    | ^('<=' e1=expr e2=expr) {$eval = new LessThanEqualsEvaluator($e1.eval , $e2.eval);}
    | ^('>=' e1=expr e2=expr) {$eval = new GreaterThanEqualsEvaluator($e1.eval , $e2.eval);}
    | ^('=' e1=expr e2=expr)  {$eval = new EqualsEvaluator($e1.eval , $e2.eval);}
    | ^('!=' e1=expr e2=expr) {$eval = new NotEqualsEvaluator($e1.eval , $e2.eval);}
    | ^('|' e1=expr e2=expr)  {$eval = new UnionEvaluator($e1.eval, $e2.eval);}
    | ^(NEGETION e1=expr)     {$eval = new NegationEvaluator($e1.eval);}
    | l1=locationPath         { $eval = $l1.eval; }
    | function { $eval = $function.eval; }
    | Literal {$eval = new LiteralEvaluator($Literal.text);}
    | Number {$eval = new NumberEvaluator($Number.text);}
    | s1=step { $eval = $s1.eval; }
    ;
 
locationPath returns [ Evaluator eval ]
  :   r1=relativeLocationPath { $eval = $r1.eval; }
  |   a1=absoluteLocationPathNoroot { $eval = $a1.eval; }
  ;

absoluteLocationPathNoroot returns [Evaluator eval ]
  : ^(CURRENT e1=expr) { $eval = new CurrentPathEvaluator($e1.eval); }
  | ^(RELATIVEFROMROOT e1=expr){ $eval = new RelativePathEvaluator($e1.eval); }
  ;

function returns [ Evaluator eval]:
  {
      List<Evaluator> evallist = new ArrayList<Evaluator>();
  }
  ^(FUNCTION fname=functionName ( e1=expr { evallist.add($e1.eval); } ( ',' e2=expr { evallist.add($e2.eval); })* )?) 
    { 
      $eval = new FunctionEvaluator($fname.eval, evallist);
    }
  ;

functionName returns [ Evaluator eval]
  :  e1=qName { $eval = $e1.eval; }
  ;

relativeLocationPath returns [ Evaluator eval]
  : ^((t=PATH|t=RELATIVEPATH) e1=expr e2=expr?) {$eval = new PathEvaluator($t.text, $e1.eval, $e2.eval);}
  ;

step returns [Evaluator eval]:
  {
     List<Evaluator> predicateEval = new ArrayList<Evaluator>();
  } 
  (a1=AxisName | a2=AT)? n1=nodeTest (p1=predicate { predicateEval.add($p1.eval);})*
  { 
    if ($a1 != null) { 
       $eval = new StepEvaluator($a1.text,$n1.eval, predicateEval);
    } else if ($a2 != null){ 
       $eval = new StepEvaluator($a2.text,$n1.eval, predicateEval);
    } else {    
       $eval = new StepEvaluator(null,$n1.eval, predicateEval);
    }
  }
  | CURRENTNODE {$eval = new AttributeEvaluator(".");}
  | PRIORNODE {$eval = new AttributeEvaluator("..");}
  ;

predicate returns [ Evaluator eval ]
  :  '[' expr ']' { $eval = new PredicateEvaluator($expr.eval);}
  ;

nodeTest returns [Evaluator eval]:  ^(NAMETEST  nameTest) { $eval = $nameTest.eval; }
  |   ^( NODETEST NodeType) { $eval = new NodeEvaluator($NodeType.text, null);}
  |   ^( NODETEST NodeType Literal) { $eval = new NodeEvaluator($NodeType.text, $Literal.text);}
  ;
  
qName returns [Evaluator eval] :  e1=nCName (':' e2=nCName)? 
    { 
      if(e2 != null){
        System.out.println("We will need to handle this case");
        $eval = $e1.eval;
      }
      else{  
        $eval = $e1.eval;
      }      
    }
  ;

nameTest returns [Evaluator eval] :  '*' { $eval = new AttributeEvaluator("*");}
  |  nCName ':' '*' { $eval = $nCName.eval ; System.out.println("Need to Handle * in nameTest");}
  |  qName { $eval = $qName.eval; }
  ;

nCName returns [Evaluator eval]  :  NCName { $eval = new AttributeEvaluator($NCName.text); }
  |  AxisName {$eval = new AxisNameEvaluator($AxisName.text); }
  ;    