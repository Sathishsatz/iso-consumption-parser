package com.mmpnc.rating.icm.algorithm.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="DomainTableKey")
public class DomainTableKey {
	@Id
	@GeneratedValue
	private String id;

	@Column(name = "sequence")
	private int sequence;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

	@ManyToOne(targetEntity = DomainTable.class)
	@JoinColumn(name = "domainTableId")
	private DomainTable domainTable;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDomainTable(DomainTable domainTable) {
		this.domainTable = domainTable;
	}

	public DomainTable getDomainTable() {
		return domainTable;
	}
	
}
