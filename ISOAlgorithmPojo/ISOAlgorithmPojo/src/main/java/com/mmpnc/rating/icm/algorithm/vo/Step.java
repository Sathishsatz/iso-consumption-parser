package com.mmpnc.rating.icm.algorithm.vo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RatingStep", propOrder = {
		"id",
		"executionorder",
		"block"
})
@Entity
@Table(name = "RatingStep")
public class Step {
	@Id
	@GeneratedValue
	@XmlElement(name="Id")
	private int id;
	
	@XmlElement(name="ExecutionOrder")
	private int executionorder;

	@Column(length = 10000)
	@XmlElement(name="Block")
	private String block;

	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Process.class)
	@JoinColumn(name = "processid")
	@XmlTransient
	private Process process;
	
	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Program.class)
	@JoinColumn(name = "programid")
	@XmlTransient
	private Program program;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public void setExecutionorder(int executionorder) {
		this.executionorder = executionorder;
	}

	public int getExecutionorder() {
		return executionorder;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public Program getProgram() {
		return program;
	}

	@Override
	public String toString() {
		return process.toString() + " [ " + this.block + " ]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Step) {
			Step step = (Step) obj;
			if (step.getBlock().equals(this.block)) {
				if(step.getProcess()!= null && step.getProcess().getName().equals(this.process.getName())){
					if(step.getProgram()!= null && step.getProgram().getName().equals(this.program.getName())){
						return true;
					} else if(step.getProgram() == null){
						return true;
					} else{
						return false;
					}
				}else if(step.getProgram()!= null && step.getProgram().getName().equals(this.program.getName())){
					return true;
				}else{
					return false;
				}
			} else {
				return super.equals(obj);
			}
		} else {
			return super.equals(obj);
		}
	}
}
