package com.mmpnc.rating.icm.algorithm.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RatingEntity", propOrder = { "id", "name", "references",
		"processes" })
@Entity
@Table(name = "RatingEntity")
public class RatingEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@XmlElement(name = "Id")
	private int id;

	@XmlElement(name = "Name")
	private String name;

	@OneToMany(mappedBy = "entity")
	@XmlElement(name = "EntityReference")
	private List<Reference> references;

	@OneToMany(mappedBy = "entity")
	@OrderBy(value = "executionorder")
	@XmlElement(name = "RatingProcess")
	private List<Process> processes;
	
	@ManyToOne(targetEntity = Ratebook.class)
	@JoinColumn(name = "ratebookid")
	@XmlTransient
	private Ratebook ratebook;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Reference> getReferences() {
		if (this.references == null) {
			this.references = new ArrayList<Reference>();
		}
		return references;
	}

	public void setReference(Reference reference) {
		if (this.references == null) {
			this.references = new ArrayList<Reference>();
		}
		this.references.add(reference);
	}

	public List<Process> getProcesses() {
		if (this.processes == null) {
			this.processes = new ArrayList<Process>();
		}
		return processes;
	}

	public void setProcesse(Process process) {
		if (this.processes == null) {
			this.processes = new ArrayList<Process>();
		}
		this.processes.add(process);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RatingEntity) {
			RatingEntity entity = (RatingEntity) obj;
			if (entity.getName().equals(name)) {
				return true;
			} else {
				return super.equals(obj);
			}
		} else {
			return super.equals(obj);
		}
	}

	@Override
	public String toString() {
		return name;
	}

	public void setRatebook(Ratebook ratebook) {
		this.ratebook = ratebook;
	}

	public Ratebook getRatebook() {
		return ratebook;
	}

}
