package com.mmpnc.rating.icm.algorithm.vo;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.mmpnc.policy.iso.vo
	 * 
	 */
	public ObjectFactory() {
	}

	public Ratebook createRatebook() {
		return new Ratebook();
	}

	public RatingEntity createRatingEntity() {
		return new RatingEntity();
	}

}
