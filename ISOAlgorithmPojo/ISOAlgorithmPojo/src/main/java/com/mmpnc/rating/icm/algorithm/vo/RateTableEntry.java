package com.mmpnc.rating.icm.algorithm.vo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RateTableEntry")
public class RateTableEntry {

	@Id
	@GeneratedValue
	private int id;
	private String addChangeDeleteFlag;
	private String column1;
	private String column2;
	private String column3;
	private String column4;
	private String column5;
	private String column6;
	private String column7;
	private String column8;
	private String column9;
	private String column10;
	private Double ncolumn1;
	private Double ncolumn2;
	private Double ncolumn3;
	private Double ncolumn4;
	private Double ncolumn5;
	private Double ncolumn6;
	private Double ncolumn7;
	private Double ncolumn8;
	private Double ncolumn9;
	private Double ncolumn10;
	private int sequence;
	private String value;
	private Double nvalue;

	@ManyToOne(targetEntity = Ratebook.class)
	@JoinColumn(name = "ratebookid")
	private Ratebook ratebook;

	@ManyToOne(targetEntity = RateTable.class)
	@JoinColumn(name = "ratetableid")
	private RateTable rateTable;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddChangeDeleteFlag() {
		return addChangeDeleteFlag;
	}

	public void setAddChangeDeleteFlag(String addChangeDeleteFlag) {
		this.addChangeDeleteFlag = addChangeDeleteFlag;
	}

	public String getColumn1() {
		return column1;
	}

	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public String getColumn2() {
		return column2;
	}

	public void setColumn2(String column2) {
		this.column2 = column2;
	}

	public String getColumn3() {
		return column3;
	}

	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumn4() {
		return column4;
	}

	public void setColumn4(String column4) {
		this.column4 = column4;
	}

	public String getColumn5() {
		return column5;
	}

	public void setColumn5(String column5) {
		this.column5 = column5;
	}

	public String getColumn6() {
		return column6;
	}

	public void setColumn6(String column6) {
		this.column6 = column6;
	}

	public String getColumn7() {
		return column7;
	}

	public void setColumn7(String column7) {
		this.column7 = column7;
	}

	public String getColumn8() {
		return column8;
	}

	public void setColumn8(String column8) {
		this.column8 = column8;
	}

	public String getColumn9() {
		return column9;
	}

	public void setColumn9(String column9) {
		this.column9 = column9;
	}

	public String getColumn10() {
		return column10;
	}

	public void setColumn10(String column10) {
		this.column10 = column10;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Ratebook getRatebook() {
		return ratebook;
	}

	public void setRatebook(Ratebook ratebook) {
		this.ratebook = ratebook;
	}

	public RateTable getRateTable() {
		return rateTable;
	}

	public void setRateTable(RateTable rateTable) {
		this.rateTable = rateTable;
	}

	public Double getNcolumn1() {
		return ncolumn1;
	}

	public void setNcolumn1(Double ncolumn1) {
		this.ncolumn1 = ncolumn1;
	}

	public Double getNcolumn2() {
		return ncolumn2;
	}

	public void setNcolumn2(Double ncolumn2) {
		this.ncolumn2 = ncolumn2;
	}

	public Double getNcolumn3() {
		return ncolumn3;
	}

	public void setNcolumn3(Double ncolumn3) {
		this.ncolumn3 = ncolumn3;
	}

	public Double getNcolumn4() {
		return ncolumn4;
	}

	public void setNcolumn4(Double ncolumn4) {
		this.ncolumn4 = ncolumn4;
	}

	public Double getNcolumn5() {
		return ncolumn5;
	}

	public void setNcolumn5(Double ncolumn5) {
		this.ncolumn5 = ncolumn5;
	}

	public Double getNcolumn6() {
		return ncolumn6;
	}

	public void setNcolumn6(Double ncolumn6) {
		this.ncolumn6 = ncolumn6;
	}

	public Double getNcolumn7() {
		return ncolumn7;
	}

	public void setNcolumn7(Double ncolumn7) {
		this.ncolumn7 = ncolumn7;
	}

	public Double getNcolumn8() {
		return ncolumn8;
	}

	public void setNcolumn8(Double ncolumn8) {
		this.ncolumn8 = ncolumn8;
	}

	public Double getNcolumn9() {
		return ncolumn9;
	}

	public void setNcolumn9(Double ncolumn9) {
		this.ncolumn9 = ncolumn9;
	}

	public Double getNcolumn10() {
		return ncolumn10;
	}

	public void setNcolumn10(Double ncolumn10) {
		this.ncolumn10 = ncolumn10;
	}

	public Double getNvalue() {
		return nvalue;
	}

	public void setNvalue(Double nvalue) {
		this.nvalue = nvalue;
	}

}
