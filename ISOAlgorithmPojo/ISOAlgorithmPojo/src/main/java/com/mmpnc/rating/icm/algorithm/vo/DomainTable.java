package com.mmpnc.rating.icm.algorithm.vo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DomainTable")
public class DomainTable {
	@Id
	@GeneratedValue
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "title")
	private String title;

	@ManyToOne(targetEntity = Ratebook.class)
	@JoinColumn(name = "ratebookid")
	private Ratebook ratebook;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "domainTable", targetEntity = DomainTableKey.class)
	private List<DomainTableKey> domainTableKey;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "domainTable", targetEntity = DomainTableEntry.class)
	private List<DomainTableEntry> domainTableEntry;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Ratebook getRatebook() {
		return ratebook;
	}

	public void setRatebook(Ratebook ratebook) {
		this.ratebook = ratebook;
	}

	public void setDomainTableKey(List<DomainTableKey> domainTableKey) {
		this.domainTableKey = domainTableKey;
	}

	public List<DomainTableKey> getDomainTableKey() {
		if (this.domainTableKey == null) {
			this.domainTableKey = new ArrayList<DomainTableKey>();
		}
		return domainTableKey;
	}

	public void setDomainTableEntry(List<DomainTableEntry> domainTableEntry) {
		this.domainTableEntry = domainTableEntry;
	}

	public List<DomainTableEntry> getDomainTableEntry() {
		if (this.domainTableEntry == null) {
			this.domainTableEntry = new ArrayList<DomainTableEntry>();
		}
		return domainTableEntry;
	}

}
