package com.mmpnc.rating.icm.algorithm.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { 
		"id", 
		"title", 
		"newbusinessEffective",
		"newbusinessExpiration", 
		"renewalEffective", 
		"renewalExpiration",
		"ratingEntity" 
})
@XmlRootElement(name = "Ratebook")
@Entity
@Table(name = "Ratebook")
public class Ratebook implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@XmlElement(name = "Id")
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlID
	@XmlSchemaType(name = "ID")
	private String id;

	@Column(name = "TITLE")
	@XmlElement(name = "Title")
	private String title;

	@Column(name = "NEWBUSINESSEFFECTIVE")
	@XmlElement(name="NewBusinessEffective")
	private String newbusinessEffective;

	@Column(name = "NEWBUSINESSEXPIRATION")
	@XmlElement(name="NewBusinessExpiration")
	private String newbusinessExpiration;

	@Column(name = "RENEWALEFFECTIVE")
	@XmlElement(name="RenewalEffective")
	private String renewalEffective;

	@Column(name = "RENEWALEXPIRATION")
	@XmlElement(name="RenewalExpiration")
	private String renewalExpiration;

	@OneToOne(fetch = FetchType.LAZY, targetEntity = Ratebook.class)
	@JoinColumn(name = "ParentRatebookId")
	@XmlTransient
	private Ratebook ratebook;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ratebook")
	@XmlTransient
	private List<RateTable> rateTable;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ratebook")
	@XmlTransient
	private List<DomainTable> domainTable;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ratebook")
	@XmlElement(name = "RatingEntity")
	private List<RatingEntity> ratingEntity;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ratebook")
	@XmlTransient
	private List<Reference> reference;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNewbusinessEffective() {
		return newbusinessEffective;
	}

	public void setNewbusinessEffective(String newbusinessEffective) {
		this.newbusinessEffective = newbusinessEffective;
	}

	public String getNewbusinessExpiration() {
		return newbusinessExpiration;
	}

	public void setNewbusinessExpiration(String newbusinessExpiration) {
		this.newbusinessExpiration = newbusinessExpiration;
	}

	public String getRenewalEffective() {
		return renewalEffective;
	}

	public void setRenewalEffective(String renewalEffective) {
		this.renewalEffective = renewalEffective;
	}

	public String getRenewalExpiration() {
		return renewalExpiration;
	}

	public void setRenewalExpiration(String renewalExpiration) {
		this.renewalExpiration = renewalExpiration;
	}

	public Ratebook getRatebook() {
		return ratebook;
	}

	public void setRatebook(Ratebook ratebook) {
		this.ratebook = ratebook;
	}

	public void setRateTable(List<RateTable> rateTable) {
		this.rateTable = rateTable;
	}

	public List<RateTable> getRateTable() {
		if (this.rateTable == null) {
			this.rateTable = new ArrayList<RateTable>();
		}
		return rateTable;
	}

	public void setDomainTable(List<DomainTable> domainTable) {
		this.domainTable = domainTable;
	}

	public List<DomainTable> getDomainTable() {
		if (this.domainTable == null) {
			this.domainTable = new ArrayList<DomainTable>();
		}
		return domainTable;
	}

	public void setRatingEntity(List<RatingEntity> ratingEntity) {
		this.ratingEntity = ratingEntity;
	}

	public List<RatingEntity> getRatingEntity() {
		if (this.ratingEntity == null) {
			this.ratingEntity = new ArrayList<RatingEntity>();
		}
		return ratingEntity;
	}

	public void setReference(List<Reference> reference) {
		this.reference = reference;
	}

	public List<Reference> getReference() {
		if(this.reference == null){
			this.reference = new ArrayList<Reference>();
		}
		return reference;
	}

}
