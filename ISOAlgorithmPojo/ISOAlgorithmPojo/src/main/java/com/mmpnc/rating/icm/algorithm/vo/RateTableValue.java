package com.mmpnc.rating.icm.algorithm.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RateTableValue")
public class RateTableValue {

	@Id
	@GeneratedValue
	@Column(name="columnid")
	private String id;
	@Column(name = "COLUMNNAME")
	private String columnname;
	@Column(name = "TYPE")
	private String type;
	@Column(name = "SIZE")
	private String size;
	@Column(name = "SCALE")
	private String scale;

	@ManyToOne(targetEntity = RateTable.class)
	@JoinColumn(name = "ratetableid")
	private RateTable rateTable;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getColumnname() {
		return columnname;
	}

	public void setColumnname(String columnname) {
		this.columnname = columnname;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public RateTable getRateTable() {
		return rateTable;
	}

	public void setRateTable(RateTable rateTable) {
		this.rateTable = rateTable;
	}

}
