package com.mmpnc.rating.icm.algorithm.vo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Ratetable")
public class RateTable {
	@Id
	@GeneratedValue
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "title")
	private String title;

	@ManyToOne(targetEntity = Ratebook.class)
	@JoinColumn(name = "ratebookid")
	private Ratebook ratebook;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rateTable", targetEntity = RateTableKey.class)
	private List<RateTableKey> rateTableKey;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rateTable", targetEntity = RateTableValue.class)
	private List<RateTableValue> rateTableValue;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rateTable", targetEntity = RateTableEntry.class)
	private List<RateTableEntry> rateTableEntry;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Ratebook getRatebook() {
		return ratebook;
	}

	public void setRatebook(Ratebook ratebook) {
		this.ratebook = ratebook;
	}

	public void setRateTableKey(List<RateTableKey> rateTableKey) {
		this.rateTableKey = rateTableKey;
	}

	public List<RateTableKey> getRateTableKey() {
		if (rateTableKey == null) {
			rateTableKey = new ArrayList<RateTableKey>();
		}
		return rateTableKey;
	}

	public void setRateTableValue(List<RateTableValue> rateTableValue) {
		this.rateTableValue = rateTableValue;
	}

	public List<RateTableValue> getRateTableValue() {
		if (rateTableValue == null) {
			rateTableValue = new ArrayList<RateTableValue>();
		}
		return rateTableValue;
	}

	public void setRateTableEntry(List<RateTableEntry> rateTableEntry) {
		this.rateTableEntry = rateTableEntry;
	}

	public List<RateTableEntry> getRateTableEntry() {
		if (rateTableEntry == null) {
			rateTableEntry = new ArrayList<RateTableEntry>();
		}
		return rateTableEntry;
	}

}
