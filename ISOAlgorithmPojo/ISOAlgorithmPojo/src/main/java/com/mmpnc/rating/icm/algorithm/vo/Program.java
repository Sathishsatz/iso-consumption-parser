package com.mmpnc.rating.icm.algorithm.vo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RatingProgram", propOrder = {
		"id",
		"name",
		"executionorder",
		"steps"
})
@Entity
@Table(name = "RatingProgram")
public class Program {
	@Id
	@GeneratedValue
	@XmlElement(name="Id")
	private int id;
	
	@XmlElement(name="Name")
	private String name;

	@XmlElement(name="ExecutionOrder")
	private int executionorder;
	
	@OneToMany(mappedBy = "program")
	@OrderBy(value = "executionorder")
	@XmlElement(name = "ProgramStep")
	private List<Step> steps;
	
	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Process.class)
	@JoinColumn(name = "processid")
	@XmlTransient
	private Process process;

	public Program() {
		this.name = "";
		this.process = new Process();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setExecutionorder(int executionorder) {
		this.executionorder = executionorder;
	}

	public int getExecutionorder() {
		return executionorder;
	}

	public void setStep(List<Step> steps) {
		this.steps = steps;
	}

	public List<Step> getStep() {
		if(this.steps == null){
			this.steps = new ArrayList<Step>();
		}
		return steps;
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	@Override
	public String toString() {
		return this.process.toString() + "." + this.name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Program) {
			Program prg = (Program) obj;
			if (prg.getName().equals(this.name)
					&& prg.getProcess().equals(this.process) && prg.getStep().equals(this.steps)) {
				return true;
			} else {
				return super.equals(obj);
			}
		} else {
			return super.equals(obj);
		}
	}

}
