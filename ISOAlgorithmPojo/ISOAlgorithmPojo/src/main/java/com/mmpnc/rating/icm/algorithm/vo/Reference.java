package com.mmpnc.rating.icm.algorithm.vo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityReference", propOrder = {
		"id",
		"name"
})
@Entity
@Table(name = "EntityReference")
public class Reference {
	@Id
	@GeneratedValue
	@XmlElement(name="Id")
	private int id;
	
	@XmlElement(name="Name")
	private String name;

	@ManyToOne(cascade = CascadeType.ALL, targetEntity = RatingEntity.class)
	@JoinColumn(name = "entityid")
	@XmlTransient
	private RatingEntity entity;

	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Ratebook.class)
	@JoinColumn(name = "ratebookid")
	@XmlTransient
	private Ratebook ratebook;
	
	public Reference() {
		name = "";
		entity = new RatingEntity();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public RatingEntity getEntity() {
		return entity;
	}

	public void setEntity(RatingEntity entity) {
		this.entity = entity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Reference) {
			Reference ref = (Reference) obj;
			if (ref.getName().equals(this.name)
					&& ref.getEntity().equals(this.entity)) {
				return true;
			} else {
				return super.equals(obj);
			}
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.name + " [" + this.entity.toString() + "]";
	}

	public void setRatebook(Ratebook ratebook) {
		this.ratebook = ratebook;
	}

	public Ratebook getRatebook() {
		return ratebook;
	}
}
